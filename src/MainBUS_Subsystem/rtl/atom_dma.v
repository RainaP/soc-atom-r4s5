module dma (
   clk                                      ,
   arstn                                    ,

   //---------------------------------------------
   // Control bus
   //---------------------------------------------
   // Control bus signals
   cbus_aclk                              ,
   cbus_aresetn                           ,

   //Write Address Channel
   cbus_awid                              ,
   cbus_awaddr                            ,
   cbus_awlen                             ,
   cbus_awsize                            ,
   cbus_awburst                           ,
   cbus_awcache                           ,
   cbus_awprot                            ,
   cbus_awqos                             ,
   cbus_awregion                          ,
   cbus_awuser                            ,
   cbus_awvalid                           ,
   cbus_awready                           ,

   //Write Data Channel
   cbus_wdata                             ,
   cbus_wstrb                             ,
   cbus_wlast                             ,
   cbus_wuser                             ,
   cbus_wvalid                            ,
   cbus_wready                            ,
   
   //Write Response Channel
   cbus_bready                            ,
   cbus_bid                               ,
   cbus_bresp                             ,
   cbus_buser                             ,
   cbus_bvalid                            ,

   //Read Address Channel
   cbus_arid                              ,
   cbus_araddr                            ,
   cbus_arlen                             ,
   cbus_arsize                            ,
   cbus_arburst                           ,
   cbus_arcache                           ,
   cbus_arprot                            ,
   cbus_arqos                             ,
   cbus_arregion                          ,
   cbus_aruser                            ,
   cbus_arvalid                           ,
   cbus_arready                           ,

   //Read Data Channel
   cbus_rready                            ,
   cbus_rid                               ,
   cbus_rdata                             ,
   cbus_rresp                             ,
   cbus_rlast                             ,
   cbus_ruser                             ,
   cbus_rvalid                            ,

   //---------------------------------------------
   // Data bus
   //---------------------------------------------
   // Control bus signals
   dbus_aclk                              ,
   dbus_aresetn                           ,

   //Write Address Channel
   dbus_awready                           ,
   dbus_awid                              ,
   dbus_awaddr                            ,
   dbus_awlen                             ,
   dbus_awsize                            ,
   dbus_awburst                           ,
   dbus_awcache                           ,
   dbus_awprot                            ,
   dbus_awqos                             ,
   dbus_awregion                          ,
   dbus_awuser                            ,
   dbus_awvalid                           ,

   //Write Data Channel
   dbus_wready                            ,
   dbus_wdata                             ,
   dbus_wstrb                             ,
   dbus_wlast                             ,
   dbus_wuser                             ,
   dbus_wvalid                            ,
   
   //Write Response Channel
   dbus_bready                            ,
   dbus_bid                               ,
   dbus_bresp                             ,
   dbus_buser                             ,
   dbus_bvalid                            ,

   //Read Address Channel
   dbus_arready                           ,
   dbus_arid                              ,
   dbus_araddr                            ,
   dbus_arlen                             ,
   dbus_arsize                            ,
   dbus_arburst                           ,
   dbus_arcache                           ,
   dbus_arprot                            ,
   dbus_arqos                             ,
   dbus_arregion                          ,
   dbus_aruser                            ,
   dbus_arvalid                           ,

   //Read Data Channel
   dbus_rid                               ,
   dbus_rdata                             ,
   dbus_rresp                             ,
   dbus_rlast                             ,
   dbus_ruser                             ,
   dbus_rvalid                            ,
   dbus_rready                            ,

   
   //Interrupt
   o_intr_finish                            ,
   o_intr_error                             ,

   o_idle
   );
   
`pragma protect begin
   //dDMA Hardware Configuration
   parameter      pt_entry_num_P            = 80;
   parameter      task_exe_num_P            = 16;
   parameter      db_entry_num_P            = 128;

   parameter      cxtid_w_P                 = 7;
   parameter      pid_w_P                   = 4;
   parameter      cmdid_w_P                 = 16;
   parameter      taskid_w_P                = 4;

   parameter      cb_addr_w_P               = 12;
   parameter      vaddr_w_P                 = 37;
   parameter      paddr_w_P                 = 37;
   parameter      pt_memrange_w_P           = 21;

   localparam     va_tag_w_LP               = vaddr_w_P - pt_memrange_w_P;

   localparam     axi_cb_id_w_LP            = 8;
   localparam     axi_cb_addr_w_LP          = cb_addr_w_P;
   localparam     axi_cb_len_w_LP           = 8;
   localparam     axi_cb_size_w_LP          = 3;
   localparam     axi_cb_burst_w_LP         = 2;
   localparam     axi_cb_cache_w_LP         = 4;
   localparam     axi_cb_prot_w_LP          = 3;
   localparam     axi_cb_qos_w_LP           = 4;
   localparam     axi_cb_region_w_LP        = 4;
   localparam     axi_cb_user_w_LP          = 4;
   localparam     axi_cb_data_w_LP          = 32;
   localparam     axi_cb_wstrb_w_LP         = axi_cb_data_w_LP/4;
   localparam     axi_cb_bresp_w_LP         = 2;
   localparam     axi_cb_rresp_w_LP         = 2;

   localparam     axi_db_id_w_LP            = 9;
   localparam     axi_db_addr_w_LP          = paddr_w_P;
   localparam     axi_db_len_w_LP           = 8;
   localparam     axi_db_size_w_LP          = 3; // 'b111 (128Bytes)
   localparam     axi_db_burst_w_LP         = 2; // 'b01  (INCR)
   localparam     axi_db_cache_w_LP         = 4;
   localparam     axi_db_prot_w_LP          = 3;
   localparam     axi_db_qos_w_LP           = 4;
   localparam     axi_db_region_w_LP        = 4;
   localparam     axi_db_user_w_LP          = 4;
   localparam     axi_db_data_w_LP          = 1024;
   localparam     axi_db_wstrb_w_LP         = axi_db_data_w_LP/8;
   localparam     axi_db_bresp_w_LP         = 2;
   localparam     axi_db_rresp_w_LP         = 2;

   ////uDMA Hardware Configuration
   //parameter   pt_entry_num_P = 16;
   //parameter   task_exe_num_P = 2;
   //parameter   db_entry_num_P = 16;
   
`pragma protect end
   //---------------------------------------------------------------------------
   //input and output port declaration
   //---------------------------------------------------------------------------
   input                                     clk                               ;
   input                                     arstn;

   //---------------------------------------------
   // Control bus
   //---------------------------------------------
   // Control bus signals
   input                                     cbus_aclk                       ;
   input                                     cbus_aresetn                    ;

   //Write Address Channel
   input  [axi_cb_id_w_LP-1       :0]        cbus_awid                       ;
   input  [axi_cb_addr_w_LP-1     :0]        cbus_awaddr                     ;
   input  [axi_cb_len_w_LP-1      :0]        cbus_awlen                      ;
   input  [axi_cb_size_w_LP-1     :0]        cbus_awsize                     ;
   input  [axi_cb_burst_w_LP-1    :0]        cbus_awburst                    ;
   input  [axi_cb_cache_w_LP-1    :0]        cbus_awcache                    ;
   input  [axi_cb_prot_w_LP-1     :0]        cbus_awprot                     ;
   input  [axi_cb_qos_w_LP-1      :0]        cbus_awqos                      ;
   input  [axi_cb_region_w_LP-1   :0]        cbus_awregion                   ;
   input  [axi_cb_user_w_LP-1     :0]        cbus_awuser                     ;
   input                                     cbus_awvalid                    ;
   output                                    cbus_awready                    ;

   //Write Data Channel
   input  [axi_cb_data_w_LP-1     :0]        cbus_wdata                      ;
   input  [axi_cb_wstrb_w_LP-1    :0]        cbus_wstrb                      ;
   input                                     cbus_wlast                      ;
   input  [axi_cb_user_w_LP-1     :0]        cbus_wuser                      ;
   input                                     cbus_wvalid                     ;
   output                                    cbus_wready                     ;
   
   //Write Response Channel
   input                                     cbus_bready                     ;
   output [axi_cb_id_w_LP-1       :0]        cbus_bid                        ;
   output [axi_cb_bresp_w_LP-1    :0]        cbus_bresp                      ;
   output [axi_cb_user_w_LP-1     :0]        cbus_buser                      ;
   output                                    cbus_bvalid                     ;

   //Read Address Channel
   input  [axi_cb_id_w_LP-1       :0]        cbus_arid                       ;
   input  [axi_cb_addr_w_LP-1     :0]        cbus_araddr                     ;
   input  [axi_cb_len_w_LP-1      :0]        cbus_arlen                      ;
   input  [axi_cb_size_w_LP-1     :0]        cbus_arsize                     ;
   input  [axi_cb_burst_w_LP-1    :0]        cbus_arburst                    ;
   input  [axi_cb_cache_w_LP-1    :0]        cbus_arcache                    ;
   input  [axi_cb_prot_w_LP-1     :0]        cbus_arprot                     ;
   input  [axi_cb_qos_w_LP-1      :0]        cbus_arqos                      ;
   input  [axi_cb_region_w_LP-1   :0]        cbus_arregion                   ;
   input  [axi_cb_user_w_LP-1     :0]        cbus_aruser                     ;
   input                                     cbus_arvalid                    ;
   output                                    cbus_arready                    ;

   //Read Data Channel
   input                                     cbus_rready                     ;
   output [axi_cb_id_w_LP-1       :0]        cbus_rid                        ;
   output [axi_cb_data_w_LP-1     :0]        cbus_rdata                      ;
   output [axi_cb_rresp_w_LP-1    :0]        cbus_rresp                      ;
   output                                    cbus_rlast                      ;
   output [axi_cb_user_w_LP-1     :0]        cbus_ruser                      ;
   output                                    cbus_rvalid                     ;

   //---------------------------------------------
   // Data bus
   //---------------------------------------------
   // Control bus signals
   input                                     dbus_aclk                       ;
   input                                     dbus_aresetn                    ;

   //Write Address Channel
   output [axi_db_id_w_LP-1       :0]        dbus_awid                       ;
   output [axi_db_addr_w_LP-1     :0]        dbus_awaddr                     ;
   output [axi_db_len_w_LP-1      :0]        dbus_awlen                      ;
   output [axi_db_size_w_LP-1     :0]        dbus_awsize                     ;
   output [axi_db_burst_w_LP-1    :0]        dbus_awburst                    ;
   output [axi_db_cache_w_LP-1    :0]        dbus_awcache                    ;
   output [axi_db_prot_w_LP-1     :0]        dbus_awprot                     ;
   output [axi_db_qos_w_LP-1      :0]        dbus_awqos                      ;
   output [axi_db_region_w_LP-1   :0]        dbus_awregion                   ;
   output [axi_db_user_w_LP-1     :0]        dbus_awuser                     ;
   output                                    dbus_awvalid                    ;
   input                                     dbus_awready                    ;

   //Write Data Channel
   output [axi_db_data_w_LP-1     :0]        dbus_wdata                      ;
   output [axi_db_wstrb_w_LP-1    :0]        dbus_wstrb                      ;
   output                                    dbus_wlast                      ;
   output [axi_db_user_w_LP-1     :0]        dbus_wuser                      ;
   output                                    dbus_wvalid                     ;
   input                                     dbus_wready                     ;
   
   //Write Response Channel
   output                                    dbus_bready                     ;
   input  [axi_db_id_w_LP-1       :0]        dbus_bid                        ;
   input  [axi_db_bresp_w_LP-1    :0]        dbus_bresp                      ;
   input  [axi_db_user_w_LP-1     :0]        dbus_buser                      ;
   input                                     dbus_bvalid                     ;

   //Read Address Channel
   output [axi_db_id_w_LP-1       :0]        dbus_arid                       ;
   output [axi_db_addr_w_LP-1     :0]        dbus_araddr                     ;
   output [axi_db_len_w_LP-1      :0]        dbus_arlen                      ;
   output [axi_db_size_w_LP-1     :0]        dbus_arsize                     ;
   output [axi_db_burst_w_LP-1    :0]        dbus_arburst                    ;
   output [axi_db_cache_w_LP-1    :0]        dbus_arcache                    ;
   output [axi_db_prot_w_LP-1     :0]        dbus_arprot                     ;
   output [axi_db_qos_w_LP-1      :0]        dbus_arqos                      ;
   output [axi_db_region_w_LP-1   :0]        dbus_arregion                   ;
   output [axi_db_user_w_LP-1     :0]        dbus_aruser                     ;
   output                                    dbus_arvalid                    ;
   input                                     dbus_arready                    ;

   //Read Data Channel
   output                                    dbus_rready                     ;
   input  [axi_db_id_w_LP-1       :0]        dbus_rid                        ;
   input  [axi_db_data_w_LP-1     :0]        dbus_rdata                      ;
   input  [axi_db_rresp_w_LP-1    :0]        dbus_rresp                      ;
   input                                     dbus_rlast                      ;
   input  [axi_db_user_w_LP-1     :0]        dbus_ruser                      ;
   input                                     dbus_rvalid                     ;

	//Interrupt signals
	output                                    o_intr_finish                     ;
	output                                    o_intr_error                      ;
	output                                    o_idle                            ;



   assign cbus_awready    = '1                ;
   assign cbus_wready     = '1                ;
   assign cbus_bid        = '0                ;
   assign cbus_bresp      = '0                ;
   assign cbus_buser      = '0                ;
   assign cbus_bvalid     = '0                ;
   assign cbus_arready    = '1                ;
   assign cbus_rid        = '0                ;
   assign cbus_rdata      = '0                ;
   assign cbus_rresp      = '0                ;
   assign cbus_rlast      = '0                ;
   assign cbus_ruser      = '0                ;
   assign cbus_rvalid     = '0                ;
   assign dbus_awid       = '0                ;
   assign dbus_awaddr     = '0                ;
   assign dbus_awlen      = '0                ;
   assign dbus_awsize     = '0                ;
   assign dbus_awburst    = '0                ;
   assign dbus_awcache    = '0                ;
   assign dbus_awprot     = '0                ;
   assign dbus_awqos      = '0                ;
   assign dbus_awregion   = '0                ;
   assign dbus_awuser     = '0                ;
   assign dbus_awvalid    = '0                ;
   assign dbus_wdata      = '0                ;
   assign dbus_wstrb      = '0                ;
   assign dbus_wlast      = '0                ;
   assign dbus_wuser      = '0                ;
   assign dbus_wvalid     = '0                ;
   assign dbus_bready     = '1                ;
   assign dbus_arid       = '0                ;
   assign dbus_araddr     = '0                ;
   assign dbus_arlen      = '0                ;
   assign dbus_arsize     = '0                ;
   assign dbus_arburst    = '0                ;
   assign dbus_arcache    = '0                ;
   assign dbus_arprot     = '0                ;
   assign dbus_arqos      = '0                ;
   assign dbus_arregion   = '0                ;
   assign dbus_aruser     = '0                ;
   assign dbus_arvalid    = '0                ;
   assign dbus_rready     = '1                ;
   assign o_intr_finish   = '0                  ;
   assign o_intr_error    = '0                  ;
   assign o_idle          = '0                  ;











endmodule
///------------------------------------------------------------------------------------------------
// End of file
///------------------------------------------------------------------------------------------------
