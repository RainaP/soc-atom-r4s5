
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:15

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_southbus_clkgrp;

// 0in set_cdc_clock Regime_cbus_south_Cm_main.root_Clk -module cbus_Structure_Module_southbus -group clk_cbus_south -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock Regime_cbus_south_Cm_main.root_Clk_ClkS -module cbus_Structure_Module_southbus -group clk_cbus_south -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock clk_cbus_south -module cbus_Structure_Module_southbus -group clk_cbus_south -period 1.000 -waveform {0.000 0.500}

endmodule
