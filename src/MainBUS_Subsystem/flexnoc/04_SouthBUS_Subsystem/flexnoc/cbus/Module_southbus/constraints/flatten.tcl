if {[get_cells ${CUSTOMER_HIERARCHY}Link8Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link8Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link8Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link8Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link8_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link8_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link8_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link8_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxClkAdapt_Link7Resp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxClkAdapt_Link7Resp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2c0_3_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2c0_3_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpRxClkAdapt_Link7_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpRxClkAdapt_Link7_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main/DtpRxClkAdapt_Link_c0_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main/DtpRxClkAdapt_Link_c0_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_southbus] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_southbus false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cbus_south_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cbus_south_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cbus_south_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cbus_south_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link8Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link8Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link8Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link8Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link8_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link8_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link8_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link8_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys/ClockGater false
}
