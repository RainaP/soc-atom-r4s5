//--------------------------------------------------------------------------------
//
//   Rebellions Inc. Confidential
//   All Rights Reserved -- Property of Rebellions Inc. 
//
//   Description : 
//
//   Author : jsyoon@rebellions.ai
//
//   date : 2021/08/07
//
//--------------------------------------------------------------------------------

module WestBUS_Subsystem(

	input         TM                                                                    ,
	input         arstn_cbus_ls_west                                                    ,
	input         clk_cbus_ls_west                                                      ,

	input  [57:0] ctrl_dp_Link2_to_Switch_west_Data                                          ,
	output [2:0]  ctrl_dp_Link2_to_Switch_west_RdCnt                                         ,
	output [1:0]  ctrl_dp_Link2_to_Switch_west_RdPtr                                         ,
	input         ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRst                                ,
	output        ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck                             ,
	output        ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRst                                ,
	input         ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck                             ,
	input  [2:0]  ctrl_dp_Link2_to_Switch_west_WrCnt                                         ,
	output [57:0] ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_Data                            ,
	input  [2:0]  ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdCnt                           ,
	input  [1:0]  ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdPtr                           ,
	output        ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRstAck               ,
	input         ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]  ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_WrCnt                           ,
	input  [57:0] ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_Data              ,
	output [2:0]  ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdCnt             ,
	output [1:0]  ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdPtr             ,
	input         ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRstAck ,
	output        ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]  ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_WrCnt             ,
	output [57:0] ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data                            ,
	input  [2:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt                           ,
	input  [1:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr                           ,
	output        ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck               ,
	input         ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt                           ,
	input  [57:0] ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data              ,
	output [2:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt             ,
	output [1:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr             ,
	input         ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck ,
	output        ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt             ,
	output [57:0] ctrl_dp_Switch_westResp001_to_Link13Resp001_Data                           ,
	input  [2:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_RdCnt                          ,
	input  [1:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_RdPtr                          ,
	output        ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst                 ,
	input         ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck              ,
	input         ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst                 ,
	output        ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck              ,
	output [2:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_WrCnt                          






);





	cbus_Structure_Module_cbus_ls_west u_cbus_Structure_Module_cbus_ls_west(
		.TM( TM )
	,	.arstn_cbus_ls_west( arstn_cbus_ls_west )
	,	.clk_cbus_ls_west( clk_cbus_ls_west )
	,	.dp_Link2_to_Switch_west_Data( ctrl_dp_Link2_to_Switch_west_Data )
	,	.dp_Link2_to_Switch_west_RdCnt( ctrl_dp_Link2_to_Switch_west_RdCnt )
	,	.dp_Link2_to_Switch_west_RdPtr( ctrl_dp_Link2_to_Switch_west_RdPtr )
	,	.dp_Link2_to_Switch_west_RxCtl_PwrOnRst( ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRst )
	,	.dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck( ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck )
	,	.dp_Link2_to_Switch_west_TxCtl_PwrOnRst( ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRst )
	,	.dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck( ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck )
	,	.dp_Link2_to_Switch_west_WrCnt( ctrl_dp_Link2_to_Switch_west_WrCnt )
	,	.dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_Data( ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_Data )
	,	.dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdCnt( ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdCnt )
	,	.dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdPtr( ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdPtr )
	,	.dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRst( ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRst( ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_WrCnt( ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_WrCnt )
	,	.dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_Data( ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_Data )
	,	.dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdCnt( ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdCnt )
	,	.dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdPtr( ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdPtr )
	,	.dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_WrCnt( ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_WrCnt )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt )
	,	.dp_Switch_westResp001_to_Link13Resp001_Data( ctrl_dp_Switch_westResp001_to_Link13Resp001_Data )
	,	.dp_Switch_westResp001_to_Link13Resp001_RdCnt( ctrl_dp_Switch_westResp001_to_Link13Resp001_RdCnt )
	,	.dp_Switch_westResp001_to_Link13Resp001_RdPtr( ctrl_dp_Switch_westResp001_to_Link13Resp001_RdPtr )
	,	.dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst( ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst )
	,	.dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck( ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck )
	,	.dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst( ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst )
	,	.dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck( ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck )
	,	.dp_Switch_westResp001_to_Link13Resp001_WrCnt( ctrl_dp_Switch_westResp001_to_Link13Resp001_WrCnt )
	);





endmodule
