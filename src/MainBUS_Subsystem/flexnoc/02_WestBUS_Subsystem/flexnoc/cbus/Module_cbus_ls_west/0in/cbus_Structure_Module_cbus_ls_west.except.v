
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:28

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_cbus_ls_west_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme reconvergence -from_signals Link_m0ctrl_asiResp001_main.DtpRxClkAdapt_Link_m0ctrl_astResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme reconvergence -from_signals Link_m0ctrl_asi_main.DtpTxClkAdapt_Link_m0ctrl_ast_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme reconvergence -from_signals Link_m1ctrl_asiResp001_main.DtpRxClkAdapt_Link_m1ctrl_astResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme reconvergence -from_signals Link_m1ctrl_asi_main.DtpTxClkAdapt_Link_m1ctrl_ast_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme reconvergence -from_signals Switch_westResp001_main.DtpTxClkAdapt_Link13Resp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme reconvergence -from_signals Switch_west_main.DtpRxClkAdapt_Link2_Async.RdCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme multi_sync_mux_select -from Link_m0ctrl_asiResp001_main.DtpRxClkAdapt_Link_m0ctrl_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme multi_sync_mux_select -from Link_m1ctrl_asiResp001_main.DtpRxClkAdapt_Link_m1ctrl_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_west -severity waived -scheme multi_sync_mux_select -from Switch_west_main.DtpRxClkAdapt_Link2_Async.uRegSync.instSynchronizerCell*

endmodule
