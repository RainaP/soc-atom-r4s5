
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:28

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_cbus_ls_west_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Link_m0ctrl_asi_main.DtpTxClkAdapt_Link_m0ctrl_ast_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Link_m0ctrl_asi_main.DtpTxClkAdapt_Link_m0ctrl_ast_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Link_m0ctrl_asi_main.DtpTxClkAdapt_Link_m0ctrl_ast_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Link_m0ctrl_asi_main.DtpTxClkAdapt_Link_m0ctrl_ast_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Link_m1ctrl_asi_main.DtpTxClkAdapt_Link_m1ctrl_ast_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Link_m1ctrl_asi_main.DtpTxClkAdapt_Link_m1ctrl_ast_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Link_m1ctrl_asi_main.DtpTxClkAdapt_Link_m1ctrl_ast_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Link_m1ctrl_asi_main.DtpTxClkAdapt_Link_m1ctrl_ast_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Switch_westResp001_main.DtpTxClkAdapt_Link13Resp001_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Switch_westResp001_main.DtpTxClkAdapt_Link13Resp001_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Switch_westResp001_main.DtpTxClkAdapt_Link13Resp001_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_west Switch_westResp001_main.DtpTxClkAdapt_Link13Resp001_Async.RegData_3

endmodule
