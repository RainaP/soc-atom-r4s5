if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_asiResp001_main/DtpRxClkAdapt_Link_m0ctrl_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_asiResp001_main/DtpRxClkAdapt_Link_m0ctrl_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_asi_main/DtpTxClkAdapt_Link_m0ctrl_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_asi_main/DtpTxClkAdapt_Link_m0ctrl_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_yResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_yResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_yResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_yResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_y_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_y_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_y_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_y_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_zResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_zResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_zResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_zResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_z_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_z_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0ctrl_z_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0ctrl_z_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_asiResp001_main/DtpRxClkAdapt_Link_m1ctrl_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_asiResp001_main/DtpRxClkAdapt_Link_m1ctrl_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_asi_main/DtpTxClkAdapt_Link_m1ctrl_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_asi_main/DtpTxClkAdapt_Link_m1ctrl_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_yResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_yResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_yResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_yResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_y_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_y_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_y_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_y_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_zResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_zResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_zResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_zResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_z_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_z_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1ctrl_z_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1ctrl_z_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_cbus_ls_west] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_cbus_ls_west false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cbus_ls_west_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cbus_ls_west_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cbus_ls_west_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cbus_ls_west_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_westResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_westResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_westResp001_main/DtpTxClkAdapt_Link13Resp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_westResp001_main/DtpTxClkAdapt_Link13Resp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_westResp001_main/Mux_Link13Resp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_westResp001_main/Mux_Link13Resp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_west_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_west_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_west_main/Demux_Link2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_west_main/Demux_Link2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_west_main/DtpRxClkAdapt_Link2_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_west_main/DtpRxClkAdapt_Link2_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_yResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_yResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_yResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_yResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_y_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_y_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_y_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_y_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_zResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_zResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_zResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_zResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_z_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_z_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_z_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0ctrl_z_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_yResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_yResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_yResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_yResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_y_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_y_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_y_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_y_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_zResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_zResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_zResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_zResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_z_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_z_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_z_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1ctrl_z_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_westResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_westResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_westResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_westResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_west_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_west_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_west_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_west_main_Sys/ClockGater false
}
