
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:30

####################################################################

# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_cbus_ls_west"]

# Create Test Mode 

set_ideal_network -no_propagate [get_ports "TM"]

set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link2_to_Switch_west_Data"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link2_to_Switch_west_WrCnt"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdCnt"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_comb_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdPtr"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_Data"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_WrCnt"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_comb_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Switch_westResp001_to_Link13Resp001_RdCnt"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_comb_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Switch_westResp001_to_Link13Resp001_RdPtr"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link2_to_Switch_west_RdCnt"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link2_to_Switch_west_RdPtr"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_comb_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_Data"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_WrCnt"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdCnt"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdPtr"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_comb_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_comb_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Switch_westResp001_to_Link13Resp001_Data"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_ls_west_Cm_root [expr ${Regime_cbus_ls_west_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "ctrl_dp_Switch_westResp001_to_Link13Resp001_WrCnt"]

