
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:28

####################################################################

arteris_clock -name "clk_cbus_ls_west" -clock_domain "clk_cbus_ls_west" -port "clk_cbus_ls_west" -period "${clk_cbus_ls_west_P}" -waveform "[expr ${clk_cbus_ls_west_P}*0.00] [expr ${clk_cbus_ls_west_P}*0.50]" -edge "R" -user_directive ""
