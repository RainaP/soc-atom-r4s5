//--------------------------------------------------------------------------------
//
//   Rebellions Inc. Confidential
//   All Rights Reserved -- Property of Rebellions Inc. 
//
//   Description : top of mainbus subsystem
//
//   Author : jsyoon@rebellions.ai
//
//   date : 2021/08/07
//
//--------------------------------------------------------------------------------

module MainBUS_Subsystem(

	input          TM,
	input          arstn_dbus,
	input          arstn_cbus,
	input          arstn_sub,
	input          clk_dbus,
	input          clk_cbus,
	input          clk_sub, 

	// PCIE
	// read
	input  [128:0] r_dp_Link_c0_asi_to_Link_c0_ast_Data                      ,
	output [2:0]   r_dp_Link_c0_asi_to_Link_c0_ast_RdCnt                     ,
	output [2:0]   r_dp_Link_c0_asi_to_Link_c0_ast_RdPtr                     ,
	input          r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst            ,
	output         r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck         ,
	output         r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst            ,
	input          r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]   r_dp_Link_c0_asi_to_Link_c0_ast_WrCnt                     ,
	output [705:0] r_dp_Link_c0_astResp_to_Link_c0_asiResp_Data              ,
	input  [2:0]   r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt             ,
	input  [2:0]   r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr             ,
	output         r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst    ,
	input          r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck ,
	input          r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst    ,
	output         r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]   r_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt             ,
	// write 
	input  [702:0] w_dp_Link_c0_asi_to_Link_c0_ast_Data                      ,
	output [2:0]   w_dp_Link_c0_asi_to_Link_c0_ast_RdCnt                     ,
	output [2:0]   w_dp_Link_c0_asi_to_Link_c0_ast_RdPtr                     ,
	input          w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst            ,
	output         w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck         ,
	output         w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst            ,
	input          w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]   w_dp_Link_c0_asi_to_Link_c0_ast_WrCnt                     ,
	output [125:0] w_dp_Link_c0_astResp_to_Link_c0_asiResp_Data              ,
	input  [2:0]   w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt             ,
	input  [2:0]   w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr             ,
	output         w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst    ,
	input          w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck ,
	input          w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst    ,
	output         w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]   w_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt             ,



	// CP
	// read
	input  [128:0]  r_dp_Link_c1_asi_to_Link_c1_ast_Data                            ,
	output [2:0]    r_dp_Link_c1_asi_to_Link_c1_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_c1_asi_to_Link_c1_ast_RdPtr                           ,
	input           r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_c1_asi_to_Link_c1_ast_WrCnt                           ,
	output [273:0]  r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr             ,
	output          r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt             ,
	output [128:0]  r_dp_Link_c1t_asi_to_Link_c1t_ast_Data                          ,
	input  [2:0]    r_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt                         ,
	input  [2:0]    r_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr                         ,
	output          r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst                ,
	input           r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck             ,
	input           r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst                ,
	output          r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck             ,
	output [2:0]    r_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt                         ,
	input  [273:0]  r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data                  ,
	output [2:0]    r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt                 ,
	output [2:0]    r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr                 ,
	input           r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst        ,
	output          r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck     ,
	output          r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst        ,
	input           r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck     ,
	input  [2:0]    r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt                 ,
	// write

	input  [270:0]  w_dp_Link_c1_asi_to_Link_c1_ast_Data                            ,
	output [2:0]    w_dp_Link_c1_asi_to_Link_c1_ast_RdCnt                           ,
	output [2:0]    w_dp_Link_c1_asi_to_Link_c1_ast_RdPtr                           ,
	input           w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst                  ,
	output          w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck               ,
	output          w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst                  ,
	input           w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    w_dp_Link_c1_asi_to_Link_c1_ast_WrCnt                           ,
	output [125:0]  w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data              ,
	input  [2:0]    w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt             ,
	input  [2:0]    w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr             ,
	output          w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst    ,
	input           w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst    ,
	output          w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt             ,
	output [270:0]  w_dp_Link_c1t_asi_to_Link_c1t_ast_Data                          ,
	input  [2:0]    w_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt                         ,
	input  [2:0]    w_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr                         ,
	output          w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst                ,
	input           w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck             ,
	input           w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst                ,
	output          w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck             ,
	output [2:0]    w_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt                         ,
	input  [125:0]  w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data                  ,
	output [2:0]    w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt                 ,
	output [2:0]    w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr                 ,
	input           w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst        ,
	output          w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck     ,
	output          w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst        ,
	input           w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck     ,
	input  [2:0]    w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt                 ,




	// DDR
	// read
	output [128:0]  r_dp_Link_m0_asi_to_Link_m0_ast_Data                            ,
	input  [2:0]    r_dp_Link_m0_asi_to_Link_m0_ast_RdCnt                           ,
	input  [2:0]    r_dp_Link_m0_asi_to_Link_m0_ast_RdPtr                           ,
	output          r_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRst                  ,
	input           r_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRstAck               ,
	input           r_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRst                  ,
	output          r_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]    r_dp_Link_m0_asi_to_Link_m0_ast_WrCnt                           ,
	input  [705:0]  r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_Data              ,
	output [2:0]    r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdCnt             ,
	output [2:0]    r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdPtr             ,
	input           r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRst    ,
	output          r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRstAck ,
	output          r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRst    ,
	input           r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]    r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_WrCnt             ,
	output [128:0]  r_dp_Link_m1_asi_to_Link_m1_ast_Data                            ,
	input  [2:0]    r_dp_Link_m1_asi_to_Link_m1_ast_RdCnt                           ,
	input  [2:0]    r_dp_Link_m1_asi_to_Link_m1_ast_RdPtr                           ,
	output          r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst                  ,
	input           r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck               ,
	input           r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst                  ,
	output          r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]    r_dp_Link_m1_asi_to_Link_m1_ast_WrCnt                           ,
	input  [705:0]  r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data              ,
	output [2:0]    r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt             ,
	output [2:0]    r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr             ,
	input           r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst    ,
	output          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck ,
	output          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst    ,
	input           r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]    r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt             ,

	output [128:0]  r_dp_Link_m2_asi_to_Link_m2_ast_Data                            ,
	input  [2:0]    r_dp_Link_m2_asi_to_Link_m2_ast_RdCnt                           ,
	input  [2:0]    r_dp_Link_m2_asi_to_Link_m2_ast_RdPtr                           ,
	output          r_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst                  ,
	input           r_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck               ,
	input           r_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst                  ,
	output          r_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]    r_dp_Link_m2_asi_to_Link_m2_ast_WrCnt                           ,
	input  [705:0]  r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data              ,
	output [2:0]    r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt             ,
	output [2:0]    r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr             ,
	input           r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst    ,
	output          r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck ,
	output          r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst    ,
	input           r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]    r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt             ,
	output [128:0]  r_dp_Link_m3_asi_to_Link_m3_ast_Data                            ,
	input  [2:0]    r_dp_Link_m3_asi_to_Link_m3_ast_RdCnt                           ,
	input  [2:0]    r_dp_Link_m3_asi_to_Link_m3_ast_RdPtr                           ,
	output          r_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst                  ,
	input           r_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck               ,
	input           r_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst                  ,
	output          r_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]    r_dp_Link_m3_asi_to_Link_m3_ast_WrCnt                           ,
	input  [705:0]  r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data              ,
	output [2:0]    r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt             ,
	output [2:0]    r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr             ,
	input           r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst    ,
	output          r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck ,
	output          r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst    ,
	input           r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]    r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt             ,
	// write
	output [702:0]  w_dp_Link_m0_asi_to_Link_m0_ast_Data                            ,
	input  [2:0]    w_dp_Link_m0_asi_to_Link_m0_ast_RdCnt                           ,
	input  [2:0]    w_dp_Link_m0_asi_to_Link_m0_ast_RdPtr                           ,
	output          w_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRst                  ,
	input           w_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRstAck               ,
	input           w_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRst                  ,
	output          w_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]    w_dp_Link_m0_asi_to_Link_m0_ast_WrCnt                           ,
	input  [125:0]  w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_Data              ,
	output [2:0]    w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdCnt             ,
	output [2:0]    w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdPtr             ,
	input           w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRst    ,
	output          w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRstAck ,
	output          w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRst    ,
	input           w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]    w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_WrCnt             ,
	output [702:0]  w_dp_Link_m1_asi_to_Link_m1_ast_Data                            ,
	input  [2:0]    w_dp_Link_m1_asi_to_Link_m1_ast_RdCnt                           ,
	input  [2:0]    w_dp_Link_m1_asi_to_Link_m1_ast_RdPtr                           ,
	output          w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst                  ,
	input           w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck               ,
	input           w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst                  ,
	output          w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]    w_dp_Link_m1_asi_to_Link_m1_ast_WrCnt                           ,
	input  [125:0]  w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data              ,
	output [2:0]    w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt             ,
	output [2:0]    w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr             ,
	input           w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst    ,
	output          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck ,
	output          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst    ,
	input           w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]    w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt             ,

	output [702:0]  w_dp_Link_m2_asi_to_Link_m2_ast_Data                            ,
	input  [2:0]    w_dp_Link_m2_asi_to_Link_m2_ast_RdCnt                           ,
	input  [2:0]    w_dp_Link_m2_asi_to_Link_m2_ast_RdPtr                           ,
	output          w_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst                  ,
	input           w_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck               ,
	input           w_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst                  ,
	output          w_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]    w_dp_Link_m2_asi_to_Link_m2_ast_WrCnt                           ,
	input  [125:0]  w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data              ,
	output [2:0]    w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt             ,
	output [2:0]    w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr             ,
	input           w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst    ,
	output          w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck ,
	output          w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst    ,
	input           w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]    w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt             ,
	output [702:0]  w_dp_Link_m3_asi_to_Link_m3_ast_Data                            ,
	input  [2:0]    w_dp_Link_m3_asi_to_Link_m3_ast_RdCnt                           ,
	input  [2:0]    w_dp_Link_m3_asi_to_Link_m3_ast_RdPtr                           ,
	output          w_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst                  ,
	input           w_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck               ,
	input           w_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst                  ,
	output          w_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]    w_dp_Link_m3_asi_to_Link_m3_ast_WrCnt                           ,
	input  [125:0]  w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data              ,
	output [2:0]    w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt             ,
	output [2:0]    w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr             ,
	input           w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst    ,
	output          w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck ,
	output          w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst    ,
	input           w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]    w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt             ,



	// Dcluster
	// read
	input  [128:0]  r_dp_Link_n0_asi_to_Link_n0_ast_Data                            ,
	output [2:0]    r_dp_Link_n0_asi_to_Link_n0_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_n0_asi_to_Link_n0_ast_RdPtr                           ,
	input           r_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_n0_asi_to_Link_n0_ast_WrCnt                           ,
	output [1281:0] r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdPtr             ,
	output          r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_WrCnt             ,
	input  [128:0]  r_dp_Link_n1_asi_to_Link_n1_ast_Data                            ,
	output [2:0]    r_dp_Link_n1_asi_to_Link_n1_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_n1_asi_to_Link_n1_ast_RdPtr                           ,
	input           r_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_n1_asi_to_Link_n1_ast_WrCnt                           ,
	output [1281:0] r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdPtr             ,
	output          r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_WrCnt             ,
	input  [128:0]  r_dp_Link_n2_asi_to_Link_n2_ast_Data                            ,
	output [2:0]    r_dp_Link_n2_asi_to_Link_n2_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_n2_asi_to_Link_n2_ast_RdPtr                           ,
	input           r_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_n2_asi_to_Link_n2_ast_WrCnt                           ,
	output [1281:0] r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdPtr             ,
	output          r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_WrCnt             ,
	input  [128:0]  r_dp_Link_n3_asi_to_Link_n3_ast_Data                            ,
	output [2:0]    r_dp_Link_n3_asi_to_Link_n3_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_n3_asi_to_Link_n3_ast_RdPtr                           ,
	input           r_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_n3_asi_to_Link_n3_ast_WrCnt                           ,
	output [1281:0] r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdPtr             ,
	output          r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_WrCnt             ,

	input  [128:0]  r_dp_Link_n4_asi_to_Link_n4_ast_Data                            ,
	output [2:0]    r_dp_Link_n4_asi_to_Link_n4_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_n4_asi_to_Link_n4_ast_RdPtr                           ,
	input           r_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_n4_asi_to_Link_n4_ast_WrCnt                           ,
	output [1281:0] r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdPtr             ,
	output          r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_WrCnt             ,
	input  [128:0]  r_dp_Link_n5_asi_to_Link_n5_ast_Data                            ,
	output [2:0]    r_dp_Link_n5_asi_to_Link_n5_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_n5_asi_to_Link_n5_ast_RdPtr                           ,
	input           r_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_n5_asi_to_Link_n5_ast_WrCnt                           ,
	output [1281:0] r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdPtr             ,
	output          r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_WrCnt             ,
	input  [128:0]  r_dp_Link_n6_asi_to_Link_n6_ast_Data                            ,
	output [2:0]    r_dp_Link_n6_asi_to_Link_n6_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_n6_asi_to_Link_n6_ast_RdPtr                           ,
	input           r_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_n6_asi_to_Link_n6_ast_WrCnt                           ,
	output [1281:0] r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdPtr             ,
	output          r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_WrCnt             ,
	input  [128:0]  r_dp_Link_n7_asi_to_Link_n7_ast_Data                            ,
	output [2:0]    r_dp_Link_n7_asi_to_Link_n7_ast_RdCnt                           ,
	output [2:0]    r_dp_Link_n7_asi_to_Link_n7_ast_RdPtr                           ,
	input           r_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst                  ,
	output          r_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck               ,
	output          r_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst                  ,
	input           r_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]    r_dp_Link_n7_asi_to_Link_n7_ast_WrCnt                           ,
	output [1281:0] r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_Data              ,
	input  [2:0]    r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdCnt             ,
	input  [2:0]    r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdPtr             ,
	output          r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRst    ,
	input           r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRstAck ,
	input           r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRst    ,
	output          r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]    r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_WrCnt             ,
	// write

	input  [1278:0] w_dp_Link_n0_asi_to_Link_n0_ast_Data                      ,
	output [2:0]    w_dp_Link_n0_asi_to_Link_n0_ast_RdCnt                     ,
	output [2:0]    w_dp_Link_n0_asi_to_Link_n0_ast_RdPtr                     ,
	input           w_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst            ,
	output          w_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck         ,
	output          w_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst            ,
	input           w_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]    w_dp_Link_n0_asi_to_Link_n0_ast_WrCnt                     ,
	output [125:0]  w_dp_Link_n0_astResp_to_Link_n0_asiResp_Data              ,
	input  [2:0]    w_dp_Link_n0_astResp_to_Link_n0_asiResp_RdCnt             ,
	input  [2:0]    w_dp_Link_n0_astResp_to_Link_n0_asiResp_RdPtr             ,
	output          w_dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRst    ,
	input           w_dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRst    ,
	output          w_dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_n0_astResp_to_Link_n0_asiResp_WrCnt             ,
	input  [1278:0] w_dp_Link_n1_asi_to_Link_n1_ast_Data                      ,
	output [2:0]    w_dp_Link_n1_asi_to_Link_n1_ast_RdCnt                     ,
	output [2:0]    w_dp_Link_n1_asi_to_Link_n1_ast_RdPtr                     ,
	input           w_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst            ,
	output          w_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck         ,
	output          w_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst            ,
	input           w_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]    w_dp_Link_n1_asi_to_Link_n1_ast_WrCnt                     ,
	output [125:0]  w_dp_Link_n1_astResp_to_Link_n1_asiResp_Data              ,
	input  [2:0]    w_dp_Link_n1_astResp_to_Link_n1_asiResp_RdCnt             ,
	input  [2:0]    w_dp_Link_n1_astResp_to_Link_n1_asiResp_RdPtr             ,
	output          w_dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRst    ,
	input           w_dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRst    ,
	output          w_dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_n1_astResp_to_Link_n1_asiResp_WrCnt             ,
	input  [1278:0] w_dp_Link_n2_asi_to_Link_n2_ast_Data                      ,
	output [2:0]    w_dp_Link_n2_asi_to_Link_n2_ast_RdCnt                     ,
	output [2:0]    w_dp_Link_n2_asi_to_Link_n2_ast_RdPtr                     ,
	input           w_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst            ,
	output          w_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck         ,
	output          w_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst            ,
	input           w_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]    w_dp_Link_n2_asi_to_Link_n2_ast_WrCnt                     ,
	output [125:0]  w_dp_Link_n2_astResp_to_Link_n2_asiResp_Data              ,
	input  [2:0]    w_dp_Link_n2_astResp_to_Link_n2_asiResp_RdCnt             ,
	input  [2:0]    w_dp_Link_n2_astResp_to_Link_n2_asiResp_RdPtr             ,
	output          w_dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRst    ,
	input           w_dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRst    ,
	output          w_dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_n2_astResp_to_Link_n2_asiResp_WrCnt             ,
	input  [1278:0] w_dp_Link_n3_asi_to_Link_n3_ast_Data                      ,
	output [2:0]    w_dp_Link_n3_asi_to_Link_n3_ast_RdCnt                     ,
	output [2:0]    w_dp_Link_n3_asi_to_Link_n3_ast_RdPtr                     ,
	input           w_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst            ,
	output          w_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck         ,
	output          w_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst            ,
	input           w_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]    w_dp_Link_n3_asi_to_Link_n3_ast_WrCnt                     ,
	output [125:0]  w_dp_Link_n3_astResp_to_Link_n3_asiResp_Data              ,
	input  [2:0]    w_dp_Link_n3_astResp_to_Link_n3_asiResp_RdCnt             ,
	input  [2:0]    w_dp_Link_n3_astResp_to_Link_n3_asiResp_RdPtr             ,
	output          w_dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRst    ,
	input           w_dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRst    ,
	output          w_dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_n3_astResp_to_Link_n3_asiResp_WrCnt             ,

	input  [1278:0] w_dp_Link_n4_asi_to_Link_n4_ast_Data                      ,
	output [2:0]    w_dp_Link_n4_asi_to_Link_n4_ast_RdCnt                     ,
	output [2:0]    w_dp_Link_n4_asi_to_Link_n4_ast_RdPtr                     ,
	input           w_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst            ,
	output          w_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck         ,
	output          w_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst            ,
	input           w_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]    w_dp_Link_n4_asi_to_Link_n4_ast_WrCnt                     ,
	output [125:0]  w_dp_Link_n4_astResp_to_Link_n4_asiResp_Data              ,
	input  [2:0]    w_dp_Link_n4_astResp_to_Link_n4_asiResp_RdCnt             ,
	input  [2:0]    w_dp_Link_n4_astResp_to_Link_n4_asiResp_RdPtr             ,
	output          w_dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRst    ,
	input           w_dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRst    ,
	output          w_dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_n4_astResp_to_Link_n4_asiResp_WrCnt             ,
	input  [1278:0] w_dp_Link_n5_asi_to_Link_n5_ast_Data                      ,
	output [2:0]    w_dp_Link_n5_asi_to_Link_n5_ast_RdCnt                     ,
	output [2:0]    w_dp_Link_n5_asi_to_Link_n5_ast_RdPtr                     ,
	input           w_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst            ,
	output          w_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck         ,
	output          w_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst            ,
	input           w_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]    w_dp_Link_n5_asi_to_Link_n5_ast_WrCnt                     ,
	output [125:0]  w_dp_Link_n5_astResp_to_Link_n5_asiResp_Data              ,
	input  [2:0]    w_dp_Link_n5_astResp_to_Link_n5_asiResp_RdCnt             ,
	input  [2:0]    w_dp_Link_n5_astResp_to_Link_n5_asiResp_RdPtr             ,
	output          w_dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRst    ,
	input           w_dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRst    ,
	output          w_dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_n5_astResp_to_Link_n5_asiResp_WrCnt             ,
	input  [1278:0] w_dp_Link_n6_asi_to_Link_n6_ast_Data                      ,
	output [2:0]    w_dp_Link_n6_asi_to_Link_n6_ast_RdCnt                     ,
	output [2:0]    w_dp_Link_n6_asi_to_Link_n6_ast_RdPtr                     ,
	input           w_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst            ,
	output          w_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck         ,
	output          w_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst            ,
	input           w_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]    w_dp_Link_n6_asi_to_Link_n6_ast_WrCnt                     ,
	output [125:0]  w_dp_Link_n6_astResp_to_Link_n6_asiResp_Data              ,
	input  [2:0]    w_dp_Link_n6_astResp_to_Link_n6_asiResp_RdCnt             ,
	input  [2:0]    w_dp_Link_n6_astResp_to_Link_n6_asiResp_RdPtr             ,
	output          w_dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRst    ,
	input           w_dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRst    ,
	output          w_dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_n6_astResp_to_Link_n6_asiResp_WrCnt             ,
	input  [1278:0] w_dp_Link_n7_asi_to_Link_n7_ast_Data                      ,
	output [2:0]    w_dp_Link_n7_asi_to_Link_n7_ast_RdCnt                     ,
	output [2:0]    w_dp_Link_n7_asi_to_Link_n7_ast_RdPtr                     ,
	input           w_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst            ,
	output          w_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck         ,
	output          w_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst            ,
	input           w_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck         ,
	input  [2:0]    w_dp_Link_n7_asi_to_Link_n7_ast_WrCnt                     ,
	output [125:0]  w_dp_Link_n7_astResp_to_Link_n7_asiResp_Data              ,
	input  [2:0]    w_dp_Link_n7_astResp_to_Link_n7_asiResp_RdCnt             ,
	input  [2:0]    w_dp_Link_n7_astResp_to_Link_n7_asiResp_RdPtr             ,
	output          w_dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRst    ,
	input           w_dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRstAck ,
	input           w_dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRst    ,
	output          w_dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRstAck ,
	output [2:0]    w_dp_Link_n7_astResp_to_Link_n7_asiResp_WrCnt             ,
	// ctrl
	output [57:0] ctrl_dp_Link_n03_asi_to_Link_n03_ast_Data                            ,
	input  [2:0]  ctrl_dp_Link_n03_asi_to_Link_n03_ast_RdCnt                           ,
	input  [2:0]  ctrl_dp_Link_n03_asi_to_Link_n03_ast_RdPtr                           ,
	output        ctrl_dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRstAck               ,
	input         ctrl_dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]  ctrl_dp_Link_n03_asi_to_Link_n03_ast_WrCnt                           ,
	input  [57:0] ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_Data              ,
	output [2:0]  ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdCnt             ,
	output [2:0]  ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdPtr             ,
	input         ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRstAck ,
	output        ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]  ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_WrCnt             ,
	output [57:0] ctrl_dp_Link_n47_asi_to_Link_n47_ast_Data                            ,
	input  [2:0]  ctrl_dp_Link_n47_asi_to_Link_n47_ast_RdCnt                           ,
	input  [2:0]  ctrl_dp_Link_n47_asi_to_Link_n47_ast_RdPtr                           ,
	output        ctrl_dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRstAck               ,
	input         ctrl_dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]  ctrl_dp_Link_n47_asi_to_Link_n47_ast_WrCnt                           ,
	input  [57:0] ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_Data              ,
	output [2:0]  ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdCnt             ,
	output [2:0]  ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdPtr             ,
	input         ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRstAck ,
	output        ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]  ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_WrCnt             ,



	// SHM
	// read
	output [27:0]   shm0_r_Ar_Addr                      ,
	output [1:0]    shm0_r_Ar_Burst                     ,
	output [3:0]    shm0_r_Ar_Cache                     ,
	output [4:0]    shm0_r_Ar_Id                        ,
	output [4:0]    shm0_r_Ar_Len                       ,
	output          shm0_r_Ar_Lock                      ,
	output [2:0]    shm0_r_Ar_Prot                      ,
	input           shm0_r_Ar_Ready                     ,
	output [2:0]    shm0_r_Ar_Size                      ,
	output [3:0]    shm0_r_Ar_User                      ,
	output          shm0_r_Ar_Valid                     ,
	input  [1023:0] shm0_r_R_Data                       ,
	input  [4:0]    shm0_r_R_Id                         ,
	input           shm0_r_R_Last                       ,
	output          shm0_r_R_Ready                      ,
	input  [1:0]    shm0_r_R_Resp                       ,
	input  [3:0]    shm0_r_R_User                       ,
	input           shm0_r_R_Valid                      ,
	output [27:0]   shm1_r_Ar_Addr                      ,
	output [1:0]    shm1_r_Ar_Burst                     ,
	output [3:0]    shm1_r_Ar_Cache                     ,
	output [4:0]    shm1_r_Ar_Id                        ,
	output [4:0]    shm1_r_Ar_Len                       ,
	output          shm1_r_Ar_Lock                      ,
	output [2:0]    shm1_r_Ar_Prot                      ,
	input           shm1_r_Ar_Ready                     ,
	output [2:0]    shm1_r_Ar_Size                      ,
	output [3:0]    shm1_r_Ar_User                      ,
	output          shm1_r_Ar_Valid                     ,
	input  [1023:0] shm1_r_R_Data                       ,
	input  [4:0]    shm1_r_R_Id                         ,
	input           shm1_r_R_Last                       ,
	output          shm1_r_R_Ready                      ,
	input  [1:0]    shm1_r_R_Resp                       ,
	input  [3:0]    shm1_r_R_User                       ,
	input           shm1_r_R_Valid                      ,
	output [27:0]   shm2_r_Ar_Addr                      ,
	output [1:0]    shm2_r_Ar_Burst                     ,
	output [3:0]    shm2_r_Ar_Cache                     ,
	output [4:0]    shm2_r_Ar_Id                        ,
	output [4:0]    shm2_r_Ar_Len                       ,
	output          shm2_r_Ar_Lock                      ,
	output [2:0]    shm2_r_Ar_Prot                      ,
	input           shm2_r_Ar_Ready                     ,
	output [2:0]    shm2_r_Ar_Size                      ,
	output [3:0]    shm2_r_Ar_User                      ,
	output          shm2_r_Ar_Valid                     ,
	input  [1023:0] shm2_r_R_Data                       ,
	input  [4:0]    shm2_r_R_Id                         ,
	input           shm2_r_R_Last                       ,
	output          shm2_r_R_Ready                      ,
	input  [1:0]    shm2_r_R_Resp                       ,
	input  [3:0]    shm2_r_R_User                       ,
	input           shm2_r_R_Valid                      ,
	output [27:0]   shm3_r_Ar_Addr                      ,
	output [1:0]    shm3_r_Ar_Burst                     ,
	output [3:0]    shm3_r_Ar_Cache                     ,
	output [4:0]    shm3_r_Ar_Id                        ,
	output [4:0]    shm3_r_Ar_Len                       ,
	output          shm3_r_Ar_Lock                      ,
	output [2:0]    shm3_r_Ar_Prot                      ,
	input           shm3_r_Ar_Ready                     ,
	output [2:0]    shm3_r_Ar_Size                      ,
	output [3:0]    shm3_r_Ar_User                      ,
	output          shm3_r_Ar_Valid                     ,
	input  [1023:0] shm3_r_R_Data                       ,
	input  [4:0]    shm3_r_R_Id                         ,
	input           shm3_r_R_Last                       ,
	output          shm3_r_R_Ready                      ,
	input  [1:0]    shm3_r_R_Resp                       ,
	input  [3:0]    shm3_r_R_User                       ,
	input           shm3_r_R_Valid                      ,

	// write
	output [27:0]   shm0_w_Aw_Addr                      ,
	output [1:0]    shm0_w_Aw_Burst                     ,
	output [3:0]    shm0_w_Aw_Cache                     ,
	output [4:0]    shm0_w_Aw_Id                        ,
	output [4:0]    shm0_w_Aw_Len                       ,
	output          shm0_w_Aw_Lock                      ,
	output [2:0]    shm0_w_Aw_Prot                      ,
	input           shm0_w_Aw_Ready                     ,
	output [2:0]    shm0_w_Aw_Size                      ,
	output [3:0]    shm0_w_Aw_User                      ,
	output          shm0_w_Aw_Valid                     ,
	input  [4:0]    shm0_w_B_Id                         ,
	output          shm0_w_B_Ready                      ,
	input  [1:0]    shm0_w_B_Resp                       ,
	input  [3:0]    shm0_w_B_User                       ,
	input           shm0_w_B_Valid                      ,
	output [1023:0] shm0_w_W_Data                       ,
	output          shm0_w_W_Last                       ,
	input           shm0_w_W_Ready                      ,
	output [127:0]  shm0_w_W_Strb                       ,
	output          shm0_w_W_Valid                      ,
	output [27:0]   shm1_w_Aw_Addr                      ,
	output [1:0]    shm1_w_Aw_Burst                     ,
	output [3:0]    shm1_w_Aw_Cache                     ,
	output [4:0]    shm1_w_Aw_Id                        ,
	output [4:0]    shm1_w_Aw_Len                       ,
	output          shm1_w_Aw_Lock                      ,
	output [2:0]    shm1_w_Aw_Prot                      ,
	input           shm1_w_Aw_Ready                     ,
	output [2:0]    shm1_w_Aw_Size                      ,
	output [3:0]    shm1_w_Aw_User                      ,
	output          shm1_w_Aw_Valid                     ,
	input  [4:0]    shm1_w_B_Id                         ,
	output          shm1_w_B_Ready                      ,
	input  [1:0]    shm1_w_B_Resp                       ,
	input  [3:0]    shm1_w_B_User                       ,
	input           shm1_w_B_Valid                      ,
	output [1023:0] shm1_w_W_Data                       ,
	output          shm1_w_W_Last                       ,
	input           shm1_w_W_Ready                      ,
	output [127:0]  shm1_w_W_Strb                       ,
	output          shm1_w_W_Valid                      ,
	output [27:0]   shm2_w_Aw_Addr                      ,
	output [1:0]    shm2_w_Aw_Burst                     ,
	output [3:0]    shm2_w_Aw_Cache                     ,
	output [4:0]    shm2_w_Aw_Id                        ,
	output [4:0]    shm2_w_Aw_Len                       ,
	output          shm2_w_Aw_Lock                      ,
	output [2:0]    shm2_w_Aw_Prot                      ,
	input           shm2_w_Aw_Ready                     ,
	output [2:0]    shm2_w_Aw_Size                      ,
	output [3:0]    shm2_w_Aw_User                      ,
	output          shm2_w_Aw_Valid                     ,
	input  [4:0]    shm2_w_B_Id                         ,
	output          shm2_w_B_Ready                      ,
	input  [1:0]    shm2_w_B_Resp                       ,
	input  [3:0]    shm2_w_B_User                       ,
	input           shm2_w_B_Valid                      ,
	output [1023:0] shm2_w_W_Data                       ,
	output          shm2_w_W_Last                       ,
	input           shm2_w_W_Ready                      ,
	output [127:0]  shm2_w_W_Strb                       ,
	output          shm2_w_W_Valid                      ,
	output [27:0]   shm3_w_Aw_Addr                      ,
	output [1:0]    shm3_w_Aw_Burst                     ,
	output [3:0]    shm3_w_Aw_Cache                     ,
	output [4:0]    shm3_w_Aw_Id                        ,
	output [4:0]    shm3_w_Aw_Len                       ,
	output          shm3_w_Aw_Lock                      ,
	output [2:0]    shm3_w_Aw_Prot                      ,
	input           shm3_w_Aw_Ready                     ,
	output [2:0]    shm3_w_Aw_Size                      ,
	output [3:0]    shm3_w_Aw_User                      ,
	output          shm3_w_Aw_Valid                     ,
	input  [4:0]    shm3_w_B_Id                         ,
	output          shm3_w_B_Ready                      ,
	input  [1:0]    shm3_w_B_Resp                       ,
	input  [3:0]    shm3_w_B_User                       ,
	input           shm3_w_B_Valid                      ,
	output [1023:0] shm3_w_W_Data                       ,
	output          shm3_w_W_Last                       ,
	input           shm3_w_W_Ready                      ,
	output [127:0]  shm3_w_W_Strb                       ,
	output          shm3_w_W_Valid                      ,
	// ctrl
	output [23:0] shm0_ctrl_Ar_Addr                                               ,
	output [1:0]  shm0_ctrl_Ar_Burst                                              ,
	output [3:0]  shm0_ctrl_Ar_Cache                                              ,
	output [1:0]  shm0_ctrl_Ar_Id                                                 ,
	output [1:0]  shm0_ctrl_Ar_Len                                                ,
	output        shm0_ctrl_Ar_Lock                                               ,
	output [2:0]  shm0_ctrl_Ar_Prot                                               ,
	input         shm0_ctrl_Ar_Ready                                              ,
	output [2:0]  shm0_ctrl_Ar_Size                                               ,
	output        shm0_ctrl_Ar_Valid                                              ,
	output [23:0] shm0_ctrl_Aw_Addr                                               ,
	output [1:0]  shm0_ctrl_Aw_Burst                                              ,
	output [3:0]  shm0_ctrl_Aw_Cache                                              ,
	output [1:0]  shm0_ctrl_Aw_Id                                                 ,
	output [1:0]  shm0_ctrl_Aw_Len                                                ,
	output        shm0_ctrl_Aw_Lock                                               ,
	output [2:0]  shm0_ctrl_Aw_Prot                                               ,
	input         shm0_ctrl_Aw_Ready                                              ,
	output [2:0]  shm0_ctrl_Aw_Size                                               ,
	output        shm0_ctrl_Aw_Valid                                              ,
	input  [1:0]  shm0_ctrl_B_Id                                                  ,
	output        shm0_ctrl_B_Ready                                               ,
	input  [1:0]  shm0_ctrl_B_Resp                                                ,
	input         shm0_ctrl_B_Valid                                               ,
	input  [31:0] shm0_ctrl_R_Data                                                ,
	input  [1:0]  shm0_ctrl_R_Id                                                  ,
	input         shm0_ctrl_R_Last                                                ,
	output        shm0_ctrl_R_Ready                                               ,
	input  [1:0]  shm0_ctrl_R_Resp                                                ,
	input         shm0_ctrl_R_Valid                                               ,
	output [31:0] shm0_ctrl_W_Data                                                ,
	output        shm0_ctrl_W_Last                                                ,
	input         shm0_ctrl_W_Ready                                               ,
	output [3:0]  shm0_ctrl_W_Strb                                                ,
	output        shm0_ctrl_W_Valid                                               ,
	output [23:0] shm1_ctrl_Ar_Addr                                               ,
	output [1:0]  shm1_ctrl_Ar_Burst                                              ,
	output [3:0]  shm1_ctrl_Ar_Cache                                              ,
	output [1:0]  shm1_ctrl_Ar_Id                                                 ,
	output [1:0]  shm1_ctrl_Ar_Len                                                ,
	output        shm1_ctrl_Ar_Lock                                               ,
	output [2:0]  shm1_ctrl_Ar_Prot                                               ,
	input         shm1_ctrl_Ar_Ready                                              ,
	output [2:0]  shm1_ctrl_Ar_Size                                               ,
	output        shm1_ctrl_Ar_Valid                                              ,
	output [23:0] shm1_ctrl_Aw_Addr                                               ,
	output [1:0]  shm1_ctrl_Aw_Burst                                              ,
	output [3:0]  shm1_ctrl_Aw_Cache                                              ,
	output [1:0]  shm1_ctrl_Aw_Id                                                 ,
	output [1:0]  shm1_ctrl_Aw_Len                                                ,
	output        shm1_ctrl_Aw_Lock                                               ,
	output [2:0]  shm1_ctrl_Aw_Prot                                               ,
	input         shm1_ctrl_Aw_Ready                                              ,
	output [2:0]  shm1_ctrl_Aw_Size                                               ,
	output        shm1_ctrl_Aw_Valid                                              ,
	input  [1:0]  shm1_ctrl_B_Id                                                  ,
	output        shm1_ctrl_B_Ready                                               ,
	input  [1:0]  shm1_ctrl_B_Resp                                                ,
	input         shm1_ctrl_B_Valid                                               ,
	input  [31:0] shm1_ctrl_R_Data                                                ,
	input  [1:0]  shm1_ctrl_R_Id                                                  ,
	input         shm1_ctrl_R_Last                                                ,
	output        shm1_ctrl_R_Ready                                               ,
	input  [1:0]  shm1_ctrl_R_Resp                                                ,
	input         shm1_ctrl_R_Valid                                               ,
	output [31:0] shm1_ctrl_W_Data                                                ,
	output        shm1_ctrl_W_Last                                                ,
	input         shm1_ctrl_W_Ready                                               ,
	output [3:0]  shm1_ctrl_W_Strb                                                ,
	output        shm1_ctrl_W_Valid                                               ,
	output [23:0] shm2_ctrl_Ar_Addr                                               ,
	output [1:0]  shm2_ctrl_Ar_Burst                                              ,
	output [3:0]  shm2_ctrl_Ar_Cache                                              ,
	output [1:0]  shm2_ctrl_Ar_Id                                                 ,
	output [1:0]  shm2_ctrl_Ar_Len                                                ,
	output        shm2_ctrl_Ar_Lock                                               ,
	output [2:0]  shm2_ctrl_Ar_Prot                                               ,
	input         shm2_ctrl_Ar_Ready                                              ,
	output [2:0]  shm2_ctrl_Ar_Size                                               ,
	output        shm2_ctrl_Ar_Valid                                              ,
	output [23:0] shm2_ctrl_Aw_Addr                                               ,
	output [1:0]  shm2_ctrl_Aw_Burst                                              ,
	output [3:0]  shm2_ctrl_Aw_Cache                                              ,
	output [1:0]  shm2_ctrl_Aw_Id                                                 ,
	output [1:0]  shm2_ctrl_Aw_Len                                                ,
	output        shm2_ctrl_Aw_Lock                                               ,
	output [2:0]  shm2_ctrl_Aw_Prot                                               ,
	input         shm2_ctrl_Aw_Ready                                              ,
	output [2:0]  shm2_ctrl_Aw_Size                                               ,
	output        shm2_ctrl_Aw_Valid                                              ,
	input  [1:0]  shm2_ctrl_B_Id                                                  ,
	output        shm2_ctrl_B_Ready                                               ,
	input  [1:0]  shm2_ctrl_B_Resp                                                ,
	input         shm2_ctrl_B_Valid                                               ,
	input  [31:0] shm2_ctrl_R_Data                                                ,
	input  [1:0]  shm2_ctrl_R_Id                                                  ,
	input         shm2_ctrl_R_Last                                                ,
	output        shm2_ctrl_R_Ready                                               ,
	input  [1:0]  shm2_ctrl_R_Resp                                                ,
	input         shm2_ctrl_R_Valid                                               ,
	output [31:0] shm2_ctrl_W_Data                                                ,
	output        shm2_ctrl_W_Last                                                ,
	input         shm2_ctrl_W_Ready                                               ,
	output [3:0]  shm2_ctrl_W_Strb                                                ,
	output        shm2_ctrl_W_Valid                                               ,
	output [23:0] shm3_ctrl_Ar_Addr                                               ,
	output [1:0]  shm3_ctrl_Ar_Burst                                              ,
	output [3:0]  shm3_ctrl_Ar_Cache                                              ,
	output [1:0]  shm3_ctrl_Ar_Id                                                 ,
	output [1:0]  shm3_ctrl_Ar_Len                                                ,
	output        shm3_ctrl_Ar_Lock                                               ,
	output [2:0]  shm3_ctrl_Ar_Prot                                               ,
	input         shm3_ctrl_Ar_Ready                                              ,
	output [2:0]  shm3_ctrl_Ar_Size                                               ,
	output        shm3_ctrl_Ar_Valid                                              ,
	output [23:0] shm3_ctrl_Aw_Addr                                               ,
	output [1:0]  shm3_ctrl_Aw_Burst                                              ,
	output [3:0]  shm3_ctrl_Aw_Cache                                              ,
	output [1:0]  shm3_ctrl_Aw_Id                                                 ,
	output [1:0]  shm3_ctrl_Aw_Len                                                ,
	output        shm3_ctrl_Aw_Lock                                               ,
	output [2:0]  shm3_ctrl_Aw_Prot                                               ,
	input         shm3_ctrl_Aw_Ready                                              ,
	output [2:0]  shm3_ctrl_Aw_Size                                               ,
	output        shm3_ctrl_Aw_Valid                                              ,
	input  [1:0]  shm3_ctrl_B_Id                                                  ,
	output        shm3_ctrl_B_Ready                                               ,
	input  [1:0]  shm3_ctrl_B_Resp                                                ,
	input         shm3_ctrl_B_Valid                                               ,
	input  [31:0] shm3_ctrl_R_Data                                                ,
	input  [1:0]  shm3_ctrl_R_Id                                                  ,
	input         shm3_ctrl_R_Last                                                ,
	output        shm3_ctrl_R_Ready                                               ,
	input  [1:0]  shm3_ctrl_R_Resp                                                ,
	input         shm3_ctrl_R_Valid                                               ,
	output [31:0] shm3_ctrl_W_Data                                                ,
	output        shm3_ctrl_W_Last                                                ,
	input         shm3_ctrl_W_Ready                                               ,
	output [3:0]  shm3_ctrl_W_Strb                                                ,
	output        shm3_ctrl_W_Valid                                               ,




    // peri
	output [57:0] ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_Data              ,
	input  [2:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt             ,
	input  [1:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr             ,
	output        ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst    ,
	input         ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck ,
	input         ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst    ,
	output        ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck ,
	output [2:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt             ,

	input  [57:0] ctrl_dp_dbg_master_I_to_Link1_Data                             ,
	output [2:0]  ctrl_dp_dbg_master_I_to_Link1_RdCnt                            ,
	output [1:0]  ctrl_dp_dbg_master_I_to_Link1_RdPtr                            ,
	input         ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst                   ,
	output        ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck                ,
	output        ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst                   ,
	input         ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck                ,
	input  [2:0]  ctrl_dp_dbg_master_I_to_Link1_WrCnt                            ,


	// cbus_ls_west

	output [57:0] ctrl_dp_Link2_to_Switch_west_Data                                    ,
	input  [2:0]  ctrl_dp_Link2_to_Switch_west_RdCnt                                   ,
	input  [1:0]  ctrl_dp_Link2_to_Switch_west_RdPtr                                   ,
	output        ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRst                          ,
	input         ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck                       ,
	input         ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRst                          ,
	output        ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck                       ,
	output [2:0]  ctrl_dp_Link2_to_Switch_west_WrCnt                                   ,

	input  [57:0] ctrl_dp_Switch_westResp001_to_Link13Resp001_Data                     ,
	output [2:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_RdCnt                    ,
	output [1:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_RdPtr                    ,
	input         ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst           ,
	output        ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck        ,
	output        ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst           ,
	input         ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck        ,
	input  [2:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_WrCnt                    ,

	// cbus_ls_east

	output [57:0] ctrl_dp_Link13_to_Link37_Data                                  ,
	input  [2:0]  ctrl_dp_Link13_to_Link37_RdCnt                                 ,
	input  [1:0]  ctrl_dp_Link13_to_Link37_RdPtr                                 ,
	output        ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRst                        ,
	input         ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRstAck                     ,
	input         ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRst                        ,
	output        ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRstAck                     ,
	output [2:0]  ctrl_dp_Link13_to_Link37_WrCnt                                 ,
	input  [57:0] ctrl_dp_Link35_to_Switch_ctrl_iResp001_Data                    ,
	output [2:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdCnt                   ,
	output [1:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdPtr                   ,
	input         ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRst          ,
	output        ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRstAck       ,
	output        ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRst          ,
	input         ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRstAck       ,
	input  [2:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_WrCnt                   ,

	input  [57:0] ctrl_dp_Link36_to_Link5_Data                                   ,
	output [2:0]  ctrl_dp_Link36_to_Link5_RdCnt                                  ,
	output [1:0]  ctrl_dp_Link36_to_Link5_RdPtr                                  ,
	input         ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRst                         ,
	output        ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRstAck                      ,
	output        ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRst                         ,
	input         ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRstAck                      ,
	input  [2:0]  ctrl_dp_Link36_to_Link5_WrCnt                                  ,

	output [57:0] ctrl_dp_Switch_ctrl_i_to_Link17_Data                           ,
	input  [2:0]  ctrl_dp_Switch_ctrl_i_to_Link17_RdCnt                          ,
	input  [1:0]  ctrl_dp_Switch_ctrl_i_to_Link17_RdPtr                          ,
	output        ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRst                 ,
	input         ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRstAck              ,
	input         ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRst                 ,
	output        ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRstAck              ,
	output [2:0]  ctrl_dp_Switch_ctrl_i_to_Link17_WrCnt                          ,


	// cbus_ls_south


	output [57:0] ctrl_dp_Link7_to_Link_2c0_3_Data                               ,
	input  [2:0]  ctrl_dp_Link7_to_Link_2c0_3_RdCnt                              ,
	input  [2:0]  ctrl_dp_Link7_to_Link_2c0_3_RdPtr                              ,
	output        ctrl_dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst                     ,
	input         ctrl_dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck                  ,
	input         ctrl_dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst                     ,
	output        ctrl_dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck                  ,
	output [2:0]  ctrl_dp_Link7_to_Link_2c0_3_WrCnt                              ,

	input  [57:0] ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_Data                 ,
	output [2:0]  ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt                ,
	output [2:0]  ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr                ,
	input         ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst       ,
	output        ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck    ,
	output        ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst       ,
	input         ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck    ,
	input  [2:0]  ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt                


);



//--------------------------------------------------------------------------------------
//
//
//
// wire
//
//
//
//--------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------
// read wire

	wire [703:0]  r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data           ;
	wire          r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head           ;
	wire          r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy            ;
	wire          r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail           ;
	wire          r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld            ;
	wire [126:0]  r_dp_Link_c01_m01_e_to_Switch_west_Data                         ;
	wire          r_dp_Link_c01_m01_e_to_Switch_west_Head                         ;
	wire          r_dp_Link_c01_m01_e_to_Switch_west_Rdy                          ;
	wire          r_dp_Link_c01_m01_e_to_Switch_west_Tail                         ;
	wire          r_dp_Link_c01_m01_e_to_Switch_west_Vld                          ;
	//wire [128:0]  r_dp_Link_c0_asi_to_Link_c0_ast_Data                            ;
	//wire [2:0]    r_dp_Link_c0_asi_to_Link_c0_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_c0_asi_to_Link_c0_ast_RdPtr                           ;
	//wire          r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_c0_asi_to_Link_c0_ast_WrCnt                           ;
	//wire [705:0]  r_dp_Link_c0_astResp_to_Link_c0_asiResp_Data                    ;
	//wire [2:0]    r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt                   ;
	//wire [2:0]    r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr                   ;
	//wire          r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst          ;
	//wire          r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst          ;
	//wire          r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    r_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt                   ;
	wire [703:0]  r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Data                      ;
	wire          r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Head                      ;
	wire          r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy                       ;
	wire          r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail                      ;
	wire          r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld                       ;
	wire [126:0]  r_dp_Link_c0s_b_to_Link_c0e_a_Data                              ;
	wire          r_dp_Link_c0s_b_to_Link_c0e_a_Head                              ;
	wire          r_dp_Link_c0s_b_to_Link_c0e_a_Rdy                               ;
	wire          r_dp_Link_c0s_b_to_Link_c0e_a_Tail                              ;
	wire          r_dp_Link_c0s_b_to_Link_c0e_a_Vld                               ;
	//wire [128:0]  r_dp_Link_c1_asi_to_Link_c1_ast_Data                            ;
	//wire [2:0]    r_dp_Link_c1_asi_to_Link_c1_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_c1_asi_to_Link_c1_ast_RdPtr                           ;
	//wire          r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_c1_asi_to_Link_c1_ast_WrCnt                           ;
	//wire [273:0]  r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr             ;
	//wire          r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_c1t_asi_to_Link_c1t_ast_Data                          ;
	//wire [2:0]    r_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt                         ;
	//wire [2:0]    r_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr                         ;
	//wire          r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst                ;
	//wire          r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck             ;
	//wire          r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst                ;
	//wire          r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck             ;
	//wire [2:0]    r_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt                         ;
	//wire [273:0]  r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data                  ;
	//wire [2:0]    r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt                 ;
	//wire [2:0]    r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr                 ;
	//wire          r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst        ;
	//wire          r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck     ;
	//wire          r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst        ;
	//wire          r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck     ;
	//wire [2:0]    r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt                 ;
	//wire [128:0]  r_dp_Link_m0_asi_to_Link_m0_ast_Data                            ;
	//wire [2:0]    r_dp_Link_m0_asi_to_Link_m0_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_m0_asi_to_Link_m0_ast_RdPtr                           ;
	//wire          r_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_m0_asi_to_Link_m0_ast_WrCnt                           ;
	//wire [705:0]  r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdPtr             ;
	//wire          r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_m1_asi_to_Link_m1_ast_Data                            ;
	//wire [2:0]    r_dp_Link_m1_asi_to_Link_m1_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_m1_asi_to_Link_m1_ast_RdPtr                           ;
	//wire          r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_m1_asi_to_Link_m1_ast_WrCnt                           ;
	//wire [705:0]  r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr             ;
	//wire          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_m2_asi_to_Link_m2_ast_Data                            ;
	//wire [2:0]    r_dp_Link_m2_asi_to_Link_m2_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_m2_asi_to_Link_m2_ast_RdPtr                           ;
	//wire          r_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_m2_asi_to_Link_m2_ast_WrCnt                           ;
	//wire [705:0]  r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr             ;
	//wire          r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_m3_asi_to_Link_m3_ast_Data                            ;
	//wire [2:0]    r_dp_Link_m3_asi_to_Link_m3_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_m3_asi_to_Link_m3_ast_RdPtr                           ;
	//wire          r_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_m3_asi_to_Link_m3_ast_WrCnt                           ;
	//wire [705:0]  r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr             ;
	//wire          r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt             ;
	wire [126:0]  r_dp_Link_n03_m01_c_to_Switch_west_Data                         ;
	wire          r_dp_Link_n03_m01_c_to_Switch_west_Head                         ;
	wire          r_dp_Link_n03_m01_c_to_Switch_west_Rdy                          ;
	wire          r_dp_Link_n03_m01_c_to_Switch_west_Tail                         ;
	wire          r_dp_Link_n03_m01_c_to_Switch_west_Vld                          ;
	wire [126:0]  r_dp_Link_n03_m23_c_to_Switch_east_Data                         ;
	wire          r_dp_Link_n03_m23_c_to_Switch_east_Head                         ;
	wire          r_dp_Link_n03_m23_c_to_Switch_east_Rdy                          ;
	wire          r_dp_Link_n03_m23_c_to_Switch_east_Tail                         ;
	wire          r_dp_Link_n03_m23_c_to_Switch_east_Vld                          ;
	//wire [128:0]  r_dp_Link_n0_asi_to_Link_n0_ast_Data                            ;
	//wire [2:0]    r_dp_Link_n0_asi_to_Link_n0_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_n0_asi_to_Link_n0_ast_RdPtr                           ;
	//wire          r_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_n0_asi_to_Link_n0_ast_WrCnt                           ;
	//wire [1281:0] r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdPtr             ;
	//wire          r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_n1_asi_to_Link_n1_ast_Data                            ;
	//wire [2:0]    r_dp_Link_n1_asi_to_Link_n1_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_n1_asi_to_Link_n1_ast_RdPtr                           ;
	//wire          r_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_n1_asi_to_Link_n1_ast_WrCnt                           ;
	//wire [1281:0] r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdPtr             ;
	//wire          r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_n2_asi_to_Link_n2_ast_Data                            ;
	//wire [2:0]    r_dp_Link_n2_asi_to_Link_n2_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_n2_asi_to_Link_n2_ast_RdPtr                           ;
	//wire          r_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_n2_asi_to_Link_n2_ast_WrCnt                           ;
	//wire [1281:0] r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdPtr             ;
	//wire          r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_n3_asi_to_Link_n3_ast_Data                            ;
	//wire [2:0]    r_dp_Link_n3_asi_to_Link_n3_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_n3_asi_to_Link_n3_ast_RdPtr                           ;
	//wire          r_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_n3_asi_to_Link_n3_ast_WrCnt                           ;
	//wire [1281:0] r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdPtr             ;
	//wire          r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_WrCnt             ;
	wire [126:0]  r_dp_Link_n47_m01_c_to_Switch_west_Data                         ;
	wire          r_dp_Link_n47_m01_c_to_Switch_west_Head                         ;
	wire          r_dp_Link_n47_m01_c_to_Switch_west_Rdy                          ;
	wire          r_dp_Link_n47_m01_c_to_Switch_west_Tail                         ;
	wire          r_dp_Link_n47_m01_c_to_Switch_west_Vld                          ;
	wire [126:0]  r_dp_Link_n47_m23_c_to_Switch_east_Data                         ;
	wire          r_dp_Link_n47_m23_c_to_Switch_east_Head                         ;
	wire          r_dp_Link_n47_m23_c_to_Switch_east_Rdy                          ;
	wire          r_dp_Link_n47_m23_c_to_Switch_east_Tail                         ;
	wire          r_dp_Link_n47_m23_c_to_Switch_east_Vld                          ;
	//wire [128:0]  r_dp_Link_n4_asi_to_Link_n4_ast_Data                            ;
	//wire [2:0]    r_dp_Link_n4_asi_to_Link_n4_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_n4_asi_to_Link_n4_ast_RdPtr                           ;
	//wire          r_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_n4_asi_to_Link_n4_ast_WrCnt                           ;
	//wire [1281:0] r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdPtr             ;
	//wire          r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_n5_asi_to_Link_n5_ast_Data                            ;
	//wire [2:0]    r_dp_Link_n5_asi_to_Link_n5_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_n5_asi_to_Link_n5_ast_RdPtr                           ;
	//wire          r_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_n5_asi_to_Link_n5_ast_WrCnt                           ;
	//wire [1281:0] r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdPtr             ;
	//wire          r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_n6_asi_to_Link_n6_ast_Data                            ;
	//wire [2:0]    r_dp_Link_n6_asi_to_Link_n6_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_n6_asi_to_Link_n6_ast_RdPtr                           ;
	//wire          r_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_n6_asi_to_Link_n6_ast_WrCnt                           ;
	//wire [1281:0] r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdPtr             ;
	//wire          r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_WrCnt             ;
	//wire [128:0]  r_dp_Link_n7_asi_to_Link_n7_ast_Data                            ;
	//wire [2:0]    r_dp_Link_n7_asi_to_Link_n7_ast_RdCnt                           ;
	//wire [2:0]    r_dp_Link_n7_asi_to_Link_n7_ast_RdPtr                           ;
	//wire          r_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck               ;
	//wire          r_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst                  ;
	//wire          r_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    r_dp_Link_n7_asi_to_Link_n7_ast_WrCnt                           ;
	//wire [1281:0] r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_Data              ;
	//wire [2:0]    r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdCnt             ;
	//wire [2:0]    r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdPtr             ;
	//wire          r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRst    ;
	//wire          r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_WrCnt             ;
	wire [126:0]  r_dp_Link_n8_m01_c_to_Switch_west_Data                          ;
	wire          r_dp_Link_n8_m01_c_to_Switch_west_Head                          ;
	wire          r_dp_Link_n8_m01_c_to_Switch_west_Rdy                           ;
	wire          r_dp_Link_n8_m01_c_to_Switch_west_Tail                          ;
	wire          r_dp_Link_n8_m01_c_to_Switch_west_Vld                           ;
	wire [126:0]  r_dp_Link_n8_m23_c_to_Switch_east_Data                          ;
	wire          r_dp_Link_n8_m23_c_to_Switch_east_Head                          ;
	wire          r_dp_Link_n8_m23_c_to_Switch_east_Rdy                           ;
	wire          r_dp_Link_n8_m23_c_to_Switch_east_Tail                          ;
	wire          r_dp_Link_n8_m23_c_to_Switch_east_Vld                           ;
	wire [703:0]  r_dp_Link_sc01Resp_to_Switch_eastResp001_Data                   ;
	wire          r_dp_Link_sc01Resp_to_Switch_eastResp001_Head                   ;
	wire          r_dp_Link_sc01Resp_to_Switch_eastResp001_Rdy                    ;
	wire          r_dp_Link_sc01Resp_to_Switch_eastResp001_Tail                   ;
	wire          r_dp_Link_sc01Resp_to_Switch_eastResp001_Vld                    ;
	wire [1279:0] r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data           ;
	wire          r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head           ;
	wire          r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy            ;
	wire          r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail           ;
	wire          r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld            ;
	wire [1279:0] r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data           ;
	wire          r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head           ;
	wire          r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy            ;
	wire          r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail           ;
	wire          r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld            ;
	wire [1279:0] r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data            ;
	wire          r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head            ;
	wire          r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy             ;
	wire          r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail            ;
	wire          r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld             ;
	wire [126:0]  r_dp_Switch_east_to_Link_c01_m01_a_Data                         ;
	wire          r_dp_Switch_east_to_Link_c01_m01_a_Head                         ;
	wire          r_dp_Switch_east_to_Link_c01_m01_a_Rdy                          ;
	wire          r_dp_Switch_east_to_Link_c01_m01_a_Tail                         ;
	wire          r_dp_Switch_east_to_Link_c01_m01_a_Vld                          ;
	wire [126:0]  r_dp_Switch_east_to_Link_sc01_Data                              ;
	wire          r_dp_Switch_east_to_Link_sc01_Head                              ;
	wire          r_dp_Switch_east_to_Link_sc01_Rdy                               ;
	wire          r_dp_Switch_east_to_Link_sc01_Tail                              ;
	wire          r_dp_Switch_east_to_Link_sc01_Vld                               ;
	wire [1279:0] r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data           ;
	wire          r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head           ;
	wire          r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy            ;
	wire          r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail           ;
	wire          r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld            ;
	wire [1279:0] r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data           ;
	wire          r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head           ;
	wire          r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy            ;
	wire          r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail           ;
	wire          r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld            ;
	wire [1279:0] r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data           ;
	wire          r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head           ;
	wire          r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy            ;
	wire          r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail           ;
	wire          r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld            ;
	wire [1279:0] r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data            ;
	wire          r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head            ;
	wire          r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy             ;
	wire          r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail            ;
	wire          r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld             ;



	wire   [27:0]   dbus2sub_dcluster0_port0_r_Ar_Addr                            ;
	wire   [1:0]    dbus2sub_dcluster0_port0_r_Ar_Burst                           ;
	wire   [3:0]    dbus2sub_dcluster0_port0_r_Ar_Cache                           ;
	wire   [4:0]    dbus2sub_dcluster0_port0_r_Ar_Id                              ;
	wire   [3:0]    dbus2sub_dcluster0_port0_r_Ar_Len                             ;
	wire            dbus2sub_dcluster0_port0_r_Ar_Lock                            ;
	wire   [2:0]    dbus2sub_dcluster0_port0_r_Ar_Prot                            ;
	wire            dbus2sub_dcluster0_port0_r_Ar_Ready                           ;
	wire   [2:0]    dbus2sub_dcluster0_port0_r_Ar_Size                            ;
	wire   [3:0]    dbus2sub_dcluster0_port0_r_Ar_User                            ;
	wire            dbus2sub_dcluster0_port0_r_Ar_Valid                           ;
	wire   [1023:0] dbus2sub_dcluster0_port0_r_R_Data                             ;
	wire   [4:0]    dbus2sub_dcluster0_port0_r_R_Id                               ;
	wire            dbus2sub_dcluster0_port0_r_R_Last                             ;
	wire            dbus2sub_dcluster0_port0_r_R_Ready                            ;
	wire   [1:0]    dbus2sub_dcluster0_port0_r_R_Resp                             ;
	wire   [3:0]    dbus2sub_dcluster0_port0_r_R_User                             ;
	wire            dbus2sub_dcluster0_port0_r_R_Valid                            ;
	wire   [27:0]   dbus2sub_dcluster0_port1_r_Ar_Addr                            ;
	wire   [1:0]    dbus2sub_dcluster0_port1_r_Ar_Burst                           ;
	wire   [3:0]    dbus2sub_dcluster0_port1_r_Ar_Cache                           ;
	wire   [4:0]    dbus2sub_dcluster0_port1_r_Ar_Id                              ;
	wire   [3:0]    dbus2sub_dcluster0_port1_r_Ar_Len                             ;
	wire            dbus2sub_dcluster0_port1_r_Ar_Lock                            ;
	wire   [2:0]    dbus2sub_dcluster0_port1_r_Ar_Prot                            ;
	wire            dbus2sub_dcluster0_port1_r_Ar_Ready                           ;
	wire   [2:0]    dbus2sub_dcluster0_port1_r_Ar_Size                            ;
	wire   [3:0]    dbus2sub_dcluster0_port1_r_Ar_User                            ;
	wire            dbus2sub_dcluster0_port1_r_Ar_Valid                           ;
	wire   [1023:0] dbus2sub_dcluster0_port1_r_R_Data                             ;
	wire   [4:0]    dbus2sub_dcluster0_port1_r_R_Id                               ;
	wire            dbus2sub_dcluster0_port1_r_R_Last                             ;
	wire            dbus2sub_dcluster0_port1_r_R_Ready                            ;
	wire   [1:0]    dbus2sub_dcluster0_port1_r_R_Resp                             ;
	wire   [3:0]    dbus2sub_dcluster0_port1_r_R_User                             ;
	wire            dbus2sub_dcluster0_port1_r_R_Valid                            ;
	wire   [27:0]   dbus2sub_dcluster0_port2_r_Ar_Addr                            ;
	wire   [1:0]    dbus2sub_dcluster0_port2_r_Ar_Burst                           ;
	wire   [3:0]    dbus2sub_dcluster0_port2_r_Ar_Cache                           ;
	wire   [4:0]    dbus2sub_dcluster0_port2_r_Ar_Id                              ;
	wire   [3:0]    dbus2sub_dcluster0_port2_r_Ar_Len                             ;
	wire            dbus2sub_dcluster0_port2_r_Ar_Lock                            ;
	wire   [2:0]    dbus2sub_dcluster0_port2_r_Ar_Prot                            ;
	wire            dbus2sub_dcluster0_port2_r_Ar_Ready                           ;
	wire   [2:0]    dbus2sub_dcluster0_port2_r_Ar_Size                            ;
	wire   [3:0]    dbus2sub_dcluster0_port2_r_Ar_User                            ;
	wire            dbus2sub_dcluster0_port2_r_Ar_Valid                           ;
	wire   [1023:0] dbus2sub_dcluster0_port2_r_R_Data                             ;
	wire   [4:0]    dbus2sub_dcluster0_port2_r_R_Id                               ;
	wire            dbus2sub_dcluster0_port2_r_R_Last                             ;
	wire            dbus2sub_dcluster0_port2_r_R_Ready                            ;
	wire   [1:0]    dbus2sub_dcluster0_port2_r_R_Resp                             ;
	wire   [3:0]    dbus2sub_dcluster0_port2_r_R_User                             ;
	wire            dbus2sub_dcluster0_port2_r_R_Valid                            ;
	wire   [27:0]   dbus2sub_dcluster0_port3_r_Ar_Addr                            ;
	wire   [1:0]    dbus2sub_dcluster0_port3_r_Ar_Burst                           ;
	wire   [3:0]    dbus2sub_dcluster0_port3_r_Ar_Cache                           ;
	wire   [4:0]    dbus2sub_dcluster0_port3_r_Ar_Id                              ;
	wire   [3:0]    dbus2sub_dcluster0_port3_r_Ar_Len                             ;
	wire            dbus2sub_dcluster0_port3_r_Ar_Lock                            ;
	wire   [2:0]    dbus2sub_dcluster0_port3_r_Ar_Prot                            ;
	wire            dbus2sub_dcluster0_port3_r_Ar_Ready                           ;
	wire   [2:0]    dbus2sub_dcluster0_port3_r_Ar_Size                            ;
	wire   [3:0]    dbus2sub_dcluster0_port3_r_Ar_User                            ;
	wire            dbus2sub_dcluster0_port3_r_Ar_Valid                           ;
	wire   [1023:0] dbus2sub_dcluster0_port3_r_R_Data                             ;
	wire   [4:0]    dbus2sub_dcluster0_port3_r_R_Id                               ;
	wire            dbus2sub_dcluster0_port3_r_R_Last                             ;
	wire            dbus2sub_dcluster0_port3_r_R_Ready                            ;
	wire   [1:0]    dbus2sub_dcluster0_port3_r_R_Resp                             ;
	wire   [3:0]    dbus2sub_dcluster0_port3_r_R_User                             ;
	wire            dbus2sub_dcluster0_port3_r_R_Valid                            ;
	wire   [27:0]   dbus2sub_dcluster1_port0_r_Ar_Addr                            ;
	wire   [1:0]    dbus2sub_dcluster1_port0_r_Ar_Burst                           ;
	wire   [3:0]    dbus2sub_dcluster1_port0_r_Ar_Cache                           ;
	wire   [4:0]    dbus2sub_dcluster1_port0_r_Ar_Id                              ;
	wire   [3:0]    dbus2sub_dcluster1_port0_r_Ar_Len                             ;
	wire            dbus2sub_dcluster1_port0_r_Ar_Lock                            ;
	wire   [2:0]    dbus2sub_dcluster1_port0_r_Ar_Prot                            ;
	wire            dbus2sub_dcluster1_port0_r_Ar_Ready                           ;
	wire   [2:0]    dbus2sub_dcluster1_port0_r_Ar_Size                            ;
	wire   [3:0]    dbus2sub_dcluster1_port0_r_Ar_User                            ;
	wire            dbus2sub_dcluster1_port0_r_Ar_Valid                           ;
	wire   [1023:0] dbus2sub_dcluster1_port0_r_R_Data                             ;
	wire   [4:0]    dbus2sub_dcluster1_port0_r_R_Id                               ;
	wire            dbus2sub_dcluster1_port0_r_R_Last                             ;
	wire            dbus2sub_dcluster1_port0_r_R_Ready                            ;
	wire   [1:0]    dbus2sub_dcluster1_port0_r_R_Resp                             ;
	wire   [3:0]    dbus2sub_dcluster1_port0_r_R_User                             ;
	wire            dbus2sub_dcluster1_port0_r_R_Valid                            ;
	wire   [27:0]   dbus2sub_dcluster1_port1_r_Ar_Addr                            ;
	wire   [1:0]    dbus2sub_dcluster1_port1_r_Ar_Burst                           ;
	wire   [3:0]    dbus2sub_dcluster1_port1_r_Ar_Cache                           ;
	wire   [4:0]    dbus2sub_dcluster1_port1_r_Ar_Id                              ;
	wire   [3:0]    dbus2sub_dcluster1_port1_r_Ar_Len                             ;
	wire            dbus2sub_dcluster1_port1_r_Ar_Lock                            ;
	wire   [2:0]    dbus2sub_dcluster1_port1_r_Ar_Prot                            ;
	wire            dbus2sub_dcluster1_port1_r_Ar_Ready                           ;
	wire   [2:0]    dbus2sub_dcluster1_port1_r_Ar_Size                            ;
	wire   [3:0]    dbus2sub_dcluster1_port1_r_Ar_User                            ;
	wire            dbus2sub_dcluster1_port1_r_Ar_Valid                           ;
	wire   [1023:0] dbus2sub_dcluster1_port1_r_R_Data                             ;
	wire   [4:0]    dbus2sub_dcluster1_port1_r_R_Id                               ;
	wire            dbus2sub_dcluster1_port1_r_R_Last                             ;
	wire            dbus2sub_dcluster1_port1_r_R_Ready                            ;
	wire   [1:0]    dbus2sub_dcluster1_port1_r_R_Resp                             ;
	wire   [3:0]    dbus2sub_dcluster1_port1_r_R_User                             ;
	wire            dbus2sub_dcluster1_port1_r_R_Valid                            ;
	wire   [27:0]   dbus2sub_dcluster1_port2_r_Ar_Addr                            ;
	wire   [1:0]    dbus2sub_dcluster1_port2_r_Ar_Burst                           ;
	wire   [3:0]    dbus2sub_dcluster1_port2_r_Ar_Cache                           ;
	wire   [4:0]    dbus2sub_dcluster1_port2_r_Ar_Id                              ;
	wire   [3:0]    dbus2sub_dcluster1_port2_r_Ar_Len                             ;
	wire            dbus2sub_dcluster1_port2_r_Ar_Lock                            ;
	wire   [2:0]    dbus2sub_dcluster1_port2_r_Ar_Prot                            ;
	wire            dbus2sub_dcluster1_port2_r_Ar_Ready                           ;
	wire   [2:0]    dbus2sub_dcluster1_port2_r_Ar_Size                            ;
	wire   [3:0]    dbus2sub_dcluster1_port2_r_Ar_User                            ;
	wire            dbus2sub_dcluster1_port2_r_Ar_Valid                           ;
	wire   [1023:0] dbus2sub_dcluster1_port2_r_R_Data                             ;
	wire   [4:0]    dbus2sub_dcluster1_port2_r_R_Id                               ;
	wire            dbus2sub_dcluster1_port2_r_R_Last                             ;
	wire            dbus2sub_dcluster1_port2_r_R_Ready                            ;
	wire   [1:0]    dbus2sub_dcluster1_port2_r_R_Resp                             ;
	wire   [3:0]    dbus2sub_dcluster1_port2_r_R_User                             ;
	wire            dbus2sub_dcluster1_port2_r_R_Valid                            ;
	wire   [27:0]   dbus2sub_dcluster1_port3_r_Ar_Addr                            ;
	wire   [1:0]    dbus2sub_dcluster1_port3_r_Ar_Burst                           ;
	wire   [3:0]    dbus2sub_dcluster1_port3_r_Ar_Cache                           ;
	wire   [4:0]    dbus2sub_dcluster1_port3_r_Ar_Id                              ;
	wire   [3:0]    dbus2sub_dcluster1_port3_r_Ar_Len                             ;
	wire            dbus2sub_dcluster1_port3_r_Ar_Lock                            ;
	wire   [2:0]    dbus2sub_dcluster1_port3_r_Ar_Prot                            ;
	wire            dbus2sub_dcluster1_port3_r_Ar_Ready                           ;
	wire   [2:0]    dbus2sub_dcluster1_port3_r_Ar_Size                            ;
	wire   [3:0]    dbus2sub_dcluster1_port3_r_Ar_User                            ;
	wire            dbus2sub_dcluster1_port3_r_Ar_Valid                           ;
	wire   [1023:0] dbus2sub_dcluster1_port3_r_R_Data                             ;
	wire   [4:0]    dbus2sub_dcluster1_port3_r_R_Id                               ;
	wire            dbus2sub_dcluster1_port3_r_R_Last                             ;
	wire            dbus2sub_dcluster1_port3_r_R_Ready                            ;
	wire   [1:0]    dbus2sub_dcluster1_port3_r_R_Resp                             ;
	wire   [3:0]    dbus2sub_dcluster1_port3_r_R_User                             ;
	wire            dbus2sub_dcluster1_port3_r_R_Valid                            ;
	wire   [27:0]   dbus2sub_ddma_r_Ar_Addr                                       ;
	wire   [1:0]    dbus2sub_ddma_r_Ar_Burst                                      ;
	wire   [3:0]    dbus2sub_ddma_r_Ar_Cache                                      ;
	wire   [4:0]    dbus2sub_ddma_r_Ar_Id                                         ;
	wire   [3:0]    dbus2sub_ddma_r_Ar_Len                                        ;
	wire            dbus2sub_ddma_r_Ar_Lock                                       ;
	wire   [2:0]    dbus2sub_ddma_r_Ar_Prot                                       ;
	wire            dbus2sub_ddma_r_Ar_Ready                                      ;
	wire   [2:0]    dbus2sub_ddma_r_Ar_Size                                       ;
	wire   [3:0]    dbus2sub_ddma_r_Ar_User                                       ;
	wire            dbus2sub_ddma_r_Ar_Valid                                      ;
	wire   [1023:0] dbus2sub_ddma_r_R_Data                                        ;
	wire   [4:0]    dbus2sub_ddma_r_R_Id                                          ;
	wire            dbus2sub_ddma_r_R_Last                                        ;
	wire            dbus2sub_ddma_r_R_Ready                                       ;
	wire   [1:0]    dbus2sub_ddma_r_R_Resp                                        ;
	wire   [3:0]    dbus2sub_ddma_r_R_User                                        ;
	wire            dbus2sub_ddma_r_R_Valid                                       ;
	wire   [27:0]   dbus2sub_pcie_cpu_r_Ar_Addr                                   ;
	wire   [1:0]    dbus2sub_pcie_cpu_r_Ar_Burst                                  ;
	wire   [3:0]    dbus2sub_pcie_cpu_r_Ar_Cache                                  ;
	wire   [3:0]    dbus2sub_pcie_cpu_r_Ar_Id                                     ;
	wire   [3:0]    dbus2sub_pcie_cpu_r_Ar_Len                                    ;
	wire            dbus2sub_pcie_cpu_r_Ar_Lock                                   ;
	wire   [2:0]    dbus2sub_pcie_cpu_r_Ar_Prot                                   ;
	wire            dbus2sub_pcie_cpu_r_Ar_Ready                                  ;
	wire   [2:0]    dbus2sub_pcie_cpu_r_Ar_Size                                   ;
	wire   [3:0]    dbus2sub_pcie_cpu_r_Ar_User                                   ;
	wire            dbus2sub_pcie_cpu_r_Ar_Valid                                  ;
	wire   [511:0]  dbus2sub_pcie_cpu_r_R_Data                                    ;
	wire   [3:0]    dbus2sub_pcie_cpu_r_R_Id                                      ;
	wire            dbus2sub_pcie_cpu_r_R_Last                                    ;
	wire            dbus2sub_pcie_cpu_r_R_Ready                                   ;
	wire   [1:0]    dbus2sub_pcie_cpu_r_R_Resp                                    ;
	wire   [3:0]    dbus2sub_pcie_cpu_r_R_User                                    ;
	wire            dbus2sub_pcie_cpu_r_R_Valid                                   ;
	wire   [36:0]   ddma_r_Ar_Addr                                                ;
	wire   [1:0]    ddma_r_Ar_Burst                                               ;
	wire   [3:0]    ddma_r_Ar_Cache                                               ;
	wire   [11:0]   ddma_r_Ar_Id                                                  ;
	wire   [4:0]    ddma_r_Ar_Len                                                 ;
	wire            ddma_r_Ar_Lock                                                ;
	wire   [2:0]    ddma_r_Ar_Prot                                                ;
	wire            ddma_r_Ar_Ready                                               ;
	wire   [2:0]    ddma_r_Ar_Size                                                ;
	wire   [3:0]    ddma_r_Ar_User                                                ;
	wire            ddma_r_Ar_Valid                                               ;
	wire   [1023:0] ddma_r_R_Data                                                 ;
	wire   [11:0]   ddma_r_R_Id                                                   ;
	wire            ddma_r_R_Last                                                 ;
	wire            ddma_r_R_Ready                                                ;
	wire   [1:0]    ddma_r_R_Resp                                                 ;
	wire   [3:0]    ddma_r_R_User                                                 ;
	wire            ddma_r_R_Valid                                                ;

//--------------------------------------------------------------------------------------
// write wire



	wire [123:0]  w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data           ;
	wire          w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head           ;
	wire          w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy            ;
	wire          w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail           ;
	wire          w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld            ;
	wire [700:0]  w_dp_Link_c01_m01_e_to_Switch_west_Data                         ;
	wire          w_dp_Link_c01_m01_e_to_Switch_west_Head                         ;
	wire          w_dp_Link_c01_m01_e_to_Switch_west_Rdy                          ;
	wire          w_dp_Link_c01_m01_e_to_Switch_west_Tail                         ;
	wire          w_dp_Link_c01_m01_e_to_Switch_west_Vld                          ;
	//wire [702:0]  w_dp_Link_c0_asi_to_Link_c0_ast_Data                            ;
	//wire [2:0]    w_dp_Link_c0_asi_to_Link_c0_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_c0_asi_to_Link_c0_ast_RdPtr                           ;
	//wire          w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_c0_asi_to_Link_c0_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_c0_astResp_to_Link_c0_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr                   ;
	//wire          w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt                   ;
	wire [123:0]  w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Data                      ;
	wire          w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Head                      ;
	wire          w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy                       ;
	wire          w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail                      ;
	wire          w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld                       ;
	wire [700:0]  w_dp_Link_c0s_b_to_Link_c0e_a_Data                              ;
	wire          w_dp_Link_c0s_b_to_Link_c0e_a_Head                              ;
	wire          w_dp_Link_c0s_b_to_Link_c0e_a_Rdy                               ;
	wire          w_dp_Link_c0s_b_to_Link_c0e_a_Tail                              ;
	wire          w_dp_Link_c0s_b_to_Link_c0e_a_Vld                               ;
	//wire [270:0]  w_dp_Link_c1_asi_to_Link_c1_ast_Data                            ;
	//wire [2:0]    w_dp_Link_c1_asi_to_Link_c1_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_c1_asi_to_Link_c1_ast_RdPtr                           ;
	//wire          w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_c1_asi_to_Link_c1_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data              ;
	//wire [2:0]    w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt             ;
	//wire [2:0]    w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr             ;
	//wire          w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst    ;
	//wire          w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst    ;
	//wire          w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt             ;
	//wire [270:0]  w_dp_Link_c1t_asi_to_Link_c1t_ast_Data                          ;
	//wire [2:0]    w_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt                         ;
	//wire [2:0]    w_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr                         ;
	//wire          w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst                ;
	//wire          w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck             ;
	//wire          w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst                ;
	//wire          w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck             ;
	//wire [2:0]    w_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt                         ;
	//wire [125:0]  w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data                  ;
	//wire [2:0]    w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt                 ;
	//wire [2:0]    w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr                 ;
	//wire          w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst        ;
	//wire          w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck     ;
	//wire          w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst        ;
	//wire          w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck     ;
	//wire [2:0]    w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt                 ;
	//wire [702:0]  w_dp_Link_m0_asi_to_Link_m0_ast_Data                            ;
	//wire [2:0]    w_dp_Link_m0_asi_to_Link_m0_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_m0_asi_to_Link_m0_ast_RdPtr                           ;
	//wire          w_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_m0_asi_to_Link_m0_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_Data              ;
	//wire [2:0]    w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdCnt             ;
	//wire [2:0]    w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdPtr             ;
	//wire          w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRst    ;
	//wire          w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRst    ;
	//wire          w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_WrCnt             ;
	//wire [702:0]  w_dp_Link_m1_asi_to_Link_m1_ast_Data                            ;
	//wire [2:0]    w_dp_Link_m1_asi_to_Link_m1_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_m1_asi_to_Link_m1_ast_RdPtr                           ;
	//wire          w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_m1_asi_to_Link_m1_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data              ;
	//wire [2:0]    w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt             ;
	//wire [2:0]    w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr             ;
	//wire          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst    ;
	//wire          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst    ;
	//wire          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt             ;
	//wire [702:0]  w_dp_Link_m2_asi_to_Link_m2_ast_Data                            ;
	//wire [2:0]    w_dp_Link_m2_asi_to_Link_m2_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_m2_asi_to_Link_m2_ast_RdPtr                           ;
	//wire          w_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_m2_asi_to_Link_m2_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data              ;
	//wire [2:0]    w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt             ;
	//wire [2:0]    w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr             ;
	//wire          w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst    ;
	//wire          w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst    ;
	//wire          w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt             ;
	//wire [702:0]  w_dp_Link_m3_asi_to_Link_m3_ast_Data                            ;
	//wire [2:0]    w_dp_Link_m3_asi_to_Link_m3_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_m3_asi_to_Link_m3_ast_RdPtr                           ;
	//wire          w_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_m3_asi_to_Link_m3_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data              ;
	//wire [2:0]    w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt             ;
	//wire [2:0]    w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr             ;
	//wire          w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst    ;
	//wire          w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck ;
	//wire          w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst    ;
	//wire          w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]    w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt             ;
	wire [1276:0] w_dp_Link_n03_m01_c_to_Switch_west_Data                         ;
	wire          w_dp_Link_n03_m01_c_to_Switch_west_Head                         ;
	wire          w_dp_Link_n03_m01_c_to_Switch_west_Rdy                          ;
	wire          w_dp_Link_n03_m01_c_to_Switch_west_Tail                         ;
	wire          w_dp_Link_n03_m01_c_to_Switch_west_Vld                          ;
	wire [1276:0] w_dp_Link_n03_m23_c_to_Switch_east_Data                         ;
	wire          w_dp_Link_n03_m23_c_to_Switch_east_Head                         ;
	wire          w_dp_Link_n03_m23_c_to_Switch_east_Rdy                          ;
	wire          w_dp_Link_n03_m23_c_to_Switch_east_Tail                         ;
	wire          w_dp_Link_n03_m23_c_to_Switch_east_Vld                          ;
	//wire [1278:0] w_dp_Link_n0_asi_to_Link_n0_ast_Data                            ;
	//wire [2:0]    w_dp_Link_n0_asi_to_Link_n0_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_n0_asi_to_Link_n0_ast_RdPtr                           ;
	//wire          w_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_n0_asi_to_Link_n0_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_n0_astResp_to_Link_n0_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_n0_astResp_to_Link_n0_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_n0_astResp_to_Link_n0_asiResp_RdPtr                   ;
	//wire          w_dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_n0_astResp_to_Link_n0_asiResp_WrCnt                   ;
	//wire [1278:0] w_dp_Link_n1_asi_to_Link_n1_ast_Data                            ;
	//wire [2:0]    w_dp_Link_n1_asi_to_Link_n1_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_n1_asi_to_Link_n1_ast_RdPtr                           ;
	//wire          w_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_n1_asi_to_Link_n1_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_n1_astResp_to_Link_n1_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_n1_astResp_to_Link_n1_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_n1_astResp_to_Link_n1_asiResp_RdPtr                   ;
	//wire          w_dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_n1_astResp_to_Link_n1_asiResp_WrCnt                   ;
	//wire [1278:0] w_dp_Link_n2_asi_to_Link_n2_ast_Data                            ;
	//wire [2:0]    w_dp_Link_n2_asi_to_Link_n2_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_n2_asi_to_Link_n2_ast_RdPtr                           ;
	//wire          w_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_n2_asi_to_Link_n2_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_n2_astResp_to_Link_n2_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_n2_astResp_to_Link_n2_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_n2_astResp_to_Link_n2_asiResp_RdPtr                   ;
	//wire          w_dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_n2_astResp_to_Link_n2_asiResp_WrCnt                   ;
	//wire [1278:0] w_dp_Link_n3_asi_to_Link_n3_ast_Data                            ;
	//wire [2:0]    w_dp_Link_n3_asi_to_Link_n3_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_n3_asi_to_Link_n3_ast_RdPtr                           ;
	//wire          w_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_n3_asi_to_Link_n3_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_n3_astResp_to_Link_n3_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_n3_astResp_to_Link_n3_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_n3_astResp_to_Link_n3_asiResp_RdPtr                   ;
	//wire          w_dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_n3_astResp_to_Link_n3_asiResp_WrCnt                   ;
	wire [1276:0] w_dp_Link_n47_m01_c_to_Switch_west_Data                         ;
	wire          w_dp_Link_n47_m01_c_to_Switch_west_Head                         ;
	wire          w_dp_Link_n47_m01_c_to_Switch_west_Rdy                          ;
	wire          w_dp_Link_n47_m01_c_to_Switch_west_Tail                         ;
	wire          w_dp_Link_n47_m01_c_to_Switch_west_Vld                          ;
	wire [1276:0] w_dp_Link_n47_m23_c_to_Switch_east_Data                         ;
	wire          w_dp_Link_n47_m23_c_to_Switch_east_Head                         ;
	wire          w_dp_Link_n47_m23_c_to_Switch_east_Rdy                          ;
	wire          w_dp_Link_n47_m23_c_to_Switch_east_Tail                         ;
	wire          w_dp_Link_n47_m23_c_to_Switch_east_Vld                          ;
	//wire [1278:0] w_dp_Link_n4_asi_to_Link_n4_ast_Data                            ;
	//wire [2:0]    w_dp_Link_n4_asi_to_Link_n4_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_n4_asi_to_Link_n4_ast_RdPtr                           ;
	//wire          w_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_n4_asi_to_Link_n4_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_n4_astResp_to_Link_n4_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_n4_astResp_to_Link_n4_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_n4_astResp_to_Link_n4_asiResp_RdPtr                   ;
	//wire          w_dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_n4_astResp_to_Link_n4_asiResp_WrCnt                   ;
	//wire [1278:0] w_dp_Link_n5_asi_to_Link_n5_ast_Data                            ;
	//wire [2:0]    w_dp_Link_n5_asi_to_Link_n5_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_n5_asi_to_Link_n5_ast_RdPtr                           ;
	//wire          w_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_n5_asi_to_Link_n5_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_n5_astResp_to_Link_n5_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_n5_astResp_to_Link_n5_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_n5_astResp_to_Link_n5_asiResp_RdPtr                   ;
	//wire          w_dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_n5_astResp_to_Link_n5_asiResp_WrCnt                   ;
	//wire [1278:0] w_dp_Link_n6_asi_to_Link_n6_ast_Data                            ;
	//wire [2:0]    w_dp_Link_n6_asi_to_Link_n6_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_n6_asi_to_Link_n6_ast_RdPtr                           ;
	//wire          w_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_n6_asi_to_Link_n6_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_n6_astResp_to_Link_n6_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_n6_astResp_to_Link_n6_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_n6_astResp_to_Link_n6_asiResp_RdPtr                   ;
	//wire          w_dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_n6_astResp_to_Link_n6_asiResp_WrCnt                   ;
	//wire [1278:0] w_dp_Link_n7_asi_to_Link_n7_ast_Data                            ;
	//wire [2:0]    w_dp_Link_n7_asi_to_Link_n7_ast_RdCnt                           ;
	//wire [2:0]    w_dp_Link_n7_asi_to_Link_n7_ast_RdPtr                           ;
	//wire          w_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck               ;
	//wire          w_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst                  ;
	//wire          w_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]    w_dp_Link_n7_asi_to_Link_n7_ast_WrCnt                           ;
	//wire [125:0]  w_dp_Link_n7_astResp_to_Link_n7_asiResp_Data                    ;
	//wire [2:0]    w_dp_Link_n7_astResp_to_Link_n7_asiResp_RdCnt                   ;
	//wire [2:0]    w_dp_Link_n7_astResp_to_Link_n7_asiResp_RdPtr                   ;
	//wire          w_dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRstAck       ;
	//wire          w_dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRst          ;
	//wire          w_dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRstAck       ;
	//wire [2:0]    w_dp_Link_n7_astResp_to_Link_n7_asiResp_WrCnt                   ;
	wire [1276:0] w_dp_Link_n8_m01_c_to_Switch_west_Data                          ;
	wire          w_dp_Link_n8_m01_c_to_Switch_west_Head                          ;
	wire          w_dp_Link_n8_m01_c_to_Switch_west_Rdy                           ;
	wire          w_dp_Link_n8_m01_c_to_Switch_west_Tail                          ;
	wire          w_dp_Link_n8_m01_c_to_Switch_west_Vld                           ;
	wire [1276:0] w_dp_Link_n8_m23_c_to_Switch_east_Data                          ;
	wire          w_dp_Link_n8_m23_c_to_Switch_east_Head                          ;
	wire          w_dp_Link_n8_m23_c_to_Switch_east_Rdy                           ;
	wire          w_dp_Link_n8_m23_c_to_Switch_east_Tail                          ;
	wire          w_dp_Link_n8_m23_c_to_Switch_east_Vld                           ;
	wire [123:0]  w_dp_Link_sc01Resp_to_Switch_eastResp001_Data                   ;
	wire          w_dp_Link_sc01Resp_to_Switch_eastResp001_Head                   ;
	wire          w_dp_Link_sc01Resp_to_Switch_eastResp001_Rdy                    ;
	wire          w_dp_Link_sc01Resp_to_Switch_eastResp001_Tail                   ;
	wire          w_dp_Link_sc01Resp_to_Switch_eastResp001_Vld                    ;
	wire [123:0]  w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data           ;
	wire          w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head           ;
	wire          w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy            ;
	wire          w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail           ;
	wire          w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld            ;
	wire [123:0]  w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data           ;
	wire          w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head           ;
	wire          w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy            ;
	wire          w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail           ;
	wire          w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld            ;
	wire [123:0]  w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data            ;
	wire          w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head            ;
	wire          w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy             ;
	wire          w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail            ;
	wire          w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld             ;
	wire [1276:0] w_dp_Switch_east_to_Link_c01_m01_a_Data                         ;
	wire          w_dp_Switch_east_to_Link_c01_m01_a_Head                         ;
	wire          w_dp_Switch_east_to_Link_c01_m01_a_Rdy                          ;
	wire          w_dp_Switch_east_to_Link_c01_m01_a_Tail                         ;
	wire          w_dp_Switch_east_to_Link_c01_m01_a_Vld                          ;
	wire [1276:0] w_dp_Switch_east_to_Link_sc01_Data                              ;
	wire          w_dp_Switch_east_to_Link_sc01_Head                              ;
	wire          w_dp_Switch_east_to_Link_sc01_Rdy                               ;
	wire          w_dp_Switch_east_to_Link_sc01_Tail                              ;
	wire          w_dp_Switch_east_to_Link_sc01_Vld                               ;
	wire [123:0]  w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data           ;
	wire          w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head           ;
	wire          w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy            ;
	wire          w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail           ;
	wire          w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld            ;
	wire [123:0]  w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data           ;
	wire          w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head           ;
	wire          w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy            ;
	wire          w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail           ;
	wire          w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld            ;
	wire [123:0]  w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data           ;
	wire          w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head           ;
	wire          w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy            ;
	wire          w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail           ;
	wire          w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld            ;
	wire [123:0]  w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data            ;
	wire          w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head            ;
	wire          w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy             ;
	wire          w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail            ;
	wire          w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld             ;



	wire   [27:0]   dbus2sub_dcluster0_port0_w_Aw_Addr  ;
	wire   [1:0]    dbus2sub_dcluster0_port0_w_Aw_Burst ;
	wire   [3:0]    dbus2sub_dcluster0_port0_w_Aw_Cache ;
	wire   [4:0]    dbus2sub_dcluster0_port0_w_Aw_Id    ;
	wire   [3:0]    dbus2sub_dcluster0_port0_w_Aw_Len   ;
	wire            dbus2sub_dcluster0_port0_w_Aw_Lock  ;
	wire   [2:0]    dbus2sub_dcluster0_port0_w_Aw_Prot  ;
	wire            dbus2sub_dcluster0_port0_w_Aw_Ready ;
	wire   [2:0]    dbus2sub_dcluster0_port0_w_Aw_Size  ;
	wire   [3:0]    dbus2sub_dcluster0_port0_w_Aw_User  ;
	wire            dbus2sub_dcluster0_port0_w_Aw_Valid ;
	wire   [4:0]    dbus2sub_dcluster0_port0_w_B_Id     ;
	wire            dbus2sub_dcluster0_port0_w_B_Ready  ;
	wire   [1:0]    dbus2sub_dcluster0_port0_w_B_Resp   ;
	wire   [3:0]    dbus2sub_dcluster0_port0_w_B_User   ;
	wire            dbus2sub_dcluster0_port0_w_B_Valid  ;
	wire   [1023:0] dbus2sub_dcluster0_port0_w_W_Data   ;
	wire            dbus2sub_dcluster0_port0_w_W_Last   ;
	wire            dbus2sub_dcluster0_port0_w_W_Ready  ;
	wire   [127:0]  dbus2sub_dcluster0_port0_w_W_Strb   ;
	wire            dbus2sub_dcluster0_port0_w_W_Valid  ;
	wire   [27:0]   dbus2sub_dcluster0_port1_w_Aw_Addr  ;
	wire   [1:0]    dbus2sub_dcluster0_port1_w_Aw_Burst ;
	wire   [3:0]    dbus2sub_dcluster0_port1_w_Aw_Cache ;
	wire   [4:0]    dbus2sub_dcluster0_port1_w_Aw_Id    ;
	wire   [3:0]    dbus2sub_dcluster0_port1_w_Aw_Len   ;
	wire            dbus2sub_dcluster0_port1_w_Aw_Lock  ;
	wire   [2:0]    dbus2sub_dcluster0_port1_w_Aw_Prot  ;
	wire            dbus2sub_dcluster0_port1_w_Aw_Ready ;
	wire   [2:0]    dbus2sub_dcluster0_port1_w_Aw_Size  ;
	wire   [3:0]    dbus2sub_dcluster0_port1_w_Aw_User  ;
	wire            dbus2sub_dcluster0_port1_w_Aw_Valid ;
	wire   [4:0]    dbus2sub_dcluster0_port1_w_B_Id     ;
	wire            dbus2sub_dcluster0_port1_w_B_Ready  ;
	wire   [1:0]    dbus2sub_dcluster0_port1_w_B_Resp   ;
	wire   [3:0]    dbus2sub_dcluster0_port1_w_B_User   ;
	wire            dbus2sub_dcluster0_port1_w_B_Valid  ;
	wire   [1023:0] dbus2sub_dcluster0_port1_w_W_Data   ;
	wire            dbus2sub_dcluster0_port1_w_W_Last   ;
	wire            dbus2sub_dcluster0_port1_w_W_Ready  ;
	wire   [127:0]  dbus2sub_dcluster0_port1_w_W_Strb   ;
	wire            dbus2sub_dcluster0_port1_w_W_Valid  ;
	wire   [27:0]   dbus2sub_dcluster0_port2_w_Aw_Addr  ;
	wire   [1:0]    dbus2sub_dcluster0_port2_w_Aw_Burst ;
	wire   [3:0]    dbus2sub_dcluster0_port2_w_Aw_Cache ;
	wire   [4:0]    dbus2sub_dcluster0_port2_w_Aw_Id    ;
	wire   [3:0]    dbus2sub_dcluster0_port2_w_Aw_Len   ;
	wire            dbus2sub_dcluster0_port2_w_Aw_Lock  ;
	wire   [2:0]    dbus2sub_dcluster0_port2_w_Aw_Prot  ;
	wire            dbus2sub_dcluster0_port2_w_Aw_Ready ;
	wire   [2:0]    dbus2sub_dcluster0_port2_w_Aw_Size  ;
	wire   [3:0]    dbus2sub_dcluster0_port2_w_Aw_User  ;
	wire            dbus2sub_dcluster0_port2_w_Aw_Valid ;
	wire   [4:0]    dbus2sub_dcluster0_port2_w_B_Id     ;
	wire            dbus2sub_dcluster0_port2_w_B_Ready  ;
	wire   [1:0]    dbus2sub_dcluster0_port2_w_B_Resp   ;
	wire   [3:0]    dbus2sub_dcluster0_port2_w_B_User   ;
	wire            dbus2sub_dcluster0_port2_w_B_Valid  ;
	wire   [1023:0] dbus2sub_dcluster0_port2_w_W_Data   ;
	wire            dbus2sub_dcluster0_port2_w_W_Last   ;
	wire            dbus2sub_dcluster0_port2_w_W_Ready  ;
	wire   [127:0]  dbus2sub_dcluster0_port2_w_W_Strb   ;
	wire            dbus2sub_dcluster0_port2_w_W_Valid  ;
	wire   [27:0]   dbus2sub_dcluster0_port3_w_Aw_Addr  ;
	wire   [1:0]    dbus2sub_dcluster0_port3_w_Aw_Burst ;
	wire   [3:0]    dbus2sub_dcluster0_port3_w_Aw_Cache ;
	wire   [4:0]    dbus2sub_dcluster0_port3_w_Aw_Id    ;
	wire   [3:0]    dbus2sub_dcluster0_port3_w_Aw_Len   ;
	wire            dbus2sub_dcluster0_port3_w_Aw_Lock  ;
	wire   [2:0]    dbus2sub_dcluster0_port3_w_Aw_Prot  ;
	wire            dbus2sub_dcluster0_port3_w_Aw_Ready ;
	wire   [2:0]    dbus2sub_dcluster0_port3_w_Aw_Size  ;
	wire   [3:0]    dbus2sub_dcluster0_port3_w_Aw_User  ;
	wire            dbus2sub_dcluster0_port3_w_Aw_Valid ;
	wire   [4:0]    dbus2sub_dcluster0_port3_w_B_Id     ;
	wire            dbus2sub_dcluster0_port3_w_B_Ready  ;
	wire   [1:0]    dbus2sub_dcluster0_port3_w_B_Resp   ;
	wire   [3:0]    dbus2sub_dcluster0_port3_w_B_User   ;
	wire            dbus2sub_dcluster0_port3_w_B_Valid  ;
	wire   [1023:0] dbus2sub_dcluster0_port3_w_W_Data   ;
	wire            dbus2sub_dcluster0_port3_w_W_Last   ;
	wire            dbus2sub_dcluster0_port3_w_W_Ready  ;
	wire   [127:0]  dbus2sub_dcluster0_port3_w_W_Strb   ;
	wire            dbus2sub_dcluster0_port3_w_W_Valid  ;
	wire   [27:0]   dbus2sub_dcluster1_port0_w_Aw_Addr  ;
	wire   [1:0]    dbus2sub_dcluster1_port0_w_Aw_Burst ;
	wire   [3:0]    dbus2sub_dcluster1_port0_w_Aw_Cache ;
	wire   [4:0]    dbus2sub_dcluster1_port0_w_Aw_Id    ;
	wire   [3:0]    dbus2sub_dcluster1_port0_w_Aw_Len   ;
	wire            dbus2sub_dcluster1_port0_w_Aw_Lock  ;
	wire   [2:0]    dbus2sub_dcluster1_port0_w_Aw_Prot  ;
	wire            dbus2sub_dcluster1_port0_w_Aw_Ready ;
	wire   [2:0]    dbus2sub_dcluster1_port0_w_Aw_Size  ;
	wire   [3:0]    dbus2sub_dcluster1_port0_w_Aw_User  ;
	wire            dbus2sub_dcluster1_port0_w_Aw_Valid ;
	wire   [4:0]    dbus2sub_dcluster1_port0_w_B_Id     ;
	wire            dbus2sub_dcluster1_port0_w_B_Ready  ;
	wire   [1:0]    dbus2sub_dcluster1_port0_w_B_Resp   ;
	wire   [3:0]    dbus2sub_dcluster1_port0_w_B_User   ;
	wire            dbus2sub_dcluster1_port0_w_B_Valid  ;
	wire   [1023:0] dbus2sub_dcluster1_port0_w_W_Data   ;
	wire            dbus2sub_dcluster1_port0_w_W_Last   ;
	wire            dbus2sub_dcluster1_port0_w_W_Ready  ;
	wire   [127:0]  dbus2sub_dcluster1_port0_w_W_Strb   ;
	wire            dbus2sub_dcluster1_port0_w_W_Valid  ;
	wire   [27:0]   dbus2sub_dcluster1_port1_w_Aw_Addr  ;
	wire   [1:0]    dbus2sub_dcluster1_port1_w_Aw_Burst ;
	wire   [3:0]    dbus2sub_dcluster1_port1_w_Aw_Cache ;
	wire   [4:0]    dbus2sub_dcluster1_port1_w_Aw_Id    ;
	wire   [3:0]    dbus2sub_dcluster1_port1_w_Aw_Len   ;
	wire            dbus2sub_dcluster1_port1_w_Aw_Lock  ;
	wire   [2:0]    dbus2sub_dcluster1_port1_w_Aw_Prot  ;
	wire            dbus2sub_dcluster1_port1_w_Aw_Ready ;
	wire   [2:0]    dbus2sub_dcluster1_port1_w_Aw_Size  ;
	wire   [3:0]    dbus2sub_dcluster1_port1_w_Aw_User  ;
	wire            dbus2sub_dcluster1_port1_w_Aw_Valid ;
	wire   [4:0]    dbus2sub_dcluster1_port1_w_B_Id     ;
	wire            dbus2sub_dcluster1_port1_w_B_Ready  ;
	wire   [1:0]    dbus2sub_dcluster1_port1_w_B_Resp   ;
	wire   [3:0]    dbus2sub_dcluster1_port1_w_B_User   ;
	wire            dbus2sub_dcluster1_port1_w_B_Valid  ;
	wire   [1023:0] dbus2sub_dcluster1_port1_w_W_Data   ;
	wire            dbus2sub_dcluster1_port1_w_W_Last   ;
	wire            dbus2sub_dcluster1_port1_w_W_Ready  ;
	wire   [127:0]  dbus2sub_dcluster1_port1_w_W_Strb   ;
	wire            dbus2sub_dcluster1_port1_w_W_Valid  ;
	wire   [27:0]   dbus2sub_dcluster1_port2_w_Aw_Addr  ;
	wire   [1:0]    dbus2sub_dcluster1_port2_w_Aw_Burst ;
	wire   [3:0]    dbus2sub_dcluster1_port2_w_Aw_Cache ;
	wire   [4:0]    dbus2sub_dcluster1_port2_w_Aw_Id    ;
	wire   [3:0]    dbus2sub_dcluster1_port2_w_Aw_Len   ;
	wire            dbus2sub_dcluster1_port2_w_Aw_Lock  ;
	wire   [2:0]    dbus2sub_dcluster1_port2_w_Aw_Prot  ;
	wire            dbus2sub_dcluster1_port2_w_Aw_Ready ;
	wire   [2:0]    dbus2sub_dcluster1_port2_w_Aw_Size  ;
	wire   [3:0]    dbus2sub_dcluster1_port2_w_Aw_User  ;
	wire            dbus2sub_dcluster1_port2_w_Aw_Valid ;
	wire   [4:0]    dbus2sub_dcluster1_port2_w_B_Id     ;
	wire            dbus2sub_dcluster1_port2_w_B_Ready  ;
	wire   [1:0]    dbus2sub_dcluster1_port2_w_B_Resp   ;
	wire   [3:0]    dbus2sub_dcluster1_port2_w_B_User   ;
	wire            dbus2sub_dcluster1_port2_w_B_Valid  ;
	wire   [1023:0] dbus2sub_dcluster1_port2_w_W_Data   ;
	wire            dbus2sub_dcluster1_port2_w_W_Last   ;
	wire            dbus2sub_dcluster1_port2_w_W_Ready  ;
	wire   [127:0]  dbus2sub_dcluster1_port2_w_W_Strb   ;
	wire            dbus2sub_dcluster1_port2_w_W_Valid  ;
	wire   [27:0]   dbus2sub_dcluster1_port3_w_Aw_Addr  ;
	wire   [1:0]    dbus2sub_dcluster1_port3_w_Aw_Burst ;
	wire   [3:0]    dbus2sub_dcluster1_port3_w_Aw_Cache ;
	wire   [4:0]    dbus2sub_dcluster1_port3_w_Aw_Id    ;
	wire   [3:0]    dbus2sub_dcluster1_port3_w_Aw_Len   ;
	wire            dbus2sub_dcluster1_port3_w_Aw_Lock  ;
	wire   [2:0]    dbus2sub_dcluster1_port3_w_Aw_Prot  ;
	wire            dbus2sub_dcluster1_port3_w_Aw_Ready ;
	wire   [2:0]    dbus2sub_dcluster1_port3_w_Aw_Size  ;
	wire   [3:0]    dbus2sub_dcluster1_port3_w_Aw_User  ;
	wire            dbus2sub_dcluster1_port3_w_Aw_Valid ;
	wire   [4:0]    dbus2sub_dcluster1_port3_w_B_Id     ;
	wire            dbus2sub_dcluster1_port3_w_B_Ready  ;
	wire   [1:0]    dbus2sub_dcluster1_port3_w_B_Resp   ;
	wire   [3:0]    dbus2sub_dcluster1_port3_w_B_User   ;
	wire            dbus2sub_dcluster1_port3_w_B_Valid  ;
	wire   [1023:0] dbus2sub_dcluster1_port3_w_W_Data   ;
	wire            dbus2sub_dcluster1_port3_w_W_Last   ;
	wire            dbus2sub_dcluster1_port3_w_W_Ready  ;
	wire   [127:0]  dbus2sub_dcluster1_port3_w_W_Strb   ;
	wire            dbus2sub_dcluster1_port3_w_W_Valid  ;
	wire   [27:0]   dbus2sub_ddma_w_Aw_Addr             ;
	wire   [1:0]    dbus2sub_ddma_w_Aw_Burst            ;
	wire   [3:0]    dbus2sub_ddma_w_Aw_Cache            ;
	wire   [4:0]    dbus2sub_ddma_w_Aw_Id               ;
	wire   [3:0]    dbus2sub_ddma_w_Aw_Len              ;
	wire            dbus2sub_ddma_w_Aw_Lock             ;
	wire   [2:0]    dbus2sub_ddma_w_Aw_Prot             ;
	wire            dbus2sub_ddma_w_Aw_Ready            ;
	wire   [2:0]    dbus2sub_ddma_w_Aw_Size             ;
	wire   [3:0]    dbus2sub_ddma_w_Aw_User             ;
	wire            dbus2sub_ddma_w_Aw_Valid            ;
	wire   [4:0]    dbus2sub_ddma_w_B_Id                ;
	wire            dbus2sub_ddma_w_B_Ready             ;
	wire   [1:0]    dbus2sub_ddma_w_B_Resp              ;
	wire   [3:0]    dbus2sub_ddma_w_B_User              ;
	wire            dbus2sub_ddma_w_B_Valid             ;
	wire   [1023:0] dbus2sub_ddma_w_W_Data              ;
	wire            dbus2sub_ddma_w_W_Last              ;
	wire            dbus2sub_ddma_w_W_Ready             ;
	wire   [127:0]  dbus2sub_ddma_w_W_Strb              ;
	wire            dbus2sub_ddma_w_W_Valid             ;
	wire   [27:0]   dbus2sub_pcie_cpu_w_Aw_Addr         ;
	wire   [1:0]    dbus2sub_pcie_cpu_w_Aw_Burst        ;
	wire   [3:0]    dbus2sub_pcie_cpu_w_Aw_Cache        ;
	wire   [3:0]    dbus2sub_pcie_cpu_w_Aw_Id           ;
	wire   [3:0]    dbus2sub_pcie_cpu_w_Aw_Len          ;
	wire            dbus2sub_pcie_cpu_w_Aw_Lock         ;
	wire   [2:0]    dbus2sub_pcie_cpu_w_Aw_Prot         ;
	wire            dbus2sub_pcie_cpu_w_Aw_Ready        ;
	wire   [2:0]    dbus2sub_pcie_cpu_w_Aw_Size         ;
	wire   [3:0]    dbus2sub_pcie_cpu_w_Aw_User         ;
	wire            dbus2sub_pcie_cpu_w_Aw_Valid        ;
	wire   [3:0]    dbus2sub_pcie_cpu_w_B_Id            ;
	wire            dbus2sub_pcie_cpu_w_B_Ready         ;
	wire   [1:0]    dbus2sub_pcie_cpu_w_B_Resp          ;
	wire   [3:0]    dbus2sub_pcie_cpu_w_B_User          ;
	wire            dbus2sub_pcie_cpu_w_B_Valid         ;
	wire   [511:0]  dbus2sub_pcie_cpu_w_W_Data          ;
	wire            dbus2sub_pcie_cpu_w_W_Last          ;
	wire            dbus2sub_pcie_cpu_w_W_Ready         ;
	wire   [63:0]   dbus2sub_pcie_cpu_w_W_Strb          ;
	wire            dbus2sub_pcie_cpu_w_W_Valid         ;
	wire   [36:0]   ddma_w_Aw_Addr                                          ;
	wire   [1:0]    ddma_w_Aw_Burst                                         ;
	wire   [3:0]    ddma_w_Aw_Cache                                         ;
	wire   [11:0]   ddma_w_Aw_Id                                            ;
	wire   [4:0]    ddma_w_Aw_Len                                           ;
	wire            ddma_w_Aw_Lock                                          ;
	wire   [2:0]    ddma_w_Aw_Prot                                          ;
	wire            ddma_w_Aw_Ready                                         ;
	wire   [2:0]    ddma_w_Aw_Size                                          ;
	wire   [3:0]    ddma_w_Aw_User                                          ;
	wire            ddma_w_Aw_Valid                                         ;
	wire   [11:0]   ddma_w_B_Id                                             ;
	wire            ddma_w_B_Ready                                          ;
	wire   [1:0]    ddma_w_B_Resp                                           ;
	wire   [3:0]    ddma_w_B_User                                           ;
	wire            ddma_w_B_Valid                                          ;
	wire   [1023:0] ddma_w_W_Data                                           ;
	wire            ddma_w_W_Last                                           ;
	wire            ddma_w_W_Ready                                          ;
	wire   [127:0]  ddma_w_W_Strb                                           ;
	wire            ddma_w_W_Valid                                          ;



//--------------------------------------------------------------------------------------
// cbus wire

	
	wire   [39:0] cbus2dbus_dbg_r_Ar_Addr                                   ;
	wire   [1:0]  cbus2dbus_dbg_r_Ar_Burst                                  ;
	wire   [3:0]  cbus2dbus_dbg_r_Ar_Cache                                  ;
	wire   [1:0]  cbus2dbus_dbg_r_Ar_Id                                     ;
	wire   [1:0]  cbus2dbus_dbg_r_Ar_Len                                    ;
	wire          cbus2dbus_dbg_r_Ar_Lock                                   ;
	wire   [2:0]  cbus2dbus_dbg_r_Ar_Prot                                   ;
	wire          cbus2dbus_dbg_r_Ar_Ready                                  ;
	wire   [2:0]  cbus2dbus_dbg_r_Ar_Size                                   ;
	wire          cbus2dbus_dbg_r_Ar_Valid                                  ;
	wire   [31:0] cbus2dbus_dbg_r_R_Data                                    ;
	wire   [1:0]  cbus2dbus_dbg_r_R_Id                                      ;
	wire          cbus2dbus_dbg_r_R_Last                                    ;
	wire          cbus2dbus_dbg_r_R_Ready                                   ;
	wire   [1:0]  cbus2dbus_dbg_r_R_Resp                                    ;
	wire          cbus2dbus_dbg_r_R_Valid                                   ;
	wire   [39:0] cbus2dbus_dbg_w_Aw_Addr                                   ;
	wire   [1:0]  cbus2dbus_dbg_w_Aw_Burst                                  ;
	wire   [3:0]  cbus2dbus_dbg_w_Aw_Cache                                  ;
	wire   [1:0]  cbus2dbus_dbg_w_Aw_Id                                     ;
	wire   [1:0]  cbus2dbus_dbg_w_Aw_Len                                    ;
	wire          cbus2dbus_dbg_w_Aw_Lock                                   ;
	wire   [2:0]  cbus2dbus_dbg_w_Aw_Prot                                   ;
	wire          cbus2dbus_dbg_w_Aw_Ready                                  ;
	wire   [2:0]  cbus2dbus_dbg_w_Aw_Size                                   ;
	wire          cbus2dbus_dbg_w_Aw_Valid                                  ;
	wire   [1:0]  cbus2dbus_dbg_w_B_Id                                      ;
	wire          cbus2dbus_dbg_w_B_Ready                                   ;
	wire   [1:0]  cbus2dbus_dbg_w_B_Resp                                    ;
	wire          cbus2dbus_dbg_w_B_Valid                                   ;
	wire   [31:0] cbus2dbus_dbg_w_W_Data                                    ;
	wire          cbus2dbus_dbg_w_W_Last                                    ;
	wire          cbus2dbus_dbg_w_W_Ready                                   ;
	wire   [3:0]  cbus2dbus_dbg_w_W_Strb                                    ;
	wire          cbus2dbus_dbg_w_W_Valid                                   ;

	wire   [39:0] dbus2cbus_cpu_r_Ar_Addr                                   ;
	wire   [1:0]  dbus2cbus_cpu_r_Ar_Burst                                  ;
	wire   [3:0]  dbus2cbus_cpu_r_Ar_Cache                                  ;
	wire   [7:0]  dbus2cbus_cpu_r_Ar_Id                                     ;
	wire   [1:0]  dbus2cbus_cpu_r_Ar_Len                                    ;
	wire          dbus2cbus_cpu_r_Ar_Lock                                   ;
	wire   [2:0]  dbus2cbus_cpu_r_Ar_Prot                                   ;
	wire          dbus2cbus_cpu_r_Ar_Ready                                  ;
	wire   [2:0]  dbus2cbus_cpu_r_Ar_Size                                   ;
	wire   [31:0] dbus2cbus_cpu_r_Ar_User                                   ;
	wire          dbus2cbus_cpu_r_Ar_Valid                                  ;
	wire   [31:0] dbus2cbus_cpu_r_R_Data                                    ;
	wire   [7:0]  dbus2cbus_cpu_r_R_Id                                      ;
	wire          dbus2cbus_cpu_r_R_Last                                    ;
	wire          dbus2cbus_cpu_r_R_Ready                                   ;
	wire   [1:0]  dbus2cbus_cpu_r_R_Resp                                    ;
	wire          dbus2cbus_cpu_r_R_Valid                                   ;
	wire   [39:0] dbus2cbus_cpu_w_Aw_Addr                                   ;
	wire   [1:0]  dbus2cbus_cpu_w_Aw_Burst                                  ;
	wire   [3:0]  dbus2cbus_cpu_w_Aw_Cache                                  ;
	wire   [7:0]  dbus2cbus_cpu_w_Aw_Id                                     ;
	wire   [1:0]  dbus2cbus_cpu_w_Aw_Len                                    ;
	wire          dbus2cbus_cpu_w_Aw_Lock                                   ;
	wire   [2:0]  dbus2cbus_cpu_w_Aw_Prot                                   ;
	wire          dbus2cbus_cpu_w_Aw_Ready                                  ;
	wire   [2:0]  dbus2cbus_cpu_w_Aw_Size                                   ;
	wire   [31:0] dbus2cbus_cpu_w_Aw_User                                   ;
	wire          dbus2cbus_cpu_w_Aw_Valid                                  ;
	wire   [7:0]  dbus2cbus_cpu_w_B_Id                                      ;
	wire          dbus2cbus_cpu_w_B_Ready                                   ;
	wire   [1:0]  dbus2cbus_cpu_w_B_Resp                                    ;
	wire          dbus2cbus_cpu_w_B_Valid                                   ;
	wire   [31:0] dbus2cbus_cpu_w_W_Data                                    ;
	wire          dbus2cbus_cpu_w_W_Last                                    ;
	wire          dbus2cbus_cpu_w_W_Ready                                   ;
	wire   [3:0]  dbus2cbus_cpu_w_W_Strb                                    ;
	wire          dbus2cbus_cpu_w_W_Valid                                   ;
	wire   [23:0] dbus2cbus_pcie_r_Ar_Addr                                  ;
	wire   [1:0]  dbus2cbus_pcie_r_Ar_Burst                                 ;
	wire   [3:0]  dbus2cbus_pcie_r_Ar_Cache                                 ;
	wire   [4:0]  dbus2cbus_pcie_r_Ar_Id                                    ;
	wire   [3:0]  dbus2cbus_pcie_r_Ar_Len                                   ;
	wire          dbus2cbus_pcie_r_Ar_Lock                                  ;
	wire   [2:0]  dbus2cbus_pcie_r_Ar_Prot                                  ;
	wire          dbus2cbus_pcie_r_Ar_Ready                                 ;
	wire   [2:0]  dbus2cbus_pcie_r_Ar_Size                                  ;
	wire          dbus2cbus_pcie_r_Ar_Valid                                 ;
	wire   [31:0] dbus2cbus_pcie_r_R_Data                                   ;
	wire   [4:0]  dbus2cbus_pcie_r_R_Id                                     ;
	wire          dbus2cbus_pcie_r_R_Last                                   ;
	wire          dbus2cbus_pcie_r_R_Ready                                  ;
	wire   [1:0]  dbus2cbus_pcie_r_R_Resp                                   ;
	wire          dbus2cbus_pcie_r_R_Valid                                  ;
	wire   [23:0] dbus2cbus_pcie_w_Aw_Addr                                  ;
	wire   [1:0]  dbus2cbus_pcie_w_Aw_Burst                                 ;
	wire   [3:0]  dbus2cbus_pcie_w_Aw_Cache                                 ;
	wire   [4:0]  dbus2cbus_pcie_w_Aw_Id                                    ;
	wire   [3:0]  dbus2cbus_pcie_w_Aw_Len                                   ;
	wire          dbus2cbus_pcie_w_Aw_Lock                                  ;
	wire   [2:0]  dbus2cbus_pcie_w_Aw_Prot                                  ;
	wire          dbus2cbus_pcie_w_Aw_Ready                                 ;
	wire   [2:0]  dbus2cbus_pcie_w_Aw_Size                                  ;
	wire          dbus2cbus_pcie_w_Aw_Valid                                 ;
	wire   [4:0]  dbus2cbus_pcie_w_B_Id                                     ;
	wire          dbus2cbus_pcie_w_B_Ready                                  ;
	wire   [1:0]  dbus2cbus_pcie_w_B_Resp                                   ;
	wire          dbus2cbus_pcie_w_B_Valid                                  ;
	wire   [31:0] dbus2cbus_pcie_w_W_Data                                   ;
	wire          dbus2cbus_pcie_w_W_Last                                   ;
	wire          dbus2cbus_pcie_w_W_Ready                                  ;
	wire   [3:0]  dbus2cbus_pcie_w_W_Strb                                   ;
	wire          dbus2cbus_pcie_w_W_Valid                                  ;


	//wire [57:0] ctrl_dp_Link13_to_Link37_Data                                              ;
	//wire [2:0]  ctrl_dp_Link13_to_Link37_RdCnt                                             ;
	//wire [1:0]  ctrl_dp_Link13_to_Link37_RdPtr                                             ;
	//wire        ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRst                                    ;
	//wire        ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRstAck                                 ;
	//wire        ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRst                                    ;
	//wire        ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRstAck                                 ;
	//wire [2:0]  ctrl_dp_Link13_to_Link37_WrCnt                                             ;
	//wire [57:0] ctrl_dp_Link2_to_Switch_west_Data                                          ;
	//wire [2:0]  ctrl_dp_Link2_to_Switch_west_RdCnt                                         ;
	//wire [1:0]  ctrl_dp_Link2_to_Switch_west_RdPtr                                         ;
	//wire        ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRst                                ;
	//wire        ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck                             ;
	//wire        ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRst                                ;
	//wire        ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck                             ;
	//wire [2:0]  ctrl_dp_Link2_to_Switch_west_WrCnt                                         ;
	//wire [57:0] ctrl_dp_Link31_to_Switch_ctrl_tResp001_Data                                ;
	//wire [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdCnt                               ;
	//wire [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdPtr                               ;
	//wire        ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck                   ;
	//wire        ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck                   ;
	//wire [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_WrCnt                               ;
	//wire [57:0] ctrl_dp_Link32_to_Switch_ctrl_tResp001_Data                                ;
	//wire [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdCnt                               ;
	//wire [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdPtr                               ;
	//wire        ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck                   ;
	//wire        ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck                   ;
	//wire [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_WrCnt                               ;
	//wire [57:0] ctrl_dp_Link33_to_Switch_ctrl_tResp001_Data                                ;
	//wire [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdCnt                               ;
	//wire [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdPtr                               ;
	//wire        ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck                   ;
	//wire        ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck                   ;
	//wire [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_WrCnt                               ;
	//wire [57:0] ctrl_dp_Link34_to_Switch_ctrl_tResp001_Data                                ;
	//wire [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdCnt                               ;
	//wire [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdPtr                               ;
	//wire        ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck                   ;
	//wire        ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck                   ;
	//wire [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_WrCnt                               ;
	//wire [57:0] ctrl_dp_Link35_to_Switch_ctrl_iResp001_Data                                ;
	//wire [2:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdCnt                               ;
	//wire [1:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdPtr                               ;
	//wire        ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRstAck                   ;
	//wire        ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRst                      ;
	//wire        ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRstAck                   ;
	//wire [2:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_WrCnt                               ;
	//wire [57:0] ctrl_dp_Link36_to_Link5_Data                                               ;
	//wire [2:0]  ctrl_dp_Link36_to_Link5_RdCnt                                              ;
	//wire [1:0]  ctrl_dp_Link36_to_Link5_RdPtr                                              ;
	//wire        ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRst                                     ;
	//wire        ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRstAck                                  ;
	//wire        ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRst                                     ;
	//wire        ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRstAck                                  ;
	//wire [2:0]  ctrl_dp_Link36_to_Link5_WrCnt                                              ;
	//wire [57:0] ctrl_dp_Link3_to_Link_m2ctrl_ast_Data                                      ;
	//wire [2:0]  ctrl_dp_Link3_to_Link_m2ctrl_ast_RdCnt                                     ;
	//wire [1:0]  ctrl_dp_Link3_to_Link_m2ctrl_ast_RdPtr                                     ;
	//wire        ctrl_dp_Link3_to_Link_m2ctrl_ast_RxCtl_PwrOnRst                            ;
	//wire        ctrl_dp_Link3_to_Link_m2ctrl_ast_RxCtl_PwrOnRstAck                         ;
	//wire        ctrl_dp_Link3_to_Link_m2ctrl_ast_TxCtl_PwrOnRst                            ;
	//wire        ctrl_dp_Link3_to_Link_m2ctrl_ast_TxCtl_PwrOnRstAck                         ;
	//wire [2:0]  ctrl_dp_Link3_to_Link_m2ctrl_ast_WrCnt                                     ;
	wire [55:0] ctrl_dp_Link6Resp001_to_Switch_eastResp001_Data                            ;
	wire        ctrl_dp_Link6Resp001_to_Switch_eastResp001_Head                            ;
	wire        ctrl_dp_Link6Resp001_to_Switch_eastResp001_Rdy                             ;
	wire        ctrl_dp_Link6Resp001_to_Switch_eastResp001_Tail                            ;
	wire        ctrl_dp_Link6Resp001_to_Switch_eastResp001_Vld                             ;
	//wire [57:0] ctrl_dp_Link7_to_Link_2c0_3_Data                                           ;
	//wire [2:0]  ctrl_dp_Link7_to_Link_2c0_3_RdCnt                                          ;
	//wire [2:0]  ctrl_dp_Link7_to_Link_2c0_3_RdPtr                                          ;
	//wire        ctrl_dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst                                 ;
	//wire        ctrl_dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck                              ;
	//wire        ctrl_dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst                                 ;
	//wire        ctrl_dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck                              ;
	//wire [2:0]  ctrl_dp_Link7_to_Link_2c0_3_WrCnt                                          ;
	//wire [57:0] ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_Data                             ;
	//wire [2:0]  ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt                            ;
	//wire [2:0]  ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr                            ;
	//wire        ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst                   ;
	//wire        ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck                ;
	//wire        ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst                   ;
	//wire        ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck                ;
	//wire [2:0]  ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt                            ;
	//wire [57:0] ctrl_dp_Link_c0_asi_to_Link_c0_ast_Data                                    ;
	//wire [2:0]  ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdCnt                                   ;
	//wire [2:0]  ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdPtr                                   ;
	//wire        ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst                          ;
	//wire        ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck                       ;
	//wire        ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst                          ;
	//wire        ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck                       ;
	//wire [2:0]  ctrl_dp_Link_c0_asi_to_Link_c0_ast_WrCnt                                   ;
	//wire [57:0] ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data                      ;
	//wire [2:0]  ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt                     ;
	//wire [2:0]  ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr                     ;
	//wire        ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst            ;
	//wire        ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck         ;
	//wire        ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst            ;
	//wire        ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck         ;
	//wire [2:0]  ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt                     ;
	//wire [57:0] ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data                            ;
	//wire [2:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt                           ;
	//wire [1:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr                           ;
	//wire        ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst                  ;
	//wire        ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck               ;
	//wire        ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst                  ;
	//wire        ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt                           ;
	//wire [57:0] ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data              ;
	//wire [2:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt             ;
	//wire [1:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr             ;
	//wire        ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst    ;
	//wire        ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck ;
	//wire        ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst    ;
	//wire        ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt             ;
	//wire [57:0] ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_Data                            ;
	//wire [2:0]  ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdCnt                           ;
	//wire [1:0]  ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdPtr                           ;
	//wire        ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRst                  ;
	//wire        ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRstAck               ;
	//wire        ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRst                  ;
	//wire        ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]  ctrl_dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_WrCnt                           ;
	//wire [57:0] ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_Data              ;
	//wire [2:0]  ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdCnt             ;
	//wire [1:0]  ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdPtr             ;
	//wire        ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRst    ;
	//wire        ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRstAck ;
	//wire        ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRst    ;
	//wire        ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]  ctrl_dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_WrCnt             ;
	//wire [57:0] ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data                            ;
	//wire [2:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt                           ;
	//wire [1:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr                           ;
	//wire        ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst                  ;
	//wire        ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck               ;
	//wire        ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst                  ;
	//wire        ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt                           ;
	//wire [57:0] ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data              ;
	//wire [2:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt             ;
	//wire [1:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr             ;
	//wire        ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst    ;
	//wire        ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck ;
	//wire        ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst    ;
	//wire        ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt             ;
	//wire [57:0] ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_Data              ;
	//wire [2:0]  ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RdCnt             ;
	//wire [1:0]  ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RdPtr             ;
	//wire        ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RxCtl_PwrOnRst    ;
	//wire        ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RxCtl_PwrOnRstAck ;
	//wire        ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_TxCtl_PwrOnRst    ;
	//wire        ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_TxCtl_PwrOnRstAck ;
	//wire [2:0]  ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_WrCnt             ;
	//wire [57:0] ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_Data                            ;
	//wire [2:0]  ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RdCnt                           ;
	//wire [1:0]  ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RdPtr                           ;
	//wire        ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RxCtl_PwrOnRst                  ;
	//wire        ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RxCtl_PwrOnRstAck               ;
	//wire        ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_TxCtl_PwrOnRst                  ;
	//wire        ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_TxCtl_PwrOnRstAck               ;
	//wire [2:0]  ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_WrCnt                           ;
	//wire [57:0] ctrl_dp_Link_m3ctrl_astResp001_to_Link10_Data                              ;
	//wire [2:0]  ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RdCnt                             ;
	//wire [1:0]  ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RdPtr                             ;
	//wire        ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RxCtl_PwrOnRst                    ;
	//wire        ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RxCtl_PwrOnRstAck                 ;
	//wire        ctrl_dp_Link_m3ctrl_astResp001_to_Link10_TxCtl_PwrOnRst                    ;
	//wire        ctrl_dp_Link_m3ctrl_astResp001_to_Link10_TxCtl_PwrOnRstAck                 ;
	//wire [2:0]  ctrl_dp_Link_m3ctrl_astResp001_to_Link10_WrCnt                             ;
	//wire [57:0] ctrl_dp_Link_n03_asi_to_Link_n03_ast_Data                                  ;
	//wire [2:0]  ctrl_dp_Link_n03_asi_to_Link_n03_ast_RdCnt                                 ;
	//wire [2:0]  ctrl_dp_Link_n03_asi_to_Link_n03_ast_RdPtr                                 ;
	//wire        ctrl_dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRst                        ;
	//wire        ctrl_dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRstAck                     ;
	//wire        ctrl_dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRst                        ;
	//wire        ctrl_dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRstAck                     ;
	//wire [2:0]  ctrl_dp_Link_n03_asi_to_Link_n03_ast_WrCnt                                 ;
	//wire [57:0] ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_Data                    ;
	//wire [2:0]  ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdCnt                   ;
	//wire [2:0]  ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdPtr                   ;
	//wire        ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRst          ;
	//wire        ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRstAck       ;
	//wire        ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRst          ;
	//wire        ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRstAck       ;
	//wire [2:0]  ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_WrCnt                   ;
	//wire [57:0] ctrl_dp_Link_n47_asi_to_Link_n47_ast_Data                                  ;
	//wire [2:0]  ctrl_dp_Link_n47_asi_to_Link_n47_ast_RdCnt                                 ;
	//wire [2:0]  ctrl_dp_Link_n47_asi_to_Link_n47_ast_RdPtr                                 ;
	//wire        ctrl_dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRst                        ;
	//wire        ctrl_dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRstAck                     ;
	//wire        ctrl_dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRst                        ;
	//wire        ctrl_dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRstAck                     ;
	//wire [2:0]  ctrl_dp_Link_n47_asi_to_Link_n47_ast_WrCnt                                 ;
	//wire [57:0] ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_Data                    ;
	//wire [2:0]  ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdCnt                   ;
	//wire [2:0]  ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdPtr                   ;
	//wire        ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRst          ;
	//wire        ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRstAck       ;
	//wire        ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRst          ;
	//wire        ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRstAck       ;
	//wire [2:0]  ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_WrCnt                   ;
	wire [55:0] ctrl_dp_Switch2_to_Link6_Data                                              ;
	wire        ctrl_dp_Switch2_to_Link6_Head                                              ;
	wire        ctrl_dp_Switch2_to_Link6_Rdy                                               ;
	wire        ctrl_dp_Switch2_to_Link6_Tail                                              ;
	wire        ctrl_dp_Switch2_to_Link6_Vld                                               ;
	//wire [57:0] ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_Data                          ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt                         ;
	//wire [1:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr                         ;
	//wire        ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst                ;
	//wire        ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck             ;
	//wire        ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst                ;
	//wire        ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck             ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt                         ;
	//wire [57:0] ctrl_dp_Switch_ctrl_i_to_Link17_Data                                       ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_i_to_Link17_RdCnt                                      ;
	//wire [1:0]  ctrl_dp_Switch_ctrl_i_to_Link17_RdPtr                                      ;
	//wire        ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRstAck                          ;
	//wire        ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRstAck                          ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_i_to_Link17_WrCnt                                      ;
	//wire [57:0] ctrl_dp_Switch_ctrl_t_to_Link27_Data                                       ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_RdCnt                                      ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_RdPtr                                      ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck                          ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck                          ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_WrCnt                                      ;
	//wire [57:0] ctrl_dp_Switch_ctrl_t_to_Link28_Data                                       ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_RdCnt                                      ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_RdPtr                                      ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck                          ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck                          ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_WrCnt                                      ;
	//wire [57:0] ctrl_dp_Switch_ctrl_t_to_Link29_Data                                       ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_RdCnt                                      ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_RdPtr                                      ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck                          ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck                          ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_WrCnt                                      ;
	//wire [57:0] ctrl_dp_Switch_ctrl_t_to_Link30_Data                                       ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_RdCnt                                      ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_RdPtr                                      ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck                          ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst                             ;
	//wire        ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck                          ;
	//wire [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_WrCnt                                      ;
	//wire [57:0] ctrl_dp_Switch_westResp001_to_Link13Resp001_Data                           ;
	//wire [2:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_RdCnt                          ;
	//wire [1:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_RdPtr                          ;
	//wire        ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst                 ;
	//wire        ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck              ;
	//wire        ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst                 ;
	//wire        ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck              ;
	//wire [2:0]  ctrl_dp_Switch_westResp001_to_Link13Resp001_WrCnt                          ;
	//wire [57:0] ctrl_dp_dbg_master_I_to_Link1_Data                                         ;
	//wire [2:0]  ctrl_dp_dbg_master_I_to_Link1_RdCnt                                        ;
	//wire [1:0]  ctrl_dp_dbg_master_I_to_Link1_RdPtr                                        ;
	//wire        ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst                               ;
	//wire        ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck                            ;
	//wire        ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst                               ;
	//wire        ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck                            ;
	//wire [2:0]  ctrl_dp_dbg_master_I_to_Link1_WrCnt                                        ;



	wire   [23:0]   ddma_ctrl_Ar_Addr            ;
	wire   [1:0]    ddma_ctrl_Ar_Burst           ;
	wire   [3:0]    ddma_ctrl_Ar_Cache           ;
	wire   [1:0]    ddma_ctrl_Ar_Id              ;
	wire   [1:0]    ddma_ctrl_Ar_Len             ;
	wire            ddma_ctrl_Ar_Lock            ;
	wire   [2:0]    ddma_ctrl_Ar_Prot            ;
	wire            ddma_ctrl_Ar_Ready           ;
	wire   [2:0]    ddma_ctrl_Ar_Size            ;
	wire            ddma_ctrl_Ar_Valid           ;
	wire   [23:0]   ddma_ctrl_Aw_Addr            ;
	wire   [1:0]    ddma_ctrl_Aw_Burst           ;
	wire   [3:0]    ddma_ctrl_Aw_Cache           ;
	wire   [1:0]    ddma_ctrl_Aw_Id              ;
	wire   [1:0]    ddma_ctrl_Aw_Len             ;
	wire            ddma_ctrl_Aw_Lock            ;
	wire   [2:0]    ddma_ctrl_Aw_Prot            ;
	wire            ddma_ctrl_Aw_Ready           ;
	wire   [2:0]    ddma_ctrl_Aw_Size            ;
	wire            ddma_ctrl_Aw_Valid           ;
	wire   [1:0]    ddma_ctrl_B_Id               ;
	wire            ddma_ctrl_B_Ready            ;
	wire   [1:0]    ddma_ctrl_B_Resp             ;
	wire            ddma_ctrl_B_Valid            ;
	wire   [31:0]   ddma_ctrl_R_Data             ;
	wire   [1:0]    ddma_ctrl_R_Id               ;
	wire            ddma_ctrl_R_Last             ;
	wire            ddma_ctrl_R_Ready            ;
	wire   [1:0]    ddma_ctrl_R_Resp             ;
	wire            ddma_ctrl_R_Valid            ;
	wire   [31:0]   ddma_ctrl_W_Data             ;
	wire            ddma_ctrl_W_Last             ;
	wire            ddma_ctrl_W_Ready            ;
	wire   [3:0]    ddma_ctrl_W_Strb             ;
	wire            ddma_ctrl_W_Valid            ;






//--------------------------------------------------------------------------------------
//
//
//
// module
//
//
//
//--------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------
// dbus_read



	dbus_read_Structure_Module_westbus u_dbus_read_Structure_Module_westbus(
		.TM( TM )
	,	.arstn_dbus( arstn_dbus )
	,	.clk_dbus( clk_dbus )
	,	.dp_Link_c01_m01_e_to_Switch_west_Data( r_dp_Link_c01_m01_e_to_Switch_west_Data )
	,	.dp_Link_c01_m01_e_to_Switch_west_Head( r_dp_Link_c01_m01_e_to_Switch_west_Head )
	,	.dp_Link_c01_m01_e_to_Switch_west_Rdy( r_dp_Link_c01_m01_e_to_Switch_west_Rdy )
	,	.dp_Link_c01_m01_e_to_Switch_west_Tail( r_dp_Link_c01_m01_e_to_Switch_west_Tail )
	,	.dp_Link_c01_m01_e_to_Switch_west_Vld( r_dp_Link_c01_m01_e_to_Switch_west_Vld )
	,	.dp_Link_m0_asi_to_Link_m0_ast_Data( r_dp_Link_m0_asi_to_Link_m0_ast_Data )
	,	.dp_Link_m0_asi_to_Link_m0_ast_RdCnt( r_dp_Link_m0_asi_to_Link_m0_ast_RdCnt )
	,	.dp_Link_m0_asi_to_Link_m0_ast_RdPtr( r_dp_Link_m0_asi_to_Link_m0_ast_RdPtr )
	,	.dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRst( r_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRstAck( r_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRst( r_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRstAck( r_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m0_asi_to_Link_m0_ast_WrCnt( r_dp_Link_m0_asi_to_Link_m0_ast_WrCnt )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_Data( r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_Data )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdCnt( r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdCnt )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdPtr( r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdPtr )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRst( r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRst( r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_WrCnt( r_dp_Link_m0_astResp001_to_Link_m0_asiResp001_WrCnt )
	,	.dp_Link_m1_asi_to_Link_m1_ast_Data( r_dp_Link_m1_asi_to_Link_m1_ast_Data )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RdCnt( r_dp_Link_m1_asi_to_Link_m1_ast_RdCnt )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RdPtr( r_dp_Link_m1_asi_to_Link_m1_ast_RdPtr )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst( r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck( r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst( r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck( r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1_asi_to_Link_m1_ast_WrCnt( r_dp_Link_m1_asi_to_Link_m1_ast_WrCnt )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt )
	,	.dp_Link_n03_m01_c_to_Switch_west_Data( r_dp_Link_n03_m01_c_to_Switch_west_Data )
	,	.dp_Link_n03_m01_c_to_Switch_west_Head( r_dp_Link_n03_m01_c_to_Switch_west_Head )
	,	.dp_Link_n03_m01_c_to_Switch_west_Rdy( r_dp_Link_n03_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n03_m01_c_to_Switch_west_Tail( r_dp_Link_n03_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n03_m01_c_to_Switch_west_Vld( r_dp_Link_n03_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n47_m01_c_to_Switch_west_Data( r_dp_Link_n47_m01_c_to_Switch_west_Data )
	,	.dp_Link_n47_m01_c_to_Switch_west_Head( r_dp_Link_n47_m01_c_to_Switch_west_Head )
	,	.dp_Link_n47_m01_c_to_Switch_west_Rdy( r_dp_Link_n47_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n47_m01_c_to_Switch_west_Tail( r_dp_Link_n47_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n47_m01_c_to_Switch_west_Vld( r_dp_Link_n47_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n8_m01_c_to_Switch_west_Data( r_dp_Link_n8_m01_c_to_Switch_west_Data )
	,	.dp_Link_n8_m01_c_to_Switch_west_Head( r_dp_Link_n8_m01_c_to_Switch_west_Head )
	,	.dp_Link_n8_m01_c_to_Switch_west_Rdy( r_dp_Link_n8_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n8_m01_c_to_Switch_west_Tail( r_dp_Link_n8_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n8_m01_c_to_Switch_west_Vld( r_dp_Link_n8_m01_c_to_Switch_west_Vld )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld )
	);


	dbus_read_Structure_Module_centerbus u_dbus_read_Structure_Module_centerbus(
		.TM( TM )
	,	.arstn_dbus( arstn_dbus )
	,	.clk_dbus( clk_dbus )
	,	.dbus2sub_dcluster0_port0_r_Ar_Addr( dbus2sub_dcluster0_port0_r_Ar_Addr )
	,	.dbus2sub_dcluster0_port0_r_Ar_Burst( dbus2sub_dcluster0_port0_r_Ar_Burst )
	,	.dbus2sub_dcluster0_port0_r_Ar_Cache( dbus2sub_dcluster0_port0_r_Ar_Cache )
	,	.dbus2sub_dcluster0_port0_r_Ar_Id( dbus2sub_dcluster0_port0_r_Ar_Id )
	,	.dbus2sub_dcluster0_port0_r_Ar_Len( dbus2sub_dcluster0_port0_r_Ar_Len )
	,	.dbus2sub_dcluster0_port0_r_Ar_Lock( dbus2sub_dcluster0_port0_r_Ar_Lock )
	,	.dbus2sub_dcluster0_port0_r_Ar_Prot( dbus2sub_dcluster0_port0_r_Ar_Prot )
	,	.dbus2sub_dcluster0_port0_r_Ar_Ready( dbus2sub_dcluster0_port0_r_Ar_Ready )
	,	.dbus2sub_dcluster0_port0_r_Ar_Size( dbus2sub_dcluster0_port0_r_Ar_Size )
	,	.dbus2sub_dcluster0_port0_r_Ar_User( dbus2sub_dcluster0_port0_r_Ar_User )
	,	.dbus2sub_dcluster0_port0_r_Ar_Valid( dbus2sub_dcluster0_port0_r_Ar_Valid )
	,	.dbus2sub_dcluster0_port0_r_R_Data( dbus2sub_dcluster0_port0_r_R_Data )
	,	.dbus2sub_dcluster0_port0_r_R_Id( dbus2sub_dcluster0_port0_r_R_Id )
	,	.dbus2sub_dcluster0_port0_r_R_Last( dbus2sub_dcluster0_port0_r_R_Last )
	,	.dbus2sub_dcluster0_port0_r_R_Ready( dbus2sub_dcluster0_port0_r_R_Ready )
	,	.dbus2sub_dcluster0_port0_r_R_Resp( dbus2sub_dcluster0_port0_r_R_Resp )
	,	.dbus2sub_dcluster0_port0_r_R_User( dbus2sub_dcluster0_port0_r_R_User )
	,	.dbus2sub_dcluster0_port0_r_R_Valid( dbus2sub_dcluster0_port0_r_R_Valid )
	,	.dbus2sub_dcluster0_port1_r_Ar_Addr( dbus2sub_dcluster0_port1_r_Ar_Addr )
	,	.dbus2sub_dcluster0_port1_r_Ar_Burst( dbus2sub_dcluster0_port1_r_Ar_Burst )
	,	.dbus2sub_dcluster0_port1_r_Ar_Cache( dbus2sub_dcluster0_port1_r_Ar_Cache )
	,	.dbus2sub_dcluster0_port1_r_Ar_Id( dbus2sub_dcluster0_port1_r_Ar_Id )
	,	.dbus2sub_dcluster0_port1_r_Ar_Len( dbus2sub_dcluster0_port1_r_Ar_Len )
	,	.dbus2sub_dcluster0_port1_r_Ar_Lock( dbus2sub_dcluster0_port1_r_Ar_Lock )
	,	.dbus2sub_dcluster0_port1_r_Ar_Prot( dbus2sub_dcluster0_port1_r_Ar_Prot )
	,	.dbus2sub_dcluster0_port1_r_Ar_Ready( dbus2sub_dcluster0_port1_r_Ar_Ready )
	,	.dbus2sub_dcluster0_port1_r_Ar_Size( dbus2sub_dcluster0_port1_r_Ar_Size )
	,	.dbus2sub_dcluster0_port1_r_Ar_User( dbus2sub_dcluster0_port1_r_Ar_User )
	,	.dbus2sub_dcluster0_port1_r_Ar_Valid( dbus2sub_dcluster0_port1_r_Ar_Valid )
	,	.dbus2sub_dcluster0_port1_r_R_Data( dbus2sub_dcluster0_port1_r_R_Data )
	,	.dbus2sub_dcluster0_port1_r_R_Id( dbus2sub_dcluster0_port1_r_R_Id )
	,	.dbus2sub_dcluster0_port1_r_R_Last( dbus2sub_dcluster0_port1_r_R_Last )
	,	.dbus2sub_dcluster0_port1_r_R_Ready( dbus2sub_dcluster0_port1_r_R_Ready )
	,	.dbus2sub_dcluster0_port1_r_R_Resp( dbus2sub_dcluster0_port1_r_R_Resp )
	,	.dbus2sub_dcluster0_port1_r_R_User( dbus2sub_dcluster0_port1_r_R_User )
	,	.dbus2sub_dcluster0_port1_r_R_Valid( dbus2sub_dcluster0_port1_r_R_Valid )
	,	.dbus2sub_dcluster0_port2_r_Ar_Addr( dbus2sub_dcluster0_port2_r_Ar_Addr )
	,	.dbus2sub_dcluster0_port2_r_Ar_Burst( dbus2sub_dcluster0_port2_r_Ar_Burst )
	,	.dbus2sub_dcluster0_port2_r_Ar_Cache( dbus2sub_dcluster0_port2_r_Ar_Cache )
	,	.dbus2sub_dcluster0_port2_r_Ar_Id( dbus2sub_dcluster0_port2_r_Ar_Id )
	,	.dbus2sub_dcluster0_port2_r_Ar_Len( dbus2sub_dcluster0_port2_r_Ar_Len )
	,	.dbus2sub_dcluster0_port2_r_Ar_Lock( dbus2sub_dcluster0_port2_r_Ar_Lock )
	,	.dbus2sub_dcluster0_port2_r_Ar_Prot( dbus2sub_dcluster0_port2_r_Ar_Prot )
	,	.dbus2sub_dcluster0_port2_r_Ar_Ready( dbus2sub_dcluster0_port2_r_Ar_Ready )
	,	.dbus2sub_dcluster0_port2_r_Ar_Size( dbus2sub_dcluster0_port2_r_Ar_Size )
	,	.dbus2sub_dcluster0_port2_r_Ar_User( dbus2sub_dcluster0_port2_r_Ar_User )
	,	.dbus2sub_dcluster0_port2_r_Ar_Valid( dbus2sub_dcluster0_port2_r_Ar_Valid )
	,	.dbus2sub_dcluster0_port2_r_R_Data( dbus2sub_dcluster0_port2_r_R_Data )
	,	.dbus2sub_dcluster0_port2_r_R_Id( dbus2sub_dcluster0_port2_r_R_Id )
	,	.dbus2sub_dcluster0_port2_r_R_Last( dbus2sub_dcluster0_port2_r_R_Last )
	,	.dbus2sub_dcluster0_port2_r_R_Ready( dbus2sub_dcluster0_port2_r_R_Ready )
	,	.dbus2sub_dcluster0_port2_r_R_Resp( dbus2sub_dcluster0_port2_r_R_Resp )
	,	.dbus2sub_dcluster0_port2_r_R_User( dbus2sub_dcluster0_port2_r_R_User )
	,	.dbus2sub_dcluster0_port2_r_R_Valid( dbus2sub_dcluster0_port2_r_R_Valid )
	,	.dbus2sub_dcluster0_port3_r_Ar_Addr( dbus2sub_dcluster0_port3_r_Ar_Addr )
	,	.dbus2sub_dcluster0_port3_r_Ar_Burst( dbus2sub_dcluster0_port3_r_Ar_Burst )
	,	.dbus2sub_dcluster0_port3_r_Ar_Cache( dbus2sub_dcluster0_port3_r_Ar_Cache )
	,	.dbus2sub_dcluster0_port3_r_Ar_Id( dbus2sub_dcluster0_port3_r_Ar_Id )
	,	.dbus2sub_dcluster0_port3_r_Ar_Len( dbus2sub_dcluster0_port3_r_Ar_Len )
	,	.dbus2sub_dcluster0_port3_r_Ar_Lock( dbus2sub_dcluster0_port3_r_Ar_Lock )
	,	.dbus2sub_dcluster0_port3_r_Ar_Prot( dbus2sub_dcluster0_port3_r_Ar_Prot )
	,	.dbus2sub_dcluster0_port3_r_Ar_Ready( dbus2sub_dcluster0_port3_r_Ar_Ready )
	,	.dbus2sub_dcluster0_port3_r_Ar_Size( dbus2sub_dcluster0_port3_r_Ar_Size )
	,	.dbus2sub_dcluster0_port3_r_Ar_User( dbus2sub_dcluster0_port3_r_Ar_User )
	,	.dbus2sub_dcluster0_port3_r_Ar_Valid( dbus2sub_dcluster0_port3_r_Ar_Valid )
	,	.dbus2sub_dcluster0_port3_r_R_Data( dbus2sub_dcluster0_port3_r_R_Data )
	,	.dbus2sub_dcluster0_port3_r_R_Id( dbus2sub_dcluster0_port3_r_R_Id )
	,	.dbus2sub_dcluster0_port3_r_R_Last( dbus2sub_dcluster0_port3_r_R_Last )
	,	.dbus2sub_dcluster0_port3_r_R_Ready( dbus2sub_dcluster0_port3_r_R_Ready )
	,	.dbus2sub_dcluster0_port3_r_R_Resp( dbus2sub_dcluster0_port3_r_R_Resp )
	,	.dbus2sub_dcluster0_port3_r_R_User( dbus2sub_dcluster0_port3_r_R_User )
	,	.dbus2sub_dcluster0_port3_r_R_Valid( dbus2sub_dcluster0_port3_r_R_Valid )
	,	.dbus2sub_dcluster1_port0_r_Ar_Addr( dbus2sub_dcluster1_port0_r_Ar_Addr )
	,	.dbus2sub_dcluster1_port0_r_Ar_Burst( dbus2sub_dcluster1_port0_r_Ar_Burst )
	,	.dbus2sub_dcluster1_port0_r_Ar_Cache( dbus2sub_dcluster1_port0_r_Ar_Cache )
	,	.dbus2sub_dcluster1_port0_r_Ar_Id( dbus2sub_dcluster1_port0_r_Ar_Id )
	,	.dbus2sub_dcluster1_port0_r_Ar_Len( dbus2sub_dcluster1_port0_r_Ar_Len )
	,	.dbus2sub_dcluster1_port0_r_Ar_Lock( dbus2sub_dcluster1_port0_r_Ar_Lock )
	,	.dbus2sub_dcluster1_port0_r_Ar_Prot( dbus2sub_dcluster1_port0_r_Ar_Prot )
	,	.dbus2sub_dcluster1_port0_r_Ar_Ready( dbus2sub_dcluster1_port0_r_Ar_Ready )
	,	.dbus2sub_dcluster1_port0_r_Ar_Size( dbus2sub_dcluster1_port0_r_Ar_Size )
	,	.dbus2sub_dcluster1_port0_r_Ar_User( dbus2sub_dcluster1_port0_r_Ar_User )
	,	.dbus2sub_dcluster1_port0_r_Ar_Valid( dbus2sub_dcluster1_port0_r_Ar_Valid )
	,	.dbus2sub_dcluster1_port0_r_R_Data( dbus2sub_dcluster1_port0_r_R_Data )
	,	.dbus2sub_dcluster1_port0_r_R_Id( dbus2sub_dcluster1_port0_r_R_Id )
	,	.dbus2sub_dcluster1_port0_r_R_Last( dbus2sub_dcluster1_port0_r_R_Last )
	,	.dbus2sub_dcluster1_port0_r_R_Ready( dbus2sub_dcluster1_port0_r_R_Ready )
	,	.dbus2sub_dcluster1_port0_r_R_Resp( dbus2sub_dcluster1_port0_r_R_Resp )
	,	.dbus2sub_dcluster1_port0_r_R_User( dbus2sub_dcluster1_port0_r_R_User )
	,	.dbus2sub_dcluster1_port0_r_R_Valid( dbus2sub_dcluster1_port0_r_R_Valid )
	,	.dbus2sub_dcluster1_port1_r_Ar_Addr( dbus2sub_dcluster1_port1_r_Ar_Addr )
	,	.dbus2sub_dcluster1_port1_r_Ar_Burst( dbus2sub_dcluster1_port1_r_Ar_Burst )
	,	.dbus2sub_dcluster1_port1_r_Ar_Cache( dbus2sub_dcluster1_port1_r_Ar_Cache )
	,	.dbus2sub_dcluster1_port1_r_Ar_Id( dbus2sub_dcluster1_port1_r_Ar_Id )
	,	.dbus2sub_dcluster1_port1_r_Ar_Len( dbus2sub_dcluster1_port1_r_Ar_Len )
	,	.dbus2sub_dcluster1_port1_r_Ar_Lock( dbus2sub_dcluster1_port1_r_Ar_Lock )
	,	.dbus2sub_dcluster1_port1_r_Ar_Prot( dbus2sub_dcluster1_port1_r_Ar_Prot )
	,	.dbus2sub_dcluster1_port1_r_Ar_Ready( dbus2sub_dcluster1_port1_r_Ar_Ready )
	,	.dbus2sub_dcluster1_port1_r_Ar_Size( dbus2sub_dcluster1_port1_r_Ar_Size )
	,	.dbus2sub_dcluster1_port1_r_Ar_User( dbus2sub_dcluster1_port1_r_Ar_User )
	,	.dbus2sub_dcluster1_port1_r_Ar_Valid( dbus2sub_dcluster1_port1_r_Ar_Valid )
	,	.dbus2sub_dcluster1_port1_r_R_Data( dbus2sub_dcluster1_port1_r_R_Data )
	,	.dbus2sub_dcluster1_port1_r_R_Id( dbus2sub_dcluster1_port1_r_R_Id )
	,	.dbus2sub_dcluster1_port1_r_R_Last( dbus2sub_dcluster1_port1_r_R_Last )
	,	.dbus2sub_dcluster1_port1_r_R_Ready( dbus2sub_dcluster1_port1_r_R_Ready )
	,	.dbus2sub_dcluster1_port1_r_R_Resp( dbus2sub_dcluster1_port1_r_R_Resp )
	,	.dbus2sub_dcluster1_port1_r_R_User( dbus2sub_dcluster1_port1_r_R_User )
	,	.dbus2sub_dcluster1_port1_r_R_Valid( dbus2sub_dcluster1_port1_r_R_Valid )
	,	.dbus2sub_dcluster1_port2_r_Ar_Addr( dbus2sub_dcluster1_port2_r_Ar_Addr )
	,	.dbus2sub_dcluster1_port2_r_Ar_Burst( dbus2sub_dcluster1_port2_r_Ar_Burst )
	,	.dbus2sub_dcluster1_port2_r_Ar_Cache( dbus2sub_dcluster1_port2_r_Ar_Cache )
	,	.dbus2sub_dcluster1_port2_r_Ar_Id( dbus2sub_dcluster1_port2_r_Ar_Id )
	,	.dbus2sub_dcluster1_port2_r_Ar_Len( dbus2sub_dcluster1_port2_r_Ar_Len )
	,	.dbus2sub_dcluster1_port2_r_Ar_Lock( dbus2sub_dcluster1_port2_r_Ar_Lock )
	,	.dbus2sub_dcluster1_port2_r_Ar_Prot( dbus2sub_dcluster1_port2_r_Ar_Prot )
	,	.dbus2sub_dcluster1_port2_r_Ar_Ready( dbus2sub_dcluster1_port2_r_Ar_Ready )
	,	.dbus2sub_dcluster1_port2_r_Ar_Size( dbus2sub_dcluster1_port2_r_Ar_Size )
	,	.dbus2sub_dcluster1_port2_r_Ar_User( dbus2sub_dcluster1_port2_r_Ar_User )
	,	.dbus2sub_dcluster1_port2_r_Ar_Valid( dbus2sub_dcluster1_port2_r_Ar_Valid )
	,	.dbus2sub_dcluster1_port2_r_R_Data( dbus2sub_dcluster1_port2_r_R_Data )
	,	.dbus2sub_dcluster1_port2_r_R_Id( dbus2sub_dcluster1_port2_r_R_Id )
	,	.dbus2sub_dcluster1_port2_r_R_Last( dbus2sub_dcluster1_port2_r_R_Last )
	,	.dbus2sub_dcluster1_port2_r_R_Ready( dbus2sub_dcluster1_port2_r_R_Ready )
	,	.dbus2sub_dcluster1_port2_r_R_Resp( dbus2sub_dcluster1_port2_r_R_Resp )
	,	.dbus2sub_dcluster1_port2_r_R_User( dbus2sub_dcluster1_port2_r_R_User )
	,	.dbus2sub_dcluster1_port2_r_R_Valid( dbus2sub_dcluster1_port2_r_R_Valid )
	,	.dbus2sub_dcluster1_port3_r_Ar_Addr( dbus2sub_dcluster1_port3_r_Ar_Addr )
	,	.dbus2sub_dcluster1_port3_r_Ar_Burst( dbus2sub_dcluster1_port3_r_Ar_Burst )
	,	.dbus2sub_dcluster1_port3_r_Ar_Cache( dbus2sub_dcluster1_port3_r_Ar_Cache )
	,	.dbus2sub_dcluster1_port3_r_Ar_Id( dbus2sub_dcluster1_port3_r_Ar_Id )
	,	.dbus2sub_dcluster1_port3_r_Ar_Len( dbus2sub_dcluster1_port3_r_Ar_Len )
	,	.dbus2sub_dcluster1_port3_r_Ar_Lock( dbus2sub_dcluster1_port3_r_Ar_Lock )
	,	.dbus2sub_dcluster1_port3_r_Ar_Prot( dbus2sub_dcluster1_port3_r_Ar_Prot )
	,	.dbus2sub_dcluster1_port3_r_Ar_Ready( dbus2sub_dcluster1_port3_r_Ar_Ready )
	,	.dbus2sub_dcluster1_port3_r_Ar_Size( dbus2sub_dcluster1_port3_r_Ar_Size )
	,	.dbus2sub_dcluster1_port3_r_Ar_User( dbus2sub_dcluster1_port3_r_Ar_User )
	,	.dbus2sub_dcluster1_port3_r_Ar_Valid( dbus2sub_dcluster1_port3_r_Ar_Valid )
	,	.dbus2sub_dcluster1_port3_r_R_Data( dbus2sub_dcluster1_port3_r_R_Data )
	,	.dbus2sub_dcluster1_port3_r_R_Id( dbus2sub_dcluster1_port3_r_R_Id )
	,	.dbus2sub_dcluster1_port3_r_R_Last( dbus2sub_dcluster1_port3_r_R_Last )
	,	.dbus2sub_dcluster1_port3_r_R_Ready( dbus2sub_dcluster1_port3_r_R_Ready )
	,	.dbus2sub_dcluster1_port3_r_R_Resp( dbus2sub_dcluster1_port3_r_R_Resp )
	,	.dbus2sub_dcluster1_port3_r_R_User( dbus2sub_dcluster1_port3_r_R_User )
	,	.dbus2sub_dcluster1_port3_r_R_Valid( dbus2sub_dcluster1_port3_r_R_Valid )
	,	.dbus2sub_ddma_r_Ar_Addr( dbus2sub_ddma_r_Ar_Addr )
	,	.dbus2sub_ddma_r_Ar_Burst( dbus2sub_ddma_r_Ar_Burst )
	,	.dbus2sub_ddma_r_Ar_Cache( dbus2sub_ddma_r_Ar_Cache )
	,	.dbus2sub_ddma_r_Ar_Id( dbus2sub_ddma_r_Ar_Id )
	,	.dbus2sub_ddma_r_Ar_Len( dbus2sub_ddma_r_Ar_Len )
	,	.dbus2sub_ddma_r_Ar_Lock( dbus2sub_ddma_r_Ar_Lock )
	,	.dbus2sub_ddma_r_Ar_Prot( dbus2sub_ddma_r_Ar_Prot )
	,	.dbus2sub_ddma_r_Ar_Ready( dbus2sub_ddma_r_Ar_Ready )
	,	.dbus2sub_ddma_r_Ar_Size( dbus2sub_ddma_r_Ar_Size )
	,	.dbus2sub_ddma_r_Ar_User( dbus2sub_ddma_r_Ar_User )
	,	.dbus2sub_ddma_r_Ar_Valid( dbus2sub_ddma_r_Ar_Valid )
	,	.dbus2sub_ddma_r_R_Data( dbus2sub_ddma_r_R_Data )
	,	.dbus2sub_ddma_r_R_Id( dbus2sub_ddma_r_R_Id )
	,	.dbus2sub_ddma_r_R_Last( dbus2sub_ddma_r_R_Last )
	,	.dbus2sub_ddma_r_R_Ready( dbus2sub_ddma_r_R_Ready )
	,	.dbus2sub_ddma_r_R_Resp( dbus2sub_ddma_r_R_Resp )
	,	.dbus2sub_ddma_r_R_User( dbus2sub_ddma_r_R_User )
	,	.dbus2sub_ddma_r_R_Valid( dbus2sub_ddma_r_R_Valid )
	,	.dbus2sub_pcie_cpu_r_Ar_Addr( dbus2sub_pcie_cpu_r_Ar_Addr )
	,	.dbus2sub_pcie_cpu_r_Ar_Burst( dbus2sub_pcie_cpu_r_Ar_Burst )
	,	.dbus2sub_pcie_cpu_r_Ar_Cache( dbus2sub_pcie_cpu_r_Ar_Cache )
	,	.dbus2sub_pcie_cpu_r_Ar_Id( dbus2sub_pcie_cpu_r_Ar_Id )
	,	.dbus2sub_pcie_cpu_r_Ar_Len( dbus2sub_pcie_cpu_r_Ar_Len )
	,	.dbus2sub_pcie_cpu_r_Ar_Lock( dbus2sub_pcie_cpu_r_Ar_Lock )
	,	.dbus2sub_pcie_cpu_r_Ar_Prot( dbus2sub_pcie_cpu_r_Ar_Prot )
	,	.dbus2sub_pcie_cpu_r_Ar_Ready( dbus2sub_pcie_cpu_r_Ar_Ready )
	,	.dbus2sub_pcie_cpu_r_Ar_Size( dbus2sub_pcie_cpu_r_Ar_Size )
	,	.dbus2sub_pcie_cpu_r_Ar_User( dbus2sub_pcie_cpu_r_Ar_User )
	,	.dbus2sub_pcie_cpu_r_Ar_Valid( dbus2sub_pcie_cpu_r_Ar_Valid )
	,	.dbus2sub_pcie_cpu_r_R_Data( dbus2sub_pcie_cpu_r_R_Data )
	,	.dbus2sub_pcie_cpu_r_R_Id( dbus2sub_pcie_cpu_r_R_Id )
	,	.dbus2sub_pcie_cpu_r_R_Last( dbus2sub_pcie_cpu_r_R_Last )
	,	.dbus2sub_pcie_cpu_r_R_Ready( dbus2sub_pcie_cpu_r_R_Ready )
	,	.dbus2sub_pcie_cpu_r_R_Resp( dbus2sub_pcie_cpu_r_R_Resp )
	,	.dbus2sub_pcie_cpu_r_R_User( dbus2sub_pcie_cpu_r_R_User )
	,	.dbus2sub_pcie_cpu_r_R_Valid( dbus2sub_pcie_cpu_r_R_Valid )
	,	.ddma_r_Ar_Addr( ddma_r_Ar_Addr )
	,	.ddma_r_Ar_Burst( ddma_r_Ar_Burst )
	,	.ddma_r_Ar_Cache( ddma_r_Ar_Cache )
	,	.ddma_r_Ar_Id( ddma_r_Ar_Id )
	,	.ddma_r_Ar_Len( ddma_r_Ar_Len )
	,	.ddma_r_Ar_Lock( ddma_r_Ar_Lock )
	,	.ddma_r_Ar_Prot( ddma_r_Ar_Prot )
	,	.ddma_r_Ar_Ready( ddma_r_Ar_Ready )
	,	.ddma_r_Ar_Size( ddma_r_Ar_Size )
	,	.ddma_r_Ar_User( ddma_r_Ar_User )
	,	.ddma_r_Ar_Valid( ddma_r_Ar_Valid )
	,	.ddma_r_R_Data( ddma_r_R_Data )
	,	.ddma_r_R_Id( ddma_r_R_Id )
	,	.ddma_r_R_Last( ddma_r_R_Last )
	,	.ddma_r_R_Ready( ddma_r_R_Ready )
	,	.ddma_r_R_Resp( ddma_r_R_Resp )
	,	.ddma_r_R_User( ddma_r_R_User )
	,	.ddma_r_R_Valid( ddma_r_R_Valid )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld )
	,	.dp_Link_c01_m01_e_to_Switch_west_Data( r_dp_Link_c01_m01_e_to_Switch_west_Data )
	,	.dp_Link_c01_m01_e_to_Switch_west_Head( r_dp_Link_c01_m01_e_to_Switch_west_Head )
	,	.dp_Link_c01_m01_e_to_Switch_west_Rdy( r_dp_Link_c01_m01_e_to_Switch_west_Rdy )
	,	.dp_Link_c01_m01_e_to_Switch_west_Tail( r_dp_Link_c01_m01_e_to_Switch_west_Tail )
	,	.dp_Link_c01_m01_e_to_Switch_west_Vld( r_dp_Link_c01_m01_e_to_Switch_west_Vld )
	,	.dp_Link_n03_m01_c_to_Switch_west_Data( r_dp_Link_n03_m01_c_to_Switch_west_Data )
	,	.dp_Link_n03_m01_c_to_Switch_west_Head( r_dp_Link_n03_m01_c_to_Switch_west_Head )
	,	.dp_Link_n03_m01_c_to_Switch_west_Rdy( r_dp_Link_n03_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n03_m01_c_to_Switch_west_Tail( r_dp_Link_n03_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n03_m01_c_to_Switch_west_Vld( r_dp_Link_n03_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n03_m23_c_to_Switch_east_Data( r_dp_Link_n03_m23_c_to_Switch_east_Data )
	,	.dp_Link_n03_m23_c_to_Switch_east_Head( r_dp_Link_n03_m23_c_to_Switch_east_Head )
	,	.dp_Link_n03_m23_c_to_Switch_east_Rdy( r_dp_Link_n03_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n03_m23_c_to_Switch_east_Tail( r_dp_Link_n03_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n03_m23_c_to_Switch_east_Vld( r_dp_Link_n03_m23_c_to_Switch_east_Vld )
	,	.dp_Link_n0_asi_to_Link_n0_ast_Data( r_dp_Link_n0_asi_to_Link_n0_ast_Data )
	,	.dp_Link_n0_asi_to_Link_n0_ast_RdCnt( r_dp_Link_n0_asi_to_Link_n0_ast_RdCnt )
	,	.dp_Link_n0_asi_to_Link_n0_ast_RdPtr( r_dp_Link_n0_asi_to_Link_n0_ast_RdPtr )
	,	.dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst( r_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck( r_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst( r_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck( r_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n0_asi_to_Link_n0_ast_WrCnt( r_dp_Link_n0_asi_to_Link_n0_ast_WrCnt )
	,	.dp_Link_n0_astResp001_to_Link_n0_asiResp001_Data( r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_Data )
	,	.dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdCnt( r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdCnt )
	,	.dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdPtr( r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdPtr )
	,	.dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRst( r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRst( r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n0_astResp001_to_Link_n0_asiResp001_WrCnt( r_dp_Link_n0_astResp001_to_Link_n0_asiResp001_WrCnt )
	,	.dp_Link_n1_asi_to_Link_n1_ast_Data( r_dp_Link_n1_asi_to_Link_n1_ast_Data )
	,	.dp_Link_n1_asi_to_Link_n1_ast_RdCnt( r_dp_Link_n1_asi_to_Link_n1_ast_RdCnt )
	,	.dp_Link_n1_asi_to_Link_n1_ast_RdPtr( r_dp_Link_n1_asi_to_Link_n1_ast_RdPtr )
	,	.dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst( r_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck( r_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst( r_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck( r_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n1_asi_to_Link_n1_ast_WrCnt( r_dp_Link_n1_asi_to_Link_n1_ast_WrCnt )
	,	.dp_Link_n1_astResp001_to_Link_n1_asiResp001_Data( r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_Data )
	,	.dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdCnt( r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdCnt )
	,	.dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdPtr( r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdPtr )
	,	.dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRst( r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRst( r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n1_astResp001_to_Link_n1_asiResp001_WrCnt( r_dp_Link_n1_astResp001_to_Link_n1_asiResp001_WrCnt )
	,	.dp_Link_n2_asi_to_Link_n2_ast_Data( r_dp_Link_n2_asi_to_Link_n2_ast_Data )
	,	.dp_Link_n2_asi_to_Link_n2_ast_RdCnt( r_dp_Link_n2_asi_to_Link_n2_ast_RdCnt )
	,	.dp_Link_n2_asi_to_Link_n2_ast_RdPtr( r_dp_Link_n2_asi_to_Link_n2_ast_RdPtr )
	,	.dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst( r_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck( r_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst( r_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck( r_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n2_asi_to_Link_n2_ast_WrCnt( r_dp_Link_n2_asi_to_Link_n2_ast_WrCnt )
	,	.dp_Link_n2_astResp001_to_Link_n2_asiResp001_Data( r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_Data )
	,	.dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdCnt( r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdCnt )
	,	.dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdPtr( r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdPtr )
	,	.dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRst( r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRst( r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n2_astResp001_to_Link_n2_asiResp001_WrCnt( r_dp_Link_n2_astResp001_to_Link_n2_asiResp001_WrCnt )
	,	.dp_Link_n3_asi_to_Link_n3_ast_Data( r_dp_Link_n3_asi_to_Link_n3_ast_Data )
	,	.dp_Link_n3_asi_to_Link_n3_ast_RdCnt( r_dp_Link_n3_asi_to_Link_n3_ast_RdCnt )
	,	.dp_Link_n3_asi_to_Link_n3_ast_RdPtr( r_dp_Link_n3_asi_to_Link_n3_ast_RdPtr )
	,	.dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst( r_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck( r_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst( r_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck( r_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n3_asi_to_Link_n3_ast_WrCnt( r_dp_Link_n3_asi_to_Link_n3_ast_WrCnt )
	,	.dp_Link_n3_astResp001_to_Link_n3_asiResp001_Data( r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_Data )
	,	.dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdCnt( r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdCnt )
	,	.dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdPtr( r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdPtr )
	,	.dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRst( r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRst( r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n3_astResp001_to_Link_n3_asiResp001_WrCnt( r_dp_Link_n3_astResp001_to_Link_n3_asiResp001_WrCnt )
	,	.dp_Link_n47_m01_c_to_Switch_west_Data( r_dp_Link_n47_m01_c_to_Switch_west_Data )
	,	.dp_Link_n47_m01_c_to_Switch_west_Head( r_dp_Link_n47_m01_c_to_Switch_west_Head )
	,	.dp_Link_n47_m01_c_to_Switch_west_Rdy( r_dp_Link_n47_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n47_m01_c_to_Switch_west_Tail( r_dp_Link_n47_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n47_m01_c_to_Switch_west_Vld( r_dp_Link_n47_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n47_m23_c_to_Switch_east_Data( r_dp_Link_n47_m23_c_to_Switch_east_Data )
	,	.dp_Link_n47_m23_c_to_Switch_east_Head( r_dp_Link_n47_m23_c_to_Switch_east_Head )
	,	.dp_Link_n47_m23_c_to_Switch_east_Rdy( r_dp_Link_n47_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n47_m23_c_to_Switch_east_Tail( r_dp_Link_n47_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n47_m23_c_to_Switch_east_Vld( r_dp_Link_n47_m23_c_to_Switch_east_Vld )
	,	.dp_Link_n4_asi_to_Link_n4_ast_Data( r_dp_Link_n4_asi_to_Link_n4_ast_Data )
	,	.dp_Link_n4_asi_to_Link_n4_ast_RdCnt( r_dp_Link_n4_asi_to_Link_n4_ast_RdCnt )
	,	.dp_Link_n4_asi_to_Link_n4_ast_RdPtr( r_dp_Link_n4_asi_to_Link_n4_ast_RdPtr )
	,	.dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst( r_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck( r_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst( r_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck( r_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n4_asi_to_Link_n4_ast_WrCnt( r_dp_Link_n4_asi_to_Link_n4_ast_WrCnt )
	,	.dp_Link_n4_astResp001_to_Link_n4_asiResp001_Data( r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_Data )
	,	.dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdCnt( r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdCnt )
	,	.dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdPtr( r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdPtr )
	,	.dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRst( r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRst( r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n4_astResp001_to_Link_n4_asiResp001_WrCnt( r_dp_Link_n4_astResp001_to_Link_n4_asiResp001_WrCnt )
	,	.dp_Link_n5_asi_to_Link_n5_ast_Data( r_dp_Link_n5_asi_to_Link_n5_ast_Data )
	,	.dp_Link_n5_asi_to_Link_n5_ast_RdCnt( r_dp_Link_n5_asi_to_Link_n5_ast_RdCnt )
	,	.dp_Link_n5_asi_to_Link_n5_ast_RdPtr( r_dp_Link_n5_asi_to_Link_n5_ast_RdPtr )
	,	.dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst( r_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck( r_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst( r_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck( r_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n5_asi_to_Link_n5_ast_WrCnt( r_dp_Link_n5_asi_to_Link_n5_ast_WrCnt )
	,	.dp_Link_n5_astResp001_to_Link_n5_asiResp001_Data( r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_Data )
	,	.dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdCnt( r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdCnt )
	,	.dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdPtr( r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdPtr )
	,	.dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRst( r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRst( r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n5_astResp001_to_Link_n5_asiResp001_WrCnt( r_dp_Link_n5_astResp001_to_Link_n5_asiResp001_WrCnt )
	,	.dp_Link_n6_asi_to_Link_n6_ast_Data( r_dp_Link_n6_asi_to_Link_n6_ast_Data )
	,	.dp_Link_n6_asi_to_Link_n6_ast_RdCnt( r_dp_Link_n6_asi_to_Link_n6_ast_RdCnt )
	,	.dp_Link_n6_asi_to_Link_n6_ast_RdPtr( r_dp_Link_n6_asi_to_Link_n6_ast_RdPtr )
	,	.dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst( r_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck( r_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst( r_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck( r_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n6_asi_to_Link_n6_ast_WrCnt( r_dp_Link_n6_asi_to_Link_n6_ast_WrCnt )
	,	.dp_Link_n6_astResp001_to_Link_n6_asiResp001_Data( r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_Data )
	,	.dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdCnt( r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdCnt )
	,	.dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdPtr( r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdPtr )
	,	.dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRst( r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRst( r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n6_astResp001_to_Link_n6_asiResp001_WrCnt( r_dp_Link_n6_astResp001_to_Link_n6_asiResp001_WrCnt )
	,	.dp_Link_n7_asi_to_Link_n7_ast_Data( r_dp_Link_n7_asi_to_Link_n7_ast_Data )
	,	.dp_Link_n7_asi_to_Link_n7_ast_RdCnt( r_dp_Link_n7_asi_to_Link_n7_ast_RdCnt )
	,	.dp_Link_n7_asi_to_Link_n7_ast_RdPtr( r_dp_Link_n7_asi_to_Link_n7_ast_RdPtr )
	,	.dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst( r_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck( r_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst( r_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck( r_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n7_asi_to_Link_n7_ast_WrCnt( r_dp_Link_n7_asi_to_Link_n7_ast_WrCnt )
	,	.dp_Link_n7_astResp001_to_Link_n7_asiResp001_Data( r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_Data )
	,	.dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdCnt( r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdCnt )
	,	.dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdPtr( r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdPtr )
	,	.dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRst( r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRst( r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n7_astResp001_to_Link_n7_asiResp001_WrCnt( r_dp_Link_n7_astResp001_to_Link_n7_asiResp001_WrCnt )
	,	.dp_Link_n8_m01_c_to_Switch_west_Data( r_dp_Link_n8_m01_c_to_Switch_west_Data )
	,	.dp_Link_n8_m01_c_to_Switch_west_Head( r_dp_Link_n8_m01_c_to_Switch_west_Head )
	,	.dp_Link_n8_m01_c_to_Switch_west_Rdy( r_dp_Link_n8_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n8_m01_c_to_Switch_west_Tail( r_dp_Link_n8_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n8_m01_c_to_Switch_west_Vld( r_dp_Link_n8_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n8_m23_c_to_Switch_east_Data( r_dp_Link_n8_m23_c_to_Switch_east_Data )
	,	.dp_Link_n8_m23_c_to_Switch_east_Head( r_dp_Link_n8_m23_c_to_Switch_east_Head )
	,	.dp_Link_n8_m23_c_to_Switch_east_Rdy( r_dp_Link_n8_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n8_m23_c_to_Switch_east_Tail( r_dp_Link_n8_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n8_m23_c_to_Switch_east_Vld( r_dp_Link_n8_m23_c_to_Switch_east_Vld )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Data( r_dp_Link_sc01Resp_to_Switch_eastResp001_Data )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Head( r_dp_Link_sc01Resp_to_Switch_eastResp001_Head )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Rdy( r_dp_Link_sc01Resp_to_Switch_eastResp001_Rdy )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Tail( r_dp_Link_sc01Resp_to_Switch_eastResp001_Tail )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Vld( r_dp_Link_sc01Resp_to_Switch_eastResp001_Vld )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld )
	,	.dp_Switch_east_to_Link_c01_m01_a_Data( r_dp_Switch_east_to_Link_c01_m01_a_Data )
	,	.dp_Switch_east_to_Link_c01_m01_a_Head( r_dp_Switch_east_to_Link_c01_m01_a_Head )
	,	.dp_Switch_east_to_Link_c01_m01_a_Rdy( r_dp_Switch_east_to_Link_c01_m01_a_Rdy )
	,	.dp_Switch_east_to_Link_c01_m01_a_Tail( r_dp_Switch_east_to_Link_c01_m01_a_Tail )
	,	.dp_Switch_east_to_Link_c01_m01_a_Vld( r_dp_Switch_east_to_Link_c01_m01_a_Vld )
	,	.dp_Switch_east_to_Link_sc01_Data( r_dp_Switch_east_to_Link_sc01_Data )
	,	.dp_Switch_east_to_Link_sc01_Head( r_dp_Switch_east_to_Link_sc01_Head )
	,	.dp_Switch_east_to_Link_sc01_Rdy( r_dp_Switch_east_to_Link_sc01_Rdy )
	,	.dp_Switch_east_to_Link_sc01_Tail( r_dp_Switch_east_to_Link_sc01_Tail )
	,	.dp_Switch_east_to_Link_sc01_Vld( r_dp_Switch_east_to_Link_sc01_Vld )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld( r_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld( r_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld( r_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld( r_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld )
	);


	dbus_read_Structure_Module_eastbus u_dbus_read_Structure_Module_eastbus(
		.TM( TM )
	,	.arstn_dbus( arstn_dbus )
	,	.cbus2dbus_dbg_r_Ar_Addr( cbus2dbus_dbg_r_Ar_Addr )
	,	.cbus2dbus_dbg_r_Ar_Burst( cbus2dbus_dbg_r_Ar_Burst )
	,	.cbus2dbus_dbg_r_Ar_Cache( cbus2dbus_dbg_r_Ar_Cache )
	,	.cbus2dbus_dbg_r_Ar_Id( cbus2dbus_dbg_r_Ar_Id )
	,	.cbus2dbus_dbg_r_Ar_Len( cbus2dbus_dbg_r_Ar_Len )
	,	.cbus2dbus_dbg_r_Ar_Lock( cbus2dbus_dbg_r_Ar_Lock )
	,	.cbus2dbus_dbg_r_Ar_Prot( cbus2dbus_dbg_r_Ar_Prot )
	,	.cbus2dbus_dbg_r_Ar_Ready( cbus2dbus_dbg_r_Ar_Ready )
	,	.cbus2dbus_dbg_r_Ar_Size( cbus2dbus_dbg_r_Ar_Size )
	,	.cbus2dbus_dbg_r_Ar_Valid( cbus2dbus_dbg_r_Ar_Valid )
	,	.cbus2dbus_dbg_r_R_Data( cbus2dbus_dbg_r_R_Data )
	,	.cbus2dbus_dbg_r_R_Id( cbus2dbus_dbg_r_R_Id )
	,	.cbus2dbus_dbg_r_R_Last( cbus2dbus_dbg_r_R_Last )
	,	.cbus2dbus_dbg_r_R_Ready( cbus2dbus_dbg_r_R_Ready )
	,	.cbus2dbus_dbg_r_R_Resp( cbus2dbus_dbg_r_R_Resp )
	,	.cbus2dbus_dbg_r_R_Valid( cbus2dbus_dbg_r_R_Valid )
	,	.clk_dbus( clk_dbus )
	,	.dbus2cbus_cpu_r_Ar_Addr( dbus2cbus_cpu_r_Ar_Addr )
	,	.dbus2cbus_cpu_r_Ar_Burst( dbus2cbus_cpu_r_Ar_Burst )
	,	.dbus2cbus_cpu_r_Ar_Cache( dbus2cbus_cpu_r_Ar_Cache )
	,	.dbus2cbus_cpu_r_Ar_Id( dbus2cbus_cpu_r_Ar_Id )
	,	.dbus2cbus_cpu_r_Ar_Len( dbus2cbus_cpu_r_Ar_Len )
	,	.dbus2cbus_cpu_r_Ar_Lock( dbus2cbus_cpu_r_Ar_Lock )
	,	.dbus2cbus_cpu_r_Ar_Prot( dbus2cbus_cpu_r_Ar_Prot )
	,	.dbus2cbus_cpu_r_Ar_Ready( dbus2cbus_cpu_r_Ar_Ready )
	,	.dbus2cbus_cpu_r_Ar_Size( dbus2cbus_cpu_r_Ar_Size )
	,	.dbus2cbus_cpu_r_Ar_User( dbus2cbus_cpu_r_Ar_User )
	,	.dbus2cbus_cpu_r_Ar_Valid( dbus2cbus_cpu_r_Ar_Valid )
	,	.dbus2cbus_cpu_r_R_Data( dbus2cbus_cpu_r_R_Data )
	,	.dbus2cbus_cpu_r_R_Id( dbus2cbus_cpu_r_R_Id )
	,	.dbus2cbus_cpu_r_R_Last( dbus2cbus_cpu_r_R_Last )
	,	.dbus2cbus_cpu_r_R_Ready( dbus2cbus_cpu_r_R_Ready )
	,	.dbus2cbus_cpu_r_R_Resp( dbus2cbus_cpu_r_R_Resp )
	,	.dbus2cbus_cpu_r_R_Valid( dbus2cbus_cpu_r_R_Valid )
	,	.dbus2cbus_pcie_r_Ar_Addr( dbus2cbus_pcie_r_Ar_Addr )
	,	.dbus2cbus_pcie_r_Ar_Burst( dbus2cbus_pcie_r_Ar_Burst )
	,	.dbus2cbus_pcie_r_Ar_Cache( dbus2cbus_pcie_r_Ar_Cache )
	,	.dbus2cbus_pcie_r_Ar_Id( dbus2cbus_pcie_r_Ar_Id )
	,	.dbus2cbus_pcie_r_Ar_Len( dbus2cbus_pcie_r_Ar_Len )
	,	.dbus2cbus_pcie_r_Ar_Lock( dbus2cbus_pcie_r_Ar_Lock )
	,	.dbus2cbus_pcie_r_Ar_Prot( dbus2cbus_pcie_r_Ar_Prot )
	,	.dbus2cbus_pcie_r_Ar_Ready( dbus2cbus_pcie_r_Ar_Ready )
	,	.dbus2cbus_pcie_r_Ar_Size( dbus2cbus_pcie_r_Ar_Size )
	,	.dbus2cbus_pcie_r_Ar_Valid( dbus2cbus_pcie_r_Ar_Valid )
	,	.dbus2cbus_pcie_r_R_Data( dbus2cbus_pcie_r_R_Data )
	,	.dbus2cbus_pcie_r_R_Id( dbus2cbus_pcie_r_R_Id )
	,	.dbus2cbus_pcie_r_R_Last( dbus2cbus_pcie_r_R_Last )
	,	.dbus2cbus_pcie_r_R_Ready( dbus2cbus_pcie_r_R_Ready )
	,	.dbus2cbus_pcie_r_R_Resp( dbus2cbus_pcie_r_R_Resp )
	,	.dbus2cbus_pcie_r_R_Valid( dbus2cbus_pcie_r_R_Valid )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld( r_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Data( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Data )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Head( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Head )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Data( r_dp_Link_c0s_b_to_Link_c0e_a_Data )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Head( r_dp_Link_c0s_b_to_Link_c0e_a_Head )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Rdy( r_dp_Link_c0s_b_to_Link_c0e_a_Rdy )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Tail( r_dp_Link_c0s_b_to_Link_c0e_a_Tail )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Vld( r_dp_Link_c0s_b_to_Link_c0e_a_Vld )
	,	.dp_Link_c1_asi_to_Link_c1_ast_Data( r_dp_Link_c1_asi_to_Link_c1_ast_Data )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RdCnt( r_dp_Link_c1_asi_to_Link_c1_ast_RdCnt )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RdPtr( r_dp_Link_c1_asi_to_Link_c1_ast_RdPtr )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst( r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck( r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst( r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck( r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1_asi_to_Link_c1_ast_WrCnt( r_dp_Link_c1_asi_to_Link_c1_ast_WrCnt )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_Data( r_dp_Link_c1t_asi_to_Link_c1t_ast_Data )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt( r_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr( r_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst( r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck( r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst( r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck( r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt( r_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt )
	,	.dp_Link_m2_asi_to_Link_m2_ast_Data( r_dp_Link_m2_asi_to_Link_m2_ast_Data )
	,	.dp_Link_m2_asi_to_Link_m2_ast_RdCnt( r_dp_Link_m2_asi_to_Link_m2_ast_RdCnt )
	,	.dp_Link_m2_asi_to_Link_m2_ast_RdPtr( r_dp_Link_m2_asi_to_Link_m2_ast_RdPtr )
	,	.dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst( r_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck( r_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst( r_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck( r_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m2_asi_to_Link_m2_ast_WrCnt( r_dp_Link_m2_asi_to_Link_m2_ast_WrCnt )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data( r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt( r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr( r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst( r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst( r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt( r_dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt )
	,	.dp_Link_m3_asi_to_Link_m3_ast_Data( r_dp_Link_m3_asi_to_Link_m3_ast_Data )
	,	.dp_Link_m3_asi_to_Link_m3_ast_RdCnt( r_dp_Link_m3_asi_to_Link_m3_ast_RdCnt )
	,	.dp_Link_m3_asi_to_Link_m3_ast_RdPtr( r_dp_Link_m3_asi_to_Link_m3_ast_RdPtr )
	,	.dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst( r_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck( r_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst( r_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck( r_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m3_asi_to_Link_m3_ast_WrCnt( r_dp_Link_m3_asi_to_Link_m3_ast_WrCnt )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data( r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt( r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr( r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst( r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst( r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt( r_dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt )
	,	.dp_Link_n03_m23_c_to_Switch_east_Data( r_dp_Link_n03_m23_c_to_Switch_east_Data )
	,	.dp_Link_n03_m23_c_to_Switch_east_Head( r_dp_Link_n03_m23_c_to_Switch_east_Head )
	,	.dp_Link_n03_m23_c_to_Switch_east_Rdy( r_dp_Link_n03_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n03_m23_c_to_Switch_east_Tail( r_dp_Link_n03_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n03_m23_c_to_Switch_east_Vld( r_dp_Link_n03_m23_c_to_Switch_east_Vld )
	,	.dp_Link_n47_m23_c_to_Switch_east_Data( r_dp_Link_n47_m23_c_to_Switch_east_Data )
	,	.dp_Link_n47_m23_c_to_Switch_east_Head( r_dp_Link_n47_m23_c_to_Switch_east_Head )
	,	.dp_Link_n47_m23_c_to_Switch_east_Rdy( r_dp_Link_n47_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n47_m23_c_to_Switch_east_Tail( r_dp_Link_n47_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n47_m23_c_to_Switch_east_Vld( r_dp_Link_n47_m23_c_to_Switch_east_Vld )
	,	.dp_Link_n8_m23_c_to_Switch_east_Data( r_dp_Link_n8_m23_c_to_Switch_east_Data )
	,	.dp_Link_n8_m23_c_to_Switch_east_Head( r_dp_Link_n8_m23_c_to_Switch_east_Head )
	,	.dp_Link_n8_m23_c_to_Switch_east_Rdy( r_dp_Link_n8_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n8_m23_c_to_Switch_east_Tail( r_dp_Link_n8_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n8_m23_c_to_Switch_east_Vld( r_dp_Link_n8_m23_c_to_Switch_east_Vld )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Data( r_dp_Link_sc01Resp_to_Switch_eastResp001_Data )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Head( r_dp_Link_sc01Resp_to_Switch_eastResp001_Head )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Rdy( r_dp_Link_sc01Resp_to_Switch_eastResp001_Rdy )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Tail( r_dp_Link_sc01Resp_to_Switch_eastResp001_Tail )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Vld( r_dp_Link_sc01Resp_to_Switch_eastResp001_Vld )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld( r_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld( r_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld( r_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld )
	,	.dp_Switch_east_to_Link_c01_m01_a_Data( r_dp_Switch_east_to_Link_c01_m01_a_Data )
	,	.dp_Switch_east_to_Link_c01_m01_a_Head( r_dp_Switch_east_to_Link_c01_m01_a_Head )
	,	.dp_Switch_east_to_Link_c01_m01_a_Rdy( r_dp_Switch_east_to_Link_c01_m01_a_Rdy )
	,	.dp_Switch_east_to_Link_c01_m01_a_Tail( r_dp_Switch_east_to_Link_c01_m01_a_Tail )
	,	.dp_Switch_east_to_Link_c01_m01_a_Vld( r_dp_Switch_east_to_Link_c01_m01_a_Vld )
	,	.dp_Switch_east_to_Link_sc01_Data( r_dp_Switch_east_to_Link_sc01_Data )
	,	.dp_Switch_east_to_Link_sc01_Head( r_dp_Switch_east_to_Link_sc01_Head )
	,	.dp_Switch_east_to_Link_sc01_Rdy( r_dp_Switch_east_to_Link_sc01_Rdy )
	,	.dp_Switch_east_to_Link_sc01_Tail( r_dp_Switch_east_to_Link_sc01_Tail )
	,	.dp_Switch_east_to_Link_sc01_Vld( r_dp_Switch_east_to_Link_sc01_Vld )
	);


	dbus_read_Structure_Module_southbus u_dbus_read_Structure_Module_southbus(
		.TM( TM )
	,	.arstn_dbus( arstn_dbus )
	,	.clk_dbus( clk_dbus )
	,	.dp_Link_c0_asi_to_Link_c0_ast_Data( r_dp_Link_c0_asi_to_Link_c0_ast_Data )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdCnt( r_dp_Link_c0_asi_to_Link_c0_ast_RdCnt )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdPtr( r_dp_Link_c0_asi_to_Link_c0_ast_RdPtr )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst( r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck( r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst( r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck( r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_WrCnt( r_dp_Link_c0_asi_to_Link_c0_ast_WrCnt )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_Data( r_dp_Link_c0_astResp_to_Link_c0_asiResp_Data )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt( r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr( r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst( r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck( r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst( r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck( r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt( r_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Data( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Data )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Head( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Head )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld( r_dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Data( r_dp_Link_c0s_b_to_Link_c0e_a_Data )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Head( r_dp_Link_c0s_b_to_Link_c0e_a_Head )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Rdy( r_dp_Link_c0s_b_to_Link_c0e_a_Rdy )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Tail( r_dp_Link_c0s_b_to_Link_c0e_a_Tail )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Vld( r_dp_Link_c0s_b_to_Link_c0e_a_Vld )
	);


	dbus_sub_read_Structure i_dbus_sub_read_Structure(
		.TM( TM )
	,	.arstn_sub( arstn_dbus )
	,	.clk_sub( clk_dbus )
	,	.dbus2sub_dcluster0_port0_r_Ar_Addr( dbus2sub_dcluster0_port0_r_Ar_Addr )
	,	.dbus2sub_dcluster0_port0_r_Ar_Burst( dbus2sub_dcluster0_port0_r_Ar_Burst )
	,	.dbus2sub_dcluster0_port0_r_Ar_Cache( dbus2sub_dcluster0_port0_r_Ar_Cache )
	,	.dbus2sub_dcluster0_port0_r_Ar_Id( dbus2sub_dcluster0_port0_r_Ar_Id )
	,	.dbus2sub_dcluster0_port0_r_Ar_Len( dbus2sub_dcluster0_port0_r_Ar_Len )
	,	.dbus2sub_dcluster0_port0_r_Ar_Lock( dbus2sub_dcluster0_port0_r_Ar_Lock )
	,	.dbus2sub_dcluster0_port0_r_Ar_Prot( dbus2sub_dcluster0_port0_r_Ar_Prot )
	,	.dbus2sub_dcluster0_port0_r_Ar_Ready( dbus2sub_dcluster0_port0_r_Ar_Ready )
	,	.dbus2sub_dcluster0_port0_r_Ar_Size( dbus2sub_dcluster0_port0_r_Ar_Size )
	,	.dbus2sub_dcluster0_port0_r_Ar_User( dbus2sub_dcluster0_port0_r_Ar_User )
	,	.dbus2sub_dcluster0_port0_r_Ar_Valid( dbus2sub_dcluster0_port0_r_Ar_Valid )
	,	.dbus2sub_dcluster0_port0_r_R_Data( dbus2sub_dcluster0_port0_r_R_Data )
	,	.dbus2sub_dcluster0_port0_r_R_Id( dbus2sub_dcluster0_port0_r_R_Id )
	,	.dbus2sub_dcluster0_port0_r_R_Last( dbus2sub_dcluster0_port0_r_R_Last )
	,	.dbus2sub_dcluster0_port0_r_R_Ready( dbus2sub_dcluster0_port0_r_R_Ready )
	,	.dbus2sub_dcluster0_port0_r_R_Resp( dbus2sub_dcluster0_port0_r_R_Resp )
	,	.dbus2sub_dcluster0_port0_r_R_User( dbus2sub_dcluster0_port0_r_R_User )
	,	.dbus2sub_dcluster0_port0_r_R_Valid( dbus2sub_dcluster0_port0_r_R_Valid )
	,	.dbus2sub_dcluster0_port1_r_Ar_Addr( dbus2sub_dcluster0_port1_r_Ar_Addr )
	,	.dbus2sub_dcluster0_port1_r_Ar_Burst( dbus2sub_dcluster0_port1_r_Ar_Burst )
	,	.dbus2sub_dcluster0_port1_r_Ar_Cache( dbus2sub_dcluster0_port1_r_Ar_Cache )
	,	.dbus2sub_dcluster0_port1_r_Ar_Id( dbus2sub_dcluster0_port1_r_Ar_Id )
	,	.dbus2sub_dcluster0_port1_r_Ar_Len( dbus2sub_dcluster0_port1_r_Ar_Len )
	,	.dbus2sub_dcluster0_port1_r_Ar_Lock( dbus2sub_dcluster0_port1_r_Ar_Lock )
	,	.dbus2sub_dcluster0_port1_r_Ar_Prot( dbus2sub_dcluster0_port1_r_Ar_Prot )
	,	.dbus2sub_dcluster0_port1_r_Ar_Ready( dbus2sub_dcluster0_port1_r_Ar_Ready )
	,	.dbus2sub_dcluster0_port1_r_Ar_Size( dbus2sub_dcluster0_port1_r_Ar_Size )
	,	.dbus2sub_dcluster0_port1_r_Ar_User( dbus2sub_dcluster0_port1_r_Ar_User )
	,	.dbus2sub_dcluster0_port1_r_Ar_Valid( dbus2sub_dcluster0_port1_r_Ar_Valid )
	,	.dbus2sub_dcluster0_port1_r_R_Data( dbus2sub_dcluster0_port1_r_R_Data )
	,	.dbus2sub_dcluster0_port1_r_R_Id( dbus2sub_dcluster0_port1_r_R_Id )
	,	.dbus2sub_dcluster0_port1_r_R_Last( dbus2sub_dcluster0_port1_r_R_Last )
	,	.dbus2sub_dcluster0_port1_r_R_Ready( dbus2sub_dcluster0_port1_r_R_Ready )
	,	.dbus2sub_dcluster0_port1_r_R_Resp( dbus2sub_dcluster0_port1_r_R_Resp )
	,	.dbus2sub_dcluster0_port1_r_R_User( dbus2sub_dcluster0_port1_r_R_User )
	,	.dbus2sub_dcluster0_port1_r_R_Valid( dbus2sub_dcluster0_port1_r_R_Valid )
	,	.dbus2sub_dcluster0_port2_r_Ar_Addr( dbus2sub_dcluster0_port2_r_Ar_Addr )
	,	.dbus2sub_dcluster0_port2_r_Ar_Burst( dbus2sub_dcluster0_port2_r_Ar_Burst )
	,	.dbus2sub_dcluster0_port2_r_Ar_Cache( dbus2sub_dcluster0_port2_r_Ar_Cache )
	,	.dbus2sub_dcluster0_port2_r_Ar_Id( dbus2sub_dcluster0_port2_r_Ar_Id )
	,	.dbus2sub_dcluster0_port2_r_Ar_Len( dbus2sub_dcluster0_port2_r_Ar_Len )
	,	.dbus2sub_dcluster0_port2_r_Ar_Lock( dbus2sub_dcluster0_port2_r_Ar_Lock )
	,	.dbus2sub_dcluster0_port2_r_Ar_Prot( dbus2sub_dcluster0_port2_r_Ar_Prot )
	,	.dbus2sub_dcluster0_port2_r_Ar_Ready( dbus2sub_dcluster0_port2_r_Ar_Ready )
	,	.dbus2sub_dcluster0_port2_r_Ar_Size( dbus2sub_dcluster0_port2_r_Ar_Size )
	,	.dbus2sub_dcluster0_port2_r_Ar_User( dbus2sub_dcluster0_port2_r_Ar_User )
	,	.dbus2sub_dcluster0_port2_r_Ar_Valid( dbus2sub_dcluster0_port2_r_Ar_Valid )
	,	.dbus2sub_dcluster0_port2_r_R_Data( dbus2sub_dcluster0_port2_r_R_Data )
	,	.dbus2sub_dcluster0_port2_r_R_Id( dbus2sub_dcluster0_port2_r_R_Id )
	,	.dbus2sub_dcluster0_port2_r_R_Last( dbus2sub_dcluster0_port2_r_R_Last )
	,	.dbus2sub_dcluster0_port2_r_R_Ready( dbus2sub_dcluster0_port2_r_R_Ready )
	,	.dbus2sub_dcluster0_port2_r_R_Resp( dbus2sub_dcluster0_port2_r_R_Resp )
	,	.dbus2sub_dcluster0_port2_r_R_User( dbus2sub_dcluster0_port2_r_R_User )
	,	.dbus2sub_dcluster0_port2_r_R_Valid( dbus2sub_dcluster0_port2_r_R_Valid )
	,	.dbus2sub_dcluster0_port3_r_Ar_Addr( dbus2sub_dcluster0_port3_r_Ar_Addr )
	,	.dbus2sub_dcluster0_port3_r_Ar_Burst( dbus2sub_dcluster0_port3_r_Ar_Burst )
	,	.dbus2sub_dcluster0_port3_r_Ar_Cache( dbus2sub_dcluster0_port3_r_Ar_Cache )
	,	.dbus2sub_dcluster0_port3_r_Ar_Id( dbus2sub_dcluster0_port3_r_Ar_Id )
	,	.dbus2sub_dcluster0_port3_r_Ar_Len( dbus2sub_dcluster0_port3_r_Ar_Len )
	,	.dbus2sub_dcluster0_port3_r_Ar_Lock( dbus2sub_dcluster0_port3_r_Ar_Lock )
	,	.dbus2sub_dcluster0_port3_r_Ar_Prot( dbus2sub_dcluster0_port3_r_Ar_Prot )
	,	.dbus2sub_dcluster0_port3_r_Ar_Ready( dbus2sub_dcluster0_port3_r_Ar_Ready )
	,	.dbus2sub_dcluster0_port3_r_Ar_Size( dbus2sub_dcluster0_port3_r_Ar_Size )
	,	.dbus2sub_dcluster0_port3_r_Ar_User( dbus2sub_dcluster0_port3_r_Ar_User )
	,	.dbus2sub_dcluster0_port3_r_Ar_Valid( dbus2sub_dcluster0_port3_r_Ar_Valid )
	,	.dbus2sub_dcluster0_port3_r_R_Data( dbus2sub_dcluster0_port3_r_R_Data )
	,	.dbus2sub_dcluster0_port3_r_R_Id( dbus2sub_dcluster0_port3_r_R_Id )
	,	.dbus2sub_dcluster0_port3_r_R_Last( dbus2sub_dcluster0_port3_r_R_Last )
	,	.dbus2sub_dcluster0_port3_r_R_Ready( dbus2sub_dcluster0_port3_r_R_Ready )
	,	.dbus2sub_dcluster0_port3_r_R_Resp( dbus2sub_dcluster0_port3_r_R_Resp )
	,	.dbus2sub_dcluster0_port3_r_R_User( dbus2sub_dcluster0_port3_r_R_User )
	,	.dbus2sub_dcluster0_port3_r_R_Valid( dbus2sub_dcluster0_port3_r_R_Valid )
	,	.dbus2sub_dcluster1_port0_r_Ar_Addr( dbus2sub_dcluster1_port0_r_Ar_Addr )
	,	.dbus2sub_dcluster1_port0_r_Ar_Burst( dbus2sub_dcluster1_port0_r_Ar_Burst )
	,	.dbus2sub_dcluster1_port0_r_Ar_Cache( dbus2sub_dcluster1_port0_r_Ar_Cache )
	,	.dbus2sub_dcluster1_port0_r_Ar_Id( dbus2sub_dcluster1_port0_r_Ar_Id )
	,	.dbus2sub_dcluster1_port0_r_Ar_Len( dbus2sub_dcluster1_port0_r_Ar_Len )
	,	.dbus2sub_dcluster1_port0_r_Ar_Lock( dbus2sub_dcluster1_port0_r_Ar_Lock )
	,	.dbus2sub_dcluster1_port0_r_Ar_Prot( dbus2sub_dcluster1_port0_r_Ar_Prot )
	,	.dbus2sub_dcluster1_port0_r_Ar_Ready( dbus2sub_dcluster1_port0_r_Ar_Ready )
	,	.dbus2sub_dcluster1_port0_r_Ar_Size( dbus2sub_dcluster1_port0_r_Ar_Size )
	,	.dbus2sub_dcluster1_port0_r_Ar_User( dbus2sub_dcluster1_port0_r_Ar_User )
	,	.dbus2sub_dcluster1_port0_r_Ar_Valid( dbus2sub_dcluster1_port0_r_Ar_Valid )
	,	.dbus2sub_dcluster1_port0_r_R_Data( dbus2sub_dcluster1_port0_r_R_Data )
	,	.dbus2sub_dcluster1_port0_r_R_Id( dbus2sub_dcluster1_port0_r_R_Id )
	,	.dbus2sub_dcluster1_port0_r_R_Last( dbus2sub_dcluster1_port0_r_R_Last )
	,	.dbus2sub_dcluster1_port0_r_R_Ready( dbus2sub_dcluster1_port0_r_R_Ready )
	,	.dbus2sub_dcluster1_port0_r_R_Resp( dbus2sub_dcluster1_port0_r_R_Resp )
	,	.dbus2sub_dcluster1_port0_r_R_User( dbus2sub_dcluster1_port0_r_R_User )
	,	.dbus2sub_dcluster1_port0_r_R_Valid( dbus2sub_dcluster1_port0_r_R_Valid )
	,	.dbus2sub_dcluster1_port1_r_Ar_Addr( dbus2sub_dcluster1_port1_r_Ar_Addr )
	,	.dbus2sub_dcluster1_port1_r_Ar_Burst( dbus2sub_dcluster1_port1_r_Ar_Burst )
	,	.dbus2sub_dcluster1_port1_r_Ar_Cache( dbus2sub_dcluster1_port1_r_Ar_Cache )
	,	.dbus2sub_dcluster1_port1_r_Ar_Id( dbus2sub_dcluster1_port1_r_Ar_Id )
	,	.dbus2sub_dcluster1_port1_r_Ar_Len( dbus2sub_dcluster1_port1_r_Ar_Len )
	,	.dbus2sub_dcluster1_port1_r_Ar_Lock( dbus2sub_dcluster1_port1_r_Ar_Lock )
	,	.dbus2sub_dcluster1_port1_r_Ar_Prot( dbus2sub_dcluster1_port1_r_Ar_Prot )
	,	.dbus2sub_dcluster1_port1_r_Ar_Ready( dbus2sub_dcluster1_port1_r_Ar_Ready )
	,	.dbus2sub_dcluster1_port1_r_Ar_Size( dbus2sub_dcluster1_port1_r_Ar_Size )
	,	.dbus2sub_dcluster1_port1_r_Ar_User( dbus2sub_dcluster1_port1_r_Ar_User )
	,	.dbus2sub_dcluster1_port1_r_Ar_Valid( dbus2sub_dcluster1_port1_r_Ar_Valid )
	,	.dbus2sub_dcluster1_port1_r_R_Data( dbus2sub_dcluster1_port1_r_R_Data )
	,	.dbus2sub_dcluster1_port1_r_R_Id( dbus2sub_dcluster1_port1_r_R_Id )
	,	.dbus2sub_dcluster1_port1_r_R_Last( dbus2sub_dcluster1_port1_r_R_Last )
	,	.dbus2sub_dcluster1_port1_r_R_Ready( dbus2sub_dcluster1_port1_r_R_Ready )
	,	.dbus2sub_dcluster1_port1_r_R_Resp( dbus2sub_dcluster1_port1_r_R_Resp )
	,	.dbus2sub_dcluster1_port1_r_R_User( dbus2sub_dcluster1_port1_r_R_User )
	,	.dbus2sub_dcluster1_port1_r_R_Valid( dbus2sub_dcluster1_port1_r_R_Valid )
	,	.dbus2sub_dcluster1_port2_r_Ar_Addr( dbus2sub_dcluster1_port2_r_Ar_Addr )
	,	.dbus2sub_dcluster1_port2_r_Ar_Burst( dbus2sub_dcluster1_port2_r_Ar_Burst )
	,	.dbus2sub_dcluster1_port2_r_Ar_Cache( dbus2sub_dcluster1_port2_r_Ar_Cache )
	,	.dbus2sub_dcluster1_port2_r_Ar_Id( dbus2sub_dcluster1_port2_r_Ar_Id )
	,	.dbus2sub_dcluster1_port2_r_Ar_Len( dbus2sub_dcluster1_port2_r_Ar_Len )
	,	.dbus2sub_dcluster1_port2_r_Ar_Lock( dbus2sub_dcluster1_port2_r_Ar_Lock )
	,	.dbus2sub_dcluster1_port2_r_Ar_Prot( dbus2sub_dcluster1_port2_r_Ar_Prot )
	,	.dbus2sub_dcluster1_port2_r_Ar_Ready( dbus2sub_dcluster1_port2_r_Ar_Ready )
	,	.dbus2sub_dcluster1_port2_r_Ar_Size( dbus2sub_dcluster1_port2_r_Ar_Size )
	,	.dbus2sub_dcluster1_port2_r_Ar_User( dbus2sub_dcluster1_port2_r_Ar_User )
	,	.dbus2sub_dcluster1_port2_r_Ar_Valid( dbus2sub_dcluster1_port2_r_Ar_Valid )
	,	.dbus2sub_dcluster1_port2_r_R_Data( dbus2sub_dcluster1_port2_r_R_Data )
	,	.dbus2sub_dcluster1_port2_r_R_Id( dbus2sub_dcluster1_port2_r_R_Id )
	,	.dbus2sub_dcluster1_port2_r_R_Last( dbus2sub_dcluster1_port2_r_R_Last )
	,	.dbus2sub_dcluster1_port2_r_R_Ready( dbus2sub_dcluster1_port2_r_R_Ready )
	,	.dbus2sub_dcluster1_port2_r_R_Resp( dbus2sub_dcluster1_port2_r_R_Resp )
	,	.dbus2sub_dcluster1_port2_r_R_User( dbus2sub_dcluster1_port2_r_R_User )
	,	.dbus2sub_dcluster1_port2_r_R_Valid( dbus2sub_dcluster1_port2_r_R_Valid )
	,	.dbus2sub_dcluster1_port3_r_Ar_Addr( dbus2sub_dcluster1_port3_r_Ar_Addr )
	,	.dbus2sub_dcluster1_port3_r_Ar_Burst( dbus2sub_dcluster1_port3_r_Ar_Burst )
	,	.dbus2sub_dcluster1_port3_r_Ar_Cache( dbus2sub_dcluster1_port3_r_Ar_Cache )
	,	.dbus2sub_dcluster1_port3_r_Ar_Id( dbus2sub_dcluster1_port3_r_Ar_Id )
	,	.dbus2sub_dcluster1_port3_r_Ar_Len( dbus2sub_dcluster1_port3_r_Ar_Len )
	,	.dbus2sub_dcluster1_port3_r_Ar_Lock( dbus2sub_dcluster1_port3_r_Ar_Lock )
	,	.dbus2sub_dcluster1_port3_r_Ar_Prot( dbus2sub_dcluster1_port3_r_Ar_Prot )
	,	.dbus2sub_dcluster1_port3_r_Ar_Ready( dbus2sub_dcluster1_port3_r_Ar_Ready )
	,	.dbus2sub_dcluster1_port3_r_Ar_Size( dbus2sub_dcluster1_port3_r_Ar_Size )
	,	.dbus2sub_dcluster1_port3_r_Ar_User( dbus2sub_dcluster1_port3_r_Ar_User )
	,	.dbus2sub_dcluster1_port3_r_Ar_Valid( dbus2sub_dcluster1_port3_r_Ar_Valid )
	,	.dbus2sub_dcluster1_port3_r_R_Data( dbus2sub_dcluster1_port3_r_R_Data )
	,	.dbus2sub_dcluster1_port3_r_R_Id( dbus2sub_dcluster1_port3_r_R_Id )
	,	.dbus2sub_dcluster1_port3_r_R_Last( dbus2sub_dcluster1_port3_r_R_Last )
	,	.dbus2sub_dcluster1_port3_r_R_Ready( dbus2sub_dcluster1_port3_r_R_Ready )
	,	.dbus2sub_dcluster1_port3_r_R_Resp( dbus2sub_dcluster1_port3_r_R_Resp )
	,	.dbus2sub_dcluster1_port3_r_R_User( dbus2sub_dcluster1_port3_r_R_User )
	,	.dbus2sub_dcluster1_port3_r_R_Valid( dbus2sub_dcluster1_port3_r_R_Valid )
	,	.dbus2sub_ddma_r_Ar_Addr( dbus2sub_ddma_r_Ar_Addr )
	,	.dbus2sub_ddma_r_Ar_Burst( dbus2sub_ddma_r_Ar_Burst )
	,	.dbus2sub_ddma_r_Ar_Cache( dbus2sub_ddma_r_Ar_Cache )
	,	.dbus2sub_ddma_r_Ar_Id( dbus2sub_ddma_r_Ar_Id )
	,	.dbus2sub_ddma_r_Ar_Len( dbus2sub_ddma_r_Ar_Len )
	,	.dbus2sub_ddma_r_Ar_Lock( dbus2sub_ddma_r_Ar_Lock )
	,	.dbus2sub_ddma_r_Ar_Prot( dbus2sub_ddma_r_Ar_Prot )
	,	.dbus2sub_ddma_r_Ar_Ready( dbus2sub_ddma_r_Ar_Ready )
	,	.dbus2sub_ddma_r_Ar_Size( dbus2sub_ddma_r_Ar_Size )
	,	.dbus2sub_ddma_r_Ar_User( dbus2sub_ddma_r_Ar_User )
	,	.dbus2sub_ddma_r_Ar_Valid( dbus2sub_ddma_r_Ar_Valid )
	,	.dbus2sub_ddma_r_R_Data( dbus2sub_ddma_r_R_Data )
	,	.dbus2sub_ddma_r_R_Id( dbus2sub_ddma_r_R_Id )
	,	.dbus2sub_ddma_r_R_Last( dbus2sub_ddma_r_R_Last )
	,	.dbus2sub_ddma_r_R_Ready( dbus2sub_ddma_r_R_Ready )
	,	.dbus2sub_ddma_r_R_Resp( dbus2sub_ddma_r_R_Resp )
	,	.dbus2sub_ddma_r_R_User( dbus2sub_ddma_r_R_User )
	,	.dbus2sub_ddma_r_R_Valid( dbus2sub_ddma_r_R_Valid )
	,	.dbus2sub_pcie_cpu_r_Ar_Addr( dbus2sub_pcie_cpu_r_Ar_Addr )
	,	.dbus2sub_pcie_cpu_r_Ar_Burst( dbus2sub_pcie_cpu_r_Ar_Burst )
	,	.dbus2sub_pcie_cpu_r_Ar_Cache( dbus2sub_pcie_cpu_r_Ar_Cache )
	,	.dbus2sub_pcie_cpu_r_Ar_Id( dbus2sub_pcie_cpu_r_Ar_Id )
	,	.dbus2sub_pcie_cpu_r_Ar_Len( dbus2sub_pcie_cpu_r_Ar_Len )
	,	.dbus2sub_pcie_cpu_r_Ar_Lock( dbus2sub_pcie_cpu_r_Ar_Lock )
	,	.dbus2sub_pcie_cpu_r_Ar_Prot( dbus2sub_pcie_cpu_r_Ar_Prot )
	,	.dbus2sub_pcie_cpu_r_Ar_Ready( dbus2sub_pcie_cpu_r_Ar_Ready )
	,	.dbus2sub_pcie_cpu_r_Ar_Size( dbus2sub_pcie_cpu_r_Ar_Size )
	,	.dbus2sub_pcie_cpu_r_Ar_User( dbus2sub_pcie_cpu_r_Ar_User )
	,	.dbus2sub_pcie_cpu_r_Ar_Valid( dbus2sub_pcie_cpu_r_Ar_Valid )
	,	.dbus2sub_pcie_cpu_r_R_Data( dbus2sub_pcie_cpu_r_R_Data )
	,	.dbus2sub_pcie_cpu_r_R_Id( dbus2sub_pcie_cpu_r_R_Id )
	,	.dbus2sub_pcie_cpu_r_R_Last( dbus2sub_pcie_cpu_r_R_Last )
	,	.dbus2sub_pcie_cpu_r_R_Ready( dbus2sub_pcie_cpu_r_R_Ready )
	,	.dbus2sub_pcie_cpu_r_R_Resp( dbus2sub_pcie_cpu_r_R_Resp )
	,	.dbus2sub_pcie_cpu_r_R_User( dbus2sub_pcie_cpu_r_R_User )
	,	.dbus2sub_pcie_cpu_r_R_Valid( dbus2sub_pcie_cpu_r_R_Valid )
	,	.shm0_r_Ar_Addr( shm0_r_Ar_Addr )
	,	.shm0_r_Ar_Burst( shm0_r_Ar_Burst )
	,	.shm0_r_Ar_Cache( shm0_r_Ar_Cache )
	,	.shm0_r_Ar_Id( shm0_r_Ar_Id )
	,	.shm0_r_Ar_Len( shm0_r_Ar_Len )
	,	.shm0_r_Ar_Lock( shm0_r_Ar_Lock )
	,	.shm0_r_Ar_Prot( shm0_r_Ar_Prot )
	,	.shm0_r_Ar_Ready( shm0_r_Ar_Ready )
	,	.shm0_r_Ar_Size( shm0_r_Ar_Size )
	,	.shm0_r_Ar_User( shm0_r_Ar_User )
	,	.shm0_r_Ar_Valid( shm0_r_Ar_Valid )
	,	.shm0_r_R_Data( shm0_r_R_Data )
	,	.shm0_r_R_Id( shm0_r_R_Id )
	,	.shm0_r_R_Last( shm0_r_R_Last )
	,	.shm0_r_R_Ready( shm0_r_R_Ready )
	,	.shm0_r_R_Resp( shm0_r_R_Resp )
	,	.shm0_r_R_User( shm0_r_R_User )
	,	.shm0_r_R_Valid( shm0_r_R_Valid )
	,	.shm1_r_Ar_Addr( shm1_r_Ar_Addr )
	,	.shm1_r_Ar_Burst( shm1_r_Ar_Burst )
	,	.shm1_r_Ar_Cache( shm1_r_Ar_Cache )
	,	.shm1_r_Ar_Id( shm1_r_Ar_Id )
	,	.shm1_r_Ar_Len( shm1_r_Ar_Len )
	,	.shm1_r_Ar_Lock( shm1_r_Ar_Lock )
	,	.shm1_r_Ar_Prot( shm1_r_Ar_Prot )
	,	.shm1_r_Ar_Ready( shm1_r_Ar_Ready )
	,	.shm1_r_Ar_Size( shm1_r_Ar_Size )
	,	.shm1_r_Ar_User( shm1_r_Ar_User )
	,	.shm1_r_Ar_Valid( shm1_r_Ar_Valid )
	,	.shm1_r_R_Data( shm1_r_R_Data )
	,	.shm1_r_R_Id( shm1_r_R_Id )
	,	.shm1_r_R_Last( shm1_r_R_Last )
	,	.shm1_r_R_Ready( shm1_r_R_Ready )
	,	.shm1_r_R_Resp( shm1_r_R_Resp )
	,	.shm1_r_R_User( shm1_r_R_User )
	,	.shm1_r_R_Valid( shm1_r_R_Valid )
	,	.shm2_r_Ar_Addr( shm2_r_Ar_Addr )
	,	.shm2_r_Ar_Burst( shm2_r_Ar_Burst )
	,	.shm2_r_Ar_Cache( shm2_r_Ar_Cache )
	,	.shm2_r_Ar_Id( shm2_r_Ar_Id )
	,	.shm2_r_Ar_Len( shm2_r_Ar_Len )
	,	.shm2_r_Ar_Lock( shm2_r_Ar_Lock )
	,	.shm2_r_Ar_Prot( shm2_r_Ar_Prot )
	,	.shm2_r_Ar_Ready( shm2_r_Ar_Ready )
	,	.shm2_r_Ar_Size( shm2_r_Ar_Size )
	,	.shm2_r_Ar_User( shm2_r_Ar_User )
	,	.shm2_r_Ar_Valid( shm2_r_Ar_Valid )
	,	.shm2_r_R_Data( shm2_r_R_Data )
	,	.shm2_r_R_Id( shm2_r_R_Id )
	,	.shm2_r_R_Last( shm2_r_R_Last )
	,	.shm2_r_R_Ready( shm2_r_R_Ready )
	,	.shm2_r_R_Resp( shm2_r_R_Resp )
	,	.shm2_r_R_User( shm2_r_R_User )
	,	.shm2_r_R_Valid( shm2_r_R_Valid )
	,	.shm3_r_Ar_Addr( shm3_r_Ar_Addr )
	,	.shm3_r_Ar_Burst( shm3_r_Ar_Burst )
	,	.shm3_r_Ar_Cache( shm3_r_Ar_Cache )
	,	.shm3_r_Ar_Id( shm3_r_Ar_Id )
	,	.shm3_r_Ar_Len( shm3_r_Ar_Len )
	,	.shm3_r_Ar_Lock( shm3_r_Ar_Lock )
	,	.shm3_r_Ar_Prot( shm3_r_Ar_Prot )
	,	.shm3_r_Ar_Ready( shm3_r_Ar_Ready )
	,	.shm3_r_Ar_Size( shm3_r_Ar_Size )
	,	.shm3_r_Ar_User( shm3_r_Ar_User )
	,	.shm3_r_Ar_Valid( shm3_r_Ar_Valid )
	,	.shm3_r_R_Data( shm3_r_R_Data )
	,	.shm3_r_R_Id( shm3_r_R_Id )
	,	.shm3_r_R_Last( shm3_r_R_Last )
	,	.shm3_r_R_Ready( shm3_r_R_Ready )
	,	.shm3_r_R_Resp( shm3_r_R_Resp )
	,	.shm3_r_R_User( shm3_r_R_User )
	,	.shm3_r_R_Valid( shm3_r_R_Valid )
	);




//--------------------------------------------------------------------------------------
// dbus_write


	dbus_write_Structure_Module_westbus u_dbus_write_Structure_Module_westbus(
		.TM( TM )
	,	.arstn_dbus( arstn_dbus )
	,	.clk_dbus( clk_dbus )
	,	.dp_Link_c01_m01_e_to_Switch_west_Data( w_dp_Link_c01_m01_e_to_Switch_west_Data )
	,	.dp_Link_c01_m01_e_to_Switch_west_Head( w_dp_Link_c01_m01_e_to_Switch_west_Head )
	,	.dp_Link_c01_m01_e_to_Switch_west_Rdy( w_dp_Link_c01_m01_e_to_Switch_west_Rdy )
	,	.dp_Link_c01_m01_e_to_Switch_west_Tail( w_dp_Link_c01_m01_e_to_Switch_west_Tail )
	,	.dp_Link_c01_m01_e_to_Switch_west_Vld( w_dp_Link_c01_m01_e_to_Switch_west_Vld )
	,	.dp_Link_m0_asi_to_Link_m0_ast_Data( w_dp_Link_m0_asi_to_Link_m0_ast_Data )
	,	.dp_Link_m0_asi_to_Link_m0_ast_RdCnt( w_dp_Link_m0_asi_to_Link_m0_ast_RdCnt )
	,	.dp_Link_m0_asi_to_Link_m0_ast_RdPtr( w_dp_Link_m0_asi_to_Link_m0_ast_RdPtr )
	,	.dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRst( w_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRstAck( w_dp_Link_m0_asi_to_Link_m0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRst( w_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRstAck( w_dp_Link_m0_asi_to_Link_m0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m0_asi_to_Link_m0_ast_WrCnt( w_dp_Link_m0_asi_to_Link_m0_ast_WrCnt )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_Data( w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_Data )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdCnt( w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdCnt )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdPtr( w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RdPtr )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRst( w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRstAck( w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRst( w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRstAck( w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m0_astResp001_to_Link_m0_asiResp001_WrCnt( w_dp_Link_m0_astResp001_to_Link_m0_asiResp001_WrCnt )
	,	.dp_Link_m1_asi_to_Link_m1_ast_Data( w_dp_Link_m1_asi_to_Link_m1_ast_Data )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RdCnt( w_dp_Link_m1_asi_to_Link_m1_ast_RdCnt )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RdPtr( w_dp_Link_m1_asi_to_Link_m1_ast_RdPtr )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst( w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck( w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst( w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck( w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1_asi_to_Link_m1_ast_WrCnt( w_dp_Link_m1_asi_to_Link_m1_ast_WrCnt )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt )
	,	.dp_Link_n03_m01_c_to_Switch_west_Data( w_dp_Link_n03_m01_c_to_Switch_west_Data )
	,	.dp_Link_n03_m01_c_to_Switch_west_Head( w_dp_Link_n03_m01_c_to_Switch_west_Head )
	,	.dp_Link_n03_m01_c_to_Switch_west_Rdy( w_dp_Link_n03_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n03_m01_c_to_Switch_west_Tail( w_dp_Link_n03_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n03_m01_c_to_Switch_west_Vld( w_dp_Link_n03_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n47_m01_c_to_Switch_west_Data( w_dp_Link_n47_m01_c_to_Switch_west_Data )
	,	.dp_Link_n47_m01_c_to_Switch_west_Head( w_dp_Link_n47_m01_c_to_Switch_west_Head )
	,	.dp_Link_n47_m01_c_to_Switch_west_Rdy( w_dp_Link_n47_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n47_m01_c_to_Switch_west_Tail( w_dp_Link_n47_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n47_m01_c_to_Switch_west_Vld( w_dp_Link_n47_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n8_m01_c_to_Switch_west_Data( w_dp_Link_n8_m01_c_to_Switch_west_Data )
	,	.dp_Link_n8_m01_c_to_Switch_west_Head( w_dp_Link_n8_m01_c_to_Switch_west_Head )
	,	.dp_Link_n8_m01_c_to_Switch_west_Rdy( w_dp_Link_n8_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n8_m01_c_to_Switch_west_Tail( w_dp_Link_n8_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n8_m01_c_to_Switch_west_Vld( w_dp_Link_n8_m01_c_to_Switch_west_Vld )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld )
	);

	dbus_write_Structure_Module_centerbus u_dbus_write_Structure_Module_centerbus(
		.TM( TM )
	,	.arstn_dbus( arstn_dbus )
	,	.clk_dbus( clk_dbus )
	,	.dbus2sub_dcluster0_port0_w_Aw_Addr( dbus2sub_dcluster0_port0_w_Aw_Addr )
	,	.dbus2sub_dcluster0_port0_w_Aw_Burst( dbus2sub_dcluster0_port0_w_Aw_Burst )
	,	.dbus2sub_dcluster0_port0_w_Aw_Cache( dbus2sub_dcluster0_port0_w_Aw_Cache )
	,	.dbus2sub_dcluster0_port0_w_Aw_Id( dbus2sub_dcluster0_port0_w_Aw_Id )
	,	.dbus2sub_dcluster0_port0_w_Aw_Len( dbus2sub_dcluster0_port0_w_Aw_Len )
	,	.dbus2sub_dcluster0_port0_w_Aw_Lock( dbus2sub_dcluster0_port0_w_Aw_Lock )
	,	.dbus2sub_dcluster0_port0_w_Aw_Prot( dbus2sub_dcluster0_port0_w_Aw_Prot )
	,	.dbus2sub_dcluster0_port0_w_Aw_Ready( dbus2sub_dcluster0_port0_w_Aw_Ready )
	,	.dbus2sub_dcluster0_port0_w_Aw_Size( dbus2sub_dcluster0_port0_w_Aw_Size )
	,	.dbus2sub_dcluster0_port0_w_Aw_User( dbus2sub_dcluster0_port0_w_Aw_User )
	,	.dbus2sub_dcluster0_port0_w_Aw_Valid( dbus2sub_dcluster0_port0_w_Aw_Valid )
	,	.dbus2sub_dcluster0_port0_w_B_Id( dbus2sub_dcluster0_port0_w_B_Id )
	,	.dbus2sub_dcluster0_port0_w_B_Ready( dbus2sub_dcluster0_port0_w_B_Ready )
	,	.dbus2sub_dcluster0_port0_w_B_Resp( dbus2sub_dcluster0_port0_w_B_Resp )
	,	.dbus2sub_dcluster0_port0_w_B_User( dbus2sub_dcluster0_port0_w_B_User )
	,	.dbus2sub_dcluster0_port0_w_B_Valid( dbus2sub_dcluster0_port0_w_B_Valid )
	,	.dbus2sub_dcluster0_port0_w_W_Data( dbus2sub_dcluster0_port0_w_W_Data )
	,	.dbus2sub_dcluster0_port0_w_W_Last( dbus2sub_dcluster0_port0_w_W_Last )
	,	.dbus2sub_dcluster0_port0_w_W_Ready( dbus2sub_dcluster0_port0_w_W_Ready )
	,	.dbus2sub_dcluster0_port0_w_W_Strb( dbus2sub_dcluster0_port0_w_W_Strb )
	,	.dbus2sub_dcluster0_port0_w_W_Valid( dbus2sub_dcluster0_port0_w_W_Valid )
	,	.dbus2sub_dcluster0_port1_w_Aw_Addr( dbus2sub_dcluster0_port1_w_Aw_Addr )
	,	.dbus2sub_dcluster0_port1_w_Aw_Burst( dbus2sub_dcluster0_port1_w_Aw_Burst )
	,	.dbus2sub_dcluster0_port1_w_Aw_Cache( dbus2sub_dcluster0_port1_w_Aw_Cache )
	,	.dbus2sub_dcluster0_port1_w_Aw_Id( dbus2sub_dcluster0_port1_w_Aw_Id )
	,	.dbus2sub_dcluster0_port1_w_Aw_Len( dbus2sub_dcluster0_port1_w_Aw_Len )
	,	.dbus2sub_dcluster0_port1_w_Aw_Lock( dbus2sub_dcluster0_port1_w_Aw_Lock )
	,	.dbus2sub_dcluster0_port1_w_Aw_Prot( dbus2sub_dcluster0_port1_w_Aw_Prot )
	,	.dbus2sub_dcluster0_port1_w_Aw_Ready( dbus2sub_dcluster0_port1_w_Aw_Ready )
	,	.dbus2sub_dcluster0_port1_w_Aw_Size( dbus2sub_dcluster0_port1_w_Aw_Size )
	,	.dbus2sub_dcluster0_port1_w_Aw_User( dbus2sub_dcluster0_port1_w_Aw_User )
	,	.dbus2sub_dcluster0_port1_w_Aw_Valid( dbus2sub_dcluster0_port1_w_Aw_Valid )
	,	.dbus2sub_dcluster0_port1_w_B_Id( dbus2sub_dcluster0_port1_w_B_Id )
	,	.dbus2sub_dcluster0_port1_w_B_Ready( dbus2sub_dcluster0_port1_w_B_Ready )
	,	.dbus2sub_dcluster0_port1_w_B_Resp( dbus2sub_dcluster0_port1_w_B_Resp )
	,	.dbus2sub_dcluster0_port1_w_B_User( dbus2sub_dcluster0_port1_w_B_User )
	,	.dbus2sub_dcluster0_port1_w_B_Valid( dbus2sub_dcluster0_port1_w_B_Valid )
	,	.dbus2sub_dcluster0_port1_w_W_Data( dbus2sub_dcluster0_port1_w_W_Data )
	,	.dbus2sub_dcluster0_port1_w_W_Last( dbus2sub_dcluster0_port1_w_W_Last )
	,	.dbus2sub_dcluster0_port1_w_W_Ready( dbus2sub_dcluster0_port1_w_W_Ready )
	,	.dbus2sub_dcluster0_port1_w_W_Strb( dbus2sub_dcluster0_port1_w_W_Strb )
	,	.dbus2sub_dcluster0_port1_w_W_Valid( dbus2sub_dcluster0_port1_w_W_Valid )
	,	.dbus2sub_dcluster0_port2_w_Aw_Addr( dbus2sub_dcluster0_port2_w_Aw_Addr )
	,	.dbus2sub_dcluster0_port2_w_Aw_Burst( dbus2sub_dcluster0_port2_w_Aw_Burst )
	,	.dbus2sub_dcluster0_port2_w_Aw_Cache( dbus2sub_dcluster0_port2_w_Aw_Cache )
	,	.dbus2sub_dcluster0_port2_w_Aw_Id( dbus2sub_dcluster0_port2_w_Aw_Id )
	,	.dbus2sub_dcluster0_port2_w_Aw_Len( dbus2sub_dcluster0_port2_w_Aw_Len )
	,	.dbus2sub_dcluster0_port2_w_Aw_Lock( dbus2sub_dcluster0_port2_w_Aw_Lock )
	,	.dbus2sub_dcluster0_port2_w_Aw_Prot( dbus2sub_dcluster0_port2_w_Aw_Prot )
	,	.dbus2sub_dcluster0_port2_w_Aw_Ready( dbus2sub_dcluster0_port2_w_Aw_Ready )
	,	.dbus2sub_dcluster0_port2_w_Aw_Size( dbus2sub_dcluster0_port2_w_Aw_Size )
	,	.dbus2sub_dcluster0_port2_w_Aw_User( dbus2sub_dcluster0_port2_w_Aw_User )
	,	.dbus2sub_dcluster0_port2_w_Aw_Valid( dbus2sub_dcluster0_port2_w_Aw_Valid )
	,	.dbus2sub_dcluster0_port2_w_B_Id( dbus2sub_dcluster0_port2_w_B_Id )
	,	.dbus2sub_dcluster0_port2_w_B_Ready( dbus2sub_dcluster0_port2_w_B_Ready )
	,	.dbus2sub_dcluster0_port2_w_B_Resp( dbus2sub_dcluster0_port2_w_B_Resp )
	,	.dbus2sub_dcluster0_port2_w_B_User( dbus2sub_dcluster0_port2_w_B_User )
	,	.dbus2sub_dcluster0_port2_w_B_Valid( dbus2sub_dcluster0_port2_w_B_Valid )
	,	.dbus2sub_dcluster0_port2_w_W_Data( dbus2sub_dcluster0_port2_w_W_Data )
	,	.dbus2sub_dcluster0_port2_w_W_Last( dbus2sub_dcluster0_port2_w_W_Last )
	,	.dbus2sub_dcluster0_port2_w_W_Ready( dbus2sub_dcluster0_port2_w_W_Ready )
	,	.dbus2sub_dcluster0_port2_w_W_Strb( dbus2sub_dcluster0_port2_w_W_Strb )
	,	.dbus2sub_dcluster0_port2_w_W_Valid( dbus2sub_dcluster0_port2_w_W_Valid )
	,	.dbus2sub_dcluster0_port3_w_Aw_Addr( dbus2sub_dcluster0_port3_w_Aw_Addr )
	,	.dbus2sub_dcluster0_port3_w_Aw_Burst( dbus2sub_dcluster0_port3_w_Aw_Burst )
	,	.dbus2sub_dcluster0_port3_w_Aw_Cache( dbus2sub_dcluster0_port3_w_Aw_Cache )
	,	.dbus2sub_dcluster0_port3_w_Aw_Id( dbus2sub_dcluster0_port3_w_Aw_Id )
	,	.dbus2sub_dcluster0_port3_w_Aw_Len( dbus2sub_dcluster0_port3_w_Aw_Len )
	,	.dbus2sub_dcluster0_port3_w_Aw_Lock( dbus2sub_dcluster0_port3_w_Aw_Lock )
	,	.dbus2sub_dcluster0_port3_w_Aw_Prot( dbus2sub_dcluster0_port3_w_Aw_Prot )
	,	.dbus2sub_dcluster0_port3_w_Aw_Ready( dbus2sub_dcluster0_port3_w_Aw_Ready )
	,	.dbus2sub_dcluster0_port3_w_Aw_Size( dbus2sub_dcluster0_port3_w_Aw_Size )
	,	.dbus2sub_dcluster0_port3_w_Aw_User( dbus2sub_dcluster0_port3_w_Aw_User )
	,	.dbus2sub_dcluster0_port3_w_Aw_Valid( dbus2sub_dcluster0_port3_w_Aw_Valid )
	,	.dbus2sub_dcluster0_port3_w_B_Id( dbus2sub_dcluster0_port3_w_B_Id )
	,	.dbus2sub_dcluster0_port3_w_B_Ready( dbus2sub_dcluster0_port3_w_B_Ready )
	,	.dbus2sub_dcluster0_port3_w_B_Resp( dbus2sub_dcluster0_port3_w_B_Resp )
	,	.dbus2sub_dcluster0_port3_w_B_User( dbus2sub_dcluster0_port3_w_B_User )
	,	.dbus2sub_dcluster0_port3_w_B_Valid( dbus2sub_dcluster0_port3_w_B_Valid )
	,	.dbus2sub_dcluster0_port3_w_W_Data( dbus2sub_dcluster0_port3_w_W_Data )
	,	.dbus2sub_dcluster0_port3_w_W_Last( dbus2sub_dcluster0_port3_w_W_Last )
	,	.dbus2sub_dcluster0_port3_w_W_Ready( dbus2sub_dcluster0_port3_w_W_Ready )
	,	.dbus2sub_dcluster0_port3_w_W_Strb( dbus2sub_dcluster0_port3_w_W_Strb )
	,	.dbus2sub_dcluster0_port3_w_W_Valid( dbus2sub_dcluster0_port3_w_W_Valid )
	,	.dbus2sub_dcluster1_port0_w_Aw_Addr( dbus2sub_dcluster1_port0_w_Aw_Addr )
	,	.dbus2sub_dcluster1_port0_w_Aw_Burst( dbus2sub_dcluster1_port0_w_Aw_Burst )
	,	.dbus2sub_dcluster1_port0_w_Aw_Cache( dbus2sub_dcluster1_port0_w_Aw_Cache )
	,	.dbus2sub_dcluster1_port0_w_Aw_Id( dbus2sub_dcluster1_port0_w_Aw_Id )
	,	.dbus2sub_dcluster1_port0_w_Aw_Len( dbus2sub_dcluster1_port0_w_Aw_Len )
	,	.dbus2sub_dcluster1_port0_w_Aw_Lock( dbus2sub_dcluster1_port0_w_Aw_Lock )
	,	.dbus2sub_dcluster1_port0_w_Aw_Prot( dbus2sub_dcluster1_port0_w_Aw_Prot )
	,	.dbus2sub_dcluster1_port0_w_Aw_Ready( dbus2sub_dcluster1_port0_w_Aw_Ready )
	,	.dbus2sub_dcluster1_port0_w_Aw_Size( dbus2sub_dcluster1_port0_w_Aw_Size )
	,	.dbus2sub_dcluster1_port0_w_Aw_User( dbus2sub_dcluster1_port0_w_Aw_User )
	,	.dbus2sub_dcluster1_port0_w_Aw_Valid( dbus2sub_dcluster1_port0_w_Aw_Valid )
	,	.dbus2sub_dcluster1_port0_w_B_Id( dbus2sub_dcluster1_port0_w_B_Id )
	,	.dbus2sub_dcluster1_port0_w_B_Ready( dbus2sub_dcluster1_port0_w_B_Ready )
	,	.dbus2sub_dcluster1_port0_w_B_Resp( dbus2sub_dcluster1_port0_w_B_Resp )
	,	.dbus2sub_dcluster1_port0_w_B_User( dbus2sub_dcluster1_port0_w_B_User )
	,	.dbus2sub_dcluster1_port0_w_B_Valid( dbus2sub_dcluster1_port0_w_B_Valid )
	,	.dbus2sub_dcluster1_port0_w_W_Data( dbus2sub_dcluster1_port0_w_W_Data )
	,	.dbus2sub_dcluster1_port0_w_W_Last( dbus2sub_dcluster1_port0_w_W_Last )
	,	.dbus2sub_dcluster1_port0_w_W_Ready( dbus2sub_dcluster1_port0_w_W_Ready )
	,	.dbus2sub_dcluster1_port0_w_W_Strb( dbus2sub_dcluster1_port0_w_W_Strb )
	,	.dbus2sub_dcluster1_port0_w_W_Valid( dbus2sub_dcluster1_port0_w_W_Valid )
	,	.dbus2sub_dcluster1_port1_w_Aw_Addr( dbus2sub_dcluster1_port1_w_Aw_Addr )
	,	.dbus2sub_dcluster1_port1_w_Aw_Burst( dbus2sub_dcluster1_port1_w_Aw_Burst )
	,	.dbus2sub_dcluster1_port1_w_Aw_Cache( dbus2sub_dcluster1_port1_w_Aw_Cache )
	,	.dbus2sub_dcluster1_port1_w_Aw_Id( dbus2sub_dcluster1_port1_w_Aw_Id )
	,	.dbus2sub_dcluster1_port1_w_Aw_Len( dbus2sub_dcluster1_port1_w_Aw_Len )
	,	.dbus2sub_dcluster1_port1_w_Aw_Lock( dbus2sub_dcluster1_port1_w_Aw_Lock )
	,	.dbus2sub_dcluster1_port1_w_Aw_Prot( dbus2sub_dcluster1_port1_w_Aw_Prot )
	,	.dbus2sub_dcluster1_port1_w_Aw_Ready( dbus2sub_dcluster1_port1_w_Aw_Ready )
	,	.dbus2sub_dcluster1_port1_w_Aw_Size( dbus2sub_dcluster1_port1_w_Aw_Size )
	,	.dbus2sub_dcluster1_port1_w_Aw_User( dbus2sub_dcluster1_port1_w_Aw_User )
	,	.dbus2sub_dcluster1_port1_w_Aw_Valid( dbus2sub_dcluster1_port1_w_Aw_Valid )
	,	.dbus2sub_dcluster1_port1_w_B_Id( dbus2sub_dcluster1_port1_w_B_Id )
	,	.dbus2sub_dcluster1_port1_w_B_Ready( dbus2sub_dcluster1_port1_w_B_Ready )
	,	.dbus2sub_dcluster1_port1_w_B_Resp( dbus2sub_dcluster1_port1_w_B_Resp )
	,	.dbus2sub_dcluster1_port1_w_B_User( dbus2sub_dcluster1_port1_w_B_User )
	,	.dbus2sub_dcluster1_port1_w_B_Valid( dbus2sub_dcluster1_port1_w_B_Valid )
	,	.dbus2sub_dcluster1_port1_w_W_Data( dbus2sub_dcluster1_port1_w_W_Data )
	,	.dbus2sub_dcluster1_port1_w_W_Last( dbus2sub_dcluster1_port1_w_W_Last )
	,	.dbus2sub_dcluster1_port1_w_W_Ready( dbus2sub_dcluster1_port1_w_W_Ready )
	,	.dbus2sub_dcluster1_port1_w_W_Strb( dbus2sub_dcluster1_port1_w_W_Strb )
	,	.dbus2sub_dcluster1_port1_w_W_Valid( dbus2sub_dcluster1_port1_w_W_Valid )
	,	.dbus2sub_dcluster1_port2_w_Aw_Addr( dbus2sub_dcluster1_port2_w_Aw_Addr )
	,	.dbus2sub_dcluster1_port2_w_Aw_Burst( dbus2sub_dcluster1_port2_w_Aw_Burst )
	,	.dbus2sub_dcluster1_port2_w_Aw_Cache( dbus2sub_dcluster1_port2_w_Aw_Cache )
	,	.dbus2sub_dcluster1_port2_w_Aw_Id( dbus2sub_dcluster1_port2_w_Aw_Id )
	,	.dbus2sub_dcluster1_port2_w_Aw_Len( dbus2sub_dcluster1_port2_w_Aw_Len )
	,	.dbus2sub_dcluster1_port2_w_Aw_Lock( dbus2sub_dcluster1_port2_w_Aw_Lock )
	,	.dbus2sub_dcluster1_port2_w_Aw_Prot( dbus2sub_dcluster1_port2_w_Aw_Prot )
	,	.dbus2sub_dcluster1_port2_w_Aw_Ready( dbus2sub_dcluster1_port2_w_Aw_Ready )
	,	.dbus2sub_dcluster1_port2_w_Aw_Size( dbus2sub_dcluster1_port2_w_Aw_Size )
	,	.dbus2sub_dcluster1_port2_w_Aw_User( dbus2sub_dcluster1_port2_w_Aw_User )
	,	.dbus2sub_dcluster1_port2_w_Aw_Valid( dbus2sub_dcluster1_port2_w_Aw_Valid )
	,	.dbus2sub_dcluster1_port2_w_B_Id( dbus2sub_dcluster1_port2_w_B_Id )
	,	.dbus2sub_dcluster1_port2_w_B_Ready( dbus2sub_dcluster1_port2_w_B_Ready )
	,	.dbus2sub_dcluster1_port2_w_B_Resp( dbus2sub_dcluster1_port2_w_B_Resp )
	,	.dbus2sub_dcluster1_port2_w_B_User( dbus2sub_dcluster1_port2_w_B_User )
	,	.dbus2sub_dcluster1_port2_w_B_Valid( dbus2sub_dcluster1_port2_w_B_Valid )
	,	.dbus2sub_dcluster1_port2_w_W_Data( dbus2sub_dcluster1_port2_w_W_Data )
	,	.dbus2sub_dcluster1_port2_w_W_Last( dbus2sub_dcluster1_port2_w_W_Last )
	,	.dbus2sub_dcluster1_port2_w_W_Ready( dbus2sub_dcluster1_port2_w_W_Ready )
	,	.dbus2sub_dcluster1_port2_w_W_Strb( dbus2sub_dcluster1_port2_w_W_Strb )
	,	.dbus2sub_dcluster1_port2_w_W_Valid( dbus2sub_dcluster1_port2_w_W_Valid )
	,	.dbus2sub_dcluster1_port3_w_Aw_Addr( dbus2sub_dcluster1_port3_w_Aw_Addr )
	,	.dbus2sub_dcluster1_port3_w_Aw_Burst( dbus2sub_dcluster1_port3_w_Aw_Burst )
	,	.dbus2sub_dcluster1_port3_w_Aw_Cache( dbus2sub_dcluster1_port3_w_Aw_Cache )
	,	.dbus2sub_dcluster1_port3_w_Aw_Id( dbus2sub_dcluster1_port3_w_Aw_Id )
	,	.dbus2sub_dcluster1_port3_w_Aw_Len( dbus2sub_dcluster1_port3_w_Aw_Len )
	,	.dbus2sub_dcluster1_port3_w_Aw_Lock( dbus2sub_dcluster1_port3_w_Aw_Lock )
	,	.dbus2sub_dcluster1_port3_w_Aw_Prot( dbus2sub_dcluster1_port3_w_Aw_Prot )
	,	.dbus2sub_dcluster1_port3_w_Aw_Ready( dbus2sub_dcluster1_port3_w_Aw_Ready )
	,	.dbus2sub_dcluster1_port3_w_Aw_Size( dbus2sub_dcluster1_port3_w_Aw_Size )
	,	.dbus2sub_dcluster1_port3_w_Aw_User( dbus2sub_dcluster1_port3_w_Aw_User )
	,	.dbus2sub_dcluster1_port3_w_Aw_Valid( dbus2sub_dcluster1_port3_w_Aw_Valid )
	,	.dbus2sub_dcluster1_port3_w_B_Id( dbus2sub_dcluster1_port3_w_B_Id )
	,	.dbus2sub_dcluster1_port3_w_B_Ready( dbus2sub_dcluster1_port3_w_B_Ready )
	,	.dbus2sub_dcluster1_port3_w_B_Resp( dbus2sub_dcluster1_port3_w_B_Resp )
	,	.dbus2sub_dcluster1_port3_w_B_User( dbus2sub_dcluster1_port3_w_B_User )
	,	.dbus2sub_dcluster1_port3_w_B_Valid( dbus2sub_dcluster1_port3_w_B_Valid )
	,	.dbus2sub_dcluster1_port3_w_W_Data( dbus2sub_dcluster1_port3_w_W_Data )
	,	.dbus2sub_dcluster1_port3_w_W_Last( dbus2sub_dcluster1_port3_w_W_Last )
	,	.dbus2sub_dcluster1_port3_w_W_Ready( dbus2sub_dcluster1_port3_w_W_Ready )
	,	.dbus2sub_dcluster1_port3_w_W_Strb( dbus2sub_dcluster1_port3_w_W_Strb )
	,	.dbus2sub_dcluster1_port3_w_W_Valid( dbus2sub_dcluster1_port3_w_W_Valid )
	,	.dbus2sub_ddma_w_Aw_Addr( dbus2sub_ddma_w_Aw_Addr )
	,	.dbus2sub_ddma_w_Aw_Burst( dbus2sub_ddma_w_Aw_Burst )
	,	.dbus2sub_ddma_w_Aw_Cache( dbus2sub_ddma_w_Aw_Cache )
	,	.dbus2sub_ddma_w_Aw_Id( dbus2sub_ddma_w_Aw_Id )
	,	.dbus2sub_ddma_w_Aw_Len( dbus2sub_ddma_w_Aw_Len )
	,	.dbus2sub_ddma_w_Aw_Lock( dbus2sub_ddma_w_Aw_Lock )
	,	.dbus2sub_ddma_w_Aw_Prot( dbus2sub_ddma_w_Aw_Prot )
	,	.dbus2sub_ddma_w_Aw_Ready( dbus2sub_ddma_w_Aw_Ready )
	,	.dbus2sub_ddma_w_Aw_Size( dbus2sub_ddma_w_Aw_Size )
	,	.dbus2sub_ddma_w_Aw_User( dbus2sub_ddma_w_Aw_User )
	,	.dbus2sub_ddma_w_Aw_Valid( dbus2sub_ddma_w_Aw_Valid )
	,	.dbus2sub_ddma_w_B_Id( dbus2sub_ddma_w_B_Id )
	,	.dbus2sub_ddma_w_B_Ready( dbus2sub_ddma_w_B_Ready )
	,	.dbus2sub_ddma_w_B_Resp( dbus2sub_ddma_w_B_Resp )
	,	.dbus2sub_ddma_w_B_User( dbus2sub_ddma_w_B_User )
	,	.dbus2sub_ddma_w_B_Valid( dbus2sub_ddma_w_B_Valid )
	,	.dbus2sub_ddma_w_W_Data( dbus2sub_ddma_w_W_Data )
	,	.dbus2sub_ddma_w_W_Last( dbus2sub_ddma_w_W_Last )
	,	.dbus2sub_ddma_w_W_Ready( dbus2sub_ddma_w_W_Ready )
	,	.dbus2sub_ddma_w_W_Strb( dbus2sub_ddma_w_W_Strb )
	,	.dbus2sub_ddma_w_W_Valid( dbus2sub_ddma_w_W_Valid )
	,	.dbus2sub_pcie_cpu_w_Aw_Addr( dbus2sub_pcie_cpu_w_Aw_Addr )
	,	.dbus2sub_pcie_cpu_w_Aw_Burst( dbus2sub_pcie_cpu_w_Aw_Burst )
	,	.dbus2sub_pcie_cpu_w_Aw_Cache( dbus2sub_pcie_cpu_w_Aw_Cache )
	,	.dbus2sub_pcie_cpu_w_Aw_Id( dbus2sub_pcie_cpu_w_Aw_Id )
	,	.dbus2sub_pcie_cpu_w_Aw_Len( dbus2sub_pcie_cpu_w_Aw_Len )
	,	.dbus2sub_pcie_cpu_w_Aw_Lock( dbus2sub_pcie_cpu_w_Aw_Lock )
	,	.dbus2sub_pcie_cpu_w_Aw_Prot( dbus2sub_pcie_cpu_w_Aw_Prot )
	,	.dbus2sub_pcie_cpu_w_Aw_Ready( dbus2sub_pcie_cpu_w_Aw_Ready )
	,	.dbus2sub_pcie_cpu_w_Aw_Size( dbus2sub_pcie_cpu_w_Aw_Size )
	,	.dbus2sub_pcie_cpu_w_Aw_User( dbus2sub_pcie_cpu_w_Aw_User )
	,	.dbus2sub_pcie_cpu_w_Aw_Valid( dbus2sub_pcie_cpu_w_Aw_Valid )
	,	.dbus2sub_pcie_cpu_w_B_Id( dbus2sub_pcie_cpu_w_B_Id )
	,	.dbus2sub_pcie_cpu_w_B_Ready( dbus2sub_pcie_cpu_w_B_Ready )
	,	.dbus2sub_pcie_cpu_w_B_Resp( dbus2sub_pcie_cpu_w_B_Resp )
	,	.dbus2sub_pcie_cpu_w_B_User( dbus2sub_pcie_cpu_w_B_User )
	,	.dbus2sub_pcie_cpu_w_B_Valid( dbus2sub_pcie_cpu_w_B_Valid )
	,	.dbus2sub_pcie_cpu_w_W_Data( dbus2sub_pcie_cpu_w_W_Data )
	,	.dbus2sub_pcie_cpu_w_W_Last( dbus2sub_pcie_cpu_w_W_Last )
	,	.dbus2sub_pcie_cpu_w_W_Ready( dbus2sub_pcie_cpu_w_W_Ready )
	,	.dbus2sub_pcie_cpu_w_W_Strb( dbus2sub_pcie_cpu_w_W_Strb )
	,	.dbus2sub_pcie_cpu_w_W_Valid( dbus2sub_pcie_cpu_w_W_Valid )
	,	.ddma_w_Aw_Addr( ddma_w_Aw_Addr )
	,	.ddma_w_Aw_Burst( ddma_w_Aw_Burst )
	,	.ddma_w_Aw_Cache( ddma_w_Aw_Cache )
	,	.ddma_w_Aw_Id( ddma_w_Aw_Id )
	,	.ddma_w_Aw_Len( ddma_w_Aw_Len )
	,	.ddma_w_Aw_Lock( ddma_w_Aw_Lock )
	,	.ddma_w_Aw_Prot( ddma_w_Aw_Prot )
	,	.ddma_w_Aw_Ready( ddma_w_Aw_Ready )
	,	.ddma_w_Aw_Size( ddma_w_Aw_Size )
	,	.ddma_w_Aw_User( ddma_w_Aw_User )
	,	.ddma_w_Aw_Valid( ddma_w_Aw_Valid )
	,	.ddma_w_B_Id( ddma_w_B_Id )
	,	.ddma_w_B_Ready( ddma_w_B_Ready )
	,	.ddma_w_B_Resp( ddma_w_B_Resp )
	,	.ddma_w_B_User( ddma_w_B_User )
	,	.ddma_w_B_Valid( ddma_w_B_Valid )
	,	.ddma_w_W_Data( ddma_w_W_Data )
	,	.ddma_w_W_Last( ddma_w_W_Last )
	,	.ddma_w_W_Ready( ddma_w_W_Ready )
	,	.ddma_w_W_Strb( ddma_w_W_Strb )
	,	.ddma_w_W_Valid( ddma_w_W_Valid )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld )
	,	.dp_Link_c01_m01_e_to_Switch_west_Data( w_dp_Link_c01_m01_e_to_Switch_west_Data )
	,	.dp_Link_c01_m01_e_to_Switch_west_Head( w_dp_Link_c01_m01_e_to_Switch_west_Head )
	,	.dp_Link_c01_m01_e_to_Switch_west_Rdy( w_dp_Link_c01_m01_e_to_Switch_west_Rdy )
	,	.dp_Link_c01_m01_e_to_Switch_west_Tail( w_dp_Link_c01_m01_e_to_Switch_west_Tail )
	,	.dp_Link_c01_m01_e_to_Switch_west_Vld( w_dp_Link_c01_m01_e_to_Switch_west_Vld )
	,	.dp_Link_n03_m01_c_to_Switch_west_Data( w_dp_Link_n03_m01_c_to_Switch_west_Data )
	,	.dp_Link_n03_m01_c_to_Switch_west_Head( w_dp_Link_n03_m01_c_to_Switch_west_Head )
	,	.dp_Link_n03_m01_c_to_Switch_west_Rdy( w_dp_Link_n03_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n03_m01_c_to_Switch_west_Tail( w_dp_Link_n03_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n03_m01_c_to_Switch_west_Vld( w_dp_Link_n03_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n03_m23_c_to_Switch_east_Data( w_dp_Link_n03_m23_c_to_Switch_east_Data )
	,	.dp_Link_n03_m23_c_to_Switch_east_Head( w_dp_Link_n03_m23_c_to_Switch_east_Head )
	,	.dp_Link_n03_m23_c_to_Switch_east_Rdy( w_dp_Link_n03_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n03_m23_c_to_Switch_east_Tail( w_dp_Link_n03_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n03_m23_c_to_Switch_east_Vld( w_dp_Link_n03_m23_c_to_Switch_east_Vld )
	,	.dp_Link_n0_asi_to_Link_n0_ast_Data( w_dp_Link_n0_asi_to_Link_n0_ast_Data )
	,	.dp_Link_n0_asi_to_Link_n0_ast_RdCnt( w_dp_Link_n0_asi_to_Link_n0_ast_RdCnt )
	,	.dp_Link_n0_asi_to_Link_n0_ast_RdPtr( w_dp_Link_n0_asi_to_Link_n0_ast_RdPtr )
	,	.dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst( w_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck( w_dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst( w_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck( w_dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n0_asi_to_Link_n0_ast_WrCnt( w_dp_Link_n0_asi_to_Link_n0_ast_WrCnt )
	,	.dp_Link_n0_astResp_to_Link_n0_asiResp_Data( w_dp_Link_n0_astResp_to_Link_n0_asiResp_Data )
	,	.dp_Link_n0_astResp_to_Link_n0_asiResp_RdCnt( w_dp_Link_n0_astResp_to_Link_n0_asiResp_RdCnt )
	,	.dp_Link_n0_astResp_to_Link_n0_asiResp_RdPtr( w_dp_Link_n0_astResp_to_Link_n0_asiResp_RdPtr )
	,	.dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRst( w_dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRst( w_dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_n0_astResp_to_Link_n0_asiResp_WrCnt( w_dp_Link_n0_astResp_to_Link_n0_asiResp_WrCnt )
	,	.dp_Link_n1_asi_to_Link_n1_ast_Data( w_dp_Link_n1_asi_to_Link_n1_ast_Data )
	,	.dp_Link_n1_asi_to_Link_n1_ast_RdCnt( w_dp_Link_n1_asi_to_Link_n1_ast_RdCnt )
	,	.dp_Link_n1_asi_to_Link_n1_ast_RdPtr( w_dp_Link_n1_asi_to_Link_n1_ast_RdPtr )
	,	.dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst( w_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck( w_dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst( w_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck( w_dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n1_asi_to_Link_n1_ast_WrCnt( w_dp_Link_n1_asi_to_Link_n1_ast_WrCnt )
	,	.dp_Link_n1_astResp_to_Link_n1_asiResp_Data( w_dp_Link_n1_astResp_to_Link_n1_asiResp_Data )
	,	.dp_Link_n1_astResp_to_Link_n1_asiResp_RdCnt( w_dp_Link_n1_astResp_to_Link_n1_asiResp_RdCnt )
	,	.dp_Link_n1_astResp_to_Link_n1_asiResp_RdPtr( w_dp_Link_n1_astResp_to_Link_n1_asiResp_RdPtr )
	,	.dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRst( w_dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRst( w_dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_n1_astResp_to_Link_n1_asiResp_WrCnt( w_dp_Link_n1_astResp_to_Link_n1_asiResp_WrCnt )
	,	.dp_Link_n2_asi_to_Link_n2_ast_Data( w_dp_Link_n2_asi_to_Link_n2_ast_Data )
	,	.dp_Link_n2_asi_to_Link_n2_ast_RdCnt( w_dp_Link_n2_asi_to_Link_n2_ast_RdCnt )
	,	.dp_Link_n2_asi_to_Link_n2_ast_RdPtr( w_dp_Link_n2_asi_to_Link_n2_ast_RdPtr )
	,	.dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst( w_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck( w_dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst( w_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck( w_dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n2_asi_to_Link_n2_ast_WrCnt( w_dp_Link_n2_asi_to_Link_n2_ast_WrCnt )
	,	.dp_Link_n2_astResp_to_Link_n2_asiResp_Data( w_dp_Link_n2_astResp_to_Link_n2_asiResp_Data )
	,	.dp_Link_n2_astResp_to_Link_n2_asiResp_RdCnt( w_dp_Link_n2_astResp_to_Link_n2_asiResp_RdCnt )
	,	.dp_Link_n2_astResp_to_Link_n2_asiResp_RdPtr( w_dp_Link_n2_astResp_to_Link_n2_asiResp_RdPtr )
	,	.dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRst( w_dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRst( w_dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_n2_astResp_to_Link_n2_asiResp_WrCnt( w_dp_Link_n2_astResp_to_Link_n2_asiResp_WrCnt )
	,	.dp_Link_n3_asi_to_Link_n3_ast_Data( w_dp_Link_n3_asi_to_Link_n3_ast_Data )
	,	.dp_Link_n3_asi_to_Link_n3_ast_RdCnt( w_dp_Link_n3_asi_to_Link_n3_ast_RdCnt )
	,	.dp_Link_n3_asi_to_Link_n3_ast_RdPtr( w_dp_Link_n3_asi_to_Link_n3_ast_RdPtr )
	,	.dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst( w_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck( w_dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst( w_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck( w_dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n3_asi_to_Link_n3_ast_WrCnt( w_dp_Link_n3_asi_to_Link_n3_ast_WrCnt )
	,	.dp_Link_n3_astResp_to_Link_n3_asiResp_Data( w_dp_Link_n3_astResp_to_Link_n3_asiResp_Data )
	,	.dp_Link_n3_astResp_to_Link_n3_asiResp_RdCnt( w_dp_Link_n3_astResp_to_Link_n3_asiResp_RdCnt )
	,	.dp_Link_n3_astResp_to_Link_n3_asiResp_RdPtr( w_dp_Link_n3_astResp_to_Link_n3_asiResp_RdPtr )
	,	.dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRst( w_dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRst( w_dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_n3_astResp_to_Link_n3_asiResp_WrCnt( w_dp_Link_n3_astResp_to_Link_n3_asiResp_WrCnt )
	,	.dp_Link_n47_m01_c_to_Switch_west_Data( w_dp_Link_n47_m01_c_to_Switch_west_Data )
	,	.dp_Link_n47_m01_c_to_Switch_west_Head( w_dp_Link_n47_m01_c_to_Switch_west_Head )
	,	.dp_Link_n47_m01_c_to_Switch_west_Rdy( w_dp_Link_n47_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n47_m01_c_to_Switch_west_Tail( w_dp_Link_n47_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n47_m01_c_to_Switch_west_Vld( w_dp_Link_n47_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n47_m23_c_to_Switch_east_Data( w_dp_Link_n47_m23_c_to_Switch_east_Data )
	,	.dp_Link_n47_m23_c_to_Switch_east_Head( w_dp_Link_n47_m23_c_to_Switch_east_Head )
	,	.dp_Link_n47_m23_c_to_Switch_east_Rdy( w_dp_Link_n47_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n47_m23_c_to_Switch_east_Tail( w_dp_Link_n47_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n47_m23_c_to_Switch_east_Vld( w_dp_Link_n47_m23_c_to_Switch_east_Vld )
	,	.dp_Link_n4_asi_to_Link_n4_ast_Data( w_dp_Link_n4_asi_to_Link_n4_ast_Data )
	,	.dp_Link_n4_asi_to_Link_n4_ast_RdCnt( w_dp_Link_n4_asi_to_Link_n4_ast_RdCnt )
	,	.dp_Link_n4_asi_to_Link_n4_ast_RdPtr( w_dp_Link_n4_asi_to_Link_n4_ast_RdPtr )
	,	.dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst( w_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck( w_dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst( w_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck( w_dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n4_asi_to_Link_n4_ast_WrCnt( w_dp_Link_n4_asi_to_Link_n4_ast_WrCnt )
	,	.dp_Link_n4_astResp_to_Link_n4_asiResp_Data( w_dp_Link_n4_astResp_to_Link_n4_asiResp_Data )
	,	.dp_Link_n4_astResp_to_Link_n4_asiResp_RdCnt( w_dp_Link_n4_astResp_to_Link_n4_asiResp_RdCnt )
	,	.dp_Link_n4_astResp_to_Link_n4_asiResp_RdPtr( w_dp_Link_n4_astResp_to_Link_n4_asiResp_RdPtr )
	,	.dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRst( w_dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRst( w_dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_n4_astResp_to_Link_n4_asiResp_WrCnt( w_dp_Link_n4_astResp_to_Link_n4_asiResp_WrCnt )
	,	.dp_Link_n5_asi_to_Link_n5_ast_Data( w_dp_Link_n5_asi_to_Link_n5_ast_Data )
	,	.dp_Link_n5_asi_to_Link_n5_ast_RdCnt( w_dp_Link_n5_asi_to_Link_n5_ast_RdCnt )
	,	.dp_Link_n5_asi_to_Link_n5_ast_RdPtr( w_dp_Link_n5_asi_to_Link_n5_ast_RdPtr )
	,	.dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst( w_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck( w_dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst( w_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck( w_dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n5_asi_to_Link_n5_ast_WrCnt( w_dp_Link_n5_asi_to_Link_n5_ast_WrCnt )
	,	.dp_Link_n5_astResp_to_Link_n5_asiResp_Data( w_dp_Link_n5_astResp_to_Link_n5_asiResp_Data )
	,	.dp_Link_n5_astResp_to_Link_n5_asiResp_RdCnt( w_dp_Link_n5_astResp_to_Link_n5_asiResp_RdCnt )
	,	.dp_Link_n5_astResp_to_Link_n5_asiResp_RdPtr( w_dp_Link_n5_astResp_to_Link_n5_asiResp_RdPtr )
	,	.dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRst( w_dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRst( w_dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_n5_astResp_to_Link_n5_asiResp_WrCnt( w_dp_Link_n5_astResp_to_Link_n5_asiResp_WrCnt )
	,	.dp_Link_n6_asi_to_Link_n6_ast_Data( w_dp_Link_n6_asi_to_Link_n6_ast_Data )
	,	.dp_Link_n6_asi_to_Link_n6_ast_RdCnt( w_dp_Link_n6_asi_to_Link_n6_ast_RdCnt )
	,	.dp_Link_n6_asi_to_Link_n6_ast_RdPtr( w_dp_Link_n6_asi_to_Link_n6_ast_RdPtr )
	,	.dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst( w_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck( w_dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst( w_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck( w_dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n6_asi_to_Link_n6_ast_WrCnt( w_dp_Link_n6_asi_to_Link_n6_ast_WrCnt )
	,	.dp_Link_n6_astResp_to_Link_n6_asiResp_Data( w_dp_Link_n6_astResp_to_Link_n6_asiResp_Data )
	,	.dp_Link_n6_astResp_to_Link_n6_asiResp_RdCnt( w_dp_Link_n6_astResp_to_Link_n6_asiResp_RdCnt )
	,	.dp_Link_n6_astResp_to_Link_n6_asiResp_RdPtr( w_dp_Link_n6_astResp_to_Link_n6_asiResp_RdPtr )
	,	.dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRst( w_dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRst( w_dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_n6_astResp_to_Link_n6_asiResp_WrCnt( w_dp_Link_n6_astResp_to_Link_n6_asiResp_WrCnt )
	,	.dp_Link_n7_asi_to_Link_n7_ast_Data( w_dp_Link_n7_asi_to_Link_n7_ast_Data )
	,	.dp_Link_n7_asi_to_Link_n7_ast_RdCnt( w_dp_Link_n7_asi_to_Link_n7_ast_RdCnt )
	,	.dp_Link_n7_asi_to_Link_n7_ast_RdPtr( w_dp_Link_n7_asi_to_Link_n7_ast_RdPtr )
	,	.dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst( w_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck( w_dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst( w_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck( w_dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n7_asi_to_Link_n7_ast_WrCnt( w_dp_Link_n7_asi_to_Link_n7_ast_WrCnt )
	,	.dp_Link_n7_astResp_to_Link_n7_asiResp_Data( w_dp_Link_n7_astResp_to_Link_n7_asiResp_Data )
	,	.dp_Link_n7_astResp_to_Link_n7_asiResp_RdCnt( w_dp_Link_n7_astResp_to_Link_n7_asiResp_RdCnt )
	,	.dp_Link_n7_astResp_to_Link_n7_asiResp_RdPtr( w_dp_Link_n7_astResp_to_Link_n7_asiResp_RdPtr )
	,	.dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRst( w_dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRst( w_dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_n7_astResp_to_Link_n7_asiResp_WrCnt( w_dp_Link_n7_astResp_to_Link_n7_asiResp_WrCnt )
	,	.dp_Link_n8_m01_c_to_Switch_west_Data( w_dp_Link_n8_m01_c_to_Switch_west_Data )
	,	.dp_Link_n8_m01_c_to_Switch_west_Head( w_dp_Link_n8_m01_c_to_Switch_west_Head )
	,	.dp_Link_n8_m01_c_to_Switch_west_Rdy( w_dp_Link_n8_m01_c_to_Switch_west_Rdy )
	,	.dp_Link_n8_m01_c_to_Switch_west_Tail( w_dp_Link_n8_m01_c_to_Switch_west_Tail )
	,	.dp_Link_n8_m01_c_to_Switch_west_Vld( w_dp_Link_n8_m01_c_to_Switch_west_Vld )
	,	.dp_Link_n8_m23_c_to_Switch_east_Data( w_dp_Link_n8_m23_c_to_Switch_east_Data )
	,	.dp_Link_n8_m23_c_to_Switch_east_Head( w_dp_Link_n8_m23_c_to_Switch_east_Head )
	,	.dp_Link_n8_m23_c_to_Switch_east_Rdy( w_dp_Link_n8_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n8_m23_c_to_Switch_east_Tail( w_dp_Link_n8_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n8_m23_c_to_Switch_east_Vld( w_dp_Link_n8_m23_c_to_Switch_east_Vld )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Data( w_dp_Link_sc01Resp_to_Switch_eastResp001_Data )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Head( w_dp_Link_sc01Resp_to_Switch_eastResp001_Head )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Rdy( w_dp_Link_sc01Resp_to_Switch_eastResp001_Rdy )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Tail( w_dp_Link_sc01Resp_to_Switch_eastResp001_Tail )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Vld( w_dp_Link_sc01Resp_to_Switch_eastResp001_Vld )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld )
	,	.dp_Switch_east_to_Link_c01_m01_a_Data( w_dp_Switch_east_to_Link_c01_m01_a_Data )
	,	.dp_Switch_east_to_Link_c01_m01_a_Head( w_dp_Switch_east_to_Link_c01_m01_a_Head )
	,	.dp_Switch_east_to_Link_c01_m01_a_Rdy( w_dp_Switch_east_to_Link_c01_m01_a_Rdy )
	,	.dp_Switch_east_to_Link_c01_m01_a_Tail( w_dp_Switch_east_to_Link_c01_m01_a_Tail )
	,	.dp_Switch_east_to_Link_c01_m01_a_Vld( w_dp_Switch_east_to_Link_c01_m01_a_Vld )
	,	.dp_Switch_east_to_Link_sc01_Data( w_dp_Switch_east_to_Link_sc01_Data )
	,	.dp_Switch_east_to_Link_sc01_Head( w_dp_Switch_east_to_Link_sc01_Head )
	,	.dp_Switch_east_to_Link_sc01_Rdy( w_dp_Switch_east_to_Link_sc01_Rdy )
	,	.dp_Switch_east_to_Link_sc01_Tail( w_dp_Switch_east_to_Link_sc01_Tail )
	,	.dp_Switch_east_to_Link_sc01_Vld( w_dp_Switch_east_to_Link_sc01_Vld )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Data )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Head )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Rdy )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Tail )
	,	.dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld( w_dp_Switch_westResp001_to_Link_c01_m01_eResp001_Vld )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Data )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Head )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Rdy )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Tail )
	,	.dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld( w_dp_Switch_westResp001_to_Link_n03_m01_cResp002_Vld )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Data )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Head )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Rdy )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Tail )
	,	.dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld( w_dp_Switch_westResp001_to_Link_n47_m01_cResp002_Vld )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Data )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Head )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Rdy )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Tail )
	,	.dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld( w_dp_Switch_westResp001_to_Link_n8_m01_cResp001_Vld )
	);
	dbus_write_Structure_Module_eastbus u_dbus_write_Structure_Module_eastbus(
		.TM( TM )
	,	.arstn_dbus( arstn_dbus )
	,	.cbus2dbus_dbg_w_Aw_Addr( cbus2dbus_dbg_w_Aw_Addr )
	,	.cbus2dbus_dbg_w_Aw_Burst( cbus2dbus_dbg_w_Aw_Burst )
	,	.cbus2dbus_dbg_w_Aw_Cache( cbus2dbus_dbg_w_Aw_Cache )
	,	.cbus2dbus_dbg_w_Aw_Id( cbus2dbus_dbg_w_Aw_Id )
	,	.cbus2dbus_dbg_w_Aw_Len( cbus2dbus_dbg_w_Aw_Len )
	,	.cbus2dbus_dbg_w_Aw_Lock( cbus2dbus_dbg_w_Aw_Lock )
	,	.cbus2dbus_dbg_w_Aw_Prot( cbus2dbus_dbg_w_Aw_Prot )
	,	.cbus2dbus_dbg_w_Aw_Ready( cbus2dbus_dbg_w_Aw_Ready )
	,	.cbus2dbus_dbg_w_Aw_Size( cbus2dbus_dbg_w_Aw_Size )
	,	.cbus2dbus_dbg_w_Aw_Valid( cbus2dbus_dbg_w_Aw_Valid )
	,	.cbus2dbus_dbg_w_B_Id( cbus2dbus_dbg_w_B_Id )
	,	.cbus2dbus_dbg_w_B_Ready( cbus2dbus_dbg_w_B_Ready )
	,	.cbus2dbus_dbg_w_B_Resp( cbus2dbus_dbg_w_B_Resp )
	,	.cbus2dbus_dbg_w_B_Valid( cbus2dbus_dbg_w_B_Valid )
	,	.cbus2dbus_dbg_w_W_Data( cbus2dbus_dbg_w_W_Data )
	,	.cbus2dbus_dbg_w_W_Last( cbus2dbus_dbg_w_W_Last )
	,	.cbus2dbus_dbg_w_W_Ready( cbus2dbus_dbg_w_W_Ready )
	,	.cbus2dbus_dbg_w_W_Strb( cbus2dbus_dbg_w_W_Strb )
	,	.cbus2dbus_dbg_w_W_Valid( cbus2dbus_dbg_w_W_Valid )
	,	.clk_dbus( clk_dbus )
	,	.dbus2cbus_cpu_w_Aw_Addr( dbus2cbus_cpu_w_Aw_Addr )
	,	.dbus2cbus_cpu_w_Aw_Burst( dbus2cbus_cpu_w_Aw_Burst )
	,	.dbus2cbus_cpu_w_Aw_Cache( dbus2cbus_cpu_w_Aw_Cache )
	,	.dbus2cbus_cpu_w_Aw_Id( dbus2cbus_cpu_w_Aw_Id )
	,	.dbus2cbus_cpu_w_Aw_Len( dbus2cbus_cpu_w_Aw_Len )
	,	.dbus2cbus_cpu_w_Aw_Lock( dbus2cbus_cpu_w_Aw_Lock )
	,	.dbus2cbus_cpu_w_Aw_Prot( dbus2cbus_cpu_w_Aw_Prot )
	,	.dbus2cbus_cpu_w_Aw_Ready( dbus2cbus_cpu_w_Aw_Ready )
	,	.dbus2cbus_cpu_w_Aw_Size( dbus2cbus_cpu_w_Aw_Size )
	,	.dbus2cbus_cpu_w_Aw_User( dbus2cbus_cpu_w_Aw_User )
	,	.dbus2cbus_cpu_w_Aw_Valid( dbus2cbus_cpu_w_Aw_Valid )
	,	.dbus2cbus_cpu_w_B_Id( dbus2cbus_cpu_w_B_Id )
	,	.dbus2cbus_cpu_w_B_Ready( dbus2cbus_cpu_w_B_Ready )
	,	.dbus2cbus_cpu_w_B_Resp( dbus2cbus_cpu_w_B_Resp )
	,	.dbus2cbus_cpu_w_B_Valid( dbus2cbus_cpu_w_B_Valid )
	,	.dbus2cbus_cpu_w_W_Data( dbus2cbus_cpu_w_W_Data )
	,	.dbus2cbus_cpu_w_W_Last( dbus2cbus_cpu_w_W_Last )
	,	.dbus2cbus_cpu_w_W_Ready( dbus2cbus_cpu_w_W_Ready )
	,	.dbus2cbus_cpu_w_W_Strb( dbus2cbus_cpu_w_W_Strb )
	,	.dbus2cbus_cpu_w_W_Valid( dbus2cbus_cpu_w_W_Valid )
	,	.dbus2cbus_pcie_w_Aw_Addr( dbus2cbus_pcie_w_Aw_Addr )
	,	.dbus2cbus_pcie_w_Aw_Burst( dbus2cbus_pcie_w_Aw_Burst )
	,	.dbus2cbus_pcie_w_Aw_Cache( dbus2cbus_pcie_w_Aw_Cache )
	,	.dbus2cbus_pcie_w_Aw_Id( dbus2cbus_pcie_w_Aw_Id )
	,	.dbus2cbus_pcie_w_Aw_Len( dbus2cbus_pcie_w_Aw_Len )
	,	.dbus2cbus_pcie_w_Aw_Lock( dbus2cbus_pcie_w_Aw_Lock )
	,	.dbus2cbus_pcie_w_Aw_Prot( dbus2cbus_pcie_w_Aw_Prot )
	,	.dbus2cbus_pcie_w_Aw_Ready( dbus2cbus_pcie_w_Aw_Ready )
	,	.dbus2cbus_pcie_w_Aw_Size( dbus2cbus_pcie_w_Aw_Size )
	,	.dbus2cbus_pcie_w_Aw_Valid( dbus2cbus_pcie_w_Aw_Valid )
	,	.dbus2cbus_pcie_w_B_Id( dbus2cbus_pcie_w_B_Id )
	,	.dbus2cbus_pcie_w_B_Ready( dbus2cbus_pcie_w_B_Ready )
	,	.dbus2cbus_pcie_w_B_Resp( dbus2cbus_pcie_w_B_Resp )
	,	.dbus2cbus_pcie_w_B_Valid( dbus2cbus_pcie_w_B_Valid )
	,	.dbus2cbus_pcie_w_W_Data( dbus2cbus_pcie_w_W_Data )
	,	.dbus2cbus_pcie_w_W_Last( dbus2cbus_pcie_w_W_Last )
	,	.dbus2cbus_pcie_w_W_Ready( dbus2cbus_pcie_w_W_Ready )
	,	.dbus2cbus_pcie_w_W_Strb( dbus2cbus_pcie_w_W_Strb )
	,	.dbus2cbus_pcie_w_W_Valid( dbus2cbus_pcie_w_W_Valid )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Data )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Head )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Rdy )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Tail )
	,	.dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld( w_dp_Link_c01_m01_aResp001_to_Switch_eastResp001_Vld )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Data( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Data )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Head( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Head )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Data( w_dp_Link_c0s_b_to_Link_c0e_a_Data )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Head( w_dp_Link_c0s_b_to_Link_c0e_a_Head )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Rdy( w_dp_Link_c0s_b_to_Link_c0e_a_Rdy )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Tail( w_dp_Link_c0s_b_to_Link_c0e_a_Tail )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Vld( w_dp_Link_c0s_b_to_Link_c0e_a_Vld )
	,	.dp_Link_c1_asi_to_Link_c1_ast_Data( w_dp_Link_c1_asi_to_Link_c1_ast_Data )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RdCnt( w_dp_Link_c1_asi_to_Link_c1_ast_RdCnt )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RdPtr( w_dp_Link_c1_asi_to_Link_c1_ast_RdPtr )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst( w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck( w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst( w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck( w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1_asi_to_Link_c1_ast_WrCnt( w_dp_Link_c1_asi_to_Link_c1_ast_WrCnt )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_Data( w_dp_Link_c1t_asi_to_Link_c1t_ast_Data )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt( w_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr( w_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst( w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck( w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst( w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck( w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt( w_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt )
	,	.dp_Link_m2_asi_to_Link_m2_ast_Data( w_dp_Link_m2_asi_to_Link_m2_ast_Data )
	,	.dp_Link_m2_asi_to_Link_m2_ast_RdCnt( w_dp_Link_m2_asi_to_Link_m2_ast_RdCnt )
	,	.dp_Link_m2_asi_to_Link_m2_ast_RdPtr( w_dp_Link_m2_asi_to_Link_m2_ast_RdPtr )
	,	.dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst( w_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck( w_dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst( w_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck( w_dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m2_asi_to_Link_m2_ast_WrCnt( w_dp_Link_m2_asi_to_Link_m2_ast_WrCnt )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data( w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt( w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr( w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst( w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck( w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst( w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck( w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt( w_dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt )
	,	.dp_Link_m3_asi_to_Link_m3_ast_Data( w_dp_Link_m3_asi_to_Link_m3_ast_Data )
	,	.dp_Link_m3_asi_to_Link_m3_ast_RdCnt( w_dp_Link_m3_asi_to_Link_m3_ast_RdCnt )
	,	.dp_Link_m3_asi_to_Link_m3_ast_RdPtr( w_dp_Link_m3_asi_to_Link_m3_ast_RdPtr )
	,	.dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst( w_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck( w_dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst( w_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck( w_dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m3_asi_to_Link_m3_ast_WrCnt( w_dp_Link_m3_asi_to_Link_m3_ast_WrCnt )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data( w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt( w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr( w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst( w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck( w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst( w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck( w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt( w_dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt )
	,	.dp_Link_n03_m23_c_to_Switch_east_Data( w_dp_Link_n03_m23_c_to_Switch_east_Data )
	,	.dp_Link_n03_m23_c_to_Switch_east_Head( w_dp_Link_n03_m23_c_to_Switch_east_Head )
	,	.dp_Link_n03_m23_c_to_Switch_east_Rdy( w_dp_Link_n03_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n03_m23_c_to_Switch_east_Tail( w_dp_Link_n03_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n03_m23_c_to_Switch_east_Vld( w_dp_Link_n03_m23_c_to_Switch_east_Vld )
	,	.dp_Link_n47_m23_c_to_Switch_east_Data( w_dp_Link_n47_m23_c_to_Switch_east_Data )
	,	.dp_Link_n47_m23_c_to_Switch_east_Head( w_dp_Link_n47_m23_c_to_Switch_east_Head )
	,	.dp_Link_n47_m23_c_to_Switch_east_Rdy( w_dp_Link_n47_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n47_m23_c_to_Switch_east_Tail( w_dp_Link_n47_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n47_m23_c_to_Switch_east_Vld( w_dp_Link_n47_m23_c_to_Switch_east_Vld )
	,	.dp_Link_n8_m23_c_to_Switch_east_Data( w_dp_Link_n8_m23_c_to_Switch_east_Data )
	,	.dp_Link_n8_m23_c_to_Switch_east_Head( w_dp_Link_n8_m23_c_to_Switch_east_Head )
	,	.dp_Link_n8_m23_c_to_Switch_east_Rdy( w_dp_Link_n8_m23_c_to_Switch_east_Rdy )
	,	.dp_Link_n8_m23_c_to_Switch_east_Tail( w_dp_Link_n8_m23_c_to_Switch_east_Tail )
	,	.dp_Link_n8_m23_c_to_Switch_east_Vld( w_dp_Link_n8_m23_c_to_Switch_east_Vld )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Data( w_dp_Link_sc01Resp_to_Switch_eastResp001_Data )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Head( w_dp_Link_sc01Resp_to_Switch_eastResp001_Head )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Rdy( w_dp_Link_sc01Resp_to_Switch_eastResp001_Rdy )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Tail( w_dp_Link_sc01Resp_to_Switch_eastResp001_Tail )
	,	.dp_Link_sc01Resp_to_Switch_eastResp001_Vld( w_dp_Link_sc01Resp_to_Switch_eastResp001_Vld )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Data )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Head )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Tail )
	,	.dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld( w_dp_Switch_eastResp001_to_Link_n03_m23_cResp002_Vld )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Data )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Head )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Tail )
	,	.dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld( w_dp_Switch_eastResp001_to_Link_n47_m23_cResp002_Vld )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Data )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Head )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Rdy )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Tail )
	,	.dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld( w_dp_Switch_eastResp001_to_Link_n8_m23_cResp001_Vld )
	,	.dp_Switch_east_to_Link_c01_m01_a_Data( w_dp_Switch_east_to_Link_c01_m01_a_Data )
	,	.dp_Switch_east_to_Link_c01_m01_a_Head( w_dp_Switch_east_to_Link_c01_m01_a_Head )
	,	.dp_Switch_east_to_Link_c01_m01_a_Rdy( w_dp_Switch_east_to_Link_c01_m01_a_Rdy )
	,	.dp_Switch_east_to_Link_c01_m01_a_Tail( w_dp_Switch_east_to_Link_c01_m01_a_Tail )
	,	.dp_Switch_east_to_Link_c01_m01_a_Vld( w_dp_Switch_east_to_Link_c01_m01_a_Vld )
	,	.dp_Switch_east_to_Link_sc01_Data( w_dp_Switch_east_to_Link_sc01_Data )
	,	.dp_Switch_east_to_Link_sc01_Head( w_dp_Switch_east_to_Link_sc01_Head )
	,	.dp_Switch_east_to_Link_sc01_Rdy( w_dp_Switch_east_to_Link_sc01_Rdy )
	,	.dp_Switch_east_to_Link_sc01_Tail( w_dp_Switch_east_to_Link_sc01_Tail )
	,	.dp_Switch_east_to_Link_sc01_Vld( w_dp_Switch_east_to_Link_sc01_Vld )
	);

	dbus_write_Structure_Module_southbus u_dbus_write_Structure_Module_southbus(
		.TM( TM )
	,	.arstn_dbus( arstn_dbus )
	,	.clk_dbus( clk_dbus )
	,	.dp_Link_c0_asi_to_Link_c0_ast_Data( w_dp_Link_c0_asi_to_Link_c0_ast_Data )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdCnt( w_dp_Link_c0_asi_to_Link_c0_ast_RdCnt )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdPtr( w_dp_Link_c0_asi_to_Link_c0_ast_RdPtr )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst( w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck( w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst( w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck( w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_WrCnt( w_dp_Link_c0_asi_to_Link_c0_ast_WrCnt )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_Data( w_dp_Link_c0_astResp_to_Link_c0_asiResp_Data )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt( w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr( w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst( w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst( w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt( w_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Data( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Data )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Head( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Head )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail )
	,	.dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld( w_dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Data( w_dp_Link_c0s_b_to_Link_c0e_a_Data )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Head( w_dp_Link_c0s_b_to_Link_c0e_a_Head )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Rdy( w_dp_Link_c0s_b_to_Link_c0e_a_Rdy )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Tail( w_dp_Link_c0s_b_to_Link_c0e_a_Tail )
	,	.dp_Link_c0s_b_to_Link_c0e_a_Vld( w_dp_Link_c0s_b_to_Link_c0e_a_Vld )
	);

	dbus_sub_write_Structure i_dbus_sub_write_Structure(
		.TM( TM )
	,	.arstn_sub( arstn_dbus )
	,	.clk_sub( clk_dbus )
	,	.dbus2sub_dcluster0_port0_w_Aw_Addr( dbus2sub_dcluster0_port0_w_Aw_Addr )
	,	.dbus2sub_dcluster0_port0_w_Aw_Burst( dbus2sub_dcluster0_port0_w_Aw_Burst )
	,	.dbus2sub_dcluster0_port0_w_Aw_Cache( dbus2sub_dcluster0_port0_w_Aw_Cache )
	,	.dbus2sub_dcluster0_port0_w_Aw_Id( dbus2sub_dcluster0_port0_w_Aw_Id )
	,	.dbus2sub_dcluster0_port0_w_Aw_Len( dbus2sub_dcluster0_port0_w_Aw_Len )
	,	.dbus2sub_dcluster0_port0_w_Aw_Lock( dbus2sub_dcluster0_port0_w_Aw_Lock )
	,	.dbus2sub_dcluster0_port0_w_Aw_Prot( dbus2sub_dcluster0_port0_w_Aw_Prot )
	,	.dbus2sub_dcluster0_port0_w_Aw_Ready( dbus2sub_dcluster0_port0_w_Aw_Ready )
	,	.dbus2sub_dcluster0_port0_w_Aw_Size( dbus2sub_dcluster0_port0_w_Aw_Size )
	,	.dbus2sub_dcluster0_port0_w_Aw_User( dbus2sub_dcluster0_port0_w_Aw_User )
	,	.dbus2sub_dcluster0_port0_w_Aw_Valid( dbus2sub_dcluster0_port0_w_Aw_Valid )
	,	.dbus2sub_dcluster0_port0_w_B_Id( dbus2sub_dcluster0_port0_w_B_Id )
	,	.dbus2sub_dcluster0_port0_w_B_Ready( dbus2sub_dcluster0_port0_w_B_Ready )
	,	.dbus2sub_dcluster0_port0_w_B_Resp( dbus2sub_dcluster0_port0_w_B_Resp )
	,	.dbus2sub_dcluster0_port0_w_B_User( dbus2sub_dcluster0_port0_w_B_User )
	,	.dbus2sub_dcluster0_port0_w_B_Valid( dbus2sub_dcluster0_port0_w_B_Valid )
	,	.dbus2sub_dcluster0_port0_w_W_Data( dbus2sub_dcluster0_port0_w_W_Data )
	,	.dbus2sub_dcluster0_port0_w_W_Last( dbus2sub_dcluster0_port0_w_W_Last )
	,	.dbus2sub_dcluster0_port0_w_W_Ready( dbus2sub_dcluster0_port0_w_W_Ready )
	,	.dbus2sub_dcluster0_port0_w_W_Strb( dbus2sub_dcluster0_port0_w_W_Strb )
	,	.dbus2sub_dcluster0_port0_w_W_Valid( dbus2sub_dcluster0_port0_w_W_Valid )
	,	.dbus2sub_dcluster0_port1_w_Aw_Addr( dbus2sub_dcluster0_port1_w_Aw_Addr )
	,	.dbus2sub_dcluster0_port1_w_Aw_Burst( dbus2sub_dcluster0_port1_w_Aw_Burst )
	,	.dbus2sub_dcluster0_port1_w_Aw_Cache( dbus2sub_dcluster0_port1_w_Aw_Cache )
	,	.dbus2sub_dcluster0_port1_w_Aw_Id( dbus2sub_dcluster0_port1_w_Aw_Id )
	,	.dbus2sub_dcluster0_port1_w_Aw_Len( dbus2sub_dcluster0_port1_w_Aw_Len )
	,	.dbus2sub_dcluster0_port1_w_Aw_Lock( dbus2sub_dcluster0_port1_w_Aw_Lock )
	,	.dbus2sub_dcluster0_port1_w_Aw_Prot( dbus2sub_dcluster0_port1_w_Aw_Prot )
	,	.dbus2sub_dcluster0_port1_w_Aw_Ready( dbus2sub_dcluster0_port1_w_Aw_Ready )
	,	.dbus2sub_dcluster0_port1_w_Aw_Size( dbus2sub_dcluster0_port1_w_Aw_Size )
	,	.dbus2sub_dcluster0_port1_w_Aw_User( dbus2sub_dcluster0_port1_w_Aw_User )
	,	.dbus2sub_dcluster0_port1_w_Aw_Valid( dbus2sub_dcluster0_port1_w_Aw_Valid )
	,	.dbus2sub_dcluster0_port1_w_B_Id( dbus2sub_dcluster0_port1_w_B_Id )
	,	.dbus2sub_dcluster0_port1_w_B_Ready( dbus2sub_dcluster0_port1_w_B_Ready )
	,	.dbus2sub_dcluster0_port1_w_B_Resp( dbus2sub_dcluster0_port1_w_B_Resp )
	,	.dbus2sub_dcluster0_port1_w_B_User( dbus2sub_dcluster0_port1_w_B_User )
	,	.dbus2sub_dcluster0_port1_w_B_Valid( dbus2sub_dcluster0_port1_w_B_Valid )
	,	.dbus2sub_dcluster0_port1_w_W_Data( dbus2sub_dcluster0_port1_w_W_Data )
	,	.dbus2sub_dcluster0_port1_w_W_Last( dbus2sub_dcluster0_port1_w_W_Last )
	,	.dbus2sub_dcluster0_port1_w_W_Ready( dbus2sub_dcluster0_port1_w_W_Ready )
	,	.dbus2sub_dcluster0_port1_w_W_Strb( dbus2sub_dcluster0_port1_w_W_Strb )
	,	.dbus2sub_dcluster0_port1_w_W_Valid( dbus2sub_dcluster0_port1_w_W_Valid )
	,	.dbus2sub_dcluster0_port2_w_Aw_Addr( dbus2sub_dcluster0_port2_w_Aw_Addr )
	,	.dbus2sub_dcluster0_port2_w_Aw_Burst( dbus2sub_dcluster0_port2_w_Aw_Burst )
	,	.dbus2sub_dcluster0_port2_w_Aw_Cache( dbus2sub_dcluster0_port2_w_Aw_Cache )
	,	.dbus2sub_dcluster0_port2_w_Aw_Id( dbus2sub_dcluster0_port2_w_Aw_Id )
	,	.dbus2sub_dcluster0_port2_w_Aw_Len( dbus2sub_dcluster0_port2_w_Aw_Len )
	,	.dbus2sub_dcluster0_port2_w_Aw_Lock( dbus2sub_dcluster0_port2_w_Aw_Lock )
	,	.dbus2sub_dcluster0_port2_w_Aw_Prot( dbus2sub_dcluster0_port2_w_Aw_Prot )
	,	.dbus2sub_dcluster0_port2_w_Aw_Ready( dbus2sub_dcluster0_port2_w_Aw_Ready )
	,	.dbus2sub_dcluster0_port2_w_Aw_Size( dbus2sub_dcluster0_port2_w_Aw_Size )
	,	.dbus2sub_dcluster0_port2_w_Aw_User( dbus2sub_dcluster0_port2_w_Aw_User )
	,	.dbus2sub_dcluster0_port2_w_Aw_Valid( dbus2sub_dcluster0_port2_w_Aw_Valid )
	,	.dbus2sub_dcluster0_port2_w_B_Id( dbus2sub_dcluster0_port2_w_B_Id )
	,	.dbus2sub_dcluster0_port2_w_B_Ready( dbus2sub_dcluster0_port2_w_B_Ready )
	,	.dbus2sub_dcluster0_port2_w_B_Resp( dbus2sub_dcluster0_port2_w_B_Resp )
	,	.dbus2sub_dcluster0_port2_w_B_User( dbus2sub_dcluster0_port2_w_B_User )
	,	.dbus2sub_dcluster0_port2_w_B_Valid( dbus2sub_dcluster0_port2_w_B_Valid )
	,	.dbus2sub_dcluster0_port2_w_W_Data( dbus2sub_dcluster0_port2_w_W_Data )
	,	.dbus2sub_dcluster0_port2_w_W_Last( dbus2sub_dcluster0_port2_w_W_Last )
	,	.dbus2sub_dcluster0_port2_w_W_Ready( dbus2sub_dcluster0_port2_w_W_Ready )
	,	.dbus2sub_dcluster0_port2_w_W_Strb( dbus2sub_dcluster0_port2_w_W_Strb )
	,	.dbus2sub_dcluster0_port2_w_W_Valid( dbus2sub_dcluster0_port2_w_W_Valid )
	,	.dbus2sub_dcluster0_port3_w_Aw_Addr( dbus2sub_dcluster0_port3_w_Aw_Addr )
	,	.dbus2sub_dcluster0_port3_w_Aw_Burst( dbus2sub_dcluster0_port3_w_Aw_Burst )
	,	.dbus2sub_dcluster0_port3_w_Aw_Cache( dbus2sub_dcluster0_port3_w_Aw_Cache )
	,	.dbus2sub_dcluster0_port3_w_Aw_Id( dbus2sub_dcluster0_port3_w_Aw_Id )
	,	.dbus2sub_dcluster0_port3_w_Aw_Len( dbus2sub_dcluster0_port3_w_Aw_Len )
	,	.dbus2sub_dcluster0_port3_w_Aw_Lock( dbus2sub_dcluster0_port3_w_Aw_Lock )
	,	.dbus2sub_dcluster0_port3_w_Aw_Prot( dbus2sub_dcluster0_port3_w_Aw_Prot )
	,	.dbus2sub_dcluster0_port3_w_Aw_Ready( dbus2sub_dcluster0_port3_w_Aw_Ready )
	,	.dbus2sub_dcluster0_port3_w_Aw_Size( dbus2sub_dcluster0_port3_w_Aw_Size )
	,	.dbus2sub_dcluster0_port3_w_Aw_User( dbus2sub_dcluster0_port3_w_Aw_User )
	,	.dbus2sub_dcluster0_port3_w_Aw_Valid( dbus2sub_dcluster0_port3_w_Aw_Valid )
	,	.dbus2sub_dcluster0_port3_w_B_Id( dbus2sub_dcluster0_port3_w_B_Id )
	,	.dbus2sub_dcluster0_port3_w_B_Ready( dbus2sub_dcluster0_port3_w_B_Ready )
	,	.dbus2sub_dcluster0_port3_w_B_Resp( dbus2sub_dcluster0_port3_w_B_Resp )
	,	.dbus2sub_dcluster0_port3_w_B_User( dbus2sub_dcluster0_port3_w_B_User )
	,	.dbus2sub_dcluster0_port3_w_B_Valid( dbus2sub_dcluster0_port3_w_B_Valid )
	,	.dbus2sub_dcluster0_port3_w_W_Data( dbus2sub_dcluster0_port3_w_W_Data )
	,	.dbus2sub_dcluster0_port3_w_W_Last( dbus2sub_dcluster0_port3_w_W_Last )
	,	.dbus2sub_dcluster0_port3_w_W_Ready( dbus2sub_dcluster0_port3_w_W_Ready )
	,	.dbus2sub_dcluster0_port3_w_W_Strb( dbus2sub_dcluster0_port3_w_W_Strb )
	,	.dbus2sub_dcluster0_port3_w_W_Valid( dbus2sub_dcluster0_port3_w_W_Valid )
	,	.dbus2sub_dcluster1_port0_w_Aw_Addr( dbus2sub_dcluster1_port0_w_Aw_Addr )
	,	.dbus2sub_dcluster1_port0_w_Aw_Burst( dbus2sub_dcluster1_port0_w_Aw_Burst )
	,	.dbus2sub_dcluster1_port0_w_Aw_Cache( dbus2sub_dcluster1_port0_w_Aw_Cache )
	,	.dbus2sub_dcluster1_port0_w_Aw_Id( dbus2sub_dcluster1_port0_w_Aw_Id )
	,	.dbus2sub_dcluster1_port0_w_Aw_Len( dbus2sub_dcluster1_port0_w_Aw_Len )
	,	.dbus2sub_dcluster1_port0_w_Aw_Lock( dbus2sub_dcluster1_port0_w_Aw_Lock )
	,	.dbus2sub_dcluster1_port0_w_Aw_Prot( dbus2sub_dcluster1_port0_w_Aw_Prot )
	,	.dbus2sub_dcluster1_port0_w_Aw_Ready( dbus2sub_dcluster1_port0_w_Aw_Ready )
	,	.dbus2sub_dcluster1_port0_w_Aw_Size( dbus2sub_dcluster1_port0_w_Aw_Size )
	,	.dbus2sub_dcluster1_port0_w_Aw_User( dbus2sub_dcluster1_port0_w_Aw_User )
	,	.dbus2sub_dcluster1_port0_w_Aw_Valid( dbus2sub_dcluster1_port0_w_Aw_Valid )
	,	.dbus2sub_dcluster1_port0_w_B_Id( dbus2sub_dcluster1_port0_w_B_Id )
	,	.dbus2sub_dcluster1_port0_w_B_Ready( dbus2sub_dcluster1_port0_w_B_Ready )
	,	.dbus2sub_dcluster1_port0_w_B_Resp( dbus2sub_dcluster1_port0_w_B_Resp )
	,	.dbus2sub_dcluster1_port0_w_B_User( dbus2sub_dcluster1_port0_w_B_User )
	,	.dbus2sub_dcluster1_port0_w_B_Valid( dbus2sub_dcluster1_port0_w_B_Valid )
	,	.dbus2sub_dcluster1_port0_w_W_Data( dbus2sub_dcluster1_port0_w_W_Data )
	,	.dbus2sub_dcluster1_port0_w_W_Last( dbus2sub_dcluster1_port0_w_W_Last )
	,	.dbus2sub_dcluster1_port0_w_W_Ready( dbus2sub_dcluster1_port0_w_W_Ready )
	,	.dbus2sub_dcluster1_port0_w_W_Strb( dbus2sub_dcluster1_port0_w_W_Strb )
	,	.dbus2sub_dcluster1_port0_w_W_Valid( dbus2sub_dcluster1_port0_w_W_Valid )
	,	.dbus2sub_dcluster1_port1_w_Aw_Addr( dbus2sub_dcluster1_port1_w_Aw_Addr )
	,	.dbus2sub_dcluster1_port1_w_Aw_Burst( dbus2sub_dcluster1_port1_w_Aw_Burst )
	,	.dbus2sub_dcluster1_port1_w_Aw_Cache( dbus2sub_dcluster1_port1_w_Aw_Cache )
	,	.dbus2sub_dcluster1_port1_w_Aw_Id( dbus2sub_dcluster1_port1_w_Aw_Id )
	,	.dbus2sub_dcluster1_port1_w_Aw_Len( dbus2sub_dcluster1_port1_w_Aw_Len )
	,	.dbus2sub_dcluster1_port1_w_Aw_Lock( dbus2sub_dcluster1_port1_w_Aw_Lock )
	,	.dbus2sub_dcluster1_port1_w_Aw_Prot( dbus2sub_dcluster1_port1_w_Aw_Prot )
	,	.dbus2sub_dcluster1_port1_w_Aw_Ready( dbus2sub_dcluster1_port1_w_Aw_Ready )
	,	.dbus2sub_dcluster1_port1_w_Aw_Size( dbus2sub_dcluster1_port1_w_Aw_Size )
	,	.dbus2sub_dcluster1_port1_w_Aw_User( dbus2sub_dcluster1_port1_w_Aw_User )
	,	.dbus2sub_dcluster1_port1_w_Aw_Valid( dbus2sub_dcluster1_port1_w_Aw_Valid )
	,	.dbus2sub_dcluster1_port1_w_B_Id( dbus2sub_dcluster1_port1_w_B_Id )
	,	.dbus2sub_dcluster1_port1_w_B_Ready( dbus2sub_dcluster1_port1_w_B_Ready )
	,	.dbus2sub_dcluster1_port1_w_B_Resp( dbus2sub_dcluster1_port1_w_B_Resp )
	,	.dbus2sub_dcluster1_port1_w_B_User( dbus2sub_dcluster1_port1_w_B_User )
	,	.dbus2sub_dcluster1_port1_w_B_Valid( dbus2sub_dcluster1_port1_w_B_Valid )
	,	.dbus2sub_dcluster1_port1_w_W_Data( dbus2sub_dcluster1_port1_w_W_Data )
	,	.dbus2sub_dcluster1_port1_w_W_Last( dbus2sub_dcluster1_port1_w_W_Last )
	,	.dbus2sub_dcluster1_port1_w_W_Ready( dbus2sub_dcluster1_port1_w_W_Ready )
	,	.dbus2sub_dcluster1_port1_w_W_Strb( dbus2sub_dcluster1_port1_w_W_Strb )
	,	.dbus2sub_dcluster1_port1_w_W_Valid( dbus2sub_dcluster1_port1_w_W_Valid )
	,	.dbus2sub_dcluster1_port2_w_Aw_Addr( dbus2sub_dcluster1_port2_w_Aw_Addr )
	,	.dbus2sub_dcluster1_port2_w_Aw_Burst( dbus2sub_dcluster1_port2_w_Aw_Burst )
	,	.dbus2sub_dcluster1_port2_w_Aw_Cache( dbus2sub_dcluster1_port2_w_Aw_Cache )
	,	.dbus2sub_dcluster1_port2_w_Aw_Id( dbus2sub_dcluster1_port2_w_Aw_Id )
	,	.dbus2sub_dcluster1_port2_w_Aw_Len( dbus2sub_dcluster1_port2_w_Aw_Len )
	,	.dbus2sub_dcluster1_port2_w_Aw_Lock( dbus2sub_dcluster1_port2_w_Aw_Lock )
	,	.dbus2sub_dcluster1_port2_w_Aw_Prot( dbus2sub_dcluster1_port2_w_Aw_Prot )
	,	.dbus2sub_dcluster1_port2_w_Aw_Ready( dbus2sub_dcluster1_port2_w_Aw_Ready )
	,	.dbus2sub_dcluster1_port2_w_Aw_Size( dbus2sub_dcluster1_port2_w_Aw_Size )
	,	.dbus2sub_dcluster1_port2_w_Aw_User( dbus2sub_dcluster1_port2_w_Aw_User )
	,	.dbus2sub_dcluster1_port2_w_Aw_Valid( dbus2sub_dcluster1_port2_w_Aw_Valid )
	,	.dbus2sub_dcluster1_port2_w_B_Id( dbus2sub_dcluster1_port2_w_B_Id )
	,	.dbus2sub_dcluster1_port2_w_B_Ready( dbus2sub_dcluster1_port2_w_B_Ready )
	,	.dbus2sub_dcluster1_port2_w_B_Resp( dbus2sub_dcluster1_port2_w_B_Resp )
	,	.dbus2sub_dcluster1_port2_w_B_User( dbus2sub_dcluster1_port2_w_B_User )
	,	.dbus2sub_dcluster1_port2_w_B_Valid( dbus2sub_dcluster1_port2_w_B_Valid )
	,	.dbus2sub_dcluster1_port2_w_W_Data( dbus2sub_dcluster1_port2_w_W_Data )
	,	.dbus2sub_dcluster1_port2_w_W_Last( dbus2sub_dcluster1_port2_w_W_Last )
	,	.dbus2sub_dcluster1_port2_w_W_Ready( dbus2sub_dcluster1_port2_w_W_Ready )
	,	.dbus2sub_dcluster1_port2_w_W_Strb( dbus2sub_dcluster1_port2_w_W_Strb )
	,	.dbus2sub_dcluster1_port2_w_W_Valid( dbus2sub_dcluster1_port2_w_W_Valid )
	,	.dbus2sub_dcluster1_port3_w_Aw_Addr( dbus2sub_dcluster1_port3_w_Aw_Addr )
	,	.dbus2sub_dcluster1_port3_w_Aw_Burst( dbus2sub_dcluster1_port3_w_Aw_Burst )
	,	.dbus2sub_dcluster1_port3_w_Aw_Cache( dbus2sub_dcluster1_port3_w_Aw_Cache )
	,	.dbus2sub_dcluster1_port3_w_Aw_Id( dbus2sub_dcluster1_port3_w_Aw_Id )
	,	.dbus2sub_dcluster1_port3_w_Aw_Len( dbus2sub_dcluster1_port3_w_Aw_Len )
	,	.dbus2sub_dcluster1_port3_w_Aw_Lock( dbus2sub_dcluster1_port3_w_Aw_Lock )
	,	.dbus2sub_dcluster1_port3_w_Aw_Prot( dbus2sub_dcluster1_port3_w_Aw_Prot )
	,	.dbus2sub_dcluster1_port3_w_Aw_Ready( dbus2sub_dcluster1_port3_w_Aw_Ready )
	,	.dbus2sub_dcluster1_port3_w_Aw_Size( dbus2sub_dcluster1_port3_w_Aw_Size )
	,	.dbus2sub_dcluster1_port3_w_Aw_User( dbus2sub_dcluster1_port3_w_Aw_User )
	,	.dbus2sub_dcluster1_port3_w_Aw_Valid( dbus2sub_dcluster1_port3_w_Aw_Valid )
	,	.dbus2sub_dcluster1_port3_w_B_Id( dbus2sub_dcluster1_port3_w_B_Id )
	,	.dbus2sub_dcluster1_port3_w_B_Ready( dbus2sub_dcluster1_port3_w_B_Ready )
	,	.dbus2sub_dcluster1_port3_w_B_Resp( dbus2sub_dcluster1_port3_w_B_Resp )
	,	.dbus2sub_dcluster1_port3_w_B_User( dbus2sub_dcluster1_port3_w_B_User )
	,	.dbus2sub_dcluster1_port3_w_B_Valid( dbus2sub_dcluster1_port3_w_B_Valid )
	,	.dbus2sub_dcluster1_port3_w_W_Data( dbus2sub_dcluster1_port3_w_W_Data )
	,	.dbus2sub_dcluster1_port3_w_W_Last( dbus2sub_dcluster1_port3_w_W_Last )
	,	.dbus2sub_dcluster1_port3_w_W_Ready( dbus2sub_dcluster1_port3_w_W_Ready )
	,	.dbus2sub_dcluster1_port3_w_W_Strb( dbus2sub_dcluster1_port3_w_W_Strb )
	,	.dbus2sub_dcluster1_port3_w_W_Valid( dbus2sub_dcluster1_port3_w_W_Valid )
	,	.dbus2sub_ddma_w_Aw_Addr( dbus2sub_ddma_w_Aw_Addr )
	,	.dbus2sub_ddma_w_Aw_Burst( dbus2sub_ddma_w_Aw_Burst )
	,	.dbus2sub_ddma_w_Aw_Cache( dbus2sub_ddma_w_Aw_Cache )
	,	.dbus2sub_ddma_w_Aw_Id( dbus2sub_ddma_w_Aw_Id )
	,	.dbus2sub_ddma_w_Aw_Len( dbus2sub_ddma_w_Aw_Len )
	,	.dbus2sub_ddma_w_Aw_Lock( dbus2sub_ddma_w_Aw_Lock )
	,	.dbus2sub_ddma_w_Aw_Prot( dbus2sub_ddma_w_Aw_Prot )
	,	.dbus2sub_ddma_w_Aw_Ready( dbus2sub_ddma_w_Aw_Ready )
	,	.dbus2sub_ddma_w_Aw_Size( dbus2sub_ddma_w_Aw_Size )
	,	.dbus2sub_ddma_w_Aw_User( dbus2sub_ddma_w_Aw_User )
	,	.dbus2sub_ddma_w_Aw_Valid( dbus2sub_ddma_w_Aw_Valid )
	,	.dbus2sub_ddma_w_B_Id( dbus2sub_ddma_w_B_Id )
	,	.dbus2sub_ddma_w_B_Ready( dbus2sub_ddma_w_B_Ready )
	,	.dbus2sub_ddma_w_B_Resp( dbus2sub_ddma_w_B_Resp )
	,	.dbus2sub_ddma_w_B_User( dbus2sub_ddma_w_B_User )
	,	.dbus2sub_ddma_w_B_Valid( dbus2sub_ddma_w_B_Valid )
	,	.dbus2sub_ddma_w_W_Data( dbus2sub_ddma_w_W_Data )
	,	.dbus2sub_ddma_w_W_Last( dbus2sub_ddma_w_W_Last )
	,	.dbus2sub_ddma_w_W_Ready( dbus2sub_ddma_w_W_Ready )
	,	.dbus2sub_ddma_w_W_Strb( dbus2sub_ddma_w_W_Strb )
	,	.dbus2sub_ddma_w_W_Valid( dbus2sub_ddma_w_W_Valid )
	,	.dbus2sub_pcie_cpu_w_Aw_Addr( dbus2sub_pcie_cpu_w_Aw_Addr )
	,	.dbus2sub_pcie_cpu_w_Aw_Burst( dbus2sub_pcie_cpu_w_Aw_Burst )
	,	.dbus2sub_pcie_cpu_w_Aw_Cache( dbus2sub_pcie_cpu_w_Aw_Cache )
	,	.dbus2sub_pcie_cpu_w_Aw_Id( dbus2sub_pcie_cpu_w_Aw_Id )
	,	.dbus2sub_pcie_cpu_w_Aw_Len( dbus2sub_pcie_cpu_w_Aw_Len )
	,	.dbus2sub_pcie_cpu_w_Aw_Lock( dbus2sub_pcie_cpu_w_Aw_Lock )
	,	.dbus2sub_pcie_cpu_w_Aw_Prot( dbus2sub_pcie_cpu_w_Aw_Prot )
	,	.dbus2sub_pcie_cpu_w_Aw_Ready( dbus2sub_pcie_cpu_w_Aw_Ready )
	,	.dbus2sub_pcie_cpu_w_Aw_Size( dbus2sub_pcie_cpu_w_Aw_Size )
	,	.dbus2sub_pcie_cpu_w_Aw_User( dbus2sub_pcie_cpu_w_Aw_User )
	,	.dbus2sub_pcie_cpu_w_Aw_Valid( dbus2sub_pcie_cpu_w_Aw_Valid )
	,	.dbus2sub_pcie_cpu_w_B_Id( dbus2sub_pcie_cpu_w_B_Id )
	,	.dbus2sub_pcie_cpu_w_B_Ready( dbus2sub_pcie_cpu_w_B_Ready )
	,	.dbus2sub_pcie_cpu_w_B_Resp( dbus2sub_pcie_cpu_w_B_Resp )
	,	.dbus2sub_pcie_cpu_w_B_User( dbus2sub_pcie_cpu_w_B_User )
	,	.dbus2sub_pcie_cpu_w_B_Valid( dbus2sub_pcie_cpu_w_B_Valid )
	,	.dbus2sub_pcie_cpu_w_W_Data( dbus2sub_pcie_cpu_w_W_Data )
	,	.dbus2sub_pcie_cpu_w_W_Last( dbus2sub_pcie_cpu_w_W_Last )
	,	.dbus2sub_pcie_cpu_w_W_Ready( dbus2sub_pcie_cpu_w_W_Ready )
	,	.dbus2sub_pcie_cpu_w_W_Strb( dbus2sub_pcie_cpu_w_W_Strb )
	,	.dbus2sub_pcie_cpu_w_W_Valid( dbus2sub_pcie_cpu_w_W_Valid )
	,	.shm0_w_Aw_Addr( shm0_w_Aw_Addr )
	,	.shm0_w_Aw_Burst( shm0_w_Aw_Burst )
	,	.shm0_w_Aw_Cache( shm0_w_Aw_Cache )
	,	.shm0_w_Aw_Id( shm0_w_Aw_Id )
	,	.shm0_w_Aw_Len( shm0_w_Aw_Len )
	,	.shm0_w_Aw_Lock( shm0_w_Aw_Lock )
	,	.shm0_w_Aw_Prot( shm0_w_Aw_Prot )
	,	.shm0_w_Aw_Ready( shm0_w_Aw_Ready )
	,	.shm0_w_Aw_Size( shm0_w_Aw_Size )
	,	.shm0_w_Aw_User( shm0_w_Aw_User )
	,	.shm0_w_Aw_Valid( shm0_w_Aw_Valid )
	,	.shm0_w_B_Id( shm0_w_B_Id )
	,	.shm0_w_B_Ready( shm0_w_B_Ready )
	,	.shm0_w_B_Resp( shm0_w_B_Resp )
	,	.shm0_w_B_User( shm0_w_B_User )
	,	.shm0_w_B_Valid( shm0_w_B_Valid )
	,	.shm0_w_W_Data( shm0_w_W_Data )
	,	.shm0_w_W_Last( shm0_w_W_Last )
	,	.shm0_w_W_Ready( shm0_w_W_Ready )
	,	.shm0_w_W_Strb( shm0_w_W_Strb )
	,	.shm0_w_W_Valid( shm0_w_W_Valid )
	,	.shm1_w_Aw_Addr( shm1_w_Aw_Addr )
	,	.shm1_w_Aw_Burst( shm1_w_Aw_Burst )
	,	.shm1_w_Aw_Cache( shm1_w_Aw_Cache )
	,	.shm1_w_Aw_Id( shm1_w_Aw_Id )
	,	.shm1_w_Aw_Len( shm1_w_Aw_Len )
	,	.shm1_w_Aw_Lock( shm1_w_Aw_Lock )
	,	.shm1_w_Aw_Prot( shm1_w_Aw_Prot )
	,	.shm1_w_Aw_Ready( shm1_w_Aw_Ready )
	,	.shm1_w_Aw_Size( shm1_w_Aw_Size )
	,	.shm1_w_Aw_User( shm1_w_Aw_User )
	,	.shm1_w_Aw_Valid( shm1_w_Aw_Valid )
	,	.shm1_w_B_Id( shm1_w_B_Id )
	,	.shm1_w_B_Ready( shm1_w_B_Ready )
	,	.shm1_w_B_Resp( shm1_w_B_Resp )
	,	.shm1_w_B_User( shm1_w_B_User )
	,	.shm1_w_B_Valid( shm1_w_B_Valid )
	,	.shm1_w_W_Data( shm1_w_W_Data )
	,	.shm1_w_W_Last( shm1_w_W_Last )
	,	.shm1_w_W_Ready( shm1_w_W_Ready )
	,	.shm1_w_W_Strb( shm1_w_W_Strb )
	,	.shm1_w_W_Valid( shm1_w_W_Valid )
	,	.shm2_w_Aw_Addr( shm2_w_Aw_Addr )
	,	.shm2_w_Aw_Burst( shm2_w_Aw_Burst )
	,	.shm2_w_Aw_Cache( shm2_w_Aw_Cache )
	,	.shm2_w_Aw_Id( shm2_w_Aw_Id )
	,	.shm2_w_Aw_Len( shm2_w_Aw_Len )
	,	.shm2_w_Aw_Lock( shm2_w_Aw_Lock )
	,	.shm2_w_Aw_Prot( shm2_w_Aw_Prot )
	,	.shm2_w_Aw_Ready( shm2_w_Aw_Ready )
	,	.shm2_w_Aw_Size( shm2_w_Aw_Size )
	,	.shm2_w_Aw_User( shm2_w_Aw_User )
	,	.shm2_w_Aw_Valid( shm2_w_Aw_Valid )
	,	.shm2_w_B_Id( shm2_w_B_Id )
	,	.shm2_w_B_Ready( shm2_w_B_Ready )
	,	.shm2_w_B_Resp( shm2_w_B_Resp )
	,	.shm2_w_B_User( shm2_w_B_User )
	,	.shm2_w_B_Valid( shm2_w_B_Valid )
	,	.shm2_w_W_Data( shm2_w_W_Data )
	,	.shm2_w_W_Last( shm2_w_W_Last )
	,	.shm2_w_W_Ready( shm2_w_W_Ready )
	,	.shm2_w_W_Strb( shm2_w_W_Strb )
	,	.shm2_w_W_Valid( shm2_w_W_Valid )
	,	.shm3_w_Aw_Addr( shm3_w_Aw_Addr )
	,	.shm3_w_Aw_Burst( shm3_w_Aw_Burst )
	,	.shm3_w_Aw_Cache( shm3_w_Aw_Cache )
	,	.shm3_w_Aw_Id( shm3_w_Aw_Id )
	,	.shm3_w_Aw_Len( shm3_w_Aw_Len )
	,	.shm3_w_Aw_Lock( shm3_w_Aw_Lock )
	,	.shm3_w_Aw_Prot( shm3_w_Aw_Prot )
	,	.shm3_w_Aw_Ready( shm3_w_Aw_Ready )
	,	.shm3_w_Aw_Size( shm3_w_Aw_Size )
	,	.shm3_w_Aw_User( shm3_w_Aw_User )
	,	.shm3_w_Aw_Valid( shm3_w_Aw_Valid )
	,	.shm3_w_B_Id( shm3_w_B_Id )
	,	.shm3_w_B_Ready( shm3_w_B_Ready )
	,	.shm3_w_B_Resp( shm3_w_B_Resp )
	,	.shm3_w_B_User( shm3_w_B_User )
	,	.shm3_w_B_Valid( shm3_w_B_Valid )
	,	.shm3_w_W_Data( shm3_w_W_Data )
	,	.shm3_w_W_Last( shm3_w_W_Last )
	,	.shm3_w_W_Ready( shm3_w_W_Ready )
	,	.shm3_w_W_Strb( shm3_w_W_Strb )
	,	.shm3_w_W_Valid( shm3_w_W_Valid )
	);


//--------------------------------------------------------------------------------------
// cbus


	cbus_Structure_Module_centerbus u_cbus_Structure_Module_centerbus(
		.TM( TM )
	,	.arstn_cbus( arstn_cbus )
	,	.clk_cbus( clk_cbus )
	,	.ddma_ctrl_Ar_Addr( ddma_ctrl_Ar_Addr )
	,	.ddma_ctrl_Ar_Burst( ddma_ctrl_Ar_Burst )
	,	.ddma_ctrl_Ar_Cache( ddma_ctrl_Ar_Cache )
	,	.ddma_ctrl_Ar_Id( ddma_ctrl_Ar_Id )
	,	.ddma_ctrl_Ar_Len( ddma_ctrl_Ar_Len )
	,	.ddma_ctrl_Ar_Lock( ddma_ctrl_Ar_Lock )
	,	.ddma_ctrl_Ar_Prot( ddma_ctrl_Ar_Prot )
	,	.ddma_ctrl_Ar_Ready( ddma_ctrl_Ar_Ready )
	,	.ddma_ctrl_Ar_Size( ddma_ctrl_Ar_Size )
	,	.ddma_ctrl_Ar_Valid( ddma_ctrl_Ar_Valid )
	,	.ddma_ctrl_Aw_Addr( ddma_ctrl_Aw_Addr )
	,	.ddma_ctrl_Aw_Burst( ddma_ctrl_Aw_Burst )
	,	.ddma_ctrl_Aw_Cache( ddma_ctrl_Aw_Cache )
	,	.ddma_ctrl_Aw_Id( ddma_ctrl_Aw_Id )
	,	.ddma_ctrl_Aw_Len( ddma_ctrl_Aw_Len )
	,	.ddma_ctrl_Aw_Lock( ddma_ctrl_Aw_Lock )
	,	.ddma_ctrl_Aw_Prot( ddma_ctrl_Aw_Prot )
	,	.ddma_ctrl_Aw_Ready( ddma_ctrl_Aw_Ready )
	,	.ddma_ctrl_Aw_Size( ddma_ctrl_Aw_Size )
	,	.ddma_ctrl_Aw_Valid( ddma_ctrl_Aw_Valid )
	,	.ddma_ctrl_B_Id( ddma_ctrl_B_Id )
	,	.ddma_ctrl_B_Ready( ddma_ctrl_B_Ready )
	,	.ddma_ctrl_B_Resp( ddma_ctrl_B_Resp )
	,	.ddma_ctrl_B_Valid( ddma_ctrl_B_Valid )
	,	.ddma_ctrl_R_Data( ddma_ctrl_R_Data )
	,	.ddma_ctrl_R_Id( ddma_ctrl_R_Id )
	,	.ddma_ctrl_R_Last( ddma_ctrl_R_Last )
	,	.ddma_ctrl_R_Ready( ddma_ctrl_R_Ready )
	,	.ddma_ctrl_R_Resp( ddma_ctrl_R_Resp )
	,	.ddma_ctrl_R_Valid( ddma_ctrl_R_Valid )
	,	.ddma_ctrl_W_Data( ddma_ctrl_W_Data )
	,	.ddma_ctrl_W_Last( ddma_ctrl_W_Last )
	,	.ddma_ctrl_W_Ready( ddma_ctrl_W_Ready )
	,	.ddma_ctrl_W_Strb( ddma_ctrl_W_Strb )
	,	.ddma_ctrl_W_Valid( ddma_ctrl_W_Valid )
	,	.dp_Link2_to_Switch_west_Data( ctrl_dp_Link2_to_Switch_west_Data )
	,	.dp_Link2_to_Switch_west_RdCnt( ctrl_dp_Link2_to_Switch_west_RdCnt )
	,	.dp_Link2_to_Switch_west_RdPtr( ctrl_dp_Link2_to_Switch_west_RdPtr )
	,	.dp_Link2_to_Switch_west_RxCtl_PwrOnRst( ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRst )
	,	.dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck( ctrl_dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck )
	,	.dp_Link2_to_Switch_west_TxCtl_PwrOnRst( ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRst )
	,	.dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck( ctrl_dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck )
	,	.dp_Link2_to_Switch_west_WrCnt( ctrl_dp_Link2_to_Switch_west_WrCnt )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Data( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Data )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Head( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Head )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Rdy( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Rdy )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Tail( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Tail )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Vld( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Vld )
	,	.dp_Link_n03_asi_to_Link_n03_ast_Data( ctrl_dp_Link_n03_asi_to_Link_n03_ast_Data )
	,	.dp_Link_n03_asi_to_Link_n03_ast_RdCnt( ctrl_dp_Link_n03_asi_to_Link_n03_ast_RdCnt )
	,	.dp_Link_n03_asi_to_Link_n03_ast_RdPtr( ctrl_dp_Link_n03_asi_to_Link_n03_ast_RdPtr )
	,	.dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRst( ctrl_dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRst( ctrl_dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n03_asi_to_Link_n03_ast_WrCnt( ctrl_dp_Link_n03_asi_to_Link_n03_ast_WrCnt )
	,	.dp_Link_n03_astResp001_to_Link_n03_asiResp001_Data( ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_Data )
	,	.dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdCnt( ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdCnt )
	,	.dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdPtr( ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdPtr )
	,	.dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n03_astResp001_to_Link_n03_asiResp001_WrCnt( ctrl_dp_Link_n03_astResp001_to_Link_n03_asiResp001_WrCnt )
	,	.dp_Link_n47_asi_to_Link_n47_ast_Data( ctrl_dp_Link_n47_asi_to_Link_n47_ast_Data )
	,	.dp_Link_n47_asi_to_Link_n47_ast_RdCnt( ctrl_dp_Link_n47_asi_to_Link_n47_ast_RdCnt )
	,	.dp_Link_n47_asi_to_Link_n47_ast_RdPtr( ctrl_dp_Link_n47_asi_to_Link_n47_ast_RdPtr )
	,	.dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRst( ctrl_dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRst )
	,	.dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRst( ctrl_dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRst )
	,	.dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_n47_asi_to_Link_n47_ast_WrCnt( ctrl_dp_Link_n47_asi_to_Link_n47_ast_WrCnt )
	,	.dp_Link_n47_astResp001_to_Link_n47_asiResp001_Data( ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_Data )
	,	.dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdCnt( ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdCnt )
	,	.dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdPtr( ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdPtr )
	,	.dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_n47_astResp001_to_Link_n47_asiResp001_WrCnt( ctrl_dp_Link_n47_astResp001_to_Link_n47_asiResp001_WrCnt )
	,	.dp_Switch2_to_Link6_Data( ctrl_dp_Switch2_to_Link6_Data )
	,	.dp_Switch2_to_Link6_Head( ctrl_dp_Switch2_to_Link6_Head )
	,	.dp_Switch2_to_Link6_Rdy( ctrl_dp_Switch2_to_Link6_Rdy )
	,	.dp_Switch2_to_Link6_Tail( ctrl_dp_Switch2_to_Link6_Tail )
	,	.dp_Switch2_to_Link6_Vld( ctrl_dp_Switch2_to_Link6_Vld )
	,	.dp_Switch_westResp001_to_Link13Resp001_Data( ctrl_dp_Switch_westResp001_to_Link13Resp001_Data )
	,	.dp_Switch_westResp001_to_Link13Resp001_RdCnt( ctrl_dp_Switch_westResp001_to_Link13Resp001_RdCnt )
	,	.dp_Switch_westResp001_to_Link13Resp001_RdPtr( ctrl_dp_Switch_westResp001_to_Link13Resp001_RdPtr )
	,	.dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst( ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst )
	,	.dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck( ctrl_dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck )
	,	.dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst( ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst )
	,	.dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck( ctrl_dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck )
	,	.dp_Switch_westResp001_to_Link13Resp001_WrCnt( ctrl_dp_Switch_westResp001_to_Link13Resp001_WrCnt )
	,	.shm0_ctrl_Ar_Addr( shm0_ctrl_Ar_Addr )
	,	.shm0_ctrl_Ar_Burst( shm0_ctrl_Ar_Burst )
	,	.shm0_ctrl_Ar_Cache( shm0_ctrl_Ar_Cache )
	,	.shm0_ctrl_Ar_Id( shm0_ctrl_Ar_Id )
	,	.shm0_ctrl_Ar_Len( shm0_ctrl_Ar_Len )
	,	.shm0_ctrl_Ar_Lock( shm0_ctrl_Ar_Lock )
	,	.shm0_ctrl_Ar_Prot( shm0_ctrl_Ar_Prot )
	,	.shm0_ctrl_Ar_Ready( shm0_ctrl_Ar_Ready )
	,	.shm0_ctrl_Ar_Size( shm0_ctrl_Ar_Size )
	,	.shm0_ctrl_Ar_Valid( shm0_ctrl_Ar_Valid )
	,	.shm0_ctrl_Aw_Addr( shm0_ctrl_Aw_Addr )
	,	.shm0_ctrl_Aw_Burst( shm0_ctrl_Aw_Burst )
	,	.shm0_ctrl_Aw_Cache( shm0_ctrl_Aw_Cache )
	,	.shm0_ctrl_Aw_Id( shm0_ctrl_Aw_Id )
	,	.shm0_ctrl_Aw_Len( shm0_ctrl_Aw_Len )
	,	.shm0_ctrl_Aw_Lock( shm0_ctrl_Aw_Lock )
	,	.shm0_ctrl_Aw_Prot( shm0_ctrl_Aw_Prot )
	,	.shm0_ctrl_Aw_Ready( shm0_ctrl_Aw_Ready )
	,	.shm0_ctrl_Aw_Size( shm0_ctrl_Aw_Size )
	,	.shm0_ctrl_Aw_Valid( shm0_ctrl_Aw_Valid )
	,	.shm0_ctrl_B_Id( shm0_ctrl_B_Id )
	,	.shm0_ctrl_B_Ready( shm0_ctrl_B_Ready )
	,	.shm0_ctrl_B_Resp( shm0_ctrl_B_Resp )
	,	.shm0_ctrl_B_Valid( shm0_ctrl_B_Valid )
	,	.shm0_ctrl_R_Data( shm0_ctrl_R_Data )
	,	.shm0_ctrl_R_Id( shm0_ctrl_R_Id )
	,	.shm0_ctrl_R_Last( shm0_ctrl_R_Last )
	,	.shm0_ctrl_R_Ready( shm0_ctrl_R_Ready )
	,	.shm0_ctrl_R_Resp( shm0_ctrl_R_Resp )
	,	.shm0_ctrl_R_Valid( shm0_ctrl_R_Valid )
	,	.shm0_ctrl_W_Data( shm0_ctrl_W_Data )
	,	.shm0_ctrl_W_Last( shm0_ctrl_W_Last )
	,	.shm0_ctrl_W_Ready( shm0_ctrl_W_Ready )
	,	.shm0_ctrl_W_Strb( shm0_ctrl_W_Strb )
	,	.shm0_ctrl_W_Valid( shm0_ctrl_W_Valid )
	,	.shm1_ctrl_Ar_Addr( shm1_ctrl_Ar_Addr )
	,	.shm1_ctrl_Ar_Burst( shm1_ctrl_Ar_Burst )
	,	.shm1_ctrl_Ar_Cache( shm1_ctrl_Ar_Cache )
	,	.shm1_ctrl_Ar_Id( shm1_ctrl_Ar_Id )
	,	.shm1_ctrl_Ar_Len( shm1_ctrl_Ar_Len )
	,	.shm1_ctrl_Ar_Lock( shm1_ctrl_Ar_Lock )
	,	.shm1_ctrl_Ar_Prot( shm1_ctrl_Ar_Prot )
	,	.shm1_ctrl_Ar_Ready( shm1_ctrl_Ar_Ready )
	,	.shm1_ctrl_Ar_Size( shm1_ctrl_Ar_Size )
	,	.shm1_ctrl_Ar_Valid( shm1_ctrl_Ar_Valid )
	,	.shm1_ctrl_Aw_Addr( shm1_ctrl_Aw_Addr )
	,	.shm1_ctrl_Aw_Burst( shm1_ctrl_Aw_Burst )
	,	.shm1_ctrl_Aw_Cache( shm1_ctrl_Aw_Cache )
	,	.shm1_ctrl_Aw_Id( shm1_ctrl_Aw_Id )
	,	.shm1_ctrl_Aw_Len( shm1_ctrl_Aw_Len )
	,	.shm1_ctrl_Aw_Lock( shm1_ctrl_Aw_Lock )
	,	.shm1_ctrl_Aw_Prot( shm1_ctrl_Aw_Prot )
	,	.shm1_ctrl_Aw_Ready( shm1_ctrl_Aw_Ready )
	,	.shm1_ctrl_Aw_Size( shm1_ctrl_Aw_Size )
	,	.shm1_ctrl_Aw_Valid( shm1_ctrl_Aw_Valid )
	,	.shm1_ctrl_B_Id( shm1_ctrl_B_Id )
	,	.shm1_ctrl_B_Ready( shm1_ctrl_B_Ready )
	,	.shm1_ctrl_B_Resp( shm1_ctrl_B_Resp )
	,	.shm1_ctrl_B_Valid( shm1_ctrl_B_Valid )
	,	.shm1_ctrl_R_Data( shm1_ctrl_R_Data )
	,	.shm1_ctrl_R_Id( shm1_ctrl_R_Id )
	,	.shm1_ctrl_R_Last( shm1_ctrl_R_Last )
	,	.shm1_ctrl_R_Ready( shm1_ctrl_R_Ready )
	,	.shm1_ctrl_R_Resp( shm1_ctrl_R_Resp )
	,	.shm1_ctrl_R_Valid( shm1_ctrl_R_Valid )
	,	.shm1_ctrl_W_Data( shm1_ctrl_W_Data )
	,	.shm1_ctrl_W_Last( shm1_ctrl_W_Last )
	,	.shm1_ctrl_W_Ready( shm1_ctrl_W_Ready )
	,	.shm1_ctrl_W_Strb( shm1_ctrl_W_Strb )
	,	.shm1_ctrl_W_Valid( shm1_ctrl_W_Valid )
	,	.shm2_ctrl_Ar_Addr( shm2_ctrl_Ar_Addr )
	,	.shm2_ctrl_Ar_Burst( shm2_ctrl_Ar_Burst )
	,	.shm2_ctrl_Ar_Cache( shm2_ctrl_Ar_Cache )
	,	.shm2_ctrl_Ar_Id( shm2_ctrl_Ar_Id )
	,	.shm2_ctrl_Ar_Len( shm2_ctrl_Ar_Len )
	,	.shm2_ctrl_Ar_Lock( shm2_ctrl_Ar_Lock )
	,	.shm2_ctrl_Ar_Prot( shm2_ctrl_Ar_Prot )
	,	.shm2_ctrl_Ar_Ready( shm2_ctrl_Ar_Ready )
	,	.shm2_ctrl_Ar_Size( shm2_ctrl_Ar_Size )
	,	.shm2_ctrl_Ar_Valid( shm2_ctrl_Ar_Valid )
	,	.shm2_ctrl_Aw_Addr( shm2_ctrl_Aw_Addr )
	,	.shm2_ctrl_Aw_Burst( shm2_ctrl_Aw_Burst )
	,	.shm2_ctrl_Aw_Cache( shm2_ctrl_Aw_Cache )
	,	.shm2_ctrl_Aw_Id( shm2_ctrl_Aw_Id )
	,	.shm2_ctrl_Aw_Len( shm2_ctrl_Aw_Len )
	,	.shm2_ctrl_Aw_Lock( shm2_ctrl_Aw_Lock )
	,	.shm2_ctrl_Aw_Prot( shm2_ctrl_Aw_Prot )
	,	.shm2_ctrl_Aw_Ready( shm2_ctrl_Aw_Ready )
	,	.shm2_ctrl_Aw_Size( shm2_ctrl_Aw_Size )
	,	.shm2_ctrl_Aw_Valid( shm2_ctrl_Aw_Valid )
	,	.shm2_ctrl_B_Id( shm2_ctrl_B_Id )
	,	.shm2_ctrl_B_Ready( shm2_ctrl_B_Ready )
	,	.shm2_ctrl_B_Resp( shm2_ctrl_B_Resp )
	,	.shm2_ctrl_B_Valid( shm2_ctrl_B_Valid )
	,	.shm2_ctrl_R_Data( shm2_ctrl_R_Data )
	,	.shm2_ctrl_R_Id( shm2_ctrl_R_Id )
	,	.shm2_ctrl_R_Last( shm2_ctrl_R_Last )
	,	.shm2_ctrl_R_Ready( shm2_ctrl_R_Ready )
	,	.shm2_ctrl_R_Resp( shm2_ctrl_R_Resp )
	,	.shm2_ctrl_R_Valid( shm2_ctrl_R_Valid )
	,	.shm2_ctrl_W_Data( shm2_ctrl_W_Data )
	,	.shm2_ctrl_W_Last( shm2_ctrl_W_Last )
	,	.shm2_ctrl_W_Ready( shm2_ctrl_W_Ready )
	,	.shm2_ctrl_W_Strb( shm2_ctrl_W_Strb )
	,	.shm2_ctrl_W_Valid( shm2_ctrl_W_Valid )
	,	.shm3_ctrl_Ar_Addr( shm3_ctrl_Ar_Addr )
	,	.shm3_ctrl_Ar_Burst( shm3_ctrl_Ar_Burst )
	,	.shm3_ctrl_Ar_Cache( shm3_ctrl_Ar_Cache )
	,	.shm3_ctrl_Ar_Id( shm3_ctrl_Ar_Id )
	,	.shm3_ctrl_Ar_Len( shm3_ctrl_Ar_Len )
	,	.shm3_ctrl_Ar_Lock( shm3_ctrl_Ar_Lock )
	,	.shm3_ctrl_Ar_Prot( shm3_ctrl_Ar_Prot )
	,	.shm3_ctrl_Ar_Ready( shm3_ctrl_Ar_Ready )
	,	.shm3_ctrl_Ar_Size( shm3_ctrl_Ar_Size )
	,	.shm3_ctrl_Ar_Valid( shm3_ctrl_Ar_Valid )
	,	.shm3_ctrl_Aw_Addr( shm3_ctrl_Aw_Addr )
	,	.shm3_ctrl_Aw_Burst( shm3_ctrl_Aw_Burst )
	,	.shm3_ctrl_Aw_Cache( shm3_ctrl_Aw_Cache )
	,	.shm3_ctrl_Aw_Id( shm3_ctrl_Aw_Id )
	,	.shm3_ctrl_Aw_Len( shm3_ctrl_Aw_Len )
	,	.shm3_ctrl_Aw_Lock( shm3_ctrl_Aw_Lock )
	,	.shm3_ctrl_Aw_Prot( shm3_ctrl_Aw_Prot )
	,	.shm3_ctrl_Aw_Ready( shm3_ctrl_Aw_Ready )
	,	.shm3_ctrl_Aw_Size( shm3_ctrl_Aw_Size )
	,	.shm3_ctrl_Aw_Valid( shm3_ctrl_Aw_Valid )
	,	.shm3_ctrl_B_Id( shm3_ctrl_B_Id )
	,	.shm3_ctrl_B_Ready( shm3_ctrl_B_Ready )
	,	.shm3_ctrl_B_Resp( shm3_ctrl_B_Resp )
	,	.shm3_ctrl_B_Valid( shm3_ctrl_B_Valid )
	,	.shm3_ctrl_R_Data( shm3_ctrl_R_Data )
	,	.shm3_ctrl_R_Id( shm3_ctrl_R_Id )
	,	.shm3_ctrl_R_Last( shm3_ctrl_R_Last )
	,	.shm3_ctrl_R_Ready( shm3_ctrl_R_Ready )
	,	.shm3_ctrl_R_Resp( shm3_ctrl_R_Resp )
	,	.shm3_ctrl_R_Valid( shm3_ctrl_R_Valid )
	,	.shm3_ctrl_W_Data( shm3_ctrl_W_Data )
	,	.shm3_ctrl_W_Last( shm3_ctrl_W_Last )
	,	.shm3_ctrl_W_Ready( shm3_ctrl_W_Ready )
	,	.shm3_ctrl_W_Strb( shm3_ctrl_W_Strb )
	,	.shm3_ctrl_W_Valid( shm3_ctrl_W_Valid )
	);

	cbus_Structure_Module_eastbus u_cbus_Structure_Module_eastbus(
		.TM( TM )
	,	.arstn_cbus( arstn_cbus )
	,	.cbus2dbus_dbg_r_Ar_Addr( cbus2dbus_dbg_r_Ar_Addr )
	,	.cbus2dbus_dbg_r_Ar_Burst( cbus2dbus_dbg_r_Ar_Burst )
	,	.cbus2dbus_dbg_r_Ar_Cache( cbus2dbus_dbg_r_Ar_Cache )
	,	.cbus2dbus_dbg_r_Ar_Id( cbus2dbus_dbg_r_Ar_Id )
	,	.cbus2dbus_dbg_r_Ar_Len( cbus2dbus_dbg_r_Ar_Len )
	,	.cbus2dbus_dbg_r_Ar_Lock( cbus2dbus_dbg_r_Ar_Lock )
	,	.cbus2dbus_dbg_r_Ar_Prot( cbus2dbus_dbg_r_Ar_Prot )
	,	.cbus2dbus_dbg_r_Ar_Ready( cbus2dbus_dbg_r_Ar_Ready )
	,	.cbus2dbus_dbg_r_Ar_Size( cbus2dbus_dbg_r_Ar_Size )
	,	.cbus2dbus_dbg_r_Ar_Valid( cbus2dbus_dbg_r_Ar_Valid )
	,	.cbus2dbus_dbg_r_R_Data( cbus2dbus_dbg_r_R_Data )
	,	.cbus2dbus_dbg_r_R_Id( cbus2dbus_dbg_r_R_Id )
	,	.cbus2dbus_dbg_r_R_Last( cbus2dbus_dbg_r_R_Last )
	,	.cbus2dbus_dbg_r_R_Ready( cbus2dbus_dbg_r_R_Ready )
	,	.cbus2dbus_dbg_r_R_Resp( cbus2dbus_dbg_r_R_Resp )
	,	.cbus2dbus_dbg_r_R_Valid( cbus2dbus_dbg_r_R_Valid )
	,	.cbus2dbus_dbg_w_Aw_Addr( cbus2dbus_dbg_w_Aw_Addr )
	,	.cbus2dbus_dbg_w_Aw_Burst( cbus2dbus_dbg_w_Aw_Burst )
	,	.cbus2dbus_dbg_w_Aw_Cache( cbus2dbus_dbg_w_Aw_Cache )
	,	.cbus2dbus_dbg_w_Aw_Id( cbus2dbus_dbg_w_Aw_Id )
	,	.cbus2dbus_dbg_w_Aw_Len( cbus2dbus_dbg_w_Aw_Len )
	,	.cbus2dbus_dbg_w_Aw_Lock( cbus2dbus_dbg_w_Aw_Lock )
	,	.cbus2dbus_dbg_w_Aw_Prot( cbus2dbus_dbg_w_Aw_Prot )
	,	.cbus2dbus_dbg_w_Aw_Ready( cbus2dbus_dbg_w_Aw_Ready )
	,	.cbus2dbus_dbg_w_Aw_Size( cbus2dbus_dbg_w_Aw_Size )
	,	.cbus2dbus_dbg_w_Aw_Valid( cbus2dbus_dbg_w_Aw_Valid )
	,	.cbus2dbus_dbg_w_B_Id( cbus2dbus_dbg_w_B_Id )
	,	.cbus2dbus_dbg_w_B_Ready( cbus2dbus_dbg_w_B_Ready )
	,	.cbus2dbus_dbg_w_B_Resp( cbus2dbus_dbg_w_B_Resp )
	,	.cbus2dbus_dbg_w_B_Valid( cbus2dbus_dbg_w_B_Valid )
	,	.cbus2dbus_dbg_w_W_Data( cbus2dbus_dbg_w_W_Data )
	,	.cbus2dbus_dbg_w_W_Last( cbus2dbus_dbg_w_W_Last )
	,	.cbus2dbus_dbg_w_W_Ready( cbus2dbus_dbg_w_W_Ready )
	,	.cbus2dbus_dbg_w_W_Strb( cbus2dbus_dbg_w_W_Strb )
	,	.cbus2dbus_dbg_w_W_Valid( cbus2dbus_dbg_w_W_Valid )
	,	.clk_cbus( clk_cbus )
	,	.dbus2cbus_cpu_r_Ar_Addr( dbus2cbus_cpu_r_Ar_Addr )
	,	.dbus2cbus_cpu_r_Ar_Burst( dbus2cbus_cpu_r_Ar_Burst )
	,	.dbus2cbus_cpu_r_Ar_Cache( dbus2cbus_cpu_r_Ar_Cache )
	,	.dbus2cbus_cpu_r_Ar_Id( dbus2cbus_cpu_r_Ar_Id )
	,	.dbus2cbus_cpu_r_Ar_Len( dbus2cbus_cpu_r_Ar_Len )
	,	.dbus2cbus_cpu_r_Ar_Lock( dbus2cbus_cpu_r_Ar_Lock )
	,	.dbus2cbus_cpu_r_Ar_Prot( dbus2cbus_cpu_r_Ar_Prot )
	,	.dbus2cbus_cpu_r_Ar_Ready( dbus2cbus_cpu_r_Ar_Ready )
	,	.dbus2cbus_cpu_r_Ar_Size( dbus2cbus_cpu_r_Ar_Size )
	,	.dbus2cbus_cpu_r_Ar_User( dbus2cbus_cpu_r_Ar_User )
	,	.dbus2cbus_cpu_r_Ar_Valid( dbus2cbus_cpu_r_Ar_Valid )
	,	.dbus2cbus_cpu_r_R_Data( dbus2cbus_cpu_r_R_Data )
	,	.dbus2cbus_cpu_r_R_Id( dbus2cbus_cpu_r_R_Id )
	,	.dbus2cbus_cpu_r_R_Last( dbus2cbus_cpu_r_R_Last )
	,	.dbus2cbus_cpu_r_R_Ready( dbus2cbus_cpu_r_R_Ready )
	,	.dbus2cbus_cpu_r_R_Resp( dbus2cbus_cpu_r_R_Resp )
	,	.dbus2cbus_cpu_r_R_Valid( dbus2cbus_cpu_r_R_Valid )
	,	.dbus2cbus_cpu_w_Aw_Addr( dbus2cbus_cpu_w_Aw_Addr )
	,	.dbus2cbus_cpu_w_Aw_Burst( dbus2cbus_cpu_w_Aw_Burst )
	,	.dbus2cbus_cpu_w_Aw_Cache( dbus2cbus_cpu_w_Aw_Cache )
	,	.dbus2cbus_cpu_w_Aw_Id( dbus2cbus_cpu_w_Aw_Id )
	,	.dbus2cbus_cpu_w_Aw_Len( dbus2cbus_cpu_w_Aw_Len )
	,	.dbus2cbus_cpu_w_Aw_Lock( dbus2cbus_cpu_w_Aw_Lock )
	,	.dbus2cbus_cpu_w_Aw_Prot( dbus2cbus_cpu_w_Aw_Prot )
	,	.dbus2cbus_cpu_w_Aw_Ready( dbus2cbus_cpu_w_Aw_Ready )
	,	.dbus2cbus_cpu_w_Aw_Size( dbus2cbus_cpu_w_Aw_Size )
	,	.dbus2cbus_cpu_w_Aw_User( dbus2cbus_cpu_w_Aw_User )
	,	.dbus2cbus_cpu_w_Aw_Valid( dbus2cbus_cpu_w_Aw_Valid )
	,	.dbus2cbus_cpu_w_B_Id( dbus2cbus_cpu_w_B_Id )
	,	.dbus2cbus_cpu_w_B_Ready( dbus2cbus_cpu_w_B_Ready )
	,	.dbus2cbus_cpu_w_B_Resp( dbus2cbus_cpu_w_B_Resp )
	,	.dbus2cbus_cpu_w_B_Valid( dbus2cbus_cpu_w_B_Valid )
	,	.dbus2cbus_cpu_w_W_Data( dbus2cbus_cpu_w_W_Data )
	,	.dbus2cbus_cpu_w_W_Last( dbus2cbus_cpu_w_W_Last )
	,	.dbus2cbus_cpu_w_W_Ready( dbus2cbus_cpu_w_W_Ready )
	,	.dbus2cbus_cpu_w_W_Strb( dbus2cbus_cpu_w_W_Strb )
	,	.dbus2cbus_cpu_w_W_Valid( dbus2cbus_cpu_w_W_Valid )
	,	.dbus2cbus_pcie_r_Ar_Addr( dbus2cbus_pcie_r_Ar_Addr )
	,	.dbus2cbus_pcie_r_Ar_Burst( dbus2cbus_pcie_r_Ar_Burst )
	,	.dbus2cbus_pcie_r_Ar_Cache( dbus2cbus_pcie_r_Ar_Cache )
	,	.dbus2cbus_pcie_r_Ar_Id( dbus2cbus_pcie_r_Ar_Id )
	,	.dbus2cbus_pcie_r_Ar_Len( dbus2cbus_pcie_r_Ar_Len )
	,	.dbus2cbus_pcie_r_Ar_Lock( dbus2cbus_pcie_r_Ar_Lock )
	,	.dbus2cbus_pcie_r_Ar_Prot( dbus2cbus_pcie_r_Ar_Prot )
	,	.dbus2cbus_pcie_r_Ar_Ready( dbus2cbus_pcie_r_Ar_Ready )
	,	.dbus2cbus_pcie_r_Ar_Size( dbus2cbus_pcie_r_Ar_Size )
	,	.dbus2cbus_pcie_r_Ar_Valid( dbus2cbus_pcie_r_Ar_Valid )
	,	.dbus2cbus_pcie_r_R_Data( dbus2cbus_pcie_r_R_Data )
	,	.dbus2cbus_pcie_r_R_Id( dbus2cbus_pcie_r_R_Id )
	,	.dbus2cbus_pcie_r_R_Last( dbus2cbus_pcie_r_R_Last )
	,	.dbus2cbus_pcie_r_R_Ready( dbus2cbus_pcie_r_R_Ready )
	,	.dbus2cbus_pcie_r_R_Resp( dbus2cbus_pcie_r_R_Resp )
	,	.dbus2cbus_pcie_r_R_Valid( dbus2cbus_pcie_r_R_Valid )
	,	.dbus2cbus_pcie_w_Aw_Addr( dbus2cbus_pcie_w_Aw_Addr )
	,	.dbus2cbus_pcie_w_Aw_Burst( dbus2cbus_pcie_w_Aw_Burst )
	,	.dbus2cbus_pcie_w_Aw_Cache( dbus2cbus_pcie_w_Aw_Cache )
	,	.dbus2cbus_pcie_w_Aw_Id( dbus2cbus_pcie_w_Aw_Id )
	,	.dbus2cbus_pcie_w_Aw_Len( dbus2cbus_pcie_w_Aw_Len )
	,	.dbus2cbus_pcie_w_Aw_Lock( dbus2cbus_pcie_w_Aw_Lock )
	,	.dbus2cbus_pcie_w_Aw_Prot( dbus2cbus_pcie_w_Aw_Prot )
	,	.dbus2cbus_pcie_w_Aw_Ready( dbus2cbus_pcie_w_Aw_Ready )
	,	.dbus2cbus_pcie_w_Aw_Size( dbus2cbus_pcie_w_Aw_Size )
	,	.dbus2cbus_pcie_w_Aw_Valid( dbus2cbus_pcie_w_Aw_Valid )
	,	.dbus2cbus_pcie_w_B_Id( dbus2cbus_pcie_w_B_Id )
	,	.dbus2cbus_pcie_w_B_Ready( dbus2cbus_pcie_w_B_Ready )
	,	.dbus2cbus_pcie_w_B_Resp( dbus2cbus_pcie_w_B_Resp )
	,	.dbus2cbus_pcie_w_B_Valid( dbus2cbus_pcie_w_B_Valid )
	,	.dbus2cbus_pcie_w_W_Data( dbus2cbus_pcie_w_W_Data )
	,	.dbus2cbus_pcie_w_W_Last( dbus2cbus_pcie_w_W_Last )
	,	.dbus2cbus_pcie_w_W_Ready( dbus2cbus_pcie_w_W_Ready )
	,	.dbus2cbus_pcie_w_W_Strb( dbus2cbus_pcie_w_W_Strb )
	,	.dbus2cbus_pcie_w_W_Valid( dbus2cbus_pcie_w_W_Valid )
	,	.dp_Link13_to_Link37_Data( ctrl_dp_Link13_to_Link37_Data )
	,	.dp_Link13_to_Link37_RdCnt( ctrl_dp_Link13_to_Link37_RdCnt )
	,	.dp_Link13_to_Link37_RdPtr( ctrl_dp_Link13_to_Link37_RdPtr )
	,	.dp_Link13_to_Link37_RxCtl_PwrOnRst( ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRst )
	,	.dp_Link13_to_Link37_RxCtl_PwrOnRstAck( ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRstAck )
	,	.dp_Link13_to_Link37_TxCtl_PwrOnRst( ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRst )
	,	.dp_Link13_to_Link37_TxCtl_PwrOnRstAck( ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRstAck )
	,	.dp_Link13_to_Link37_WrCnt( ctrl_dp_Link13_to_Link37_WrCnt )
	,	.dp_Link35_to_Switch_ctrl_iResp001_Data( ctrl_dp_Link35_to_Switch_ctrl_iResp001_Data )
	,	.dp_Link35_to_Switch_ctrl_iResp001_RdCnt( ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdCnt )
	,	.dp_Link35_to_Switch_ctrl_iResp001_RdPtr( ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdPtr )
	,	.dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRst( ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRst )
	,	.dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRst( ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRst )
	,	.dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link35_to_Switch_ctrl_iResp001_WrCnt( ctrl_dp_Link35_to_Switch_ctrl_iResp001_WrCnt )
	,	.dp_Link36_to_Link5_Data( ctrl_dp_Link36_to_Link5_Data )
	,	.dp_Link36_to_Link5_RdCnt( ctrl_dp_Link36_to_Link5_RdCnt )
	,	.dp_Link36_to_Link5_RdPtr( ctrl_dp_Link36_to_Link5_RdPtr )
	,	.dp_Link36_to_Link5_RxCtl_PwrOnRst( ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRst )
	,	.dp_Link36_to_Link5_RxCtl_PwrOnRstAck( ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRstAck )
	,	.dp_Link36_to_Link5_TxCtl_PwrOnRst( ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRst )
	,	.dp_Link36_to_Link5_TxCtl_PwrOnRstAck( ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRstAck )
	,	.dp_Link36_to_Link5_WrCnt( ctrl_dp_Link36_to_Link5_WrCnt )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Data( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Data )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Head( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Head )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Rdy( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Rdy )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Tail( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Tail )
	,	.dp_Link6Resp001_to_Switch_eastResp001_Vld( ctrl_dp_Link6Resp001_to_Switch_eastResp001_Vld )
	,	.dp_Link7_to_Link_2c0_3_Data( ctrl_dp_Link7_to_Link_2c0_3_Data )
	,	.dp_Link7_to_Link_2c0_3_RdCnt( ctrl_dp_Link7_to_Link_2c0_3_RdCnt )
	,	.dp_Link7_to_Link_2c0_3_RdPtr( ctrl_dp_Link7_to_Link_2c0_3_RdPtr )
	,	.dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst( ctrl_dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst )
	,	.dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck( ctrl_dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck )
	,	.dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst( ctrl_dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst )
	,	.dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck( ctrl_dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck )
	,	.dp_Link7_to_Link_2c0_3_WrCnt( ctrl_dp_Link7_to_Link_2c0_3_WrCnt )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_Data( ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_Data )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt( ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr( ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst( ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst( ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt( ctrl_dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt )
	,	.dp_Switch2_to_Link6_Data( ctrl_dp_Switch2_to_Link6_Data )
	,	.dp_Switch2_to_Link6_Head( ctrl_dp_Switch2_to_Link6_Head )
	,	.dp_Switch2_to_Link6_Rdy( ctrl_dp_Switch2_to_Link6_Rdy )
	,	.dp_Switch2_to_Link6_Tail( ctrl_dp_Switch2_to_Link6_Tail )
	,	.dp_Switch2_to_Link6_Vld( ctrl_dp_Switch2_to_Link6_Vld )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_Data( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_Data )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt )
	,	.dp_Switch_ctrl_i_to_Link17_Data( ctrl_dp_Switch_ctrl_i_to_Link17_Data )
	,	.dp_Switch_ctrl_i_to_Link17_RdCnt( ctrl_dp_Switch_ctrl_i_to_Link17_RdCnt )
	,	.dp_Switch_ctrl_i_to_Link17_RdPtr( ctrl_dp_Switch_ctrl_i_to_Link17_RdPtr )
	,	.dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_i_to_Link17_WrCnt( ctrl_dp_Switch_ctrl_i_to_Link17_WrCnt )
	,	.dp_dbg_master_I_to_Link1_Data( ctrl_dp_dbg_master_I_to_Link1_Data )
	,	.dp_dbg_master_I_to_Link1_RdCnt( ctrl_dp_dbg_master_I_to_Link1_RdCnt )
	,	.dp_dbg_master_I_to_Link1_RdPtr( ctrl_dp_dbg_master_I_to_Link1_RdPtr )
	,	.dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst( ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst )
	,	.dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck( ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck )
	,	.dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst( ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst )
	,	.dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck( ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck )
	,	.dp_dbg_master_I_to_Link1_WrCnt( ctrl_dp_dbg_master_I_to_Link1_WrCnt )
	);



	//-----------------------------------------------------------------------
	// dDMA blackbox

	dDMA_Blackbox  u_dDMA_Blackbox(
		.arstn_dbus( arstn_dbus )
	,	.clk_dbus( clk_dbus )
	,	.arstn_cbus( arstn_cbus )
	,	.clk_cbus( clk_cbus )

	,	.ddma_r_Ar_Addr( ddma_r_Ar_Addr )
	,	.ddma_r_Ar_Burst( ddma_r_Ar_Burst )
	,	.ddma_r_Ar_Cache( ddma_r_Ar_Cache )
	,	.ddma_r_Ar_Id( ddma_r_Ar_Id )
	,	.ddma_r_Ar_Len( ddma_r_Ar_Len )
	,	.ddma_r_Ar_Lock( ddma_r_Ar_Lock )
	,	.ddma_r_Ar_Prot( ddma_r_Ar_Prot )
	,	.ddma_r_Ar_Ready( ddma_r_Ar_Ready )
	,	.ddma_r_Ar_Size( ddma_r_Ar_Size )
	,	.ddma_r_Ar_User( ddma_r_Ar_User )
	,	.ddma_r_Ar_Valid( ddma_r_Ar_Valid )
	,	.ddma_r_R_Data( ddma_r_R_Data )
	,	.ddma_r_R_Id( ddma_r_R_Id )
	,	.ddma_r_R_Last( ddma_r_R_Last )
	,	.ddma_r_R_Ready( ddma_r_R_Ready )
	,	.ddma_r_R_Resp( ddma_r_R_Resp )
	,	.ddma_r_R_User( ddma_r_R_User )
	,	.ddma_r_R_Valid( ddma_r_R_Valid )

	,	.ddma_w_Aw_Addr( ddma_w_Aw_Addr )
	,	.ddma_w_Aw_Burst( ddma_w_Aw_Burst )
	,	.ddma_w_Aw_Cache( ddma_w_Aw_Cache )
	,	.ddma_w_Aw_Id( ddma_w_Aw_Id )
	,	.ddma_w_Aw_Len( ddma_w_Aw_Len )
	,	.ddma_w_Aw_Lock( ddma_w_Aw_Lock )
	,	.ddma_w_Aw_Prot( ddma_w_Aw_Prot )
	,	.ddma_w_Aw_Ready( ddma_w_Aw_Ready )
	,	.ddma_w_Aw_Size( ddma_w_Aw_Size )
	,	.ddma_w_Aw_User( ddma_w_Aw_User )
	,	.ddma_w_Aw_Valid( ddma_w_Aw_Valid )
	,	.ddma_w_B_Id( ddma_w_B_Id )
	,	.ddma_w_B_Ready( ddma_w_B_Ready )
	,	.ddma_w_B_Resp( ddma_w_B_Resp )
	,	.ddma_w_B_User( ddma_w_B_User )
	,	.ddma_w_B_Valid( ddma_w_B_Valid )
	,	.ddma_w_W_Data( ddma_w_W_Data )
	,	.ddma_w_W_Last( ddma_w_W_Last )
	,	.ddma_w_W_Ready( ddma_w_W_Ready )
	,	.ddma_w_W_Strb( ddma_w_W_Strb )
	,	.ddma_w_W_Valid( ddma_w_W_Valid )

	,	.ddma_ctrl_Ar_Addr( ddma_ctrl_Ar_Addr )
	,	.ddma_ctrl_Ar_Burst( ddma_ctrl_Ar_Burst )
	,	.ddma_ctrl_Ar_Cache( ddma_ctrl_Ar_Cache )
	,	.ddma_ctrl_Ar_Id( ddma_ctrl_Ar_Id )
	,	.ddma_ctrl_Ar_Len( ddma_ctrl_Ar_Len )
	,	.ddma_ctrl_Ar_Lock( ddma_ctrl_Ar_Lock )
	,	.ddma_ctrl_Ar_Prot( ddma_ctrl_Ar_Prot )
	,	.ddma_ctrl_Ar_Ready( ddma_ctrl_Ar_Ready )
	,	.ddma_ctrl_Ar_Size( ddma_ctrl_Ar_Size )
	,	.ddma_ctrl_Ar_Valid( ddma_ctrl_Ar_Valid )
	,	.ddma_ctrl_Aw_Addr( ddma_ctrl_Aw_Addr )
	,	.ddma_ctrl_Aw_Burst( ddma_ctrl_Aw_Burst )
	,	.ddma_ctrl_Aw_Cache( ddma_ctrl_Aw_Cache )
	,	.ddma_ctrl_Aw_Id( ddma_ctrl_Aw_Id )
	,	.ddma_ctrl_Aw_Len( ddma_ctrl_Aw_Len )
	,	.ddma_ctrl_Aw_Lock( ddma_ctrl_Aw_Lock )
	,	.ddma_ctrl_Aw_Prot( ddma_ctrl_Aw_Prot )
	,	.ddma_ctrl_Aw_Ready( ddma_ctrl_Aw_Ready )
	,	.ddma_ctrl_Aw_Size( ddma_ctrl_Aw_Size )
	,	.ddma_ctrl_Aw_Valid( ddma_ctrl_Aw_Valid )
	,	.ddma_ctrl_B_Id( ddma_ctrl_B_Id )
	,	.ddma_ctrl_B_Ready( ddma_ctrl_B_Ready )
	,	.ddma_ctrl_B_Resp( ddma_ctrl_B_Resp )
	,	.ddma_ctrl_B_Valid( ddma_ctrl_B_Valid )
	,	.ddma_ctrl_R_Data( ddma_ctrl_R_Data )
	,	.ddma_ctrl_R_Id( ddma_ctrl_R_Id )
	,	.ddma_ctrl_R_Last( ddma_ctrl_R_Last )
	,	.ddma_ctrl_R_Ready( ddma_ctrl_R_Ready )
	,	.ddma_ctrl_R_Resp( ddma_ctrl_R_Resp )
	,	.ddma_ctrl_R_Valid( ddma_ctrl_R_Valid )
	,	.ddma_ctrl_W_Data( ddma_ctrl_W_Data )
	,	.ddma_ctrl_W_Last( ddma_ctrl_W_Last )
	,	.ddma_ctrl_W_Ready( ddma_ctrl_W_Ready )
	,	.ddma_ctrl_W_Strb( ddma_ctrl_W_Strb )
	,	.ddma_ctrl_W_Valid( ddma_ctrl_W_Valid )
    );

endmodule






