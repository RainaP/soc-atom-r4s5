
module dDMA_Blackbox(

	input         arstn_cbus                                                      ,
	input         clk_cbus                                                        ,
	input         arstn_dbus                                                      ,
	input         clk_dbus                                                        ,


	output [36:0]   ddma_r_Ar_Addr                                                
,	output [1:0]    ddma_r_Ar_Burst                                               
,	output [3:0]    ddma_r_Ar_Cache                                               
,	output [11:0]   ddma_r_Ar_Id                                                  
,	output [4:0]    ddma_r_Ar_Len                                                 
,	output          ddma_r_Ar_Lock                                                
,	output [2:0]    ddma_r_Ar_Prot                                                
,	input           ddma_r_Ar_Ready                                               
,	output [2:0]    ddma_r_Ar_Size                                                
,	output [3:0]    ddma_r_Ar_User                                                
,	output          ddma_r_Ar_Valid                                               
,	input  [1023:0] ddma_r_R_Data                                                 
,	input  [11:0]   ddma_r_R_Id                                                   
,	input           ddma_r_R_Last                                                 
,	output          ddma_r_R_Ready                                                
,	input  [1:0]    ddma_r_R_Resp                                                 
,	input  [3:0]    ddma_r_R_User                                                 
,	input           ddma_r_R_Valid                                                

,	output [36:0]   ddma_w_Aw_Addr                                          
,	output [1:0]    ddma_w_Aw_Burst                                         
,	output [3:0]    ddma_w_Aw_Cache                                         
,	output [11:0]   ddma_w_Aw_Id                                            
,	output [4:0]    ddma_w_Aw_Len                                           
,	output          ddma_w_Aw_Lock                                          
,	output [2:0]    ddma_w_Aw_Prot                                          
,	input           ddma_w_Aw_Ready                                         
,	output [2:0]    ddma_w_Aw_Size                                          
,	output [3:0]    ddma_w_Aw_User                                          
,	output          ddma_w_Aw_Valid                                         
,	input  [11:0]   ddma_w_B_Id                                             
,	output          ddma_w_B_Ready                                          
,	input  [1:0]    ddma_w_B_Resp                                           
,	input  [3:0]    ddma_w_B_User                                           
,	input           ddma_w_B_Valid                                          
,	output [1023:0] ddma_w_W_Data                                           
,	output          ddma_w_W_Last                                           
,	input           ddma_w_W_Ready                                          
,	output [127:0]  ddma_w_W_Strb                                           
,	output          ddma_w_W_Valid                                          

,	input  [23:0] ddma_ctrl_Ar_Addr                                               
,	input  [1:0]  ddma_ctrl_Ar_Burst                                              
,	input  [3:0]  ddma_ctrl_Ar_Cache                                              
,	input  [1:0]  ddma_ctrl_Ar_Id                                                 
,	input  [1:0]  ddma_ctrl_Ar_Len                                                
,	input         ddma_ctrl_Ar_Lock                                               
,	input  [2:0]  ddma_ctrl_Ar_Prot                                               
,	output        ddma_ctrl_Ar_Ready                                              
,	input  [2:0]  ddma_ctrl_Ar_Size                                               
,	input         ddma_ctrl_Ar_Valid                                              
,	input  [23:0] ddma_ctrl_Aw_Addr                                               
,	input  [1:0]  ddma_ctrl_Aw_Burst                                              
,	input  [3:0]  ddma_ctrl_Aw_Cache                                              
,	input  [1:0]  ddma_ctrl_Aw_Id                                                 
,	input  [1:0]  ddma_ctrl_Aw_Len                                                
,	input         ddma_ctrl_Aw_Lock                                               
,	input  [2:0]  ddma_ctrl_Aw_Prot                                               
,	output        ddma_ctrl_Aw_Ready                                              
,	input  [2:0]  ddma_ctrl_Aw_Size                                               
,	input         ddma_ctrl_Aw_Valid                                              
,	output [1:0]  ddma_ctrl_B_Id                                                  
,	input         ddma_ctrl_B_Ready                                               
,	output [1:0]  ddma_ctrl_B_Resp                                                
,	output        ddma_ctrl_B_Valid                                               
,	output [31:0] ddma_ctrl_R_Data                                                
,	output [1:0]  ddma_ctrl_R_Id                                                  
,	output        ddma_ctrl_R_Last                                                
,	input         ddma_ctrl_R_Ready                                               
,	output [1:0]  ddma_ctrl_R_Resp                                                
,	output        ddma_ctrl_R_Valid                                               
,	input  [31:0] ddma_ctrl_W_Data                                                
,	input         ddma_ctrl_W_Last                                                
,	output        ddma_ctrl_W_Ready                                               
,	input  [3:0]  ddma_ctrl_W_Strb                                                
,	input         ddma_ctrl_W_Valid                                               

		);





endmodule
