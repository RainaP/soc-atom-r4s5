
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:30

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link13_main.DtpTxClkAdapt_Link37_Async.WrCnt -graycode -module cbus_Structure_Module_eastbus
cdc signal Link1_main.DtpRxClkAdapt_dbg_master_I_Async.RdCnt -graycode -module cbus_Structure_Module_eastbus
cdc signal Link5_main.DtpRxClkAdapt_Link36_Async.RdCnt -graycode -module cbus_Structure_Module_eastbus
cdc signal Link7Resp001_main.DtpRxClkAdapt_Link_2c0_3Resp001_Async.RdCnt -graycode -module cbus_Structure_Module_eastbus
cdc signal Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.WrCnt -graycode -module cbus_Structure_Module_eastbus
cdc signal Switch_ctrl_iResp001_main.DtpRxClkAdapt_Link35_Async.RdCnt -graycode -module cbus_Structure_Module_eastbus
cdc signal Switch_ctrl_iResp001_main.DtpTxClkAdapt_dbg_master_I_Async.WrCnt -graycode -module cbus_Structure_Module_eastbus
cdc signal Switch_ctrl_i_main.DtpTxClkAdapt_Link17_Async.WrCnt -graycode -module cbus_Structure_Module_eastbus

