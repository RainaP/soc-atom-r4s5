
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:30

####################################################################

# FIFO module definition
cdc fifo -module cbus_Structure_Module_eastbus Link13_main.DtpTxClkAdapt_Link37_Async.RegData_*
cdc fifo -module cbus_Structure_Module_eastbus Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.RegData_*
cdc fifo -module cbus_Structure_Module_eastbus Switch_ctrl_iResp001_main.DtpTxClkAdapt_dbg_master_I_Async.RegData_*
cdc fifo -module cbus_Structure_Module_eastbus Switch_ctrl_i_main.DtpTxClkAdapt_Link17_Async.RegData_*

