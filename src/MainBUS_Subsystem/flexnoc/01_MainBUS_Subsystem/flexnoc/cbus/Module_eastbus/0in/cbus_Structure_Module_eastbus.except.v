
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:30

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_eastbus_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link13_main.DtpTxClkAdapt_Link37_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link1_main.DtpRxClkAdapt_dbg_master_I_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link5_main.DtpRxClkAdapt_Link36_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link7Resp001_main.DtpRxClkAdapt_Link_2c0_3Resp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Switch_ctrl_iResp001_main.DtpRxClkAdapt_Link35_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Switch_ctrl_iResp001_main.DtpTxClkAdapt_dbg_master_I_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Switch_ctrl_i_main.DtpTxClkAdapt_Link17_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme multi_sync_mux_select -from Link1_main.DtpRxClkAdapt_dbg_master_I_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme multi_sync_mux_select -from Link5_main.DtpRxClkAdapt_Link36_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme multi_sync_mux_select -from Link7Resp001_main.DtpRxClkAdapt_Link_2c0_3Resp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_eastbus -severity waived -scheme multi_sync_mux_select -from Switch_ctrl_iResp001_main.DtpRxClkAdapt_Link35_Async.uRegSync.instSynchronizerCell*

endmodule
