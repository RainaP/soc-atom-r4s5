
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:30

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_eastbus_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link13_main.DtpTxClkAdapt_Link37_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link13_main.DtpTxClkAdapt_Link37_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link13_main.DtpTxClkAdapt_Link37_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link13_main.DtpTxClkAdapt_Link37_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Link7_main.DtpTxClkAdapt_Link_2c0_3_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Switch_ctrl_iResp001_main.DtpTxClkAdapt_dbg_master_I_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Switch_ctrl_iResp001_main.DtpTxClkAdapt_dbg_master_I_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Switch_ctrl_iResp001_main.DtpTxClkAdapt_dbg_master_I_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Switch_ctrl_iResp001_main.DtpTxClkAdapt_dbg_master_I_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Switch_ctrl_i_main.DtpTxClkAdapt_Link17_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Switch_ctrl_i_main.DtpTxClkAdapt_Link17_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Switch_ctrl_i_main.DtpTxClkAdapt_Link17_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_eastbus Switch_ctrl_i_main.DtpTxClkAdapt_Link17_Async.RegData_3

endmodule
