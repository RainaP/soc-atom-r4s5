
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:30

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_eastbus_clkgrp;

// 0in set_cdc_clock Regime_cbus_Cm_east_main.root_Clk -module cbus_Structure_Module_eastbus -group clk_cbus -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock Regime_cbus_Cm_east_main.root_Clk_ClkS -module cbus_Structure_Module_eastbus -group clk_cbus -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock clk_cbus -module cbus_Structure_Module_eastbus -group clk_cbus -period 1.000 -waveform {0.000 0.500}

endmodule
