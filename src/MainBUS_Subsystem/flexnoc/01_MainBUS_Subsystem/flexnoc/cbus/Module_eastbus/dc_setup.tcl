# Script dc_shell.tcl



set DESIGN_NAME                               "cbus_Structure_Module_eastbus"
set LIBRARY_DONT_USE_FILE                     ""
set LIBRARY_DONT_USE_PRE_COMPILE_LIST         ""
set LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST    ""
set ADDITIONAL_SEARCH_PATH                    ""
source -echo -verbose 
set_units -time ns -resistance kOhm -capacitance pF -voltage V -current mA
set_app_var synthetic_library dw_foundation.sldb
set_app_var link_library [concat {*} $target_library $synthetic_library]
source -echo -verbose ./dc_setup_filenames.tcl
set_app_var sh_new_variable_message false
set RTL_SOURCE_FILES  "rtl.GaterCell.v rtl.ClockManagerCell.v rtl.SynchronizerCell.v cbus_Structure_Module_eastbus_commons.v cbus_Structure_Module_eastbus.v"
set REPORTS_DIR  "./reports"
set RESULTS_DIR  "./results"
file mkdir ${REPORTS_DIR}
file mkdir ${RESULTS_DIR}
set CLOCK_GATE_INST_LIST  " clockGaters_cbus2dbus_dbg_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link24_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch_eastResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link7_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_cbus2dbus_dbg_w_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2cbus_cpu_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link25_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2cbus_pcie_r_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link26_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link5_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch2_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link13_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_from_ctrlResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_cbus2dbus_dbg_r/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2cbus_cpu_r_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link7Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2cbus_cpu_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_from_ctrl_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2cbus_pcie_r/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch_ctrl_i_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link1_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2cbus_cpu_r/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2cbus_pcie_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_cbus2dbus_dbg_r_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch_ctrl_iResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2cbus_pcie_w/ClockGater/usce4c71b1c0/instGaterCell "
set OPTIMIZATION_FLOW hplp
set_app_var search_path ". ./sdc ${ADDITIONAL_SEARCH_PATH} $search_path"
set mw_design_library ${DCRM_MW_LIBRARY_NAME}
extend_mw_layers
if {![file isdirectory $mw_design_library ]} {
  create_mw_lib   -technology $MW_TECH_FILE -mw_reference_library $MW_REF_LIB_LIST $mw_design_library
} else {
set_mw_lib_reference $mw_design_library -mw_reference_library $MW_REF_LIB_LIST
}
open_mw_lib     $mw_design_library
check_library > ${REPORTS_DIR}/${DCRM_CHECK_LIBRARY_REPORT}
set_tlu_plus_files -max_tluplus $MAX_TLU_PLUS -min_tluplus $MIN_TLU_PLUS
check_tlu_plus_files
if {[file exists [which ${LIBRARY_DONT_USE_FILE}]]} {
  source -echo -verbose ${LIBRARY_DONT_USE_FILE}
}
