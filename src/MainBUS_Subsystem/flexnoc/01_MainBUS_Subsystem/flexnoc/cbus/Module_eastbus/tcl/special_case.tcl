
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:30

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ia/OrdCam_0_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ia/OrdCam_1_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ia/OrdCam_2_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ia/OrdCam_3_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ia/OrdCam_0_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ia/OrdCam_1_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ia/OrdCam_2_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ia/OrdCam_3_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ia/OrdCam_0_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ia/OrdCam_1_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ia/OrdCam_2_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ia/OrdCam_3_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ia/OrdCam_0_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ia/OrdCam_1_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ia/OrdCam_2_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ia/OrdCam_3_PndCnt" -lsb "0" -msb "2"
