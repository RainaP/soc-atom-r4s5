
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:30

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy

# SYNC_LINES



# ASYNC_LINES


# PSEUDO_STATIC_LINES


# COMMENTED_LINES
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link13_main/DtpTxClkAdapt_Link37_Async/RegData_0" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link13_main/DtpTxClkAdapt_Link37_Async/RegData_1" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link13_main/DtpTxClkAdapt_Link37_Async/RegData_2" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link13_main/DtpTxClkAdapt_Link37_Async/RegData_3" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link7_main/DtpTxClkAdapt_Link_2c0_3_Async/RegData_0" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link7_main/DtpTxClkAdapt_Link_2c0_3_Async/RegData_1" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link7_main/DtpTxClkAdapt_Link_2c0_3_Async/RegData_2" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link7_main/DtpTxClkAdapt_Link_2c0_3_Async/RegData_3" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link7_main/DtpTxClkAdapt_Link_2c0_3_Async/RegData_4" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link7_main/DtpTxClkAdapt_Link_2c0_3_Async/RegData_5" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/DtpTxClkAdapt_dbg_master_I_Async/RegData_0" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_peri_Cm_root" -to_clk_P "${Regime_peri_Cm_root_P}" -delay "[expr 2*${Regime_peri_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/DtpTxClkAdapt_dbg_master_I_Async/RegData_1" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_peri_Cm_root" -to_clk_P "${Regime_peri_Cm_root_P}" -delay "[expr 2*${Regime_peri_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/DtpTxClkAdapt_dbg_master_I_Async/RegData_2" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_peri_Cm_root" -to_clk_P "${Regime_peri_Cm_root_P}" -delay "[expr 2*${Regime_peri_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/DtpTxClkAdapt_dbg_master_I_Async/RegData_3" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_peri_Cm_root" -to_clk_P "${Regime_peri_Cm_root_P}" -delay "[expr 2*${Regime_peri_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Switch_ctrl_i_main/DtpTxClkAdapt_Link17_Async/RegData_0" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Switch_ctrl_i_main/DtpTxClkAdapt_Link17_Async/RegData_1" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Switch_ctrl_i_main/DtpTxClkAdapt_Link17_Async/RegData_2" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Switch_ctrl_i_main/DtpTxClkAdapt_Link17_Async/RegData_3" -from_lsb "0" -from_msb "57" -from_clk "Regime_cbus_Cm_east_root" -from_clk_P "${Regime_cbus_Cm_east_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
