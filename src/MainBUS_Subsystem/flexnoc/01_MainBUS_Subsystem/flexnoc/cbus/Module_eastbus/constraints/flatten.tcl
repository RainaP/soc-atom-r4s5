if {[get_cells ${CUSTOMER_HIERARCHY}Link13_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link13_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link13_main/DtpTxClkAdapt_Link37_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link13_main/DtpTxClkAdapt_Link37_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link1_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link1_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link1_main/DtpRxClkAdapt_dbg_master_I_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link1_main/DtpRxClkAdapt_dbg_master_I_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link23_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link23_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link23_main/DtpRxSerAdapt_Switch_ctrl_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link23_main/DtpRxSerAdapt_Switch_ctrl_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link24_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link24_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link24_main/DtpTxSerAdapt_Switch_ctrl_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link24_main/DtpTxSerAdapt_Switch_ctrl_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link25_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link25_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link25_main/DtpRxSerAdapt_Switch_ctrl_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link25_main/DtpRxSerAdapt_Switch_ctrl_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link26_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link26_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link26_main/DtpTxSerAdapt_Switch_ctrl_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link26_main/DtpTxSerAdapt_Switch_ctrl_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link5_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link5_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link5_main/DtpRxClkAdapt_Link36_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link5_main/DtpRxClkAdapt_Link36_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link7Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link7Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link7Resp001_main/DtpRxClkAdapt_Link_2c0_3Resp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link7Resp001_main/DtpRxClkAdapt_Link_2c0_3Resp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link7Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link7Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link7_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link7_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link7_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link7_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link7_main/DtpTxClkAdapt_Link_2c0_3_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link7_main/DtpTxClkAdapt_Link_2c0_3_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link9_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link9_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_from_ctrlResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_from_ctrlResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_from_ctrlResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_from_ctrlResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_from_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_from_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_from_ctrl_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_from_ctrl_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_eastbus] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_eastbus false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cbus_Cm_east_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cbus_Cm_east_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cbus_Cm_east_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cbus_Cm_east_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main/Demux_Link_from_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main/Demux_Link_from_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main/Demux_dbus2cbus_cpu_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main/Demux_dbus2cbus_cpu_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main/Demux_dbus2cbus_cpu_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main/Demux_dbus2cbus_cpu_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main/Demux_dbus2cbus_pcie_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main/Demux_dbus2cbus_pcie_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main/Demux_dbus2cbus_pcie_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main/Demux_dbus2cbus_pcie_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main/Mux_Link] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main/Mux_Link false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main/Mux_Link13] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main/Mux_Link13 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch2_main/Mux_Link6] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch2_main/Mux_Link6 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/DtpRxClkAdapt_Link35_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/DtpRxClkAdapt_Link35_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/DtpTxClkAdapt_dbg_master_I_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/DtpTxClkAdapt_dbg_master_I_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/Mux_dbg_master_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_iResp001_main/Mux_dbg_master_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_i_main/Demux_Link1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_i_main/Demux_Link1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_i_main/DtpTxClkAdapt_Link17_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_i_main/DtpTxClkAdapt_Link17_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Demux_Link5] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Demux_Link5 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Demux_Link6Resp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Demux_Link6Resp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Demux_Link9] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Demux_Link9 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/DtpTxBwdPipe_dbus2cbus_cpu_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/DtpTxBwdPipe_dbus2cbus_cpu_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/DtpTxBwdPipe_dbus2cbus_cpu_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/DtpTxBwdPipe_dbus2cbus_cpu_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/DtpTxBwdPipe_dbus2cbus_pcie_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/DtpTxBwdPipe_dbus2cbus_pcie_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/DtpTxBwdPipe_dbus2cbus_pcie_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/DtpTxBwdPipe_dbus2cbus_pcie_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_Link_from_ctrlResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_Link_from_ctrlResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_dbus2cbus_cpu_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_dbus2cbus_cpu_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_dbus2cbus_cpu_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_dbus2cbus_cpu_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_dbus2cbus_pcie_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_dbus2cbus_pcie_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_dbus2cbus_pcie_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_eastResp001_main/Mux_dbus2cbus_pcie_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/DtpTxSerAdapt_Link24] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/DtpTxSerAdapt_Link24 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cbus2dbus_dbg_w_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link13_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link13_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link13_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link13_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link1_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link1_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link1_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link1_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link23_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link23_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link23_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link23_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link24_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link24_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link24_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link24_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link25_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link25_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link25_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link25_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link26_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link26_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link26_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link26_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link5_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link5_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link5_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link5_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link7Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link7Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link7Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link7Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link7_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link7_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link7_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link7_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_from_ctrlResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_from_ctrlResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_from_ctrlResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_from_ctrlResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_from_ctrl_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_from_ctrl_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_from_ctrl_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_from_ctrl_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch2_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch2_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch2_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch2_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_eastResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_eastResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_eastResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_eastResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_w_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_w_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_w_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cbus2dbus_dbg_w_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_cpu_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dbus2cbus_pcie_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/DtpRxSerAdapt_Switch_eastResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/DtpRxSerAdapt_Switch_eastResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/DtpTxSerAdapt_Switch2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/DtpTxSerAdapt_Switch2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Id_0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Id_0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/DtpRxSerAdapt_Switch_eastResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/DtpRxSerAdapt_Switch_eastResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/DtpTxSerAdapt_Switch2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/DtpTxSerAdapt_Switch2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_cpu_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/DtpRxSerAdapt_Switch_eastResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/DtpRxSerAdapt_Switch_eastResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/DtpTxSerAdapt_Switch2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/DtpTxSerAdapt_Switch2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Id_0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Id_0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/DtpRxSerAdapt_Switch_eastResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/DtpRxSerAdapt_Switch_eastResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/DtpTxSerAdapt_Switch2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/DtpTxSerAdapt_Switch2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dbus2cbus_pcie_w_main/SpecificToGeneric false
}
