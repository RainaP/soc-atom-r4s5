
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:32

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link13Resp001_main.DtpRxClkAdapt_Switch_westResp001_Async.RdCnt -graycode -module cbus_Structure_Module_centerbus
cdc signal Link2_main.DtpTxClkAdapt_Switch_west_Async.WrCnt -graycode -module cbus_Structure_Module_centerbus
cdc signal Link_n03_asiResp001_main.DtpRxClkAdapt_Link_n03_astResp001_Async.RdCnt -graycode -module cbus_Structure_Module_centerbus
cdc signal Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.WrCnt -graycode -module cbus_Structure_Module_centerbus
cdc signal Link_n47_asiResp001_main.DtpRxClkAdapt_Link_n47_astResp001_Async.RdCnt -graycode -module cbus_Structure_Module_centerbus
cdc signal Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.WrCnt -graycode -module cbus_Structure_Module_centerbus

