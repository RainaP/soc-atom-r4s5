# Validated with 0in (TM) version Version 10.4c_6 linux_x86_64 21 Dec 2015
# vlib work; vmap -c work work; vlog -skipsynthoffregion *.v
# qverify -c -do 0in/do.tcl
#

cdc preference -vectorize_nl

# turn on fifo synchronization recognition
#
cdc preference -fifo_scheme
#cdc preference fifo -sync_effort high
#cdc preference fifo -effort high

# turn on reconvergence
#
cdc reconvergence on
cdc preference reconvergence -depth 0 -divergence_depth 0

do 0in/cbus_Structure_Module_centerbus.clkgrp.tcl
do 0in/cbus_Structure_Module_centerbus.rst.tcl
do 0in/cbus_Structure_Module_centerbus.io.tcl

do 0in/cbus_Structure_Module_centerbus.ctrl.tcl
do 0in/cbus_Structure_Module_centerbus.except.tcl

onerror continue

 cdc run -d cbus_Structure_Module_centerbus -auto_black_box -hcdc -process_dead_end -cr cbus_Structure_Module_centerbus.rpt
 cdc generate report cdc_detail.rpt

exit

