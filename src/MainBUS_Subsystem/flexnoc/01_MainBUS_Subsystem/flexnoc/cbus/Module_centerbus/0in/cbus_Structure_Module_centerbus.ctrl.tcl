
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:32

####################################################################

# FIFO module definition
cdc fifo -module cbus_Structure_Module_centerbus Link2_main.DtpTxClkAdapt_Switch_west_Async.RegData_*
cdc fifo -module cbus_Structure_Module_centerbus Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.RegData_*
cdc fifo -module cbus_Structure_Module_centerbus Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.RegData_*

