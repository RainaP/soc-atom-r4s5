
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:32

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_centerbus_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link2_main.DtpTxClkAdapt_Switch_west_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link2_main.DtpTxClkAdapt_Switch_west_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link2_main.DtpTxClkAdapt_Switch_west_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link2_main.DtpTxClkAdapt_Switch_west_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_centerbus Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.RegData_5

endmodule
