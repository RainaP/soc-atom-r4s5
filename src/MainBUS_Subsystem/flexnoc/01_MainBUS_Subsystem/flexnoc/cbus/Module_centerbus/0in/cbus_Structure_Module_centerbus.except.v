
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:32

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_centerbus_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals Link13Resp001_main.DtpRxClkAdapt_Switch_westResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals Link2_main.DtpTxClkAdapt_Switch_west_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals Link_n03_asiResp001_main.DtpRxClkAdapt_Link_n03_astResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals Link_n03_asi_main.DtpTxClkAdapt_Link_n03_ast_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals Link_n47_asiResp001_main.DtpRxClkAdapt_Link_n47_astResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals Link_n47_asi_main.DtpTxClkAdapt_Link_n47_ast_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from Link13Resp001_main.DtpRxClkAdapt_Switch_westResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from Link_n03_asiResp001_main.DtpRxClkAdapt_Link_n03_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from Link_n47_asiResp001_main.DtpRxClkAdapt_Link_n47_astResp001_Async.uRegSync.instSynchronizerCell*

endmodule
