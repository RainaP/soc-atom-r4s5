
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:32

####################################################################

arteris_gen_clock -name "Regime_cbus_Cm_center_root" -pin "Regime_cbus_Cm_center_main/root_Clk Regime_cbus_Cm_center_main/root_Clk_ClkS" -clock_domain "clk_cbus" -spec_domain_clock "/Regime_cbus/Cm_center/root" -divide_by "1" -source "clk_cbus" -source_period "${clk_cbus_P}" -source_waveform "[expr ${clk_cbus_P}*0.00] [expr ${clk_cbus_P}*0.50]" -user_directive "" -add "FALSE"
