
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:32

####################################################################

arteris_clock -name "clk_cbus" -clock_domain "clk_cbus" -port "clk_cbus" -period "${clk_cbus_P}" -waveform "[expr ${clk_cbus_P}*0.00] [expr ${clk_cbus_P}*0.50]" -edge "R" -user_directive ""
