# Script dc_shell.tcl



set DESIGN_NAME                               "cbus_Structure_Module_centerbus"
set LIBRARY_DONT_USE_FILE                     ""
set LIBRARY_DONT_USE_PRE_COMPILE_LIST         ""
set LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST    ""
set ADDITIONAL_SEARCH_PATH                    ""
source -echo -verbose 
set_units -time ns -resistance kOhm -capacitance pF -voltage V -current mA
set_app_var synthetic_library dw_foundation.sldb
set_app_var link_library [concat {*} $target_library $synthetic_library]
source -echo -verbose ./dc_setup_filenames.tcl
set_app_var sh_new_variable_message false
set RTL_SOURCE_FILES  "rtl.GaterCell.v rtl.ClockManagerCell.v rtl.SynchronizerCell.v cbus_Structure_Module_centerbus_restr_sub.v cbus_Structure_Module_centerbus_commons.v cbus_Structure_Module_centerbus.v"
set REPORTS_DIR  "./reports"
set RESULTS_DIR  "./results"
file mkdir ${REPORTS_DIR}
file mkdir ${RESULTS_DIR}
set CLOCK_GATE_INST_LIST  " clockGaters_Link_n03_y_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link11Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n03_zResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link11_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n03_z_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch_centerResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link12Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm3_ctrl/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link12_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_2s02Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link13Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n47_y_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm0_ctrl_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n47_zResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link6_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link14_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n47_z_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link16_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link15Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n8Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link15_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_2s13_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n8_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm1_ctrl/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link14Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n03_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link19_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link2_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link19Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch_center_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm1_ctrl_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n47_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_2s02_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n47_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link16Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link20_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link6Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n03_yResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_ddma_ctrl_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm3_ctrl_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch1_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link20Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm2_ctrl/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_2s13Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n03_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm0_ctrl/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n47_yResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm2_ctrl_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_ddma_ctrl/ClockGater/usce4c71b1c0/instGaterCell "
set OPTIMIZATION_FLOW hplp
set_app_var search_path ". ./sdc ${ADDITIONAL_SEARCH_PATH} $search_path"
set mw_design_library ${DCRM_MW_LIBRARY_NAME}
extend_mw_layers
if {![file isdirectory $mw_design_library ]} {
  create_mw_lib   -technology $MW_TECH_FILE -mw_reference_library $MW_REF_LIB_LIST $mw_design_library
} else {
set_mw_lib_reference $mw_design_library -mw_reference_library $MW_REF_LIB_LIST
}
open_mw_lib     $mw_design_library
check_library > ${REPORTS_DIR}/${DCRM_CHECK_LIBRARY_REPORT}
set_tlu_plus_files -max_tluplus $MAX_TLU_PLUS -min_tluplus $MIN_TLU_PLUS
check_tlu_plus_files
if {[file exists [which ${LIBRARY_DONT_USE_FILE}]]} {
  source -echo -verbose ${LIBRARY_DONT_USE_FILE}
}
