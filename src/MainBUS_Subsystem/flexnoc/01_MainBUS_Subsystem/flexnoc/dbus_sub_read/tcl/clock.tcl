
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:26:51

####################################################################

arteris_clock -name "clk_sub" -clock_domain "clk_sub" -port "clk_sub" -period "${clk_sub_P}" -waveform "[expr ${clk_sub_P}*0.00] [expr ${clk_sub_P}*0.50]" -edge "R" -user_directive ""
