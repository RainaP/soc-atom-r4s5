# Design variables used later in constraints
set CUSTOMER_HIERARCHY ""
set arteris_dflt_ARRIVAL_PERCENT           30
set arteris_dflt_REQUIRED_PERCENT          30
set arteris_comb_ARRIVAL_PERCENT           30
set arteris_comb_REQUIRED_PERCENT          40
set arteris_internal_dflt_ARRIVAL_PERCENT  70
set arteris_internal_dflt_REQUIRED_PERCENT 30
set arteris_internal_comb_ARRIVAL_PERCENT  40
set arteris_internal_comb_REQUIRED_PERCENT 60
set arteris_dflt_CLOCK_UNCERTAINTY          0
set T_Flex2library 1
set {clk_sub_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_bus_Cm_center_root_P} [ expr ${clk_sub_P}*1 ]

source "constraints/internalConstraints.sdc"
source "constraints/externalConstraints.sdc"

