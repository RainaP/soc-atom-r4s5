
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:27:39

####################################################################

# Create Clocks

create_clock -name "clk_sub" [get_ports "clk_sub"] -period "${clk_sub_P}" -waveform "[expr ${clk_sub_P}*0.00] [expr ${clk_sub_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks clk_sub] 
# Create Virtual Clocks

# Create Generated Virtual Clocks 

# Create Generated Clocks

create_generated_clock -name "Regime_bus_Cm_center_root" [get_pins "${CUSTOMER_HIERARCHY}Regime_bus_Cm_center_main/root_Clk ${CUSTOMER_HIERARCHY}Regime_bus_Cm_center_main/root_Clk_ClkS "] -source "clk_sub" -divide_by "1"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_center_root] 

#warning:  uncomment below line will disabled inter-clock constraint(s) by considering them as false path(s)
#set_clock_groups -asynchronous  -group { clk_sub Regime_bus_Cm_center_root } 
# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_sub"]
# Clock Gating Checks 

set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n4rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n4r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n5rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n5r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n6rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n6r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n7rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n7r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s0rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s0r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s1rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s1r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s2rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s2r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s3rResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s3r_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch2Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch2_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port0_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port0_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port1_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port1_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port2_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port2_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port3_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port3_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port0_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port0_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port1_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port1_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port2_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port2_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port3_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port3_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_ddma_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_ddma_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_pcie_cpu_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_pcie_cpu_r_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm0_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm0_r_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm1_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm1_r_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm2_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm2_r_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm3_r/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm3_r_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]


