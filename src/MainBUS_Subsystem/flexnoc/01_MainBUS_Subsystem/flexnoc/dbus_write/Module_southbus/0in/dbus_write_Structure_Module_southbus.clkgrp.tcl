
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:23:19

####################################################################

netlist clock Regime_bus_Cm_south_main.root_Clk -module dbus_write_Structure_Module_southbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_bus_Cm_south_main.root_Clk_ClkS -module dbus_write_Structure_Module_southbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}
netlist clock clk_dbus -module dbus_write_Structure_Module_southbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}

