
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:23:19

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal restr_corner_s.Link_c0_astResp_main.DtpTxClkAdapt_Link_c0_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_southbus
cdc signal restr_corner_s.Link_c0_ast_main.DtpRxClkAdapt_Link_c0_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_southbus

