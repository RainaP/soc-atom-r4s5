
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:23:19

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_customer_cell -module "ClockManager_Regime_bus_Cm_south" -type "ClockManagerCell" -instance_name "${CUSTOMER_HIERARCHY}Regime_bus_Cm_south_main/ClockManager/IClockManager_Regime_bus_Cm_south"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main/DtpTxClkAdapt_Link_c0_asiResp_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main/DtpTxClkAdapt_Link_c0_asiResp_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main/DtpTxClkAdapt_Link_c0_asiResp_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main/DtpTxClkAdapt_Link_c0_asiResp_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main/DtpTxClkAdapt_Link_c0_asiResp_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main/DtpRxClkAdapt_Link_c0_asi_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main/DtpRxClkAdapt_Link_c0_asi_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main/DtpRxClkAdapt_Link_c0_asi_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main/DtpRxClkAdapt_Link_c0_asi_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main/DtpRxClkAdapt_Link_c0_asi_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_aResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_south_root" -clock_period "${Regime_bus_Cm_south_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_a_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_south_root" -clock_period "${Regime_bus_Cm_south_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_south_root" -clock_period "${Regime_bus_Cm_south_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_south_root" -clock_period "${Regime_bus_Cm_south_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_bResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_south_root" -clock_period "${Regime_bus_Cm_south_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_south_root" -clock_period "${Regime_bus_Cm_south_root_P}"

