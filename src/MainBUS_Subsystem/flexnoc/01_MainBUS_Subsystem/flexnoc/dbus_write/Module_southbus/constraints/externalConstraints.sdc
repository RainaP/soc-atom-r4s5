
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:23:20

####################################################################

# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_dbus"]

# Create Test Mode 

set_ideal_network -no_propagate [get_ports "TM"]

set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_Data"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_WrCnt"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_comb_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0e_aResp_to_Link_c0s_bResp_Data"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0e_aResp_to_Link_c0s_bResp_Head"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0e_aResp_to_Link_c0s_bResp_Tail"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0e_aResp_to_Link_c0s_bResp_Vld"]
set_input_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0s_b_to_Link_c0e_a_Rdy"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_RdCnt"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_RdPtr"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_comb_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_astResp_to_Link_c0_asiResp_Data"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0e_aResp_to_Link_c0s_bResp_Rdy"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0s_b_to_Link_c0e_a_Data"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0s_b_to_Link_c0e_a_Head"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0s_b_to_Link_c0e_a_Tail"]
set_output_delay -clock Regime_bus_Cm_south_root [expr ${Regime_bus_Cm_south_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0s_b_to_Link_c0e_a_Vld"]

