
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:43

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_w_I_main/GenericToTransport/Ia/OrdCam_0_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_w_I_main/GenericToTransport/Ia/OrdCam_1_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_w_I_main/GenericToTransport/Ia/OrdCam_2_PndCnt" -lsb "0" -msb "2"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_w_I_main/GenericToTransport/Ia/OrdCam_3_PndCnt" -lsb "0" -msb "2"
