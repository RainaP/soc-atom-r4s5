
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:43

####################################################################

arteris_gen_reset -name "Regime_bus_Cm_east_root" -clock_domain "clk_dbus" -clock "Regime_bus_Cm_east_root" -spec_domain_clock "/Regime_bus/Cm_east/root" -clock_period "${Regime_bus_Cm_east_root_P}" -clock_waveform "[expr ${Regime_bus_Cm_east_root_P}*0.00] [expr ${Regime_bus_Cm_east_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_bus_Cm_east_main/root_Clk_RstN" -lsb "" -msb ""
arteris_gen_reset -name "Regime_bus_Cm_east_root" -clock_domain "clk_dbus" -clock "Regime_bus_Cm_east_root" -spec_domain_clock "/Regime_bus/Cm_east/root" -clock_period "${Regime_bus_Cm_east_root_P}" -clock_waveform "[expr ${Regime_bus_Cm_east_root_P}*0.00] [expr ${Regime_bus_Cm_east_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_bus_Cm_east_main/root_Clk_RstN" -lsb "" -msb ""
