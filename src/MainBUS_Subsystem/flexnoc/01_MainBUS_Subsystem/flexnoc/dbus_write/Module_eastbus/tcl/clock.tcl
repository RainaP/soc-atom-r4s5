
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:43

####################################################################

arteris_clock -name "clk_dbus" -clock_domain "clk_dbus" -port "clk_dbus" -period "${clk_dbus_P}" -waveform "[expr ${clk_dbus_P}*0.00] [expr ${clk_dbus_P}*0.50]" -edge "R" -user_directive ""
