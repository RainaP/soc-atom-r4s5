
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:43

####################################################################

# FIFO module definition
cdc fifo -module dbus_write_Structure_Module_eastbus Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.RegData_*
cdc fifo -module dbus_write_Structure_Module_eastbus Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.RegData_*
cdc fifo -module dbus_write_Structure_Module_eastbus Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.RegData_*
cdc fifo -module dbus_write_Structure_Module_eastbus Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.RegData_*

