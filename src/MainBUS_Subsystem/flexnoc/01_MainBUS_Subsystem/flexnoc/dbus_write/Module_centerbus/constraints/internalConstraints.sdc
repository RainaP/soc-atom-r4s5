
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:19:55

####################################################################

# Create Clocks

create_clock -name "clk_dbus" [get_ports "clk_dbus"] -period "${clk_dbus_P}" -waveform "[expr ${clk_dbus_P}*0.00] [expr ${clk_dbus_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks clk_dbus] 
# Create Virtual Clocks

create_clock -name "Regime_cpu" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]"
create_clock -name "Regime_dcluster0" -period "${Regime_dcluster0_P}" -waveform "[expr ${Regime_dcluster0_P}*0.00] [expr ${Regime_dcluster0_P}*0.50]"
create_clock -name "Regime_dcluster1" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]"
create_clock -name "Regime_dram0" -period "${Regime_dram0_P}" -waveform "[expr ${Regime_dram0_P}*0.00] [expr ${Regime_dram0_P}*0.50]"
create_clock -name "Regime_dram1" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]"
create_clock -name "Regime_dram2" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]"
create_clock -name "Regime_dram3" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]"
create_clock -name "Regime_pcie" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cpu] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster0] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster1] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram0] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram1] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram2] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram3] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_pcie] 
# Create Generated Virtual Clocks 

create_clock -name "Regime_bus_Cm_east_root" -period "${clk_dbus_P}" -waveform "[expr ${clk_dbus_P}*0.00] [expr ${clk_dbus_P}*0.50]"
create_clock -name "Regime_bus_Cm_south_root" -period "${clk_dbus_P}" -waveform "[expr ${clk_dbus_P}*0.00] [expr ${clk_dbus_P}*0.50]"
create_clock -name "Regime_bus_Cm_west_root" -period "${clk_dbus_P}" -waveform "[expr ${clk_dbus_P}*0.00] [expr ${clk_dbus_P}*0.50]"
create_clock -name "Regime_cpu_Cm_root" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]"
create_clock -name "Regime_dcluster0_Cm_root" -period "${Regime_dcluster0_P}" -waveform "[expr ${Regime_dcluster0_P}*0.00] [expr ${Regime_dcluster0_P}*0.50]"
create_clock -name "Regime_dcluster1_Cm_root" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]"
create_clock -name "Regime_dram0_Cm_root" -period "${Regime_dram0_P}" -waveform "[expr ${Regime_dram0_P}*0.00] [expr ${Regime_dram0_P}*0.50]"
create_clock -name "Regime_dram1_Cm_root" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]"
create_clock -name "Regime_dram2_Cm_root" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]"
create_clock -name "Regime_dram3_Cm_root" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]"
create_clock -name "Regime_pcie_Cm_root" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_east_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_south_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_west_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cpu_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster0_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster1_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram0_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram1_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram2_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram3_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_pcie_Cm_root] 
# Create Generated Clocks

create_generated_clock -name "Regime_bus_Cm_center_root" [get_pins "${CUSTOMER_HIERARCHY}Regime_bus_Cm_center_main/root_Clk ${CUSTOMER_HIERARCHY}Regime_bus_Cm_center_main/root_Clk_ClkS "] -source "clk_dbus" -divide_by "1"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_center_root] 

#warning:  uncomment below line will disabled inter-clock constraint(s) by considering them as false path(s)
#set_clock_groups -asynchronous  -group { Regime_dcluster1_Cm_root }  -group { Regime_bus_Cm_west_root }  -group { clk_dbus Regime_bus_Cm_center_root }  -group { Regime_dram3_Cm_root }  -group { Regime_dram2_Cm_root }  -group { Regime_dcluster0 }  -group { Regime_dcluster1 }  -group { Regime_cpu_Cm_root }  -group { Regime_bus_Cm_south_root }  -group { Regime_dram0_Cm_root }  -group { Regime_dcluster0_Cm_root }  -group { Regime_dram1_Cm_root }  -group { Regime_pcie_Cm_root }  -group { Regime_bus_Cm_east_root }  -group { Regime_cpu }  -group { Regime_pcie }  -group { Regime_dram0 }  -group { Regime_dram1 }  -group { Regime_dram2 }  -group { Regime_dram3 } 
# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_dbus"]
# Clock Gating Checks 

set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_aResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_a_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_bResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_cResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_dResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_d_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_eResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01_m01_e_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_m01_bResp002_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_m01_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_m01_cResp002_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_m01_c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_m23_bResp002_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_m23_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_m23_cResp002_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_m23_c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n47_m01_bResp002_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n47_m01_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n47_m01_cResp002_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n47_m01_c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n47_m23_bResp002_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n47_m23_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n47_m23_cResp002_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n47_m23_c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n4_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n4_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n4_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n4_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n5_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n5_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n5_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n5_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n6_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n6_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n6_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n6_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n7_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n7_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n7_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n7_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m01_aResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m01_a_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m01_bResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m01_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m01_cResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m01_c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m23_aResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m23_a_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m23_bResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m23_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m23_cResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8_m23_c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sc01Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sc01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sc01pResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sc01p_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn0Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn0_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn1Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn1_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn2Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn2_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn3Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn3_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn4Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn4_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn5Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn5_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn6Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn6_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn7Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_sn7_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n47_m01Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n47_m01_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n47_m23Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n47_m23_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n8Resp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n8_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port0_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port0_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port1_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port1_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port2_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port2_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port3_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port3_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port0_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port0_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port1_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port1_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port2_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port2_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port3_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port3_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_ddma_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_ddma_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_pcie_cpu_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_pcie_cpu_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_ddma_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_ddma_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Link_n0_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Link_n0_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Link_n1_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Link_n1_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Link_n2_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Link_n2_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Link_n3_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Link_n3_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Switch_n0cResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Switch_n0c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Switch_n1cResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Switch_n1c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Switch_n2cResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Switch_n2c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Switch_n3cResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm03con/clockGaters_Switch_n3c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Link_n4_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Link_n4_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Link_n5_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Link_n5_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Link_n6_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Link_n6_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Link_n7_astResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Link_n7_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Switch_n4cResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Switch_n4c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Switch_n5cResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Switch_n5c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Switch_n6cResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Switch_n6c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Switch_n7cResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}restr_ncm47con/clockGaters_Switch_n7c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]


