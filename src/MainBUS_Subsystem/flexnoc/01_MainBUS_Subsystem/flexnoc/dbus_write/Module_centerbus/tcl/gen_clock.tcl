
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:19:01

####################################################################

arteris_gen_clock -name "Regime_bus_Cm_center_root" -pin "Regime_bus_Cm_center_main/root_Clk Regime_bus_Cm_center_main/root_Clk_ClkS" -clock_domain "clk_dbus" -spec_domain_clock "/Regime_bus/Cm_center/root" -divide_by "1" -source "clk_dbus" -source_period "${clk_dbus_P}" -source_waveform "[expr ${clk_dbus_P}*0.00] [expr ${clk_dbus_P}*0.50]" -user_directive "" -add "FALSE"
