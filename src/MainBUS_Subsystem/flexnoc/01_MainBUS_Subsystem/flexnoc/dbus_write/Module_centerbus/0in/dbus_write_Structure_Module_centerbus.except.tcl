
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:19:01

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal restr_ncm03con.Link_n0_astResp_main.DtpTxClkAdapt_Link_n0_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm03con.Link_n0_ast_main.DtpRxClkAdapt_Link_n0_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm03con.Link_n1_astResp_main.DtpTxClkAdapt_Link_n1_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm03con.Link_n1_ast_main.DtpRxClkAdapt_Link_n1_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm03con.Link_n2_astResp_main.DtpTxClkAdapt_Link_n2_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm03con.Link_n2_ast_main.DtpRxClkAdapt_Link_n2_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm03con.Link_n3_astResp_main.DtpTxClkAdapt_Link_n3_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm03con.Link_n3_ast_main.DtpRxClkAdapt_Link_n3_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm47con.Link_n4_astResp_main.DtpTxClkAdapt_Link_n4_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm47con.Link_n4_ast_main.DtpRxClkAdapt_Link_n4_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm47con.Link_n5_astResp_main.DtpTxClkAdapt_Link_n5_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm47con.Link_n5_ast_main.DtpRxClkAdapt_Link_n5_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm47con.Link_n6_astResp_main.DtpTxClkAdapt_Link_n6_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm47con.Link_n6_ast_main.DtpRxClkAdapt_Link_n6_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm47con.Link_n7_astResp_main.DtpTxClkAdapt_Link_n7_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_centerbus
cdc signal restr_ncm47con.Link_n7_ast_main.DtpRxClkAdapt_Link_n7_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_centerbus

