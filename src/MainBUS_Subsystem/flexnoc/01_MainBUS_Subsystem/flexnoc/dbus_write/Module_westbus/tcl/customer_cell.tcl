
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:23:26

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_customer_cell -module "ClockManager_Regime_bus_Cm_west" -type "ClockManagerCell" -instance_name "${CUSTOMER_HIERARCHY}Regime_bus_Cm_west_main/ClockManager/IClockManager_Regime_bus_Cm_west"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main/DtpRxClkAdapt_Link_m0_astResp001_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main/DtpRxClkAdapt_Link_m0_astResp001_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main/DtpRxClkAdapt_Link_m0_astResp001_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main/DtpRxClkAdapt_Link_m0_astResp001_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main/DtpRxClkAdapt_Link_m0_astResp001_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main/DtpRxClkAdapt_Link_m1_astResp001_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main/DtpRxClkAdapt_Link_m1_astResp001_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main/DtpRxClkAdapt_Link_m1_astResp001_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main/DtpRxClkAdapt_Link_m1_astResp001_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main/DtpRxClkAdapt_Link_m1_astResp001_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_yResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_y_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_zResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_z_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_yResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_y_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_zResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_z_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_westResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_west_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_bus_Cm_west_root" -clock_period "${Regime_bus_Cm_west_root_P}"

