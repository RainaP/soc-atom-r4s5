
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:23:26

////////////////////////////////////////////////////////////////////

module dbus_write_Structure_Module_westbus_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_write_Structure_Module_westbus -severity waived -scheme reconvergence -from_signals Link_m0_asiResp001_main.DtpRxClkAdapt_Link_m0_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_westbus -severity waived -scheme reconvergence -from_signals Link_m0_asi_main.DtpTxClkAdapt_Link_m0_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_westbus -severity waived -scheme reconvergence -from_signals Link_m1_asiResp001_main.DtpRxClkAdapt_Link_m1_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_westbus -severity waived -scheme reconvergence -from_signals Link_m1_asi_main.DtpTxClkAdapt_Link_m1_ast_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_write_Structure_Module_westbus -severity waived -scheme multi_sync_mux_select -from Link_m0_asiResp001_main.DtpRxClkAdapt_Link_m0_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_write_Structure_Module_westbus -severity waived -scheme multi_sync_mux_select -from Link_m1_asiResp001_main.DtpRxClkAdapt_Link_m1_astResp001_Async.uRegSync.instSynchronizerCell*

endmodule
