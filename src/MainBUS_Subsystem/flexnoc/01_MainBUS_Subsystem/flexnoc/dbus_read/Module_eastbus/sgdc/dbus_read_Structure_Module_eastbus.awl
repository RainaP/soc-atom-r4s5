
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:48

####################################################################

waive -ip {dbus_read_Structure_Module_eastbus} -rule {InferLatch} -comment {\
We allow the inference of a latch in clock gater cells.\
If you see this suppressed in more than a gater cell, it should be investigated. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {Clock_Reset_check02} -comment {\
There is no race condition between the gater and the data.\
The clock is turned on before data arrives and shut off after data stops being transmitted. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {Ac_cdc01a} -comment {\
Possible data loss in async crossing going from fast to slow.\
There will be no data loss the crossings, they are correct by construction. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {Reset_sync02} -comment {\
Async reset generated in an async clock domain domain.\
This is fine when reset is de-asserted with clock off. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {MuxSelConst} -comment {\
Width of the select lines can be greater than the recommended value because \
either input of the Mux share identical value (case of constants which are read in a static table) \
or all possible states of the select signal are not used. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {FlopEConst} -comment {\
Local constants propagation has been made (which means constant set in the module where the Mux is instantiated). \
Remaining unpropagated constants are due to non-uniquify FlexNoC output. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {DisabledAnd} -comment {\
Local constants propagation has been made (which means constant set in the module where the And is instantiated). \
Remaining unpropagated constants are due to non-uniquify FlexNoC output. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {DisabledOr} -comment {\
Local constants propagation has been made (which means constant set in the module where the Or is instantiated). \
Remaining unpropagated constants are due to non-uniquify FlexNoC output. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {FlopDataConstant} -comment {\
Local constants propagation has been made (which means constant set in the module where the Dff is instantiated). \
Remaining unpropagated constants are due to non-uniquify FlexNoC output. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {LogNMux} -comment {\
Select input can be greater than the recommended value because input of the Mux share identical value. \
(In practice the decodage is incomplete so there is not any timing problem). \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {RegOutputs} -comment {Output not registered are supported only with AHB interface with signals: HRData, HReady, HReadySel}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC-1.1.1.1} -comment {\
Due to synthesis engine, interconnect design usually contains hundred of module description. \
Can be confused if design is written based on one module per file. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {NoExprInPort-ML} -comment {Supported by Verilog HDL specification}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {UnloadedInPort-ML} -comment {\
Genericity of the RTL database which is vector based for dedicated signals. \
For instance internal clock signal is bus based containing the following bits: \
Sys_Clk, Sys_Clk_ClkS, Sys_Clk_En, Sys_Clk_EnS, Sys_Clk_RetRstN, Sys_Clk_RstN, Sys_Clk_Tm \
Using this clock signal (which is described as vector) will automatically create ports at the interface even if some of them are not used. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {UnloadedNet-ML} -comment {\
Genericity of the RTL database which is vector based for dedicated signals. \
Using an internal signal will automatically declare all bits of the signal even some one them are not used. \
There is not any RTL optimization due to non-uniquify FlexNoC output. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {UnloadedOutTerm-ML} -comment {Genericity about module interface.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {UnloadedOutTerm-ML} -comment {Genericity about module interface.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {W240} -comment {Genericity about module interface.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {W164a} -comment {Arithmetic overflow/underflow checked with assert in simulation & adress wrap checked by monitor.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {W484} -comment {Arithmetic overflow/underflow checked with assert in simulation & adress wrap checked by monitor.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {W528} -comment {Genericity about module interface.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {W287b} -comment {Interface output port Not connected. Possible reasons are unused bits in a regiser, unstitched BIST wrappers, etc.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {W110} -comment {Construction is allowed and tested by simulation.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC-2.10.6.1} -comment {Arithmetic checked with assert in simulation & assignment width checked by monitor.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC-2.10.6.1} -comment {Arithmetic checked with assert in simulation & adress wrap checked by monitor.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {SYNTH_5032} -comment {\
Due to synthesis engine, hanging user instance can be present in the generated HDL file. \
Corner case is usually: trigger signals used with simulation primitive \$display(SimulError ...) \
and these signals are not protected with translate_off directive. \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {WRN_69} -comment {Timescale directive always defined for every modules}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {WRN_74} -comment {Warning is reported but directive: translate_on/translate_off is well balanced \
(HDL description is alway simulated and synthesized) \
Following directive can also be used to avoid this message: set_option pragma { synopsys } \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {W189} -comment {Warning is reported but directive: translate_on is well balanced with translate_off \
(HDL description is alway simulated and synthesized) \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {PESTR08} -comment {Clock gating implementation is made by third-party CAD tools.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {PESTR11} -comment {\
Internal constant can be present due to Specification setting enabled but not used \
(for example case of a definition of nUrgencyLevel but no QoS feature present in some NIUs leads to stuck-at the internal Pressure signal). \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {PESTR12} -comment {Clock gating implementation is made by third-party CAD tools.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {PESTR20} -comment {Clock gating implementation is made by third-party CAD tools.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {PESTR21} -comment {Clock gating implementation is made by third-party CAD tools.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {SYNTH_5066} -comment {RTL description written by FlexNoC needs to be optimized because HDL data is generated from on generic module.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC05-1.1.2.1a} -comment {Module name is made on the concatenation of the different module name present in the hierarchy to improve readibility.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC05-1.1.2.1b} -comment {Instance name is based on the concatenation of the different instance name present in the hierarchy to improve readibility.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC05-1.1.3.3a} -comment {Signal name is based on the concatenation of the different instance name present in the hierarchy to improve readibility.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC05-1.1.3.3b} -comment {Port name is based on the concatenation of the different instance name present in the hierarchy to improve readibility.}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC-2.2.3.1} -comment {\
Delay has been inserted to avoid race condition during simulation \
(in case of none delay, hold problem can arrive with clock gating structure for example. Normally delay is not taken into account during synthesis). \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {STARC05-2.2.3.1} -comment {\
Delay has been inserted to avoid race condition during simulation \
(in case of none delay, hold problem can arrive with clock gating structure for example. Normally delay is not taken into account during synthesis). \
}
waive -ip {dbus_read_Structure_Module_eastbus} -rule {W257} -comment {\
Delay has been inserted to avoid race condition during simulation \
(in case of none delay, hold problem can arrive with clock gating structure for example. Normally delay is not taken into account during synthesis). \
}
