if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_aResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_aResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_aResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_aResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_aResp_main/DtpTxSerAdapt_Link_c0s_bResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_aResp_main/DtpTxSerAdapt_Link_c0s_bResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_a_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_a_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_a_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_a_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_bResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_bResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_bResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_bResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_b_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_b_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_b_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_b_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_cResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_cResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_cResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_cResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_c_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_c_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_c_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_c_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_dResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_dResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_dResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_dResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_dResp_main/Fifo] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_dResp_main/Fifo false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_d_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_d_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0e_d_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0e_d_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_aResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_aResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_aResp_main/DtpTxSerAdapt_Link_c1_astResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_aResp_main/DtpTxSerAdapt_Link_c1_astResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_aResp_main/Fifo] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_aResp_main/Fifo false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_a_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_a_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_astResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_astResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_astResp001_main/DtpTxClkAdapt_Link_c1_asiResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_astResp001_main/DtpTxClkAdapt_Link_c1_asiResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_ast_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_ast_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_ast_main/DtpRxClkAdapt_Link_c1_asi_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_ast_main/DtpRxClkAdapt_Link_c1_asi_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1t_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1t_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1t_asiResp_main/DtpRxClkAdapt_Link_c1t_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1t_asiResp_main/DtpRxClkAdapt_Link_c1t_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1t_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1t_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1t_asi_main/DtpTxClkAdapt_Link_c1t_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1t_asi_main/DtpTxClkAdapt_Link_c1t_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2_asiResp001_main/DtpRxClkAdapt_Link_m2_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2_asiResp001_main/DtpRxClkAdapt_Link_m2_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2_asi_main/DtpTxClkAdapt_Link_m2_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2_asi_main/DtpTxClkAdapt_Link_m2_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2e_yResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2e_yResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2e_yResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2e_yResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2e_y_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2e_y_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2e_y_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2e_y_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2e_zResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2e_zResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2e_zResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2e_zResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2e_z_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2e_z_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2e_z_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2e_z_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3_asiResp001_main/DtpRxClkAdapt_Link_m3_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3_asiResp001_main/DtpRxClkAdapt_Link_m3_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3_asi_main/DtpTxClkAdapt_Link_m3_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3_asi_main/DtpTxClkAdapt_Link_m3_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3e_yResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3e_yResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3e_yResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3e_yResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3e_y_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3e_y_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3e_y_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3e_y_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3e_zResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3e_zResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3e_zResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3e_zResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3e_z_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3e_z_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3e_z_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3e_z_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_eastbus] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_eastbus false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_bus_Cm_east_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_bus_Cm_east_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_bus_Cm_east_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_bus_Cm_east_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_aResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_aResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_aResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_aResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_a_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_a_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_a_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_a_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_bResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_bResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_bResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_bResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_b_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_b_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_b_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_b_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_cResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_cResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_cResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_cResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_c_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_c_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_c_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_c_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_dResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_dResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_dResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_dResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_d_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_d_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_d_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0e_d_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_aResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_aResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_aResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_aResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_astResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_astResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_astResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_astResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_ast_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_ast_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_ast_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_ast_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_yResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_yResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_yResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_yResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_y_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_y_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_y_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_y_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_zResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_zResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_zResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_zResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_z_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_z_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_z_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2e_z_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_yResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_yResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_yResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_yResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_y_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_y_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_y_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_y_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_zResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_zResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_zResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_zResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_z_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_z_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_z_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3e_z_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c02ctrlResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c02ctrlResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c02ctrlResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c02ctrlResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c02ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c02ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c02ctrl_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c02ctrl_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c12ctrlResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c12ctrlResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c12ctrlResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c12ctrlResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c12ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c12ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c12ctrl_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_c12ctrl_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_ctrlResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_ctrlResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_ctrlResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_ctrlResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Link_ctrl_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Link_ctrl_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Demux_Link_c01_m01_aResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Demux_Link_c01_m01_aResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Demux_Link_m2e_yResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Demux_Link_m2e_yResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Demux_Link_m3e_yResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Demux_Link_m3e_yResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Demux_Link_sc01Resp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Demux_Link_sc01Resp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_c01_m01_aResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_c01_m01_aResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_c02ctrlResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_c02ctrlResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_c12ctrlResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_c12ctrlResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_c1t_asiResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_c1t_asiResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_m2e_yResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_m2e_yResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_m3e_yResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_m3e_yResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_sc01Resp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpRxSerAdapt_Link_sc01Resp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpTxSerAdapt_Link_ctrlResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/DtpTxSerAdapt_Link_ctrlResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_c0e_dResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_c0e_dResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_c1_aResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_c1_aResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_ctrlResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_ctrlResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_n03_m23_cResp002] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_n03_m23_cResp002 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_n47_m23_cResp002] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_n47_m23_cResp002 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_n8_m23_cResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_eastResp001_main/Mux_Link_n8_m23_cResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_c0e_d] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_c0e_d false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_c1_a] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_c1_a false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_n03_m23_c] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_n03_m23_c false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_n47_m23_c] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_n47_m23_c false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_n8_m23_c] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Demux_Link_n8_m23_c false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Mux_Link_c01_m01_a] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Mux_Link_c01_m01_a false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Mux_Link_m2e_y] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Mux_Link_m2e_y false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Mux_Link_m3e_y] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Mux_Link_m3e_y false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Mux_Link_sc01] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/Switch_east_main/Mux_Link_sc01 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/DtpTxSerAdapt_Link_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/DtpTxSerAdapt_Link_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Id_0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Id_0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/cbus2dbus_dbg_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c02ctrlResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c02ctrlResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c02ctrlResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c02ctrlResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c02ctrl_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c02ctrl_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c02ctrl_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c02ctrl_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c12ctrlResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c12ctrlResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c12ctrlResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c12ctrlResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c12ctrl_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c12ctrl_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c12ctrl_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_c12ctrl_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_ctrlResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_ctrlResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_ctrlResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_ctrlResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_ctrl_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_ctrl_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_ctrl_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Link_ctrl_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Switch_eastResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Switch_eastResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Switch_eastResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Switch_eastResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Switch_east_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Switch_east_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Switch_east_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_Switch_east_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_cbus2dbus_dbg_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_cbus2dbus_dbg_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_cbus2dbus_dbg_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_cbus2dbus_dbg_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_cbus2dbus_dbg_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_cbus2dbus_dbg_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_cbus2dbus_dbg_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_cbus2dbus_dbg_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_cpu_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_cpu_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_cpu_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_cpu_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_cpu_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_cpu_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_cpu_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_cpu_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_pcie_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_pcie_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_pcie_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_pcie_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_pcie_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_pcie_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_pcie_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/clockGaters_dbus2cbus_pcie_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/DtpRxSerAdapt_Link_c12ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/DtpRxSerAdapt_Link_c12ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_cpu_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/DtpRxSerAdapt_Link_c02ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/DtpRxSerAdapt_Link_c02ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_e/dbus2cbus_pcie_r_main/GenericToSpecific false
}
