
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:48

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy

# SYNC_LINES



# ASYNC_LINES


# PSEUDO_STATIC_LINES


# COMMENTED_LINES
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1_astResp001_main/DtpTxClkAdapt_Link_c1_asiResp001_Async/RegData_0" -from_lsb "0" -from_msb "273" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1_astResp001_main/DtpTxClkAdapt_Link_c1_asiResp001_Async/RegData_1" -from_lsb "0" -from_msb "273" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1_astResp001_main/DtpTxClkAdapt_Link_c1_asiResp001_Async/RegData_2" -from_lsb "0" -from_msb "273" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1_astResp001_main/DtpTxClkAdapt_Link_c1_asiResp001_Async/RegData_3" -from_lsb "0" -from_msb "273" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1_astResp001_main/DtpTxClkAdapt_Link_c1_asiResp001_Async/RegData_4" -from_lsb "0" -from_msb "273" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1_astResp001_main/DtpTxClkAdapt_Link_c1_asiResp001_Async/RegData_5" -from_lsb "0" -from_msb "273" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1t_asi_main/DtpTxClkAdapt_Link_c1t_ast_Async/RegData_0" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1t_asi_main/DtpTxClkAdapt_Link_c1t_ast_Async/RegData_1" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1t_asi_main/DtpTxClkAdapt_Link_c1t_ast_Async/RegData_2" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1t_asi_main/DtpTxClkAdapt_Link_c1t_ast_Async/RegData_3" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1t_asi_main/DtpTxClkAdapt_Link_c1t_ast_Async/RegData_4" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c1t_asi_main/DtpTxClkAdapt_Link_c1t_ast_Async/RegData_5" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_cpu_Cm_root" -to_clk_P "${Regime_cpu_Cm_root_P}" -delay "[expr 2*${Regime_cpu_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m2_asi_main/DtpTxClkAdapt_Link_m2_ast_Async/RegData_0" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram2_Cm_root" -to_clk_P "${Regime_dram2_Cm_root_P}" -delay "[expr 2*${Regime_dram2_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m2_asi_main/DtpTxClkAdapt_Link_m2_ast_Async/RegData_1" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram2_Cm_root" -to_clk_P "${Regime_dram2_Cm_root_P}" -delay "[expr 2*${Regime_dram2_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m2_asi_main/DtpTxClkAdapt_Link_m2_ast_Async/RegData_2" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram2_Cm_root" -to_clk_P "${Regime_dram2_Cm_root_P}" -delay "[expr 2*${Regime_dram2_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m2_asi_main/DtpTxClkAdapt_Link_m2_ast_Async/RegData_3" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram2_Cm_root" -to_clk_P "${Regime_dram2_Cm_root_P}" -delay "[expr 2*${Regime_dram2_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m2_asi_main/DtpTxClkAdapt_Link_m2_ast_Async/RegData_4" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram2_Cm_root" -to_clk_P "${Regime_dram2_Cm_root_P}" -delay "[expr 2*${Regime_dram2_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m2_asi_main/DtpTxClkAdapt_Link_m2_ast_Async/RegData_5" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram2_Cm_root" -to_clk_P "${Regime_dram2_Cm_root_P}" -delay "[expr 2*${Regime_dram2_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3_asi_main/DtpTxClkAdapt_Link_m3_ast_Async/RegData_0" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram3_Cm_root" -to_clk_P "${Regime_dram3_Cm_root_P}" -delay "[expr 2*${Regime_dram3_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3_asi_main/DtpTxClkAdapt_Link_m3_ast_Async/RegData_1" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram3_Cm_root" -to_clk_P "${Regime_dram3_Cm_root_P}" -delay "[expr 2*${Regime_dram3_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3_asi_main/DtpTxClkAdapt_Link_m3_ast_Async/RegData_2" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram3_Cm_root" -to_clk_P "${Regime_dram3_Cm_root_P}" -delay "[expr 2*${Regime_dram3_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3_asi_main/DtpTxClkAdapt_Link_m3_ast_Async/RegData_3" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram3_Cm_root" -to_clk_P "${Regime_dram3_Cm_root_P}" -delay "[expr 2*${Regime_dram3_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3_asi_main/DtpTxClkAdapt_Link_m3_ast_Async/RegData_4" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram3_Cm_root" -to_clk_P "${Regime_dram3_Cm_root_P}" -delay "[expr 2*${Regime_dram3_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3_asi_main/DtpTxClkAdapt_Link_m3_ast_Async/RegData_5" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_east_root" -from_clk_P "${Regime_bus_Cm_east_root_P}" -to_clk "Regime_dram3_Cm_root" -to_clk_P "${Regime_dram3_Cm_root_P}" -delay "[expr 2*${Regime_dram3_Cm_root_P}]"
