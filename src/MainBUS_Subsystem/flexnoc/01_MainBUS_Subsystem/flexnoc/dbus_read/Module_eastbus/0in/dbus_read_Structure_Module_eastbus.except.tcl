
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:48

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.WrCnt -graycode -module dbus_read_Structure_Module_eastbus
cdc signal Link_c1_ast_main.DtpRxClkAdapt_Link_c1_asi_Async.RdCnt -graycode -module dbus_read_Structure_Module_eastbus
cdc signal Link_c1t_asiResp_main.DtpRxClkAdapt_Link_c1t_astResp_Async.RdCnt -graycode -module dbus_read_Structure_Module_eastbus
cdc signal Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.WrCnt -graycode -module dbus_read_Structure_Module_eastbus
cdc signal Link_m2_asiResp001_main.DtpRxClkAdapt_Link_m2_astResp001_Async.RdCnt -graycode -module dbus_read_Structure_Module_eastbus
cdc signal Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.WrCnt -graycode -module dbus_read_Structure_Module_eastbus
cdc signal Link_m3_asiResp001_main.DtpRxClkAdapt_Link_m3_astResp001_Async.RdCnt -graycode -module dbus_read_Structure_Module_eastbus
cdc signal Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.WrCnt -graycode -module dbus_read_Structure_Module_eastbus

