
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:48

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_eastbus_clkgrp;

// 0in set_cdc_clock Regime_bus_Cm_east_main.root_Clk -module dbus_read_Structure_Module_eastbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock Regime_bus_Cm_east_main.root_Clk_ClkS -module dbus_read_Structure_Module_eastbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock clk_dbus -module dbus_read_Structure_Module_eastbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}

endmodule
