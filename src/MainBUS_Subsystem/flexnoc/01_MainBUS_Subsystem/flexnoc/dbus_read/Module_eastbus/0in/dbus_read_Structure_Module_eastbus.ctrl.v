
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:48

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_eastbus_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.RegData_0
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.RegData_1
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.RegData_2
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.RegData_3
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.RegData_4
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.RegData_5
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.RegData_0
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.RegData_1
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.RegData_2
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.RegData_3
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.RegData_4
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.RegData_5
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.RegData_0
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.RegData_1
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.RegData_2
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.RegData_3
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.RegData_4
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.RegData_5
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.RegData_0
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.RegData_1
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.RegData_2
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.RegData_3
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.RegData_4
// 0in set_cdc_fifo -module dbus_read_Structure_Module_eastbus Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.RegData_5

endmodule
