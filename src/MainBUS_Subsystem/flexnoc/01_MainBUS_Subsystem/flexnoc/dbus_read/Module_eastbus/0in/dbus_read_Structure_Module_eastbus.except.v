
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:48

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_eastbus_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link_c1_astResp001_main.DtpTxClkAdapt_Link_c1_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link_c1_ast_main.DtpRxClkAdapt_Link_c1_asi_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link_c1t_asiResp_main.DtpRxClkAdapt_Link_c1t_astResp_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link_c1t_asi_main.DtpTxClkAdapt_Link_c1t_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link_m2_asiResp001_main.DtpRxClkAdapt_Link_m2_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link_m2_asi_main.DtpTxClkAdapt_Link_m2_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link_m3_asiResp001_main.DtpRxClkAdapt_Link_m3_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme reconvergence -from_signals Link_m3_asi_main.DtpTxClkAdapt_Link_m3_ast_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme multi_sync_mux_select -from Link_c1_ast_main.DtpRxClkAdapt_Link_c1_asi_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme multi_sync_mux_select -from Link_c1t_asiResp_main.DtpRxClkAdapt_Link_c1t_astResp_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme multi_sync_mux_select -from Link_m2_asiResp001_main.DtpRxClkAdapt_Link_m2_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_eastbus -severity waived -scheme multi_sync_mux_select -from Link_m3_asiResp001_main.DtpRxClkAdapt_Link_m3_astResp001_Async.uRegSync.instSynchronizerCell*

endmodule
