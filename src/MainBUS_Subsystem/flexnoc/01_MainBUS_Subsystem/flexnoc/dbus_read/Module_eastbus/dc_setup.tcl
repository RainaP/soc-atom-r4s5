# Script dc_shell.tcl



set DESIGN_NAME                               "dbus_read_Structure_Module_eastbus"
set LIBRARY_DONT_USE_FILE                     ""
set LIBRARY_DONT_USE_PRE_COMPILE_LIST         ""
set LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST    ""
set ADDITIONAL_SEARCH_PATH                    ""
source -echo -verbose 
set_units -time ns -resistance kOhm -capacitance pF -voltage V -current mA
set_app_var synthetic_library dw_foundation.sldb
set_app_var link_library [concat {*} $target_library $synthetic_library]
source -echo -verbose ./dc_setup_filenames.tcl
set_app_var sh_new_variable_message false
set RTL_SOURCE_FILES  "rtl.GaterCell.v rtl.ClockManagerCell.v rtl.SynchronizerCell.v dbus_read_Structure_Module_eastbus_restr_corner_e.v dbus_read_Structure_Module_eastbus_commons.v dbus_read_Structure_Module_eastbus.v"
set REPORTS_DIR  "./reports"
set RESULTS_DIR  "./results"
file mkdir ${REPORTS_DIR}
file mkdir ${RESULTS_DIR}
set CLOCK_GATE_INST_LIST  " clockGaters_Link_m2e_yResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m2e_zResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c0e_aResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c0e_dResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m3e_zResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c1_aResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_Link_c02ctrl_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m3e_yResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c0e_d_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_Link_c02ctrlResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_Link_c12ctrl_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c1_astResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_Link_ctrl_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_dbus2cbus_pcie_r/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m2e_z_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m2_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c0e_a_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_Switch_east_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c0e_b_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c1_ast_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m3e_y_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m3_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_Link_c12ctrlResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c0e_bResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_dbus2cbus_pcie_r_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c1t_asiResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_dbus2cbus_cpu_r/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_cbus2dbus_dbg_r_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m2e_y_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m2_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_dbus2cbus_cpu_r_T/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_Link_ctrlResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_cbus2dbus_dbg_r/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c0e_cResp_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c1t_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c0e_c_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m3e_z_main_Sys/ClockGater/usce4c71b1c0/instGaterCell restr_corner_e/clockGaters_Switch_eastResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m3_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell "
set OPTIMIZATION_FLOW hplp
set_app_var search_path ". ./sdc ${ADDITIONAL_SEARCH_PATH} $search_path"
set mw_design_library ${DCRM_MW_LIBRARY_NAME}
extend_mw_layers
if {![file isdirectory $mw_design_library ]} {
  create_mw_lib   -technology $MW_TECH_FILE -mw_reference_library $MW_REF_LIB_LIST $mw_design_library
} else {
set_mw_lib_reference $mw_design_library -mw_reference_library $MW_REF_LIB_LIST
}
open_mw_lib     $mw_design_library
check_library > ${REPORTS_DIR}/${DCRM_CHECK_LIBRARY_REPORT}
set_tlu_plus_files -max_tluplus $MAX_TLU_PLUS -min_tluplus $MIN_TLU_PLUS
check_tlu_plus_files
if {[file exists [which ${LIBRARY_DONT_USE_FILE}]]} {
  source -echo -verbose ${LIBRARY_DONT_USE_FILE}
}
