
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:26

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_m0_asiResp001_main.DtpRxClkAdapt_Link_m0_astResp001_Async.RdCnt -graycode -module dbus_read_Structure_Module_westbus
cdc signal Link_m0_asi_main.DtpTxClkAdapt_Link_m0_ast_Async.WrCnt -graycode -module dbus_read_Structure_Module_westbus
cdc signal Link_m1_asiResp001_main.DtpRxClkAdapt_Link_m1_astResp001_Async.RdCnt -graycode -module dbus_read_Structure_Module_westbus
cdc signal Link_m1_asi_main.DtpTxClkAdapt_Link_m1_ast_Async.WrCnt -graycode -module dbus_read_Structure_Module_westbus

