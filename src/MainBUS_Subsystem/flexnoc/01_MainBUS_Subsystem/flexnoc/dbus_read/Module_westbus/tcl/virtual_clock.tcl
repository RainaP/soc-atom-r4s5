
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:26

####################################################################

arteris_virtual_clock -name "Regime_cpu" -clock_domain "Regime_cpu" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]" -edge "R" -user_directive ""
arteris_virtual_clock -name "Regime_dcluster0" -clock_domain "Regime_dcluster0" -period "${Regime_dcluster0_P}" -waveform "[expr ${Regime_dcluster0_P}*0.00] [expr ${Regime_dcluster0_P}*0.50]" -edge "R" -user_directive ""
arteris_virtual_clock -name "Regime_dcluster1" -clock_domain "Regime_dcluster1" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]" -edge "R" -user_directive ""
arteris_virtual_clock -name "Regime_dram0" -clock_domain "Regime_dram0" -period "${Regime_dram0_P}" -waveform "[expr ${Regime_dram0_P}*0.00] [expr ${Regime_dram0_P}*0.50]" -edge "R" -user_directive ""
arteris_virtual_clock -name "Regime_dram1" -clock_domain "Regime_dram1" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]" -edge "R" -user_directive ""
arteris_virtual_clock -name "Regime_dram2" -clock_domain "Regime_dram2" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]" -edge "R" -user_directive ""
arteris_virtual_clock -name "Regime_dram3" -clock_domain "Regime_dram3" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]" -edge "R" -user_directive ""
arteris_virtual_clock -name "Regime_pcie" -clock_domain "Regime_pcie" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]" -edge "R" -user_directive ""
