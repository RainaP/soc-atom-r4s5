
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:26

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy

# SYNC_LINES



# ASYNC_LINES


# PSEUDO_STATIC_LINES


# COMMENTED_LINES
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/RegData_0" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram0_Cm_root" -to_clk_P "${Regime_dram0_Cm_root_P}" -delay "[expr 2*${Regime_dram0_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/RegData_1" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram0_Cm_root" -to_clk_P "${Regime_dram0_Cm_root_P}" -delay "[expr 2*${Regime_dram0_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/RegData_2" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram0_Cm_root" -to_clk_P "${Regime_dram0_Cm_root_P}" -delay "[expr 2*${Regime_dram0_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/RegData_3" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram0_Cm_root" -to_clk_P "${Regime_dram0_Cm_root_P}" -delay "[expr 2*${Regime_dram0_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/RegData_4" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram0_Cm_root" -to_clk_P "${Regime_dram0_Cm_root_P}" -delay "[expr 2*${Regime_dram0_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async/RegData_5" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram0_Cm_root" -to_clk_P "${Regime_dram0_Cm_root_P}" -delay "[expr 2*${Regime_dram0_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/RegData_0" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram1_Cm_root" -to_clk_P "${Regime_dram1_Cm_root_P}" -delay "[expr 2*${Regime_dram1_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/RegData_1" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram1_Cm_root" -to_clk_P "${Regime_dram1_Cm_root_P}" -delay "[expr 2*${Regime_dram1_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/RegData_2" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram1_Cm_root" -to_clk_P "${Regime_dram1_Cm_root_P}" -delay "[expr 2*${Regime_dram1_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/RegData_3" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram1_Cm_root" -to_clk_P "${Regime_dram1_Cm_root_P}" -delay "[expr 2*${Regime_dram1_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/RegData_4" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram1_Cm_root" -to_clk_P "${Regime_dram1_Cm_root_P}" -delay "[expr 2*${Regime_dram1_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async/RegData_5" -from_lsb "0" -from_msb "128" -from_clk "Regime_bus_Cm_west_root" -from_clk_P "${Regime_bus_Cm_west_root_P}" -to_clk "Regime_dram1_Cm_root" -to_clk_P "${Regime_dram1_Cm_root_P}" -delay "[expr 2*${Regime_dram1_Cm_root_P}]"
