if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main/DtpRxClkAdapt_Link_m0_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0_asiResp001_main/DtpRxClkAdapt_Link_m0_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0_asi_main/DtpTxClkAdapt_Link_m0_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0w_yResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0w_yResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0w_yResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0w_yResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0w_y_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0w_y_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0w_y_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0w_y_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0w_zResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0w_zResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0w_zResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0w_zResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0w_z_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0w_z_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m0w_z_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m0w_z_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main/DtpRxClkAdapt_Link_m1_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1_asiResp001_main/DtpRxClkAdapt_Link_m1_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1_asi_main/DtpTxClkAdapt_Link_m1_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1w_yResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1w_yResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1w_yResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1w_yResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1w_y_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1w_y_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1w_y_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1w_y_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1w_zResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1w_zResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1w_zResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1w_zResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1w_z_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1w_z_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m1w_z_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m1w_z_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_westbus] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_westbus false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_bus_Cm_west_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_bus_Cm_west_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_bus_Cm_west_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_bus_Cm_west_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_yResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_yResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_yResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_yResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_y_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_y_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_y_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_y_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_zResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_zResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_zResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_zResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_z_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_z_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_z_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m0w_z_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_yResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_yResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_yResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_yResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_y_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_y_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_y_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_y_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_zResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_zResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_zResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_zResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_z_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_z_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_z_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m1w_z_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Demux_Link_m0w_yResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Demux_Link_m0w_yResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Demux_Link_m1w_yResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Demux_Link_m1w_yResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/DtpRxSerAdapt_Link_m0w_yResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/DtpRxSerAdapt_Link_m0w_yResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/DtpRxSerAdapt_Link_m1w_yResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/DtpRxSerAdapt_Link_m1w_yResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Mux_Link_c01_m01_eResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Mux_Link_c01_m01_eResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Mux_Link_n03_m01_cResp002] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Mux_Link_n03_m01_cResp002 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Mux_Link_n47_m01_cResp002] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Mux_Link_n47_m01_cResp002 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Mux_Link_n8_m01_cResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_westResp001_main/Mux_Link_n8_m01_cResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Demux_Link_c01_m01_e] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Demux_Link_c01_m01_e false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Demux_Link_n03_m01_c] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Demux_Link_n03_m01_c false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Demux_Link_n47_m01_c] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Demux_Link_n47_m01_c false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Demux_Link_n8_m01_c] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Demux_Link_n8_m01_c false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Mux_Link_m0w_y] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Mux_Link_m0w_y false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Mux_Link_m1w_y] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/Switch_west_main/Mux_Link_m1w_y false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_westResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_westResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_westResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_westResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_west_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_west_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_west_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_w/clockGaters_Switch_west_main_Sys/ClockGater false
}
