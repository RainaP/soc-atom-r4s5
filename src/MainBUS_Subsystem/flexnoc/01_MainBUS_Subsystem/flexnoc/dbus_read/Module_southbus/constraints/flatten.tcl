if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0s_aResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0s_aResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0s_aResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0s_aResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0s_a_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0s_a_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0s_a_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0s_a_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_southbus] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_southbus false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_bus_Cm_south_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_bus_Cm_south_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_bus_Cm_south_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_bus_Cm_south_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_aResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_aResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_aResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_aResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_a_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_a_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_a_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0s_a_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main/DtpTxClkAdapt_Link_c0_asiResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_astResp_main/DtpTxClkAdapt_Link_c0_asiResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main/DtpRxClkAdapt_Link_c0_asi_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_s/Link_c0_ast_main/DtpRxClkAdapt_Link_c0_asi_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_astResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_astResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_astResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_astResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_ast_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_ast_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_ast_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_s/clockGaters_Link_c0_ast_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_se/Link_c0s_bResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_se/Link_c0s_bResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_se/Link_c0s_bResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_se/Link_c0s_bResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_se/Link_c0s_b_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_se/Link_c0s_b_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_se/Link_c0s_b_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_se/Link_c0s_b_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_bResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_bResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_bResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_bResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_b_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_b_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_b_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}restr_corner_se/clockGaters_Link_c0s_b_main_Sys/ClockGater false
}
