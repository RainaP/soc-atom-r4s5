
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:22

####################################################################

arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_s_Link_c0_astResp_main" -generator "DatapathLink" -instance_name "restr_corner_s/Link_c0_astResp_main" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_s_Link_c0_ast_main" -generator "DatapathLink" -instance_name "restr_corner_s/Link_c0_ast_main" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_s_clockGaters_Link_c0_astResp_main_Sys" -generator "ClockGater" -instance_name "restr_corner_s/clockGaters_Link_c0_astResp_main_Sys" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_s_clockGaters_Link_c0_ast_main_Sys" -generator "ClockGater" -instance_name "restr_corner_s/clockGaters_Link_c0_ast_main_Sys" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_se_Link_c0s_bResp_main" -generator "DatapathLink" -instance_name "restr_corner_se/Link_c0s_bResp_main" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_se_Link_c0s_b_main" -generator "DatapathLink" -instance_name "restr_corner_se/Link_c0s_b_main" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_se_clockGaters_Link_c0s_bResp_main_Sys" -generator "ClockGater" -instance_name "restr_corner_se/clockGaters_Link_c0s_bResp_main_Sys" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_se_clockGaters_Link_c0s_b_main_Sys" -generator "ClockGater" -instance_name "restr_corner_se/clockGaters_Link_c0s_b_main_Sys" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_z_T_C_S_C_L_R_A_2_1" -generator "gate" -instance_name "restr_corner_s/uAnd2" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_z_T_C_S_C_L_R_A_2_1" -generator "gate" -instance_name "restr_corner_se/uAnd2" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_z_T_C_S_C_L_R_O_2_1" -generator "gate" -instance_name "restr_corner_s/uOr2" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_z_T_C_S_C_L_R_O_2_1" -generator "gate" -instance_name "restr_corner_se/uOr2" -level "3"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_Link_c0s_aResp_main" -generator "DatapathLink" -instance_name "Link_c0s_aResp_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_Link_c0s_a_main" -generator "DatapathLink" -instance_name "Link_c0s_a_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_Regime_bus_Cm_south_main" -generator "ClockManager" -instance_name "Regime_bus_Cm_south_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_clockGaters_Link_c0s_aResp_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c0s_aResp_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_clockGaters_Link_c0s_a_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c0s_a_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_s" -generator "" -instance_name "restr_corner_s" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_restr_corner_se" -generator "" -instance_name "restr_corner_se" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_z_T_C_S_C_L_R_A_4_1" -generator "gate" -instance_name "uAnd4" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus_z_T_C_S_C_L_R_O_4_1" -generator "gate" -instance_name "uOr4" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_southbus" -generator "" -instance_name "dbus_read_Structure_Module_southbus" -level "1"
