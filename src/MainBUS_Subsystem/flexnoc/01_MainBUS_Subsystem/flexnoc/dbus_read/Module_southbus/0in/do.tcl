# Validated with 0in (TM) version Version 10.4c_6 linux_x86_64 21 Dec 2015
# vlib work; vmap -c work work; vlog -skipsynthoffregion *.v
# qverify -c -do 0in/do.tcl
#

cdc preference -vectorize_nl

# turn on fifo synchronization recognition
#
cdc preference -fifo_scheme
#cdc preference fifo -sync_effort high
#cdc preference fifo -effort high

# turn on reconvergence
#
cdc reconvergence on
cdc preference reconvergence -depth 0 -divergence_depth 0

do 0in/dbus_read_Structure_Module_southbus.clkgrp.tcl
do 0in/dbus_read_Structure_Module_southbus.rst.tcl
do 0in/dbus_read_Structure_Module_southbus.io.tcl

do 0in/dbus_read_Structure_Module_southbus.ctrl.tcl
do 0in/dbus_read_Structure_Module_southbus.except.tcl

onerror continue

 cdc run -d dbus_read_Structure_Module_southbus -auto_black_box -hcdc -process_dead_end -cr dbus_read_Structure_Module_southbus.rpt
 cdc generate report cdc_detail.rpt

exit

