
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:22

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_southbus_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_read_Structure_Module_southbus -severity waived -scheme reconvergence -from_signals restr_corner_s.Link_c0_astResp_main.DtpTxClkAdapt_Link_c0_asiResp_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_southbus -severity waived -scheme reconvergence -from_signals restr_corner_s.Link_c0_ast_main.DtpRxClkAdapt_Link_c0_asi_Async.RdCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_read_Structure_Module_southbus -severity waived -scheme multi_sync_mux_select -from restr_corner_s.Link_c0_ast_main.DtpRxClkAdapt_Link_c0_asi_Async.uRegSync.instSynchronizerCell*

endmodule
