
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:22

####################################################################




####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:22

####################################################################

# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_Link_c0s_aResp_main_Sys.SysOut_Pwr_Idle connected to pin clockGaters_Link_c0s_aResp_main_Sys.ClockGater.SysO_0_Pwr_Idle (attached to instance clockGaters_Link_c0s_aResp_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_Link_c0s_aResp_main_Sys.SysOut_Pwr_WakeUp connected to pin clockGaters_Link_c0s_aResp_main_Sys.ClockGater.SysO_0_Pwr_WakeUp (attached to instance clockGaters_Link_c0s_aResp_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_Link_c0s_a_main_Sys.SysOut_Pwr_Idle connected to pin clockGaters_Link_c0s_a_main_Sys.ClockGater.SysO_0_Pwr_Idle (attached to instance clockGaters_Link_c0s_a_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_Link_c0s_a_main_Sys.SysOut_Pwr_WakeUp connected to pin clockGaters_Link_c0s_a_main_Sys.ClockGater.SysO_0_Pwr_WakeUp (attached to instance clockGaters_Link_c0s_a_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net restr_corner_s.clockGaters_Link_c0_astResp_main_Sys.SysOut_Pwr_Idle connected to pin restr_corner_s.clockGaters_Link_c0_astResp_main_Sys.ClockGater.SysO_0_Pwr_Idle (attached to instance restr_corner_s.clockGaters_Link_c0_astResp_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net restr_corner_s.clockGaters_Link_c0_astResp_main_Sys.SysOut_Pwr_WakeUp connected to pin restr_corner_s.clockGaters_Link_c0_astResp_main_Sys.ClockGater.SysO_0_Pwr_WakeUp (attached to instance restr_corner_s.clockGaters_Link_c0_astResp_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net restr_corner_s.clockGaters_Link_c0_ast_main_Sys.SysOut_Pwr_Idle connected to pin restr_corner_s.clockGaters_Link_c0_ast_main_Sys.ClockGater.SysO_0_Pwr_Idle (attached to instance restr_corner_s.clockGaters_Link_c0_ast_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net restr_corner_s.clockGaters_Link_c0_ast_main_Sys.SysOut_Pwr_WakeUp connected to pin restr_corner_s.clockGaters_Link_c0_ast_main_Sys.ClockGater.SysO_0_Pwr_WakeUp (attached to instance restr_corner_s.clockGaters_Link_c0_ast_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net restr_corner_se.clockGaters_Link_c0s_bResp_main_Sys.SysOut_Pwr_Idle connected to pin restr_corner_se.clockGaters_Link_c0s_bResp_main_Sys.ClockGater.SysO_0_Pwr_Idle (attached to instance restr_corner_se.clockGaters_Link_c0s_bResp_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net restr_corner_se.clockGaters_Link_c0s_bResp_main_Sys.SysOut_Pwr_WakeUp connected to pin restr_corner_se.clockGaters_Link_c0s_bResp_main_Sys.ClockGater.SysO_0_Pwr_WakeUp (attached to instance restr_corner_se.clockGaters_Link_c0s_bResp_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net restr_corner_se.clockGaters_Link_c0s_b_main_Sys.SysOut_Pwr_Idle connected to pin restr_corner_se.clockGaters_Link_c0s_b_main_Sys.ClockGater.SysO_0_Pwr_Idle (attached to instance restr_corner_se.clockGaters_Link_c0s_b_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net restr_corner_se.clockGaters_Link_c0s_b_main_Sys.SysOut_Pwr_WakeUp connected to pin restr_corner_se.clockGaters_Link_c0s_b_main_Sys.ClockGater.SysO_0_Pwr_WakeUp (attached to instance restr_corner_se.clockGaters_Link_c0s_b_main_Sys.ClockGater from module: dbus_read_Structure_Module_southbus_z_H_R_U_Cg_U_103c4e6e)
