
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:08:25

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_centerbus_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm03con.Link_n0_astResp001_main.DtpTxClkAdapt_Link_n0_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm03con.Link_n0_ast_main.DtpRxClkAdapt_Link_n0_asi_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm03con.Link_n1_astResp001_main.DtpTxClkAdapt_Link_n1_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm03con.Link_n1_ast_main.DtpRxClkAdapt_Link_n1_asi_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm03con.Link_n2_astResp001_main.DtpTxClkAdapt_Link_n2_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm03con.Link_n2_ast_main.DtpRxClkAdapt_Link_n2_asi_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm03con.Link_n3_astResp001_main.DtpTxClkAdapt_Link_n3_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm03con.Link_n3_ast_main.DtpRxClkAdapt_Link_n3_asi_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm47con.Link_n4_astResp001_main.DtpTxClkAdapt_Link_n4_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm47con.Link_n4_ast_main.DtpRxClkAdapt_Link_n4_asi_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm47con.Link_n5_astResp001_main.DtpTxClkAdapt_Link_n5_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm47con.Link_n5_ast_main.DtpRxClkAdapt_Link_n5_asi_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm47con.Link_n6_astResp001_main.DtpTxClkAdapt_Link_n6_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm47con.Link_n6_ast_main.DtpRxClkAdapt_Link_n6_asi_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm47con.Link_n7_astResp001_main.DtpTxClkAdapt_Link_n7_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme reconvergence -from_signals restr_ncm47con.Link_n7_ast_main.DtpRxClkAdapt_Link_n7_asi_Async.RdCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from restr_ncm03con.Link_n0_ast_main.DtpRxClkAdapt_Link_n0_asi_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from restr_ncm03con.Link_n1_ast_main.DtpRxClkAdapt_Link_n1_asi_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from restr_ncm03con.Link_n2_ast_main.DtpRxClkAdapt_Link_n2_asi_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from restr_ncm03con.Link_n3_ast_main.DtpRxClkAdapt_Link_n3_asi_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from restr_ncm47con.Link_n4_ast_main.DtpRxClkAdapt_Link_n4_asi_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from restr_ncm47con.Link_n5_ast_main.DtpRxClkAdapt_Link_n5_asi_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from restr_ncm47con.Link_n6_ast_main.DtpRxClkAdapt_Link_n6_asi_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_centerbus -severity waived -scheme multi_sync_mux_select -from restr_ncm47con.Link_n7_ast_main.DtpRxClkAdapt_Link_n7_asi_Async.uRegSync.instSynchronizerCell*

endmodule
