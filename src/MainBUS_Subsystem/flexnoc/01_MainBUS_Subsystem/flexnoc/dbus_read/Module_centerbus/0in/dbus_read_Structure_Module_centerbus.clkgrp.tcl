
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:08:25

####################################################################

netlist clock Regime_bus_Cm_center_main.root_Clk -module dbus_read_Structure_Module_centerbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_bus_Cm_center_main.root_Clk_ClkS -module dbus_read_Structure_Module_centerbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}
netlist clock clk_dbus -module dbus_read_Structure_Module_centerbus -group clk_dbus -period 1.000 -waveform {0.000 0.500}

