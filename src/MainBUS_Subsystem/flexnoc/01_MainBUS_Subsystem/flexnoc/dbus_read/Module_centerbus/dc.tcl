# Script dc_shell.tcl




source -echo -verbose ./dc_setup.tcl
set_app_var spg_enable_via_resistance_support "true"
set hdlin_preserve_sequential "true"
set compile_seqmap_enable_output_inversion "false"
set compile_delete_unloaded_sequential_cells "false"
set compile_seqmap_propagate_high_effort "false"
set compile_seqmap_propagate_constants "false"
set compile_enable_register_merging "false"
set_host_options -max_cores 6
# set_multi_vth_constraint -lvth_group {ulvt} -lvth_percentage $CFG(ulvtPercentage) -type hard
if {[file exists [which ${LIBRARY_DONT_USE_PRE_COMPILE_LIST}]]} {
    source -echo -verbose $LIBRARY_DONT_USE_PRE_COMPILE_LIST
}
define_design_lib WORK -path ./WORK
analyze -format verilog -define ARTERIS_REMOVE_XZ_FOR_SYNTHESIS ${RTL_SOURCE_FILES}
elaborate ${DESIGN_NAME}
write -hierarchy -format ddc -output ${RESULTS_DIR}/${DCRM_ELABORATED_DESIGN_DDC_OUTPUT_FILE}
link > reports/${DESIGN_NAME}\.link.rpt
set_multibit_options -mode timing_driven
#source tcl/topLevel.tcl
source constraints/sdcTop.sdc
if {[file exists "constraints/tunnel.tcl"]} {
  source -echo -verbose constraints/tunnel.tcl
}
set ports_clock_root [filter_collection [get_attribute [get_clocks] sources] object_class==port]
group_path -name REGOUT -to [all_outputs]
group_path -name REGIN -from [remove_from_collection [all_inputs] ${ports_clock_root}]
group_path -name FEEDTHROUGH -from [remove_from_collection [all_inputs] ${ports_clock_root}] -to [all_outputs]
identify_clock_gating
report_clock_gating -multi_stage -nosplit > ${REPORTS_DIR}/${DCRM_INSTANTIATE_CLOCK_GATES_REPORT}
set_preserve_clock_gate [get_cell $CLOCK_GATE_INST_LIST]
set compile_clock_gating_through_hierarchy true
set_app_var power_cg_physically_aware_cg true
if {[file exists [which ${DCRM_DCT_PHYSICAL_CONSTRAINTS_INPUT_FILE}]]} {
  set_app_var enable_rule_based_query true
  puts "RM-Info: Sourcing script file [which ${DCRM_DCT_PHYSICAL_CONSTRAINTS_INPUT_FILE}]"
  source -echo -verbose ${DCRM_DCT_PHYSICAL_CONSTRAINTS_INPUT_FILE}
  set_app_var enable_rule_based_query false
}
write_floorplan -all ${RESULTS_DIR}/${DCRM_DCT_FLOORPLAN_OUTPUT_FILE}
report_physical_constraints > ${REPORTS_DIR}/${DCRM_DCT_PHYSICAL_CONSTRAINTS_REPORT}
set_fix_multiple_port_nets -all -buffer_constants
check_design -summary
check_design > ${REPORTS_DIR}/${DCRM_CHECK_DESIGN_REPORT}
set_app_var compile_timing_high_effort true
set_app_var power_cg_physically_aware_cg true
set_app_var power_low_power_placement true
set_dynamic_optimization true
if {[file exists "constraints/setDontTouch.tcl"]} {
  source -echo -verbose constraints/setDontTouch.tcl
}
uniquify
compile_ultra -no_autoungroup -no_boundary_optimization -no_seq_output_inversion
define_name_rules verilog -add_dummy_nets -dummy_net_prefix "SYNOPSYS_UNCONNECTED_%d"
change_names -rules verilog -verbose -hier
source constraints/synPreserveRegisters.tcl
source constraints/flatten.tcl
compile_ultra -gate_clock -spg -no_seq_output_inversion
write -format ddc -hierarchy -output ${RESULTS_DIR}/${DCRM_COMPILE_ULTRA_DDC_OUTPUT_FILE}
identify_register_banks -output ${RESULTS_DIR}/${DCRM_MULTIBIT_CREATE_REGISTER_BANK_FILE}
source -echo -verbose ${RESULTS_DIR}/${DCRM_MULTIBIT_CREATE_REGISTER_BANK_FILE}
create_auto_path_groups -mode mapped
if {[file exists [which ${LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST}]]} {
  puts "RM-Info: Sourcing script file [which ${LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST}]"
source -echo -verbose $LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST
}
compile_ultra -incremental -scan -spg -no_seq_output_inversion
remove_auto_path_groups
optimize_netlist -area
define_name_rules verilog -add_dummy_nets -dummy_net_prefix "SYNOPSYS_UNCONNECTED_%d"
change_names -rules verilog -hierarchy
write_icc2_files -force  -output ${RESULTS_DIR}/${DCRM_FINAL_DESIGN_ICC2}
write -format verilog -hierarchy -output ${RESULTS_DIR}/${DCRM_FINAL_VERILOG_OUTPUT_FILE}
write -format ddc     -hierarchy -output ${RESULTS_DIR}/${DCRM_FINAL_DDC_OUTPUT_FILE}
write_floorplan -all ${RESULTS_DIR}/${DCRM_DCT_FINAL_FLOORPLAN_OUTPUT_FILE}
if {[info exists DCRM_DCT_SPG_PLACEMENT_OUTPUT_FILE]} {
  write_def -components -output ${RESULTS_DIR}/${DCRM_DCT_SPG_PLACEMENT_OUTPUT_FILE}
}
write_parasitics -output ${RESULTS_DIR}/${DCRM_DCT_FINAL_SPEF_OUTPUT_FILE}
write_sdf ${RESULTS_DIR}/${DCRM_DCT_FINAL_SDF_OUTPUT_FILE}
set_app_var write_sdc_output_lumped_net_capacitance false
set_app_var write_sdc_output_net_resistance false
write_sdc -nosplit ${RESULTS_DIR}/${DCRM_FINAL_SDC_OUTPUT_FILE}
report_qor > ${REPORTS_DIR}/${DCRM_FINAL_QOR_REPORT}
report_timing -transition_time -nets -attributes -nosplit > ${REPORTS_DIR}/${DCRM_FINAL_TIMING_REPORT}
report_area -physical -nosplit > ${REPORTS_DIR}/${DCRM_FINAL_AREA_REPORT}
report_area -hierarchy -physical -nosplit > ${REPORTS_DIR}/${DCRM_FINAL_HIER_AREA_REPORT}
report_resources -hierarchy > ${REPORTS_DIR}/${DCRM_FINAL_RESOURCES_REPORT}
report_clock_gating -nosplit > ${REPORTS_DIR}/${DCRM_FINAL_CLOCK_GATING_REPORT}
report_power -nosplit > ${REPORTS_DIR}/${DCRM_FINAL_POWER_REPORT}
report_congestion > ${REPORTS_DIR}/${DCRM_DCT_FINAL_CONGESTION_REPORT}
exit
