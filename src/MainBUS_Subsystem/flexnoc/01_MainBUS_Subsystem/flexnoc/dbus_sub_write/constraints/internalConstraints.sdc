
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:29:34

####################################################################

# Create Clocks

create_clock -name "clk_sub" [get_ports "clk_sub"] -period "${clk_sub_P}" -waveform "[expr ${clk_sub_P}*0.00] [expr ${clk_sub_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks clk_sub] 
# Create Virtual Clocks

# Create Generated Virtual Clocks 

# Create Generated Clocks

create_generated_clock -name "Regime_bus_Cm_center_root" [get_pins "${CUSTOMER_HIERARCHY}Regime_bus_Cm_center_main/root_Clk ${CUSTOMER_HIERARCHY}Regime_bus_Cm_center_main/root_Clk_ClkS "] -source "clk_sub" -divide_by "1"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_center_root] 

#warning:  uncomment below line will disabled inter-clock constraint(s) by considering them as false path(s)
#set_clock_groups -asynchronous  -group { clk_sub Regime_bus_Cm_center_root } 
# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_sub"]
# Clock Gating Checks 

set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_c01w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n4wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n4w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n5wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n5w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n6wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n6w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n7wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n7w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n8w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s0wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s0w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s1wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s1w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s2wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s2w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s3wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_s3w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch1Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch1_main_Sys/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port0_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port0_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port1_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port1_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port2_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port2_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port3_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster0_port3_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port0_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port0_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port1_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port1_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port2_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port2_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port3_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_dcluster1_port3_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_ddma_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_ddma_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_pcie_cpu_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dbus2sub_pcie_cpu_w_I/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm0_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm0_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm1_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm1_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm2_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm2_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm3_w/ClockGater/usce4c71b1c0/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_shm3_w_T/ClockGater/usce4c71b1c0/instGaterCell/EN"]


