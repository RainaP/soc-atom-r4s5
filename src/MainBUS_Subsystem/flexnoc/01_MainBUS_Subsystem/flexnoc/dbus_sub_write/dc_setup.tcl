# Script dc_shell.tcl



set DESIGN_NAME                               "dbus_sub_write_Structure"
set LIBRARY_DONT_USE_FILE                     ""
set LIBRARY_DONT_USE_PRE_COMPILE_LIST         ""
set LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST    ""
set ADDITIONAL_SEARCH_PATH                    ""
source -echo -verbose 
set_units -time ns -resistance kOhm -capacitance pF -voltage V -current mA
set_app_var synthetic_library dw_foundation.sldb
set_app_var link_library [concat {*} $target_library $synthetic_library]
source -echo -verbose ./dc_setup_filenames.tcl
set_app_var sh_new_variable_message false
set RTL_SOURCE_FILES  "rtl.GaterCell.v rtl.ClockManagerCell.v dbus_sub_write_Structure_commons.v dbus_sub_write_Structure.v"
set REPORTS_DIR  "./reports"
set RESULTS_DIR  "./results"
file mkdir ${REPORTS_DIR}
file mkdir ${RESULTS_DIR}
set CLOCK_GATE_INST_LIST  " clockGaters_Switch1Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n7w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n3wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_s3w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster0_port1_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n4wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster0_port0_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_s1w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n5wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n3w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch1_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n1w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm1_w_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_ddma_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster0_port2_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n0w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm2_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c01wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster1_port0_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n7wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster1_port1_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n2w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm3_w_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm0_w_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_s0wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster0_port2_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster1_port3_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster1_port0_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster1_port1_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster0_port1_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster0_port3_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_pcie_cpu_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster1_port2_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_ddma_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n1wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster1_port3_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm3_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n8w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_pcie_cpu_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster1_port2_w_I/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_s1wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster0_port3_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n6wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_s0w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm2_w_T/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_s2wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm1_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_shm0_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c01w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n4w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n0wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_s3wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_dbus2sub_dcluster0_port0_w/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n5w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_s2w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n8wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n6w_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_n2wResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell "
set OPTIMIZATION_FLOW hplp
set_app_var search_path ". ./sdc ${ADDITIONAL_SEARCH_PATH} $search_path"
set mw_design_library ${DCRM_MW_LIBRARY_NAME}
extend_mw_layers
if {![file isdirectory $mw_design_library ]} {
  create_mw_lib   -technology $MW_TECH_FILE -mw_reference_library $MW_REF_LIB_LIST $mw_design_library
} else {
set_mw_lib_reference $mw_design_library -mw_reference_library $MW_REF_LIB_LIST
}
open_mw_lib     $mw_design_library
check_library > ${REPORTS_DIR}/${DCRM_CHECK_LIBRARY_REPORT}
set_tlu_plus_files -max_tluplus $MAX_TLU_PLUS -min_tluplus $MIN_TLU_PLUS
check_tlu_plus_files
if {[file exists [which ${LIBRARY_DONT_USE_FILE}]]} {
  source -echo -verbose ${LIBRARY_DONT_USE_FILE}
}
