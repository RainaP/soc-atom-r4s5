
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:28:43

####################################################################

netlist clock Regime_bus_Cm_center_main.root_Clk -module dbus_sub_write_Structure -group clk_sub -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_bus_Cm_center_main.root_Clk_ClkS -module dbus_sub_write_Structure -group clk_sub -period 1.000 -waveform {0.000 0.500}
netlist clock clk_sub -module dbus_sub_write_Structure -group clk_sub -period 1.000 -waveform {0.000 0.500}

