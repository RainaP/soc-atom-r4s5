
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:25

####################################################################

arteris_clock -name "clk_cbus_ls_east" -clock_domain "clk_cbus_ls_east" -port "clk_cbus_ls_east" -period "${clk_cbus_ls_east_P}" -waveform "[expr ${clk_cbus_ls_east_P}*0.00] [expr ${clk_cbus_ls_east_P}*0.50]" -edge "R" -user_directive ""
