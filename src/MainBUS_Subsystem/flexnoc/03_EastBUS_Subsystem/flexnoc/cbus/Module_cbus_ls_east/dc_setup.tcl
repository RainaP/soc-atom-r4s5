# Script dc_shell.tcl



set DESIGN_NAME                               "cbus_Structure_Module_cbus_ls_east"
set LIBRARY_DONT_USE_FILE                     ""
set LIBRARY_DONT_USE_PRE_COMPILE_LIST         ""
set LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST    ""
set ADDITIONAL_SEARCH_PATH                    ""
source -echo -verbose 
set_units -time ns -resistance kOhm -capacitance pF -voltage V -current mA
set_app_var synthetic_library dw_foundation.sldb
set_app_var link_library [concat {*} $target_library $synthetic_library]
source -echo -verbose ./dc_setup_filenames.tcl
set_app_var sh_new_variable_message false
set RTL_SOURCE_FILES  "rtl.GaterCell.v rtl.ClockManagerCell.v rtl.SynchronizerCell.v cbus_Structure_Module_cbus_ls_east_restr_corner_e.v cbus_Structure_Module_cbus_ls_east_commons.v cbus_Structure_Module_cbus_ls_east.v"
set REPORTS_DIR  "./reports"
set RESULTS_DIR  "./results"
file mkdir ${REPORTS_DIR}
file mkdir ${RESULTS_DIR}
set CLOCK_GATE_INST_LIST  " clockGaters_Link36_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch3_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m3ctrl_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch_ctrl_t_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link10_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link17_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Switch_ctrl_tResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c1ctrl_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link35_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_c1ctrl_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link3_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link37_main_Sys/ClockGater/usce4c71b1c0/instGaterCell clockGaters_Link_m2ctrl_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell "
set OPTIMIZATION_FLOW hplp
set_app_var search_path ". ./sdc ${ADDITIONAL_SEARCH_PATH} $search_path"
set mw_design_library ${DCRM_MW_LIBRARY_NAME}
extend_mw_layers
if {![file isdirectory $mw_design_library ]} {
  create_mw_lib   -technology $MW_TECH_FILE -mw_reference_library $MW_REF_LIB_LIST $mw_design_library
} else {
set_mw_lib_reference $mw_design_library -mw_reference_library $MW_REF_LIB_LIST
}
open_mw_lib     $mw_design_library
check_library > ${REPORTS_DIR}/${DCRM_CHECK_LIBRARY_REPORT}
set_tlu_plus_files -max_tluplus $MAX_TLU_PLUS -min_tluplus $MIN_TLU_PLUS
check_tlu_plus_files
if {[file exists [which ${LIBRARY_DONT_USE_FILE}]]} {
  source -echo -verbose ${LIBRARY_DONT_USE_FILE}
}
