
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:25

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_cbus_ls_east_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link10_main.DtpRxClkAdapt_Link_m3ctrl_astResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link17_main.DtpRxClkAdapt_Switch_ctrl_i_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link35_main.DtpTxClkAdapt_Switch_ctrl_iResp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link36_main.DtpTxClkAdapt_Link5_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link37_main.DtpRxClkAdapt_Link13_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link3_main.DtpTxClkAdapt_Link_m2ctrl_ast_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link_c1ctrl_asiResp001_main.DtpRxClkAdapt_Link_c1ctrl_astResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link_c1ctrl_asi_main.DtpTxClkAdapt_Link_c1ctrl_ast_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link_m2ctrl_asiResp001_main.DtpRxClkAdapt_Link_m2ctrl_astResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Link_m3ctrl_asi_main.DtpTxClkAdapt_Link_m3ctrl_ast_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link31_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link32_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link33_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link34_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme reconvergence -from_signals Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Link10_main.DtpRxClkAdapt_Link_m3ctrl_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Link17_main.DtpRxClkAdapt_Switch_ctrl_i_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Link37_main.DtpRxClkAdapt_Link13_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Link_c1ctrl_asiResp001_main.DtpRxClkAdapt_Link_c1ctrl_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Link_m2ctrl_asiResp001_main.DtpRxClkAdapt_Link_m2ctrl_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link31_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link32_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link33_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_cbus_ls_east -severity waived -scheme multi_sync_mux_select -from Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link34_Async.uRegSync.instSynchronizerCell*

endmodule
