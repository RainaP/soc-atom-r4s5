
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:25

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_cbus_ls_east_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link35_main.DtpTxClkAdapt_Switch_ctrl_iResp001_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link35_main.DtpTxClkAdapt_Switch_ctrl_iResp001_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link35_main.DtpTxClkAdapt_Switch_ctrl_iResp001_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link35_main.DtpTxClkAdapt_Switch_ctrl_iResp001_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link36_main.DtpTxClkAdapt_Link5_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link36_main.DtpTxClkAdapt_Link5_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link36_main.DtpTxClkAdapt_Link5_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link36_main.DtpTxClkAdapt_Link5_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link3_main.DtpTxClkAdapt_Link_m2ctrl_ast_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link3_main.DtpTxClkAdapt_Link_m2ctrl_ast_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link3_main.DtpTxClkAdapt_Link_m2ctrl_ast_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link3_main.DtpTxClkAdapt_Link_m2ctrl_ast_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link_c1ctrl_asi_main.DtpTxClkAdapt_Link_c1ctrl_ast_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link_c1ctrl_asi_main.DtpTxClkAdapt_Link_c1ctrl_ast_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link_c1ctrl_asi_main.DtpTxClkAdapt_Link_c1ctrl_ast_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link_c1ctrl_asi_main.DtpTxClkAdapt_Link_c1ctrl_ast_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link_m3ctrl_asi_main.DtpTxClkAdapt_Link_m3ctrl_ast_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link_m3ctrl_asi_main.DtpTxClkAdapt_Link_m3ctrl_ast_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link_m3ctrl_asi_main.DtpTxClkAdapt_Link_m3ctrl_ast_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Link_m3ctrl_asi_main.DtpTxClkAdapt_Link_m3ctrl_ast_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.RegData_5

endmodule
