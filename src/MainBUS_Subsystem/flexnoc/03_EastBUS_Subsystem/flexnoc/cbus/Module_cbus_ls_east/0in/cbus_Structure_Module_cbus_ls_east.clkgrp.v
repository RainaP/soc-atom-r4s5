
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:25

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_cbus_ls_east_clkgrp;

// 0in set_cdc_clock Regime_cbus_ls_east_Cm_main.root_Clk -module cbus_Structure_Module_cbus_ls_east -group clk_cbus_ls_east -period 5.000 -waveform {0.000 2.500}
// 0in set_cdc_clock Regime_cbus_ls_east_Cm_main.root_Clk_ClkS -module cbus_Structure_Module_cbus_ls_east -group clk_cbus_ls_east -period 5.000 -waveform {0.000 2.500}
// 0in set_cdc_clock clk_cbus_ls_east -module cbus_Structure_Module_cbus_ls_east -group clk_cbus_ls_east -period 5.000 -waveform {0.000 2.500}

endmodule
