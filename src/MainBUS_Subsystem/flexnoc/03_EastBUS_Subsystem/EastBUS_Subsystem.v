//--------------------------------------------------------------------------------
//
//   Rebellions Inc. Confidential
//   All Rights Reserved -- Property of Rebellions Inc. 
//
//   Description : 
//
//   Author : jsyoon@rebellions.ai
//
//   date : 2021/08/07
//
//--------------------------------------------------------------------------------

module EastBUS_Subsystem(


	input         TM                                                                    ,
	input         arstn_cbus_ls_east                                                    ,
	input         clk_cbus_ls_east                                                      ,

	input  [57:0] ctrl_dp_Link13_to_Link37_Data                                              ,
	output [2:0]  ctrl_dp_Link13_to_Link37_RdCnt                                             ,
	output [1:0]  ctrl_dp_Link13_to_Link37_RdPtr                                             ,
	input         ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRst                                    ,
	output        ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRstAck                                 ,
	output        ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRst                                    ,
	input         ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRstAck                                 ,
	input  [2:0]  ctrl_dp_Link13_to_Link37_WrCnt                                             ,
	input  [57:0] ctrl_dp_Link31_to_Switch_ctrl_tResp001_Data                                ,
	output [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdCnt                               ,
	output [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdPtr                               ,
	input         ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst                      ,
	output        ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck                   ,
	output        ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst                      ,
	input         ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck                   ,
	input  [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_WrCnt                               ,
	input  [57:0] ctrl_dp_Link32_to_Switch_ctrl_tResp001_Data                                ,
	output [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdCnt                               ,
	output [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdPtr                               ,
	input         ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst                      ,
	output        ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck                   ,
	output        ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst                      ,
	input         ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck                   ,
	input  [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_WrCnt                               ,
	input  [57:0] ctrl_dp_Link33_to_Switch_ctrl_tResp001_Data                                ,
	output [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdCnt                               ,
	output [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdPtr                               ,
	input         ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst                      ,
	output        ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck                   ,
	output        ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst                      ,
	input         ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck                   ,
	input  [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_WrCnt                               ,
	input  [57:0] ctrl_dp_Link34_to_Switch_ctrl_tResp001_Data                                ,
	output [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdCnt                               ,
	output [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdPtr                               ,
	input         ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst                      ,
	output        ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck                   ,
	output        ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst                      ,
	input         ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck                   ,
	input  [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_WrCnt                               ,
	output [57:0] ctrl_dp_Link35_to_Switch_ctrl_iResp001_Data                                ,
	input  [2:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdCnt                               ,
	input  [1:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdPtr                               ,
	output        ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRst                      ,
	input         ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRstAck                   ,
	input         ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRst                      ,
	output        ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRstAck                   ,
	output [2:0]  ctrl_dp_Link35_to_Switch_ctrl_iResp001_WrCnt                               ,
	output [57:0] ctrl_dp_Link36_to_Link5_Data                                               ,
	input  [2:0]  ctrl_dp_Link36_to_Link5_RdCnt                                              ,
	input  [1:0]  ctrl_dp_Link36_to_Link5_RdPtr                                              ,
	output        ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRst                                     ,
	input         ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRstAck                                  ,
	input         ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRst                                     ,
	output        ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRstAck                                  ,
	output [2:0]  ctrl_dp_Link36_to_Link5_WrCnt                                              ,
	output [57:0] ctrl_dp_Link3_to_Link_m2ctrl_ast_Data                                      ,
	input  [2:0]  ctrl_dp_Link3_to_Link_m2ctrl_ast_RdCnt                                     ,
	input  [1:0]  ctrl_dp_Link3_to_Link_m2ctrl_ast_RdPtr                                     ,
	output        ctrl_dp_Link3_to_Link_m2ctrl_ast_RxCtl_PwrOnRst                            ,
	input         ctrl_dp_Link3_to_Link_m2ctrl_ast_RxCtl_PwrOnRstAck                         ,
	input         ctrl_dp_Link3_to_Link_m2ctrl_ast_TxCtl_PwrOnRst                            ,
	output        ctrl_dp_Link3_to_Link_m2ctrl_ast_TxCtl_PwrOnRstAck                         ,
	output [2:0]  ctrl_dp_Link3_to_Link_m2ctrl_ast_WrCnt                                     ,
	output [57:0] ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data                            ,
	input  [2:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt                           ,
	input  [1:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr                           ,
	output        ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck               ,
	input         ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt                           ,
	input  [57:0] ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data              ,
	output [2:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt             ,
	output [1:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr             ,
	input         ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck ,
	output        ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt             ,
	input  [57:0] ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_Data              ,
	output [2:0]  ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RdCnt             ,
	output [1:0]  ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RdPtr             ,
	input         ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RxCtl_PwrOnRstAck ,
	output        ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_TxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]  ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_WrCnt             ,
	output [57:0] ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_Data                            ,
	input  [2:0]  ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RdCnt                           ,
	input  [1:0]  ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RdPtr                           ,
	output        ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RxCtl_PwrOnRstAck               ,
	input         ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_TxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]  ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_WrCnt                           ,
	input  [57:0] ctrl_dp_Link_m3ctrl_astResp001_to_Link10_Data                              ,
	output [2:0]  ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RdCnt                             ,
	output [1:0]  ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RdPtr                             ,
	input         ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RxCtl_PwrOnRst                    ,
	output        ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RxCtl_PwrOnRstAck                 ,
	output        ctrl_dp_Link_m3ctrl_astResp001_to_Link10_TxCtl_PwrOnRst                    ,
	input         ctrl_dp_Link_m3ctrl_astResp001_to_Link10_TxCtl_PwrOnRstAck                 ,
	input  [2:0]  ctrl_dp_Link_m3ctrl_astResp001_to_Link10_WrCnt                             ,
	input  [57:0] ctrl_dp_Switch_ctrl_i_to_Link17_Data                                       ,
	output [2:0]  ctrl_dp_Switch_ctrl_i_to_Link17_RdCnt                                      ,
	output [1:0]  ctrl_dp_Switch_ctrl_i_to_Link17_RdPtr                                      ,
	input         ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRst                             ,
	output        ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRstAck                          ,
	output        ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRst                             ,
	input         ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRstAck                          ,
	input  [2:0]  ctrl_dp_Switch_ctrl_i_to_Link17_WrCnt                                      ,
	output [57:0] ctrl_dp_Switch_ctrl_t_to_Link27_Data                                       ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_RdCnt                                      ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_RdPtr                                      ,
	output        ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst                             ,
	input         ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck                          ,
	input         ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst                             ,
	output        ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck                          ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_WrCnt                                      ,
	output [57:0] ctrl_dp_Switch_ctrl_t_to_Link28_Data                                       ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_RdCnt                                      ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_RdPtr                                      ,
	output        ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst                             ,
	input         ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck                          ,
	input         ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst                             ,
	output        ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck                          ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_WrCnt                                      ,
	output [57:0] ctrl_dp_Switch_ctrl_t_to_Link29_Data                                       ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_RdCnt                                      ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_RdPtr                                      ,
	output        ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst                             ,
	input         ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck                          ,
	input         ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst                             ,
	output        ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck                          ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_WrCnt                                      ,
	output [57:0] ctrl_dp_Switch_ctrl_t_to_Link30_Data                                       ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_RdCnt                                      ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_RdPtr                                      ,
	output        ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst                             ,
	input         ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck                          ,
	input         ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst                             ,
	output        ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck                          ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_WrCnt                                      




);


	cbus_Structure_Module_cbus_ls_east u_cbus_Structure_Module_cbus_ls_east(
		.TM( TM )
	,	.arstn_cbus_ls_east( arstn_cbus_ls_east )
	,	.clk_cbus_ls_east( clk_cbus_ls_east )
	,	.dp_Link13_to_Link37_Data( ctrl_dp_Link13_to_Link37_Data )
	,	.dp_Link13_to_Link37_RdCnt( ctrl_dp_Link13_to_Link37_RdCnt )
	,	.dp_Link13_to_Link37_RdPtr( ctrl_dp_Link13_to_Link37_RdPtr )
	,	.dp_Link13_to_Link37_RxCtl_PwrOnRst( ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRst )
	,	.dp_Link13_to_Link37_RxCtl_PwrOnRstAck( ctrl_dp_Link13_to_Link37_RxCtl_PwrOnRstAck )
	,	.dp_Link13_to_Link37_TxCtl_PwrOnRst( ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRst )
	,	.dp_Link13_to_Link37_TxCtl_PwrOnRstAck( ctrl_dp_Link13_to_Link37_TxCtl_PwrOnRstAck )
	,	.dp_Link13_to_Link37_WrCnt( ctrl_dp_Link13_to_Link37_WrCnt )
	,	.dp_Link31_to_Switch_ctrl_tResp001_Data( ctrl_dp_Link31_to_Switch_ctrl_tResp001_Data )
	,	.dp_Link31_to_Switch_ctrl_tResp001_RdCnt( ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdCnt )
	,	.dp_Link31_to_Switch_ctrl_tResp001_RdPtr( ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdPtr )
	,	.dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst( ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst )
	,	.dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst( ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst )
	,	.dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link31_to_Switch_ctrl_tResp001_WrCnt( ctrl_dp_Link31_to_Switch_ctrl_tResp001_WrCnt )
	,	.dp_Link32_to_Switch_ctrl_tResp001_Data( ctrl_dp_Link32_to_Switch_ctrl_tResp001_Data )
	,	.dp_Link32_to_Switch_ctrl_tResp001_RdCnt( ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdCnt )
	,	.dp_Link32_to_Switch_ctrl_tResp001_RdPtr( ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdPtr )
	,	.dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst( ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst )
	,	.dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst( ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst )
	,	.dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link32_to_Switch_ctrl_tResp001_WrCnt( ctrl_dp_Link32_to_Switch_ctrl_tResp001_WrCnt )
	,	.dp_Link33_to_Switch_ctrl_tResp001_Data( ctrl_dp_Link33_to_Switch_ctrl_tResp001_Data )
	,	.dp_Link33_to_Switch_ctrl_tResp001_RdCnt( ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdCnt )
	,	.dp_Link33_to_Switch_ctrl_tResp001_RdPtr( ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdPtr )
	,	.dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst( ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst )
	,	.dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst( ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst )
	,	.dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link33_to_Switch_ctrl_tResp001_WrCnt( ctrl_dp_Link33_to_Switch_ctrl_tResp001_WrCnt )
	,	.dp_Link34_to_Switch_ctrl_tResp001_Data( ctrl_dp_Link34_to_Switch_ctrl_tResp001_Data )
	,	.dp_Link34_to_Switch_ctrl_tResp001_RdCnt( ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdCnt )
	,	.dp_Link34_to_Switch_ctrl_tResp001_RdPtr( ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdPtr )
	,	.dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst( ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst )
	,	.dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst( ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst )
	,	.dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link34_to_Switch_ctrl_tResp001_WrCnt( ctrl_dp_Link34_to_Switch_ctrl_tResp001_WrCnt )
	,	.dp_Link35_to_Switch_ctrl_iResp001_Data( ctrl_dp_Link35_to_Switch_ctrl_iResp001_Data )
	,	.dp_Link35_to_Switch_ctrl_iResp001_RdCnt( ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdCnt )
	,	.dp_Link35_to_Switch_ctrl_iResp001_RdPtr( ctrl_dp_Link35_to_Switch_ctrl_iResp001_RdPtr )
	,	.dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRst( ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRst )
	,	.dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link35_to_Switch_ctrl_iResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRst( ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRst )
	,	.dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link35_to_Switch_ctrl_iResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link35_to_Switch_ctrl_iResp001_WrCnt( ctrl_dp_Link35_to_Switch_ctrl_iResp001_WrCnt )
	,	.dp_Link36_to_Link5_Data( ctrl_dp_Link36_to_Link5_Data )
	,	.dp_Link36_to_Link5_RdCnt( ctrl_dp_Link36_to_Link5_RdCnt )
	,	.dp_Link36_to_Link5_RdPtr( ctrl_dp_Link36_to_Link5_RdPtr )
	,	.dp_Link36_to_Link5_RxCtl_PwrOnRst( ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRst )
	,	.dp_Link36_to_Link5_RxCtl_PwrOnRstAck( ctrl_dp_Link36_to_Link5_RxCtl_PwrOnRstAck )
	,	.dp_Link36_to_Link5_TxCtl_PwrOnRst( ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRst )
	,	.dp_Link36_to_Link5_TxCtl_PwrOnRstAck( ctrl_dp_Link36_to_Link5_TxCtl_PwrOnRstAck )
	,	.dp_Link36_to_Link5_WrCnt( ctrl_dp_Link36_to_Link5_WrCnt )
	,	.dp_Link3_to_Link_m2ctrl_ast_Data( ctrl_dp_Link3_to_Link_m2ctrl_ast_Data )
	,	.dp_Link3_to_Link_m2ctrl_ast_RdCnt( ctrl_dp_Link3_to_Link_m2ctrl_ast_RdCnt )
	,	.dp_Link3_to_Link_m2ctrl_ast_RdPtr( ctrl_dp_Link3_to_Link_m2ctrl_ast_RdPtr )
	,	.dp_Link3_to_Link_m2ctrl_ast_RxCtl_PwrOnRst( ctrl_dp_Link3_to_Link_m2ctrl_ast_RxCtl_PwrOnRst )
	,	.dp_Link3_to_Link_m2ctrl_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link3_to_Link_m2ctrl_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link3_to_Link_m2ctrl_ast_TxCtl_PwrOnRst( ctrl_dp_Link3_to_Link_m2ctrl_ast_TxCtl_PwrOnRst )
	,	.dp_Link3_to_Link_m2ctrl_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link3_to_Link_m2ctrl_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link3_to_Link_m2ctrl_ast_WrCnt( ctrl_dp_Link3_to_Link_m2ctrl_ast_WrCnt )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt )
	,	.dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_Data( ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_Data )
	,	.dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RdCnt( ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RdCnt )
	,	.dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RdPtr( ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RdPtr )
	,	.dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_WrCnt( ctrl_dp_Link_m2ctrl_astResp001_to_Link_m2ctrl_asiResp001_WrCnt )
	,	.dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_Data( ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_Data )
	,	.dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RdCnt( ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RdCnt )
	,	.dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RdPtr( ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RdPtr )
	,	.dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RxCtl_PwrOnRst( ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_TxCtl_PwrOnRst( ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_WrCnt( ctrl_dp_Link_m3ctrl_asi_to_Link_m3ctrl_ast_WrCnt )
	,	.dp_Link_m3ctrl_astResp001_to_Link10_Data( ctrl_dp_Link_m3ctrl_astResp001_to_Link10_Data )
	,	.dp_Link_m3ctrl_astResp001_to_Link10_RdCnt( ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RdCnt )
	,	.dp_Link_m3ctrl_astResp001_to_Link10_RdPtr( ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RdPtr )
	,	.dp_Link_m3ctrl_astResp001_to_Link10_RxCtl_PwrOnRst( ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RxCtl_PwrOnRst )
	,	.dp_Link_m3ctrl_astResp001_to_Link10_RxCtl_PwrOnRstAck( ctrl_dp_Link_m3ctrl_astResp001_to_Link10_RxCtl_PwrOnRstAck )
	,	.dp_Link_m3ctrl_astResp001_to_Link10_TxCtl_PwrOnRst( ctrl_dp_Link_m3ctrl_astResp001_to_Link10_TxCtl_PwrOnRst )
	,	.dp_Link_m3ctrl_astResp001_to_Link10_TxCtl_PwrOnRstAck( ctrl_dp_Link_m3ctrl_astResp001_to_Link10_TxCtl_PwrOnRstAck )
	,	.dp_Link_m3ctrl_astResp001_to_Link10_WrCnt( ctrl_dp_Link_m3ctrl_astResp001_to_Link10_WrCnt )
	,	.dp_Switch_ctrl_i_to_Link17_Data( ctrl_dp_Switch_ctrl_i_to_Link17_Data )
	,	.dp_Switch_ctrl_i_to_Link17_RdCnt( ctrl_dp_Switch_ctrl_i_to_Link17_RdCnt )
	,	.dp_Switch_ctrl_i_to_Link17_RdPtr( ctrl_dp_Switch_ctrl_i_to_Link17_RdPtr )
	,	.dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_i_to_Link17_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_i_to_Link17_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_i_to_Link17_WrCnt( ctrl_dp_Switch_ctrl_i_to_Link17_WrCnt )
	,	.dp_Switch_ctrl_t_to_Link27_Data( ctrl_dp_Switch_ctrl_t_to_Link27_Data )
	,	.dp_Switch_ctrl_t_to_Link27_RdCnt( ctrl_dp_Switch_ctrl_t_to_Link27_RdCnt )
	,	.dp_Switch_ctrl_t_to_Link27_RdPtr( ctrl_dp_Switch_ctrl_t_to_Link27_RdPtr )
	,	.dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link27_WrCnt( ctrl_dp_Switch_ctrl_t_to_Link27_WrCnt )
	,	.dp_Switch_ctrl_t_to_Link28_Data( ctrl_dp_Switch_ctrl_t_to_Link28_Data )
	,	.dp_Switch_ctrl_t_to_Link28_RdCnt( ctrl_dp_Switch_ctrl_t_to_Link28_RdCnt )
	,	.dp_Switch_ctrl_t_to_Link28_RdPtr( ctrl_dp_Switch_ctrl_t_to_Link28_RdPtr )
	,	.dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link28_WrCnt( ctrl_dp_Switch_ctrl_t_to_Link28_WrCnt )
	,	.dp_Switch_ctrl_t_to_Link29_Data( ctrl_dp_Switch_ctrl_t_to_Link29_Data )
	,	.dp_Switch_ctrl_t_to_Link29_RdCnt( ctrl_dp_Switch_ctrl_t_to_Link29_RdCnt )
	,	.dp_Switch_ctrl_t_to_Link29_RdPtr( ctrl_dp_Switch_ctrl_t_to_Link29_RdPtr )
	,	.dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link29_WrCnt( ctrl_dp_Switch_ctrl_t_to_Link29_WrCnt )
	,	.dp_Switch_ctrl_t_to_Link30_Data( ctrl_dp_Switch_ctrl_t_to_Link30_Data )
	,	.dp_Switch_ctrl_t_to_Link30_RdCnt( ctrl_dp_Switch_ctrl_t_to_Link30_RdCnt )
	,	.dp_Switch_ctrl_t_to_Link30_RdPtr( ctrl_dp_Switch_ctrl_t_to_Link30_RdPtr )
	,	.dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link30_WrCnt( ctrl_dp_Switch_ctrl_t_to_Link30_WrCnt )
	);




endmodule
