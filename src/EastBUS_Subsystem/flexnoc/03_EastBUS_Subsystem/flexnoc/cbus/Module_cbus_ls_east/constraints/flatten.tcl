if {[get_cells ${CUSTOMER_HIERARCHY}Link10_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link10_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link10_main/DtpRxClkAdapt_Link_m3ctrl_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link10_main/DtpRxClkAdapt_Link_m3ctrl_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link17_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link17_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link17_main/DtpRxClkAdapt_Switch_ctrl_i_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link17_main/DtpRxClkAdapt_Switch_ctrl_i_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link18_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link18_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link22_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link22_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link35_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link35_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link35_main/DtpTxClkAdapt_Switch_ctrl_iResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link35_main/DtpTxClkAdapt_Switch_ctrl_iResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link36_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link36_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link36_main/DtpTxClkAdapt_Link5_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link36_main/DtpTxClkAdapt_Link5_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link37_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link37_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link37_main/DtpRxClkAdapt_Link13_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link37_main/DtpRxClkAdapt_Link13_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link3_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link3_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link3_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link3_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link3_main/DtpTxClkAdapt_Link_m2ctrl_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link3_main/DtpTxClkAdapt_Link_m2ctrl_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1ctrl_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1ctrl_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1ctrl_asiResp001_main/DtpRxClkAdapt_Link_c1ctrl_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1ctrl_asiResp001_main/DtpRxClkAdapt_Link_c1ctrl_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1ctrl_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1ctrl_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1ctrl_asi_main/DtpTxClkAdapt_Link_c1ctrl_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1ctrl_asi_main/DtpTxClkAdapt_Link_c1ctrl_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2ctrl_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2ctrl_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2ctrl_asiResp001_main/DtpRxClkAdapt_Link_m2ctrl_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2ctrl_asiResp001_main/DtpRxClkAdapt_Link_m2ctrl_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3ctrl_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3ctrl_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m3ctrl_asi_main/DtpTxClkAdapt_Link_m3ctrl_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m3ctrl_asi_main/DtpTxClkAdapt_Link_m3ctrl_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_cbus_ls_east] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_cbus_ls_east false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cbus_ls_east_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cbus_ls_east_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cbus_ls_east_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cbus_ls_east_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch3_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch3_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch3_main/Mux_Link36] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch3_main/Mux_Link36 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Demux_Link31] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Demux_Link31 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Demux_Link32] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Demux_Link32 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Demux_Link33] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Demux_Link33 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Demux_Link34] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Demux_Link34 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/DtpRxClkAdapt_Link31_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/DtpRxClkAdapt_Link31_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/DtpRxClkAdapt_Link32_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/DtpRxClkAdapt_Link32_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/DtpRxClkAdapt_Link33_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/DtpRxClkAdapt_Link33_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/DtpRxClkAdapt_Link34_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/DtpRxClkAdapt_Link34_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Mux_Link18] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Mux_Link18 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Mux_Link35] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_tResp001_main/Mux_Link35 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Demux_Link17] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Demux_Link17 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Demux_Link22] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Demux_Link22 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/DtpTxClkAdapt_Link27_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/DtpTxClkAdapt_Link27_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/DtpTxClkAdapt_Link28_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/DtpTxClkAdapt_Link28_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/DtpTxClkAdapt_Link29_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/DtpTxClkAdapt_Link29_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/DtpTxClkAdapt_Link30_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/DtpTxClkAdapt_Link30_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Mux_Link27] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Mux_Link27 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Mux_Link28] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Mux_Link28 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Mux_Link29] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Mux_Link29 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Mux_Link30] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_ctrl_t_main/Mux_Link30 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_main/Demux_Link37] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_main/Demux_Link37 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link10_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link10_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link10_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link10_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link17_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link17_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link17_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link17_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link35_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link35_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link35_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link35_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link36_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link36_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link36_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link36_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link37_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link37_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link37_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link37_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link3_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link3_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link3_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link3_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2ctrl_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2ctrl_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2ctrl_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2ctrl_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3ctrl_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3ctrl_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m3ctrl_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m3ctrl_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch3_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch3_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch3_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch3_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_tResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_tResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_tResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_tResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_t_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_t_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_t_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_ctrl_t_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_main_Sys/ClockGater false
}
