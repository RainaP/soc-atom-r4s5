
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:25

####################################################################

arteris_gen_clock -name "Regime_cbus_ls_east_Cm_root" -pin "Regime_cbus_ls_east_Cm_main/root_Clk Regime_cbus_ls_east_Cm_main/root_Clk_ClkS" -clock_domain "clk_cbus_ls_east" -spec_domain_clock "/Regime_cbus_ls_east/Cm/root" -divide_by "1" -source "clk_cbus_ls_east" -source_period "${clk_cbus_ls_east_P}" -source_waveform "[expr ${clk_cbus_ls_east_P}*0.00] [expr ${clk_cbus_ls_east_P}*0.50]" -user_directive "" -add "FALSE"
