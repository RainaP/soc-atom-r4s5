
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:25

####################################################################

# FIFO module definition
cdc fifo -module cbus_Structure_Module_cbus_ls_east Link35_main.DtpTxClkAdapt_Switch_ctrl_iResp001_Async.RegData_*
cdc fifo -module cbus_Structure_Module_cbus_ls_east Link36_main.DtpTxClkAdapt_Link5_Async.RegData_*
cdc fifo -module cbus_Structure_Module_cbus_ls_east Link3_main.DtpTxClkAdapt_Link_m2ctrl_ast_Async.RegData_*
cdc fifo -module cbus_Structure_Module_cbus_ls_east Link_c1ctrl_asi_main.DtpTxClkAdapt_Link_c1ctrl_ast_Async.RegData_*
cdc fifo -module cbus_Structure_Module_cbus_ls_east Link_m3ctrl_asi_main.DtpTxClkAdapt_Link_m3ctrl_ast_Async.RegData_*
cdc fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.RegData_*
cdc fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.RegData_*
cdc fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.RegData_*
cdc fifo -module cbus_Structure_Module_cbus_ls_east Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.RegData_*

