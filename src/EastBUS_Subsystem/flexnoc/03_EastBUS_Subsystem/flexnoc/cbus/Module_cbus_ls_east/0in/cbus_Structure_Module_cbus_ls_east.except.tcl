
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:25

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link10_main.DtpRxClkAdapt_Link_m3ctrl_astResp001_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link17_main.DtpRxClkAdapt_Switch_ctrl_i_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link35_main.DtpTxClkAdapt_Switch_ctrl_iResp001_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link36_main.DtpTxClkAdapt_Link5_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link37_main.DtpRxClkAdapt_Link13_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link3_main.DtpTxClkAdapt_Link_m2ctrl_ast_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link_c1ctrl_asiResp001_main.DtpRxClkAdapt_Link_c1ctrl_astResp001_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link_c1ctrl_asi_main.DtpTxClkAdapt_Link_c1ctrl_ast_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link_m2ctrl_asiResp001_main.DtpRxClkAdapt_Link_m2ctrl_astResp001_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Link_m3ctrl_asi_main.DtpTxClkAdapt_Link_m3ctrl_ast_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link31_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link32_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link33_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Switch_ctrl_tResp001_main.DtpRxClkAdapt_Link34_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Switch_ctrl_t_main.DtpTxClkAdapt_Link27_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Switch_ctrl_t_main.DtpTxClkAdapt_Link28_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Switch_ctrl_t_main.DtpTxClkAdapt_Link29_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east
cdc signal Switch_ctrl_t_main.DtpTxClkAdapt_Link30_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_east

