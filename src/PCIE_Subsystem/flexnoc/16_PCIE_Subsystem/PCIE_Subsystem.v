//--------------------------------------------------------------------------------
//
//   Rebellions Inc. Confidential
//   All Rights Reserved -- Property of Rebellions Inc. 
//
//   Description : 
//
//   Author : jsyoon@rebellions.ai
//
//   date : 2021/08/07
//
//--------------------------------------------------------------------------------

module PCIE_Subsystem(

	input         TM                                                            ,
	input         arstn_pcie                                                    ,
	input         clk_pcie                                                      ,

	input  [57:0] ctrl_dp_Link_c0_asi_to_Link_c0_ast_Data                            ,
	output [2:0]  ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdCnt                           ,
	output [2:0]  ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdPtr                           ,
	input         ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck               ,
	output        ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]  ctrl_dp_Link_c0_asi_to_Link_c0_ast_WrCnt                           ,
	output [57:0] ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data              ,
	input  [2:0]  ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt             ,
	input  [2:0]  ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr             ,
	output        ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck ,
	input         ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]  ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt             ,

	output [128:0] r_dp_Link_c0_asi_to_Link_c0_ast_Data                      ,
	input  [2:0]   r_dp_Link_c0_asi_to_Link_c0_ast_RdCnt                     ,
	input  [2:0]   r_dp_Link_c0_asi_to_Link_c0_ast_RdPtr                     ,
	output         r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst            ,
	input          r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck         ,
	input          r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst            ,
	output         r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck         ,
	output [2:0]   r_dp_Link_c0_asi_to_Link_c0_ast_WrCnt                     ,
	input  [705:0] r_dp_Link_c0_astResp_to_Link_c0_asiResp_Data              ,
	output [2:0]   r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt             ,
	output [2:0]   r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr             ,
	input          r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst    ,
	output         r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck ,
	output         r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst    ,
	input          r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck ,
	input  [2:0]   r_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt             ,

	output [702:0] w_dp_Link_c0_asi_to_Link_c0_ast_Data                      ,
	input  [2:0]   w_dp_Link_c0_asi_to_Link_c0_ast_RdCnt                     ,
	input  [2:0]   w_dp_Link_c0_asi_to_Link_c0_ast_RdPtr                     ,
	output         w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst            ,
	input          w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck         ,
	input          w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst            ,
	output         w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck         ,
	output [2:0]   w_dp_Link_c0_asi_to_Link_c0_ast_WrCnt                     ,
	input  [125:0] w_dp_Link_c0_astResp_to_Link_c0_asiResp_Data              ,
	output [2:0]   w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt             ,
	output [2:0]   w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr             ,
	input          w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst    ,
	output         w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck ,
	output         w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst    ,
	input          w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck ,
	input  [2:0]   w_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt             






);


	cbus_Structure_Module_pcie u_cbus_Structure_Module_pcie(
		.TM( TM )
	,	.arstn_pcie( arstn_pcie )
	,	.clk_pcie( clk_pcie )
	,	.dp_Link_c0_asi_to_Link_c0_ast_Data( ctrl_dp_Link_c0_asi_to_Link_c0_ast_Data )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdCnt( ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdCnt )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdPtr( ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdPtr )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst( ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst( ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_WrCnt( ctrl_dp_Link_c0_asi_to_Link_c0_ast_WrCnt )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data( ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt( ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr( ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt( ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt )
	,	.pcie_ctrl_Ar_Addr( pcie_ctrl_Ar_Addr )
	,	.pcie_ctrl_Ar_Burst( pcie_ctrl_Ar_Burst )
	,	.pcie_ctrl_Ar_Cache( pcie_ctrl_Ar_Cache )
	,	.pcie_ctrl_Ar_Id( pcie_ctrl_Ar_Id )
	,	.pcie_ctrl_Ar_Len( pcie_ctrl_Ar_Len )
	,	.pcie_ctrl_Ar_Lock( pcie_ctrl_Ar_Lock )
	,	.pcie_ctrl_Ar_Prot( pcie_ctrl_Ar_Prot )
	,	.pcie_ctrl_Ar_Ready( pcie_ctrl_Ar_Ready )
	,	.pcie_ctrl_Ar_Size( pcie_ctrl_Ar_Size )
	,	.pcie_ctrl_Ar_User( pcie_ctrl_Ar_User )
	,	.pcie_ctrl_Ar_Valid( pcie_ctrl_Ar_Valid )
	,	.pcie_ctrl_Aw_Addr( pcie_ctrl_Aw_Addr )
	,	.pcie_ctrl_Aw_Burst( pcie_ctrl_Aw_Burst )
	,	.pcie_ctrl_Aw_Cache( pcie_ctrl_Aw_Cache )
	,	.pcie_ctrl_Aw_Id( pcie_ctrl_Aw_Id )
	,	.pcie_ctrl_Aw_Len( pcie_ctrl_Aw_Len )
	,	.pcie_ctrl_Aw_Lock( pcie_ctrl_Aw_Lock )
	,	.pcie_ctrl_Aw_Prot( pcie_ctrl_Aw_Prot )
	,	.pcie_ctrl_Aw_Ready( pcie_ctrl_Aw_Ready )
	,	.pcie_ctrl_Aw_Size( pcie_ctrl_Aw_Size )
	,	.pcie_ctrl_Aw_User( pcie_ctrl_Aw_User )
	,	.pcie_ctrl_Aw_Valid( pcie_ctrl_Aw_Valid )
	,	.pcie_ctrl_B_Id( pcie_ctrl_B_Id )
	,	.pcie_ctrl_B_Ready( pcie_ctrl_B_Ready )
	,	.pcie_ctrl_B_Resp( pcie_ctrl_B_Resp )
	,	.pcie_ctrl_B_Valid( pcie_ctrl_B_Valid )
	,	.pcie_ctrl_R_Data( pcie_ctrl_R_Data )
	,	.pcie_ctrl_R_Id( pcie_ctrl_R_Id )
	,	.pcie_ctrl_R_Last( pcie_ctrl_R_Last )
	,	.pcie_ctrl_R_Ready( pcie_ctrl_R_Ready )
	,	.pcie_ctrl_R_Resp( pcie_ctrl_R_Resp )
	,	.pcie_ctrl_R_Valid( pcie_ctrl_R_Valid )
	,	.pcie_ctrl_W_Data( pcie_ctrl_W_Data )
	,	.pcie_ctrl_W_Last( pcie_ctrl_W_Last )
	,	.pcie_ctrl_W_Ready( pcie_ctrl_W_Ready )
	,	.pcie_ctrl_W_Strb( pcie_ctrl_W_Strb )
	,	.pcie_ctrl_W_Valid( pcie_ctrl_W_Valid )
	);

	dbus_read_Structure_Module_pcie u_dbus_read_Structure_Module_pcie(
		.TM( TM )
	,	.arstn_pcie( arstn_pcie )
	,	.clk_pcie( clk_pcie )
	,	.dp_Link_c0_asi_to_Link_c0_ast_Data( r_dp_Link_c0_asi_to_Link_c0_ast_Data )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdCnt( r_dp_Link_c0_asi_to_Link_c0_ast_RdCnt )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdPtr( r_dp_Link_c0_asi_to_Link_c0_ast_RdPtr )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst( r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck( r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst( r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck( r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_WrCnt( r_dp_Link_c0_asi_to_Link_c0_ast_WrCnt )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_Data( r_dp_Link_c0_astResp_to_Link_c0_asiResp_Data )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt( r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr( r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst( r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck( r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst( r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck( r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt( r_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt )
	,	.pcie_r_Ar_Addr( pcie_r_Ar_Addr )
	,	.pcie_r_Ar_Burst( pcie_r_Ar_Burst )
	,	.pcie_r_Ar_Cache( pcie_r_Ar_Cache )
	,	.pcie_r_Ar_Id( pcie_r_Ar_Id )
	,	.pcie_r_Ar_Len( pcie_r_Ar_Len )
	,	.pcie_r_Ar_Lock( pcie_r_Ar_Lock )
	,	.pcie_r_Ar_Prot( pcie_r_Ar_Prot )
	,	.pcie_r_Ar_Ready( pcie_r_Ar_Ready )
	,	.pcie_r_Ar_Size( pcie_r_Ar_Size )
	,	.pcie_r_Ar_User( pcie_r_Ar_User )
	,	.pcie_r_Ar_Valid( pcie_r_Ar_Valid )
	,	.pcie_r_R_Data( pcie_r_R_Data )
	,	.pcie_r_R_Id( pcie_r_R_Id )
	,	.pcie_r_R_Last( pcie_r_R_Last )
	,	.pcie_r_R_Ready( pcie_r_R_Ready )
	,	.pcie_r_R_Resp( pcie_r_R_Resp )
	,	.pcie_r_R_User( pcie_r_R_User )
	,	.pcie_r_R_Valid( pcie_r_R_Valid )
	);

	dbus_write_Structure_Module_pcie u_dbus_write_Structure_Module_pcie(
		.TM( TM )
	,	.arstn_pcie( arstn_pcie )
	,	.clk_pcie( clk_pcie )
	,	.dp_Link_c0_asi_to_Link_c0_ast_Data( w_dp_Link_c0_asi_to_Link_c0_ast_Data )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdCnt( w_dp_Link_c0_asi_to_Link_c0_ast_RdCnt )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdPtr( w_dp_Link_c0_asi_to_Link_c0_ast_RdPtr )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst( w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck( w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst( w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck( w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_WrCnt( w_dp_Link_c0_asi_to_Link_c0_ast_WrCnt )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_Data( w_dp_Link_c0_astResp_to_Link_c0_asiResp_Data )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt( w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr( w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst( w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst( w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt( w_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt )
	,	.pcie_w_Aw_Addr( pcie_w_Aw_Addr )
	,	.pcie_w_Aw_Burst( pcie_w_Aw_Burst )
	,	.pcie_w_Aw_Cache( pcie_w_Aw_Cache )
	,	.pcie_w_Aw_Id( pcie_w_Aw_Id )
	,	.pcie_w_Aw_Len( pcie_w_Aw_Len )
	,	.pcie_w_Aw_Lock( pcie_w_Aw_Lock )
	,	.pcie_w_Aw_Prot( pcie_w_Aw_Prot )
	,	.pcie_w_Aw_Ready( pcie_w_Aw_Ready )
	,	.pcie_w_Aw_Size( pcie_w_Aw_Size )
	,	.pcie_w_Aw_User( pcie_w_Aw_User )
	,	.pcie_w_Aw_Valid( pcie_w_Aw_Valid )
	,	.pcie_w_B_Id( pcie_w_B_Id )
	,	.pcie_w_B_Ready( pcie_w_B_Ready )
	,	.pcie_w_B_Resp( pcie_w_B_Resp )
	,	.pcie_w_B_User( pcie_w_B_User )
	,	.pcie_w_B_Valid( pcie_w_B_Valid )
	,	.pcie_w_W_Data( pcie_w_W_Data )
	,	.pcie_w_W_Last( pcie_w_W_Last )
	,	.pcie_w_W_Ready( pcie_w_W_Ready )
	,	.pcie_w_W_Strb( pcie_w_W_Strb )
	,	.pcie_w_W_Valid( pcie_w_W_Valid )
	);


endmodule
