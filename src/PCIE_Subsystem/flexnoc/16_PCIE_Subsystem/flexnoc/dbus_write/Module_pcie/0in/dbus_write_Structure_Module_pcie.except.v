
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:23:03

////////////////////////////////////////////////////////////////////

module dbus_write_Structure_Module_pcie_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_write_Structure_Module_pcie -severity waived -scheme reconvergence -from_signals Link_c0_asiResp_main.DtpRxClkAdapt_Link_c0_astResp_Async.RdCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_pcie -severity waived -scheme reconvergence -from_signals Link_c0_asi_main.DtpTxClkAdapt_Link_c0_ast_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_write_Structure_Module_pcie -severity waived -scheme multi_sync_mux_select -from Link_c0_asiResp_main.DtpRxClkAdapt_Link_c0_astResp_Async.uRegSync.instSynchronizerCell*

endmodule
