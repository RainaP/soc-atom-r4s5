# Validated with 0in (TM) version Version 10.4c_6 linux_x86_64 21 Dec 2015
# vlib work; vmap -c work work; vlog -skipsynthoffregion *.v
# qverify -c -do 0in/do.tcl
#

cdc preference -vectorize_nl

# turn on fifo synchronization recognition
#
cdc preference -fifo_scheme
#cdc preference fifo -sync_effort high
#cdc preference fifo -effort high

# turn on reconvergence
#
cdc reconvergence on
cdc preference reconvergence -depth 0 -divergence_depth 0

do 0in/dbus_write_Structure_Module_pcie.clkgrp.tcl
do 0in/dbus_write_Structure_Module_pcie.rst.tcl
do 0in/dbus_write_Structure_Module_pcie.io.tcl

do 0in/dbus_write_Structure_Module_pcie.ctrl.tcl
do 0in/dbus_write_Structure_Module_pcie.except.tcl

onerror continue

 cdc run -d dbus_write_Structure_Module_pcie -auto_black_box -hcdc -process_dead_end -cr dbus_write_Structure_Module_pcie.rpt
 cdc generate report cdc_detail.rpt

exit

