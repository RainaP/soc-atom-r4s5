
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:23:03

####################################################################

arteris_clock -name "clk_pcie" -clock_domain "clk_pcie" -port "clk_pcie" -period "${clk_pcie_P}" -waveform "[expr ${clk_pcie_P}*0.00] [expr ${clk_pcie_P}*0.50]" -edge "R" -user_directive ""
