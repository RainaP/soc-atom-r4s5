
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:54

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_c0_astResp001_main.DtpTxClkAdapt_Link_c0_asiResp001_Async.WrCnt -graycode -module cbus_Structure_Module_pcie
cdc signal Link_c0_ast_main.DtpRxClkAdapt_Link_c0_asi_Async.RdCnt -graycode -module cbus_Structure_Module_pcie

