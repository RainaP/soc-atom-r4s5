
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:54

####################################################################

netlist clock Regime_pcie_Cm_main.root_Clk -module cbus_Structure_Module_pcie -group clk_pcie -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_pcie_Cm_main.root_Clk_ClkS -module cbus_Structure_Module_pcie -group clk_pcie -period 1.000 -waveform {0.000 0.500}
netlist clock clk_pcie -module cbus_Structure_Module_pcie -group clk_pcie -period 1.000 -waveform {0.000 0.500}

