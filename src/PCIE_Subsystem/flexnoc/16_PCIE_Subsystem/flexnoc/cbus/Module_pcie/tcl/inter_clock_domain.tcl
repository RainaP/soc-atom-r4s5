
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:54

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy

# SYNC_LINES



# ASYNC_LINES


# PSEUDO_STATIC_LINES


# COMMENTED_LINES
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c0_astResp001_main/DtpTxClkAdapt_Link_c0_asiResp001_Async/RegData_0" -from_lsb "0" -from_msb "57" -from_clk "Regime_pcie_Cm_root" -from_clk_P "${Regime_pcie_Cm_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c0_astResp001_main/DtpTxClkAdapt_Link_c0_asiResp001_Async/RegData_1" -from_lsb "0" -from_msb "57" -from_clk "Regime_pcie_Cm_root" -from_clk_P "${Regime_pcie_Cm_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c0_astResp001_main/DtpTxClkAdapt_Link_c0_asiResp001_Async/RegData_2" -from_lsb "0" -from_msb "57" -from_clk "Regime_pcie_Cm_root" -from_clk_P "${Regime_pcie_Cm_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c0_astResp001_main/DtpTxClkAdapt_Link_c0_asiResp001_Async/RegData_3" -from_lsb "0" -from_msb "57" -from_clk "Regime_pcie_Cm_root" -from_clk_P "${Regime_pcie_Cm_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c0_astResp001_main/DtpTxClkAdapt_Link_c0_asiResp001_Async/RegData_4" -from_lsb "0" -from_msb "57" -from_clk "Regime_pcie_Cm_root" -from_clk_P "${Regime_pcie_Cm_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_c0_astResp001_main/DtpTxClkAdapt_Link_c0_asiResp001_Async/RegData_5" -from_lsb "0" -from_msb "57" -from_clk "Regime_pcie_Cm_root" -from_clk_P "${Regime_pcie_Cm_root_P}" -to_clk "Regime_cbus_south_Cm_root" -to_clk_P "${Regime_cbus_south_Cm_root_P}" -delay "[expr 2*${Regime_cbus_south_Cm_root_P}]"
