
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:54

####################################################################

arteris_gen_clock -name "Regime_pcie_Cm_root" -pin "Regime_pcie_Cm_main/root_Clk Regime_pcie_Cm_main/root_Clk_ClkS" -clock_domain "clk_pcie" -spec_domain_clock "/Regime_pcie/Cm/root" -divide_by "1" -source "clk_pcie" -source_period "${clk_pcie_P}" -source_waveform "[expr ${clk_pcie_P}*0.00] [expr ${clk_pcie_P}*0.50]" -user_directive "" -add "FALSE"
