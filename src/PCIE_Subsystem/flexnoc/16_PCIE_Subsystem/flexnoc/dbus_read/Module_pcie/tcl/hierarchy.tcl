
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:09

####################################################################

arteris_hierarchy -module "dbus_read_Structure_Module_pcie_Link_c0_asiResp_main" -generator "DatapathLink" -instance_name "Link_c0_asiResp_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_Link_c0_asi_main" -generator "DatapathLink" -instance_name "Link_c0_asi_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_Regime_pcie_Cm_main" -generator "ClockManager" -instance_name "Regime_pcie_Cm_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_clockGaters_Link_c0_asiResp_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c0_asiResp_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_clockGaters_Link_c0_asi_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c0_asi_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_clockGaters_pcie_r" -generator "ClockGater" -instance_name "clockGaters_pcie_r" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_clockGaters_pcie_r_I" -generator "ClockGater" -instance_name "clockGaters_pcie_r_I" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_pcie_r_I_main" -generator "G2T" -instance_name "pcie_r_I_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_pcie_r_main" -generator "S2G" -instance_name "pcie_r_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_z_T_C_S_C_L_R_A_4_1" -generator "gate" -instance_name "uAnd4" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie_z_T_C_S_C_L_R_O_4_1" -generator "gate" -instance_name "uOr4" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_pcie" -generator "" -instance_name "dbus_read_Structure_Module_pcie" -level "1"
