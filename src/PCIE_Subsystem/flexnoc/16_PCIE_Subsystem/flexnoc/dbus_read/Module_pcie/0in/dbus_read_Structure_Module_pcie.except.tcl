
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:09

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_c0_asiResp_main.DtpRxClkAdapt_Link_c0_astResp_Async.RdCnt -graycode -module dbus_read_Structure_Module_pcie
cdc signal Link_c0_asi_main.DtpTxClkAdapt_Link_c0_ast_Async.WrCnt -graycode -module dbus_read_Structure_Module_pcie

