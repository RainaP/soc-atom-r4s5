
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:09

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_pcie_clkgrp;

// 0in set_cdc_clock Regime_pcie_Cm_main.root_Clk -module dbus_read_Structure_Module_pcie -group clk_pcie -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock Regime_pcie_Cm_main.root_Clk_ClkS -module dbus_read_Structure_Module_pcie -group clk_pcie -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock clk_pcie -module dbus_read_Structure_Module_pcie -group clk_pcie -period 1.000 -waveform {0.000 0.500}

endmodule
