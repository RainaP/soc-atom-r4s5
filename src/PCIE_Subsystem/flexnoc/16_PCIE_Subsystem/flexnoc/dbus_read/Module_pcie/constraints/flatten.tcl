if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0_asiResp_main/DtpRxClkAdapt_Link_c0_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0_asiResp_main/DtpRxClkAdapt_Link_c0_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_pcie] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_pcie false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_pcie_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_pcie_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_pcie_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_pcie_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_pcie_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_pcie_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_pcie_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_pcie_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_pcie_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_pcie_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_pcie_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_pcie_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/DtpTxSerAdapt_Link_c0_asi] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/DtpTxSerAdapt_Link_c0_asi false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie_r_main/SpecificToGeneric false
}
