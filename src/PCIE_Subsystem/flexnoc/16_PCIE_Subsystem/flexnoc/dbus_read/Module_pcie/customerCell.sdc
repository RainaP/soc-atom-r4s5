
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:14:19

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_customer_cell -module "ClockManager_Regime_pcie_Cm" -type "ClockManagerCell" -instance_name "${CUSTOMER_HIERARCHY}Regime_pcie_Cm_main/ClockManager/IClockManager_Regime_pcie_Cm"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp_main/DtpRxClkAdapt_Link_c0_astResp_Async/urs/Isc0"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp_main/DtpRxClkAdapt_Link_c0_astResp_Async/urs/Isc1"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp_main/DtpRxClkAdapt_Link_c0_astResp_Async/urs/Isc2"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp_main/DtpRxClkAdapt_Link_c0_astResp_Async/urs0/Isc0"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp_main/DtpRxClkAdapt_Link_c0_astResp_Async/urs1/Isc0"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs/Isc0"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs/Isc1"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs/Isc2"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs0/Isc0"
arteris_customer_cell -module "VD_pcie_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs1/Isc0"
arteris_customer_cell -module "VD_pcie_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp_main_Sys/ClockGater/uscef16a3fa9/instGaterCell" -clock "Regime_pcie_Cm_root" -clock_period "[ expr 1.000 * $T_Flex2library ]"
arteris_customer_cell -module "VD_pcie_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys/ClockGater/uscef16a3fa9/instGaterCell" -clock "Regime_pcie_Cm_root" -clock_period "[ expr 1.000 * $T_Flex2library ]"
arteris_customer_cell -module "VD_pcie_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_pcie_r/ClockGater/uscef16a3fa9/instGaterCell" -clock "Regime_pcie_Cm_root" -clock_period "[ expr 1.000 * $T_Flex2library ]"
arteris_customer_cell -module "VD_pcie_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_pcie_r_I/ClockGater/uscef16a3fa9/instGaterCell" -clock "Regime_pcie_Cm_root" -clock_period "[ expr 1.000 * $T_Flex2library ]"

