//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module PCIE_Subsystem (
	 input          TEST_MODE
	,input          CLK__PCIE
	,input          RSTN__PCIE
	,output         SouthBUS__IRQ__cfg_vpd_int
	,output         SouthBUS__IRQ__cfg_link_eq_req_int
	,output         SouthBUS__IRQ__usp_eq_redo_executed_int
	,output [ 15:0] SouthBUS__IRQ__edma_int
	,output         SouthBUS__IRQ__assert_inta_grt
	,output         SouthBUS__IRQ__assert_intb_grt
	,output         SouthBUS__IRQ__assert_intc_grt
	,output         SouthBUS__IRQ__assert_intd_grt
	,output         SouthBUS__IRQ__deassert_inta_grt
	,output         SouthBUS__IRQ__deassert_intb_grt
	,output         SouthBUS__IRQ__deassert_intc_grt
	,output         SouthBUS__IRQ__deassert_intd_grt
	,output         SouthBUS__IRQ__cfg_safety_corr
	,output         SouthBUS__IRQ__cfg_safety_uncorr
	,input  [ 57:0] SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_Data
	,output [  2:0] SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdCnt
	,output [  2:0] SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdPtr
	,input          SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst
	,output         SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck
	,output         SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst
	,input          SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck
	,input  [  2:0] SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_WrCnt
	,output [ 57:0] SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data
	,input  [  2:0] SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt
	,input  [  2:0] SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr
	,output         SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst
	,input          SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck
	,input          SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst
	,output         SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck
	,output [  2:0] SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt
	,output [128:0] MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_Data
	,input  [  2:0] MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RdCnt
	,input  [  2:0] MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RdPtr
	,output         MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst
	,input          MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck
	,input          MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst
	,output         MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck
	,output [  2:0] MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_WrCnt
	,input  [705:0] MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_Data
	,output [  2:0] MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt
	,output [  2:0] MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr
	,input          MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst
	,output         MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck
	,output         MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst
	,input          MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck
	,input  [  2:0] MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt
	,output [702:0] MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_Data
	,input  [  2:0] MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RdCnt
	,input  [  2:0] MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RdPtr
	,output         MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst
	,input          MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck
	,input          MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst
	,output         MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck
	,output [  2:0] MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_WrCnt
	,input  [125:0] MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_Data
	,output [  2:0] MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt
	,output [  2:0] MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr
	,input          MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst
	,output         MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck
	,output         MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst
	,input          MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck
	,input  [  2:0] MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_PCIE_Subsystem
	assign SouthBUS__IRQ__cfg_vpd_int = 1'h0;
	assign SouthBUS__IRQ__cfg_link_eq_req_int = 1'h0;
	assign SouthBUS__IRQ__usp_eq_redo_executed_int = 1'h0;
	assign SouthBUS__IRQ__edma_int = 16'h0;
	assign SouthBUS__IRQ__assert_inta_grt = 1'h0;
	assign SouthBUS__IRQ__assert_intb_grt = 1'h0;
	assign SouthBUS__IRQ__assert_intc_grt = 1'h0;
	assign SouthBUS__IRQ__assert_intd_grt = 1'h0;
	assign SouthBUS__IRQ__deassert_inta_grt = 1'h0;
	assign SouthBUS__IRQ__deassert_intb_grt = 1'h0;
	assign SouthBUS__IRQ__deassert_intc_grt = 1'h0;
	assign SouthBUS__IRQ__deassert_intd_grt = 1'h0;
	assign SouthBUS__IRQ__cfg_safety_corr = 1'h0;
	assign SouthBUS__IRQ__cfg_safety_uncorr = 1'h0;
	assign SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdCnt = 3'h0;
	assign SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdPtr = 3'h0;
	assign SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck = 1'h0;
	assign SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst = 1'h0;
	assign SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data = 58'h0;
	assign SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst = 1'h0;
	assign SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt = 3'h0;
	assign MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_Data = 129'h0;
	assign MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_WrCnt = 3'h0;
	assign MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt = 3'h0;
	assign MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr = 3'h0;
	assign MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_Data = 703'h0;
	assign MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_WrCnt = 3'h0;
	assign MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt = 3'h0;
	assign MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr = 3'h0;
	assign MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst = 1'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_PCIE_Subsystem
	wire [23:0] AXI2APB_0__PCIE_PBUS_0__apb_mst2_PAddr;
	wire        AXI2APB_0__PCIE_PBUS_0__apb_mst2_PEnable;
	wire [ 2:0] AXI2APB_0__PCIE_PBUS_0__apb_mst2_PProt;
	wire [31:0] AXI2APB_0__PCIE_PBUS_0__apb_mst2_PRData;
	wire        AXI2APB_0__PCIE_PBUS_0__apb_mst2_PReady;
	wire        AXI2APB_0__PCIE_PBUS_0__apb_mst2_PSel;
	wire        AXI2APB_0__PCIE_PBUS_0__apb_mst2_PSlvErr;
	wire [ 3:0] AXI2APB_0__PCIE_PBUS_0__apb_mst2_PStrb;
	wire [31:0] AXI2APB_0__PCIE_PBUS_0__apb_mst2_PWData;
	wire        AXI2APB_0__PCIE_PBUS_0__apb_mst2_PWrite;
	wire        SCU_0__PCIE_PBUS_0__APB__PENABLE;
	wire        SCU_0__PCIE_PBUS_0__APB__PSEL;
	wire        SCU_0__PCIE_PBUS_0__APB__PREADY;
	wire        SCU_0__PCIE_PBUS_0__APB__PSLVERR;
	wire        SCU_0__PCIE_PBUS_0__APB__PWRITE;
	wire [ 2:0] SCU_0__PCIE_PBUS_0__APB__PPROT;
	wire [11:0] SCU_0__PCIE_PBUS_0__APB__PADDR;
	wire [ 3:0] SCU_0__PCIE_PBUS_0__APB__PSTRB;
	wire [31:0] SCU_0__PCIE_PBUS_0__APB__PWDATA;
	wire [31:0] SCU_0__PCIE_PBUS_0__APB__PRDATA;
	wire        PCIECON_0__PCIE_PBUS_0__APB__PENABLE;
	wire        PCIECON_0__PCIE_PBUS_0__APB__PSEL;
	wire        PCIECON_0__PCIE_PBUS_0__APB__PREADY;
	wire        PCIECON_0__PCIE_PBUS_0__APB__PSLVERR;
	wire        PCIECON_0__PCIE_PBUS_0__APB__PWRITE;
	wire [ 2:0] PCIECON_0__PCIE_PBUS_0__APB__PPROT;
	wire [11:0] PCIECON_0__PCIE_PBUS_0__APB__PADDR;
	wire [ 3:0] PCIECON_0__PCIE_PBUS_0__APB__PSTRB;
	wire [31:0] PCIECON_0__PCIE_PBUS_0__APB__PWDATA;
	wire [31:0] PCIECON_0__PCIE_PBUS_0__APB__PRDATA;
	wire        PCIEEPCSR_0__PCIE_PBUS_0__APB__PENABLE;
	wire        PCIEEPCSR_0__PCIE_PBUS_0__APB__PSEL;
	wire        PCIEEPCSR_0__PCIE_PBUS_0__APB__PREADY;
	wire        PCIEEPCSR_0__PCIE_PBUS_0__APB__PSLVERR;
	wire        PCIEEPCSR_0__PCIE_PBUS_0__APB__PWRITE;
	wire [ 2:0] PCIEEPCSR_0__PCIE_PBUS_0__APB__PPROT;
	wire [11:0] PCIEEPCSR_0__PCIE_PBUS_0__APB__PADDR;
	wire [ 3:0] PCIEEPCSR_0__PCIE_PBUS_0__APB__PSTRB;
	wire [31:0] PCIEEPCSR_0__PCIE_PBUS_0__APB__PWDATA;
	wire [31:0] PCIEEPCSR_0__PCIE_PBUS_0__APB__PRDATA;
	wire        PCIEUPCS_0__PCIE_PBUS_0__APB__PENABLE;
	wire        PCIEUPCS_0__PCIE_PBUS_0__APB__PSEL;
	wire        PCIEUPCS_0__PCIE_PBUS_0__APB__PREADY;
	wire        PCIEUPCS_0__PCIE_PBUS_0__APB__PSLVERR;
	wire        PCIEUPCS_0__PCIE_PBUS_0__APB__PWRITE;
	wire [ 2:0] PCIEUPCS_0__PCIE_PBUS_0__APB__PPROT;
	wire [11:0] PCIEUPCS_0__PCIE_PBUS_0__APB__PADDR;
	wire [ 3:0] PCIEUPCS_0__PCIE_PBUS_0__APB__PSTRB;
	wire [31:0] PCIEUPCS_0__PCIE_PBUS_0__APB__PWDATA;
	wire [31:0] PCIEUPCS_0__PCIE_PBUS_0__APB__PRDATA;
	wire [ 1:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Burst;
	wire [ 3:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Cache;
	wire [ 7:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Id;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Lock;
	wire [ 2:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Prot;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Ready;
	wire [ 2:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Size;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Valid;
	wire [ 1:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Burst;
	wire [ 3:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Cache;
	wire [ 7:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Id;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Lock;
	wire [ 2:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Prot;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Ready;
	wire [ 2:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Size;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Valid;
	wire [ 7:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Id;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Ready;
	wire [ 1:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Resp;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Valid;
	wire [31:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Data;
	wire [ 7:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Id;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Last;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Ready;
	wire [ 1:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Resp;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Valid;
	wire [31:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Data;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Last;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Ready;
	wire [ 3:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Strb;
	wire        cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Valid;
	wire [23:0] cbus_Structure_Module_pcie__AXI2APB_0__axi_slv0_Ar_Addr;
	wire [23:0] cbus_Structure_Module_pcie__AXI2APB_0__axi_slv0_Aw_Addr;
	wire [ 1:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Len;
	wire [ 1:0] cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Len;

	assign SouthBUS__IRQ__cfg_vpd_int = 1'h0;
	assign SouthBUS__IRQ__cfg_link_eq_req_int = 1'h0;
	assign SouthBUS__IRQ__usp_eq_redo_executed_int = 1'h0;
	assign SouthBUS__IRQ__edma_int = 16'h0;
	assign SouthBUS__IRQ__assert_inta_grt = 1'h0;
	assign SouthBUS__IRQ__assert_intb_grt = 1'h0;
	assign SouthBUS__IRQ__assert_intc_grt = 1'h0;
	assign SouthBUS__IRQ__assert_intd_grt = 1'h0;
	assign SouthBUS__IRQ__deassert_inta_grt = 1'h0;
	assign SouthBUS__IRQ__deassert_intb_grt = 1'h0;
	assign SouthBUS__IRQ__deassert_intc_grt = 1'h0;
	assign SouthBUS__IRQ__deassert_intd_grt = 1'h0;
	assign SouthBUS__IRQ__cfg_safety_corr = 1'h0;
	assign SouthBUS__IRQ__cfg_safety_uncorr = 1'h0;

	PCIE_PBUS #(
		 .ABITS(24)
	) PCIE_PBUS_0 (
		 .CLK__APB                 (CLK__PCIE)
		,.RSTN__APB                (RSTN__PCIE)
		,.APB__PSEL                (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PSel)
		,.APB__PENABLE             (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PEnable)
		,.APB__PWRITE              (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PWrite)
		,.APB__PPROT               (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PProt)
		,.APB__PADDR               (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PAddr)
		,.APB__PSTRB               (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PStrb)
		,.APB__PWDATA              (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PWData)
		,.APB__PREADY              (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PReady)
		,.APB__PSLVERR             (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PSlvErr)
		,.APB__PRDATA              (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PRData)
		,.SCU_0__APB__PSEL         (SCU_0__PCIE_PBUS_0__APB__PSEL)
		,.SCU_0__APB__PENABLE      (SCU_0__PCIE_PBUS_0__APB__PENABLE)
		,.SCU_0__APB__PWRITE       (SCU_0__PCIE_PBUS_0__APB__PWRITE)
		,.SCU_0__APB__PPROT        (SCU_0__PCIE_PBUS_0__APB__PPROT)
		,.SCU_0__APB__PADDR        (SCU_0__PCIE_PBUS_0__APB__PADDR)
		,.SCU_0__APB__PSTRB        (SCU_0__PCIE_PBUS_0__APB__PSTRB)
		,.SCU_0__APB__PWDATA       (SCU_0__PCIE_PBUS_0__APB__PWDATA)
		,.SCU_0__APB__PREADY       (SCU_0__PCIE_PBUS_0__APB__PREADY)
		,.SCU_0__APB__PSLVERR      (SCU_0__PCIE_PBUS_0__APB__PSLVERR)
		,.SCU_0__APB__PRDATA       (SCU_0__PCIE_PBUS_0__APB__PRDATA)
		,.PCIECON_0__APB__PSEL     (PCIECON_0__PCIE_PBUS_0__APB__PSEL)
		,.PCIECON_0__APB__PENABLE  (PCIECON_0__PCIE_PBUS_0__APB__PENABLE)
		,.PCIECON_0__APB__PWRITE   (PCIECON_0__PCIE_PBUS_0__APB__PWRITE)
		,.PCIECON_0__APB__PPROT    (PCIECON_0__PCIE_PBUS_0__APB__PPROT)
		,.PCIECON_0__APB__PADDR    (PCIECON_0__PCIE_PBUS_0__APB__PADDR)
		,.PCIECON_0__APB__PSTRB    (PCIECON_0__PCIE_PBUS_0__APB__PSTRB)
		,.PCIECON_0__APB__PWDATA   (PCIECON_0__PCIE_PBUS_0__APB__PWDATA)
		,.PCIECON_0__APB__PREADY   (PCIECON_0__PCIE_PBUS_0__APB__PREADY)
		,.PCIECON_0__APB__PSLVERR  (PCIECON_0__PCIE_PBUS_0__APB__PSLVERR)
		,.PCIECON_0__APB__PRDATA   (PCIECON_0__PCIE_PBUS_0__APB__PRDATA)
		,.PCIEEPCSR_0__APB__PSEL   (PCIEEPCSR_0__PCIE_PBUS_0__APB__PSEL)
		,.PCIEEPCSR_0__APB__PENABLE(PCIEEPCSR_0__PCIE_PBUS_0__APB__PENABLE)
		,.PCIEEPCSR_0__APB__PWRITE (PCIEEPCSR_0__PCIE_PBUS_0__APB__PWRITE)
		,.PCIEEPCSR_0__APB__PPROT  (PCIEEPCSR_0__PCIE_PBUS_0__APB__PPROT)
		,.PCIEEPCSR_0__APB__PADDR  (PCIEEPCSR_0__PCIE_PBUS_0__APB__PADDR)
		,.PCIEEPCSR_0__APB__PSTRB  (PCIEEPCSR_0__PCIE_PBUS_0__APB__PSTRB)
		,.PCIEEPCSR_0__APB__PWDATA (PCIEEPCSR_0__PCIE_PBUS_0__APB__PWDATA)
		,.PCIEEPCSR_0__APB__PREADY (PCIEEPCSR_0__PCIE_PBUS_0__APB__PREADY)
		,.PCIEEPCSR_0__APB__PSLVERR(PCIEEPCSR_0__PCIE_PBUS_0__APB__PSLVERR)
		,.PCIEEPCSR_0__APB__PRDATA (PCIEEPCSR_0__PCIE_PBUS_0__APB__PRDATA)
		,.PCIEUPCS_0__APB__PSEL    (PCIEUPCS_0__PCIE_PBUS_0__APB__PSEL)
		,.PCIEUPCS_0__APB__PENABLE (PCIEUPCS_0__PCIE_PBUS_0__APB__PENABLE)
		,.PCIEUPCS_0__APB__PWRITE  (PCIEUPCS_0__PCIE_PBUS_0__APB__PWRITE)
		,.PCIEUPCS_0__APB__PPROT   (PCIEUPCS_0__PCIE_PBUS_0__APB__PPROT)
		,.PCIEUPCS_0__APB__PADDR   (PCIEUPCS_0__PCIE_PBUS_0__APB__PADDR)
		,.PCIEUPCS_0__APB__PSTRB   (PCIEUPCS_0__PCIE_PBUS_0__APB__PSTRB)
		,.PCIEUPCS_0__APB__PWDATA  (PCIEUPCS_0__PCIE_PBUS_0__APB__PWDATA)
		,.PCIEUPCS_0__APB__PREADY  (PCIEUPCS_0__PCIE_PBUS_0__APB__PREADY)
		,.PCIEUPCS_0__APB__PSLVERR (PCIEUPCS_0__PCIE_PBUS_0__APB__PSLVERR)
		,.PCIEUPCS_0__APB__PRDATA  (PCIEUPCS_0__PCIE_PBUS_0__APB__PRDATA)
	);

	axi2apb_bridge
	AXI2APB_0 (
		 .TM               (TEST_MODE)
		,.apb_mst0_PAddr   ()
		,.apb_mst0_PEnable ()
		,.apb_mst0_PProt   ()
		,.apb_mst0_PRData  (32'h0)
		,.apb_mst0_PReady  (1'h0)
		,.apb_mst0_PSel    ()
		,.apb_mst0_PSlvErr (1'h0)
		,.apb_mst0_PStrb   ()
		,.apb_mst0_PWData  ()
		,.apb_mst0_PWrite  ()
		,.apb_mst1_PAddr   ()
		,.apb_mst1_PEnable ()
		,.apb_mst1_PProt   ()
		,.apb_mst1_PRData  (32'h0)
		,.apb_mst1_PReady  (1'h0)
		,.apb_mst1_PSel    ()
		,.apb_mst1_PSlvErr (1'h0)
		,.apb_mst1_PStrb   ()
		,.apb_mst1_PWData  ()
		,.apb_mst1_PWrite  ()
		,.apb_mst2_PAddr   (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PAddr)
		,.apb_mst2_PEnable (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PEnable)
		,.apb_mst2_PProt   (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PProt)
		,.apb_mst2_PRData  (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PRData)
		,.apb_mst2_PReady  (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PReady)
		,.apb_mst2_PSel    (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PSel)
		,.apb_mst2_PSlvErr (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PSlvErr)
		,.apb_mst2_PStrb   (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PStrb)
		,.apb_mst2_PWData  (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PWData)
		,.apb_mst2_PWrite  (AXI2APB_0__PCIE_PBUS_0__apb_mst2_PWrite)
		,.apb_mst3_PAddr   ()
		,.apb_mst3_PEnable ()
		,.apb_mst3_PProt   ()
		,.apb_mst3_PRData  (32'h0)
		,.apb_mst3_PReady  (1'h0)
		,.apb_mst3_PSel    ()
		,.apb_mst3_PSlvErr (1'h0)
		,.apb_mst3_PStrb   ()
		,.apb_mst3_PWData  ()
		,.apb_mst3_PWrite  ()
		,.apb_mst4_PAddr   ()
		,.apb_mst4_PEnable ()
		,.apb_mst4_PProt   ()
		,.apb_mst4_PRData  (32'h0)
		,.apb_mst4_PReady  (1'h0)
		,.apb_mst4_PSel    ()
		,.apb_mst4_PSlvErr (1'h0)
		,.apb_mst4_PStrb   ()
		,.apb_mst4_PWData  ()
		,.apb_mst4_PWrite  ()
		,.apb_mst5_PAddr   ()
		,.apb_mst5_PEnable ()
		,.apb_mst5_PProt   ()
		,.apb_mst5_PRData  (32'h0)
		,.apb_mst5_PReady  (1'h0)
		,.apb_mst5_PSel    ()
		,.apb_mst5_PSlvErr (1'h0)
		,.apb_mst5_PStrb   ()
		,.apb_mst5_PWData  ()
		,.apb_mst5_PWrite  ()
		,.apb_mst6_PAddr   ()
		,.apb_mst6_PEnable ()
		,.apb_mst6_PProt   ()
		,.apb_mst6_PRData  (32'h0)
		,.apb_mst6_PReady  (1'h0)
		,.apb_mst6_PSel    ()
		,.apb_mst6_PSlvErr (1'h0)
		,.apb_mst6_PStrb   ()
		,.apb_mst6_PWData  ()
		,.apb_mst6_PWrite  ()
		,.apb_mst7_PAddr   ()
		,.apb_mst7_PEnable ()
		,.apb_mst7_PProt   ()
		,.apb_mst7_PRData  (32'h0)
		,.apb_mst7_PReady  (1'h0)
		,.apb_mst7_PSel    ()
		,.apb_mst7_PSlvErr (1'h0)
		,.apb_mst7_PStrb   ()
		,.apb_mst7_PWData  ()
		,.apb_mst7_PWrite  ()
		,.arstn            (RSTN__PCIE)
		,.axi_slv0_Ar_Addr (cbus_Structure_Module_pcie__AXI2APB_0__axi_slv0_Ar_Addr)
		,.axi_slv0_Ar_Burst(cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Burst)
		,.axi_slv0_Ar_Cache(cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Cache)
		,.axi_slv0_Ar_Id   (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Id)
		,.axi_slv0_Ar_Len  ({
			2'h0
			,cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Len})
		,.axi_slv0_Ar_Lock (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Lock)
		,.axi_slv0_Ar_Prot (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Prot)
		,.axi_slv0_Ar_Ready(cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Ready)
		,.axi_slv0_Ar_Size (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Size)
		,.axi_slv0_Ar_Valid(cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Valid)
		,.axi_slv0_Aw_Addr (cbus_Structure_Module_pcie__AXI2APB_0__axi_slv0_Aw_Addr)
		,.axi_slv0_Aw_Burst(cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Burst)
		,.axi_slv0_Aw_Cache(cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Cache)
		,.axi_slv0_Aw_Id   (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Id)
		,.axi_slv0_Aw_Len  ({
			2'h0
			,cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Len})
		,.axi_slv0_Aw_Lock (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Lock)
		,.axi_slv0_Aw_Prot (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Prot)
		,.axi_slv0_Aw_Ready(cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Ready)
		,.axi_slv0_Aw_Size (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Size)
		,.axi_slv0_Aw_Valid(cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Valid)
		,.axi_slv0_B_Id    (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Id)
		,.axi_slv0_B_Ready (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Ready)
		,.axi_slv0_B_Resp  (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Resp)
		,.axi_slv0_B_Valid (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Valid)
		,.axi_slv0_R_Data  (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Data)
		,.axi_slv0_R_Id    (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Id)
		,.axi_slv0_R_Last  (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Last)
		,.axi_slv0_R_Ready (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Ready)
		,.axi_slv0_R_Resp  (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Resp)
		,.axi_slv0_R_Valid (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Valid)
		,.axi_slv0_W_Data  (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Data)
		,.axi_slv0_W_Last  (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Last)
		,.axi_slv0_W_Ready (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Ready)
		,.axi_slv0_W_Strb  (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Strb)
		,.axi_slv0_W_Valid (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Valid)
		,.clk              (CLK__PCIE)
	);

	semifive_apb_dummy
	SCU_0 (
		 .CLK__APB    (CLK__PCIE)
		,.RSTN__APB   (RSTN__PCIE)
		,.APB__PENABLE(SCU_0__PCIE_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (SCU_0__PCIE_PBUS_0__APB__PSEL)
		,.APB__PREADY (SCU_0__PCIE_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(SCU_0__PCIE_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (SCU_0__PCIE_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (SCU_0__PCIE_PBUS_0__APB__PPROT)
		,.APB__PADDR  (SCU_0__PCIE_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (SCU_0__PCIE_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (SCU_0__PCIE_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (SCU_0__PCIE_PBUS_0__APB__PRDATA)
	);

	semifive_apb_dummy
	PCIECON_0 (
		 .CLK__APB    (CLK__PCIE)
		,.RSTN__APB   (RSTN__PCIE)
		,.APB__PENABLE(PCIECON_0__PCIE_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (PCIECON_0__PCIE_PBUS_0__APB__PSEL)
		,.APB__PREADY (PCIECON_0__PCIE_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(PCIECON_0__PCIE_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (PCIECON_0__PCIE_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (PCIECON_0__PCIE_PBUS_0__APB__PPROT)
		,.APB__PADDR  (PCIECON_0__PCIE_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (PCIECON_0__PCIE_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (PCIECON_0__PCIE_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (PCIECON_0__PCIE_PBUS_0__APB__PRDATA)
	);

	semifive_apb_dummy
	PCIEEPCSR_0 (
		 .CLK__APB    (CLK__PCIE)
		,.RSTN__APB   (RSTN__PCIE)
		,.APB__PENABLE(PCIEEPCSR_0__PCIE_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (PCIEEPCSR_0__PCIE_PBUS_0__APB__PSEL)
		,.APB__PREADY (PCIEEPCSR_0__PCIE_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(PCIEEPCSR_0__PCIE_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (PCIEEPCSR_0__PCIE_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (PCIEEPCSR_0__PCIE_PBUS_0__APB__PPROT)
		,.APB__PADDR  (PCIEEPCSR_0__PCIE_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (PCIEEPCSR_0__PCIE_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (PCIEEPCSR_0__PCIE_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (PCIEEPCSR_0__PCIE_PBUS_0__APB__PRDATA)
	);

	semifive_apb_dummy
	PCIEUPCS_0 (
		 .CLK__APB    (CLK__PCIE)
		,.RSTN__APB   (RSTN__PCIE)
		,.APB__PENABLE(PCIEUPCS_0__PCIE_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (PCIEUPCS_0__PCIE_PBUS_0__APB__PSEL)
		,.APB__PREADY (PCIEUPCS_0__PCIE_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(PCIEUPCS_0__PCIE_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (PCIEUPCS_0__PCIE_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (PCIEUPCS_0__PCIE_PBUS_0__APB__PPROT)
		,.APB__PADDR  (PCIEUPCS_0__PCIE_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (PCIEUPCS_0__PCIE_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (PCIEUPCS_0__PCIE_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (PCIEUPCS_0__PCIE_PBUS_0__APB__PRDATA)
	);

	wire [ 39: 24] __opened__cbus_Structure_Module_pcie__pcie_ctrl_Ar_Addr_msb_39_lsb_24;
	wire [ 39: 24] __opened__cbus_Structure_Module_pcie__pcie_ctrl_Aw_Addr_msb_39_lsb_24;
	cbus_Structure_Module_pcie
	cbus_Structure_Module_pcie (
		 .TM                                                           (TEST_MODE)
		,.arstn_pcie                                                   (RSTN__PCIE)
		,.clk_pcie                                                     (CLK__PCIE)
		,.dp_Link_c0_asi_to_Link_c0_ast_Data                           (SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_Data)
		,.dp_Link_c0_asi_to_Link_c0_ast_RdCnt                          (SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdCnt)
		,.dp_Link_c0_asi_to_Link_c0_ast_RdPtr                          (SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RdPtr)
		,.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst                 (SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst)
		,.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck              (SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst                 (SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst)
		,.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck              (SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_c0_asi_to_Link_c0_ast_WrCnt                          (SouthBUS__ctrl_dp_Link_c0_asi_to_Link_c0_ast_WrCnt)
		,.dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data             (SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data)
		,.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt            (SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt)
		,.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr            (SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr)
		,.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst   (SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst)
		,.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck(SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck)
		,.dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst   (SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst)
		,.dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck(SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck)
		,.dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt            (SouthBUS__ctrl_dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt)
		,.pcie_ctrl_Ar_Addr                                            ({
			__opened__cbus_Structure_Module_pcie__pcie_ctrl_Ar_Addr_msb_39_lsb_24
			,cbus_Structure_Module_pcie__AXI2APB_0__axi_slv0_Ar_Addr})
		,.pcie_ctrl_Ar_Burst                                           (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Burst)
		,.pcie_ctrl_Ar_Cache                                           (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Cache)
		,.pcie_ctrl_Ar_Id                                              (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Id)
		,.pcie_ctrl_Ar_Len                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Len)
		,.pcie_ctrl_Ar_Lock                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Lock)
		,.pcie_ctrl_Ar_Prot                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Prot)
		,.pcie_ctrl_Ar_Ready                                           (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Ready)
		,.pcie_ctrl_Ar_Size                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Size)
		,.pcie_ctrl_Ar_User                                            ()
		,.pcie_ctrl_Ar_Valid                                           (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Ar_Valid)
		,.pcie_ctrl_Aw_Addr                                            ({
			__opened__cbus_Structure_Module_pcie__pcie_ctrl_Aw_Addr_msb_39_lsb_24
			,cbus_Structure_Module_pcie__AXI2APB_0__axi_slv0_Aw_Addr})
		,.pcie_ctrl_Aw_Burst                                           (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Burst)
		,.pcie_ctrl_Aw_Cache                                           (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Cache)
		,.pcie_ctrl_Aw_Id                                              (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Id)
		,.pcie_ctrl_Aw_Len                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Len)
		,.pcie_ctrl_Aw_Lock                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Lock)
		,.pcie_ctrl_Aw_Prot                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Prot)
		,.pcie_ctrl_Aw_Ready                                           (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Ready)
		,.pcie_ctrl_Aw_Size                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Size)
		,.pcie_ctrl_Aw_User                                            ()
		,.pcie_ctrl_Aw_Valid                                           (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_Aw_Valid)
		,.pcie_ctrl_B_Id                                               (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Id)
		,.pcie_ctrl_B_Ready                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Ready)
		,.pcie_ctrl_B_Resp                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Resp)
		,.pcie_ctrl_B_Valid                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_B_Valid)
		,.pcie_ctrl_R_Data                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Data)
		,.pcie_ctrl_R_Id                                               (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Id)
		,.pcie_ctrl_R_Last                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Last)
		,.pcie_ctrl_R_Ready                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Ready)
		,.pcie_ctrl_R_Resp                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Resp)
		,.pcie_ctrl_R_Valid                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_R_Valid)
		,.pcie_ctrl_W_Data                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Data)
		,.pcie_ctrl_W_Last                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Last)
		,.pcie_ctrl_W_Ready                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Ready)
		,.pcie_ctrl_W_Strb                                             (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Strb)
		,.pcie_ctrl_W_Valid                                            (cbus_Structure_Module_pcie__AXI2APB_0__pcie_ctrl_W_Valid)
	);

	dbus_read_Structure_Module_pcie
	dbus_read_Structure_Module_pcie (
		 .TM                                                     (TEST_MODE)
		,.arstn_pcie                                             (RSTN__PCIE)
		,.clk_pcie                                               (CLK__PCIE)
		,.dp_Link_c0_asi_to_Link_c0_ast_Data                     (MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_Data)
		,.dp_Link_c0_asi_to_Link_c0_ast_RdCnt                    (MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RdCnt)
		,.dp_Link_c0_asi_to_Link_c0_ast_RdPtr                    (MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RdPtr)
		,.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst           (MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst)
		,.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck        (MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst           (MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst)
		,.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck        (MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_c0_asi_to_Link_c0_ast_WrCnt                    (MustBeConnected_To_SouthBUS__r_dp_Link_c0_asi_to_Link_c0_ast_WrCnt)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_Data             (MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_Data)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt            (MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr            (MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst   (MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck(MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst   (MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck(MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt            (MustBeConnected_To_SouthBUS__r_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt)
		,.pcie_r_Ar_Addr                                         (38'h0)
		,.pcie_r_Ar_Burst                                        (2'h0)
		,.pcie_r_Ar_Cache                                        (4'h0)
		,.pcie_r_Ar_Id                                           (6'h0)
		,.pcie_r_Ar_Len                                          (6'h0)
		,.pcie_r_Ar_Lock                                         (1'h0)
		,.pcie_r_Ar_Prot                                         (3'h0)
		,.pcie_r_Ar_Ready                                        ()
		,.pcie_r_Ar_Size                                         (3'h0)
		,.pcie_r_Ar_User                                         (32'h0)
		,.pcie_r_Ar_Valid                                        (1'h0)
		,.pcie_r_R_Data                                          ()
		,.pcie_r_R_Id                                            ()
		,.pcie_r_R_Last                                          ()
		,.pcie_r_R_Ready                                         (1'h0)
		,.pcie_r_R_Resp                                          ()
		,.pcie_r_R_User                                          ()
		,.pcie_r_R_Valid                                         ()
	);

	dbus_write_Structure_Module_pcie
	dbus_write_Structure_Module_pcie (
		 .TM                                                     (TEST_MODE)
		,.arstn_pcie                                             (RSTN__PCIE)
		,.clk_pcie                                               (CLK__PCIE)
		,.dp_Link_c0_asi_to_Link_c0_ast_Data                     (MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_Data)
		,.dp_Link_c0_asi_to_Link_c0_ast_RdCnt                    (MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RdCnt)
		,.dp_Link_c0_asi_to_Link_c0_ast_RdPtr                    (MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RdPtr)
		,.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst           (MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst)
		,.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck        (MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst           (MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst)
		,.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck        (MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_c0_asi_to_Link_c0_ast_WrCnt                    (MustBeConnected_To_SouthBUS__w_dp_Link_c0_asi_to_Link_c0_ast_WrCnt)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_Data             (MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_Data)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt            (MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdCnt)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr            (MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RdPtr)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst   (MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRst)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck(MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_RxCtl_PwrOnRstAck)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst   (MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRst)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck(MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_TxCtl_PwrOnRstAck)
		,.dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt            (MustBeConnected_To_SouthBUS__w_dp_Link_c0_astResp_to_Link_c0_asiResp_WrCnt)
		,.pcie_w_Aw_Addr                                         (38'h0)
		,.pcie_w_Aw_Burst                                        (2'h0)
		,.pcie_w_Aw_Cache                                        (4'h0)
		,.pcie_w_Aw_Id                                           (6'h0)
		,.pcie_w_Aw_Len                                          (6'h0)
		,.pcie_w_Aw_Lock                                         (1'h0)
		,.pcie_w_Aw_Prot                                         (3'h0)
		,.pcie_w_Aw_Ready                                        ()
		,.pcie_w_Aw_Size                                         (3'h0)
		,.pcie_w_Aw_User                                         (32'h0)
		,.pcie_w_Aw_Valid                                        (1'h0)
		,.pcie_w_B_Id                                            ()
		,.pcie_w_B_Ready                                         (1'h0)
		,.pcie_w_B_Resp                                          ()
		,.pcie_w_B_User                                          ()
		,.pcie_w_B_Valid                                         ()
		,.pcie_w_W_Data                                          (512'h0)
		,.pcie_w_W_Last                                          (1'h0)
		,.pcie_w_W_Ready                                         ()
		,.pcie_w_W_Strb                                          (64'h0)
		,.pcie_w_W_Valid                                         (1'h0)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_PCIE_Subsystem
endmodule 
