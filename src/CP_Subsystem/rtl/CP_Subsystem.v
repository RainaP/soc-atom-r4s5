//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module CP_Subsystem (
	 input          TEST_MODE
	,input          CLK__CPU
	,input          RSTN__CPU
	,input          EastBUS__INTG_0__IRQ
	,input          EastBUS__WDT_0__IRQ
	,input  [  3:0] EastBUS__TIMER_0__IRQ
	,input  [ 15:0] EastBUS__GPIO_0__IRQ
	,input          EastBUS__UART_0__IRQ
	,input          EastBUS__UART_1__IRQ
	,input          EastBUS__I2C_0__IRQ
	,input          EastBUS__I2C_1__IRQ
	,input          EastBUS__I2C_2__IRQ
	,input          EastBUS__I2C_3__IRQ
	,input          EastBUS__SPI_0__IRQ
	,input          EastBUS__SPI_1__IRQ
	,input          EastBUS__SPI_2__IRQ
	,input          EastBUS__SPI_3__IRQ
	,input          EastBUS__DRAM0__IRQ__controller_int
	,input          EastBUS__DRAM0__IRQ__phy_pi_int
	,input          EastBUS__DRAM1__IRQ__controller_int
	,input          EastBUS__DRAM1__IRQ__phy_pi_int
	,input          EastBUS__DRAM2__IRQ__controller_int
	,input          EastBUS__DRAM2__IRQ__phy_pi_int
	,input          EastBUS__DRAM3__IRQ__controller_int
	,input          EastBUS__DRAM3__IRQ__phy_pi_int
	,input          EastBUS__PCIE__IRQ__cfg_vpd_int
	,input          EastBUS__PCIE__IRQ__cfg_link_eq_req_int
	,input          EastBUS__PCIE__IRQ__usp_eq_redo_executed_int
	,input  [ 15:0] EastBUS__PCIE__IRQ__edma_int
	,input          EastBUS__PCIE__IRQ__assert_inta_grt
	,input          EastBUS__PCIE__IRQ__assert_intb_grt
	,input          EastBUS__PCIE__IRQ__assert_intc_grt
	,input          EastBUS__PCIE__IRQ__assert_intd_grt
	,input          EastBUS__PCIE__IRQ__deassert_inta_grt
	,input          EastBUS__PCIE__IRQ__deassert_intb_grt
	,input          EastBUS__PCIE__IRQ__deassert_intc_grt
	,input          EastBUS__PCIE__IRQ__deassert_intd_grt
	,input          EastBUS__PCIE__IRQ__cfg_safety_corr
	,input          EastBUS__PCIE__IRQ__cfg_safety_uncorr
	,output         EastBUS__UART_0__DMA__TX_ACK_N
	,output         EastBUS__UART_0__DMA__RX_ACK_N
	,input          EastBUS__UART_0__DMA__TX_REQ_N
	,input          EastBUS__UART_0__DMA__TX_SINGLE_N
	,input          EastBUS__UART_0__DMA__RX_REQ_N
	,input          EastBUS__UART_0__DMA__RX_SINGLE_N
	,output         EastBUS__UART_1__DMA__TX_ACK_N
	,output         EastBUS__UART_1__DMA__RX_ACK_N
	,input          EastBUS__UART_1__DMA__TX_REQ_N
	,input          EastBUS__UART_1__DMA__TX_SINGLE_N
	,input          EastBUS__UART_1__DMA__RX_REQ_N
	,input          EastBUS__UART_1__DMA__RX_SINGLE_N
	,output         EastBUS__I2C_0__DMA__TX_ACK
	,input          EastBUS__I2C_0__DMA__TX_REQ
	,input          EastBUS__I2C_0__DMA__TX_SINGLE
	,output         EastBUS__I2C_0__DMA__RX_ACK
	,input          EastBUS__I2C_0__DMA__RX_REQ
	,input          EastBUS__I2C_0__DMA__RX_SINGLE
	,output         EastBUS__I2C_1__DMA__TX_ACK
	,input          EastBUS__I2C_1__DMA__TX_REQ
	,input          EastBUS__I2C_1__DMA__TX_SINGLE
	,output         EastBUS__I2C_1__DMA__RX_ACK
	,input          EastBUS__I2C_1__DMA__RX_REQ
	,input          EastBUS__I2C_1__DMA__RX_SINGLE
	,output         EastBUS__I2C_2__DMA__TX_ACK
	,input          EastBUS__I2C_2__DMA__TX_REQ
	,input          EastBUS__I2C_2__DMA__TX_SINGLE
	,output         EastBUS__I2C_2__DMA__RX_ACK
	,input          EastBUS__I2C_2__DMA__RX_REQ
	,input          EastBUS__I2C_2__DMA__RX_SINGLE
	,output         EastBUS__I2C_3__DMA__TX_ACK
	,input          EastBUS__I2C_3__DMA__TX_REQ
	,input          EastBUS__I2C_3__DMA__TX_SINGLE
	,output         EastBUS__I2C_3__DMA__RX_ACK
	,input          EastBUS__I2C_3__DMA__RX_REQ
	,input          EastBUS__I2C_3__DMA__RX_SINGLE
	,output         EastBUS__SPI_0__DMA__TX_ACK
	,output         EastBUS__SPI_0__DMA__RX_ACK
	,input          EastBUS__SPI_0__DMA__TX_REQ
	,input          EastBUS__SPI_0__DMA__RX_REQ
	,input          EastBUS__SPI_0__DMA__TX_SINGLE
	,input          EastBUS__SPI_0__DMA__RX_SINGLE
	,output         EastBUS__SPI_1__DMA__TX_ACK
	,output         EastBUS__SPI_1__DMA__RX_ACK
	,input          EastBUS__SPI_1__DMA__TX_REQ
	,input          EastBUS__SPI_1__DMA__RX_REQ
	,input          EastBUS__SPI_1__DMA__TX_SINGLE
	,input          EastBUS__SPI_1__DMA__RX_SINGLE
	,output         EastBUS__SPI_2__DMA__TX_ACK
	,output         EastBUS__SPI_2__DMA__RX_ACK
	,input          EastBUS__SPI_2__DMA__TX_REQ
	,input          EastBUS__SPI_2__DMA__RX_REQ
	,input          EastBUS__SPI_2__DMA__TX_SINGLE
	,input          EastBUS__SPI_2__DMA__RX_SINGLE
	,output         EastBUS__SPI_3__DMA__TX_ACK
	,output         EastBUS__SPI_3__DMA__RX_ACK
	,input          EastBUS__SPI_3__DMA__TX_REQ
	,input          EastBUS__SPI_3__DMA__RX_REQ
	,input          EastBUS__SPI_3__DMA__TX_SINGLE
	,input          EastBUS__SPI_3__DMA__RX_SINGLE
	,output [128:0] MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_Data
	,input  [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RdCnt
	,input  [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RdPtr
	,output         MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst
	,input          MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck
	,input          MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst
	,output         MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck
	,output [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_WrCnt
	,input  [273:0] MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data
	,output [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt
	,output [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr
	,input          MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst
	,output         MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck
	,output         MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst
	,input          MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck
	,input  [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt
	,input  [128:0] MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_Data
	,output [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt
	,output [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr
	,input          MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst
	,output         MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck
	,output         MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst
	,input          MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck
	,input  [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt
	,output [273:0] MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data
	,input  [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt
	,input  [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr
	,output         MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst
	,input          MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck
	,input          MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst
	,output         MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck
	,output [  2:0] MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt
	,output [270:0] MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_Data
	,input  [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RdCnt
	,input  [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RdPtr
	,output         MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst
	,input          MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck
	,input          MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst
	,output         MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck
	,output [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_WrCnt
	,input  [125:0] MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data
	,output [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt
	,output [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr
	,input          MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst
	,output         MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck
	,output         MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst
	,input          MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck
	,input  [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt
	,input  [270:0] MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_Data
	,output [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt
	,output [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr
	,input          MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst
	,output         MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck
	,output         MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst
	,input          MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck
	,input  [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt
	,output [125:0] MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data
	,input  [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt
	,input  [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr
	,output         MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst
	,input          MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck
	,input          MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst
	,output         MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck
	,output [  2:0] MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt
	,input  [ 57:0] EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data
	,output [  2:0] EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt
	,output [  1:0] EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr
	,input          EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst
	,output         EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck
	,output         EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst
	,input          EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck
	,input  [  2:0] EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt
	,output [ 57:0] EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data
	,input  [  2:0] EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt
	,input  [  1:0] EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr
	,output         EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst
	,input          EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck
	,input          EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst
	,output         EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck
	,output [  2:0] EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_CP_Subsystem
	assign EastBUS__UART_0__DMA__TX_ACK_N = 1'h0;
	assign EastBUS__UART_0__DMA__RX_ACK_N = 1'h0;
	assign EastBUS__UART_1__DMA__TX_ACK_N = 1'h0;
	assign EastBUS__UART_1__DMA__RX_ACK_N = 1'h0;
	assign EastBUS__I2C_0__DMA__TX_ACK = 1'h0;
	assign EastBUS__I2C_0__DMA__RX_ACK = 1'h0;
	assign EastBUS__I2C_1__DMA__TX_ACK = 1'h0;
	assign EastBUS__I2C_1__DMA__RX_ACK = 1'h0;
	assign EastBUS__I2C_2__DMA__TX_ACK = 1'h0;
	assign EastBUS__I2C_2__DMA__RX_ACK = 1'h0;
	assign EastBUS__I2C_3__DMA__TX_ACK = 1'h0;
	assign EastBUS__I2C_3__DMA__RX_ACK = 1'h0;
	assign EastBUS__SPI_0__DMA__TX_ACK = 1'h0;
	assign EastBUS__SPI_0__DMA__RX_ACK = 1'h0;
	assign EastBUS__SPI_1__DMA__TX_ACK = 1'h0;
	assign EastBUS__SPI_1__DMA__RX_ACK = 1'h0;
	assign EastBUS__SPI_2__DMA__TX_ACK = 1'h0;
	assign EastBUS__SPI_2__DMA__RX_ACK = 1'h0;
	assign EastBUS__SPI_3__DMA__TX_ACK = 1'h0;
	assign EastBUS__SPI_3__DMA__RX_ACK = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_Data = 129'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_WrCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr = 3'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr = 3'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data = 274'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_Data = 271'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_WrCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr = 3'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr = 3'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data = 126'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt = 3'h0;
	assign EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt = 3'h0;
	assign EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr = 2'h0;
	assign EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data = 58'h0;
	assign EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt = 3'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_CP_Subsystem
	wire         CPU_0__INTC_0__APB__BUS__PSEL;
	wire         CPU_0__INTC_0__APB__BUS__PENABLE;
	wire         CPU_0__INTC_0__APB__BUS__PWRITE;
	wire [ 31:0] CPU_0__INTC_0__APB__BUS__PWDATA;
	wire         CPU_0__INTC_0__APB__BUS__PREADY;
	wire         CPU_0__INTC_0__APB__BUS__PSLVERR;
	wire [ 31:0] CPU_0__INTC_0__APB__BUS__PRDATA;
	wire [  7:0] CPU_0__INTC_0__APB__PADDR;
	wire         INTC_0__CPU_0__IRQ__CORE;
	wire [ 23:0] AXI2APB_0__CP_PBUS_0__apb_mst0_PAddr;
	wire         AXI2APB_0__CP_PBUS_0__apb_mst0_PEnable;
	wire [  2:0] AXI2APB_0__CP_PBUS_0__apb_mst0_PProt;
	wire [ 31:0] AXI2APB_0__CP_PBUS_0__apb_mst0_PRData;
	wire         AXI2APB_0__CP_PBUS_0__apb_mst0_PReady;
	wire         AXI2APB_0__CP_PBUS_0__apb_mst0_PSel;
	wire         AXI2APB_0__CP_PBUS_0__apb_mst0_PSlvErr;
	wire [  3:0] AXI2APB_0__CP_PBUS_0__apb_mst0_PStrb;
	wire [ 31:0] AXI2APB_0__CP_PBUS_0__apb_mst0_PWData;
	wire         AXI2APB_0__CP_PBUS_0__apb_mst0_PWrite;
	wire [ 39:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Addr;
	wire [  1:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Burst;
	wire [  3:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Cache;
	wire [  7:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Id;
	wire [  7:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Len;
	wire         dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Lock;
	wire [  2:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Prot;
	wire         dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Ready;
	wire [  2:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Size;
	wire [ 31:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_User;
	wire         dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Valid;
	wire [127:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Data;
	wire [  7:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Id;
	wire         dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Last;
	wire         dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Ready;
	wire [  1:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Resp;
	wire [  3:0] dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_User;
	wire         dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Valid;
	wire [ 39:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Addr;
	wire [  1:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Burst;
	wire [  3:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Cache;
	wire [  7:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Id;
	wire [  7:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Len;
	wire         dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Lock;
	wire [  2:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Prot;
	wire         dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Ready;
	wire [  2:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Size;
	wire [ 31:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_User;
	wire         dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Valid;
	wire [  7:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Id;
	wire         dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Ready;
	wire [  1:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Resp;
	wire [  3:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_User;
	wire         dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Valid;
	wire [127:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Data;
	wire         dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Last;
	wire         dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Ready;
	wire [ 15:0] dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Strb;
	wire         dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Valid;
	wire [ 23:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Addr;
	wire [  1:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Burst;
	wire [  3:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Cache;
	wire [  3:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Len;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Lock;
	wire [  2:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Prot;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Ready;
	wire [  2:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Size;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Valid;
	wire [ 23:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Addr;
	wire [  1:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Burst;
	wire [  3:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Cache;
	wire [  3:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Len;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Lock;
	wire [  2:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Prot;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Ready;
	wire [  2:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Size;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Valid;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Ready;
	wire [  1:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Resp;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Valid;
	wire [ 31:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Data;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Last;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Ready;
	wire [  1:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Resp;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Valid;
	wire [ 31:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Data;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Last;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Ready;
	wire [  3:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Strb;
	wire         cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Valid;
	wire [  4:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Id;
	wire [  4:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Id;
	wire [  4:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Id;
	wire [  4:0] cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Id;

	assign EastBUS__UART_0__DMA__TX_ACK_N = 1'h0;
	assign EastBUS__UART_0__DMA__RX_ACK_N = 1'h0;
	assign EastBUS__UART_1__DMA__TX_ACK_N = 1'h0;
	assign EastBUS__UART_1__DMA__RX_ACK_N = 1'h0;
	assign EastBUS__I2C_0__DMA__TX_ACK = 1'h0;
	assign EastBUS__I2C_0__DMA__RX_ACK = 1'h0;
	assign EastBUS__I2C_1__DMA__TX_ACK = 1'h0;
	assign EastBUS__I2C_1__DMA__RX_ACK = 1'h0;
	assign EastBUS__I2C_2__DMA__TX_ACK = 1'h0;
	assign EastBUS__I2C_2__DMA__RX_ACK = 1'h0;
	assign EastBUS__I2C_3__DMA__TX_ACK = 1'h0;
	assign EastBUS__I2C_3__DMA__RX_ACK = 1'h0;
	assign EastBUS__SPI_0__DMA__TX_ACK = 1'h0;
	assign EastBUS__SPI_0__DMA__RX_ACK = 1'h0;
	assign EastBUS__SPI_1__DMA__TX_ACK = 1'h0;
	assign EastBUS__SPI_1__DMA__RX_ACK = 1'h0;
	assign EastBUS__SPI_2__DMA__TX_ACK = 1'h0;
	assign EastBUS__SPI_2__DMA__RX_ACK = 1'h0;
	assign EastBUS__SPI_3__DMA__TX_ACK = 1'h0;
	assign EastBUS__SPI_3__DMA__RX_ACK = 1'h0;

	wire [ 37:  8] __opened__CPU_0__APB__BUS__PADDR_msb_37_lsb_8;
	semifive_cpu_dummy #(
		 .MAX_BURST(64)
		,.MAX_OUTSTANDING(64)
		,.D(128)
		,.RUSER(4)
		,.L(8)
		,.AUSER(32)
		,.ID(8)
		,.A(40)
		,.hartid(0)
	) CPU_0 (
		 .CLK__AXI           (CLK__CPU)
		,.RSTN__AXI          (RSTN__CPU)
		,.IRQ__CORE          ()
		,.IRQ__GIC           (INTC_0__CPU_0__IRQ__CORE)
		,.APB__BUS__PSEL     (CPU_0__INTC_0__APB__BUS__PSEL)
		,.APB__BUS__PENABLE  (CPU_0__INTC_0__APB__BUS__PENABLE)
		,.APB__BUS__PWRITE   (CPU_0__INTC_0__APB__BUS__PWRITE)
		,.APB__BUS__PADDR    ({
			__opened__CPU_0__APB__BUS__PADDR_msb_37_lsb_8
			,CPU_0__INTC_0__APB__PADDR})
		,.APB__BUS__PWDATA   (CPU_0__INTC_0__APB__BUS__PWDATA)
		,.APB__BUS__PREADY   (CPU_0__INTC_0__APB__BUS__PREADY)
		,.APB__BUS__PSLVERR  (CPU_0__INTC_0__APB__BUS__PSLVERR)
		,.APB__BUS__PRDATA   (CPU_0__INTC_0__APB__BUS__PRDATA)
		,.AXI4__CORE__AWID   (8'h0)
		,.AXI4__CORE__AWADDR (40'h0)
		,.AXI4__CORE__AWLEN  (8'h0)
		,.AXI4__CORE__AWSIZE (3'h0)
		,.AXI4__CORE__AWBURST(2'h0)
		,.AXI4__CORE__AWLOCK (1'h0)
		,.AXI4__CORE__AWCACHE(4'h0)
		,.AXI4__CORE__AWPROT (3'h0)
		,.AXI4__CORE__AWUSER (32'h0)
		,.AXI4__CORE__AWVALID(1'h0)
		,.AXI4__CORE__AWREADY()
		,.AXI4__CORE__WDATA  (128'h0)
		,.AXI4__CORE__WSTRB  (16'h0)
		,.AXI4__CORE__WLAST  (1'h0)
		,.AXI4__CORE__WVALID (1'h0)
		,.AXI4__CORE__WREADY ()
		,.AXI4__CORE__BID    ()
		,.AXI4__CORE__BUSER  ()
		,.AXI4__CORE__BVALID ()
		,.AXI4__CORE__BRESP  ()
		,.AXI4__CORE__BREADY (1'h0)
		,.AXI4__CORE__ARID   (8'h0)
		,.AXI4__CORE__ARADDR (40'h0)
		,.AXI4__CORE__ARLEN  (8'h0)
		,.AXI4__CORE__ARSIZE (3'h0)
		,.AXI4__CORE__ARBURST(2'h0)
		,.AXI4__CORE__ARLOCK (1'h0)
		,.AXI4__CORE__ARCACHE(4'h0)
		,.AXI4__CORE__ARPROT (3'h0)
		,.AXI4__CORE__ARUSER (32'h0)
		,.AXI4__CORE__ARVALID(1'h0)
		,.AXI4__CORE__ARREADY()
		,.AXI4__CORE__RID    ()
		,.AXI4__CORE__RDATA  ()
		,.AXI4__CORE__RRESP  ()
		,.AXI4__CORE__RLAST  ()
		,.AXI4__CORE__RUSER  ()
		,.AXI4__CORE__RVALID ()
		,.AXI4__CORE__RREADY (1'h0)
		,.AXI4__BUS__AWID    (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Id)
		,.AXI4__BUS__AWADDR  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Addr)
		,.AXI4__BUS__AWLEN   (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Len)
		,.AXI4__BUS__AWSIZE  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Size)
		,.AXI4__BUS__AWBURST (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Burst)
		,.AXI4__BUS__AWLOCK  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Lock)
		,.AXI4__BUS__AWCACHE (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Cache)
		,.AXI4__BUS__AWPROT  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Prot)
		,.AXI4__BUS__AWUSER  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_User)
		,.AXI4__BUS__AWVALID (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Valid)
		,.AXI4__BUS__AWREADY (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Ready)
		,.AXI4__BUS__WDATA   (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Data)
		,.AXI4__BUS__WSTRB   (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Strb)
		,.AXI4__BUS__WLAST   (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Last)
		,.AXI4__BUS__WVALID  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Valid)
		,.AXI4__BUS__WREADY  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Ready)
		,.AXI4__BUS__BID     (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Id)
		,.AXI4__BUS__BUSER   (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_User)
		,.AXI4__BUS__BVALID  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Valid)
		,.AXI4__BUS__BRESP   (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Resp)
		,.AXI4__BUS__BREADY  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Ready)
		,.AXI4__BUS__ARID    (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Id)
		,.AXI4__BUS__ARADDR  (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Addr)
		,.AXI4__BUS__ARLEN   (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Len)
		,.AXI4__BUS__ARSIZE  (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Size)
		,.AXI4__BUS__ARBURST (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Burst)
		,.AXI4__BUS__ARLOCK  (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Lock)
		,.AXI4__BUS__ARCACHE (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Cache)
		,.AXI4__BUS__ARPROT  (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Prot)
		,.AXI4__BUS__ARUSER  (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_User)
		,.AXI4__BUS__ARVALID (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Valid)
		,.AXI4__BUS__ARREADY (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Ready)
		,.AXI4__BUS__RID     (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Id)
		,.AXI4__BUS__RDATA   (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Data)
		,.AXI4__BUS__RRESP   (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Resp)
		,.AXI4__BUS__RLAST   (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Last)
		,.AXI4__BUS__RUSER   (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_User)
		,.AXI4__BUS__RVALID  (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Valid)
		,.AXI4__BUS__RREADY  (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Ready)
	);

	semifive_apb_dummy
	SCU_0 (
		 .CLK__APB    (CLK__CPU)
		,.RSTN__APB   (RSTN__CPU)
		,.APB__PENABLE(1'h0)
		,.APB__PSEL   (1'h0)
		,.APB__PREADY ()
		,.APB__PSLVERR()
		,.APB__PWRITE (1'h0)
		,.APB__PPROT  (3'h0)
		,.APB__PADDR  (12'h0)
		,.APB__PSTRB  (4'h0)
		,.APB__PWDATA (32'h0)
		,.APB__PRDATA ()
	);

	SemiINTC
	INTC_0 (
		 .CLK__APB    (CLK__CPU)
		,.RSTN__APB   (RSTN__CPU)
		,.IRQ__CORE   (INTC_0__CPU_0__IRQ__CORE)
		,.IRQ__SRC    ({
			27'h0
			,EastBUS__PCIE__IRQ__cfg_safety_uncorr
			,EastBUS__PCIE__IRQ__cfg_safety_corr
			,EastBUS__PCIE__IRQ__deassert_intd_grt
			,EastBUS__PCIE__IRQ__deassert_intc_grt
			,EastBUS__PCIE__IRQ__deassert_intb_grt
			,EastBUS__PCIE__IRQ__deassert_inta_grt
			,EastBUS__PCIE__IRQ__assert_intd_grt
			,EastBUS__PCIE__IRQ__assert_intc_grt
			,EastBUS__PCIE__IRQ__assert_intb_grt
			,EastBUS__PCIE__IRQ__assert_inta_grt
			,EastBUS__PCIE__IRQ__edma_int
			,EastBUS__PCIE__IRQ__usp_eq_redo_executed_int
			,EastBUS__PCIE__IRQ__cfg_link_eq_req_int
			,EastBUS__PCIE__IRQ__cfg_vpd_int
			,EastBUS__DRAM3__IRQ__phy_pi_int
			,EastBUS__DRAM3__IRQ__controller_int
			,EastBUS__DRAM2__IRQ__phy_pi_int
			,EastBUS__DRAM2__IRQ__controller_int
			,EastBUS__DRAM1__IRQ__phy_pi_int
			,EastBUS__DRAM1__IRQ__controller_int
			,EastBUS__DRAM0__IRQ__phy_pi_int
			,EastBUS__DRAM0__IRQ__controller_int
			,32'h0
			,EastBUS__GPIO_0__IRQ
			,EastBUS__SPI_3__IRQ
			,EastBUS__SPI_2__IRQ
			,EastBUS__SPI_1__IRQ
			,EastBUS__SPI_0__IRQ
			,EastBUS__I2C_3__IRQ
			,EastBUS__I2C_2__IRQ
			,EastBUS__I2C_1__IRQ
			,EastBUS__I2C_0__IRQ
			,EastBUS__TIMER_0__IRQ
			,EastBUS__UART_1__IRQ
			,EastBUS__UART_0__IRQ
			,EastBUS__WDT_0__IRQ
			,EastBUS__INTG_0__IRQ})
		,.APB__PENABLE(CPU_0__INTC_0__APB__BUS__PENABLE)
		,.APB__PSEL   (CPU_0__INTC_0__APB__BUS__PSEL)
		,.APB__PREADY (CPU_0__INTC_0__APB__BUS__PREADY)
		,.APB__PSLVERR(CPU_0__INTC_0__APB__BUS__PSLVERR)
		,.APB__PWRITE (CPU_0__INTC_0__APB__BUS__PWRITE)
		,.APB__PPROT  (3'h0)
		,.APB__PADDR  (CPU_0__INTC_0__APB__PADDR)
		,.APB__PSTRB  (4'hf)
		,.APB__PWDATA (CPU_0__INTC_0__APB__BUS__PWDATA)
		,.APB__PRDATA (CPU_0__INTC_0__APB__BUS__PRDATA)
	);

	CP_PBUS #(
		 .ABITS(24)
	) CP_PBUS_0 (
		 .CLK__APB            (CLK__CPU)
		,.RSTN__APB           (RSTN__CPU)
		,.APB__PSEL           (AXI2APB_0__CP_PBUS_0__apb_mst0_PSel)
		,.APB__PENABLE        (AXI2APB_0__CP_PBUS_0__apb_mst0_PEnable)
		,.APB__PWRITE         (AXI2APB_0__CP_PBUS_0__apb_mst0_PWrite)
		,.APB__PPROT          (AXI2APB_0__CP_PBUS_0__apb_mst0_PProt)
		,.APB__PADDR          (AXI2APB_0__CP_PBUS_0__apb_mst0_PAddr)
		,.APB__PSTRB          (AXI2APB_0__CP_PBUS_0__apb_mst0_PStrb)
		,.APB__PWDATA         (AXI2APB_0__CP_PBUS_0__apb_mst0_PWData)
		,.APB__PREADY         (AXI2APB_0__CP_PBUS_0__apb_mst0_PReady)
		,.APB__PSLVERR        (AXI2APB_0__CP_PBUS_0__apb_mst0_PSlvErr)
		,.APB__PRDATA         (AXI2APB_0__CP_PBUS_0__apb_mst0_PRData)
		,.SCU_0__APB__PSEL    ()
		,.SCU_0__APB__PENABLE ()
		,.SCU_0__APB__PWRITE  ()
		,.SCU_0__APB__PPROT   ()
		,.SCU_0__APB__PADDR   ()
		,.SCU_0__APB__PSTRB   ()
		,.SCU_0__APB__PWDATA  ()
		,.SCU_0__APB__PREADY  (1'h0)
		,.SCU_0__APB__PSLVERR (1'h0)
		,.SCU_0__APB__PRDATA  (32'h0)
		,.INTC_0__APB__PSEL   ()
		,.INTC_0__APB__PENABLE()
		,.INTC_0__APB__PWRITE ()
		,.INTC_0__APB__PPROT  ()
		,.INTC_0__APB__PADDR  ()
		,.INTC_0__APB__PSTRB  ()
		,.INTC_0__APB__PWDATA ()
		,.INTC_0__APB__PREADY (1'h0)
		,.INTC_0__APB__PSLVERR(1'h0)
		,.INTC_0__APB__PRDATA (32'h0)
	);

	wire [  7:  5] __opened__AXI2APB_0__axi_slv0_B_Id_msb_7_lsb_5;
	wire [  7:  5] __opened__AXI2APB_0__axi_slv0_R_Id_msb_7_lsb_5;
	axi2apb_bridge
	AXI2APB_0 (
		 .TM               (TEST_MODE)
		,.apb_mst0_PAddr   (AXI2APB_0__CP_PBUS_0__apb_mst0_PAddr)
		,.apb_mst0_PEnable (AXI2APB_0__CP_PBUS_0__apb_mst0_PEnable)
		,.apb_mst0_PProt   (AXI2APB_0__CP_PBUS_0__apb_mst0_PProt)
		,.apb_mst0_PRData  (AXI2APB_0__CP_PBUS_0__apb_mst0_PRData)
		,.apb_mst0_PReady  (AXI2APB_0__CP_PBUS_0__apb_mst0_PReady)
		,.apb_mst0_PSel    (AXI2APB_0__CP_PBUS_0__apb_mst0_PSel)
		,.apb_mst0_PSlvErr (AXI2APB_0__CP_PBUS_0__apb_mst0_PSlvErr)
		,.apb_mst0_PStrb   (AXI2APB_0__CP_PBUS_0__apb_mst0_PStrb)
		,.apb_mst0_PWData  (AXI2APB_0__CP_PBUS_0__apb_mst0_PWData)
		,.apb_mst0_PWrite  (AXI2APB_0__CP_PBUS_0__apb_mst0_PWrite)
		,.apb_mst1_PAddr   ()
		,.apb_mst1_PEnable ()
		,.apb_mst1_PProt   ()
		,.apb_mst1_PRData  (32'h0)
		,.apb_mst1_PReady  (1'h0)
		,.apb_mst1_PSel    ()
		,.apb_mst1_PSlvErr (1'h0)
		,.apb_mst1_PStrb   ()
		,.apb_mst1_PWData  ()
		,.apb_mst1_PWrite  ()
		,.apb_mst2_PAddr   ()
		,.apb_mst2_PEnable ()
		,.apb_mst2_PProt   ()
		,.apb_mst2_PRData  (32'h0)
		,.apb_mst2_PReady  (1'h0)
		,.apb_mst2_PSel    ()
		,.apb_mst2_PSlvErr (1'h0)
		,.apb_mst2_PStrb   ()
		,.apb_mst2_PWData  ()
		,.apb_mst2_PWrite  ()
		,.apb_mst3_PAddr   ()
		,.apb_mst3_PEnable ()
		,.apb_mst3_PProt   ()
		,.apb_mst3_PRData  (32'h0)
		,.apb_mst3_PReady  (1'h0)
		,.apb_mst3_PSel    ()
		,.apb_mst3_PSlvErr (1'h0)
		,.apb_mst3_PStrb   ()
		,.apb_mst3_PWData  ()
		,.apb_mst3_PWrite  ()
		,.apb_mst4_PAddr   ()
		,.apb_mst4_PEnable ()
		,.apb_mst4_PProt   ()
		,.apb_mst4_PRData  (32'h0)
		,.apb_mst4_PReady  (1'h0)
		,.apb_mst4_PSel    ()
		,.apb_mst4_PSlvErr (1'h0)
		,.apb_mst4_PStrb   ()
		,.apb_mst4_PWData  ()
		,.apb_mst4_PWrite  ()
		,.apb_mst5_PAddr   ()
		,.apb_mst5_PEnable ()
		,.apb_mst5_PProt   ()
		,.apb_mst5_PRData  (32'h0)
		,.apb_mst5_PReady  (1'h0)
		,.apb_mst5_PSel    ()
		,.apb_mst5_PSlvErr (1'h0)
		,.apb_mst5_PStrb   ()
		,.apb_mst5_PWData  ()
		,.apb_mst5_PWrite  ()
		,.apb_mst6_PAddr   ()
		,.apb_mst6_PEnable ()
		,.apb_mst6_PProt   ()
		,.apb_mst6_PRData  (32'h0)
		,.apb_mst6_PReady  (1'h0)
		,.apb_mst6_PSel    ()
		,.apb_mst6_PSlvErr (1'h0)
		,.apb_mst6_PStrb   ()
		,.apb_mst6_PWData  ()
		,.apb_mst6_PWrite  ()
		,.apb_mst7_PAddr   ()
		,.apb_mst7_PEnable ()
		,.apb_mst7_PProt   ()
		,.apb_mst7_PRData  (32'h0)
		,.apb_mst7_PReady  (1'h0)
		,.apb_mst7_PSel    ()
		,.apb_mst7_PSlvErr (1'h0)
		,.apb_mst7_PStrb   ()
		,.apb_mst7_PWData  ()
		,.apb_mst7_PWrite  ()
		,.arstn            (RSTN__CPU)
		,.axi_slv0_Ar_Addr (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Addr)
		,.axi_slv0_Ar_Burst(cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Burst)
		,.axi_slv0_Ar_Cache(cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Cache)
		,.axi_slv0_Ar_Id   ({
			3'h0
			,cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Id})
		,.axi_slv0_Ar_Len  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Len)
		,.axi_slv0_Ar_Lock (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Lock)
		,.axi_slv0_Ar_Prot (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Prot)
		,.axi_slv0_Ar_Ready(cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Ready)
		,.axi_slv0_Ar_Size (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Size)
		,.axi_slv0_Ar_Valid(cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Valid)
		,.axi_slv0_Aw_Addr (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Addr)
		,.axi_slv0_Aw_Burst(cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Burst)
		,.axi_slv0_Aw_Cache(cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Cache)
		,.axi_slv0_Aw_Id   ({
			3'h0
			,cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Id})
		,.axi_slv0_Aw_Len  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Len)
		,.axi_slv0_Aw_Lock (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Lock)
		,.axi_slv0_Aw_Prot (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Prot)
		,.axi_slv0_Aw_Ready(cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Ready)
		,.axi_slv0_Aw_Size (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Size)
		,.axi_slv0_Aw_Valid(cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Valid)
		,.axi_slv0_B_Id    ({
			__opened__AXI2APB_0__axi_slv0_B_Id_msb_7_lsb_5
			,cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Id})
		,.axi_slv0_B_Ready (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Ready)
		,.axi_slv0_B_Resp  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Resp)
		,.axi_slv0_B_Valid (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Valid)
		,.axi_slv0_R_Data  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Data)
		,.axi_slv0_R_Id    ({
			__opened__AXI2APB_0__axi_slv0_R_Id_msb_7_lsb_5
			,cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Id})
		,.axi_slv0_R_Last  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Last)
		,.axi_slv0_R_Ready (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Ready)
		,.axi_slv0_R_Resp  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Resp)
		,.axi_slv0_R_Valid (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Valid)
		,.axi_slv0_W_Data  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Data)
		,.axi_slv0_W_Last  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Last)
		,.axi_slv0_W_Ready (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Ready)
		,.axi_slv0_W_Strb  (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Strb)
		,.axi_slv0_W_Valid (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Valid)
		,.clk              (CLK__CPU)
	);

	dbus_read_Structure_Module_cpu
	dbus_read_Structure_Module_cpu (
		 .TM                                                           (TEST_MODE)
		,.arstn_cpu                                                    (RSTN__CPU)
		,.clk_cpu                                                      (CLK__CPU)
		,.cpu_r_Ar_Addr                                                (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Addr)
		,.cpu_r_Ar_Burst                                               (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Burst)
		,.cpu_r_Ar_Cache                                               (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Cache)
		,.cpu_r_Ar_Id                                                  (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Id)
		,.cpu_r_Ar_Len                                                 (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Len)
		,.cpu_r_Ar_Lock                                                (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Lock)
		,.cpu_r_Ar_Prot                                                (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Prot)
		,.cpu_r_Ar_Ready                                               (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Ready)
		,.cpu_r_Ar_Size                                                (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Size)
		,.cpu_r_Ar_User                                                (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_User)
		,.cpu_r_Ar_Valid                                               (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_Ar_Valid)
		,.cpu_r_R_Data                                                 (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Data)
		,.cpu_r_R_Id                                                   (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Id)
		,.cpu_r_R_Last                                                 (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Last)
		,.cpu_r_R_Ready                                                (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Ready)
		,.cpu_r_R_Resp                                                 (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Resp)
		,.cpu_r_R_User                                                 (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_User)
		,.cpu_r_R_Valid                                                (dbus_read_Structure_Module_cpu__CPU_0__cpu_r_R_Valid)
		,.dp_Link_c1_asi_to_Link_c1_ast_Data                           (MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_Data)
		,.dp_Link_c1_asi_to_Link_c1_ast_RdCnt                          (MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RdCnt)
		,.dp_Link_c1_asi_to_Link_c1_ast_RdPtr                          (MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RdPtr)
		,.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst                 (MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst)
		,.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck              (MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst                 (MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst)
		,.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck              (MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_c1_asi_to_Link_c1_ast_WrCnt                          (MustBeConnected_To_EastBUS__r_dp_Link_c1_asi_to_Link_c1_ast_WrCnt)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data             (MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt            (MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr            (MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst   (MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck(MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst   (MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck(MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt            (MustBeConnected_To_EastBUS__r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_Data                         (MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_Data)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt                        (MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr                        (MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst               (MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck            (MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst               (MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck            (MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt                        (MustBeConnected_To_EastBUS__r_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data                 (MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt                (MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr                (MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst       (MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck    (MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst       (MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck    (MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt                (MustBeConnected_To_EastBUS__r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt)
		,.pcie2cpu_acp_r_Ar_Addr                                       ()
		,.pcie2cpu_acp_r_Ar_Burst                                      ()
		,.pcie2cpu_acp_r_Ar_Cache                                      ()
		,.pcie2cpu_acp_r_Ar_Id                                         ()
		,.pcie2cpu_acp_r_Ar_Len                                        ()
		,.pcie2cpu_acp_r_Ar_Lock                                       ()
		,.pcie2cpu_acp_r_Ar_Prot                                       ()
		,.pcie2cpu_acp_r_Ar_Ready                                      (1'h0)
		,.pcie2cpu_acp_r_Ar_Size                                       ()
		,.pcie2cpu_acp_r_Ar_User                                       ()
		,.pcie2cpu_acp_r_Ar_Valid                                      ()
		,.pcie2cpu_acp_r_R_Data                                        (128'h0)
		,.pcie2cpu_acp_r_R_Id                                          (5'h0)
		,.pcie2cpu_acp_r_R_Last                                        (1'h0)
		,.pcie2cpu_acp_r_R_Ready                                       ()
		,.pcie2cpu_acp_r_R_Resp                                        (2'h0)
		,.pcie2cpu_acp_r_R_Valid                                       (1'h0)
	);

	dbus_write_Structure_Module_cpu
	dbus_write_Structure_Module_cpu (
		 .TM                                                           (TEST_MODE)
		,.arstn_cpu                                                    (RSTN__CPU)
		,.clk_cpu                                                      (CLK__CPU)
		,.cpu_w_Aw_Addr                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Addr)
		,.cpu_w_Aw_Burst                                               (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Burst)
		,.cpu_w_Aw_Cache                                               (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Cache)
		,.cpu_w_Aw_Id                                                  (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Id)
		,.cpu_w_Aw_Len                                                 (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Len)
		,.cpu_w_Aw_Lock                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Lock)
		,.cpu_w_Aw_Prot                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Prot)
		,.cpu_w_Aw_Ready                                               (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Ready)
		,.cpu_w_Aw_Size                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Size)
		,.cpu_w_Aw_User                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_User)
		,.cpu_w_Aw_Valid                                               (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_Aw_Valid)
		,.cpu_w_B_Id                                                   (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Id)
		,.cpu_w_B_Ready                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Ready)
		,.cpu_w_B_Resp                                                 (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Resp)
		,.cpu_w_B_User                                                 (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_User)
		,.cpu_w_B_Valid                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_B_Valid)
		,.cpu_w_W_Data                                                 (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Data)
		,.cpu_w_W_Last                                                 (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Last)
		,.cpu_w_W_Ready                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Ready)
		,.cpu_w_W_Strb                                                 (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Strb)
		,.cpu_w_W_Valid                                                (dbus_write_Structure_Module_cpu__CPU_0__cpu_w_W_Valid)
		,.dp_Link_c1_asi_to_Link_c1_ast_Data                           (MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_Data)
		,.dp_Link_c1_asi_to_Link_c1_ast_RdCnt                          (MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RdCnt)
		,.dp_Link_c1_asi_to_Link_c1_ast_RdPtr                          (MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RdPtr)
		,.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst                 (MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst)
		,.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck              (MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst                 (MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst)
		,.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck              (MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_c1_asi_to_Link_c1_ast_WrCnt                          (MustBeConnected_To_EastBUS__w_dp_Link_c1_asi_to_Link_c1_ast_WrCnt)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data             (MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt            (MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr            (MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst   (MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck(MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst   (MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck(MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck)
		,.dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt            (MustBeConnected_To_EastBUS__w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_Data                         (MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_Data)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt                        (MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr                        (MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst               (MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck            (MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst               (MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck            (MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt                        (MustBeConnected_To_EastBUS__w_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data                 (MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt                (MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr                (MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst       (MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck    (MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst       (MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck    (MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck)
		,.dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt                (MustBeConnected_To_EastBUS__w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt)
		,.pcie2cpu_acp_w_Aw_Addr                                       ()
		,.pcie2cpu_acp_w_Aw_Burst                                      ()
		,.pcie2cpu_acp_w_Aw_Cache                                      ()
		,.pcie2cpu_acp_w_Aw_Id                                         ()
		,.pcie2cpu_acp_w_Aw_Len                                        ()
		,.pcie2cpu_acp_w_Aw_Lock                                       ()
		,.pcie2cpu_acp_w_Aw_Prot                                       ()
		,.pcie2cpu_acp_w_Aw_Ready                                      (1'h0)
		,.pcie2cpu_acp_w_Aw_Size                                       ()
		,.pcie2cpu_acp_w_Aw_User                                       ()
		,.pcie2cpu_acp_w_Aw_Valid                                      ()
		,.pcie2cpu_acp_w_B_Id                                          (5'h0)
		,.pcie2cpu_acp_w_B_Ready                                       ()
		,.pcie2cpu_acp_w_B_Resp                                        (2'h0)
		,.pcie2cpu_acp_w_B_Valid                                       (1'h0)
		,.pcie2cpu_acp_w_W_Data                                        ()
		,.pcie2cpu_acp_w_W_Last                                        ()
		,.pcie2cpu_acp_w_W_Ready                                       (1'h0)
		,.pcie2cpu_acp_w_W_Strb                                        ()
		,.pcie2cpu_acp_w_W_Valid                                       ()
	);

	cbus_Structure_Module_cpu
	cbus_Structure_Module_cpu (
		 .TM                                                                   (TEST_MODE)
		,.arstn_cpu                                                            (RSTN__CPU)
		,.clk_cpu                                                              (CLK__CPU)
		,.cpu_ctrl_Ar_Addr                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Addr)
		,.cpu_ctrl_Ar_Burst                                                    (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Burst)
		,.cpu_ctrl_Ar_Cache                                                    (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Cache)
		,.cpu_ctrl_Ar_Id                                                       (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Id)
		,.cpu_ctrl_Ar_Len                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Len)
		,.cpu_ctrl_Ar_Lock                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Lock)
		,.cpu_ctrl_Ar_Prot                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Prot)
		,.cpu_ctrl_Ar_Ready                                                    (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Ready)
		,.cpu_ctrl_Ar_Size                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Size)
		,.cpu_ctrl_Ar_Valid                                                    (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Ar_Valid)
		,.cpu_ctrl_Aw_Addr                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Addr)
		,.cpu_ctrl_Aw_Burst                                                    (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Burst)
		,.cpu_ctrl_Aw_Cache                                                    (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Cache)
		,.cpu_ctrl_Aw_Id                                                       (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Id)
		,.cpu_ctrl_Aw_Len                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Len)
		,.cpu_ctrl_Aw_Lock                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Lock)
		,.cpu_ctrl_Aw_Prot                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Prot)
		,.cpu_ctrl_Aw_Ready                                                    (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Ready)
		,.cpu_ctrl_Aw_Size                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Size)
		,.cpu_ctrl_Aw_Valid                                                    (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_Aw_Valid)
		,.cpu_ctrl_B_Id                                                        (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Id)
		,.cpu_ctrl_B_Ready                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Ready)
		,.cpu_ctrl_B_Resp                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Resp)
		,.cpu_ctrl_B_Valid                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_B_Valid)
		,.cpu_ctrl_R_Data                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Data)
		,.cpu_ctrl_R_Id                                                        (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Id)
		,.cpu_ctrl_R_Last                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Last)
		,.cpu_ctrl_R_Ready                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Ready)
		,.cpu_ctrl_R_Resp                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Resp)
		,.cpu_ctrl_R_Valid                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_R_Valid)
		,.cpu_ctrl_W_Data                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Data)
		,.cpu_ctrl_W_Last                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Last)
		,.cpu_ctrl_W_Ready                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Ready)
		,.cpu_ctrl_W_Strb                                                      (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Strb)
		,.cpu_ctrl_W_Valid                                                     (cbus_Structure_Module_cpu__AXI2APB_0__cpu_ctrl_W_Valid)
		,.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data                           (EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data)
		,.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt                          (EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt)
		,.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr                          (EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr)
		,.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst                 (EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst)
		,.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck              (EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst                 (EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst)
		,.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck              (EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt                          (EastBUS__dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt)
		,.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data             (EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data)
		,.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt            (EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt)
		,.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr            (EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr)
		,.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst   (EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst)
		,.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck(EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck)
		,.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst   (EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst)
		,.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck(EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck)
		,.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt            (EastBUS__dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_CP_Subsystem
endmodule 
