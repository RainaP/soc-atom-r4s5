// See LICENSE.SEMIFIVE for license details.

module SemiINTC
(
	 input                   CLK__APB
	,input                   RSTN__APB

	,output                  IRQ__CORE
	,input  [127:0]          IRQ__SRC

    ,input                   APB__PENABLE
    ,input                   APB__PSEL
    ,output                  APB__PREADY
    ,output                  APB__PSLVERR
    ,input                   APB__PWRITE
    ,input  [ 2:0]           APB__PPROT
    ,input  [ 7:0]           APB__PADDR
    ,input  [ 3:0]           APB__PSTRB
	,input  [31:0]           APB__PWDATA
    ,output [31:0]           APB__PRDATA
);
    wire       APB_RE  = APB__PSEL && APB__PENABLE && (0==APB__PWRITE);
    wire       APB_WE  = APB__PSEL && APB__PENABLE && (1==APB__PWRITE);
    reg [31:0] rPRDATA  ;

    assign APB__PREADY  = 1;
    assign APB__PSLVERR = 0;
    assign APB__PRDATA = rPRDATA;

	//--------------------------------------------------------------------
	reg[127:0] ENABLE  ;
	reg[127:0] PEND    ;
	reg[127:0] STATUS  ;

	genvar g;
	generate
    for( g=0; g<(128/8); g=g+1 )
    begin : SFR
		always @(posedge CLK__APB or negedge RSTN__APB)
			if (~RSTN__APB)                                                          ENABLE[7+g*8:g*8] <= 0;
			else if(APB_WE && (APB__PADDR[7:2]==(('h00+g)>>2)) && APB__PSTRB[(g&3)]) ENABLE[7+g*8:g*8] <= ENABLE[7+g*8:g*8] | APB__PWDATA[7+(g&3)*8:(g&3)*8];
			else if(APB_WE && (APB__PADDR[7:2]==(('h10+g)>>2)) && APB__PSTRB[(g&3)]) ENABLE[7+g*8:g*8] <= ENABLE[7+g*8:g*8] & (~APB__PWDATA[7+(g&3)*8:(g&3)*8]);

		always @(posedge CLK__APB or negedge RSTN__APB)
			if (~RSTN__APB)                                                          PEND[7+g*8:g*8] <= 0;
			else if(APB_WE && (APB__PADDR[7:2]==(('h20+g)>>2)) && APB__PSTRB[(g&3)]) PEND[7+g*8:g*8] <= (PEND[7+g*8:g*8] | (STATUS[7+g*8:g*8]&ENABLE[7+g*8:g*8]) ) & (~APB__PWDATA[7+(g&3)*8:(g&3)*8]);
			else if(0!=STATUS[7+g*8:g*8])                                            PEND[7+g*8:g*8] <= (PEND[7+g*8:g*8] | (STATUS[7+g*8:g*8]&ENABLE[7+g*8:g*8]) );
    end
    endgenerate

	always @(posedge CLK__APB or negedge RSTN__APB)
		if (~RSTN__APB) STATUS <= 0;
		else			STATUS <= IRQ__SRC; // @TODO Insert synchronizer here

	//--------------------------------------------------------------------
	always @( * )
		case (APB__PADDR[7:0])
		'h00:    rPRDATA = ENABLE [31+32*0:32*0];
		'h04:    rPRDATA = ENABLE [31+32*1:32*1];
		'h08:    rPRDATA = ENABLE [31+32*2:32*2];
		'h0C:    rPRDATA = ENABLE [31+32*3:32*3];
		'h10:    rPRDATA = ENABLE [31+32*0:32*0];
		'h14:    rPRDATA = ENABLE [31+32*1:32*1];
		'h18:    rPRDATA = ENABLE [31+32*2:32*2];
		'h1C:    rPRDATA = ENABLE [31+32*3:32*3];
		'h20:    rPRDATA = PEND   [31+32*0:32*0];
		'h24:    rPRDATA = PEND   [31+32*1:32*1];
		'h28:    rPRDATA = PEND   [31+32*2:32*2];
		'h2C:    rPRDATA = PEND   [31+32*3:32*3];
		'h30:    rPRDATA = STATUS [31+32*0:32*0];
		'h34:    rPRDATA = STATUS [31+32*1:32*1];
		'h38:    rPRDATA = STATUS [31+32*2:32*2];
		'h3C:    rPRDATA = STATUS [31+32*3:32*3];
		default: rPRDATA = 'hDEAD_C0DE;
		endcase

	assign IRQ__CORE = (0!=PEND);
endmodule

