

`timescale 1ns/1ps
module atom_bus_ClockManagerCell( In_Clk , In_RstN , In_Tm , root_Clk , root_En , root_RstN , root_Tm );
	input   In_Clk    ;
	input   In_RstN   ;
	input   In_Tm     ;
	output  root_Clk  ;
	output  root_En   ;
	output  root_RstN ;
	output  root_Tm   ;

// synthesis translate_off
`define USER_BEHAV_GATES
// synthesis translate_on


`ifdef USER_BEHAV_GATES

	wire  u_0_root  ;
	reg   u_ec16    ;
	wire  Clk0      ;
	wire  Clk0_RstN ;
	reg   IntRstN   ;
	wire  LClk_Tm   ;
	assign u_0_root = In_Clk;
	assign root_Clk = u_0_root;
	assign root_En = 1'b1;
	assign Clk0 = In_Clk;
	assign Clk0_RstN = In_RstN;
	assign root_RstN = IntRstN;
	always @( posedge Clk0 or negedge Clk0_RstN )
		if ( ! Clk0_RstN )
			u_ec16 <= #0.001 ( 1'b0 );
		else	u_ec16 <= #0.001 ( 1'b1 );
	always @( posedge Clk0 or negedge Clk0_RstN )
		if ( ! Clk0_RstN )
			IntRstN <= #0.001 ( 1'b0 );
		else	IntRstN <= #0.001 ( u_ec16 );
	assign LClk_Tm = In_Tm;
	assign root_Tm = LClk_Tm;

`else
SDFFYRPQ3D_X2N_A6ZTR_C10 sdffy(.Q(root_RstN), .CK(In_Clk), .D(1'b1), .R(~In_RstN), .SE(1'b0), .SI(1'b0));
assign root_Clk = In_Clk;
assign root_En = 1'b1;
assign root_Tm = In_Tm;
`endif


endmodule


module ClockManager_Regime_cbus_Cm_center ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_cbus_Cm_east ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule


module ClockManager_Regime_bus_Cm_center ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule


module ClockManager_Regime_bus_Cm_east ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule


module ClockManager_Regime_bus_Cm_south ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule


module ClockManager_Regime_bus_Cm_west ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule



module ClockManager_Regime_cbus_ls_east_Cm( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_cbus_ls_west_Cm ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_cbus_south_Cm( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule


module ClockManager_Regime_cpu_Cm ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_dcluster0_Cm ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule


module ClockManager_Regime_dcluster1_Cm( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_dram0_Cm( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_dram1_Cm( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_dram2_Cm( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_dram3_Cm( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule


module ClockManager_Regime_pcie_Cm ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule

module ClockManager_Regime_peri_Cm ( input In_Clk , input In_RstN , input In_Tm , output root_Clk , output root_En , output root_RstN , output root_Tm );
    atom_bus_ClockManagerCell 
	u_atom_bus_ClockManagerCell(.In_Clk(In_Clk), .In_RstN(In_RstN), .In_Tm(In_Tm), .root_Clk(root_Clk), .root_En(root_En), .root_RstN(root_RstN), .root_Tm(root_Tm) );
endmodule






