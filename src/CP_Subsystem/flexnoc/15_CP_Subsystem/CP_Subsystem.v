//--------------------------------------------------------------------------------
//
//   Rebellions Inc. Confidential
//   All Rights Reserved -- Property of Rebellions Inc. 
//
//   Description : 
//
//   Author : jsyoon@rebellions.ai
//
//   date : 2021/08/07
//
//--------------------------------------------------------------------------------

module CP_Subsystem(



	input         TM                                                                    ,
	input         arstn_cpu                                                             ,
	input         clk_cpu                                                               ,

	input  [57:0] ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data                            ,
	output [2:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt                           ,
	output [1:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr                           ,
	input         ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck               ,
	output        ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]  ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt                           ,
	output [57:0] ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data              ,
	input  [2:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt             ,
	input  [1:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr             ,
	output        ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck ,
	input         ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]  ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt             ,

	output [128:0] r_dp_Link_c1_asi_to_Link_c1_ast_Data                            ,
	input  [2:0]   r_dp_Link_c1_asi_to_Link_c1_ast_RdCnt                           ,
	input  [2:0]   r_dp_Link_c1_asi_to_Link_c1_ast_RdPtr                           ,
	output         r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst                  ,
	input          r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck               ,
	input          r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst                  ,
	output         r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]   r_dp_Link_c1_asi_to_Link_c1_ast_WrCnt                           ,
	input  [273:0] r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data              ,
	output [2:0]   r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt             ,
	output [2:0]   r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr             ,
	input          r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst    ,
	output         r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck ,
	output         r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst    ,
	input          r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]   r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt             ,
	input  [128:0] r_dp_Link_c1t_asi_to_Link_c1t_ast_Data                          ,
	output [2:0]   r_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt                         ,
	output [2:0]   r_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr                         ,
	input          r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst                ,
	output         r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck             ,
	output         r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst                ,
	input          r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck             ,
	input  [2:0]   r_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt                         ,
	output [273:0] r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data                  ,
	input  [2:0]   r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt                 ,
	input  [2:0]   r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr                 ,
	output         r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst        ,
	input          r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck     ,
	input          r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst        ,
	output         r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck     ,
	output [2:0]   r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt                 ,

	output [270:0] w_dp_Link_c1_asi_to_Link_c1_ast_Data                            ,
	input  [2:0]   w_dp_Link_c1_asi_to_Link_c1_ast_RdCnt                           ,
	input  [2:0]   w_dp_Link_c1_asi_to_Link_c1_ast_RdPtr                           ,
	output         w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst                  ,
	input          w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck               ,
	input          w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst                  ,
	output         w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]   w_dp_Link_c1_asi_to_Link_c1_ast_WrCnt                           ,
	input  [125:0] w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data              ,
	output [2:0]   w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt             ,
	output [2:0]   w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr             ,
	input          w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst    ,
	output         w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck ,
	output         w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst    ,
	input          w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]   w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt             ,
	input  [270:0] w_dp_Link_c1t_asi_to_Link_c1t_ast_Data                          ,
	output [2:0]   w_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt                         ,
	output [2:0]   w_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr                         ,
	input          w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst                ,
	output         w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck             ,
	output         w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst                ,
	input          w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck             ,
	input  [2:0]   w_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt                         ,
	output [125:0] w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data                  ,
	input  [2:0]   w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt                 ,
	input  [2:0]   w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr                 ,
	output         w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst        ,
	input          w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck     ,
	input          w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst        ,
	output         w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck     ,
	output [2:0]   w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt                 




);


	cbus_Structure_Module_cpu u_cbus_Structure_Module_cpu(
		.TM( TM )
	,	.arstn_cpu( arstn_cpu )
	,	.clk_cpu( clk_cpu )
	,	.cpu_ctrl_Ar_Addr( cpu_ctrl_Ar_Addr )
	,	.cpu_ctrl_Ar_Burst( cpu_ctrl_Ar_Burst )
	,	.cpu_ctrl_Ar_Cache( cpu_ctrl_Ar_Cache )
	,	.cpu_ctrl_Ar_Id( cpu_ctrl_Ar_Id )
	,	.cpu_ctrl_Ar_Len( cpu_ctrl_Ar_Len )
	,	.cpu_ctrl_Ar_Lock( cpu_ctrl_Ar_Lock )
	,	.cpu_ctrl_Ar_Prot( cpu_ctrl_Ar_Prot )
	,	.cpu_ctrl_Ar_Ready( cpu_ctrl_Ar_Ready )
	,	.cpu_ctrl_Ar_Size( cpu_ctrl_Ar_Size )
	,	.cpu_ctrl_Ar_Valid( cpu_ctrl_Ar_Valid )
	,	.cpu_ctrl_Aw_Addr( cpu_ctrl_Aw_Addr )
	,	.cpu_ctrl_Aw_Burst( cpu_ctrl_Aw_Burst )
	,	.cpu_ctrl_Aw_Cache( cpu_ctrl_Aw_Cache )
	,	.cpu_ctrl_Aw_Id( cpu_ctrl_Aw_Id )
	,	.cpu_ctrl_Aw_Len( cpu_ctrl_Aw_Len )
	,	.cpu_ctrl_Aw_Lock( cpu_ctrl_Aw_Lock )
	,	.cpu_ctrl_Aw_Prot( cpu_ctrl_Aw_Prot )
	,	.cpu_ctrl_Aw_Ready( cpu_ctrl_Aw_Ready )
	,	.cpu_ctrl_Aw_Size( cpu_ctrl_Aw_Size )
	,	.cpu_ctrl_Aw_Valid( cpu_ctrl_Aw_Valid )
	,	.cpu_ctrl_B_Id( cpu_ctrl_B_Id )
	,	.cpu_ctrl_B_Ready( cpu_ctrl_B_Ready )
	,	.cpu_ctrl_B_Resp( cpu_ctrl_B_Resp )
	,	.cpu_ctrl_B_Valid( cpu_ctrl_B_Valid )
	,	.cpu_ctrl_R_Data( cpu_ctrl_R_Data )
	,	.cpu_ctrl_R_Id( cpu_ctrl_R_Id )
	,	.cpu_ctrl_R_Last( cpu_ctrl_R_Last )
	,	.cpu_ctrl_R_Ready( cpu_ctrl_R_Ready )
	,	.cpu_ctrl_R_Resp( cpu_ctrl_R_Resp )
	,	.cpu_ctrl_R_Valid( cpu_ctrl_R_Valid )
	,	.cpu_ctrl_W_Data( cpu_ctrl_W_Data )
	,	.cpu_ctrl_W_Last( cpu_ctrl_W_Last )
	,	.cpu_ctrl_W_Ready( cpu_ctrl_W_Ready )
	,	.cpu_ctrl_W_Strb( cpu_ctrl_W_Strb )
	,	.cpu_ctrl_W_Valid( cpu_ctrl_W_Valid )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_Data )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdCnt )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RdPtr )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt( ctrl_dp_Link_c1ctrl_asi_to_Link_c1ctrl_ast_WrCnt )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_Data )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdCnt )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RdPtr )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt( ctrl_dp_Link_c1ctrl_astResp001_to_Link_c1ctrl_asiResp001_WrCnt )
	);



	dbus_read_Structure_Module_cpu u_dbus_read_Structure_Module_cpu(
		.TM( TM )
	,	.arstn_cpu( arstn_cpu )
	,	.clk_cpu( clk_cpu )
	,	.cpu_r_Ar_Addr( cpu_r_Ar_Addr )
	,	.cpu_r_Ar_Burst( cpu_r_Ar_Burst )
	,	.cpu_r_Ar_Cache( cpu_r_Ar_Cache )
	,	.cpu_r_Ar_Id( cpu_r_Ar_Id )
	,	.cpu_r_Ar_Len( cpu_r_Ar_Len )
	,	.cpu_r_Ar_Lock( cpu_r_Ar_Lock )
	,	.cpu_r_Ar_Prot( cpu_r_Ar_Prot )
	,	.cpu_r_Ar_Ready( cpu_r_Ar_Ready )
	,	.cpu_r_Ar_Size( cpu_r_Ar_Size )
	,	.cpu_r_Ar_User( cpu_r_Ar_User )
	,	.cpu_r_Ar_Valid( cpu_r_Ar_Valid )
	,	.cpu_r_R_Data( cpu_r_R_Data )
	,	.cpu_r_R_Id( cpu_r_R_Id )
	,	.cpu_r_R_Last( cpu_r_R_Last )
	,	.cpu_r_R_Ready( cpu_r_R_Ready )
	,	.cpu_r_R_Resp( cpu_r_R_Resp )
	,	.cpu_r_R_User( cpu_r_R_User )
	,	.cpu_r_R_Valid( cpu_r_R_Valid )
	,	.dp_Link_c1_asi_to_Link_c1_ast_Data( r_dp_Link_c1_asi_to_Link_c1_ast_Data )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RdCnt( r_dp_Link_c1_asi_to_Link_c1_ast_RdCnt )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RdPtr( r_dp_Link_c1_asi_to_Link_c1_ast_RdPtr )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst( r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck( r_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst( r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck( r_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1_asi_to_Link_c1_ast_WrCnt( r_dp_Link_c1_asi_to_Link_c1_ast_WrCnt )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt( r_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_Data( r_dp_Link_c1t_asi_to_Link_c1t_ast_Data )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt( r_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr( r_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst( r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck( r_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst( r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck( r_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt( r_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt( r_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt )
	,	.pcie2cpu_acp_r_Ar_Addr( pcie2cpu_acp_r_Ar_Addr )
	,	.pcie2cpu_acp_r_Ar_Burst( pcie2cpu_acp_r_Ar_Burst )
	,	.pcie2cpu_acp_r_Ar_Cache( pcie2cpu_acp_r_Ar_Cache )
	,	.pcie2cpu_acp_r_Ar_Id( pcie2cpu_acp_r_Ar_Id )
	,	.pcie2cpu_acp_r_Ar_Len( pcie2cpu_acp_r_Ar_Len )
	,	.pcie2cpu_acp_r_Ar_Lock( pcie2cpu_acp_r_Ar_Lock )
	,	.pcie2cpu_acp_r_Ar_Prot( pcie2cpu_acp_r_Ar_Prot )
	,	.pcie2cpu_acp_r_Ar_Ready( pcie2cpu_acp_r_Ar_Ready )
	,	.pcie2cpu_acp_r_Ar_Size( pcie2cpu_acp_r_Ar_Size )
	,	.pcie2cpu_acp_r_Ar_User( pcie2cpu_acp_r_Ar_User )
	,	.pcie2cpu_acp_r_Ar_Valid( pcie2cpu_acp_r_Ar_Valid )
	,	.pcie2cpu_acp_r_R_Data( pcie2cpu_acp_r_R_Data )
	,	.pcie2cpu_acp_r_R_Id( pcie2cpu_acp_r_R_Id )
	,	.pcie2cpu_acp_r_R_Last( pcie2cpu_acp_r_R_Last )
	,	.pcie2cpu_acp_r_R_Ready( pcie2cpu_acp_r_R_Ready )
	,	.pcie2cpu_acp_r_R_Resp( pcie2cpu_acp_r_R_Resp )
	,	.pcie2cpu_acp_r_R_Valid( pcie2cpu_acp_r_R_Valid )
	);

	dbus_write_Structure_Module_cpu u_dbus_write_Structure_Module_cpu(
		.TM( TM )
	,	.arstn_cpu( arstn_cpu )
	,	.clk_cpu( clk_cpu )
	,	.cpu_w_Aw_Addr( cpu_w_Aw_Addr )
	,	.cpu_w_Aw_Burst( cpu_w_Aw_Burst )
	,	.cpu_w_Aw_Cache( cpu_w_Aw_Cache )
	,	.cpu_w_Aw_Id( cpu_w_Aw_Id )
	,	.cpu_w_Aw_Len( cpu_w_Aw_Len )
	,	.cpu_w_Aw_Lock( cpu_w_Aw_Lock )
	,	.cpu_w_Aw_Prot( cpu_w_Aw_Prot )
	,	.cpu_w_Aw_Ready( cpu_w_Aw_Ready )
	,	.cpu_w_Aw_Size( cpu_w_Aw_Size )
	,	.cpu_w_Aw_User( cpu_w_Aw_User )
	,	.cpu_w_Aw_Valid( cpu_w_Aw_Valid )
	,	.cpu_w_B_Id( cpu_w_B_Id )
	,	.cpu_w_B_Ready( cpu_w_B_Ready )
	,	.cpu_w_B_Resp( cpu_w_B_Resp )
	,	.cpu_w_B_User( cpu_w_B_User )
	,	.cpu_w_B_Valid( cpu_w_B_Valid )
	,	.cpu_w_W_Data( cpu_w_W_Data )
	,	.cpu_w_W_Last( cpu_w_W_Last )
	,	.cpu_w_W_Ready( cpu_w_W_Ready )
	,	.cpu_w_W_Strb( cpu_w_W_Strb )
	,	.cpu_w_W_Valid( cpu_w_W_Valid )
	,	.dp_Link_c1_asi_to_Link_c1_ast_Data( w_dp_Link_c1_asi_to_Link_c1_ast_Data )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RdCnt( w_dp_Link_c1_asi_to_Link_c1_ast_RdCnt )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RdPtr( w_dp_Link_c1_asi_to_Link_c1_ast_RdPtr )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst( w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck( w_dp_Link_c1_asi_to_Link_c1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst( w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck( w_dp_Link_c1_asi_to_Link_c1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1_asi_to_Link_c1_ast_WrCnt( w_dp_Link_c1_asi_to_Link_c1_ast_WrCnt )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_Data )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdCnt )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RdPtr )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt( w_dp_Link_c1_astResp001_to_Link_c1_asiResp001_WrCnt )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_Data( w_dp_Link_c1t_asi_to_Link_c1t_ast_Data )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt( w_dp_Link_c1t_asi_to_Link_c1t_ast_RdCnt )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr( w_dp_Link_c1t_asi_to_Link_c1t_ast_RdPtr )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst( w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck( w_dp_Link_c1t_asi_to_Link_c1t_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst( w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck( w_dp_Link_c1t_asi_to_Link_c1t_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt( w_dp_Link_c1t_asi_to_Link_c1t_ast_WrCnt )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_Data )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdCnt )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RdPtr )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRst )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_RxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRst )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_TxCtl_PwrOnRstAck )
	,	.dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt( w_dp_Link_c1t_astResp_to_Link_c1t_asiResp_WrCnt )
	,	.pcie2cpu_acp_w_Aw_Addr( pcie2cpu_acp_w_Aw_Addr )
	,	.pcie2cpu_acp_w_Aw_Burst( pcie2cpu_acp_w_Aw_Burst )
	,	.pcie2cpu_acp_w_Aw_Cache( pcie2cpu_acp_w_Aw_Cache )
	,	.pcie2cpu_acp_w_Aw_Id( pcie2cpu_acp_w_Aw_Id )
	,	.pcie2cpu_acp_w_Aw_Len( pcie2cpu_acp_w_Aw_Len )
	,	.pcie2cpu_acp_w_Aw_Lock( pcie2cpu_acp_w_Aw_Lock )
	,	.pcie2cpu_acp_w_Aw_Prot( pcie2cpu_acp_w_Aw_Prot )
	,	.pcie2cpu_acp_w_Aw_Ready( pcie2cpu_acp_w_Aw_Ready )
	,	.pcie2cpu_acp_w_Aw_Size( pcie2cpu_acp_w_Aw_Size )
	,	.pcie2cpu_acp_w_Aw_User( pcie2cpu_acp_w_Aw_User )
	,	.pcie2cpu_acp_w_Aw_Valid( pcie2cpu_acp_w_Aw_Valid )
	,	.pcie2cpu_acp_w_B_Id( pcie2cpu_acp_w_B_Id )
	,	.pcie2cpu_acp_w_B_Ready( pcie2cpu_acp_w_B_Ready )
	,	.pcie2cpu_acp_w_B_Resp( pcie2cpu_acp_w_B_Resp )
	,	.pcie2cpu_acp_w_B_Valid( pcie2cpu_acp_w_B_Valid )
	,	.pcie2cpu_acp_w_W_Data( pcie2cpu_acp_w_W_Data )
	,	.pcie2cpu_acp_w_W_Last( pcie2cpu_acp_w_W_Last )
	,	.pcie2cpu_acp_w_W_Ready( pcie2cpu_acp_w_W_Ready )
	,	.pcie2cpu_acp_w_W_Strb( pcie2cpu_acp_w_W_Strb )
	,	.pcie2cpu_acp_w_W_Valid( pcie2cpu_acp_w_W_Valid )
	);


endmodule
