
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:43

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_cpu_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module cbus_Structure_Module_cpu -severity waived -scheme reconvergence -from_signals Link_c1ctrl_astResp001_main.DtpTxClkAdapt_Link_c1ctrl_asiResp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_cpu -severity waived -scheme reconvergence -from_signals Link_c1ctrl_ast_main.DtpRxClkAdapt_Link_c1ctrl_asi_Async.RdCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module cbus_Structure_Module_cpu -severity waived -scheme multi_sync_mux_select -from Link_c1ctrl_ast_main.DtpRxClkAdapt_Link_c1ctrl_asi_Async.uRegSync.instSynchronizerCell*

endmodule
