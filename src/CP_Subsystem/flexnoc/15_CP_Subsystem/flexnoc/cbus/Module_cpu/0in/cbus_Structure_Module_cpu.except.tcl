
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:43

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_c1ctrl_astResp001_main.DtpTxClkAdapt_Link_c1ctrl_asiResp001_Async.WrCnt -graycode -module cbus_Structure_Module_cpu
cdc signal Link_c1ctrl_ast_main.DtpRxClkAdapt_Link_c1ctrl_asi_Async.RdCnt -graycode -module cbus_Structure_Module_cpu

