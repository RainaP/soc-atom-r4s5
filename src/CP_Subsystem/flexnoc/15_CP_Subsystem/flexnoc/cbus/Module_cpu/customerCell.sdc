
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:46

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_customer_cell -module "ClockManager_Regime_cpu_Cm" -type "ClockManagerCell" -instance_name "${CUSTOMER_HIERARCHY}Regime_cpu_Cm_main/ClockManager/IClockManager_Regime_cpu_Cm"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_astResp001_main/DtpTxClkAdapt_Link_c1ctrl_asiResp001_Async/urs/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_astResp001_main/DtpTxClkAdapt_Link_c1ctrl_asiResp001_Async/urs/Isc1"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_astResp001_main/DtpTxClkAdapt_Link_c1ctrl_asiResp001_Async/urs/Isc2"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_astResp001_main/DtpTxClkAdapt_Link_c1ctrl_asiResp001_Async/urs0/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_astResp001_main/DtpTxClkAdapt_Link_c1ctrl_asiResp001_Async/urs1/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_ast_main/DtpRxClkAdapt_Link_c1ctrl_asi_Async/urs/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_ast_main/DtpRxClkAdapt_Link_c1ctrl_asi_Async/urs/Isc1"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_ast_main/DtpRxClkAdapt_Link_c1ctrl_asi_Async/urs/Isc2"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_ast_main/DtpRxClkAdapt_Link_c1ctrl_asi_Async/urs0/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1ctrl_ast_main/DtpRxClkAdapt_Link_c1ctrl_asi_Async/urs1/Isc0"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_astResp001_main_Sys/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "[ expr 0.667 * $T_Flex2library ]"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c1ctrl_ast_main_Sys/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "[ expr 0.667 * $T_Flex2library ]"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_cpu_ctrl/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "[ expr 0.667 * $T_Flex2library ]"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_cpu_ctrl_T/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "[ expr 0.667 * $T_Flex2library ]"

