
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:02

////////////////////////////////////////////////////////////////////

module dbus_write_Structure_Module_cpu_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1_asi_main.DtpTxClkAdapt_Link_c1_ast_Async.RegData_0
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1_asi_main.DtpTxClkAdapt_Link_c1_ast_Async.RegData_1
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1_asi_main.DtpTxClkAdapt_Link_c1_ast_Async.RegData_2
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1_asi_main.DtpTxClkAdapt_Link_c1_ast_Async.RegData_3
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1_asi_main.DtpTxClkAdapt_Link_c1_ast_Async.RegData_4
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1_asi_main.DtpTxClkAdapt_Link_c1_ast_Async.RegData_5
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1t_astResp_main.DtpTxClkAdapt_Link_c1t_asiResp_Async.RegData_0
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1t_astResp_main.DtpTxClkAdapt_Link_c1t_asiResp_Async.RegData_1
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1t_astResp_main.DtpTxClkAdapt_Link_c1t_asiResp_Async.RegData_2
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1t_astResp_main.DtpTxClkAdapt_Link_c1t_asiResp_Async.RegData_3
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1t_astResp_main.DtpTxClkAdapt_Link_c1t_asiResp_Async.RegData_4
// 0in set_cdc_fifo -module dbus_write_Structure_Module_cpu Link_c1t_astResp_main.DtpTxClkAdapt_Link_c1t_asiResp_Async.RegData_5

endmodule
