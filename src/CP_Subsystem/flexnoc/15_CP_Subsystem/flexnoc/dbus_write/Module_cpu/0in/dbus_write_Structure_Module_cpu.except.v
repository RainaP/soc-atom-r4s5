
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:02

////////////////////////////////////////////////////////////////////

module dbus_write_Structure_Module_cpu_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_write_Structure_Module_cpu -severity waived -scheme reconvergence -from_signals Link_c1_asiResp001_main.DtpRxClkAdapt_Link_c1_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_cpu -severity waived -scheme reconvergence -from_signals Link_c1_asi_main.DtpTxClkAdapt_Link_c1_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_cpu -severity waived -scheme reconvergence -from_signals Link_c1t_astResp_main.DtpTxClkAdapt_Link_c1t_asiResp_Async.WrCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_cpu -severity waived -scheme reconvergence -from_signals Link_c1t_ast_main.DtpRxClkAdapt_Link_c1t_asi_Async.RdCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_write_Structure_Module_cpu -severity waived -scheme multi_sync_mux_select -from Link_c1_asiResp001_main.DtpRxClkAdapt_Link_c1_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_write_Structure_Module_cpu -severity waived -scheme multi_sync_mux_select -from Link_c1t_ast_main.DtpRxClkAdapt_Link_c1t_asi_Async.uRegSync.instSynchronizerCell*

endmodule
