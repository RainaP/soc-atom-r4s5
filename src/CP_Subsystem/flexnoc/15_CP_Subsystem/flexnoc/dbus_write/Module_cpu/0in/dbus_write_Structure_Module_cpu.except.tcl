
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:02

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_c1_asiResp001_main.DtpRxClkAdapt_Link_c1_astResp001_Async.RdCnt -graycode -module dbus_write_Structure_Module_cpu
cdc signal Link_c1_asi_main.DtpTxClkAdapt_Link_c1_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_cpu
cdc signal Link_c1t_astResp_main.DtpTxClkAdapt_Link_c1t_asiResp_Async.WrCnt -graycode -module dbus_write_Structure_Module_cpu
cdc signal Link_c1t_ast_main.DtpRxClkAdapt_Link_c1t_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_cpu

