
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:02

####################################################################

arteris_gen_reset -name "Regime_cpu_Cm_root" -clock_domain "clk_cpu" -clock "Regime_cpu_Cm_root" -spec_domain_clock "/Regime_cpu/Cm/root" -clock_period "${Regime_cpu_Cm_root_P}" -clock_waveform "[expr ${Regime_cpu_Cm_root_P}*0.00] [expr ${Regime_cpu_Cm_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_cpu_Cm_main/root_Clk_RstN" -lsb "" -msb ""
arteris_gen_reset -name "Regime_cpu_Cm_root" -clock_domain "clk_cpu" -clock "Regime_cpu_Cm_root" -spec_domain_clock "/Regime_cpu/Cm/root" -clock_period "${Regime_cpu_Cm_root_P}" -clock_waveform "[expr ${Regime_cpu_Cm_root_P}*0.00] [expr ${Regime_cpu_Cm_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_cpu_Cm_main/root_Clk_RstN" -lsb "" -msb ""
