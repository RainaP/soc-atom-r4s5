
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:02

####################################################################

arteris_gen_clock -name "Regime_cpu_Cm_root" -pin "Regime_cpu_Cm_main/root_Clk Regime_cpu_Cm_main/root_Clk_ClkS" -clock_domain "clk_cpu" -spec_domain_clock "/Regime_cpu/Cm/root" -divide_by "1" -source "clk_cpu" -source_period "${clk_cpu_P}" -source_waveform "[expr ${clk_cpu_P}*0.00] [expr ${clk_cpu_P}*0.50]" -user_directive "" -add "FALSE"
