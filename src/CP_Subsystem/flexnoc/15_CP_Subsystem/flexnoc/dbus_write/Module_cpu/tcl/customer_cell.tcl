
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:02

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_customer_cell -module "ClockManager_Regime_cpu_Cm" -type "ClockManagerCell" -instance_name "${CUSTOMER_HIERARCHY}Regime_cpu_Cm_main/ClockManager/IClockManager_Regime_cpu_Cm"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main/DtpRxClkAdapt_Link_c1_astResp001_Async/urs/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main/DtpRxClkAdapt_Link_c1_astResp001_Async/urs/Isc1"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main/DtpRxClkAdapt_Link_c1_astResp001_Async/urs/Isc2"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main/DtpRxClkAdapt_Link_c1_astResp001_Async/urs0/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main/DtpRxClkAdapt_Link_c1_astResp001_Async/urs1/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asi_main/DtpTxClkAdapt_Link_c1_ast_Async/urs/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asi_main/DtpTxClkAdapt_Link_c1_ast_Async/urs/Isc1"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asi_main/DtpTxClkAdapt_Link_c1_ast_Async/urs/Isc2"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asi_main/DtpTxClkAdapt_Link_c1_ast_Async/urs0/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1_asi_main/DtpTxClkAdapt_Link_c1_ast_Async/urs1/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_astResp_main/DtpTxClkAdapt_Link_c1t_asiResp_Async/urs/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_astResp_main/DtpTxClkAdapt_Link_c1t_asiResp_Async/urs/Isc1"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_astResp_main/DtpTxClkAdapt_Link_c1t_asiResp_Async/urs/Isc2"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_astResp_main/DtpTxClkAdapt_Link_c1t_asiResp_Async/urs0/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_astResp_main/DtpTxClkAdapt_Link_c1t_asiResp_Async/urs1/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_ast_main/DtpRxClkAdapt_Link_c1t_asi_Async/urs/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_ast_main/DtpRxClkAdapt_Link_c1t_asi_Async/urs/Isc1"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_ast_main/DtpRxClkAdapt_Link_c1t_asi_Async/urs/Isc2"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_ast_main/DtpRxClkAdapt_Link_c1t_asi_Async/urs0/Isc0"
arteris_customer_cell -module "VD_cpu_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c1t_ast_main/DtpRxClkAdapt_Link_c1t_asi_Async/urs1/Isc0"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asiResp001_main_Sys/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "${Regime_cpu_Cm_root_P}"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asi_main_Sys/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "${Regime_cpu_Cm_root_P}"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_astResp_main_Sys/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "${Regime_cpu_Cm_root_P}"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_ast_main_Sys/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "${Regime_cpu_Cm_root_P}"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_cpu_w/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "${Regime_cpu_Cm_root_P}"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_cpu_w_I/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "${Regime_cpu_Cm_root_P}"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_w/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "${Regime_cpu_Cm_root_P}"
arteris_customer_cell -module "VD_cpu_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_w_T/ClockGater/usceb8c4521c/instGaterCell" -clock "Regime_cpu_Cm_root" -clock_period "${Regime_cpu_Cm_root_P}"

