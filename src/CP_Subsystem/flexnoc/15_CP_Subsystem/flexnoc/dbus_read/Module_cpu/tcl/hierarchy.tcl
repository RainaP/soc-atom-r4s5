
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:09:33

####################################################################

arteris_hierarchy -module "dbus_read_Structure_Module_cpu_Link_c1_asiResp001_main" -generator "DatapathLink" -instance_name "Link_c1_asiResp001_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_Link_c1_asi_main" -generator "DatapathLink" -instance_name "Link_c1_asi_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_Link_c1t_astResp_main" -generator "DatapathLink" -instance_name "Link_c1t_astResp_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_Link_c1t_ast_main" -generator "DatapathLink" -instance_name "Link_c1t_ast_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_Regime_cpu_Cm_main" -generator "ClockManager" -instance_name "Regime_cpu_Cm_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_clockGaters_Link_c1_asiResp001_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c1_asiResp001_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_clockGaters_Link_c1_asi_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c1_asi_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_clockGaters_Link_c1t_astResp_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c1t_astResp_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_clockGaters_Link_c1t_ast_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c1t_ast_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_clockGaters_cpu_r" -generator "ClockGater" -instance_name "clockGaters_cpu_r" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_clockGaters_cpu_r_I" -generator "ClockGater" -instance_name "clockGaters_cpu_r_I" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_clockGaters_pcie2cpu_acp_r" -generator "ClockGater" -instance_name "clockGaters_pcie2cpu_acp_r" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_clockGaters_pcie2cpu_acp_r_T" -generator "ClockGater" -instance_name "clockGaters_pcie2cpu_acp_r_T" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_cpu_r_I_main" -generator "G2T" -instance_name "cpu_r_I_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_cpu_r_main" -generator "S2G" -instance_name "cpu_r_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_pcie2cpu_acp_r_T_main" -generator "T2G" -instance_name "pcie2cpu_acp_r_T_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_pcie2cpu_acp_r_main" -generator "G2S" -instance_name "pcie2cpu_acp_r_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_z_T_C_S_C_L_R_A_8_1" -generator "gate" -instance_name "uAnd8" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu_z_T_C_S_C_L_R_O_8_1" -generator "gate" -instance_name "uOr8" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_cpu" -generator "" -instance_name "dbus_read_Structure_Module_cpu" -level "1"
