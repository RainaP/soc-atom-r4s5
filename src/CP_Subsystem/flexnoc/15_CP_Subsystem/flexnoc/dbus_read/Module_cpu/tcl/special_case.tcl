
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:09:33

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_0_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_10_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_11_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_12_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_13_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_14_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_15_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_1_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_2_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_3_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_4_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_5_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_6_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_7_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_8_PndCnt" -lsb "0" -msb "4"
arteris_special -case "dont_touch" -instance_name "${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia/OrdCam_9_PndCnt" -lsb "0" -msb "4"
