if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main/DtpRxClkAdapt_Link_c1_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_asiResp001_main/DtpRxClkAdapt_Link_c1_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1_asi_main/DtpTxClkAdapt_Link_c1_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1_asi_main/DtpTxClkAdapt_Link_c1_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1t_astResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1t_astResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1t_astResp_main/DtpTxClkAdapt_Link_c1t_asiResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1t_astResp_main/DtpTxClkAdapt_Link_c1t_asiResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1t_ast_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1t_ast_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_c1t_ast_main/DtpRxClkAdapt_Link_c1t_asi_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_c1t_ast_main/DtpRxClkAdapt_Link_c1t_asi_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_cpu] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_cpu false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cpu_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cpu_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_cpu_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_cpu_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_astResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_astResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_astResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_astResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_ast_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_ast_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_ast_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_c1t_ast_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cpu_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cpu_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cpu_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cpu_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cpu_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cpu_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_cpu_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_cpu_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_pcie2cpu_acp_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/DtpTxSerAdapt_Link_c1_asi] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/DtpTxSerAdapt_Link_c1_asi false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}cpu_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}cpu_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/DtpRxSerAdapt_Link_c1t_ast] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/DtpRxSerAdapt_Link_c1t_ast false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}pcie2cpu_acp_r_main/GenericToSpecific false
}
