//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module SHM_Blackbox (
	 input           TM
	,input           CLK__AXI
	,input           RSTN__AXI

	,input  [ 23:0]  AXI4__CTRL__Ar_Addr
	,input  [  1:0]  AXI4__CTRL__Ar_Burst
	,input  [  3:0]  AXI4__CTRL__Ar_Cache
	,input  [  1:0]  AXI4__CTRL__Ar_Id
	,input  [  1:0]  AXI4__CTRL__Ar_Len
	,input           AXI4__CTRL__Ar_Lock
	,input  [  2:0]  AXI4__CTRL__Ar_Prot
	,output          AXI4__CTRL__Ar_Ready
	,input  [  2:0]  AXI4__CTRL__Ar_Size
	,input           AXI4__CTRL__Ar_Valid
	,input  [ 23:0]  AXI4__CTRL__Aw_Addr
	,input  [  1:0]  AXI4__CTRL__Aw_Burst
	,input  [  3:0]  AXI4__CTRL__Aw_Cache
	,input  [  1:0]  AXI4__CTRL__Aw_Id
	,input  [  1:0]  AXI4__CTRL__Aw_Len
	,input           AXI4__CTRL__Aw_Lock
	,input  [  2:0]  AXI4__CTRL__Aw_Prot
	,output          AXI4__CTRL__Aw_Ready
	,input  [  2:0]  AXI4__CTRL__Aw_Size
	,input           AXI4__CTRL__Aw_Valid
	,output [  1:0]  AXI4__CTRL__B_Id
	,input           AXI4__CTRL__B_Ready
	,output [  1:0]  AXI4__CTRL__B_Resp
	,output          AXI4__CTRL__B_Valid
	,output [ 31:0]  AXI4__CTRL__R_Data
	,output [  1:0]  AXI4__CTRL__R_Id
	,output          AXI4__CTRL__R_Last
	,input           AXI4__CTRL__R_Ready
	,output [  1:0]  AXI4__CTRL__R_Resp
	,output          AXI4__CTRL__R_Valid
	,input  [ 31:0]  AXI4__CTRL__W_Data
	,input           AXI4__CTRL__W_Last
	,output          AXI4__CTRL__W_Ready
	,input  [  3:0]  AXI4__CTRL__W_Strb
	,input           AXI4__CTRL__W_Valid

	,input  [  27:0] AXI4__DATA__Ar_Addr
	,input  [   1:0] AXI4__DATA__Ar_Burst
	,input  [   3:0] AXI4__DATA__Ar_Cache
	,input  [   4:0] AXI4__DATA__Ar_Id
	,input  [   4:0] AXI4__DATA__Ar_Len
	,input           AXI4__DATA__Ar_Lock
	,input  [   2:0] AXI4__DATA__Ar_Prot
	,output          AXI4__DATA__Ar_Ready
	,input  [   2:0] AXI4__DATA__Ar_Size
	,input  [   3:0] AXI4__DATA__Ar_User
	,input           AXI4__DATA__Ar_Valid
	,output [1023:0] AXI4__DATA__R_Data
	,output [   4:0] AXI4__DATA__R_Id
	,output          AXI4__DATA__R_Last
	,input           AXI4__DATA__R_Ready
	,output [   1:0] AXI4__DATA__R_Resp
	,output [   3:0] AXI4__DATA__R_User
	,output          AXI4__DATA__R_Valid
	,input  [  27:0] AXI4__DATA__Aw_Addr
	,input  [   1:0] AXI4__DATA__Aw_Burst
	,input  [   3:0] AXI4__DATA__Aw_Cache
	,input  [   4:0] AXI4__DATA__Aw_Id
	,input  [   4:0] AXI4__DATA__Aw_Len
	,input           AXI4__DATA__Aw_Lock
	,input  [   2:0] AXI4__DATA__Aw_Prot
	,output          AXI4__DATA__Aw_Ready
	,input  [   2:0] AXI4__DATA__Aw_Size
	,input  [   3:0] AXI4__DATA__Aw_User
	,input           AXI4__DATA__Aw_Valid
	,output [   4:0] AXI4__DATA__B_Id
	,input           AXI4__DATA__B_Ready
	,output [   1:0] AXI4__DATA__B_Resp
	,output [   3:0] AXI4__DATA__B_User
	,output          AXI4__DATA__B_Valid
	,input  [1023:0] AXI4__DATA__W_Data
	,input           AXI4__DATA__W_Last
	,output          AXI4__DATA__W_Ready
	,input  [ 127:0] AXI4__DATA__W_Strb
	,input           AXI4__DATA__W_Valid
);
	assign AXI4__CTRL__Ar_Ready = 1;
	assign AXI4__CTRL__Aw_Ready = 1;
	assign AXI4__CTRL__B_Id     = 0;
	assign AXI4__CTRL__B_Resp   = 0;
	assign AXI4__CTRL__B_Valid  = 0;
	assign AXI4__CTRL__R_Data   = 0;
	assign AXI4__CTRL__R_Id     = 0;
	assign AXI4__CTRL__R_Last   = 0;
	assign AXI4__CTRL__R_Resp   = 0;
	assign AXI4__CTRL__R_Valid  = 0;
	assign AXI4__CTRL__W_Ready  = 1;


	assign AXI4__DATA__Ar_Ready = 0;
	assign AXI4__DATA__R_Data   = 0;
	assign AXI4__DATA__R_Id     = 0;
	assign AXI4__DATA__R_Last   = 0;
	assign AXI4__DATA__R_Resp   = 0;
	assign AXI4__DATA__R_User   = 0;
	assign AXI4__DATA__R_Valid  = 0;
	assign AXI4__DATA__Aw_Ready = 0;
	assign AXI4__DATA__B_Id     = 0;
	assign AXI4__DATA__B_Resp   = 0;
	assign AXI4__DATA__B_User   = 0;
	assign AXI4__DATA__B_Valid  = 0;
	assign AXI4__DATA__W_Ready  = 0;

endmodule
