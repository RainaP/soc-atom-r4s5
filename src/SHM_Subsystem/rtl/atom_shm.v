module atom_shm

//*** (1) MODULE PARAMETERS
#(
		parameter W_AXI_ID_P    = 5
	,	parameter W_AXI_ADDR_P  = 25
	,	parameter W_AXI_USER_P  = 4
	,	parameter W_AXI_DATA_P  = 1024
	,	parameter PRJ_CODE_P    = 1
	,	parameter BLK_CODE_P    = 7
	,	parameter SLICE_NUM_P   = 0
	,	parameter RTL_VER_P     = 3
	,	parameter ARCH_VER_P    = 2
)

//*** (2) IO-DEFINITION
(
	//********* (2.0) GLOBAL -- CLOCK / RESET
	
		input	wire  arstn        // Async-Reset
	,	input	wire  clk          // Interconnect Clock --   1 GHz
	,	input	wire  clk_shmb_0   // (BG0) Bank-0 Clock -- 700 MHz
	,	input	wire  clk_shmb_1   // (BG0) Bank-1 Clock -- 700 MHz
	,	input	wire  clk_shmb_2   // (BG0) Bank-2 Clock -- 700 MHz
	,	input	wire  clk_shmb_3   // (BG0) Bank-3 Clock -- 700 MHz
	,	input	wire  clk_shmb_4   // (BG1) Bank-4 Clock -- 700 MHz
	,	input	wire  clk_shmb_5   // (BG1) Bank-5 Clock -- 700 MHz
	,	input	wire  clk_shmb_6   // (BG1) Bank-6 Clock -- 700 MHz
	,	input	wire  clk_shmb_7   // (BG1) Bank-7 Clock -- 700 MHz
	
	//********* (2.1) AXI-PORT -- MAIN-RW
	
	,	input	wire [W_AXI_ID_P-1:0]        awid      // (i)
	,	input	wire [W_AXI_ADDR_P-1:0]      awaddr    // (i)
	,	input	wire [7:0]                   awlen     // (i)
	,	input	wire [2:0]                   awsize    // (i)
	,	input	wire [1:0]                   awburst   // (i)
	,	input	wire                         awlock    // (i)
	,	input	wire [3:0]                   awcache   // (i)
	,	input	wire [2:0]                   awprot    // (i)
	,	input	wire [W_AXI_USER_P-1:0]      awuser    // (i)
	,	input	wire                         awvalid   // (i)
	,	output	wire                         awready   // (o)
	
	,	input	wire [W_AXI_DATA_P-1:0]      wdata     // (i)
	,	input	wire [(W_AXI_DATA_P/8)-1:0]  wstrb     // (i)
	,	input	wire                         wlast     // (i)
	,	input	wire [W_AXI_USER_P-1:0]      wuser     // (i)
	,	input	wire                         wvalid    // (i)
	,	output	wire                         wready    // (o)
	
	,	output	wire [W_AXI_ID_P-1:0]        bid       // (o)
	,	output	wire [1:0]                   bresp     // (o)
	,	output	wire [W_AXI_USER_P-1:0]      buser     // (o)
	,	output	wire                         bvalid    // (o)
	,	input	wire                         bready    // (i)
	
	,	input	wire [W_AXI_ID_P-1:0]        arid      // (i)
	,	input	wire [W_AXI_ADDR_P-1:0]      araddr    // (i)
	,	input	wire [7:0]                   arlen     // (i)
	,	input	wire [2:0]                   arsize    // (i)
	,	input	wire [1:0]                   arburst   // (i)
	,	input	wire                         arlock    // (i)
	,	input	wire [3:0]                   arcache   // (i)
	,	input	wire [2:0]                   arprot    // (i)
	,	input	wire [W_AXI_USER_P-1:0]      aruser    // (i)
	,	input	wire                         arvalid   // (i)
	,	output	wire                         arready   // (o)
	
	,	output	wire [W_AXI_ID_P-1:0]        rid       // (o)
	,	output	wire [W_AXI_DATA_P-1:0]      rdata     // (o)
	,	output	wire [1:0]                   rresp     // (o)
	,	output	wire                         rlast     // (o)
	,	output	wire [W_AXI_USER_P-1:0]      ruser     // (o)
	,	output	wire                         rvalid    // (o)
	,	input	wire                         rready    // (i)
	
	//********* (2.2) APB-PORT -- CSR REGISTER
	
	,	input	wire [31:0]  pwdata    // (i)
	,	input	wire         penable   // (i)
	,	input	wire         pwrite    // (i)
	,	input	wire [2:0]   pprot     // (i)
	,	input	wire         psel      // (i)
	,	input	wire [3:0]   pstrb     // (i)
	,	output	wire [31:0]  prdata    // (o)
	,	output	wire         pready    // (o)
	,	output	wire         pslverr   // (o)
	,	input	wire [11:0]  paddr     // (i)
);


	
	assign awready  = '1; // (o)
	assign wready   = '1; // (o)
	assign bid      = '0; // (o)
	assign bresp    = '0; // (o)
	assign buser    = '0; // (o)
	assign bvalid   = '0; // (o)
	assign arready  = '1; // (o)
	assign rid      = '0; // (o)
	assign rdata    = '0; // (o)
	assign rresp    = '0; // (o)
	assign rlast    = '0; // (o)
	assign ruser    = '0; // (o)
	assign rvalid   = '0; // (o)
	assign prdata   = '0; // (o)
	assign pready   = '1; // (o)
	assign pslverr  = '0; // (o)
endmodule
