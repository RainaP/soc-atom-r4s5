//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module SHM_PBUS
#(
      parameter ABITS = 38
)
(
     input              CLK__APB
    ,input              RSTN__APB

    ,input              APB__PSEL
    ,input              APB__PENABLE
    ,input              APB__PWRITE
    //,input  [2:0]       APB__PPROT
    ,input  [ABITS-1:0] APB__PADDR
    ,input  [31:0]      APB__PWDATA
    ,output             APB__PREADY
    ,output             APB__PSLVERR
    ,output [31:0]      APB__PRDATA

    ,output             SCU_0__APB__PSEL
    ,output             SCU_0__APB__PENABLE
    ,output             SCU_0__APB__PWRITE
    //,output [2:0]       SCU_0__APB__PPROT
    ,output [12-1:0]    SCU_0__APB__PADDR
    ,output [31:0]      SCU_0__APB__PWDATA
    ,input              SCU_0__APB__PREADY
    ,input              SCU_0__APB__PSLVERR
    ,input  [31:0]      SCU_0__APB__PRDATA

    ,output             SHM_0__APB__PSEL
    ,output             SHM_0__APB__PENABLE
    ,output             SHM_0__APB__PWRITE
    //,output [2:0]       SHM_0__APB__PPROT
    ,output [12-1:0]    SHM_0__APB__PADDR
    ,output [31:0]      SHM_0__APB__PWDATA
    ,input              SHM_0__APB__PREADY
    ,input              SHM_0__APB__PSLVERR
    ,input  [31:0]      SHM_0__APB__PRDATA

);
    localparam DECBIT = 12;
    localparam DECMASK= (1<<(15-12+1))-1;

    wire [2-1:0] psels;
    assign psels[  0] = ( ((('h0000)>>DECBIT)&DECMASK) == APB__PADDR[15:12]); // SCU_0
    assign psels[  1] = ( ((('h1000)>>DECBIT)&DECMASK) == APB__PADDR[15:12]); // SHM_0

    assign SCU_0__APB__PSEL    = APB__PSEL && psels[  0];
    assign SCU_0__APB__PENABLE = APB__PENABLE;
    assign SCU_0__APB__PWRITE  = APB__PWRITE ;
    //assign SCU_0__APB__PPROT   = APB__PPROT  ;
    assign SCU_0__APB__PADDR   = APB__PADDR  ;
    assign SCU_0__APB__PWDATA  = APB__PWDATA ;

    assign SHM_0__APB__PSEL    = APB__PSEL && psels[  1];
    assign SHM_0__APB__PENABLE = APB__PENABLE;
    assign SHM_0__APB__PWRITE  = APB__PWRITE ;
    //assign SHM_0__APB__PPROT   = APB__PPROT  ;
    assign SHM_0__APB__PADDR   = APB__PADDR  ;
    assign SHM_0__APB__PWDATA  = APB__PWDATA ;

    assign APB__PREADY = (0==APB__PSEL) 
        | (psels[  0] ? SCU_0__APB__PREADY : 0)
        | (psels[  1] ? SHM_0__APB__PREADY : 0)
        ;
    assign APB__PSLVERR = 0 
        | (psels[  0] ? SCU_0__APB__PSLVERR : 0)
        | (psels[  1] ? SHM_0__APB__PSLVERR : 0)
        ;
    assign APB__PRDATA = 0 
        | (psels[  0] ? SCU_0__APB__PRDATA : 0)
        | (psels[  1] ? SHM_0__APB__PRDATA : 0)
        ;

`ifndef SYNTHESIS
    always@(posedge CLK__APB or negedge RSTN__APB )
        if( RSTN__APB && (0!=APB__PSEL) && (0==psels) ) begin
            $display( "*E:%s(%d): (%m)", `__FILE__, `__LINE__,$time );
            $display( "*E:Invalid address : %x", APB__PADDR );
            #100;
            $finish();
        end
`endif //SYNTHESIS

endmodule
