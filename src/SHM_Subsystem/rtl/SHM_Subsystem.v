//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module SHM_Subsystem (
	 input           TEST_MODE
	,input           CLK__SHM
	,input           RSTN__SHM
	,input  [  23:0] AXI4__CTRL__ARADDR
	,input  [   1:0] AXI4__CTRL__ARBURST
	,input  [   3:0] AXI4__CTRL__ARCACHE
	,input  [   1:0] AXI4__CTRL__ARID
	,input  [   1:0] AXI4__CTRL__ARLEN
	,input           AXI4__CTRL__ARLOCK
	,input  [   2:0] AXI4__CTRL__ARPROT
	,output          AXI4__CTRL__ARREADY
	,input  [   2:0] AXI4__CTRL__ARSIZE
	,input           AXI4__CTRL__ARVALID
	,input  [  23:0] AXI4__CTRL__AWADDR
	,input  [   1:0] AXI4__CTRL__AWBURST
	,input  [   3:0] AXI4__CTRL__AWCACHE
	,input  [   1:0] AXI4__CTRL__AWID
	,input  [   1:0] AXI4__CTRL__AWLEN
	,input           AXI4__CTRL__AWLOCK
	,input  [   2:0] AXI4__CTRL__AWPROT
	,output          AXI4__CTRL__AWREADY
	,input  [   2:0] AXI4__CTRL__AWSIZE
	,input           AXI4__CTRL__AWVALID
	,output [   1:0] AXI4__CTRL__BID
	,input           AXI4__CTRL__BREADY
	,output [   1:0] AXI4__CTRL__BRESP
	,output          AXI4__CTRL__BVALID
	,output [  31:0] AXI4__CTRL__RDATA
	,output [   1:0] AXI4__CTRL__RID
	,output          AXI4__CTRL__RLAST
	,input           AXI4__CTRL__RREADY
	,output [   1:0] AXI4__CTRL__RRESP
	,output          AXI4__CTRL__RVALID
	,input  [  31:0] AXI4__CTRL__WDATA
	,input           AXI4__CTRL__WLAST
	,output          AXI4__CTRL__WREADY
	,input  [   3:0] AXI4__CTRL__WSTRB
	,input           AXI4__CTRL__WVALID
	,input  [  27:0] AXI4__DATA__ARADDR
	,input  [   1:0] AXI4__DATA__ARBURST
	,input  [   3:0] AXI4__DATA__ARCACHE
	,input  [   4:0] AXI4__DATA__ARID
	,input  [   4:0] AXI4__DATA__ARLEN
	,input           AXI4__DATA__ARLOCK
	,input  [   2:0] AXI4__DATA__ARPROT
	,output          AXI4__DATA__ARREADY
	,input  [   2:0] AXI4__DATA__ARSIZE
	,input  [   3:0] AXI4__DATA__ARUSER
	,input           AXI4__DATA__ARVALID
	,output [1023:0] AXI4__DATA__RDATA
	,output [   4:0] AXI4__DATA__RID
	,output          AXI4__DATA__RLAST
	,input           AXI4__DATA__RREADY
	,output [   1:0] AXI4__DATA__RRESP
	,output [   3:0] AXI4__DATA__RUSER
	,output          AXI4__DATA__RVALID
	,input  [  27:0] AXI4__DATA__AWADDR
	,input  [   1:0] AXI4__DATA__AWBURST
	,input  [   3:0] AXI4__DATA__AWCACHE
	,input  [   4:0] AXI4__DATA__AWID
	,input  [   4:0] AXI4__DATA__AWLEN
	,input           AXI4__DATA__AWLOCK
	,input  [   2:0] AXI4__DATA__AWPROT
	,output          AXI4__DATA__AWREADY
	,input  [   2:0] AXI4__DATA__AWSIZE
	,input  [   3:0] AXI4__DATA__AWUSER
	,input           AXI4__DATA__AWVALID
	,output [   4:0] AXI4__DATA__BID
	,input           AXI4__DATA__BREADY
	,output [   1:0] AXI4__DATA__BRESP
	,output [   3:0] AXI4__DATA__BUSER
	,output          AXI4__DATA__BVALID
	,input  [1023:0] AXI4__DATA__WDATA
	,input           AXI4__DATA__WLAST
	,output          AXI4__DATA__WREADY
	,input  [ 127:0] AXI4__DATA__WSTRB
	,input           AXI4__DATA__WVALID
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_SHM_Subsystem
	assign AXI4__CTRL__ARREADY = 1'h1;
	assign AXI4__CTRL__AWREADY = 1'h1;
	assign AXI4__CTRL__BID = 2'h0;
	assign AXI4__CTRL__BRESP = 2'h0;
	assign AXI4__CTRL__BVALID = 1'h0;
	assign AXI4__CTRL__RDATA = 32'h0;
	assign AXI4__CTRL__RID = 2'h0;
	assign AXI4__CTRL__RLAST = 1'h0;
	assign AXI4__CTRL__RRESP = 2'h0;
	assign AXI4__CTRL__RVALID = 1'h0;
	assign AXI4__CTRL__WREADY = 1'h1;
	assign AXI4__DATA__ARREADY = 1'h1;
	assign AXI4__DATA__RDATA = 1024'h0;
	assign AXI4__DATA__RID = 5'h0;
	assign AXI4__DATA__RLAST = 1'h0;
	assign AXI4__DATA__RRESP = 2'h0;
	assign AXI4__DATA__RUSER = 4'h0;
	assign AXI4__DATA__RVALID = 1'h0;
	assign AXI4__DATA__AWREADY = 1'h1;
	assign AXI4__DATA__BID = 5'h0;
	assign AXI4__DATA__BRESP = 2'h0;
	assign AXI4__DATA__BUSER = 4'h0;
	assign AXI4__DATA__BVALID = 1'h0;
	assign AXI4__DATA__WREADY = 1'h1;
`else // SEMIFIVE_MAKE_BLACKBOX_SHM_Subsystem
	SHM_Blackbox
	SHM_0 (
		 .TM                  (TEST_MODE)
		,.CLK__AXI            (CLK__SHM)
		,.RSTN__AXI           (RSTN__SHM)
		,.AXI4__CTRL__Ar_Addr (AXI4__CTRL__ARADDR)
		,.AXI4__CTRL__Ar_Burst(AXI4__CTRL__ARBURST)
		,.AXI4__CTRL__Ar_Cache(AXI4__CTRL__ARCACHE)
		,.AXI4__CTRL__Ar_Id   (AXI4__CTRL__ARID)
		,.AXI4__CTRL__Ar_Len  (AXI4__CTRL__ARLEN)
		,.AXI4__CTRL__Ar_Lock (AXI4__CTRL__ARLOCK)
		,.AXI4__CTRL__Ar_Prot (AXI4__CTRL__ARPROT)
		,.AXI4__CTRL__Ar_Ready(AXI4__CTRL__ARREADY)
		,.AXI4__CTRL__Ar_Size (AXI4__CTRL__ARSIZE)
		,.AXI4__CTRL__Ar_Valid(AXI4__CTRL__ARVALID)
		,.AXI4__CTRL__Aw_Addr (AXI4__CTRL__AWADDR)
		,.AXI4__CTRL__Aw_Burst(AXI4__CTRL__AWBURST)
		,.AXI4__CTRL__Aw_Cache(AXI4__CTRL__AWCACHE)
		,.AXI4__CTRL__Aw_Id   (AXI4__CTRL__AWID)
		,.AXI4__CTRL__Aw_Len  (AXI4__CTRL__AWLEN)
		,.AXI4__CTRL__Aw_Lock (AXI4__CTRL__AWLOCK)
		,.AXI4__CTRL__Aw_Prot (AXI4__CTRL__AWPROT)
		,.AXI4__CTRL__Aw_Ready(AXI4__CTRL__AWREADY)
		,.AXI4__CTRL__Aw_Size (AXI4__CTRL__AWSIZE)
		,.AXI4__CTRL__Aw_Valid(AXI4__CTRL__AWVALID)
		,.AXI4__CTRL__B_Id    (AXI4__CTRL__BID)
		,.AXI4__CTRL__B_Ready (AXI4__CTRL__BREADY)
		,.AXI4__CTRL__B_Resp  (AXI4__CTRL__BRESP)
		,.AXI4__CTRL__B_Valid (AXI4__CTRL__BVALID)
		,.AXI4__CTRL__R_Data  (AXI4__CTRL__RDATA)
		,.AXI4__CTRL__R_Id    (AXI4__CTRL__RID)
		,.AXI4__CTRL__R_Last  (AXI4__CTRL__RLAST)
		,.AXI4__CTRL__R_Ready (AXI4__CTRL__RREADY)
		,.AXI4__CTRL__R_Resp  (AXI4__CTRL__RRESP)
		,.AXI4__CTRL__R_Valid (AXI4__CTRL__RVALID)
		,.AXI4__CTRL__W_Data  (AXI4__CTRL__WDATA)
		,.AXI4__CTRL__W_Last  (AXI4__CTRL__WLAST)
		,.AXI4__CTRL__W_Ready (AXI4__CTRL__WREADY)
		,.AXI4__CTRL__W_Strb  (AXI4__CTRL__WSTRB)
		,.AXI4__CTRL__W_Valid (AXI4__CTRL__WVALID)
		,.AXI4__DATA__Ar_Addr (AXI4__DATA__ARADDR)
		,.AXI4__DATA__Ar_Burst(AXI4__DATA__ARBURST)
		,.AXI4__DATA__Ar_Cache(AXI4__DATA__ARCACHE)
		,.AXI4__DATA__Ar_Id   (AXI4__DATA__ARID)
		,.AXI4__DATA__Ar_Len  (AXI4__DATA__ARLEN)
		,.AXI4__DATA__Ar_Lock (AXI4__DATA__ARLOCK)
		,.AXI4__DATA__Ar_Prot (AXI4__DATA__ARPROT)
		,.AXI4__DATA__Ar_Ready(AXI4__DATA__ARREADY)
		,.AXI4__DATA__Ar_Size (AXI4__DATA__ARSIZE)
		,.AXI4__DATA__Ar_User (AXI4__DATA__ARUSER)
		,.AXI4__DATA__Ar_Valid(AXI4__DATA__ARVALID)
		,.AXI4__DATA__R_Data  (AXI4__DATA__RDATA)
		,.AXI4__DATA__R_Id    (AXI4__DATA__RID)
		,.AXI4__DATA__R_Last  (AXI4__DATA__RLAST)
		,.AXI4__DATA__R_Ready (AXI4__DATA__RREADY)
		,.AXI4__DATA__R_Resp  (AXI4__DATA__RRESP)
		,.AXI4__DATA__R_User  (AXI4__DATA__RUSER)
		,.AXI4__DATA__R_Valid (AXI4__DATA__RVALID)
		,.AXI4__DATA__Aw_Addr (AXI4__DATA__AWADDR)
		,.AXI4__DATA__Aw_Burst(AXI4__DATA__AWBURST)
		,.AXI4__DATA__Aw_Cache(AXI4__DATA__AWCACHE)
		,.AXI4__DATA__Aw_Id   (AXI4__DATA__AWID)
		,.AXI4__DATA__Aw_Len  (AXI4__DATA__AWLEN)
		,.AXI4__DATA__Aw_Lock (AXI4__DATA__AWLOCK)
		,.AXI4__DATA__Aw_Prot (AXI4__DATA__AWPROT)
		,.AXI4__DATA__Aw_Ready(AXI4__DATA__AWREADY)
		,.AXI4__DATA__Aw_Size (AXI4__DATA__AWSIZE)
		,.AXI4__DATA__Aw_User (AXI4__DATA__AWUSER)
		,.AXI4__DATA__Aw_Valid(AXI4__DATA__AWVALID)
		,.AXI4__DATA__B_Id    (AXI4__DATA__BID)
		,.AXI4__DATA__B_Ready (AXI4__DATA__BREADY)
		,.AXI4__DATA__B_Resp  (AXI4__DATA__BRESP)
		,.AXI4__DATA__B_User  (AXI4__DATA__BUSER)
		,.AXI4__DATA__B_Valid (AXI4__DATA__BVALID)
		,.AXI4__DATA__W_Data  (AXI4__DATA__WDATA)
		,.AXI4__DATA__W_Last  (AXI4__DATA__WLAST)
		,.AXI4__DATA__W_Ready (AXI4__DATA__WREADY)
		,.AXI4__DATA__W_Strb  (AXI4__DATA__WSTRB)
		,.AXI4__DATA__W_Valid (AXI4__DATA__WVALID)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_SHM_Subsystem
endmodule 
