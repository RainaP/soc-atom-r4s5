// This file is generated from Magillem
// Generation : Wed Aug 11 13:20:10 KST 2021 by miock from project atom
// Component : rebellions atom shm_subsys 0.0
// Design : rebellions atom shm_subsys_arch 0.0
//  u_shm            rebellions  atom    shm            0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_shm_0.0.xml 
//  u_axi2apb_bridge arteris.com FLEXNOC axi2apb_bridge 4.5.2 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/axi2apb_bridge.xml 
// Magillem Release : 5.2021.1


module shm_subsys(
   input  wire          TM,
   input  wire          aclk,
   input  wire          arstn,
   input  wire          clk,
   input  wire [24:0]   shm_r_araddr,     // (i) 
   input  wire [1:0]    shm_r_arburst,    // (i) 
   input  wire [3:0]    shm_r_arcache,    // (i) 
   input  wire [4:0]    shm_r_arid,       // (i) 
   input  wire [7:0]    shm_r_arlen,      // (i) 
   input  wire          shm_r_arlock,     // (i) 
   input  wire [2:0]    shm_r_arprot,     // (i) 
   output wire          shm_r_arready,    // (o) 
   input  wire [2:0]    shm_r_arsize,     // (i) 
   input  wire [3:0]    shm_r_aruser,     // (i) 
   input  wire          shm_r_arvalid,    // (i) 
   output wire [1023:0] shm_r_rdata,      // (o) 
   output wire [4:0]    shm_r_rid,        // (o) 
   output wire          shm_r_rlast,      // (o) 
   input  wire          shm_r_rready,     // (i) 
   output wire [1:0]    shm_r_rresp,      // (o) 
   output wire [3:0]    shm_r_ruser,      // (o) 
   output wire          shm_r_rvalid,     // (o) 
   input  wire [24:0]   shm_w_awaddr,     // (i) 
   input  wire [1:0]    shm_w_awburst,    // (i) 
   input  wire [3:0]    shm_w_awcache,    // (i) 
   input  wire [4:0]    shm_w_awid,       // (i) 
   input  wire [7:0]    shm_w_awlen,      // (i) 
   input  wire          shm_w_awlock,     // (i) 
   input  wire [2:0]    shm_w_awprot,     // (i) 
   output wire          shm_w_awready,    // (o) 
   input  wire [2:0]    shm_w_awsize,     // (i) 
   input  wire [3:0]    shm_w_awuser,     // (i) 
   input  wire          shm_w_awvalid,    // (i) 
   output wire [4:0]    shm_w_bid,        // (o) 
   input  wire          shm_w_bready,     // (i) 
   output wire [1:0]    shm_w_bresp,      // (o) 
   output wire [3:0]    shm_w_buser,      // (o) 
   output wire          shm_w_bvalid,     // (o) 
   input  wire [1023:0] shm_w_wdata,      // (i) 
   input  wire          shm_w_wlast,      // (i) 
   output wire          shm_w_wready,     // (o) 
   input  wire [127:0]  shm_w_wstrb,      // (i) 
   input  wire [3:0]    shm_w_wuser,      // (i) 
   input  wire          shm_w_wvalid,     // (i) 
   output wire          shm_ctrl_rlast,
   output wire [1:0]    shm_ctrl_rresp,
   output wire          shm_ctrl_rvalid,
   input  wire          shm_ctrl_rready,
   output wire [31:0]   shm_ctrl_rdata,
   output wire [3:0]    shm_ctrl_rid,
   input  wire          shm_ctrl_bready,
   output wire [1:0]    shm_ctrl_bresp,
   output wire          shm_ctrl_bvalid,
   output wire [3:0]    shm_ctrl_bid,
   input  wire [2:0]    shm_ctrl_awprot,
   input  wire [23:0]   shm_ctrl_awaddr,
   input  wire [1:0]    shm_ctrl_awburst,
   input  wire          shm_ctrl_awlock,
   input  wire [3:0]    shm_ctrl_awcache,
   input  wire [1:0]    shm_ctrl_awlen,
   input  wire          shm_ctrl_awvalid,
   output wire          shm_ctrl_awready,
   input  wire [3:0]    shm_ctrl_awid,
   input  wire [2:0]    shm_ctrl_awsize,
   input  wire          shm_ctrl_wlast,
   input  wire          shm_ctrl_wvalid,
   output wire          shm_ctrl_wready,
   input  wire [3:0]    shm_ctrl_wstrb,
   input  wire [31:0]   shm_ctrl_wdata,
   input  wire [2:0]    shm_ctrl_arprot,
   input  wire [23:0]   shm_ctrl_araddr,
   input  wire [1:0]    shm_ctrl_arburst,
   input  wire          shm_ctrl_arlock,
   input  wire [3:0]    shm_ctrl_arcache,
   input  wire [1:0]    shm_ctrl_arlen,
   input  wire          shm_ctrl_arvalid,
   output wire          shm_ctrl_arready,
   input  wire [3:0]    shm_ctrl_arid,
   input  wire [2:0]    shm_ctrl_arsize 
);



wire  [ 31 : 0 ] prdata;
wire             pready;
wire             pslverr;
wire  [ 31 : 0 ] apb_mst0_PWData;
wire             apb_mst0_PWrite;
wire             apb_mst0_PSel;
wire  [ 3  : 0 ] apb_mst0_PStrb;
wire             apb_mst0_PEnable;
wire  [ 23 : 0 ] apb_mst0_PAddr;
wire  [ 2  : 0 ] apb_mst0_PProt;
wire  [ 31 : 0 ] apb_mst1_PWData;
wire             apb_mst1_PWrite;
wire             apb_mst1_PSel;
wire  [ 3  : 0 ] apb_mst1_PStrb;
wire             apb_mst1_PEnable;
wire  [ 23 : 0 ] apb_mst1_PAddr;
wire  [ 2  : 0 ] apb_mst1_PProt;
wire  [ 31 : 0 ] apb_mst2_PWData;
wire             apb_mst2_PWrite;
wire             apb_mst2_PSel;
wire  [ 3  : 0 ] apb_mst2_PStrb;
wire             apb_mst2_PEnable;
wire  [ 23 : 0 ] apb_mst2_PAddr;
wire  [ 2  : 0 ] apb_mst2_PProt;
wire  [ 31 : 0 ] apb_mst3_PWData;
wire             apb_mst3_PWrite;
wire             apb_mst3_PSel;
wire  [ 3  : 0 ] apb_mst3_PStrb;
wire             apb_mst3_PEnable;
wire  [ 23 : 0 ] apb_mst3_PAddr;
wire  [ 2  : 0 ] apb_mst3_PProt;
wire  [ 31 : 0 ] apb_mst4_PWData;
wire             apb_mst4_PWrite;
wire             apb_mst4_PSel;
wire  [ 3  : 0 ] apb_mst4_PStrb;
wire             apb_mst4_PEnable;
wire  [ 23 : 0 ] apb_mst4_PAddr;
wire  [ 2  : 0 ] apb_mst4_PProt;
wire  [ 31 : 0 ] apb_mst5_PWData;
wire             apb_mst5_PWrite;
wire             apb_mst5_PSel;
wire  [ 3  : 0 ] apb_mst5_PStrb;
wire             apb_mst5_PEnable;
wire  [ 23 : 0 ] apb_mst5_PAddr;
wire  [ 2  : 0 ] apb_mst5_PProt;
wire  [ 31 : 0 ] apb_mst6_PWData;
wire             apb_mst6_PWrite;
wire             apb_mst6_PSel;
wire  [ 3  : 0 ] apb_mst6_PStrb;
wire             apb_mst6_PEnable;
wire  [ 23 : 0 ] apb_mst6_PAddr;
wire  [ 2  : 0 ] apb_mst6_PProt;
wire  [ 31 : 0 ] apb_mst7_PWData;
wire             apb_mst7_PWrite;
wire             apb_mst7_PSel;
wire  [ 3  : 0 ] apb_mst7_PStrb;
wire             apb_mst7_PEnable;
wire  [ 23 : 0 ] apb_mst7_PAddr;
wire  [ 2  : 0 ] apb_mst7_PProt;

atom_shm #(
      .W_AXI_ID_P   (5),
      .W_AXI_ADDR_P (25),
      .W_AXI_USER_P (4),
      .W_AXI_DATA_P (1024),
      .PRJ_CODE_P   (1),
      .BLK_CODE_P   (7),
      .SLICE_NUM_P  (0),
      .RTL_VER_P    (3),
      .ARCH_VER_P   (2) 
      ) 
u_shm (
      .arstn(         arstn                ),
      .clk(           aclk                 ),
      .clk_shmb_0(    clk                  ),
      .clk_shmb_1(    clk                  ),
      .clk_shmb_2(    clk                  ),
      .clk_shmb_3(    clk                  ),
      .clk_shmb_4(    clk                  ),
      .clk_shmb_5(    clk                  ),
      .clk_shmb_6(    clk                  ),
      .clk_shmb_7(    clk                  ),
      .awid(          shm_w_awid           ),
      .awaddr(        shm_w_awaddr         ),
      .awlen(         shm_w_awlen          ),
      .awsize(        shm_w_awsize         ),
      .awburst(       shm_w_awburst        ),
      .awlock(        shm_w_awlock         ),
      .awcache(       shm_w_awcache        ),
      .awprot(        shm_w_awprot         ),
      .awuser(        shm_w_awuser         ),
      .awvalid(       shm_w_awvalid        ),
      .awready(       shm_w_awready        ),
      .wdata(         shm_w_wdata          ),
      .wstrb(         shm_w_wstrb          ),
      .wlast(         shm_w_wlast          ),
      .wuser(         shm_w_wuser          ),
      .wvalid(        shm_w_wvalid         ),
      .wready(        shm_w_wready         ),
      .bid(           shm_w_bid            ),
      .bresp(         shm_w_bresp          ),
      .buser(         shm_w_buser          ),
      .bvalid(        shm_w_bvalid         ),
      .bready(        shm_w_bready         ),
      .arid(          shm_r_arid           ),
      .araddr(        shm_r_araddr         ),
      .arlen(         shm_r_arlen          ),
      .arsize(        shm_r_arsize         ),
      .arburst(       shm_r_arburst        ),
      .arlock(        shm_r_arlock         ),
      .arcache(       shm_r_arcache        ),
      .arprot(        shm_r_arprot         ),
      .aruser(        shm_r_aruser         ),
      .arvalid(       shm_r_arvalid        ),
      .arready(       shm_r_arready        ),
      .rid(           shm_r_rid            ),
      .rdata(         shm_r_rdata          ),
      .rresp(         shm_r_rresp          ),
      .rlast(         shm_r_rlast          ),
      .ruser(         shm_r_ruser          ),
      .rvalid(        shm_r_rvalid         ),
      .rready(        shm_r_rready         ),
      .pwdata(        apb_mst0_PWData      ),
      .penable(       apb_mst0_PEnable     ),
      .pwrite(        apb_mst0_PWrite      ),
      .pprot(         apb_mst0_PProt       ),
      .psel(          apb_mst0_PSel        ),
      .pstrb(         apb_mst0_PStrb       ),
      .prdata(        prdata               ),
      .pready(        pready               ),
      .pslverr(       pslverr              ),
      .paddr(         apb_mst0_PAddr[11:0] ) 
      );


axi2apb_bridge u_axi2apb_bridge (
      .TM(                   TM               ),
      .apb_mst0_PWData(      apb_mst0_PWData  ),
      .apb_mst0_PWrite(      apb_mst0_PWrite  ),
      .apb_mst0_PRData(      prdata           ),
      .apb_mst0_PReady(      pready           ),
      .apb_mst0_PSel(        apb_mst0_PSel    ),
      .apb_mst0_PSlvErr(     pslverr          ),
      .apb_mst0_PStrb(       apb_mst0_PStrb   ),
      .apb_mst0_PEnable(     apb_mst0_PEnable ),
      .apb_mst0_PAddr(       apb_mst0_PAddr   ),
      .apb_mst0_PProt(       apb_mst0_PProt   ),
      .arstn(                arstn            ),
      .clk(                  aclk             ),
      .apb_mst1_PWData(      apb_mst1_PWData  ),
      .apb_mst1_PWrite(      apb_mst1_PWrite  ),
      .apb_mst1_PRData(                       ),
      .apb_mst1_PReady(      1'b1             ),
      .apb_mst1_PSel(        apb_mst1_PSel    ),
      .apb_mst1_PSlvErr(     1'b0             ),
      .apb_mst1_PStrb(       apb_mst1_PStrb   ),
      .apb_mst1_PEnable(     apb_mst1_PEnable ),
      .apb_mst1_PAddr(       apb_mst1_PAddr   ),
      .apb_mst1_PProt(       apb_mst1_PProt   ),
      .apb_mst2_PWData(      apb_mst2_PWData  ),
      .apb_mst2_PWrite(      apb_mst2_PWrite  ),
      .apb_mst2_PRData(                       ),
      .apb_mst2_PReady(      1'b1             ),
      .apb_mst2_PSel(        apb_mst2_PSel    ),
      .apb_mst2_PSlvErr(     1'b0             ),
      .apb_mst2_PStrb(       apb_mst2_PStrb   ),
      .apb_mst2_PEnable(     apb_mst2_PEnable ),
      .apb_mst2_PAddr(       apb_mst2_PAddr   ),
      .apb_mst2_PProt(       apb_mst2_PProt   ),
      .apb_mst3_PWData(      apb_mst3_PWData  ),
      .apb_mst3_PWrite(      apb_mst3_PWrite  ),
      .apb_mst3_PRData(                       ),
      .apb_mst3_PReady(      1'b1             ),
      .apb_mst3_PSel(        apb_mst3_PSel    ),
      .apb_mst3_PSlvErr(     1'b0             ),
      .apb_mst3_PStrb(       apb_mst3_PStrb   ),
      .apb_mst3_PEnable(     apb_mst3_PEnable ),
      .apb_mst3_PAddr(       apb_mst3_PAddr   ),
      .apb_mst3_PProt(       apb_mst3_PProt   ),
      .apb_mst4_PWData(      apb_mst4_PWData  ),
      .apb_mst4_PWrite(      apb_mst4_PWrite  ),
      .apb_mst4_PRData(                       ),
      .apb_mst4_PReady(      1'b1             ),
      .apb_mst4_PSel(        apb_mst4_PSel    ),
      .apb_mst4_PSlvErr(     1'b0             ),
      .apb_mst4_PStrb(       apb_mst4_PStrb   ),
      .apb_mst4_PEnable(     apb_mst4_PEnable ),
      .apb_mst4_PAddr(       apb_mst4_PAddr   ),
      .apb_mst4_PProt(       apb_mst4_PProt   ),
      .apb_mst5_PWData(      apb_mst5_PWData  ),
      .apb_mst5_PWrite(      apb_mst5_PWrite  ),
      .apb_mst5_PRData(                       ),
      .apb_mst5_PReady(      1'b1             ),
      .apb_mst5_PSel(        apb_mst5_PSel    ),
      .apb_mst5_PSlvErr(     1'b0             ),
      .apb_mst5_PStrb(       apb_mst5_PStrb   ),
      .apb_mst5_PEnable(     apb_mst5_PEnable ),
      .apb_mst5_PAddr(       apb_mst5_PAddr   ),
      .apb_mst5_PProt(       apb_mst5_PProt   ),
      .apb_mst6_PWData(      apb_mst6_PWData  ),
      .apb_mst6_PWrite(      apb_mst6_PWrite  ),
      .apb_mst6_PRData(                       ),
      .apb_mst6_PReady(      1'b1             ),
      .apb_mst6_PSel(        apb_mst6_PSel    ),
      .apb_mst6_PSlvErr(     1'b0             ),
      .apb_mst6_PStrb(       apb_mst6_PStrb   ),
      .apb_mst6_PEnable(     apb_mst6_PEnable ),
      .apb_mst6_PAddr(       apb_mst6_PAddr   ),
      .apb_mst6_PProt(       apb_mst6_PProt   ),
      .apb_mst7_PWData(      apb_mst7_PWData  ),
      .apb_mst7_PWrite(      apb_mst7_PWrite  ),
      .apb_mst7_PRData(                       ),
      .apb_mst7_PReady(      1'b1             ),
      .apb_mst7_PSel(        apb_mst7_PSel    ),
      .apb_mst7_PSlvErr(     1'b0             ),
      .apb_mst7_PStrb(       apb_mst7_PStrb   ),
      .apb_mst7_PEnable(     apb_mst7_PEnable ),
      .apb_mst7_PAddr(       apb_mst7_PAddr   ),
      .apb_mst7_PProt(       apb_mst7_PProt   ),
      .axi_slv0_R_Last(      shm_ctrl_rlast   ),
      .axi_slv0_R_Resp(      shm_ctrl_rresp   ),
      .axi_slv0_R_Valid(     shm_ctrl_rvalid  ),
      .axi_slv0_R_Ready(     shm_ctrl_rready  ),
      .axi_slv0_R_Data(      shm_ctrl_rdata   ),
      .axi_slv0_R_Id(        shm_ctrl_rid     ),
      .axi_slv0_B_Ready(     shm_ctrl_bready  ),
      .axi_slv0_B_Resp(      shm_ctrl_bresp   ),
      .axi_slv0_B_Valid(     shm_ctrl_bvalid  ),
      .axi_slv0_B_Id(        shm_ctrl_bid     ),
      .axi_slv0_Aw_Prot(     shm_ctrl_awprot  ),
      .axi_slv0_Aw_Addr(     shm_ctrl_awaddr  ),
      .axi_slv0_Aw_Burst(    shm_ctrl_awburst ),
      .axi_slv0_Aw_Lock(     shm_ctrl_awlock  ),
      .axi_slv0_Aw_Cache(    shm_ctrl_awcache ),
      .axi_slv0_Aw_Len(      shm_ctrl_awlen   ),
      .axi_slv0_Aw_Valid(    shm_ctrl_awvalid ),
      .axi_slv0_Aw_Ready(    shm_ctrl_awready ),
      .axi_slv0_Aw_Id(       shm_ctrl_awid    ),
      .axi_slv0_Aw_Size(     shm_ctrl_awsize  ),
      .axi_slv0_W_Last(      shm_ctrl_wlast   ),
      .axi_slv0_W_Valid(     shm_ctrl_wvalid  ),
      .axi_slv0_W_Ready(     shm_ctrl_wready  ),
      .axi_slv0_W_Strb(      shm_ctrl_wstrb   ),
      .axi_slv0_W_Data(      shm_ctrl_wdata   ),
      .axi_slv0_Ar_Prot(     shm_ctrl_arprot  ),
      .axi_slv0_Ar_Addr(     shm_ctrl_araddr  ),
      .axi_slv0_Ar_Burst(    shm_ctrl_arburst ),
      .axi_slv0_Ar_Lock(     shm_ctrl_arlock  ),
      .axi_slv0_Ar_Cache(    shm_ctrl_arcache ),
      .axi_slv0_Ar_Len(      shm_ctrl_arlen   ),
      .axi_slv0_Ar_Valid(    shm_ctrl_arvalid ),
      .axi_slv0_Ar_Ready(    shm_ctrl_arready ),
      .axi_slv0_Ar_Id(       shm_ctrl_arid    ),
      .axi_slv0_Ar_Size(     shm_ctrl_arsize  ) 
      );



// constant signals initialisation
endmodule
