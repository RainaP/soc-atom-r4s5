
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:15

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_customer_cell -module "ClockManager_Regime_cbus_south_Cm" -type "ClockManagerCell" -instance_name "${CUSTOMER_HIERARCHY}Regime_cbus_south_Cm_main/ClockManager/IClockManager_Regime_cbus_south_Cm"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxClkAdapt_Link7Resp001_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxClkAdapt_Link7Resp001_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxClkAdapt_Link7Resp001_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxClkAdapt_Link7Resp001_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3Resp001_main/DtpTxClkAdapt_Link7Resp001_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpRxClkAdapt_Link7_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpRxClkAdapt_Link7_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpRxClkAdapt_Link7_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpRxClkAdapt_Link7_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_2c0_3_main/DtpRxClkAdapt_Link7_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main/DtpRxClkAdapt_Link_c0_astResp001_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main/DtpRxClkAdapt_Link_c0_astResp001_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main/DtpRxClkAdapt_Link_c0_astResp001_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main/DtpRxClkAdapt_Link_c0_astResp001_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asiResp001_main/DtpRxClkAdapt_Link_c0_astResp001_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs/Isc1"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs/Isc2"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs0/Isc0"
arteris_customer_cell -module "VD_bus_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_c0_asi_main/DtpTxClkAdapt_Link_c0_ast_Async/urs1/Isc0"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link8Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_cbus_south_Cm_root" -clock_period "${Regime_cbus_south_Cm_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link8_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_cbus_south_Cm_root" -clock_period "${Regime_cbus_south_Cm_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3Resp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_cbus_south_Cm_root" -clock_period "${Regime_cbus_south_Cm_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_2c0_3_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_cbus_south_Cm_root" -clock_period "${Regime_cbus_south_Cm_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asiResp001_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_cbus_south_Cm_root" -clock_period "${Regime_cbus_south_Cm_root_P}"
arteris_customer_cell -module "VD_bus_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_c0_asi_main_Sys/ClockGater/usce4c71b1c0/instGaterCell" -clock "Regime_cbus_south_Cm_root" -clock_period "${Regime_cbus_south_Cm_root_P}"

