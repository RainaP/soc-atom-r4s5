
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:15

####################################################################

arteris_hierarchy -module "cbus_Structure_Module_southbus_Link8Resp001_main" -generator "DatapathLink" -instance_name "Link8Resp001_main" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_Link8_main" -generator "DatapathLink" -instance_name "Link8_main" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_Link_2c0_3Resp001_main" -generator "DatapathLink" -instance_name "Link_2c0_3Resp001_main" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_Link_2c0_3_main" -generator "DatapathLink" -instance_name "Link_2c0_3_main" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_Link_c0_asiResp001_main" -generator "DatapathLink" -instance_name "Link_c0_asiResp001_main" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_Link_c0_asi_main" -generator "DatapathLink" -instance_name "Link_c0_asi_main" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_Regime_cbus_south_Cm_main" -generator "ClockManager" -instance_name "Regime_cbus_south_Cm_main" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_clockGaters_Link8Resp001_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link8Resp001_main_Sys" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_clockGaters_Link8_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link8_main_Sys" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_clockGaters_Link_2c0_3Resp001_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_2c0_3Resp001_main_Sys" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_clockGaters_Link_2c0_3_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_2c0_3_main_Sys" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_clockGaters_Link_c0_asiResp001_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c0_asiResp001_main_Sys" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_clockGaters_Link_c0_asi_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_c0_asi_main_Sys" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_z_T_C_S_C_L_R_A_6_1" -generator "gate" -instance_name "uAnd6" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus_z_T_C_S_C_L_R_O_6_1" -generator "gate" -instance_name "uOr6" -level "2"
arteris_hierarchy -module "cbus_Structure_Module_southbus" -generator "" -instance_name "cbus_Structure_Module_southbus" -level "1"
