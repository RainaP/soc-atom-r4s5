
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:15

####################################################################

arteris_gen_reset -name "Regime_cbus_south_Cm_root" -clock_domain "clk_cbus_south" -clock "Regime_cbus_south_Cm_root" -spec_domain_clock "/Regime_cbus_south/Cm/root" -clock_period "${Regime_cbus_south_Cm_root_P}" -clock_waveform "[expr ${Regime_cbus_south_Cm_root_P}*0.00] [expr ${Regime_cbus_south_Cm_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_cbus_south_Cm_main/root_Clk_RstN" -lsb "" -msb ""
arteris_gen_reset -name "Regime_cbus_south_Cm_root" -clock_domain "clk_cbus_south" -clock "Regime_cbus_south_Cm_root" -spec_domain_clock "/Regime_cbus_south/Cm/root" -clock_period "${Regime_cbus_south_Cm_root_P}" -clock_waveform "[expr ${Regime_cbus_south_Cm_root_P}*0.00] [expr ${Regime_cbus_south_Cm_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_cbus_south_Cm_main/root_Clk_RstN" -lsb "" -msb ""
