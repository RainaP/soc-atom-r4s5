
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:15

####################################################################

arteris_gen_clock -name "Regime_cbus_south_Cm_root" -pin "Regime_cbus_south_Cm_main/root_Clk Regime_cbus_south_Cm_main/root_Clk_ClkS" -clock_domain "clk_cbus_south" -spec_domain_clock "/Regime_cbus_south/Cm/root" -divide_by "1" -source "clk_cbus_south" -source_period "${clk_cbus_south_P}" -source_waveform "[expr ${clk_cbus_south_P}*0.00] [expr ${clk_cbus_south_P}*0.50]" -user_directive "" -add "FALSE"
