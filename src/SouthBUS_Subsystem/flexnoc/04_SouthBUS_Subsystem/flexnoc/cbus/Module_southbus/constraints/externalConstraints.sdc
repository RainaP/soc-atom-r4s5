
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:15

####################################################################

# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_cbus_south"]

# Create Test Mode 

set_ideal_network -no_propagate [get_ports "TM"]

set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link7_to_Link_2c0_3_Data"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link7_to_Link_2c0_3_WrCnt"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_comb_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_RdCnt"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_comb_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_RdPtr"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link7_to_Link_2c0_3_RdCnt"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link7_to_Link_2c0_3_RdPtr"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_comb_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_2c0_3Resp001_to_Link7Resp001_Data"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_comb_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_Data"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_asi_to_Link_c0_ast_WrCnt"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_cbus_south_Cm_root [expr ${Regime_cbus_south_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst"]

