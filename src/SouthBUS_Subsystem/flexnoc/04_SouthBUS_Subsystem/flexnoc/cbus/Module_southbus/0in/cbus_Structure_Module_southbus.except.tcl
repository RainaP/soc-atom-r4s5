
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:15

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_2c0_3Resp001_main.DtpTxClkAdapt_Link7Resp001_Async.WrCnt -graycode -module cbus_Structure_Module_southbus
cdc signal Link_2c0_3_main.DtpRxClkAdapt_Link7_Async.RdCnt -graycode -module cbus_Structure_Module_southbus
cdc signal Link_c0_asiResp001_main.DtpRxClkAdapt_Link_c0_astResp001_Async.RdCnt -graycode -module cbus_Structure_Module_southbus
cdc signal Link_c0_asi_main.DtpTxClkAdapt_Link_c0_ast_Async.WrCnt -graycode -module cbus_Structure_Module_southbus

