
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:15

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_southbus_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module cbus_Structure_Module_southbus -severity waived -scheme reconvergence -from_signals Link_2c0_3Resp001_main.DtpTxClkAdapt_Link7Resp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_southbus -severity waived -scheme reconvergence -from_signals Link_2c0_3_main.DtpRxClkAdapt_Link7_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_southbus -severity waived -scheme reconvergence -from_signals Link_c0_asiResp001_main.DtpRxClkAdapt_Link_c0_astResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_southbus -severity waived -scheme reconvergence -from_signals Link_c0_asi_main.DtpTxClkAdapt_Link_c0_ast_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module cbus_Structure_Module_southbus -severity waived -scheme multi_sync_mux_select -from Link_2c0_3_main.DtpRxClkAdapt_Link7_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_southbus -severity waived -scheme multi_sync_mux_select -from Link_c0_asiResp001_main.DtpRxClkAdapt_Link_c0_astResp001_Async.uRegSync.instSynchronizerCell*

endmodule
