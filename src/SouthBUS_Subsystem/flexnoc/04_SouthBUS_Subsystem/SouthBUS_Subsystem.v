//--------------------------------------------------------------------------------
//
//   Rebellions Inc. Confidential
//   All Rights Reserved -- Property of Rebellions Inc. 
//
//   Description : 
//
//   Author : jsyoon@rebellions.ai
//
//   date : 2021/08/07
//
//--------------------------------------------------------------------------------

module SouthBUS_Subsystem(

	input         TM                                                            ,
	input         arstn_cbus_south                                              ,
	input         clk_cbus_south                                                ,

	input  [57:0] dp_Link7_to_Link_2c0_3_Data                                   ,
	output [2:0]  dp_Link7_to_Link_2c0_3_RdCnt                                  ,
	output [2:0]  dp_Link7_to_Link_2c0_3_RdPtr                                  ,
	input         dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst                         ,
	output        dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck                      ,
	output        dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst                         ,
	input         dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck                      ,
	input  [2:0]  dp_Link7_to_Link_2c0_3_WrCnt                                  ,
	output [57:0] dp_Link_2c0_3Resp001_to_Link7Resp001_Data                     ,
	input  [2:0]  dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt                    ,
	input  [2:0]  dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr                    ,
	output        dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst           ,
	input         dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck        ,
	input         dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst           ,
	output        dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck        ,
	output [2:0]  dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt                    ,
	output [57:0] dp_Link_c0_asi_to_Link_c0_ast_Data                            ,
	input  [2:0]  dp_Link_c0_asi_to_Link_c0_ast_RdCnt                           ,
	input  [2:0]  dp_Link_c0_asi_to_Link_c0_ast_RdPtr                           ,
	output        dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst                  ,
	input         dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck               ,
	input         dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst                  ,
	output        dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck               ,
	output [2:0]  dp_Link_c0_asi_to_Link_c0_ast_WrCnt                           ,
	input  [57:0] dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data              ,
	output [2:0]  dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt             ,
	output [2:0]  dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr             ,
	input         dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst    ,
	output        dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck ,
	output        dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst    ,
	input         dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck ,
	input  [2:0]  dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt             





);



	cbus_Structure_Module_southbus u_cbus_Structure_Module_southbus(
		.TM( TM )
	,	.arstn_cbus_south( arstn_cbus_south )
	,	.clk_cbus_south( clk_cbus_south )
	,	.dp_Link7_to_Link_2c0_3_Data( dp_Link7_to_Link_2c0_3_Data )
	,	.dp_Link7_to_Link_2c0_3_RdCnt( dp_Link7_to_Link_2c0_3_RdCnt )
	,	.dp_Link7_to_Link_2c0_3_RdPtr( dp_Link7_to_Link_2c0_3_RdPtr )
	,	.dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst( dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst )
	,	.dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck( dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck )
	,	.dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst( dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst )
	,	.dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck( dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck )
	,	.dp_Link7_to_Link_2c0_3_WrCnt( dp_Link7_to_Link_2c0_3_WrCnt )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_Data( dp_Link_2c0_3Resp001_to_Link7Resp001_Data )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt( dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr( dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst( dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck( dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst( dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck( dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt( dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt )
	,	.dp_Link_c0_asi_to_Link_c0_ast_Data( dp_Link_c0_asi_to_Link_c0_ast_Data )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdCnt( dp_Link_c0_asi_to_Link_c0_ast_RdCnt )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RdPtr( dp_Link_c0_asi_to_Link_c0_ast_RdPtr )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst( dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck( dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst( dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst )
	,	.dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck( dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_asi_to_Link_c0_ast_WrCnt( dp_Link_c0_asi_to_Link_c0_ast_WrCnt )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data( dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt( dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr( dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst( dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck( dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst( dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck( dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt( dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt )
	);


endmodule
