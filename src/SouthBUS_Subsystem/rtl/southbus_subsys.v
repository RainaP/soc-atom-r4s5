// This file is generated from Magillem
// Generation : Wed Aug 11 13:20:13 KST 2021 by miock from project atom
// Component : rebellions atom southbus_subsys 0.0
// Design : rebellions atom southbus_subsys_arch 0.0
//  u_cbus_Structure_Module_southbus arteris.com FLEXNOC cbus_Structure_Module_southbus 4.5.1 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/cbus_Structure_Module_southbus_1.xml 
// Magillem Release : 5.2021.1


module southbus_subsys(
   input  wire        TM,
   input  wire        aclk,
   input  wire        arstn,
   output wire [2:0]  dp_Link_c0_astResp001_to_Link_c0_asiResp001_rdcnt,
   output wire        dp_Link_c0_astResp001_to_Link_c0_asiResp001_rxctl_pwronrstack,
   input  wire        dp_Link_c0_astResp001_to_Link_c0_asiResp001_rxctl_pwronrst,
   output wire [2:0]  dp_Link_c0_astResp001_to_Link_c0_asiResp001_rdptr,
   input  wire        dp_Link_c0_astResp001_to_Link_c0_asiResp001_txctl_pwronrstack,
   output wire        dp_Link_c0_astResp001_to_Link_c0_asiResp001_txctl_pwronrst,
   input  wire [57:0] dp_Link_c0_astResp001_to_Link_c0_asiResp001_data,
   input  wire [2:0]  dp_Link_c0_astResp001_to_Link_c0_asiResp001_wrcnt,
   input  wire [2:0]  dp_Link_c0_asi_to_Link_c0_ast_rdcnt,
   input  wire        dp_Link_c0_asi_to_Link_c0_ast_rxctl_pwronrstack,
   output wire        dp_Link_c0_asi_to_Link_c0_ast_rxctl_pwronrst,
   input  wire [2:0]  dp_Link_c0_asi_to_Link_c0_ast_rdptr,
   output wire        dp_Link_c0_asi_to_Link_c0_ast_txctl_pwronrstack,
   input  wire        dp_Link_c0_asi_to_Link_c0_ast_txctl_pwronrst,
   output wire [57:0] dp_Link_c0_asi_to_Link_c0_ast_data,
   output wire [2:0]  dp_Link_c0_asi_to_Link_c0_ast_wrcnt,
   input  wire [2:0]  dp_Link_2c0_3Resp001_to_Link7Resp001_rdcnt,
   input  wire        dp_Link_2c0_3Resp001_to_Link7Resp001_rxctl_pwronrstack,
   output wire        dp_Link_2c0_3Resp001_to_Link7Resp001_rxctl_pwronrst,
   input  wire [2:0]  dp_Link_2c0_3Resp001_to_Link7Resp001_rdptr,
   output wire        dp_Link_2c0_3Resp001_to_Link7Resp001_txctl_pwronrstack,
   input  wire        dp_Link_2c0_3Resp001_to_Link7Resp001_txctl_pwronrst,
   output wire [57:0] dp_Link_2c0_3Resp001_to_Link7Resp001_data,
   output wire [2:0]  dp_Link_2c0_3Resp001_to_Link7Resp001_wrcnt,
   output wire [2:0]  dp_Link7_to_Link_2c0_3_rdcnt,
   output wire        dp_Link7_to_Link_2c0_3_rxctl_pwronrstack,
   input  wire        dp_Link7_to_Link_2c0_3_rxctl_pwronrst,
   output wire [2:0]  dp_Link7_to_Link_2c0_3_rdptr,
   input  wire        dp_Link7_to_Link_2c0_3_txctl_pwronrstack,
   output wire        dp_Link7_to_Link_2c0_3_txctl_pwronrst,
   input  wire [57:0] dp_Link7_to_Link_2c0_3_data,
   input  wire [2:0]  dp_Link7_to_Link_2c0_3_wrcnt 
);




cbus_Structure_Module_southbus u_cbus_Structure_Module_southbus (
      .dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdCnt(                dp_Link_c0_astResp001_to_Link_c0_asiResp001_rdcnt             ),
      .dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_c0_astResp001_to_Link_c0_asiResp001_rxctl_pwronrstack ),
      .dp_Link_c0_astResp001_to_Link_c0_asiResp001_RxCtl_PwrOnRst(       dp_Link_c0_astResp001_to_Link_c0_asiResp001_rxctl_pwronrst    ),
      .dp_Link_c0_astResp001_to_Link_c0_asiResp001_RdPtr(                dp_Link_c0_astResp001_to_Link_c0_asiResp001_rdptr             ),
      .dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_c0_astResp001_to_Link_c0_asiResp001_txctl_pwronrstack ),
      .dp_Link_c0_astResp001_to_Link_c0_asiResp001_TxCtl_PwrOnRst(       dp_Link_c0_astResp001_to_Link_c0_asiResp001_txctl_pwronrst    ),
      .dp_Link_c0_astResp001_to_Link_c0_asiResp001_Data(                 dp_Link_c0_astResp001_to_Link_c0_asiResp001_data              ),
      .dp_Link_c0_astResp001_to_Link_c0_asiResp001_WrCnt(                dp_Link_c0_astResp001_to_Link_c0_asiResp001_wrcnt             ),
      .dp_Link_c0_asi_to_Link_c0_ast_RdCnt(                              dp_Link_c0_asi_to_Link_c0_ast_rdcnt                           ),
      .dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRstAck(                  dp_Link_c0_asi_to_Link_c0_ast_rxctl_pwronrstack               ),
      .dp_Link_c0_asi_to_Link_c0_ast_RxCtl_PwrOnRst(                     dp_Link_c0_asi_to_Link_c0_ast_rxctl_pwronrst                  ),
      .dp_Link_c0_asi_to_Link_c0_ast_RdPtr(                              dp_Link_c0_asi_to_Link_c0_ast_rdptr                           ),
      .dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRstAck(                  dp_Link_c0_asi_to_Link_c0_ast_txctl_pwronrstack               ),
      .dp_Link_c0_asi_to_Link_c0_ast_TxCtl_PwrOnRst(                     dp_Link_c0_asi_to_Link_c0_ast_txctl_pwronrst                  ),
      .dp_Link_c0_asi_to_Link_c0_ast_Data(                               dp_Link_c0_asi_to_Link_c0_ast_data                            ),
      .dp_Link_c0_asi_to_Link_c0_ast_WrCnt(                              dp_Link_c0_asi_to_Link_c0_ast_wrcnt                           ),
      .dp_Link_2c0_3Resp001_to_Link7Resp001_RdCnt(                       dp_Link_2c0_3Resp001_to_Link7Resp001_rdcnt                    ),
      .dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRstAck(           dp_Link_2c0_3Resp001_to_Link7Resp001_rxctl_pwronrstack        ),
      .dp_Link_2c0_3Resp001_to_Link7Resp001_RxCtl_PwrOnRst(              dp_Link_2c0_3Resp001_to_Link7Resp001_rxctl_pwronrst           ),
      .dp_Link_2c0_3Resp001_to_Link7Resp001_RdPtr(                       dp_Link_2c0_3Resp001_to_Link7Resp001_rdptr                    ),
      .dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRstAck(           dp_Link_2c0_3Resp001_to_Link7Resp001_txctl_pwronrstack        ),
      .dp_Link_2c0_3Resp001_to_Link7Resp001_TxCtl_PwrOnRst(              dp_Link_2c0_3Resp001_to_Link7Resp001_txctl_pwronrst           ),
      .dp_Link_2c0_3Resp001_to_Link7Resp001_Data(                        dp_Link_2c0_3Resp001_to_Link7Resp001_data                     ),
      .dp_Link_2c0_3Resp001_to_Link7Resp001_WrCnt(                       dp_Link_2c0_3Resp001_to_Link7Resp001_wrcnt                    ),
      .dp_Link7_to_Link_2c0_3_RdCnt(                                     dp_Link7_to_Link_2c0_3_rdcnt                                  ),
      .dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRstAck(                         dp_Link7_to_Link_2c0_3_rxctl_pwronrstack                      ),
      .dp_Link7_to_Link_2c0_3_RxCtl_PwrOnRst(                            dp_Link7_to_Link_2c0_3_rxctl_pwronrst                         ),
      .dp_Link7_to_Link_2c0_3_RdPtr(                                     dp_Link7_to_Link_2c0_3_rdptr                                  ),
      .dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRstAck(                         dp_Link7_to_Link_2c0_3_txctl_pwronrstack                      ),
      .dp_Link7_to_Link_2c0_3_TxCtl_PwrOnRst(                            dp_Link7_to_Link_2c0_3_txctl_pwronrst                         ),
      .dp_Link7_to_Link_2c0_3_Data(                                      dp_Link7_to_Link_2c0_3_data                                   ),
      .dp_Link7_to_Link_2c0_3_WrCnt(                                     dp_Link7_to_Link_2c0_3_wrcnt                                  ),
      .clk_cbus_south(                                                   aclk                                                          ),
      .arstn_cbus_south(                                                 arstn                                                         ),
      .TM(                                                               TM                                                            ) 
      );



// constant signals initialisation
endmodule
