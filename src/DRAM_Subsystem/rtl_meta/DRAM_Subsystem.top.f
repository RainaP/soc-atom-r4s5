${DESIGN_DIR}/src/DRAM_Subsystem/rtl/DRAM_PBUS.v

${DESIGN_DIR}/src/DRAM_Subsystem/rtl/DRAM0_Subsystem.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/11_DRAM0_Subsystem/flexnoc/cbus/Module_dram0/cbus_Structure_Module_dram0_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/11_DRAM0_Subsystem/flexnoc/cbus/Module_dram0/cbus_Structure_Module_dram0.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/11_DRAM0_Subsystem/flexnoc/dbus_read/Module_dram0/dbus_read_Structure_Module_dram0_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/11_DRAM0_Subsystem/flexnoc/dbus_read/Module_dram0/dbus_read_Structure_Module_dram0.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/11_DRAM0_Subsystem/flexnoc/dbus_write/Module_dram0/dbus_write_Structure_Module_dram0_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/11_DRAM0_Subsystem/flexnoc/dbus_write/Module_dram0/dbus_write_Structure_Module_dram0.v


${DESIGN_DIR}/src/DRAM_Subsystem/rtl/DRAM1_Subsystem.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/12_DRAM1_Subsystem/flexnoc/cbus/Module_dram1/cbus_Structure_Module_dram1_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/12_DRAM1_Subsystem/flexnoc/cbus/Module_dram1/cbus_Structure_Module_dram1.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/12_DRAM1_Subsystem/flexnoc/dbus_read/Module_dram1/dbus_read_Structure_Module_dram1_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/12_DRAM1_Subsystem/flexnoc/dbus_read/Module_dram1/dbus_read_Structure_Module_dram1.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/12_DRAM1_Subsystem/flexnoc/dbus_write/Module_dram1/dbus_write_Structure_Module_dram1_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/12_DRAM1_Subsystem/flexnoc/dbus_write/Module_dram1/dbus_write_Structure_Module_dram1.v


${DESIGN_DIR}/src/DRAM_Subsystem/rtl/DRAM2_Subsystem.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/13_DRAM2_Subsystem/flexnoc/cbus/Module_dram2/cbus_Structure_Module_dram2_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/13_DRAM2_Subsystem/flexnoc/cbus/Module_dram2/cbus_Structure_Module_dram2.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/13_DRAM2_Subsystem/flexnoc/dbus_read/Module_dram2/dbus_read_Structure_Module_dram2_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/13_DRAM2_Subsystem/flexnoc/dbus_read/Module_dram2/dbus_read_Structure_Module_dram2.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/13_DRAM2_Subsystem/flexnoc/dbus_write/Module_dram2/dbus_write_Structure_Module_dram2_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/13_DRAM2_Subsystem/flexnoc/dbus_write/Module_dram2/dbus_write_Structure_Module_dram2.v


${DESIGN_DIR}/src/DRAM_Subsystem/rtl/DRAM3_Subsystem.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/14_DRAM3_Subsystem/flexnoc/cbus/Module_dram3/cbus_Structure_Module_dram3_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/14_DRAM3_Subsystem/flexnoc/cbus/Module_dram3/cbus_Structure_Module_dram3.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/14_DRAM3_Subsystem/flexnoc/dbus_read/Module_dram3/dbus_read_Structure_Module_dram3_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/14_DRAM3_Subsystem/flexnoc/dbus_read/Module_dram3/dbus_read_Structure_Module_dram3.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/14_DRAM3_Subsystem/flexnoc/dbus_write/Module_dram3/dbus_write_Structure_Module_dram3_commons.v
${DESIGN_DIR}/src/DRAM_Subsystem/flexnoc/14_DRAM3_Subsystem/flexnoc/dbus_write/Module_dram3/dbus_write_Structure_Module_dram3.v

