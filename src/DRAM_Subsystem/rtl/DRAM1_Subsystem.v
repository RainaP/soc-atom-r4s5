//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module DRAM1_Subsystem (
	 input          TEST_MODE
	,input          CLK__DRAM
	,input          RSTN__DRAM
	,output         IRQ__controller_int
	,output         IRQ__phy_pi_int
	,input  [ 57:0] ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data
	,output [  2:0] ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt
	,output [  1:0] ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr
	,input          ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst
	,output         ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck
	,output         ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst
	,input          ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck
	,input  [  2:0] ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt
	,output [ 57:0] ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data
	,input  [  2:0] ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt
	,input  [  1:0] ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr
	,output         ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst
	,input          ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck
	,input          ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst
	,output         ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck
	,output [  2:0] ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt
	,input  [128:0] r_dp_Link_m1_asi_to_Link_m1_ast_Data
	,output [  2:0] r_dp_Link_m1_asi_to_Link_m1_ast_RdCnt
	,output [  2:0] r_dp_Link_m1_asi_to_Link_m1_ast_RdPtr
	,input          r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst
	,output         r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck
	,output         r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst
	,input          r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck
	,input  [  2:0] r_dp_Link_m1_asi_to_Link_m1_ast_WrCnt
	,output [705:0] r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data
	,input  [  2:0] r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt
	,input  [  2:0] r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr
	,output         r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst
	,input          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck
	,input          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst
	,output         r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck
	,output [  2:0] r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt
	,input  [702:0] w_dp_Link_m1_asi_to_Link_m1_ast_Data
	,output [  2:0] w_dp_Link_m1_asi_to_Link_m1_ast_RdCnt
	,output [  2:0] w_dp_Link_m1_asi_to_Link_m1_ast_RdPtr
	,input          w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst
	,output         w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck
	,output         w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst
	,input          w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck
	,input  [  2:0] w_dp_Link_m1_asi_to_Link_m1_ast_WrCnt
	,output [125:0] w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data
	,input  [  2:0] w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt
	,input  [  2:0] w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr
	,output         w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst
	,input          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck
	,input          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst
	,output         w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck
	,output [  2:0] w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_DRAM1_Subsystem
	assign IRQ__controller_int = 1'h0;
	assign IRQ__phy_pi_int = 1'h0;
	assign ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt = 3'h0;
	assign ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr = 2'h0;
	assign ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck = 1'h0;
	assign ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst = 1'h0;
	assign ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data = 58'h0;
	assign ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst = 1'h0;
	assign ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt = 3'h0;
	assign r_dp_Link_m1_asi_to_Link_m1_ast_RdCnt = 3'h0;
	assign r_dp_Link_m1_asi_to_Link_m1_ast_RdPtr = 3'h0;
	assign r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck = 1'h0;
	assign r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst = 1'h0;
	assign r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data = 706'h0;
	assign r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst = 1'h0;
	assign r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt = 3'h0;
	assign w_dp_Link_m1_asi_to_Link_m1_ast_RdCnt = 3'h0;
	assign w_dp_Link_m1_asi_to_Link_m1_ast_RdPtr = 3'h0;
	assign w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck = 1'h0;
	assign w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst = 1'h0;
	assign w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data = 126'h0;
	assign w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst = 1'h0;
	assign w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt = 3'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_DRAM1_Subsystem
	wire [ 23:0] AXI2APB_0__DRAM_PBUS_0__apb_mst0_PAddr;
	wire         AXI2APB_0__DRAM_PBUS_0__apb_mst0_PEnable;
	wire [  2:0] AXI2APB_0__DRAM_PBUS_0__apb_mst0_PProt;
	wire [ 31:0] AXI2APB_0__DRAM_PBUS_0__apb_mst0_PRData;
	wire         AXI2APB_0__DRAM_PBUS_0__apb_mst0_PReady;
	wire         AXI2APB_0__DRAM_PBUS_0__apb_mst0_PSel;
	wire         AXI2APB_0__DRAM_PBUS_0__apb_mst0_PSlvErr;
	wire [  3:0] AXI2APB_0__DRAM_PBUS_0__apb_mst0_PStrb;
	wire [ 31:0] AXI2APB_0__DRAM_PBUS_0__apb_mst0_PWData;
	wire         AXI2APB_0__DRAM_PBUS_0__apb_mst0_PWrite;
	wire [ 23:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Addr;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Burst;
	wire [  3:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Cache;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Lock;
	wire [  2:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Prot;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Ready;
	wire [  2:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Size;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Valid;
	wire [ 23:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Addr;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Burst;
	wire [  3:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Cache;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Lock;
	wire [  2:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Prot;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Ready;
	wire [  2:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Size;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Valid;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Ready;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Resp;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Valid;
	wire [ 31:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Data;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Last;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Ready;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Resp;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Valid;
	wire [ 31:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Data;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Last;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Ready;
	wire [  3:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Strb;
	wire         cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Valid;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Id;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Id;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Id;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Id;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Len;
	wire [  1:0] cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Len;
	wire [ 36:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Addr;
	wire [  1:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Burst;
	wire [  3:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Cache;
	wire [  6:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Id;
	wire         dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Lock;
	wire [  2:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Prot;
	wire         dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Ready;
	wire [  2:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Size;
	wire [  3:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_User;
	wire         dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Valid;
	wire [511:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Data;
	wire [  6:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Id;
	wire         dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Last;
	wire         dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Ready;
	wire [  1:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Resp;
	wire [  3:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_User;
	wire         dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Valid;
	wire [  5:0] dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Len;
	wire [ 36:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Addr;
	wire [  1:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Burst;
	wire [  3:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Cache;
	wire [  6:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Id;
	wire         dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Lock;
	wire [  2:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Prot;
	wire         dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Ready;
	wire [  2:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Size;
	wire [  3:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_User;
	wire         dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Valid;
	wire [  6:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Id;
	wire         dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Ready;
	wire [  1:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Resp;
	wire [  3:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_User;
	wire         dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Valid;
	wire [511:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Data;
	wire         dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Last;
	wire         dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Ready;
	wire [ 63:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Strb;
	wire         dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Valid;
	wire [  5:0] dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Len;
	wire         SCU_0__DRAM_PBUS_0__APB__PENABLE;
	wire         SCU_0__DRAM_PBUS_0__APB__PSEL;
	wire         SCU_0__DRAM_PBUS_0__APB__PREADY;
	wire         SCU_0__DRAM_PBUS_0__APB__PSLVERR;
	wire         SCU_0__DRAM_PBUS_0__APB__PWRITE;
	wire [  2:0] SCU_0__DRAM_PBUS_0__APB__PPROT;
	wire [ 11:0] SCU_0__DRAM_PBUS_0__APB__PADDR;
	wire [  3:0] SCU_0__DRAM_PBUS_0__APB__PSTRB;
	wire [ 31:0] SCU_0__DRAM_PBUS_0__APB__PWDATA;
	wire [ 31:0] SCU_0__DRAM_PBUS_0__APB__PRDATA;
	wire         DRAMCON_0__DRAM_PBUS_0__APB__PENABLE;
	wire         DRAMCON_0__DRAM_PBUS_0__APB__PSEL;
	wire         DRAMCON_0__DRAM_PBUS_0__APB__PREADY;
	wire         DRAMCON_0__DRAM_PBUS_0__APB__PSLVERR;
	wire         DRAMCON_0__DRAM_PBUS_0__APB__PWRITE;
	wire [  2:0] DRAMCON_0__DRAM_PBUS_0__APB__PPROT;
	wire [ 11:0] DRAMCON_0__DRAM_PBUS_0__APB__PADDR;
	wire [  3:0] DRAMCON_0__DRAM_PBUS_0__APB__PSTRB;
	wire [ 31:0] DRAMCON_0__DRAM_PBUS_0__APB__PWDATA;
	wire [ 31:0] DRAMCON_0__DRAM_PBUS_0__APB__PRDATA;
	wire         DRAMPHY_0__DRAM_PBUS_0__APB__PENABLE;
	wire         DRAMPHY_0__DRAM_PBUS_0__APB__PSEL;
	wire         DRAMPHY_0__DRAM_PBUS_0__APB__PREADY;
	wire         DRAMPHY_0__DRAM_PBUS_0__APB__PSLVERR;
	wire         DRAMPHY_0__DRAM_PBUS_0__APB__PWRITE;
	wire [  2:0] DRAMPHY_0__DRAM_PBUS_0__APB__PPROT;
	wire [ 11:0] DRAMPHY_0__DRAM_PBUS_0__APB__PADDR;
	wire [  3:0] DRAMPHY_0__DRAM_PBUS_0__APB__PSTRB;
	wire [ 31:0] DRAMPHY_0__DRAM_PBUS_0__APB__PWDATA;
	wire [ 31:0] DRAMPHY_0__DRAM_PBUS_0__APB__PRDATA;

	assign IRQ__controller_int = 1'h0;
	assign IRQ__phy_pi_int = 1'h0;

	DRAM_PBUS #(
		 .ABITS(24)
	) DRAM_PBUS_0 (
		 .CLK__APB               (CLK__DRAM)
		,.RSTN__APB              (RSTN__DRAM)
		,.APB__PSEL              (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PSel)
		,.APB__PENABLE           (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PEnable)
		,.APB__PWRITE            (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PWrite)
		,.APB__PPROT             (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PProt)
		,.APB__PADDR             (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PAddr)
		,.APB__PSTRB             (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PStrb)
		,.APB__PWDATA            (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PWData)
		,.APB__PREADY            (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PReady)
		,.APB__PSLVERR           (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PSlvErr)
		,.APB__PRDATA            (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PRData)
		,.SCU_0__APB__PSEL       (SCU_0__DRAM_PBUS_0__APB__PSEL)
		,.SCU_0__APB__PENABLE    (SCU_0__DRAM_PBUS_0__APB__PENABLE)
		,.SCU_0__APB__PWRITE     (SCU_0__DRAM_PBUS_0__APB__PWRITE)
		,.SCU_0__APB__PPROT      (SCU_0__DRAM_PBUS_0__APB__PPROT)
		,.SCU_0__APB__PADDR      (SCU_0__DRAM_PBUS_0__APB__PADDR)
		,.SCU_0__APB__PSTRB      (SCU_0__DRAM_PBUS_0__APB__PSTRB)
		,.SCU_0__APB__PWDATA     (SCU_0__DRAM_PBUS_0__APB__PWDATA)
		,.SCU_0__APB__PREADY     (SCU_0__DRAM_PBUS_0__APB__PREADY)
		,.SCU_0__APB__PSLVERR    (SCU_0__DRAM_PBUS_0__APB__PSLVERR)
		,.SCU_0__APB__PRDATA     (SCU_0__DRAM_PBUS_0__APB__PRDATA)
		,.DRAMCON_0__APB__PSEL   (DRAMCON_0__DRAM_PBUS_0__APB__PSEL)
		,.DRAMCON_0__APB__PENABLE(DRAMCON_0__DRAM_PBUS_0__APB__PENABLE)
		,.DRAMCON_0__APB__PWRITE (DRAMCON_0__DRAM_PBUS_0__APB__PWRITE)
		,.DRAMCON_0__APB__PPROT  (DRAMCON_0__DRAM_PBUS_0__APB__PPROT)
		,.DRAMCON_0__APB__PADDR  (DRAMCON_0__DRAM_PBUS_0__APB__PADDR)
		,.DRAMCON_0__APB__PSTRB  (DRAMCON_0__DRAM_PBUS_0__APB__PSTRB)
		,.DRAMCON_0__APB__PWDATA (DRAMCON_0__DRAM_PBUS_0__APB__PWDATA)
		,.DRAMCON_0__APB__PREADY (DRAMCON_0__DRAM_PBUS_0__APB__PREADY)
		,.DRAMCON_0__APB__PSLVERR(DRAMCON_0__DRAM_PBUS_0__APB__PSLVERR)
		,.DRAMCON_0__APB__PRDATA (DRAMCON_0__DRAM_PBUS_0__APB__PRDATA)
		,.DRAMPHY_0__APB__PSEL   (DRAMPHY_0__DRAM_PBUS_0__APB__PSEL)
		,.DRAMPHY_0__APB__PENABLE(DRAMPHY_0__DRAM_PBUS_0__APB__PENABLE)
		,.DRAMPHY_0__APB__PWRITE (DRAMPHY_0__DRAM_PBUS_0__APB__PWRITE)
		,.DRAMPHY_0__APB__PPROT  (DRAMPHY_0__DRAM_PBUS_0__APB__PPROT)
		,.DRAMPHY_0__APB__PADDR  (DRAMPHY_0__DRAM_PBUS_0__APB__PADDR)
		,.DRAMPHY_0__APB__PSTRB  (DRAMPHY_0__DRAM_PBUS_0__APB__PSTRB)
		,.DRAMPHY_0__APB__PWDATA (DRAMPHY_0__DRAM_PBUS_0__APB__PWDATA)
		,.DRAMPHY_0__APB__PREADY (DRAMPHY_0__DRAM_PBUS_0__APB__PREADY)
		,.DRAMPHY_0__APB__PSLVERR(DRAMPHY_0__DRAM_PBUS_0__APB__PSLVERR)
		,.DRAMPHY_0__APB__PRDATA (DRAMPHY_0__DRAM_PBUS_0__APB__PRDATA)
	);

	wire [  7:  2] __opened__AXI2APB_0__axi_slv0_B_Id_msb_7_lsb_2;
	wire [  7:  2] __opened__AXI2APB_0__axi_slv0_R_Id_msb_7_lsb_2;
	axi2apb_bridge
	AXI2APB_0 (
		 .TM               (TEST_MODE)
		,.apb_mst0_PAddr   (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PAddr)
		,.apb_mst0_PEnable (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PEnable)
		,.apb_mst0_PProt   (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PProt)
		,.apb_mst0_PRData  (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PRData)
		,.apb_mst0_PReady  (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PReady)
		,.apb_mst0_PSel    (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PSel)
		,.apb_mst0_PSlvErr (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PSlvErr)
		,.apb_mst0_PStrb   (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PStrb)
		,.apb_mst0_PWData  (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PWData)
		,.apb_mst0_PWrite  (AXI2APB_0__DRAM_PBUS_0__apb_mst0_PWrite)
		,.apb_mst1_PAddr   ()
		,.apb_mst1_PEnable ()
		,.apb_mst1_PProt   ()
		,.apb_mst1_PRData  (32'h0)
		,.apb_mst1_PReady  (1'h0)
		,.apb_mst1_PSel    ()
		,.apb_mst1_PSlvErr (1'h0)
		,.apb_mst1_PStrb   ()
		,.apb_mst1_PWData  ()
		,.apb_mst1_PWrite  ()
		,.apb_mst2_PAddr   ()
		,.apb_mst2_PEnable ()
		,.apb_mst2_PProt   ()
		,.apb_mst2_PRData  (32'h0)
		,.apb_mst2_PReady  (1'h0)
		,.apb_mst2_PSel    ()
		,.apb_mst2_PSlvErr (1'h0)
		,.apb_mst2_PStrb   ()
		,.apb_mst2_PWData  ()
		,.apb_mst2_PWrite  ()
		,.apb_mst3_PAddr   ()
		,.apb_mst3_PEnable ()
		,.apb_mst3_PProt   ()
		,.apb_mst3_PRData  (32'h0)
		,.apb_mst3_PReady  (1'h0)
		,.apb_mst3_PSel    ()
		,.apb_mst3_PSlvErr (1'h0)
		,.apb_mst3_PStrb   ()
		,.apb_mst3_PWData  ()
		,.apb_mst3_PWrite  ()
		,.apb_mst4_PAddr   ()
		,.apb_mst4_PEnable ()
		,.apb_mst4_PProt   ()
		,.apb_mst4_PRData  (32'h0)
		,.apb_mst4_PReady  (1'h0)
		,.apb_mst4_PSel    ()
		,.apb_mst4_PSlvErr (1'h0)
		,.apb_mst4_PStrb   ()
		,.apb_mst4_PWData  ()
		,.apb_mst4_PWrite  ()
		,.apb_mst5_PAddr   ()
		,.apb_mst5_PEnable ()
		,.apb_mst5_PProt   ()
		,.apb_mst5_PRData  (32'h0)
		,.apb_mst5_PReady  (1'h0)
		,.apb_mst5_PSel    ()
		,.apb_mst5_PSlvErr (1'h0)
		,.apb_mst5_PStrb   ()
		,.apb_mst5_PWData  ()
		,.apb_mst5_PWrite  ()
		,.apb_mst6_PAddr   ()
		,.apb_mst6_PEnable ()
		,.apb_mst6_PProt   ()
		,.apb_mst6_PRData  (32'h0)
		,.apb_mst6_PReady  (1'h0)
		,.apb_mst6_PSel    ()
		,.apb_mst6_PSlvErr (1'h0)
		,.apb_mst6_PStrb   ()
		,.apb_mst6_PWData  ()
		,.apb_mst6_PWrite  ()
		,.apb_mst7_PAddr   ()
		,.apb_mst7_PEnable ()
		,.apb_mst7_PProt   ()
		,.apb_mst7_PRData  (32'h0)
		,.apb_mst7_PReady  (1'h0)
		,.apb_mst7_PSel    ()
		,.apb_mst7_PSlvErr (1'h0)
		,.apb_mst7_PStrb   ()
		,.apb_mst7_PWData  ()
		,.apb_mst7_PWrite  ()
		,.arstn            (RSTN__DRAM)
		,.axi_slv0_Ar_Addr (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Addr)
		,.axi_slv0_Ar_Burst(cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Burst)
		,.axi_slv0_Ar_Cache(cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Cache)
		,.axi_slv0_Ar_Id   ({
			6'h0
			,cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Id})
		,.axi_slv0_Ar_Len  ({
			2'h0
			,cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Len})
		,.axi_slv0_Ar_Lock (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Lock)
		,.axi_slv0_Ar_Prot (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Prot)
		,.axi_slv0_Ar_Ready(cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Ready)
		,.axi_slv0_Ar_Size (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Size)
		,.axi_slv0_Ar_Valid(cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Valid)
		,.axi_slv0_Aw_Addr (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Addr)
		,.axi_slv0_Aw_Burst(cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Burst)
		,.axi_slv0_Aw_Cache(cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Cache)
		,.axi_slv0_Aw_Id   ({
			6'h0
			,cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Id})
		,.axi_slv0_Aw_Len  ({
			2'h0
			,cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Len})
		,.axi_slv0_Aw_Lock (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Lock)
		,.axi_slv0_Aw_Prot (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Prot)
		,.axi_slv0_Aw_Ready(cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Ready)
		,.axi_slv0_Aw_Size (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Size)
		,.axi_slv0_Aw_Valid(cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Valid)
		,.axi_slv0_B_Id    ({
			__opened__AXI2APB_0__axi_slv0_B_Id_msb_7_lsb_2
			,cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Id})
		,.axi_slv0_B_Ready (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Ready)
		,.axi_slv0_B_Resp  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Resp)
		,.axi_slv0_B_Valid (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Valid)
		,.axi_slv0_R_Data  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Data)
		,.axi_slv0_R_Id    ({
			__opened__AXI2APB_0__axi_slv0_R_Id_msb_7_lsb_2
			,cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Id})
		,.axi_slv0_R_Last  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Last)
		,.axi_slv0_R_Ready (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Ready)
		,.axi_slv0_R_Resp  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Resp)
		,.axi_slv0_R_Valid (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Valid)
		,.axi_slv0_W_Data  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Data)
		,.axi_slv0_W_Last  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Last)
		,.axi_slv0_W_Ready (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Ready)
		,.axi_slv0_W_Strb  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Strb)
		,.axi_slv0_W_Valid (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Valid)
		,.clk              (CLK__DRAM)
	);

	cbus_Structure_Module_dram1
	cbus_Structure_Module_dram (
		 .TM                                                                   (TEST_MODE)
		,.arstn_dram1                                                          (RSTN__DRAM)
		,.clk_dram1                                                            (CLK__DRAM)
		,.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data                           (ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data)
		,.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt                          (ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt)
		,.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr                          (ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr)
		,.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst                 (ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst)
		,.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck              (ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst                 (ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst)
		,.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck              (ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt                          (ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt)
		,.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data             (ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data)
		,.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt            (ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt)
		,.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr            (ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr)
		,.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst   (ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst)
		,.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck(ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck)
		,.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst   (ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst)
		,.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck(ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck)
		,.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt            (ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt)
		,.dram1_ctrl_Ar_Addr                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Addr)
		,.dram1_ctrl_Ar_Burst                                                  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Burst)
		,.dram1_ctrl_Ar_Cache                                                  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Cache)
		,.dram1_ctrl_Ar_Id                                                     (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Id)
		,.dram1_ctrl_Ar_Len                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Len)
		,.dram1_ctrl_Ar_Lock                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Lock)
		,.dram1_ctrl_Ar_Prot                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Prot)
		,.dram1_ctrl_Ar_Ready                                                  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Ready)
		,.dram1_ctrl_Ar_Size                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Size)
		,.dram1_ctrl_Ar_Valid                                                  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Ar_Valid)
		,.dram1_ctrl_Aw_Addr                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Addr)
		,.dram1_ctrl_Aw_Burst                                                  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Burst)
		,.dram1_ctrl_Aw_Cache                                                  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Cache)
		,.dram1_ctrl_Aw_Id                                                     (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Id)
		,.dram1_ctrl_Aw_Len                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Len)
		,.dram1_ctrl_Aw_Lock                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Lock)
		,.dram1_ctrl_Aw_Prot                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Prot)
		,.dram1_ctrl_Aw_Ready                                                  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Ready)
		,.dram1_ctrl_Aw_Size                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Size)
		,.dram1_ctrl_Aw_Valid                                                  (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_Aw_Valid)
		,.dram1_ctrl_B_Id                                                      (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Id)
		,.dram1_ctrl_B_Ready                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Ready)
		,.dram1_ctrl_B_Resp                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Resp)
		,.dram1_ctrl_B_Valid                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_B_Valid)
		,.dram1_ctrl_R_Data                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Data)
		,.dram1_ctrl_R_Id                                                      (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Id)
		,.dram1_ctrl_R_Last                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Last)
		,.dram1_ctrl_R_Ready                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Ready)
		,.dram1_ctrl_R_Resp                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Resp)
		,.dram1_ctrl_R_Valid                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_R_Valid)
		,.dram1_ctrl_W_Data                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Data)
		,.dram1_ctrl_W_Last                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Last)
		,.dram1_ctrl_W_Ready                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Ready)
		,.dram1_ctrl_W_Strb                                                    (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Strb)
		,.dram1_ctrl_W_Valid                                                   (cbus_Structure_Module_dram__AXI2APB_0__dram1_ctrl_W_Valid)
	);

	dbus_read_Structure_Module_dram1
	dbus_read_Structure_Module_dram (
		 .TM                                                           (TEST_MODE)
		,.arstn_dram1                                                  (RSTN__DRAM)
		,.clk_dram1                                                    (CLK__DRAM)
		,.dp_Link_m1_asi_to_Link_m1_ast_Data                           (r_dp_Link_m1_asi_to_Link_m1_ast_Data)
		,.dp_Link_m1_asi_to_Link_m1_ast_RdCnt                          (r_dp_Link_m1_asi_to_Link_m1_ast_RdCnt)
		,.dp_Link_m1_asi_to_Link_m1_ast_RdPtr                          (r_dp_Link_m1_asi_to_Link_m1_ast_RdPtr)
		,.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst                 (r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst)
		,.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck              (r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst                 (r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst)
		,.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck              (r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_m1_asi_to_Link_m1_ast_WrCnt                          (r_dp_Link_m1_asi_to_Link_m1_ast_WrCnt)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data             (r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt            (r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr            (r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst   (r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck(r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst   (r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck(r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt            (r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt)
		,.dram1_r_Ar_Addr                                              (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Addr)
		,.dram1_r_Ar_Burst                                             (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Burst)
		,.dram1_r_Ar_Cache                                             (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Cache)
		,.dram1_r_Ar_Id                                                (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Id)
		,.dram1_r_Ar_Len                                               (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Len)
		,.dram1_r_Ar_Lock                                              (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Lock)
		,.dram1_r_Ar_Prot                                              (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Prot)
		,.dram1_r_Ar_Ready                                             (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Ready)
		,.dram1_r_Ar_Size                                              (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Size)
		,.dram1_r_Ar_User                                              (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_User)
		,.dram1_r_Ar_Valid                                             (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Valid)
		,.dram1_r_R_Data                                               (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Data)
		,.dram1_r_R_Id                                                 (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Id)
		,.dram1_r_R_Last                                               (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Last)
		,.dram1_r_R_Ready                                              (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Ready)
		,.dram1_r_R_Resp                                               (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Resp)
		,.dram1_r_R_User                                               (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_User)
		,.dram1_r_R_Valid                                              (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Valid)
	);

	dbus_write_Structure_Module_dram1
	dbus_write_Structure_Module_dram (
		 .TM                                                           (TEST_MODE)
		,.arstn_dram1                                                  (RSTN__DRAM)
		,.clk_dram1                                                    (CLK__DRAM)
		,.dp_Link_m1_asi_to_Link_m1_ast_Data                           (w_dp_Link_m1_asi_to_Link_m1_ast_Data)
		,.dp_Link_m1_asi_to_Link_m1_ast_RdCnt                          (w_dp_Link_m1_asi_to_Link_m1_ast_RdCnt)
		,.dp_Link_m1_asi_to_Link_m1_ast_RdPtr                          (w_dp_Link_m1_asi_to_Link_m1_ast_RdPtr)
		,.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst                 (w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst)
		,.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck              (w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck)
		,.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst                 (w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst)
		,.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck              (w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck)
		,.dp_Link_m1_asi_to_Link_m1_ast_WrCnt                          (w_dp_Link_m1_asi_to_Link_m1_ast_WrCnt)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data             (w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt            (w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr            (w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst   (w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck(w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst   (w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck(w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck)
		,.dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt            (w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt)
		,.dram1_w_Aw_Addr                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Addr)
		,.dram1_w_Aw_Burst                                             (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Burst)
		,.dram1_w_Aw_Cache                                             (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Cache)
		,.dram1_w_Aw_Id                                                (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Id)
		,.dram1_w_Aw_Len                                               (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Len)
		,.dram1_w_Aw_Lock                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Lock)
		,.dram1_w_Aw_Prot                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Prot)
		,.dram1_w_Aw_Ready                                             (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Ready)
		,.dram1_w_Aw_Size                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Size)
		,.dram1_w_Aw_User                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_User)
		,.dram1_w_Aw_Valid                                             (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Valid)
		,.dram1_w_B_Id                                                 (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Id)
		,.dram1_w_B_Ready                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Ready)
		,.dram1_w_B_Resp                                               (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Resp)
		,.dram1_w_B_User                                               (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_User)
		,.dram1_w_B_Valid                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Valid)
		,.dram1_w_W_Data                                               (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Data)
		,.dram1_w_W_Last                                               (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Last)
		,.dram1_w_W_Ready                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Ready)
		,.dram1_w_W_Strb                                               (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Strb)
		,.dram1_w_W_Valid                                              (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Valid)
	);

	semifive_axi_dummy
	semifive_axi_dummy (
		 .clk     (CLK__DRAM)
		,.rst_n   (RSTN__DRAM)
		,.awid    (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Id)
		,.awaddr  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Addr)
		,.awlen   ({
			2'h0
			,dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Len})
		,.awsize  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Size)
		,.awburst (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Burst)
		,.awlock  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Lock)
		,.awcache (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Cache)
		,.awprot  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Prot)
		,.awqos   (4'h0)
		,.awregion(4'h0)
		,.awuser  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_User)
		,.awvalid (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Valid)
		,.awready (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_Aw_Ready)
		,.wdata   (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Data)
		,.wstrb   (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Strb)
		,.wlast   (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Last)
		,.wvalid  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Valid)
		,.wready  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_W_Ready)
		,.bid     (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Id)
		,.bresp   (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Resp)
		,.buser   (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_User)
		,.bvalid  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Valid)
		,.bready  (dbus_write_Structure_Module_dram__semifive_axi_dummy__dram1_w_B_Ready)
		,.arid    (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Id)
		,.araddr  (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Addr)
		,.arlen   ({
			2'h0
			,dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Len})
		,.arsize  (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Size)
		,.arburst (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Burst)
		,.arlock  (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Lock)
		,.arcache (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Cache)
		,.arprot  (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Prot)
		,.arqos   (4'h0)
		,.arregion(4'h0)
		,.aruser  (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_User)
		,.arvalid (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Valid)
		,.arready (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_Ar_Ready)
		,.rid     (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Id)
		,.rdata   (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Data)
		,.rresp   (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Resp)
		,.rlast   (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Last)
		,.ruser   (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_User)
		,.rvalid  (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Valid)
		,.rready  (dbus_read_Structure_Module_dram__semifive_axi_dummy__dram1_r_R_Ready)
	);

	semifive_apb_dummy
	SCU_0 (
		 .CLK__APB    (CLK__DRAM)
		,.RSTN__APB   (RSTN__DRAM)
		,.APB__PENABLE(SCU_0__DRAM_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (SCU_0__DRAM_PBUS_0__APB__PSEL)
		,.APB__PREADY (SCU_0__DRAM_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(SCU_0__DRAM_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (SCU_0__DRAM_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (SCU_0__DRAM_PBUS_0__APB__PPROT)
		,.APB__PADDR  (SCU_0__DRAM_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (SCU_0__DRAM_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (SCU_0__DRAM_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (SCU_0__DRAM_PBUS_0__APB__PRDATA)
	);

	semifive_apb_dummy
	DRAMCON_0 (
		 .CLK__APB    (CLK__DRAM)
		,.RSTN__APB   (RSTN__DRAM)
		,.APB__PENABLE(DRAMCON_0__DRAM_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (DRAMCON_0__DRAM_PBUS_0__APB__PSEL)
		,.APB__PREADY (DRAMCON_0__DRAM_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(DRAMCON_0__DRAM_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (DRAMCON_0__DRAM_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (DRAMCON_0__DRAM_PBUS_0__APB__PPROT)
		,.APB__PADDR  (DRAMCON_0__DRAM_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (DRAMCON_0__DRAM_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (DRAMCON_0__DRAM_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (DRAMCON_0__DRAM_PBUS_0__APB__PRDATA)
	);

	semifive_apb_dummy
	DRAMPHY_0 (
		 .CLK__APB    (CLK__DRAM)
		,.RSTN__APB   (RSTN__DRAM)
		,.APB__PENABLE(DRAMPHY_0__DRAM_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (DRAMPHY_0__DRAM_PBUS_0__APB__PSEL)
		,.APB__PREADY (DRAMPHY_0__DRAM_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(DRAMPHY_0__DRAM_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (DRAMPHY_0__DRAM_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (DRAMPHY_0__DRAM_PBUS_0__APB__PPROT)
		,.APB__PADDR  (DRAMPHY_0__DRAM_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (DRAMPHY_0__DRAM_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (DRAMPHY_0__DRAM_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (DRAMPHY_0__DRAM_PBUS_0__APB__PRDATA)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_DRAM1_Subsystem
endmodule 
