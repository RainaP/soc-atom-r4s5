//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module DRAM_PBUS
#(
      parameter ABITS = 24
)
(
     input              CLK__APB
    ,input              RSTN__APB

    ,input              APB__PSEL
    ,input              APB__PENABLE
    ,input              APB__PWRITE
    ,input  [2:0]       APB__PPROT
    ,input  [ABITS-1:0] APB__PADDR
    ,input  [ 3:0]      APB__PSTRB
    ,input  [31:0]      APB__PWDATA
    ,output             APB__PREADY
    ,output             APB__PSLVERR
    ,output [31:0]      APB__PRDATA

    ,output             SCU_0__APB__PSEL
    ,output             SCU_0__APB__PENABLE
    ,output             SCU_0__APB__PWRITE
    ,output [2:0]       SCU_0__APB__PPROT
    ,output [12-1:0]    SCU_0__APB__PADDR
    ,output [ 3:0]      SCU_0__APB__PSTRB
    ,output [31:0]      SCU_0__APB__PWDATA
    ,input              SCU_0__APB__PREADY
    ,input              SCU_0__APB__PSLVERR
    ,input  [31:0]      SCU_0__APB__PRDATA

    ,output             DRAMCON_0__APB__PSEL
    ,output             DRAMCON_0__APB__PENABLE
    ,output             DRAMCON_0__APB__PWRITE
    ,output [2:0]       DRAMCON_0__APB__PPROT
    ,output [12-1:0]    DRAMCON_0__APB__PADDR
    ,output [ 3:0]      DRAMCON_0__APB__PSTRB
    ,output [31:0]      DRAMCON_0__APB__PWDATA
    ,input              DRAMCON_0__APB__PREADY
    ,input              DRAMCON_0__APB__PSLVERR
    ,input  [31:0]      DRAMCON_0__APB__PRDATA

    ,output             DRAMPHY_0__APB__PSEL
    ,output             DRAMPHY_0__APB__PENABLE
    ,output             DRAMPHY_0__APB__PWRITE
    ,output [2:0]       DRAMPHY_0__APB__PPROT
    ,output [12-1:0]    DRAMPHY_0__APB__PADDR
    ,output [ 3:0]      DRAMPHY_0__APB__PSTRB
    ,output [31:0]      DRAMPHY_0__APB__PWDATA
    ,input              DRAMPHY_0__APB__PREADY
    ,input              DRAMPHY_0__APB__PSLVERR
    ,input  [31:0]      DRAMPHY_0__APB__PRDATA

);
    localparam DECBIT = 12;
    localparam DECMASK= (1<<(15-12+1))-1;

    wire [3-1:0] psels;
    assign psels[  0] = ( ((('h00_0000)>>DECBIT)&DECMASK) == APB__PADDR[15:12]); // SCU_0
    assign psels[  1] = ( ((('h00_1000)>>DECBIT)&DECMASK) == APB__PADDR[15:12]); // DRAMCON_0
    assign psels[  2] = ( ((('h00_2000)>>DECBIT)&DECMASK) == APB__PADDR[15:12]); // DRAMPHY_0

    assign SCU_0__APB__PSEL    = APB__PSEL && psels[  0];
    assign SCU_0__APB__PENABLE = APB__PENABLE;
    assign SCU_0__APB__PWRITE  = APB__PWRITE ;
    assign SCU_0__APB__PPROT   = APB__PPROT  ;
    assign SCU_0__APB__PADDR   = APB__PADDR  ;
    assign SCU_0__APB__PSTRB   = APB__PSTRB  ;
    assign SCU_0__APB__PWDATA  = APB__PWDATA ;

    assign DRAMCON_0__APB__PSEL    = APB__PSEL && psels[  1];
    assign DRAMCON_0__APB__PENABLE = APB__PENABLE;
    assign DRAMCON_0__APB__PWRITE  = APB__PWRITE ;
    assign DRAMCON_0__APB__PPROT   = APB__PPROT  ;
    assign DRAMCON_0__APB__PADDR   = APB__PADDR  ;
    assign DRAMCON_0__APB__PSTRB   = APB__PSTRB  ;
    assign DRAMCON_0__APB__PWDATA  = APB__PWDATA ;

    assign DRAMPHY_0__APB__PSEL    = APB__PSEL && psels[  2];
    assign DRAMPHY_0__APB__PENABLE = APB__PENABLE;
    assign DRAMPHY_0__APB__PWRITE  = APB__PWRITE ;
    assign DRAMPHY_0__APB__PPROT   = APB__PPROT  ;
    assign DRAMPHY_0__APB__PADDR   = APB__PADDR  ;
    assign DRAMPHY_0__APB__PSTRB   = APB__PSTRB  ;
    assign DRAMPHY_0__APB__PWDATA  = APB__PWDATA ;

    assign APB__PREADY = (0==APB__PSEL) 
        | (psels[  0] ? SCU_0__APB__PREADY : 0)
        | (psels[  1] ? DRAMCON_0__APB__PREADY : 0)
        | (psels[  2] ? DRAMPHY_0__APB__PREADY : 0)
        ;
    assign APB__PSLVERR = 0 
        | (psels[  0] ? SCU_0__APB__PSLVERR : 0)
        | (psels[  1] ? DRAMCON_0__APB__PSLVERR : 0)
        | (psels[  2] ? DRAMPHY_0__APB__PSLVERR : 0)
        ;
    assign APB__PRDATA = 0 
        | (psels[  0] ? SCU_0__APB__PRDATA : 0)
        | (psels[  1] ? DRAMCON_0__APB__PRDATA : 0)
        | (psels[  2] ? DRAMPHY_0__APB__PRDATA : 0)
        ;

`ifndef SYNTHESIS
    always@(posedge CLK__APB or negedge RSTN__APB )
        if( RSTN__APB && (0!=APB__PSEL) && (0==psels) ) begin
            $display( "*E:%s(%d): (%m)", `__FILE__, `__LINE__,$time );
            $display( "*E:Invalid address : %x", APB__PADDR );
            #100;
            $finish();
        end
`endif //SYNTHESIS

endmodule
