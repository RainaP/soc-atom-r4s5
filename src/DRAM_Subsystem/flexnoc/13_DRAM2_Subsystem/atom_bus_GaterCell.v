

`timescale 1ns/1ps
module atom_bus_GaterCell( CLKIN , CLKOUT , EN , RSTN , TE );
	input   CLKIN  ;
	output  CLKOUT ;
	input   EN     ;
	input   RSTN   ;
	input   TE     ;

// synthesis translate_off
`define USER_BEHAV_GATES
// synthesis translate_on

`ifdef USER_BEHAV_GATES

	reg  LatchedEn ;
	always @( CLKIN or (EN | TE) )		if ( CLKIN == 0)
			LatchedEn <= #0.001 ( EN | TE );
assign CLKOUT = LatchedEn && CLKIN;


`else
	PREICG_X4N_A6ZTL_C10 (.ECK(CLKOUT), .CK(CLKIN), .E( EN | TE ), .SE(1'b0));
`endif
endmodule



module VD_bus_GaterCell_RstAsync ( input CLKIN , output CLKOUT , input EN , input RSTN , input TE );
	atom_bus_GaterCell u_atom_bus_GaterCell( .CLKIN(CLKIN) , .CLKOUT(CLKOUT) , .EN(EN) , .RSTN(RSTN) , .TE(TE) );
endmodule


module GaterCell_RstAsync ( input CLKIN , output CLKOUT , input EN , input RSTN , input TE );
	atom_bus_GaterCell u_atom_bus_GaterCell( .CLKIN(CLKIN) , .CLKOUT(CLKOUT) , .EN(EN) , .RSTN(RSTN) , .TE(TE) );
endmodule


module VD_cpu_GaterCell_RstAsync( input CLKIN , output CLKOUT , input EN , input RSTN , input TE );
	atom_bus_GaterCell u_atom_bus_GaterCell( .CLKIN(CLKIN) , .CLKOUT(CLKOUT) , .EN(EN) , .RSTN(RSTN) , .TE(TE) );
endmodule


module VD_dcluster0_GaterCell_RstAsync( input CLKIN , output CLKOUT , input EN , input RSTN , input TE );
	atom_bus_GaterCell u_atom_bus_GaterCell( .CLKIN(CLKIN) , .CLKOUT(CLKOUT) , .EN(EN) , .RSTN(RSTN) , .TE(TE) );
endmodule


module VD_dcluster1_GaterCell_RstAsync( input CLKIN , output CLKOUT , input EN , input RSTN , input TE );
	atom_bus_GaterCell u_atom_bus_GaterCell( .CLKIN(CLKIN) , .CLKOUT(CLKOUT) , .EN(EN) , .RSTN(RSTN) , .TE(TE) );
endmodule

module VD_dram_left_GaterCell_RstAsync( input CLKIN , output CLKOUT , input EN , input RSTN , input TE );
	atom_bus_GaterCell u_atom_bus_GaterCell( .CLKIN(CLKIN) , .CLKOUT(CLKOUT) , .EN(EN) , .RSTN(RSTN) , .TE(TE) );
endmodule

module VD_dram_right_GaterCell_RstAsync( input CLKIN , output CLKOUT , input EN , input RSTN , input TE );
	atom_bus_GaterCell u_atom_bus_GaterCell( .CLKIN(CLKIN) , .CLKOUT(CLKOUT) , .EN(EN) , .RSTN(RSTN) , .TE(TE) );
endmodule

module VD_pcie_GaterCell_RstAsync( input CLKIN , output CLKOUT , input EN , input RSTN , input TE );
	atom_bus_GaterCell u_atom_bus_GaterCell( .CLKIN(CLKIN) , .CLKOUT(CLKOUT) , .EN(EN) , .RSTN(RSTN) , .TE(TE) );
endmodule





