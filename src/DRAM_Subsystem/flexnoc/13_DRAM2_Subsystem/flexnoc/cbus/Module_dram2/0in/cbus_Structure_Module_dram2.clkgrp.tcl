
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:20

####################################################################

netlist clock Regime_dram2_Cm_main.root_Clk -module cbus_Structure_Module_dram2 -group clk_dram2 -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_dram2_Cm_main.root_Clk_ClkS -module cbus_Structure_Module_dram2 -group clk_dram2 -period 1.000 -waveform {0.000 0.500}
netlist clock clk_dram2 -module cbus_Structure_Module_dram2 -group clk_dram2 -period 1.000 -waveform {0.000 0.500}

