
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:20

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_m2ctrl_astResp001_main.DtpTxClkAdapt_Link_m2ctrl_asiResp001_Async.WrCnt -graycode -module cbus_Structure_Module_dram2
cdc signal Link_m2ctrl_ast_main.DtpRxClkAdapt_Link3_Async.RdCnt -graycode -module cbus_Structure_Module_dram2

