
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:20

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy
arteris_customer_cell -module "ClockManager_Regime_dram2_Cm" -type "ClockManagerCell" -instance_name "${CUSTOMER_HIERARCHY}Regime_dram2_Cm_main/ClockManager/IClockManager_Regime_dram2_Cm"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_astResp001_main/DtpTxClkAdapt_Link_m2ctrl_asiResp001_Async/urs/Isc0"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_astResp001_main/DtpTxClkAdapt_Link_m2ctrl_asiResp001_Async/urs/Isc1"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_astResp001_main/DtpTxClkAdapt_Link_m2ctrl_asiResp001_Async/urs/Isc2"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_astResp001_main/DtpTxClkAdapt_Link_m2ctrl_asiResp001_Async/urs0/Isc0"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_astResp001_main/DtpTxClkAdapt_Link_m2ctrl_asiResp001_Async/urs1/Isc0"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_ast_main/DtpRxClkAdapt_Link3_Async/urs/Isc0"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_ast_main/DtpRxClkAdapt_Link3_Async/urs/Isc1"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_ast_main/DtpRxClkAdapt_Link3_Async/urs/Isc2"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_ast_main/DtpRxClkAdapt_Link3_Async/urs0/Isc0"
arteris_customer_cell -module "VD_dram_right_SynchronizerCell_RstAsync" -type "SynchronizerCell" -instance_name "${CUSTOMER_HIERARCHY}Link_m2ctrl_ast_main/DtpRxClkAdapt_Link3_Async/urs1/Isc0"
arteris_customer_cell -module "VD_dram_right_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m2ctrl_astResp001_main_Sys/ClockGater/usce4720c4c9/instGaterCell" -clock "Regime_dram2_Cm_root" -clock_period "${Regime_dram2_Cm_root_P}"
arteris_customer_cell -module "VD_dram_right_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_Link_m2ctrl_ast_main_Sys/ClockGater/usce4720c4c9/instGaterCell" -clock "Regime_dram2_Cm_root" -clock_period "${Regime_dram2_Cm_root_P}"
arteris_customer_cell -module "VD_dram_right_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_dram2_ctrl/ClockGater/usce4720c4c9/instGaterCell" -clock "Regime_dram2_Cm_root" -clock_period "${Regime_dram2_Cm_root_P}"
arteris_customer_cell -module "VD_dram_right_GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_dram2_ctrl_T/ClockGater/usce4720c4c9/instGaterCell" -clock "Regime_dram2_Cm_root" -clock_period "${Regime_dram2_Cm_root_P}"

