
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:20

####################################################################

arteris_gen_clock -name "Regime_dram2_Cm_root" -pin "Regime_dram2_Cm_main/root_Clk Regime_dram2_Cm_main/root_Clk_ClkS" -clock_domain "clk_dram2" -spec_domain_clock "/Regime_dram2/Cm/root" -divide_by "1" -source "clk_dram2" -source_period "${clk_dram2_P}" -source_waveform "[expr ${clk_dram2_P}*0.00] [expr ${clk_dram2_P}*0.50]" -user_directive "" -add "FALSE"
