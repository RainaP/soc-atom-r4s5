
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:33

####################################################################

# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_dram2"]

# Create Test Mode 

set_ideal_network -no_propagate [get_ports "TM"]

set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram2_r_Ar_Ready"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram2_r_R_Data"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram2_r_R_Id"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram2_r_R_Last"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram2_r_R_Resp"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram2_r_R_User"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram2_r_R_Valid"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m2_asi_to_Link_m2_ast_Data"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRst"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m2_asi_to_Link_m2_ast_WrCnt"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdCnt"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_internal_comb_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m2_astResp001_to_Link_m2_asiResp001_RdPtr"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Addr"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Burst"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Cache"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Id"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Len"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Lock"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Prot"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Size"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_User"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_Ar_Valid"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram2_r_R_Ready"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m2_asi_to_Link_m2_ast_RdCnt"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m2_asi_to_Link_m2_ast_RdPtr"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m2_asi_to_Link_m2_ast_RxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m2_asi_to_Link_m2_ast_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_internal_comb_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m2_astResp001_to_Link_m2_asiResp001_Data"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m2_astResp001_to_Link_m2_asiResp001_RxCtl_PwrOnRst"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m2_astResp001_to_Link_m2_asiResp001_TxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_dram2_Cm_root [expr ${Regime_dram2_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m2_astResp001_to_Link_m2_asiResp001_WrCnt"]

