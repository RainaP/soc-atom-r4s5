
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:24

####################################################################

arteris_clock -name "clk_dram2" -clock_domain "clk_dram2" -port "clk_dram2" -period "${clk_dram2_P}" -waveform "[expr ${clk_dram2_P}*0.00] [expr ${clk_dram2_P}*0.50]" -edge "R" -user_directive ""
