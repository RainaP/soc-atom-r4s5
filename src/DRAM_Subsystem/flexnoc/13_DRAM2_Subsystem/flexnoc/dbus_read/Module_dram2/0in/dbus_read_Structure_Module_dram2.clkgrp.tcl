
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:24

####################################################################

netlist clock Regime_dram2_Cm_main.root_Clk -module dbus_read_Structure_Module_dram2 -group clk_dram2 -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_dram2_Cm_main.root_Clk_ClkS -module dbus_read_Structure_Module_dram2 -group clk_dram2 -period 1.000 -waveform {0.000 0.500}
netlist clock clk_dram2 -module dbus_read_Structure_Module_dram2 -group clk_dram2 -period 1.000 -waveform {0.000 0.500}

