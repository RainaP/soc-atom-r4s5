
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:16

////////////////////////////////////////////////////////////////////

module dbus_write_Structure_Module_dram2_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_write_Structure_Module_dram2 -severity waived -scheme reconvergence -from_signals Link_m2_astResp001_main.DtpTxClkAdapt_Link_m2_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_dram2 -severity waived -scheme reconvergence -from_signals Link_m2_ast_main.DtpRxClkAdapt_Link_m2_asi_Async.RdCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_write_Structure_Module_dram2 -severity waived -scheme multi_sync_mux_select -from Link_m2_ast_main.DtpRxClkAdapt_Link_m2_asi_Async.uRegSync.instSynchronizerCell*

endmodule
