
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:16

####################################################################

arteris_hierarchy -module "dbus_write_Structure_Module_dram2_Link_m2_astResp001_main" -generator "DatapathLink" -instance_name "Link_m2_astResp001_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_Link_m2_ast_main" -generator "DatapathLink" -instance_name "Link_m2_ast_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_Regime_dram2_Cm_main" -generator "ClockManager" -instance_name "Regime_dram2_Cm_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_clockGaters_Link_m2_astResp001_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_m2_astResp001_main_Sys" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_clockGaters_Link_m2_ast_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_m2_ast_main_Sys" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_clockGaters_dram2_w" -generator "ClockGater" -instance_name "clockGaters_dram2_w" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_clockGaters_dram2_w_T" -generator "ClockGater" -instance_name "clockGaters_dram2_w_T" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_dram2_w_T_main" -generator "T2G" -instance_name "dram2_w_T_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_dram2_w_main" -generator "G2S" -instance_name "dram2_w_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_z_T_C_S_C_L_R_A_4_1" -generator "gate" -instance_name "uAnd4" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2_z_T_C_S_C_L_R_O_4_1" -generator "gate" -instance_name "uOr4" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram2" -generator "" -instance_name "dbus_write_Structure_Module_dram2" -level "1"
