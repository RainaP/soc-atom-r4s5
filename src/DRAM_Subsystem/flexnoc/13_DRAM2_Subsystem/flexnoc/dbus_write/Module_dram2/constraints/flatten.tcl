if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2_astResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2_astResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2_astResp001_main/DtpTxClkAdapt_Link_m2_asiResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2_astResp001_main/DtpTxClkAdapt_Link_m2_asiResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2_ast_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2_ast_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_m2_ast_main/DtpRxClkAdapt_Link_m2_asi_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_m2_ast_main/DtpRxClkAdapt_Link_m2_asi_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dram2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dram2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dram2_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dram2_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dram2_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dram2_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_astResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_astResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_astResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_astResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_ast_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_ast_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_ast_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_m2_ast_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dram2_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dram2_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dram2_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dram2_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dram2_w_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dram2_w_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_dram2_w_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_dram2_w_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/DtpTxSerAdapt_Link_m2_astResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/DtpTxSerAdapt_Link_m2_astResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}dram2_w_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}dram2_w_main/GenericToSpecific false
}
