//--------------------------------------------------------------------------------
//
//   Rebellions Inc. Confidential
//   All Rights Reserved -- Property of Rebellions Inc. 
//
//   Description : 
//
//   Author : jsyoon@rebellions.ai
//
//   date : 2021/08/07
//
//--------------------------------------------------------------------------------

module DRAM1_Subsystem(

	input         TM                                                                    ,
	input         arstn_dram1                                                           ,
	input         clk_dram1                                                             ,

	// cbus
	input  [57:0] ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data                            ,
	output [2:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt                           ,
	output [1:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr                           ,
	input         ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst                  ,
	output        ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck               ,
	output        ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst                  ,
	input         ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]  ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt                           ,
	output [57:0] ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data              ,
	input  [2:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt             ,
	input  [1:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr             ,
	output        ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst    ,
	input         ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck ,
	input         ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst    ,
	output        ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]  ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt             ,

	// dbus_read
	input  [128:0] r_dp_Link_m1_asi_to_Link_m1_ast_Data                            ,
	output [2:0]   r_dp_Link_m1_asi_to_Link_m1_ast_RdCnt                           ,
	output [2:0]   r_dp_Link_m1_asi_to_Link_m1_ast_RdPtr                           ,
	input          r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst                  ,
	output         r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck               ,
	output         r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst                  ,
	input          r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]   r_dp_Link_m1_asi_to_Link_m1_ast_WrCnt                           ,
	output [705:0] r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data              ,
	input  [2:0]   r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt             ,
	input  [2:0]   r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr             ,
	output         r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst    ,
	input          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck ,
	input          r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst    ,
	output         r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]   r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt             ,

	// dbus_write
	input  [702:0] w_dp_Link_m1_asi_to_Link_m1_ast_Data                            ,
	output [2:0]   w_dp_Link_m1_asi_to_Link_m1_ast_RdCnt                           ,
	output [2:0]   w_dp_Link_m1_asi_to_Link_m1_ast_RdPtr                           ,
	input          w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst                  ,
	output         w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck               ,
	output         w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst                  ,
	input          w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck               ,
	input  [2:0]   w_dp_Link_m1_asi_to_Link_m1_ast_WrCnt                           ,
	output [125:0] w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data              ,
	input  [2:0]   w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt             ,
	input  [2:0]   w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr             ,
	output         w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst    ,
	input          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck ,
	input          w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst    ,
	output         w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck ,
	output [2:0]   w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt             


);



	cbus_Structure_Module_dram1 u_cbus_Structure_Module_dram1(
		.TM( TM )
	,	.arstn_dram1( arstn_dram1 )
	,	.clk_dram1( clk_dram1 )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt( ctrl_dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt( ctrl_dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt )
	,	.dram1_ctrl_Ar_Addr( dram1_ctrl_Ar_Addr )
	,	.dram1_ctrl_Ar_Burst( dram1_ctrl_Ar_Burst )
	,	.dram1_ctrl_Ar_Cache( dram1_ctrl_Ar_Cache )
	,	.dram1_ctrl_Ar_Id( dram1_ctrl_Ar_Id )
	,	.dram1_ctrl_Ar_Len( dram1_ctrl_Ar_Len )
	,	.dram1_ctrl_Ar_Lock( dram1_ctrl_Ar_Lock )
	,	.dram1_ctrl_Ar_Prot( dram1_ctrl_Ar_Prot )
	,	.dram1_ctrl_Ar_Ready( dram1_ctrl_Ar_Ready )
	,	.dram1_ctrl_Ar_Size( dram1_ctrl_Ar_Size )
	,	.dram1_ctrl_Ar_Valid( dram1_ctrl_Ar_Valid )
	,	.dram1_ctrl_Aw_Addr( dram1_ctrl_Aw_Addr )
	,	.dram1_ctrl_Aw_Burst( dram1_ctrl_Aw_Burst )
	,	.dram1_ctrl_Aw_Cache( dram1_ctrl_Aw_Cache )
	,	.dram1_ctrl_Aw_Id( dram1_ctrl_Aw_Id )
	,	.dram1_ctrl_Aw_Len( dram1_ctrl_Aw_Len )
	,	.dram1_ctrl_Aw_Lock( dram1_ctrl_Aw_Lock )
	,	.dram1_ctrl_Aw_Prot( dram1_ctrl_Aw_Prot )
	,	.dram1_ctrl_Aw_Ready( dram1_ctrl_Aw_Ready )
	,	.dram1_ctrl_Aw_Size( dram1_ctrl_Aw_Size )
	,	.dram1_ctrl_Aw_Valid( dram1_ctrl_Aw_Valid )
	,	.dram1_ctrl_B_Id( dram1_ctrl_B_Id )
	,	.dram1_ctrl_B_Ready( dram1_ctrl_B_Ready )
	,	.dram1_ctrl_B_Resp( dram1_ctrl_B_Resp )
	,	.dram1_ctrl_B_Valid( dram1_ctrl_B_Valid )
	,	.dram1_ctrl_R_Data( dram1_ctrl_R_Data )
	,	.dram1_ctrl_R_Id( dram1_ctrl_R_Id )
	,	.dram1_ctrl_R_Last( dram1_ctrl_R_Last )
	,	.dram1_ctrl_R_Ready( dram1_ctrl_R_Ready )
	,	.dram1_ctrl_R_Resp( dram1_ctrl_R_Resp )
	,	.dram1_ctrl_R_Valid( dram1_ctrl_R_Valid )
	,	.dram1_ctrl_W_Data( dram1_ctrl_W_Data )
	,	.dram1_ctrl_W_Last( dram1_ctrl_W_Last )
	,	.dram1_ctrl_W_Ready( dram1_ctrl_W_Ready )
	,	.dram1_ctrl_W_Strb( dram1_ctrl_W_Strb )
	,	.dram1_ctrl_W_Valid( dram1_ctrl_W_Valid )
	);

	dbus_read_Structure_Module_dram1 u_dbus_read_Structure_Module_dram1(
		.TM( TM )
	,	.arstn_dram1( arstn_dram1 )
	,	.clk_dram1( clk_dram1 )
	,	.dp_Link_m1_asi_to_Link_m1_ast_Data( r_dp_Link_m1_asi_to_Link_m1_ast_Data )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RdCnt( r_dp_Link_m1_asi_to_Link_m1_ast_RdCnt )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RdPtr( r_dp_Link_m1_asi_to_Link_m1_ast_RdPtr )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst( r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck( r_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst( r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck( r_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1_asi_to_Link_m1_ast_WrCnt( r_dp_Link_m1_asi_to_Link_m1_ast_WrCnt )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt( r_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt )
	,	.dram1_r_Ar_Addr( dram1_r_Ar_Addr )
	,	.dram1_r_Ar_Burst( dram1_r_Ar_Burst )
	,	.dram1_r_Ar_Cache( dram1_r_Ar_Cache )
	,	.dram1_r_Ar_Id( dram1_r_Ar_Id )
	,	.dram1_r_Ar_Len( dram1_r_Ar_Len )
	,	.dram1_r_Ar_Lock( dram1_r_Ar_Lock )
	,	.dram1_r_Ar_Prot( dram1_r_Ar_Prot )
	,	.dram1_r_Ar_Ready( dram1_r_Ar_Ready )
	,	.dram1_r_Ar_Size( dram1_r_Ar_Size )
	,	.dram1_r_Ar_User( dram1_r_Ar_User )
	,	.dram1_r_Ar_Valid( dram1_r_Ar_Valid )
	,	.dram1_r_R_Data( dram1_r_R_Data )
	,	.dram1_r_R_Id( dram1_r_R_Id )
	,	.dram1_r_R_Last( dram1_r_R_Last )
	,	.dram1_r_R_Ready( dram1_r_R_Ready )
	,	.dram1_r_R_Resp( dram1_r_R_Resp )
	,	.dram1_r_R_User( dram1_r_R_User )
	,	.dram1_r_R_Valid( dram1_r_R_Valid )
	);


	dbus_write_Structure_Module_dram1 u_dbus_write_Structure_Module_dram1(
		.TM( TM )
	,	.arstn_dram1( arstn_dram1 )
	,	.clk_dram1( clk_dram1 )
	,	.dp_Link_m1_asi_to_Link_m1_ast_Data( w_dp_Link_m1_asi_to_Link_m1_ast_Data )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RdCnt( w_dp_Link_m1_asi_to_Link_m1_ast_RdCnt )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RdPtr( w_dp_Link_m1_asi_to_Link_m1_ast_RdPtr )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst( w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRst )
	,	.dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck( w_dp_Link_m1_asi_to_Link_m1_ast_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst( w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRst )
	,	.dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck( w_dp_Link_m1_asi_to_Link_m1_ast_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1_asi_to_Link_m1_ast_WrCnt( w_dp_Link_m1_asi_to_Link_m1_ast_WrCnt )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_Data )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdCnt )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RdPtr )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRst )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRst )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt( w_dp_Link_m1_astResp001_to_Link_m1_asiResp001_WrCnt )
	,	.dram1_w_Aw_Addr( dram1_w_Aw_Addr )
	,	.dram1_w_Aw_Burst( dram1_w_Aw_Burst )
	,	.dram1_w_Aw_Cache( dram1_w_Aw_Cache )
	,	.dram1_w_Aw_Id( dram1_w_Aw_Id )
	,	.dram1_w_Aw_Len( dram1_w_Aw_Len )
	,	.dram1_w_Aw_Lock( dram1_w_Aw_Lock )
	,	.dram1_w_Aw_Prot( dram1_w_Aw_Prot )
	,	.dram1_w_Aw_Ready( dram1_w_Aw_Ready )
	,	.dram1_w_Aw_Size( dram1_w_Aw_Size )
	,	.dram1_w_Aw_User( dram1_w_Aw_User )
	,	.dram1_w_Aw_Valid( dram1_w_Aw_Valid )
	,	.dram1_w_B_Id( dram1_w_B_Id )
	,	.dram1_w_B_Ready( dram1_w_B_Ready )
	,	.dram1_w_B_Resp( dram1_w_B_Resp )
	,	.dram1_w_B_User( dram1_w_B_User )
	,	.dram1_w_B_Valid( dram1_w_B_Valid )
	,	.dram1_w_W_Data( dram1_w_W_Data )
	,	.dram1_w_W_Last( dram1_w_W_Last )
	,	.dram1_w_W_Ready( dram1_w_W_Ready )
	,	.dram1_w_W_Strb( dram1_w_W_Strb )
	,	.dram1_w_W_Valid( dram1_w_W_Valid )
	);



endmodule
