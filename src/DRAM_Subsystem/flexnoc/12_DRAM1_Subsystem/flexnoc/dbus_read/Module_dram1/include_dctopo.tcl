#
proc arteris_max {val1 val2} {
 set max $val2
 if {$val1 > $val2} {
  set max $val1
 }
 return $max
}
proc arteris_min {val1 val2} {
 set min $val2
 if {$val1 < $val2} {
  set min $val1
 }
 return $min
}
proc arteris_get_tmp_file { } {
 file mkdir [ concat "/tmp/arteris_[sh whoami]" ]
 return [ concat "/tmp/arteris_[sh whoami]/[sh echo $$]" ]
}

#
proc arteris_get_register { net_name } {
 foreach_in_collection obj [all_fanin -flat -to $net_name] {
  if {[regexp {([^/]*)/(.+)_reg/(.+)} [get_object_name $obj]]} {
   set reg_name [get_object_name $obj]
  }
 }
 return $reg_name
}


#
### arteris_filter_output #######################################################################################
# arteris_filter_output -tcl_cmd "analyze -format verilog -library WORK $ARTERIS_INPUT_DIR/rtl.ClockManagerCell.v" -filter_cmd { /bin/egrep -v (VER-130) }
# arteris_filter_output -tcl_cmd { analyze -format verilog -library WORK rtl.ClockManagerCell.v } -filter_cmd { /bin/egrep -v (VER-130) }
# arteris_filter_output -tcl_cmd { analyze -format verilog -library WORK rtl.ClockManagerCell.v } -filter_cmd { /bin/egrep -v (VER-130) | grep -v source}
# arteris_filter_output -tcl_cmd { analyze -format verilog -library WORK rtl.ClockManagerCell.v } -filter_cmd { /bin/egrep -v (VER-130) | tr [a-z] [A-Z] } 
#
proc arteris_filter_output { args } {
 #
 set state     "flag"
 
 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -tcl_cmd     { set state "tcl_cmd"    }
     -filter_cmd  { set state "filter_cmd" }
    }
   }
   tcl_cmd     { set tcl_cmd    $arg ; set state "flag" }
   filter_cmd  { set filter_cmd $arg ; set state "flag" }
  }
 }

 set status [catch { open |[concat $filter_cmd [list >&@ stdout]] w } pipe]

 if { $status == 0 } {
  redirect -channel $pipe $tcl_cmd
  close $pipe
 } else {
  return -code error [format "function arteris_filter_output : cannot open pipe for command `%s'" $filter_cmd]
 }
 
 return
}


#
# Update library in memory to include external wire load description
# arteris_update_lib_wlm (no argument - only global variable: A_LIBRARY_INFO)
#
proc arteris_update_lib_wlm {} {
 #
 global A_LIBRARY_INFO
 global ARTERIS_INPUT_DIR ARTERIS_LIB_DIR

 foreach { _lib _val } [array get A_LIBRARY_INFO] {
  upvar $_lib $_lib
  if { [info exists [format "%s(wire_load_model)" $_lib]] && ![info exists [format "%s(library_name)" $_lib]] } {
   set wireload_path [eval format "%s" [format "$%s(wireload_path)" $_lib]]
   foreach { __lib __val } [array get A_LIBRARY_INFO] {
    upvar $__lib $__lib
    if { [info exists [format "%s(library_name)" $__lib]] && ![info exists [format "%s(wire_load_table)" $__lib]] } {
     set library_name [eval format "%s" [format "$%s(library_name)" $__lib]]
     set library_path [eval format "%s" [format "$%s(library_path)" $__lib]]
     set cmd [ concat "update_lib" [eval format "%s:%s" $library_path $library_name] [eval format "%s" $wireload_path] ]
     puts " launching : `$cmd'" ; eval $cmd
    }
   }
  }
 }

}
# Add set_dont_use property on specific library cell to be not used
# arteris_set_dont_use_lib_cell (no argument - only global variable: A_LIBRARY_INFO)
proc arteris_set_dont_use_lib_cell {} {
 #
 global A_LIBRARY_INFO

 set dont_use_cell ""
 foreach { _lib _val } [array get A_LIBRARY_INFO] {
  upvar $_lib $_lib
  if { [info exists [format "%s(dont_use_cell)" $_lib]] } {
   lset dont_use_cell [eval format "%s" [format "$%s(dont_use_cell)" $_lib]]
   set library_name [eval format "%s" [format "$%s(library_name)" $_lib]]
   foreach c_dont_use_cell $dont_use_cell {
    set_dont_use [get_lib_cells $library_name/$c_dont_use_cell]
    set cmd [concat "set_dont_use" [format "\[get_lib_cells %s/%s\]" $library_name $c_dont_use_cell]]
    puts " launching : `$cmd'" ; eval $cmd
   }
  }
 }

}


### Area reporting ##############################################################################################
#
proc arteris_extract_area { file_name } {
 #
 if [catch {open $file_name r} fp] {
  return -code error [format "function arteris_extract_area : cannot open temporary file: `%s'" $fp]
 } else {
  set area "void"
  foreach line [split [read $fp] \n] {
   if {[string match "Total *" $line]} {
     scan $line "%s %f %s %f" str0 nb_cell str1 area
     break
   }
  }
  close $fp

  return "$nb_cell:$area"
 }
 #
}

#
# Function to format report area output (especially in gate unit)
# arteris_get_area -type optl -ofile $ARTERIS_OUTPUT_DIR/optl.area
#
# In order to convert the area in gate unit, we use the elementary unit of the NAND2X1 (defined with
# the global variable: NAND2. This variable can be overwritten with the argument: -nand2
# arteris_get_area -nand2 NAND2X4 -wireload wireloadp45 -type optl -ofile $ARTERIS_OUTPUT_DIR/optl.area
#
proc arteris_get_area { args } {
 #
 global library
 set library_name "TO_BE_DEFINED"
 set area_unit "um2"
 if { [array exist library] && [info exist library(name)] } {
  set name      $library(name)
  set area_unit $library(area_unit)
 }
 global sh_product_version

 set state     "flag"
 set type      "void"
 set ofile     "void"
 set nand2     "void"

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -type     { set state "type"     }
     -ofile    { set state "ofile"     }
     -nand2    { set state "nand2"    }
    }
   }
   type     { set type  $arg ; set state "flag" }
   ofile    { set ofile $arg ; set state "flag" }
   nand2    { set nand2 $arg ; set state "flag" }
  }
 }
 #
 if {$type == "void"}  { return -code error [format "function arteris_get_area : compile step is not defined (optl \[|map\]"] }
 if {$ofile == "void"}  { return -code error [format "function arteris_get_area : output file is not defined"] }
 if {$nand2 == "void"} {
  global NAND2_FOR_AREA
  if {![info exists NAND2_FOR_AREA]} { return -code error [format "function arteris_get_area : NAND2 reference cell is not defined (use to compute area equivalent gate)"] }  
  set nand2 $NAND2_FOR_AREA
 }

 set wireload "void"
 redirect -variable msg { report_wire_load }
 foreach _line [split $msg \n] {
  if { [string match "Wire *load *model: *" $_line] } { scan $_line "%s %s %s %s" str1 str2 str3 wireload; break }
 }
 set current_design "void"
 redirect -variable msg { current_design }
 foreach _line [split $msg \n] {
  if { [string match "{*}" $_line] } { set current_design [string map {\{ {} \} {}} $_line]; break }
 }

 set sequential_nb_cell 0.0
 set sequential_area 0.0
 set dirname [file dirname $ofile]

 # specific check because some module doesn't have any register (for instance Hub)
 set cell_list [get_cells -hierarchical -filter "is_sequential==true && is_hierarchical==false"]
 if { [llength $cell_list] > 0 } {
  report_cell -nosplit $cell_list > $dirname/$type.report_cell.sequential
  scan [arteris_extract_area $dirname/$type.report_cell.sequential] "%f:%f" sequential_nb_cell sequential_area
 } else {
  redirect -file $dirname/$type.report_cell.sequential { puts "# No sequential cell has been founded in the design" }
 }

 report_cell -nosplit [get_cells -hierarchical -filter "is_combinational==true && is_hierarchical==false"] > $dirname/$type.report_cell.combinational
 scan [arteris_extract_area $dirname/$type.report_cell.combinational] "%f:%f" combinational_nb_cell combinational_area

  ##
  set interconnect_area 0.0
  set nand2_area [get_attribute [get_lib_cells $NAND2_FOR_AREA] area]
  set buffer_tax 1.03

  redirect -file $ofile {
   puts ""
   puts " DC version      : $sh_product_version"
   puts " Library         : $name"
   puts " Reference NAND2 : [format "%s - ( %.2f %s )" [get_attribute [get_lib_cells $NAND2_FOR_AREA] name] $nand2_area $area_unit]"
   puts ""
   puts "-----------------------------------------"
   puts " Design : $current_design"
   puts "-----------------------------------------"
   puts ""
   puts "  Report with DC - context : wire load model = $wireload"
   puts "                             (gate area only)"
   puts ""
   puts [format "Combinational area    :   %10.2f %s  (%6.0f nb of instances)" $combinational_area $area_unit $combinational_nb_cell]
   puts [format "Sequential area       :   %10.2f %s  (%6.0f nb of instances)" $sequential_area $area_unit $sequential_nb_cell]
   puts [format "Net Interconnect area :   %10.2f %s" $interconnect_area $area_unit]
   puts ""
   puts [format "Total cell area       :   %10.2f %s" [expr $combinational_area + $sequential_area] $area_unit]
   puts [format "Total area            :   %10.2f %s" [expr $combinational_area + $sequential_area + $interconnect_area] $area_unit]
   puts ""
   puts [format "Total equivalent gate :   %10.2f Kgate" [expr [expr $combinational_area + $sequential_area + $interconnect_area] / $nand2_area / 1000] ]
   puts ""
#  puts "-----------------------------------------------------------------"
#  puts "  Report with DC - context : estimation of bufferization after placement (+ [expr ($buffer_tax-1)*100] %)"
#  puts "                             (no scan insertion)"
#  puts "                             (gate area only)"
#  puts ""
#  puts [format "Total area            :   %10.2f um2"  [expr $combinational_area*$buffer_tax + $sequential_area*$buffer_tax + $interconnect_area] ]
#  puts ""
#  puts [format "Total equivalent gate :   %10.2f Kgate" [expr (($combinational_area*$buffer_tax + $sequential_area*$buffer_tax + $interconnect_area)/$nand2_area) / 1000] ]
#  puts ""
   # hierarchical area (better readability)
   arteris_hierarchical_report_area -ifile "$dirname/$type.report_cell.combinational; $dirname/$type.report_cell.sequential"
   puts ""
   # library cell distribution (quality of the result)
   arteris_report_library_cell_usage -ifile "$dirname/$type.report_cell.combinational; $dirname/$type.report_cell.sequential"
   puts ""
  }

 # specific area reported module by module
 report_cell -significant_digits 2 -nosplit [get_cells * -hier -filter "@is_hierarchical == true"] > $dirname/$type.report_cell.hierarchical

 set _cell [get_cells * -hier -filter "@is_unmapped == true"]
 if { [sizeof_collection $_cell] > 0} then {
  report_cell -significant_digits 2 -nosplit $_cell > $dirname/$type.report_cell.unmapped
 } else {
  redirect -f $dirname/$type.report_cell.unmapped { puts "No unmapped cell found in the design." }
 }

 set _cell [get_cells * -hier -filter "@is_black_box == true"]
 if { [sizeof_collection $_cell] > 0} then {
  report_cell -significant_digits 2 -nosplit $_cell > $dirname/$type.report_cell.black_box
 } else {
  redirect -f $dirname/$type.report_cell.black_box { puts "No black box cell found in the design." }
 }
 
}
proc arteris_hierarchical_report_area { args } {
 #
 global VERBOSE
 set state         "flag"
 set ifile          "optl.report_cell.combinational; optl.report_cell.sequential"
 set min_area       12.0

 set cmd ""
 foreach _args $args {
  switch -- $state {
   flag {
    switch -glob -- $_args {
     -ifile          { set state "ifile"    }
     -min_area       { set state "min_area" }
     default         { set state "default"  }
    }
   }
   ifile {
    set ifile [string map {{;} { }} $_args]
    set state "flag"
   }
   min_area {
    set min_area $_args
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 
 set ALL_icg 0; set ALL_register 0
 set ALL_area 0.0; set ALL_icg_area 0.0; set ALL_register_area 0.0; set ALL_combinational_area 0.0
 
 foreach file "$ifile" {
  set lnum 0
  if [catch {open $file r} fp] {
   return -code error [format "function arteris_hierarchical_report_area : cannot open file: `%s'" $file]
  } else {
   set header "false"
 
   ###
   while {[gets $fp line] >= 0} {
    incr lnum
 
    if { [string match "^#*" $line]        } { continue }
    if { [string match "---------*" $line] } { continue }
    if { [string match "Design : *" $line] } { scan $line "%s %s %s" str1 str2 design; continue }
    if { [string match "Cell *Reference *Library *Area *Attributes*" $line] && $header == "false" } { set header "true"; continue }
    if { [string match "Total *cells*" $line] && $header == "true" } { scan $line "%s %d %s %f" str1 _total_cell str2 _total_area; set header "false"; break }

    if { $header == "true" } {
     set _attribute ""
     scan [string map {{, } {,}} $line] "%s %s %s %f %s" _instance _cell _lib _area _attribute
     if { [info exists VERBOSE] } { puts [ format " instance: %s (cell: %s - area: %f - attribute: %s) (line=%d)" $_instance $_cell $_area $_attribute $lnum] }
 
     if { [string match "*cg*" $_attribute] } {
      set ALL_icg_area [expr $ALL_icg_area + $_area]; incr ALL_icg
     } elseif { [string match "*n*" $_attribute] } {
      set ALL_register_area [expr $ALL_register_area + $_area]; incr ALL_register
     } else {
      set ALL_combinational_area [expr $ALL_combinational_area + $_area]
     }
 
     # in case of gate present at the top level, the instance name becomes "." ("/" is not present in the instance name in this case)
     set _hier_name ""; set level 0
     foreach _inst [split [file dirname $_instance] "/"] {
     if { $_inst != "." } { incr level }
      set _hier_name [format "%s/%s" $_hier_name $_inst]
      if { ![info exists A_area($_hier_name)] } {
       set A_area($_hier_name) 0.0
       set A_icg_area($_hier_name) 0.0; set A_icg_nb($_hier_name) 0
       set A_register_area($_hier_name) 0.0; set A_register_nb($_hier_name) 0
       set A_combinational_area($_hier_name) 0.0
      }
      set A_area($_hier_name) [expr $A_area($_hier_name) + $_area]
      if { [string match "*cg*" $_attribute] } {
       incr A_icg_nb($_hier_name); set A_icg_area($_hier_name) [expr $A_icg_area($_hier_name) + $_area]
      } elseif { [string match "*n*" $_attribute] } {
       incr A_register_nb($_hier_name); set A_register_area($_hier_name) [expr $A_register_area($_hier_name) + $_area]
      } else {
       set A_combinational_area($_hier_name) [expr $A_combinational_area($_hier_name) + $_area]
      }
     }

    }
   }
   ###
   close $fp
  }
 }
 set ALL_area [expr $ALL_icg_area + $ALL_register_area + $ALL_combinational_area]

 puts [format "%s" "----------------------------------------- ------------------------ --------------------- -------------------------- ----------------------------"]
 puts [format "%s" " Instance                                   Total (um2)      (%)     Comb (um2)    (%)     Dffs (um2)    (%) (nb)     ICG (um2)     (%) (nb)"]
 puts [format "%s" "----------------------------------------- ------------------------ --------------------- -------------------------- ----------------------------"]
 puts [format "%-44s %10.2f (%6.2f)    %10.2f (%5.2f)    %10.2f (%5.2f) (%5d) %10.2f (%5.2f) (%5d)" \
  [format "%s%s" "/" $design] \
  $ALL_area \
  "100.0" \
  [expr $ALL_combinational_area] \
  [expr $ALL_combinational_area / $ALL_area * 100] \
  $ALL_register_area \
  [expr $ALL_register_area / $ALL_area * 100] \
  $ALL_register \
  $ALL_icg_area \
  [expr $ALL_icg_area / $ALL_area * 100] \
  $ALL_icg ]
 
 set l_instance [list]
 foreach {_name _val} [array get A_area] {
  # if area too low then instance is not printed in the report
  if { $A_area($_name) > $min_area } {
   lappend l_instance $_name
  } else {
   if { $_name == "/." } {
    set sep "."; set level 0
   } else {
    set level [expr [llength [split "$_name" "/"]] -1]
    set sep ""
    for {set i 1} {$i <= $level } {incr i 1} { set sep [format "%s--." $sep] }
   }
   set misc [format "%s%s%d" $sep "Others" $level]
   if { ![info exists A_area($misc)] } {
    lappend l_instance $misc
    set A_area($misc) 0.0
    set A_icg_area($misc) 0.0; set A_icg_nb($misc) 0
    set A_register_area($misc) 0.0; set A_register_nb($misc) 0
    set A_combinational_area($misc) 0.0
   }
   set A_area($misc) [expr $A_area($misc) + $A_area($_name)]
   if { $A_icg_area($_name) > 0 }           { set A_icg_area($misc) [expr $A_icg_area($misc) + $A_icg_area($_name)]; incr A_icg_nb($misc) }
   if { $A_register_area($_name) > 0 }      { set A_register_area($misc) [expr $A_register_area($misc) + $A_register_area($_name)]; incr A_register_nb($misc) }
   if { $A_combinational_area($_name) > 0 } { set A_combinational_area($misc) [expr $A_combinational_area($misc) + $A_combinational_area($_name)] }
  }
 }

 foreach _instance [lsort -ascii $l_instance] {
  set tail [file tail $_instance]
   
  set sep ""
  for {set i 1} {$i < [llength [split $_instance "/"]] } {incr i 1} { set sep [format "%s--." $sep] }

   puts [format "%-44s %10.2f (%6.2f)    %10.2f (%5.2f)    %10.2f (%5.2f) (%5d) %10.2f (%5.2f) (%5d)" \
    [format "%s%s" $sep $tail] \
    $A_area($_instance) \
    [expr $A_area($_instance) / $ALL_area * 100] \
    $A_combinational_area($_instance) \
    [expr $A_combinational_area($_instance) / $ALL_area * 100] \
    $A_register_area($_instance) \
    [expr $A_register_area($_instance) / $ALL_area * 100] \
    $A_register_nb($_instance) \
    $A_icg_area($_instance) \
    [expr $A_icg_area($_instance) / $ALL_area * 100] \
    $A_icg_nb($_instance) ]
 }

}
proc arteris_report_library_cell_usage { args } {
 #
 global VERBOSE
 set state         "flag"
 set ifile          "optl.report_cell.combinational; optl.report_cell.sequential"

 set cmd ""
 foreach _args $args {
  switch -- $state {
   flag {
    switch -glob -- $_args {
     -ifile          { set state "ifile"    }
     default         { set state "default"  }
    }
   }
   ifile {
    set ifile [string map {{;} { }} $_args]
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 
 set ALL_icg 0; set ALL_register 0
 set ALL_area 0.0; set ALL_icg_area 0.0; set ALL_register_area 0.0; set ALL_combinational_area 0.0
 
 foreach file "$ifile" {
  set lnum 0
  if [catch {open $file r} fp] {
   return -code error [format "function hierarchical_report_area : cannot open file: `%s'" $file]
  } else {
   set header "false"
 
   ###
   while {[gets $fp line] >= 0} {
    incr lnum
 
    if { [string match "^#*" $line]        } { continue }
    if { [string match "---------*" $line] } { continue }
    if { [string match "Design : *" $line] } { scan $line "%s %s %s" str1 str2 design; continue }
    if { [string match "Cell *Reference *Library *Area *Attributes*" $line] && $header == "false" } { set header "true"; continue }
    if { [string match "Total *cells*" $line] && $header == "true" } { scan $line "%s %d %s %f" str1 _total_cell str2 _total_area; set header "false"; break }

    if { $header == "true" } {
     set _attribute ""
     scan [string map {{, } {,}} $line] "%s %s %s %f %s" _instance _cell _lib _area _attribute
     if { [info exists VERBOSE] } { puts [ format " instance: %s (cell: %s - area: %f - library: %s) (line=%d)" $_instance $_cell $_area $_lib $lnum] }
 
     if { [string match "*cg*" $_attribute] } {
      set ALL_icg_area [expr $ALL_icg_area + $_area]; incr ALL_icg
     } elseif { [string match "*n*" $_attribute] } {
      set ALL_register_area [expr $ALL_register_area + $_area]; incr ALL_register
     } else {
      set ALL_combinational_area [expr $ALL_combinational_area + $_area]
     }
 
     if { ![info exists A_area($_lib)] } {
      set A_area($_lib) 0.0
      set A_icg_area($_lib) 0.0; set A_icg_nb($_lib) 0
      set A_register_area($_lib) 0.0; set A_register_nb($_lib) 0
      set A_combinational_area($_lib) 0.0
     }
     set A_area($_lib) [expr $A_area($_lib) + $_area]
     if { [string match "*cg*" $_attribute] } {
      incr A_icg_nb($_lib); set A_icg_area($_lib) [expr $A_icg_area($_lib) + $_area]
     } elseif { [string match "*n*" $_attribute] } {
      incr A_register_nb($_lib); set A_register_area($_lib) [expr $A_register_area($_lib) + $_area]
     } else {
      set A_combinational_area($_lib) [expr $A_combinational_area($_lib) + $_area]
     }

    }
   }
   close $fp
  }
 }
 set ALL_area [expr $ALL_icg_area + $ALL_register_area + $ALL_combinational_area]

 puts [format "%s" "----------------------------------------- ------------------------ --------------------- -------------------------- ----------------------------"]
 puts [format "%s" " Library cell distribution                  Total (um2)      (%)     Comb (um2)    (%)     Dffs (um2)    (%) (nb)     ICG (um2)     (%) (nb)"]
 puts [format "%s" "----------------------------------------- ------------------------ --------------------- -------------------------- ----------------------------"]
 
 set l_library [list]
 foreach {_name _val} [array get A_area] { lappend l_library $_name }

 foreach _library [lsort -ascii $l_library] {
   
  puts [format "%-44s %10.2f (%6.2f)    %10.2f (%5.2f)    %10.2f (%5.2f) (%5d) %10.2f (%5.2f) (%5d)" \
   [format "%s" $_library] \
   $A_area($_library) \
   [expr $A_area($_library) / $ALL_area * 100] \
   $A_combinational_area($_library) \
   [expr $A_combinational_area($_library) / $ALL_area * 100] \
   $A_register_area($_library) \
   [expr $A_register_area($_library) / $ALL_area * 100] \
   $A_register_nb($_library) \
   $A_icg_area($_library) \
   [expr $A_icg_area($_library) / $ALL_area * 100] \
   $A_icg_nb($_library) ]

 }
}


#
### Clock definition ############################################################################################
#
# arteris_clock -name "clk_cpu" -period "$clk_cpu_P" -waveform "[expr $clk_cpu_P*0.00] [expr $clk_cpu_P*0.50]" -domain "cpu" -edge "R" -port "clk_cpu" -user_directive "MASTER; skew=340ps; skew=10%"
# arteris_gen_clock -name "internalClk100" -divide_by "2" -source "Clk" -source_period "$internalClk_P" -source_waveform "[expr $internalClk_P*0.00] [expr $internalClk_P*0.50]" -source_domain "Clk_domain" -pin "Pllmodule/Clk200/out" -user_directive ""
#
proc arteris_create_clock { args } {
 #
 global TCL2SDC PARTIAL_EXPORT
 if { ![info exists TCL2SDC] } { set TCL2SDC "false" }
 if { ![info exists PARTIAL_EXPORT] } { set PARTIAL_EXPORT "false" }
 #
 set state         "flag"
 set name          "void"
 set period        "void"
 set waveform      "void"
 set divide_by     "void"
 set source        "void"
 set source_period "void"
 set port_or_pin   "void"
 set add           "void"

 set cmd ""
 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -name            { set state "name"          }
     -period          { set state "period"        }
     -waveform        { set state "waveform"      }
     -divide_by       { set state "divide_by"     }
     -source          { set state "source"        }
     -source_period   { set state "source_period" }
     -source_waveform { set state "waveform"      }
     -port            { set state "port"          }
     -pin             { set state "pin"           }
     -add             { set state "add"           }
     default          { set state "default"       }
    }
   }
   name {
    set name $arg
    set state "flag"
   }
   period {
    set period [format "%.3f" $arg]
    set state "flag"
   }
   waveform {
    scan $arg "%f %f" w0 w1
    set waveform [format "%.3f %.3f" $w0 $w1]
    set state "flag"
   }
   divide_by {
    set divide_by [format "%.0f" $arg]
    set state "flag"
   }
   source {
    set source $arg
    set state "flag"
   }
   source_period {
    set source_period [format "%.3f" $arg]
    set state "flag"
   }
   port {
    set port_or_pin [format "\[ get_ports { %s } \]" $arg]
    set state "flag"
   }
   pin {
    set port_or_pin [format "\[ get_pins { %s } \]" $arg]
    if { $PARTIAL_EXPORT == "true" } {
     #separator is supposed to be /
     set port_or_pin [format "\[ get_pins { %s/%s } \]" [get_xref [file dirname $arg]] [file tail $arg] ]
    }
    set state "flag"
   }
   add {
    set add ""
    if {$arg == "TRUE"} { set add "-add" }
    set state "add"
   }
   default { set state "flag" }
  }
 }

 if { [info procs "arteris_clock"] == "arteris_clock" } {
  set cmd [ format "create_clock -name \"%s\" -period %.3f -waveform \"%s\" %s" $name $period $waveform $port_or_pin ]

 } elseif { [info procs "arteris_virtual_clock"] == "arteris_virtual_clock" } {
  set cmd [ format "create_clock -name \"%s\" -period %.3f -waveform \"%s\"" $name $period $waveform ]

 } elseif { [info procs "arteris_gen_clock"] == "arteris_gen_clock" } {
  set cmd [ format "create_generated_clock -name \"%s\" -divide_by %.0f -source \"%s\" %s" $name $divide_by $source $port_or_pin $add ]

 } elseif { [info procs "arteris_gen_virtual_clock"] == "arteris_gen_virtual_clock" } {
  set cmd [ format "create_clock -name \"%s\" -period %.3f -waveform \"%s\"" $name [expr $source_period * $divide_by] $waveform ]

 } else {
  return -code error [format "function arteris_create_clock : call of pre-defined function which is not set"]
 }
 if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
}
#
#
proc arteris_set_clock_transition { args } {
 #
 global TCL2SDC
 if { ![info exists TCL2SDC] } { set TCL2SDC "false" }
 #
 set state          "flag"

 set cmd "set_clock_transition 0 "
 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -name          { set state "name"     }
     default        { set state "default"  }
    }
   }
   name {
    set cmd [concat $cmd [format " %s" $arg]]
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
}

#
### clock skew insertion (fixed or percentage) ##################################################################
#
# arteris_clock -name "clk_cpu" -period "$clk_cpu_P" -waveform "[expr $clk_cpu_P*0.00] [expr $clk_cpu_P*0.50]" -domain "cpu" -edge "R" -port "clk_cpu" -user_directive "MASTER; skew=340ps; skew=10%"
#
proc arteris_set_clock_uncertainty { args } {
 #
 global library arteris_dflt_CLOCK_UNCERTAINTY_PERCENT
 global TCL2SDC
 if { ![info exists TCL2SDC] } { set TCL2SDC "false" }
 #
 set T_Flex2library 1.0
 if { [array exist library] && [info exist library(T_Flex2library)] } {
  set T_Flex2library $library(T_Flex2library)
 }

 set state          "flag"
 set name           "void"
 set port           "void"
 set period         "void"
 set user_directive "void"
 set divide_by       1.0

 foreach _args $args {
  switch -- $state { 
   flag {
    switch -glob -- $_args {
     -name           { set state "name"           }
     -port           { set state "port"           }
     -period         { set state "period"         }
     -divide_by      { set state "divide_by"      }
     -source_period  { set state "source_period"  }
     -user_directive { set state "user_directive" }
     default         { set state "default"        }
    }
   }
   name {
    set name $_args
    set state "flag"
   }
   port {
    set port $_args
    set state "flag"
   }
   period {
    set period $_args
    set state "flag"
   }
   source_period {
    set period $_args
    set state "flag"
   }
   divide_by {
    set divide_by $_args
    set state "flag"
   }
   user_directive {
    set user_directive $_args
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 set skew_percentage 0.0
 set skew_constant   0.0

 # separator in synthesis_info field is `;'
 set user_directive [string map {{ } {}} $user_directive]
 foreach _arg [split $user_directive {;}] {
  switch -glob "$_arg" {
   skew=*% {
    scan $_arg {skew=%f%s} skew_percentage str_percent
  }
   skew=*ps {
    scan $_arg {skew=%f%s} skew_constant unit
    set skew_constant [expr ${skew_constant} * $T_Flex2library / 1000]
  }
   skew=*ns {
    scan $_arg {skew=%f%s} skew_constant unit
    set skew_constant [expr ${skew_constant} * $T_Flex2library]
   }
   default { }
  }
 }

 if {$skew_percentage == 0.0 && $skew_constant == 0.0 } {
  if {![info exists arteris_dflt_CLOCK_UNCERTAINTY_PERCENT]} {
   return -code error [format "function arteris_set_clock_uncertainty : arteris_dflt_CLOCK_UNCERTAINTY_PERCENT not defined"]
  }
  set skew_percentage [expr ${arteris_dflt_CLOCK_UNCERTAINTY_PERCENT}]
 }

 foreach _proc [info procs "arteris_*"] {
  switch -exact $_proc {

   arteris_clock {
    set cmd [format "set_clock_uncertainty %.3f \"%s\"" [expr ${skew_constant} + [expr ${period} * (${skew_percentage}/100.0)]] $name]
    if { $TCL2SDC == "true" } {
     puts "$cmd"
    } else {
     puts " launching : `$cmd' ($skew_constant + ${skew_percentage}% of period $period attached to master clock: $name)" ; eval $cmd
    }
   }

   arteris_gen_clock {
    set cmd [format "set_clock_uncertainty %.3f \"%s\"" [expr ${skew_constant} + [expr ${period}*${divide_by} * (${skew_percentage}/100.0)]] $name]
    if { $TCL2SDC == "true" } {
     puts "$cmd"
    } else {
     puts " launching : `$cmd' ($skew_constant + ${skew_percentage}% of period [expr ${period}*${divide_by}] attached to derived clock: $name)" ; eval $cmd
    }
   }

  }
 }

}


#
### setup margin for the clock-gating signal ####################################################################
#
# arteris_customer_cell -module "GaterCell_RstAsync" -type "GaterCell" -instance_name "${CUSTOMER_HIERARCHY}clockGaters_datapathSwitches_002/ClockGater/usce4c71b1c0/instGaterCell" -clock "mainRegime_Cm_root" -period "${mainRegime_Cm_root_P}"
#  set setup time constraint on the enable of the global clock gater (the value is a percentage control by the variable: GLOBAL_ICG_ENABLE_SETUP_PERCENT which is set in the optl.tcl file)
#
proc arteris_set_clock_gating_check_setup { args } {
 #
 global library GLOBAL_ICG_ENABLE_SETUP
 #
 set T_Flex2library 1.0
 if { [array exist library] && [info exist library(T_Flex2library)] } {
  set T_Flex2library $library(T_Flex2library)
 }
 #
 set state         "flag"
 set type          "void"
 set instance_name "void"
 set clock         "void"
 set clock_period  "void"

 set cmd " "

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -type          { set state "type"          }
     -instance_name { set state "instance_name" }
     -clock         { set state "clock"         }
     -clock_period  { set state "clock_period"  }
     default        { set state "default"       }
    }
   }
   type {
    set type $arg
    set state "flag"
   }
   instance_name {
    set instance_name $arg
    set state "flag"
   }
   clock {
    set clock $arg
    set state "flag"
   }
   clock_period {
    set clock_period $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if { $type == "GaterCell" } {

  set global_icg_enable_setup "void"
  switch -glob [string tolower [string map {{ } {}} "$GLOBAL_ICG_ENABLE_SETUP"]] {
   *% {
    scan $GLOBAL_ICG_ENABLE_SETUP {%f%s} percentage str_percent
    set global_icg_enable_setup [expr $clock_period*($percentage/100.0) * $T_Flex2library ]
    set msg [ format "setup value is %.3fps : %.0f%s of period %.3fps attached to clock: %s" ${global_icg_enable_setup} ${percentage} "%" ${clock_period} ${clock} ]
   }
   *ps {
    scan $GLOBAL_ICG_ENABLE_SETUP {%f%s} delay unit
    set global_icg_enable_setup [expr ${delay} * $T_Flex2library / 1000]
    set msg [ format "setup value is: %.3f" ${global_icg_enable_setup} ]
   }
   *ns {
    scan $GLOBAL_ICG_ENABLE_SETUP {%f%s} delay unit
    set global_icg_enable_setup [expr ${delay} * $T_Flex2library]
    set msg [ format "setup value is: %.3f" ${global_icg_enable_setup} ]
   }
   default { }
  }
  if { $global_icg_enable_setup == "void" } {
   return -code error [format "function arteris_set_clock_gating_check_setup : setup value for enable of global clock gater is not defined"]
  }

  # By convention the name of the enable at the interface of the customer cell is: EN
  set all_fanout [all_fanout -endpoints_only -flat -from ${instance_name}/EN]

  if { [sizeof_collection $all_fanout] > 1 } {
   return -code error [format "function arteris_set_clock_gating_check_setup : Founded more than one enable pin with the gater_cell `%s'" $instance_name]
  }

  foreach_in_collection _all_fanout $all_fanout {
   set _icg_pin [get_attribute $_all_fanout full_name]
   set cmd [format "set_clock_gating_check -setup %.3f %s" $global_icg_enable_setup $_icg_pin]
   puts " launching : `$cmd ($msg)'" ; eval $cmd
  }

 }
}


#
### creation of specific cell list used for path group setting ##################################################
#
# arteris_create_list -list all_registers -clock "l3_clk_Cm_l3_clk" -file "$ARTERIS_OUTPUT_DIR/list/all_registers.l3_clk_Cm_l3_clk"
#
proc arteris_create_list { args } {
 #
 set state   "flag"
 set list    "void"
 set clock   "void"
 set ofile   "void"
 set obj     "void"

 set cmd ""
 foreach _args $args {
  switch -- $state {
   flag {
    switch -glob -- $_args {
     -list   { set state "list"   }
     -clock  { set state "clock"   }
     -file   { set state "file"    }
     default { set state "default" }
    }
   }
   list {
    set list $_args
    set state "flag"
   }
   clock {
    set clock $_args
    set state "flag"
   }
   file {
    set ofile $_args
    if { ![file exist [file dirname $ofile]] } { file mkdir [file dirname $ofile] }
    file delete -force $ofile
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if {$ofile == "void"}  {
  return -code error [format "function arteris_create_list : output file is not defined"]
 }
 if [catch {open $ofile w} ofp] {
  return -code error [format "function arteris_create_list : cannot open output file: `%s'" $ofile]
 }

 switch -glob "$list" {
  all_clock_gates {
   set get_type "get_pins"
   set obj [all_clock_gates -test_pins -enable_pins -observation_pins -clock $clock]
  }
  all_inputs {
   set get_type "get_ports"
   if {$clock == "void"} {
    set obj [all_inputs]
   } else {
    set obj [all_inputs -clock $clock]
   }
  }
  all_registers {
   set get_type "get_cells"
   set obj [all_registers -clock $clock]
  }
  all_outputs {
   set get_type "get_ports"
   if {$clock == "void"} {
    set obj [all_outputs]
   } else {
    set obj [all_outputs -clock $clock]
   }
  }
  default { }
 }
 
 if { $obj != "" } {
  puts -nonewline $ofp [ format "list " ]
  foreach_in_collection _obj $obj {
   puts -nonewline $ofp [format "\[%s %s\] " $get_type [get_object_name $_obj] ]
  }
  puts $ofp "\n"
 }
 close $ofp
}
#
### check foreach path group if timing path is reported #########################################################
#
# check_report_timing -ofile $ARTERIS_OUTPUT_DIR/map.path__from_$n_clk\_to_$nn_clk.max.rpt
#
proc check_report_timing { args } {
 #
 global VERBOSE
 #
 set state "flag"
 set ofile "void"

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -ofile  { set state "ofile" }
     default { set state "default" }
    }
   }
   ofile   {
    set ofile $arg
    set state "flag"
   }
   default {
    set state "flag"
   }
  }
 }
 #

 if { [info exists VERBOSE] } { puts "# Working on file: $ofile" }

 if {$ofile == "void"} {
  return -code error [format "function check_report_timing: output file is not defined"]
 }
 if [catch {open $ofile r} ofp] {
   return -code error [format "function check_report_timing: cannot open output file: `%s'" $ofile]
 } else {

  if { [string match "*No paths.*" [read $ofp] ]} {
   if { [regexp {(.+?)__to_clock_gates_(.+?)\.(.+?)} $ofile match str to] } {
    puts stdout [format "# Info: no timing path founded to clock gate: `%s'" $to]
   } elseif { [ regexp {(.+?)__from_(.+?)_to_(.+?)\.(.+?)} $ofile match str from to ] } {
    puts stdout [format "# Info: no timing path founded from clock: `%s' to clock: `%s'" $from $to]
   }
   file delete $ofile
  }

  close $ofp
 }

}


#
### special_case ################################################################################################
#
# arteris_special -case "dont_touch" -instance_name "Req0/BarrelShifter/IntHdrVld" -lsb "" -msb ""
#
proc arteris_special_dont_touch { args } {
 #
 set state         "flag"
 set instance_name "void"
 set case          "void"
 set lsb           "void"
 set msb           "void"

 set cmd " "

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -case          { set state "case"          }
     -instance_name { set state "instance_name" }
     -lsb           { set state "lsb"           }
     -msb           { set state "msb"           }
     default        { set state "default"       }
    }
   }
   instance_name {
    set instance_name $arg
    set state "flag"
   }
   case {
    set case "set_dont_touch"
    set state "flag"
   }
   lsb {
    set lsb $arg
    set state "flag"
   }
   msb {
    set msb $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if { $lsb == {} && $msb == {} } {
  set cmd [concat $case [format "%s_reg" $instance_name] true]
  puts " launching : `$cmd'" ; eval $cmd
 } else {
  for {set i $lsb} {$i <= $msb} {incr i 1} {
   set cmd [concat $case [format "%s_reg_%d_" $instance_name $i] true]
   puts " launching : `$cmd'" ; eval $cmd
  } 
 }
}


#
### Input/Output constraint #####################################################################################
#
proc arteris_list_io_fanin_fanout_cone { args } {
 #
 global VERBOSE
 #
 set state          "flag"
 set port           "void"
 set ofile          "io_fanin_fanout_cone.rpt"

 set cmd ""
 foreach _args $args {
  switch -- $state {
   flag {
    switch -glob -- $_args {
     -port           { set state "port"    }
     -ofile          { set state "ofile"   }
     default         { set state "default" }
    }
   }
   port {
    set port $_args
    set state "flag"
   }
   ofile {
    set ofile $_args
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if [catch {open $ofile w} ofp] {
   return -code error [format "function arteris_io_type : cannot open output file: `%s'" $ofile]
 }

 set l_all_inputs [list]
 foreach_in_collection _all_inputs [all_inputs] { lappend l_all_inputs [get_attribute $_all_inputs full_name] }
 set l_all_outputs [list]
 foreach_in_collection _all_outputs [all_outputs] { lappend l_all_outputs [get_attribute $_all_outputs full_name] }

 if { [lsearch $l_all_inputs $port] > 0 } {
  set l_all_fanout [all_fanout -endpoints_only -flat -from [get_ports $port] ]
 } elseif { [lsearch $l_all_outputs $port] > 0 } {
  set l_all_fanin [all_fanin -startpoints_only -flat -to [get_ports $port] ]
 } else {

  # case of input
  foreach _input [lsort -dictionary $l_all_inputs] {
   set connected_to_output ""
   set connected_to_dff ""
   set _coma "" ; set __coma ""
   foreach_in_collection _all_fanout [sort_collection -dictionary [all_fanout -endpoints_only -flat -from [get_ports $_input]] full_name] {
    set n_all_fanout [get_attribute $_all_fanout full_name]
    if { [info exists VERBOSE] } { puts "   founded end point:  [get_object_name $_all_fanout]" }
    switch -glob $n_all_fanout {
     */* {
      set connected_to_dff [concat $connected_to_dff $_coma $n_all_fanout]
      set _coma ";"
     }
     default {
      puts stdout [ format "# Info: input port `%s' connected to output port: `%s'" $_input $n_all_fanout ]
      set connected_to_output [concat $connected_to_output $__coma $n_all_fanout ]
      set __coma ";"
     }
    }
   }
   puts $ofp [format "arteris_io_type -port \"%s\" -type \"input\" -connected_to_output \"%s\"" $_input $connected_to_output]
  }
  #case of output
  foreach _output [lsort -dictionary $l_all_outputs] {
   set connected_to_input ""
   set connected_to_dff ""
   set _coma "" ; set __coma ""
   foreach_in_collection _all_fanin [sort_collection -dictionary [all_fanin -startpoints_only -flat -to [get_ports $_output]] full_name] {
    set n_all_fanin [get_attribute $_all_fanin full_name]
    if { [info exists VERBOSE] } { puts "   founded start point:  [get_object_name $_all_fanin]" }
    switch -glob $n_all_fanin {
     */* {
      set connected_to_dff [concat $connected_to_dff $n_all_fanin]
      set _coma ";"
     }
     default {
      puts stdout [ format "# Info: output port `%s' connected to input port: `%s'" $_output $n_all_fanin ]
      set connected_to_input [concat $connected_to_input $n_all_fanin ]
      set __coma ";"
     }
    }
   }
   puts $ofp [format "arteris_io_type -port \"%s\" -type \"output\" -connected_to_input \"%s\"" $_output $connected_to_input]
  }

 }

 close $ofp
}
#
#arteris_input -port "ACP_Ar_Ready" -socket "ACP" -clock_domain "SysClk266_i" -clock "PLL_266_ClockManager_SysClk266" -spec_domain_clock "/PLL_266/ClockManager/SysClk266" -clock_period "$PLL_266_ClockManager_SysClk266_P" -type "SYNC" -input_delay "[expr $PLL_266_ClockManager_SysClk266_P*($arteris_dflt_ARRIVAL_PERCENT/100.0)]" -arrival_time "[expr $PLL_266_ClockManager_SysClk266_P*($arteris_dflt_ARRIVAL_PERCENT/100.0)]" -add_delay "FALSE" -lsb "" -msb "" -user_directive "input_delay=1603ps ; output_delay=20%"
#
# 3 ways are available to set the arrival time of an input:
#  .arteris_dflt_ARRIVAL_PERCENT (default value proposed during the export)
#  .setting the synthesisInfo field (FlexNoC GUI with parameter: input_delay=0.5ns [input_delay=200ps | input_delay=22% | arrival_time=1.0ns | arrival_time=12%]
#  .modifying the parameters: -input_delay (respectively -arrival_time) in the input.tcl file (modification made by hand)
#
# Precedence is: .synthesisInfo field (if used in the GUI)
#                .parameters: -input_delay (respectively -arrival_time) (compararison is made with the default value)
#                .variable: arteris_dflt_ARRIVAL_PERCENT (default value)
#
proc arteris_set_input_delay { args } {
 #
 global library arteris_dflt_CLOCK_UNCERTAINTY_PERCENT
 global TCL2SDC
 if { ![info exists TCL2SDC] } { set TCL2SDC "false" }
 #
 set T_Flex2library 1.0
 if { [array exist library] && [info exist library(T_Flex2library)] } {
  set T_Flex2library $library(T_Flex2library)
 }
 #
 global arteris_dflt_ARRIVAL_PERCENT
 global arteris_dflt_REQUIRED_PERCENT
 global arteris_comb_ARRIVAL_PERCENT
 global arteris_comb_REQUIRED_PERCENT
 #
 set state          "flag"
 set add_delay      "void"
 set clock          "void"
 set clock_period   "void"
 set port           "void"
 set port_type      "void"
 set input_delay    "void"
 set arrival_time   "void"
 set user_directive "void"
 set iofile         "void"

 set cmd ""
 foreach _args $args {
  switch -- $state {
   flag {
    switch -glob -- $_args {
     -add_delay      { set state "add_delay"      }
     -clock          { set state "clock"          }
     -clock_period   { set state "clock_period"   }
     -port           { set state "port"           }
     -portType       { set state "port_type"      }
     -input_delay    { set state "input_delay"    }
     -arrival_time   { set state "arrival_time"   }
     -user_directive { set state "user_directive" }
     -iofile         { set state "iofile"         }
     default         { set state "default"        }
    }
   }
   add_delay {
    set add_delay ""
    if {$_args == "TRUE"} { set add_delay "-add_delay" }
    set state "flag"
   }
   clock {
    set clock $_args
    set state "flag"
   }
   clock_period {
    set clock_period $_args
    set state "flag"
   }
   port {
    set port $_args
    set state "flag"
   }
   port_type {
    # separator in synthesis_info field is `;'
    set port_type [string map {{ } {}} $_args]
    set state "flag"
   }
   input_delay {
    set input_delay $_args
    set state "flag"
   }
   arrival_time {
    set arrival_time $_args
    set state "flag"
   }
   user_directive {
    # catch user directives added in the specification project in the field synthesisInfo
    # separator in synthesis_info field is `;'
    set user_directive [string map {{ } {}} $_args]
    foreach _arg [split $user_directive {;}] {
     # parameter -user_directive is alway the final parameter and then take precedence over previous setting for variable: -input_delay
     switch -glob "$_arg" {
      input_delay=*% {
       scan $_arg {input_delay=%f%s} input_delay_percentage str_percent
       set input_delay [expr $clock_period*($input_delay_percentage/100.0)]; set arrival_time $input_delay
      }
      input_delay=*ps {
       scan $_arg {input_delay=%f%s} input_delay str_unit
       set input_delay [expr ${input_delay} * $T_Flex2library / 1000]; set arrival_time $input_delay
      }
      input_delay=*ns {
       scan $_arg {input_delay=%f%s} input_delay str_unit
       set input_delay [expr ${input_delay} * $T_Flex2library]; set arrival_time $input_delay
      }
      arrival_time=*% {
       scan $_arg {arrival_time=%f%s} arrival_time_percentage str_percent
       set arrival_time [expr $clock_period*($arrival_time_percentage/100.0)]; set input_delay $arrival_time
      }
      arrival_time=*ps {
       scan $_arg {arrival_time=%f%s} arrival_time_delay str_unit
       set arrival_time [expr ${arrival_time_delay} * $T_Flex2library / 1000]; set input_delay $arrival_time
      }
      arrival_time=*ns {
       scan $_arg {arrival_time=%f%s} arrival_time_delay str_unit
       set arrival_time [expr ${arrival_time_delay} * $T_Flex2library]; set input_delay $arrival_time
      }
      default { }
     }
    }
    set state "flag"
   }
   iofile {
    set iofile $_args
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if { $clock == "NA" } {
  puts stdout [ format "# Warning: input port `%s' has no timing constraint (clock domain is unavailable)" $port ]
  return
 }
 # case of modification has been introduced by hand but not equal for parameter -input_delay and -arrival_time
 if { $input_delay != $arrival_time } {
# return -code error [format "function arteris_set_input_delay : parameter input_delay=`%.3f' and arrival_time=`%.3f' are not equivalent with port `%s'" $input_delay $arrival_time $port]
 }

 if { $port_type != "void" } {
   switch -glob "$port_type" {
    "LLI_PSI;ReceiveData;DDR"  {
     # Specific constraints for LLI_PSI mode with signal: Rx_C (Receive Clock), Rx_D (Receive Data)
     #  Rx_D is captured on the rising edge and falling edge of the Rx_C clock
     #  Rx_C is an input port feeds with the transmit clock (no arrival time constraint on this port)
     puts stdout [ format "Info: input port `%s' is the receive data feeds with the transmit data coming from Tx host" $port ]
     set cmd [format "set_input_delay %.3f -clock %s %s" [expr $clock_period*0.45] $clock "\[get_ports $port\]" ]
     if { $TCL2SDC == "true" } { puts "$cmd" } else {  puts " launching : `$cmd'" ; eval $cmd }
     set cmd [format "set_input_delay %.3f -clock_fall -clock %s %s -add_delay" [expr $clock_period*0.45] $clock "\[get_ports $port\]" ]
     if { $TCL2SDC == "true" } { puts "$cmd" } else {  puts " launching : `$cmd'" ; eval $cmd }
     return
    }
    "LLI_PSI;ReceiveClock;DDR" {
     puts stdout [ format "Info: input port `%s' is the receive clock feeds with the transmit clock coming from Tx host" $port ]
     return
    }
    default { }
   }
  }

 # case of default value written by FlexNoC
 if { $input_delay == [expr $clock_period*($arteris_dflt_ARRIVAL_PERCENT/100.0)] } {
  set cmd [format "set_input_delay %.3f -clock %s %s %s" $input_delay $clock $add_delay "\[get_ports $port\]" ]

  if {$iofile == "void"} {
   global IO_FILE
   if { [info exists IO_FILE] } { set iofile $IO_FILE }
  }

  # iofile has been previously generated by arteris_list_io_combinatorial function
  if { [file exist $iofile] } {

   if [catch {open $iofile r} fp] {
    return -code error [format "function arteris_set_input_delay : cannot open input file: `%s'" $iofile]
   }

   foreach line [split [read $fp] \n] {
    if {[regexp [format "(\[^-\]+) -port \"%s\[\\\[0-9\\\]\]*\" -type (\[^-\]+) -connected_to_output \"(.+)\"" $port] $line match str0 str1 str2]} {
     puts stdout [ format "# Info: input port `%s' connected to output port: `%s'" $port $str2 ]
     set cmd [format "set_input_delay %.3f -clock %s %s %s" [expr $clock_period*($arteris_comb_ARRIVAL_PERCENT/100.0)] $clock $add_delay "\[get_ports $port\]" ]
     break
    }
   }
   close $fp

  # default case (no user constraints and no information in iofile)
  } else {

   set _all_fanout [all_fanout -endpoints_only -from [get_ports $port] ]
   if { [ sizeof_collection $_all_fanout ] } {
    # case of output port connected to output port
    puts stdout [ format "# Info: input port `%s' connected to output port: `%s'" $port [query_objects [index_collection $_all_fanout 0]] ]
    set cmd [format "set_input_delay %.3f -clock %s %s %s" [expr $clock_period*($arteris_comb_ARRIVAL_PERCENT/100.0)] $clock $add_delay "\[get_ports $port\]" ]
   }

  }

  if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }

 # extracted from user constraint (which can be set with parameters: -input_delay, -arrival_time or -user_directive)
 } else {
  set cmd [format "set_input_delay %.3f -clock %s %s %s" $input_delay $clock $add_delay "\[get_ports $port\]" ]
  if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
 }

}
#
#arteris_output -port "ACP_Ar_Addr" -socket "ACP" -clock_domain "SysClk266_i" -clock "PLL_266_ClockManager_SysClk266" -spec_domain_clock "/PLL_266/ClockManager/SysClk266" -clock_period "$PLL_266_ClockManager_SysClk266_P" -type "SYNC" -output_delay "[expr $PLL_266_ClockManager_SysClk266_P*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))]" -required_time "[expr $PLL_266_ClockManager_SysClk266_P*($arteris_dflt_REQUIRED_PERCENT/100.0)]" -add_delay "FALSE" -lsb "0" -msb "31" -user_directive "input_delay=1603ps ; output_delay=10%"
#
# 3 ways are available to set the required time of an output:
#  .arteris_dflt_REQUIRED_PERCENT (default value proposed during the export)
#  .setting the synthesisInfo field (FlexNoC GUI with parameter: output_delay=0.5ns [output_delay=200ps | output_delay=22% | required_time=1.0ns | required_time=12%]
#  .modifying the parameters: -output_delay (respectively -required_time) in the output.tcl file (modification made by hand)
#
# Precedence is: .synthesisInfo field (if used in the GUI)
#                .parameters: -output_delay (respectively -required_time) (compararison is made with the default value)
#                .variable: arteris_dflt_REQUIRED_PERCENT (default value)
#
proc arteris_set_output_delay { args } {
 #
 global library arteris_dflt_CLOCK_UNCERTAINTY_PERCENT
 global TCL2SDC
 if { ![info exists TCL2SDC] } { set TCL2SDC "false" }
 #
 set T_Flex2library 1.0
 if { [array exist library] && [info exist library(T_Flex2library)] } {
  set T_Flex2library $library(T_Flex2library)
 }
 #
 global arteris_dflt_ARRIVAL_PERCENT
 global arteris_dflt_REQUIRED_PERCENT
 global arteris_comb_ARRIVAL_PERCENT
 global arteris_comb_REQUIRED_PERCENT
 #
 set state          "flag"
 set add_delay      "void"
 set clock          "void"
 set clock_period   "void"
 set port           "void"
 set port_type      "void"
 set user_directive "void"
 set output_delay   "void"
 set required_time  "void"
 set iofile         "void"

 set cmd ""
 foreach _args $args {
  switch -- $state {
   flag {
    switch -glob -- $_args {
     -add_delay      { set state "add_delay"      }
     -clock          { set state "clock"          }
     -clock_period   { set state "clock_period"   }
     -port           { set state "port"           }
     -portType       { set state "port_type"      }
     -output_delay   { set state "output_delay"   }
     -required_time  { set state "required_time"  }
     -user_directive { set state "user_directive" }
     -iofile         { set state "iofile"         }
     default         { set state "default"        }
    }
   }
   add_delay {
    set add_delay ""
    if {$_args == "TRUE"} { set add_delay "-add_delay" }
    set state "flag"
   }
   clock {
    set clock $_args
    set state "flag"
   }
   clock_period {
    set clock_period $_args
    set state "flag"
   }
   port {
    set port $_args
    set state "flag"
   }
   port_type {
    # separator in synthesis_info field is `;'
    set port_type [string map {{ } {}} $_args]
    set state "flag"
   }
   output_delay {
    set output_delay $_args
    set state "flag"
   }
   required_time {
    set required_time $_args
    set state "flag"
   }
   user_directive {
    # catch user directives added in the specification project in the field synthesisInfo
    # separator in synthesis_info field is `;'
    set user_directive [string map {{ } {}} $_args]
    foreach _arg [split $user_directive {;}] {
     # parameter -user_directive is alway the final parameter and then take precedence over previous setting for variable: -output_delay
     switch -glob "$_arg" {
      output_delay=*% {
       scan $_arg {output_delay=%f%s} output_delay_percentage str_percent
       set output_delay [expr $clock_period * ($output_delay_percentage/100.0)]; set required_time [expr $clock_period-$output_delay]
      }
      output_delay=*ps {
       scan $_arg {output_delay=%f%s} output_delay str_unit
       set output_delay [expr ${output_delay} * $T_Flex2library / 1000]; set required_time [expr $clock_period-$output_delay]
      }
      output_delay=*ns {
       scan $_arg {output_delay=%f%s} output_delay str_unit
       set output_delay [expr ${output_delay} * $T_Flex2library]; set required_time [expr $clock_period-$output_delay]
      }
      required_time=*% {
       scan $_arg {required_time=%f%s} required_time_percentage str_percent
       set required_time [expr $clock_period * ($required_time_percentage/100.0)]; set output_delay [expr $clock_period-$required_time]
      }
      required_time=*ps {
       scan $_arg {required_time=%f%s} required_time_delay str_unit
       set required_time [expr $clock_period - (${required_time_delay} * $T_Flex2library / 1000)]; set output_delay [expr $clock_period-$required_time]
      }
      required_time=*ns {
       scan $_arg {required_time=%f%s} required_time_delay str_unit
       set required_time [expr $clock_period - (${required_time_delay} * $T_Flex2library)]; set output_delay [expr $clock_period-$required_time]
      }
      default { }
     }
    }
    set state "flag"
   }
   iofile {
    set iofile $_args
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if { $clock == "NA" } {
  puts stdout [ format "# Warning: output port `%s' has no timing constraint (clock domain is unavailable)" $port ]
  return
 }
 # case of modification has been introduced by hand but not equal for parameter -output_delay and -required_time
 if { $output_delay != [expr $clock_period-$required_time] } {
# return -code error [format "function arteris_set_output_delay : parameter output_delay=`%.3f' and required_time=`%.3f' are not equivalent with port `%s'" $output_delay $required_time $port]
 }

 if { $port_type != "void" } {
   switch -glob "$port_type" {
    "LLI_PSI;TransmitData;DDR"  {
     # Specific constraints for LLI_PSI mode with signal: Tx_C (Transmit Clock), Tx_D (Transmit Data)
     #  Tx_D is launched on the rising edge
     #  Tx_C is launched on the falling edge
     puts stdout [ format "Info: output port `%s' is the transmit data going to the receive data present in the Rx host" $port ]
     set cmd [format "set_output_delay %.3f -clock %s %s" [expr $clock_period*0.95] $clock "\[get_ports $port\]" ]
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
     return
    }
    "LLI_PSI;TransmitClock;DDR" {
     puts stdout [ format "Info: output port `%s' is the transmit clock going to the receive clock present in the Rx host" $port ]
     set cmd [format "set_output_delay %.3f -clock_fall -clock %s %s" [expr $clock_period*0.95] $clock "\[get_ports $port\]" ]
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
     return
    }
    default { }
   }
  }

 # case of default value written by FlexNoC
 if { $output_delay == [expr $clock_period*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] } {
  set cmd [format "set_output_delay %.3f -clock %s %s %s" $output_delay $clock $add_delay "\[get_ports $port\]" ]

  if {$iofile == "void"} {
   global IO_FILE
   if { [info exists IO_FILE] } { set iofile $IO_FILE }
  }

  # iofile has been previously generated by arteris_list_io_combinatorial function
  if { [file exist $iofile] } {

   if [catch {open $iofile r} fp] {
    return -code error [format "function arteris_set_output_delay : cannot open input file: `%s'" $iofile]
   }

   foreach line [split [read $fp] \n] {
    if {[regexp [format "(\[^-\]+) -port \"%s\[\\\[0-9\\\]\]*\" -type (\[^-\]+) -connected_to_input \"(.+)\"" $port] $line match str0 str1 str2]} {
     puts stdout [ format "# Info: output port `%s' connected to input port: `%s'" $port $str2 ]
     set cmd [format "set_output_delay %.3f -clock %s %s %s" [expr $clock_period*(1-($arteris_comb_REQUIRED_PERCENT/100.0))] $clock $add_delay "\[get_ports $port\]" ]
     break
    }
   }
   close $fp

  # default case (no user constraints and no information in iofile)
  } else {

   set _all_fanin [all_fanin -startpoints_only -to [get_ports $port] ]
   if { [ sizeof_collection $_all_fanin ] } {
    # case of output port connected to input port
    puts stdout [ format "# Info: input port `%s' connected to output port: `%s'" $port [query_objects [index_collection $_all_fanin 0] -truncate 1] ]
    set cmd [format "set_output_delay %.3f -clock %s %s %s" [expr $clock_period*(1-($arteris_comb_REQUIRED_PERCENT/100.0))] $clock $add_delay "\[get_ports $port\]" ]
   }

  }

  if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }

 # extracted from user constraint (which can be set with parameters: -output_delay, -required_time or -user_directive)
 } else {
  set cmd [format "set_output_delay %.3f -clock %s %s %s" $output_delay $clock $add_delay "\[get_ports $port\]" ]
  if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
 }

}


#
### Asynchronous register metastability delay tax setting #######################################################
#
# arteris_set_meta_delay_tax -module "SynchronizerCellAsyncRst" -type "SynchronizerCell" -generator "DemuxReq" -instance_name "DemuxReq_0/W/u" [-delay $delay]
#
proc arteris_set_meta_delay_tax { args } {
 #
 set state          "flag"
 set type           "void"
 set module         "void"
 set instance_name  "void"
 set delay          "void"
 set filled         "void"

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -type          { set state "type"          }
     -module        { set state "module"        }
     -instance_name { set state "instance_name" }
     -delay         { set state "delay"         }
     -filled        { set state "filled"        }
     default        { set state "default"       }
    }
   }
   type {
    if {$arg != "SynchronizerCell"} { return }
    set type $arg
    set state "flag"
   }
   module {
    set module $arg
    set state "flag"
   }
   instance_name {
    set instance_name $arg
    set state "flag"
   }
   delay {
    set delay $arg
    set state "flag"
   }
   filled {
    set filled $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 # Default metastability constraint (if no delay specification in the function call)
 if {$delay == "void"} {
  global META_STABILITY_DELAY
  if {![info exists META_STABILITY_DELAY]} {
   return -code error [format "function arteris_set_meta_delay_tax : META_STABILITY_DELAY variable not defined"]
  }  
  set delay $META_STABILITY_DELAY
 }

 # now we put the metastability constraint
 set top_design [current_design]

 puts "# Working on module: [get_attribute $instance_name ref_name] (instance $instance_name)"
 current_design [get_attribute $instance_name ref_name]

 set n_register 0
 set l_cmd [list]
 set l_clock_pin [list]
 set l_output_pin [list]
 foreach_in_collection _all_registers [get_cells "RegAsync*"] {
  incr n_register
  set _all_registers_name [get_object_name $_all_registers]
  puts " founded register: [get_object_name $_all_registers]"
  
  foreach_in_collection _get_pins [get_pins -of_objects $_all_registers] {
   set _get_pins_name [get_object_name $_get_pins]
   set _get_lib_pins [get_lib_pins -of_objects $_get_pins_name]
   set _get_lib_pins_name [get_object_name $_get_lib_pins]
   #puts "  with pins: $_get_pins_name (linked to primitive $_get_lib_pins_name)"

   if { [get_attribute -quiet $_get_lib_pins_name pin_direction] == "out" } { lappend l_output_pin $_get_pins_name }
   if { [get_attribute -quiet $_get_lib_pins_name clock] == "true" } { lappend l_clock_pin $_get_pins_name }
  }
 }

 current_design $top_design

 if {$filled == "TRUE" && $n_register == 0} {
   return -code error [format "function arteris_set_meta_delay_tax : no aynchronous register founded in %s" [get_object_name [current_design]]]
 }

 foreach _clock_pin "$l_clock_pin" {
  foreach _output_pin "$l_output_pin" {
   set cmd [format "set_annotated_delay -cell -load_delay cell %s -from %s -to %s" $delay $instance_name/$_clock_pin $instance_name/$_output_pin]
   puts " launching : `$cmd'" ; eval $cmd
  }
 }

}


#
### Inter clock domain constraint ###############################################################################
#
# arteris_set_inter_clock_domain_constraints -from_clk "ClkRx" -from_clk_P "$ClkRx_P" -to_clk "ClkTx" -to_clk_P "$ClkTx_P" [-margin 0.80 (default is 1.00)]
#
# Usually Flag: PARTIAL_EXPORT is not enabled
# But: PARTIAL_EXPORT can be enabled in the optl.tcl file to proposed a bottom/up flow (separateExport directive has been enabled in this case in the pdd)
# Then: 1) submodule is first compiled in order to have submodule description
#       2) then submodule description is re-read with the top level interconnect description
#       3) and top level constraint is set on the global description
#          In this case RTL hierarchy can be not the same compared to the gate level (xref is made with A_2REG array)
#
proc arteris_set_inter_clock_domain_constraints { args } {
 #
 global TCL2SDC PARTIAL_EXPORT
 if { ![info exists TCL2SDC] } { set TCL2SDC "false" }
 if { ![info exists PARTIAL_EXPORT] } { set PARTIAL_EXPORT "false" }
 #
 set state          "flag"
 set from_clk       "void"
 set from_reg       "void"
 set from_port      "void"
 set from_pin       "void"
 set from_clk_P     "void"
 set from_lsb       "void"
 set from_msb       "void"
 set to_clk         "void"
 set to_reg         "void"
 set to_clk_P       "void"
 set to_lsb         "void"
 set to_msb         "void"
 set delay          "void"
 set margin          1.00

 set cmd ""
 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -from_clk   { set state "from_clk"   }
     -from_reg   { set state "from_reg"   }
     -from_port  { set state "from_port"  }
     -from_pin   { set state "from_pin"   }
     -from_clk_P { set state "from_clk_P" }
     -from_lsb   { set state "from_lsb"   }
     -from_msb   { set state "from_msb"   }
     -to_clk     { set state "to_clk"     }
     -to_reg     { set state "to_reg"     }
     -to_clk_P   { set state "to_clk_P"   }
     -to_lsb     { set state "to_lsb"     }
     -to_msb     { set state "to_msb"     }
     -delay      { set state "delay"      }
     -margin     { set state "margin"     }
     default     { set state "default"    }
    }
   }
   from_clk {
    set from_clk $arg
    set state "flag"
   }
   from_reg {
    set from_reg $arg
    set state "flag"
   }
   from_port {
    set from_port $arg
    set state "flag"
   }
   from_pin {
    set from_pin $arg
    set state "flag"
   }
   from_clk_P {
    set from_clk_P $arg
    set state "flag"
   }
   from_lsb {
    set from_lsb $arg
    set state "flag"
   }
   from_msb {
    set from_msb $arg
    set state "flag"
   }
   to_clk {
    set to_clk $arg
    set state "flag"
   }
   to_reg {
    set to_reg $arg
    set state "flag"
   }
   to_clk_P {
    set to_clk_P $arg
    set state "flag"
   }
   to_lsb {
    set to_lsb $arg
    set state "flag"
   }
   to_msb {
    set to_msb $arg
    set state "flag"
   }
   delay {
    set delay $arg
    set state "flag"
   }
   margin {
    set margin $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 set from_type ""
 if { $from_clk  != "void" } { set from_type "from_clk"  }
 if { $from_reg  != "void" } { set from_type "from_reg"  }
 if { $from_port != "void" } { set from_type "from_port" }
 if { $from_pin  != "void" } { set from_type "from_pin"  }
 set to_type ""
 if { $to_clk != "void" } { set to_type "to_clk" }
 if { $to_reg != "void" } { set to_type "to_reg" }
 set type [format "%s%s%s" $from_type [if {$to_type != "" && $from_type != ""} {format "_"}] $to_type]

 set delay [format "%.3f" [expr $delay * $margin]]
 switch -glob -- $type {

  from_clk {
   set cmd [format "set_max_delay %s -from \[get_clock %s\]" $delay $from_clk]
   if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
  }

  from_clk_to_clk {
   set cmd [format "set_max_delay %s -from \[get_clock %s\] -to \[get_clock %s\]" $delay $from_clk $to_clk]
   if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
  }

  from_clk_to_reg {
   if { $to_lsb == {} } {
    set _to_reg [format "%s_reg" $to_reg]
    if { $PARTIAL_EXPORT == "true" } {
     if { [ catch { set _to_reg [get_xref $_to_reg] } result ] } { puts $result; continue }
    }
    set cmd [format "set_max_delay %s -from \[get_clock %s\] -to \[get_cells %s\]" $delay $from_clk $_to_reg]
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   } else {
    for {set j $to_lsb} {$j <= $to_msb} {incr j 1} {
     set _to_reg [format "%s_reg\[%d\]" $to_reg $i]
     if { $PARTIAL_EXPORT == "true" } {
      if { [ catch { set _to_reg [get_xref $_to_reg] } result ] } { puts $result; continue }
     }
     set cmd [format "set_max_delay %s -from \[get_clock %s\] -to \[get_cells %s\]" $delay $from_clk $_to_reg]
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
    }
   }
  }

  from_reg {
   if { $from_lsb == {} } {
    set _from_reg [format "%s_reg" $from_reg]
    if { $PARTIAL_EXPORT == "true" } {
     if { [ catch { set _from_reg [get_xref $_from_reg] } result ] } { puts $result; continue }
    }
    set cmd [format "set_max_delay %s -from \[get_cells %s\]" $delay $_from_reg]
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   } elseif { $from_lsb != {} } {
    for {set i $from_lsb} {$i <= $from_msb} {incr i 1} {
     set _from_reg [format "%s_reg\[%d\]" $from_reg $i]
     if { $PARTIAL_EXPORT == "true" } {
      if { [ catch { set _from_reg [get_xref $_from_reg] } result ] } { puts $result; continue }
     }
     set cmd [format "set_max_delay %s -from \[get_cells %s\]" $delay $_from_reg]
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
    }
   }
  }

  from_reg_to_clk {
   if { $from_lsb == {} } {
    set _from_reg [format "%s_reg" $from_reg]
    if { $PARTIAL_EXPORT == "true" } {
     if { [ catch { set _from_reg [get_xref $_from_reg] } result ] } { puts $result; continue }
    }
    set cmd [format "set_max_delay %s -from \[get_cells %s\] -to \[get_clock %s\]" $delay $_from_reg $to_clk]
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   } else {
    for {set i $from_lsb} {$i <= $from_msb} {incr i 1} {
     set _from_reg [format "%s_reg\[%d\]" $from_reg $i]
     if { $PARTIAL_EXPORT == "true" } {
      if { [ catch { set _from_reg [get_xref $_from_reg] } result ] } { puts $result; continue }
     }
     set cmd [format "set_max_delay %s -from \[get_cells %s\] -to \[get_clock %s\]" $delay $_from_reg $to_clk]
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
    }
   }
  }

  from_reg_to_reg {
   if { $from_lsb == {} && $to_lsb == {}  } {
    set _from_reg [format "%s_reg" $from_reg]; set _to_reg [format "%s_reg" $to_reg]
    if { $PARTIAL_EXPORT == "true" } {
      if { [ catch { set _from_reg [get_xref $_from_reg] } result ] } { puts $result; continue }
      if { [ catch { set _to_reg [get_xref $_to_reg] } result ] } { puts $result; continue }
    }
    set cmd [format "set_max_delay %s -from \[get_cells %s\] -to \[get_cells %s\]" $delay $_from_reg $_to_reg]
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   } elseif { $from_lsb != {} && $to_lsb == {}  } {
    set _to_reg [format "%s_reg" $to_reg]
    if { $PARTIAL_EXPORT == "true" } {
     if { [ catch { set _to_reg [get_xref $_to_reg] } result ] } { puts $result; continue }
    }
    for {set i $from_lsb} {$i <= $from_msb} {incr i 1} {
     set _from_reg [format "%s_reg\[%d\]" $from_reg $i]
     if { $PARTIAL_EXPORT == "true" } {
      if { [ catch { set _from_reg [get_xref $_from_reg] } result ] } { puts $result; continue }
     }
     set cmd [format "set_max_delay %s -from \[get_cells %s\] -to \[get_cells %s\]" $delay $_from_reg $_to_reg]
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
    }
   } elseif { $from_lsb == {} && $to_lsb != {}  } {
    set _from_reg [format "%s_reg" $from_reg]
    if { $PARTIAL_EXPORT == "true" } {
     if { [ catch { set _from_reg [get_xref $_from_reg] } result ] } { puts $result; continue }
    }
    for {set j $to_lsb} {$j <= $to_msb} {incr j 1} {
     set _to_reg [format "%s_reg\[%d\]" $to_reg $j]
     if { $PARTIAL_EXPORT == "true" } {
      if { [ catch { set _to_reg [get_xref $_to_reg] } result ] } { puts $result; continue }
     }
     set cmd [format "set_max_delay %s -from \[get_cells %s\] -to \[get_cells %s\]" $delay $_from_reg $_to_reg] 
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
    }
   } elseif { $from_lsb != {} && $to_lsb != {}  } {
    for {set i $from_lsb} {$i <= $from_msb} {incr i 1} {
     for {set j $to_lsb} {$j <= $to_msb} {incr j 1} {
      set _from_reg [format "%s_reg\[%d\]" $from_reg $i]; set _to_reg [format "%s_reg\[%d\]" $to_reg $j]
      if { $PARTIAL_EXPORT == "true" } {
       if { [ catch { set _from_reg [get_xref $_from_reg] } result ] } { puts $result; continue }
       if { [ catch { set _to_reg [get_xref $_to_reg] } result ] } { puts $result; continue }
      }
      set cmd [format "set_max_delay %s -from \[get_cells %s\] -to \[get_cells %s\]\]" $delay $_from_reg $_to_reg]
      if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
     }
    }
   }
  }

  from_port_to_clk {
   if { $from_lsb == {} } {
    set cmd [format "set_max_delay %s -from \[get_port %s\] -to \[get_clock %s\]" $delay $from_port $to_clk]
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   } else {
    for {set i $from_lsb} {$i <= $from_msb} {incr i 1} {
     set cmd [format "set_max_delay %s -from \[get_port %s\[%d\]\] -to \[get_clock %s\]" $delay $from_port $i $to_clk]
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
    }
   }
  }

  from_pin_to_clk {
   if { $from_lsb == {} } {
    set cmd [format "set_max_delay %s -from \[all_fanin -flat -startpoints_only -to %s\] -to \[get_clock %s\]" $delay $from_pin $to_clk]
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   } else {
    for {set i $from_lsb} {$i <= $from_msb} {incr i 1} {
     set cmd [format "set_max_delay %s -from \[all_fanin -flat -startpoints_only -to %s\[%d\]\] -to \[get_clock %s\]" $delay $from_pin $i $to_clk]
     if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
    }
   }
  }

  default {
   return -code error [format "function arteris_set_inter_clock_domain_constraints : constraints type is not defined (`%s')" $type]
  }
 }
 
}
proc set_xref { file } {
 global A_2REG
 #
 # method to find a pattern in the array A_2REG
 #  set pattern "NewLink*Resp_datapathOutput_RxClkAdapt_Async_RdCnt_reg_0_"
 #  foreach {token value} [array get A_2REG $pattern] { puts "token = $token -val = $value" }
 #

 if [catch {open $file r} fp] {
  return -code error [format "function set_xref: cannot open file: `%s'" $fp]
 } else {

  set _n 0; set _n_reg 0
  set _read "false"

  foreach _line [split [read $fp] \n] {
   incr _n
   if { [string length $_line] <= 1    } { continue }
   if { [string match "Total *" $_line]} { scan $_line "%s %d %s %f" str0 _n_cell str1 area; break }
   if { [string match {\-\-\-\-\-*} $_line] && $_read == "false" } { set _read "true" ; continue }
   if { [string match {\-\-\-\-\-*} $_line] && $_read == "true" } { set _read "false" ; continue }
   if { $_read == "false" } { continue }
   incr _n_reg

   if { [string match "uAnd* *" $_line] } { continue }
   if { [string match "uOr* *" $_line]  } { continue }

   set _reg [lindex $_line 0]
   set _token [string map {/ _ [ _ ] _} $_reg]

   if { [info exists A_2REG($_token)] } {
    return -code error [format "function set_xref: register/module `%s' already seen during the processing (line %d in file %s)" $_reg $_n $file]
   } else {
    set A_2REG($_token) $_reg
   }

  }

  if { $_n_cell != $_n_reg } {
   return -code error [format "function set_xref: number of `%d' registers/modules have been read in which is not equal to `%d' specified in file `%s'" $_n $_n_cell $file]
  }

 }

 close $fp
}
proc get_xref { reg } {
 global VERBOSE+ A_2REG
 #
 if { [info exists VERBOSE+] } { puts "# Working on register: $reg" }

 foreach _s [split $reg "/"] {
  set head [ string range $reg 0 [expr [string first $_s $reg]+[string length $_s]] ]
  set tail [ string range $reg [expr [string first $_s $reg]+[string length $_s]+1] [expr [string length $reg]-1] ]
  set _pattern [format "%s" [string map {/ _ [ _ ] _} $tail]]
  #if { [info exists VERBOSE+] } { puts [format "#  head=`%s' - tail=`%s'" $head $tail] }
  set _reg [lindex [array get A_2REG $_pattern] 1]
  if { $_reg != "" } {
   if { [info exists VERBOSE+] } { puts [format "#  matching register: `%s' in submodule `%s' (pattern=%s)" $_reg $head $_pattern] }
   return [format "%s%s" $head $_reg]
  } else {
   return -code error [format "Warning: register/module `%s' does not have any xreference in array A_2REG" $reg]
  }
 }
}


#
### Timing constraint on reset port #############################################################################
#
proc arteris_set_ideal_network { args } {
 #
 global TCL2SDC PARTIAL_EXPORT
 if { ![info exists TCL2SDC] } { set TCL2SDC "false" }
 if { ![info exists PARTIAL_EXPORT] } { set PARTIAL_EXPORT "false" }
 #
 set state          "flag"
 set port_or_pin    "void"
 set port_f         "void"
 set lsb            "void"
 set msb            "void"

 set cmd ""
 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -port       { set state "port"    }
     -pin        { set state "pin"     }
     -lsb        { set state "lsb"     }
     -msb        { set state "msb"     }
     default     { set state "default" }
    }
   }
   port {
    set port_or_pin $arg
    set state "flag"
   }
   pin {
    set port_or_pin $arg
    set state "flag"
   }
   lsb {
    set lsb $arg
    set state "flag"
   }
   msb {
    set msb $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if { [info procs "arteris_reset"] == "arteris_reset" } {
  set port_f "reset_port"
  if { $lsb == {} } {
   set cmd [format "set_ideal_network -no_propagate \[get_ports %s\]" $port_or_pin]
   if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
  } else {
   for {set i $lsb} {$i <= $msb} {incr i 1} {
    set cmd [format "set_ideal_network -no_propagate \[get_ports %s\[%d\]\]" $port_or_pin $i]
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   }
  }
 } elseif { [info procs "arteris_gen_reset"] == "arteris_gen_reset" } {
  set port_f "reset_pin"
  if { $lsb == {} } {
   if { $PARTIAL_EXPORT == "true" } {
    #separator is supposed to be /
    set cmd [format "set_ideal_network -no_propagate  \[get_pin %s/%s\]" [get_xref [file dirname $port_or_pin]] [file tail $port_or_pin]]
   } else {
    set cmd [format "set_ideal_network -no_propagate  \[get_pin %s\]" $port_or_pin]
   }
   if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
  } else {
   for {set i $lsb} {$i <= $msb} {incr i 1} {
    set port_or_pin [format "%s\[%d\]" $port_or_pin $i]
    if { $PARTIAL_EXPORT == "true" } {
     #separator is supposed to be /
     set cmd [format "set_ideal_network -no_propagate  \[get_pin %s/%s\]" [get_xref [file dirname $port_or_pin]] [file tail $port_or_pin]]
    } else {
     set cmd [format "set_ideal_network -no_propagate \[get_pin %s\]" $port_or_pin]
    }
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   }
  }
 } elseif { [info procs "arteris_testMode"] == "arteris_testMode" } {
  set port_f "test_port"
  if { $lsb == {} } {
   set cmd [format "set_ideal_network -no_propagate \[get_ports %s\]" $port_or_pin]
   if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
  } else {
   for {set i $lsb} {$i <= $msb} {incr i 1} {
    set cmd [format "set_ideal_network -no_propagate \[get_ports %s\[%d\]\]" $port_or_pin $i]
    if { $TCL2SDC == "true" } { puts "$cmd" } else { puts " launching : `$cmd'" ; eval $cmd }
   }
  }
 } else {
  return -code error [format "function arteris_set_ideal_network : call of pre-defined function which is not set"]
 }

}


#
### set_dont_touch attribute ####################################################################################
# arteris_set_dont_touch -module "GaterCell" -type "GaterCell" -generator "niui_acc_cpu_data" -instance_name "acc_noc/niui_acc_cpu_data/Wrapped/u_1/instGaterCell" -filled "FALSE"
# arteris_customer_cell -module "securityCore_sl2" -type "CoreWrapper" -generator "sl2_fw" -instance_name "l3_noc_attila_smp_clk2_0/sl2_fw/W/Ci" -filled "TRUE"
#
#
proc arteris_set_dont_touch_mapped_cell { args } {
 #
 set state         "flag"
 set instance_name "void"
 set type          "void"

 set cmd " "

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -type          { set state "type"          }
     -instance_name { set state "instance_name" }
     default        { set state "default"       }
    }
   }
   type {
    set type $arg
    set state "flag"
   }
   instance_name {
    set instance_name $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 # most of the time GaterCell, SynchronizerCell are mapped on the target technology and optimization is forbidden
 if { [string match -nocase {*GaterCell*} $type] || [string match -nocase {*SynchronizerCell*} $type] } {

  set cmd [format "set_dont_touch %s true" $instance_name]
  puts " launching : `$cmd'" ; eval $cmd

 }

 if { [string match -nocase {*ClockManagerCell*} $type]} {

  set cmd [format "set_boundary_optimization %s false" $instance_name]
  puts " launching : `$cmd'" ; eval $cmd

 }



 # EBH FN-700, Aggregators need to be preserved for resilience but need to use set_boundary_optimization false
 # not set_dont_touch because logic in this still needs to be synthesized 
 if { [string match -nocase {*AggregatorBlock*} $type] } {

  set cmd [format "set_boundary_optimization %s false" $instance_name]
  puts " launching : `$cmd'" ; eval $cmd

 }


}
proc arteris_ungroup_customer_cell { args } {
 #
 set state         "flag"
 set module        "void"
 set instance_name "void"
 set type          "void"

 set cmd " "

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -type          { set state "type"          }
     -module        { set state "module"        }
     -instance_name { set state "instance_name" }
     default        { set state "default"       }
    }
   }
   type {
    set type $arg
    set state "flag"
   }
   module {
    set module $arg
    set state "flag"
   }
   instance_name {
    set instance_name $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 # most of the time we do not have the final description for such module (for example CoreWrapper aka Firewall)
 # so we emulate the HDL description but at the end it must be changed by the final customer description
 # these modules can also be based on hard macro (for example ClockManager, RegFileHm)
 if { [string match -nocase {*ClockManager*} $type] || [string match -nocase {*CoreWrapper*} $type] || [string match -nocase {*CoreCell*} $type] || [string match -nocase {*FifoHm*} $type] || [string match -nocase {*RegFileHm*} $type] } {

  puts "# Working on module: [get_attribute $instance_name ref_name] (instance $instance_name)"
  puts "# Warning: RTL is used as description but usually final description is based on hard macro"

  if { ![string match -nocase {*ClockManager*} $type] } {
   set cmd [format "set_dont_touch %s false" $instance_name]
   puts " launching : `$cmd'" ; eval $cmd

   set cmd [ format "current_design %s; ungroup -all -flatten; current_design %s" [get_attribute $instance_name ref_name] [get_object_name [current_design]] ]
   puts " launching : `$cmd'" ; eval $cmd
   set cmd [format "set_dont_touch %s true" $instance_name]
   puts " launching : `$cmd'" ; eval $cmd
  } else {
   set cmd [format "set_dont_touch %s true" [file dirname $instance_name]]
   puts " launching : `$cmd'" ; eval $cmd
  }
 }
}


#
### arteris_report_clock_gating #################################################################################
# parameter -ofile is used to report summary about clock gating percentage (either local or global according to the parameters)
# and is also used as base name to report the list of registers when parameter -ungated or -gated is used
#
# arteris_report_clock_gating -local -ofile $ARTERIS_OUTPUT_DIR/map.report_local_clock_gating
# arteris_report_clock_gating -local -ungated -ofile $ARTERIS_OUTPUT_DIR/$ARTERIS_OUTPUT_DIR/map.report_local_clock_gating
# arteris_report_clock_gating -local -gated -ungated -ofile $ARTERIS_OUTPUT_DIR/$ARTERIS_OUTPUT_DIR/map.report_local_clock_gating
# arteris_report_clock_gating -global -ofile  $ARTERIS_OUTPUT_DIR/map.report_global_clock_gating
# arteris_report_clock_gating -global -ungated -ofile $ARTERIS_OUTPUT_DIR/map.report_global_clock_gating
# arteris_report_clock_gating -global -gated -ungated -ofile $ARTERIS_OUTPUT_DIR/$ARTERIS_OUTPUT_DIR/optl.report_local_clock_gating
#
proc arteris_report_clock_gating { args } {
 #
 global VERBOSE
 set state   "flag"
 set gated   "void"
 set ungated "void"
 set local   "void"
 set global  "void"
 set ofile   "void"

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -gated   { set gated "true" }
     -ungated { set ungated "true" }
     -local   { set local "true" }
     -global  { set global "true" }
     -ofile   { set state "ofile" }
     default  { set state "default" }
    }
   }
   ofile   { set ofile $arg ; set state "flag" }
   default { set state "flag" }
  }
 }
 #

 if { $local == "void" && $global == "void" }  { return -code error [format "function arteris_report_clock_gating : select either-local or -global mode"] }
 if { $local != "void" && $global != "void" }  { return -code error [format "function arteris_report_clock_gating : select only -local or -global mode"] }
 if { $ofile == "void" }                       { return -code error [format "function arteris_report_clock_gating : output file is not defined"] }

 set _n_local_clock_gater 0
 set _n_global_clock_gater 0
 # in order to speed up report use of an array to store net already visited
 array set arr_net {init 1}
 set _all_register 0
 set _ungated_register 0
 set l_gated_register_with_local_icg [list]
 set l_gated_register_with_global_icg [list]
 set l_unexpected_gated_register [list]

 foreach _l_register [get_attribute [all_registers -cells] full_name] {
  array set tab_gated_register_with_global_icg "$_l_register none"
  array set tab_gated_register_with_local_icg "$_l_register none"
 }

 foreach_in_collection _all_clock_gates [all_clock_gates -output_pins] {
  set _pins_name [get_attribute $_all_clock_gates full_name]
  set _clock_gates_module_name [get_attribute [get_cell -of_objects $_pins_name] ref_name]
  set _net_name [get_attribute [get_nets -of_objects $_pins_name] full_name]
  set _lib_pins [get_lib_pins -of_objects $_pins_name]
  set _lib_pins_name [get_object_name $_lib_pins]
  set _full_pins_name [get_attribute $_pins_name full_name]

  ## case of icg instantiated by DC
  if { [string match "*SNPS_CLOCK_GATE*" $_clock_gates_module_name] } {
   if { [info exists VERBOSE] } { puts "# Working on LOCAL clock gater: $_pins_name (connected to net: $_net_name)" }
   set _clock_gater_pin $_pins_name
   incr _n_local_clock_gater

   set _drived_by_global_clock_gater "false"
   foreach_in_collection __pins [get_pins -of_objects [get_cell -of_objects $_all_clock_gates] ] {
    set __pins_name [get_object_name $__pins]
    if { [get_attribute $__pins pin_direction] == "in" && [get_attribute $__pins name] == "CLK" } {
     foreach_in_collection _pins [ all_fanin -flat -to $__pins_name ] {
      set _pins_name [get_attribute $_pins full_name]
      if { [string match "*instGaterCell*" $_pins_name] } {
       if { [info exists VERBOSE] } { puts "  drived by GLOBAL clock gater: $_pins_name" }
       set __clock_gater_pin $_pins_name
       set _drived_by_global_clock_gater "true"; break
      }
     }
     break
    }
   }

   foreach_in_collection _pins [ all_fanout -flat -from $_full_pins_name ] {
    set _reg_name [ get_attribute [get_cell -of_objects [get_attribute $_pins full_name]] full_name ]

    # case of output of the local clock gater (instantiated by DC)
    if { [get_attribute $_pins pin_direction] == "out" } { continue }

    lappend l_gated_register_with_local_icg $_reg_name
    array set tab_gated_register_with_local_icg "$_reg_name $_clock_gater_pin"

    if { $_drived_by_global_clock_gater == "true" } {
     lappend l_gated_register_with_global_icg $_reg_name
     array set tab_gated_register_with_global_icg "$_reg_name $__clock_gater_pin"
    }

   }

  ## case of icg instantiated by FlexNoC  
  } elseif { [string match "*instGaterCell*" $_pins_name] } {
   if { [info exists VERBOSE] } { puts "# Working on GLOBAL clock gater: $_pins_name (connected to net: $_net_name)" }
   set __clock_gater_pin $_pins_name
   incr _n_global_clock_gater

   foreach_in_collection _pins [ all_fanout -flat -from $_full_pins_name ] {
    set _pins_name [get_attribute $_pins full_name]

    # case of pin is connected to a local clock gater (instantiated by DC)
    if { [get_attribute [get_cell -of_objects $_pins_name] is_icg] == "true" } { continue }
    # case of pin is connected to a clockManager
    if { [get_attribute $_pins pin_direction] == "out" } { continue }

    lappend l_gated_register_with_global_icg [get_attribute [get_cell -of_objects $_pins_name] full_name]
    array set tab_gated_register_with_global_icg "[get_attribute [get_cell -of_objects $_pins_name] full_name] $__clock_gater_pin"
   }

  ## normaly never used
  } else {
   if { [info exists VERBOSE] } { puts "# Working on UNKNOWN clock gater: $_pins_name (connected to net: $_net_name)" }
   foreach_in_collection _pins [ all_fanout -flat -from $_full_pins_name ] {
    set _pins_name [get_attribute $_pins full_name]
    if { [get_attribute $_pins pin_direction] == "out" } { continue }
    lappend l_unexpected_gated_register [get_attribute [get_cell -of_objects $_pins_name] full_name]
   }
  }

 }

 set l_ungated_register_with_local_icg [remove_from_collection [all_registers -cells] [lsort -unique $l_gated_register_with_local_icg] ]
 set n_ungated_register_with_local_icg [sizeof_collection $l_ungated_register_with_local_icg]
 set l_ungated_register_with_global_icg [remove_from_collection [all_registers -cells] [lsort -unique $l_gated_register_with_global_icg] ]
 set n_ungated_register_with_global_icg [sizeof_collection $l_ungated_register_with_global_icg]
 set _n_clock_gater "NA"

 redirect -file $ofile.dump {
  puts [ format "%-90s %-80s %-100s %-70s" "Register" "     ClockManager" "     Global ICG" "     Local ICG" ]
  puts -nonewline "--------------------------------------------------------------------------------------------------------"
  puts -nonewline "--------------------------------------------------------------------------------------------------------"
  puts "-------------------------------------------------------------------------------------------------------------------"

  foreach_in_collection _l_register [sort_collection [all_registers -cells] full_name] {
   set _reg [get_attribute $_l_register full_name]
   set _reg_clk "NA"
   foreach_in_collection _pin [get_pin -of $_reg] { if { [get_attribute $_pin is_on_clock_network] == "true" } { set _reg_clk [get_attribute $_pin full_name] } }
   set _global_icg_pin $tab_gated_register_with_global_icg($_reg)
   set _local_icg_pin $tab_gated_register_with_local_icg($_reg)

   puts -nonewline [ format "%-90s " $_reg ]
   puts -nonewline [ format "%80s " [ \
     if {$_global_icg_pin != "none"} { format "%s" [get_object_name [all_fanin -startpoints_only -flat -to $_global_icg_pin]] } \
     elseif {$_local_icg_pin != "none"} { format "%s" [get_object_name [all_fanin -startpoints_only -flat -to $_local_icg_pin]] } \
     else { format "%s" [get_object_name [all_fanin -startpoints_only -flat -to $_reg_clk]] }] ]
   puts -nonewline [ format "%100s " [ \
    if {$_global_icg_pin != "none"} { format "%s" [file dirname [get_attribute [get_cell -of_objects $_global_icg_pin] full_name]] } \
    else { format "%s" $_global_icg_pin }] ]
   puts -nonewline [ format "%70s\n" [ \
    if {$_local_icg_pin != "none"} { format "%s" [get_attribute [get_cell -of_objects $_local_icg_pin] ref_name] } \
    else { format "%s" $_local_icg_pin }] ]
  }

 }

 set _all_registers [sizeof_collection [all_registers -cells]]
 if { $_all_registers == 0 } {

  redirect -file $ofile {
   puts "# Warning: No registers has been founded in the module"
   puts ""
  }

 } else {

  if { $local == "true" } {

   set _n_clock_gater $_n_local_clock_gater
   if { $gated == "true" } { redirect -file $ofile.gated {
     puts "--------------------------------------------------------------------------------"
     puts "                             Gated Register Report (local clock gater type instantiated by DC)"
     puts "--------------------------------------------------------------------------------"
     foreach _l_register [lsort -ascii $l_gated_register_with_local_icg] {
      puts [get_attribute $_l_register full_name]
     }
   } }
   if { $ungated == "true" } { redirect -file $ofile.ungated {
    puts "--------------------------------------------------------------------------------"
    puts "                             Ungated Register Report (local clock gater type instantiated by DC)"
    puts "--------------------------------------------------------------------------------"
    foreach_in_collection _l_register [sort_collection $l_ungated_register_with_local_icg full_name] {
     puts [get_attribute $_l_register full_name]
    }
   } }

  }
  if { $global == "true" } {

   set _n_clock_gater $_n_global_clock_gater
   if { $gated == "true" } { redirect -file $ofile.gated {
    puts "--------------------------------------------------------------------------------"
    puts "                             Gated Register Report (global clock gater type instantiated by FlexNoC)"
    puts "--------------------------------------------------------------------------------"
    foreach _l_register [lsort -ascii $l_gated_register_with_global_icg] {
     puts [get_attribute $_l_register full_name]
    }
   } }
   if { $ungated == "true" } { redirect -file $ofile.ungated {
    puts "--------------------------------------------------------------------------------"
    puts "                             Ungated Register Report (global clock gater type instantiated by FlexNoC)"
    puts "--------------------------------------------------------------------------------"
    foreach_in_collection _l_register [sort_collection $l_ungated_register_with_global_icg full_name] {
     puts [get_attribute $_l_register full_name]
    }
   } }

  }

 }

 set _ungated_register $n_ungated_register_with_global_icg
 if { $local == "true" } { set _ungated_register $n_ungated_register_with_local_icg }

 redirect /dev/null { set design_name [get_object_name [current_design]] }
 redirect -file $ofile {
  puts ""
  puts [ format "--------------------------------------------------------------------------------"]
  puts [ format "                              Clock Gating Summary (%s)" $design_name ]
  puts [ format "          ----------------------------------------------------------------"]
  puts [ format "          |    Number of Clock gating elements    |     %16s |" [format "%d         " $_n_clock_gater] ]
  puts [ format "          |                                       |                      |"]
  puts [ format "          |    Number of Gated registers          |     %16s |" [format "%d (%5.2f%s)" [expr $_all_registers-$_ungated_register] [expr 100*(1.0 - (1.0*$_ungated_register / $_all_registers))] "%"] ]
  puts [ format "          |                                       |                      |"]
  puts [ format "          |    Number of Ungated registers        |     %16s |" [format "%d (%5.2f%s)" $_ungated_register [expr 100*(1.0*$_ungated_register / $_all_registers)] "%"] ]
  puts [ format "          |                                       |                      |"]
  puts [ format "          |    Total number of registers          |     %16s |" [format "%d         " $_all_registers] ]
  puts [ format "          ----------------------------------------------------------------"]
 }

 if { [llength $l_unexpected_gated_register] > 0 } {
  redirect -file $ofile.unexpected.gated {
   puts "--------------------------------------------------------------------------------"
   puts "                             Unexpected Gated Register Report"
   puts "--------------------------------------------------------------------------------"
   foreach _l_register $l_unexpected_gated_register {
    puts [get_attribute $_l_register full_name]
  } }
  return -code error [format "function arteris_report_clock_gating : ICG has been detected with undefined type"]
 }
 
}


#
# arteris_read_submodule -module "Specification_Architecture_Structure_L1" -generator "submodule" -instance_name "L1" -level "2" -unix_path "$INPUT_DIR/L1"
#
# Procedure is usually used when separateExport directive has been enabled in the pdd
#  and then we need to re-read in submodule description (Verilog netlist by default) and xreference of internal object
# By default optl step is used but map can be defined with PARTIAL_EXPORT_TYPE
#
proc arteris_read_submodule { args } {
 #
 global PARTIAL_EXPORT_TYPE
 if { ![info exists PARTIAL_EXPORT_TYPE] } { set PARTIAL_EXPORT_TYPE "optl" }
 #
 set state          "flag"
 set module         "void"
 set generator      "void"
 set instance_name  "void"

 set cmd " "

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -generator     { set state "generator" }
     -module        { set state "module"    }
     -unix_path     { set state "unix_path" }
     default        { set state "default"   }
    }
   }
   generator {
    set generator $arg
    set state "flag"
   }
   module {
    set module $arg
    set state "flag"
   }
   unix_path {
    set unix_path $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if { $generator == "submodule" } {
  set netlist $unix_path/${PARTIAL_EXPORT_TYPE}.v

  if { [file exist $unix_path/${PARTIAL_EXPORT_TYPE}.v] } {
   set cmd [format "read_verilog -netlist %s/%s" $unix_path ${PARTIAL_EXPORT_TYPE}.v]
   puts " launching : `$cmd'" ; eval $cmd
  } else {
   return -code error [format "function arteris_read_submodule : input file `%s' does not exist" $unix_path/${PARTIAL_EXPORT_TYPE}.v]
  }

  if { [file exist $unix_path/${PARTIAL_EXPORT_TYPE}.report_cell.sequential] } {
   set cmd [format "set_xref %s/%s" $unix_path ${PARTIAL_EXPORT_TYPE}.report_cell.sequential]
   puts " launching : `$cmd'" ; eval $cmd
  } else {
   return -code error [format "function arteris_read_submodule : input file `%s' does not exist" $unix_path/${PARTIAL_EXPORT_TYPE}.report_cell.sequential]
  }

  if { [file exist $unix_path/${PARTIAL_EXPORT_TYPE}.report_cell.hierarchical] } {
   set cmd [format "set_xref %s/%s" $unix_path ${PARTIAL_EXPORT_TYPE}.report_cell.hierarchical]
   puts " launching : `$cmd'" ; eval $cmd
  } else {
  return -code error [format "function arteris_read_submodule : input file `%s' does not exist" $unix_path/${PARTIAL_EXPORT_TYPE}.report_cell.hierarchical]
  }

 }
}
#
# Procedure: arteris_set_dont_touch_submodule is used in a bottom/up methodology.
# In this case, previous compiled submodule is preserved with the attribute dont_touch
#
# arteris_set_dont_touch_submodule -module "Specification_Architecture_Structure_USPC" -generator "submodule" -instance_name "USPC" -level "2" -unix_path "$INPUT_DIR/USPC"
#
proc arteris_set_dont_touch_submodule { args } {
 #
 set state         "flag"
 set generator     "void"
 set instance_name "void"

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -generator     { set state "generator"     }
     -instance_name { set state "instance_name" }
     default        { set state "default"       }
    }
   }
   generator {
    set generator $arg
    set state "flag"
   }
   instance_name {
    set instance_name $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 if { $generator == "submodule" } {
  set cmd [format "set_dont_touch %s true" $instance_name]
  puts " launching : `$cmd'" ; eval $cmd
 }

}


#
### rm_feedthrough_module ##################################################################################
# Disolve hierarchy level of a module:
#  - when only feedthrough are present in the module
#  - and/or when only output are connected to tieoff cells
#
# rm_feedthrough_module -design $TOP_DESIGN
#
proc rm_feedthrough_module { args } {
 #
 global VERBOSE
 set state          "flag"
 set design         "void"
 set ungroup        "false"
 set level          0

 set cmd ""
 foreach _args $args {
  switch -- $state {
   flag {
    switch -glob -- $_args {
     -design         { set state "design"  }
     -level          { set state "level"   }
     default         { set state "default" }
    }
   }
   design {
    set design $_args
    set state "flag"
   }
   level {
    set level $_args
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 # for a better visibility during hierarchy browsing
 set blank "" 
 for {set i 0} {$i<$level} { incr i} { set blank [format " %s" $blank] }

 redirect -variable msg { current_design $design }  

 regexp {([^\{]+)\{([^\}]+)\}(.*)} $msg str str0 str1 str2
 puts [format "%02d.%sChecking module: %s" $level $blank $str1]

 set l_instance [list]
 foreach_in_collection _instance [sub_instances_of $design] {
  lappend l_instance [get_attribute $_instance name] 
 }

 if { [llength $l_instance] > 0 } {
  foreach _instance $l_instance {
   if { [info exists VERBOSE] } { puts [ format "    %sjumping to instance: %s (parent module: %s)" $blank [get_attribute $_instance name] [get_attribute $_instance ref_name] ] }

   incr level 1
   switch [ rm_feedthrough_module -design [get_attribute $_instance ref_name] -level $level ] {
    "true" {
     redirect -variable msg { current_design $design }
     set cmd [format "ungroup -all_instance %s" $_instance]
     puts [ format "    %slaunching : `%s' (from current module $design)" $blank $cmd]; eval $cmd
    }
    default {
     redirect -variable msg { current_design $design }
    }
   }
   incr level -1

  }
 } else {
  if { [info exists VERBOSE] } { puts [ format "    %seeing lower module: %s" $blank $design] }
  set ungroup "false"

  set l_tieoff [list]
  foreach_in_collection _tieoff [all_tieoff] {
   lappend l_tieoff [get_attribute $_tieoff ref_name]
  }

  set l_insts [list]
  foreach_in_collection _cells [get_cells -quiet "*"] {
   lappend l_insts [get_attribute $_cells ref_name]
   # to improve cpu run time
   if { [llength $l_insts] > [llength $l_tieoff] } { return $ungroup }
  }

  if { [llength $l_insts] > 0 } {
   set l_tieoff [lsort -ascii $l_tieoff]
   set l_insts [lsort -ascii $l_insts]
   if { $l_insts == $l_tieoff } { set ungroup "true" }
   if { [info exists VERBOSE] } { puts "      .logic present in module: $design (ungroup=$ungroup)" }
  } else {
   set ungroup "true"
   if { [info exists VERBOSE] } { puts "      .logic not present in module: $design (ungroup=$ungroup)" }
  }

 }

 return $ungroup
}


#
### arteris_ungroup_generator_level attribute ###################################################################
# Ungroup hierarchy module with a selection based on the FlexNoC generator present in the hierarchy.tcl
# Generators have to described in the following pre-defined list:
#  list_of_generator_with_no_processing    [list "ClockGater"]
#  list_of_generator_with_one_deeper_level [list "G2T" "T2G"]
#  list_of_generator_with_two_deeper_level [list "G2S"]
#  list_of_generator_with_elementary_gate  [list "gate"]
#
# Default behavoir is:
#  if parameter: -generator is not void (module has been created by FlexNoC and not by the customer) then ungroup is made
#
# To use the procedure:
# rename arteris_ungroup_generator_level arteris_hierarchy
#  source hierarchy.tcl
# rename arteris_hierarchy arteris_ungroup_generator_level
#
proc arteris_ungroup_generator_level { args } {
 #
 global VERBOSE
 #
 set state          "flag"
 set module         "void"
 set generator      "void"
 set instance_name  "void"

 set cmd " "

 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -generator     { set state "generator"     }
     -module        { set state "module"        }
     -instance_name { set state "instance_name" }
     default        { set state "default"       }
    }
   }
   generator {
    set generator $arg
    if { [string length $arg] == 0 } { return }
    set state "flag"
   }
   module {
    set module $arg
    set state "flag"
   }
   instance_name {
    set instance_name $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 set top_design [get_attribute [current_design] name]
 puts ""
 puts [ format "# Working on module: %-64s %64s %s" [get_attribute $instance_name ref_name] [format "(instance %s)" $instance_name] [format "(GENERATOR %16s)" $generator] ]

 set list_of_generator_with_no_processing    [list "ClockGater"]
 set list_of_generator_with_one_deeper_level [list "G2T" "T2G"]
 set list_of_generator_with_two_deeper_level [list "G2S"]
 set list_of_generator_with_elementary_gate  [list "gate"]

 if { [string match "*$generator*" $list_of_generator_with_no_processing]} {
  # case of generator which need to go down in order to keep one level more

  puts [ format "   processing has been stopped with instance %s" [get_attribute $instance_name name] ]
  return

 }

 if { [string match "*$generator*" $list_of_generator_with_one_deeper_level] } {
  # case of generator which need to go down in order to keep one level more

  current_design [get_attribute $instance_name ref_name]

  set deeper_level 1; set l_sub_module [list]
  foreach_in_collection _instance [sub_instances_of [current_design] -hierarchy] {
   set _instance_full_name [get_attribute $_instance full_name]
   set level "level"
   if { [ llength [split $_instance_full_name "/"] ] >= $deeper_level } {
    lappend l_sub_module [get_attribute $_instance ref_name]
    set level "LEVEL"
   }
   puts [ format " found sub_module %s (instance %s) (inst %s %d)" [get_attribute $_instance ref_name] $_instance_full_name $level [expr [llength [split $_instance_full_name "/"]]-1] ]
  }

  foreach _sub_module $l_sub_module {
   puts [format "  flatenning sub_module %s" $_sub_module]
   set cmd [format "current_design %s; ungroup -all -flatten" $_sub_module]
   puts " launching : `$cmd'" ; eval $cmd
  }

  current_design $top_design

 } elseif { [string match "*$generator*" $list_of_generator_with_two_deeper_level] } {
  # case of generator which need to go down in order to keep two level more

  current_design [get_attribute $instance_name ref_name]

  set deeper_level 2; set l_sub_module [list]
  foreach_in_collection _instance [sub_instances_of [current_design] -hierarchy] {
   set _instance_full_name [get_attribute $_instance full_name]
   set level "level"
   if { [ llength [split $_instance_full_name "/"] ] >= $deeper_level } {
    lappend l_sub_module [get_attribute $_instance ref_name]
    set level "LEVEL"
   }
   puts [ format " found sub_module %s (instance %s) (inst %s %d)" [get_attribute $_instance ref_name] $_instance_full_name $level [expr [llength [split $_instance_full_name "/"]]-1] ]
  }

  foreach _sub_module $l_sub_module {
   puts [format "  flatenning sub_module %s" $_sub_module]
   set cmd [format "current_design %s; ungroup -all -flatten" $_sub_module]
   puts " launching : `$cmd'" ; eval $cmd 
  }

  current_design $top_design

 } elseif { [string match "*$generator*" $list_of_generator_with_elementary_gate] } {

  # case of very small module containing only logic gate
  set uplevel_instance_name [file dirname $instance_name]
  set cmd [ format "current_design %s; ungroup %s -flatten; current_design %s" [get_attribute $uplevel_instance_name ref_name] [file tail $instance_name] $top_design ]
  puts " launching : `$cmd'" ; eval $cmd

 } else {

  # case of generator is defined (in this case the module is not a user defined module). Then default case is: flatten at the generator level without any restriction
  set cmd [ format "current_design %s; ungroup -all -flatten; current_design %s" [get_attribute $instance_name ref_name] $top_design ]
  puts " launching : `$cmd'" ; eval $cmd

 }
}


#
### Capture Rx register ($to_reg) connected to the Tx register ($from_reg) ######################################
#
# Syntax is based on the inter_clock_domain.tcl format where tx/rx clocks and tx register are present as command line parameters
# and then can be use directly as the following command line:
#  arteris_get_start_reg_end_reg -from_reg "${CUSTOMER_HIERARCHY}aux/SG2AUX_pwrDiscInit/PwrDiscInit/IdleReqS" -from_lsb "" -from_msb "" -from_clk "aux_cd_Cm_auxclk" -from_clk_P "${aux_cd_Cm_auxclk_P}" -to_clk "mcu_cd_Cm_busclk" -to_clk_P "${mcu_cd_Cm_busclk_P}" -delay "${aux_cd_Cm_auxclk_P}"
# Result is printed at the stdout with the tag: "Tx_reg"
# 
# In order to capture all matched tx_reg/rx_reg just used the following syntax:
# rename arteris_get_start_reg_end_reg arteris_inter_clock_domain
#  source $ARTERIS_INPUT_DIR/inter_clock_domain.tcl
# rename arteris_inter_clock_domain arteris_get_start_reg_end_reg
#
proc arteris_get_start_reg_end_reg { args } {
 #
 global VERBOSE
 #
 set state          "flag"
 set from_clk       "void"
 set from_reg       "void"
 set from_pin       "void"
 set from_clk_P     "void"
 set from_lsb       "void"
 set from_msb       "void"
 set to_clk         "void"
 set to_reg         "void"
 set to_clk_P       "void"
 set to_lsb         "void"
 set to_msb         "void"
 set delay          "void"

 set cmd ""
 foreach arg $args {
  switch -- $state {
   flag {
    switch -glob -- $arg {
     -from_clk   { set state "from_clk"   }
     -from_reg   { set state "from_reg"   }
     -from_pin   { set state "from_pin"   }
     -from_clk_P { set state "from_clk_P" }
     -from_lsb   { set state "from_lsb"   }
     -from_msb   { set state "from_msb"   }
     -to_clk     { set state "to_clk"     }
     -to_reg     { set state "to_reg"     }
     -to_clk_P   { set state "to_clk_P"   }
     -to_lsb     { set state "to_lsb"     }
     -to_msb     { set state "to_msb"     }
     -delay      { set state "delay"      }
     -margin     { set state "margin"     }
     default     { set state "default"    }
    }
   }
   from_clk {
    set from_clk $arg
    set state "flag"
   }
   from_reg {
    set from_reg $arg
    set state "flag"
   }
   from_pin {
    set from_pin $arg
    set state "flag"
   }
   from_clk_P {
    set from_clk_P $arg
    set state "flag"
   }
   from_lsb {
    set from_lsb $arg
    set state "flag"
   }
   from_msb {
    set from_msb $arg
    set state "flag"
   }
   to_clk {
    set to_clk $arg
    set state "flag"
   }
   to_reg {
    set to_reg $arg
    set state "flag"
   }
   to_clk_P {
    set to_clk_P $arg
    set state "flag"
   }
   to_lsb {
    set to_lsb $arg
    set state "flag"
   }
   to_msb {
    set to_msb $arg
    set state "flag"
   }
   delay {
    set delay $arg
    set state "flag"
   }
   margin {
    set margin $arg
    set state "flag"
   }
   default { set state "flag" }
  }
 }

 # check if string "_reg" is present in the register name to detect bus naming
 # list of all registers present in Rx clock domain
 set bus_lbracket {[}; set bus_rbracket {]}
 set not_seen_bracket 1
 set pattern {*_reg_*}
 set all_rx_reg [list]
 foreach_in_collection _reg [all_registers -clock $to_clk] {
  lappend all_rx_reg [string map {[ _ ] _} [get_attribute $_reg full_name]]
  if { $not_seen_bracket && [string match $pattern [get_attribute $_reg full_name]] } { set bus_lbracket {_}; set bus_rbracket {_}; set not_seen_bracket 0 }
 }
 # list of all ICG present in Rx clock domain
 set all_rx_icg [list]
 foreach_in_collection _icg [all_clock_gates -clock $to_clk] { lappend all_rx_icg [string map {[ _ ] _} [get_attribute $_icg full_name]] }

 # elaboration of a list of all tx registers
 list l_tx_reg
 if { $from_lsb == {} } {
  lappend l_tx_reg [format "%s_reg" $from_reg]
 } else {
  for {set i $from_lsb} {$i <= $from_msb} {incr i 1} {
   lappend l_tx_reg [format "%s_reg%s%d%s" $from_reg $bus_lbracket $i $bus_rbracket]
  }
 }

 set l_tx_output_pin [list]
 set global_rx_reg [list]
 puts ""
 foreach _tx_reg $l_tx_reg {
  puts [format "# Working on Tx register: %s (%s)" $_tx_reg [get_attribute $_tx_reg ref_name]]

  set l_rx_reg [list]
  foreach_in_collection _tx_pins [get_pins -of_objects $_tx_reg] {
   set _tx_pins_name [get_object_name $_tx_pins]
   if { [info exists VERBOSE] } { puts [ format "  ..founded Tx reg pin: %s (type=%s)" $_tx_pins_name [get_attribute $_tx_pins_name pin_direction] ] }

   if { [get_attribute -quiet $_tx_pins_name pin_direction] == "out" } {
    puts "  .Browsing fanout of Tx pin: $_tx_pins_name (type=[get_attribute $_tx_pins_name pin_direction])"

    foreach_in_collection _rx_reg [all_fanout -from $_tx_pins_name -flat -only_cell -endpoints] {
     if { [info exists VERBOSE] } { puts [ format "  ..founded Rx reg: %s (type=%s - id=%d/%d)" [get_attribute $_rx_reg full_name] [get_attribute $_rx_reg ref_name] [lsearch $all_rx_reg [get_attribute $_rx_reg full_name]] [lsearch $all_rx_icg [get_attribute $_rx_reg full_name]] ] }
     if { [lsearch $all_rx_reg [string map {[ _ ] _} [get_attribute $_rx_reg full_name]]] >= 0 || [lsearch $all_rx_icg [string map {[ _ ] _} [file dirname [get_attribute $_rx_reg full_name]]]] >= 0 } {
      lappend l_rx_reg [get_attribute $_rx_reg full_name]
     }
    }

   }
  }
  if { [llength l_rx_reg] > 0 } { array set tx_reg_rx_reg "$_tx_reg [list $l_rx_reg]" }

 }

 # print Tx reg connected to Rx reg
 puts "# Result is:"
 foreach {_tx_reg _rx_reg} [array get tx_reg_rx_reg] {
  set i 0
  foreach __rx_reg $_rx_reg {
   puts [format " Tx_reg: %s connected to Rx_reg(%3d): %s" $_tx_reg [incr i] $__rx_reg ]
   ### in order to verify the start/end point just, uncomment the following line (very slow)
   #redirect -variable path_to_be_validated "[format "report_timing -nosplit -from %s/* -to %s/*" $_tx_reg $__rx_reg]"
   #set __tx_reg "void"; set __rx_reg "void"; set __tx_clk "void"; set __rx_clk "void"; set _seen_max_delay "void"
   #foreach _line [split $path_to_be_validated \n] {
   # if { [string match "*Startpoint: *" $_line]} { scan $_line "%s %s" str1 __tx_reg; continue }
   # if { [string match "*Endpoint: *" $_line]}   { scan $_line "%s %s" str1 __rx_reg; continue }
   # if { [string match "*rising*clocked*" $_line] && $__tx_clk == "void" } { [regexp {(.*)rising edge-triggered flip-flop clocked by (.*)\)} $_line match str __tx_clk]; continue }
   # if { [string match "*internal*clocked*" $_line] && $__tx_clk == "void" } { [regexp {(.*)internal path startpoint clocked by (.*)\)} $_line match str __tx_clk]; continue }
   # if { [string match "*rising*clocked*" $_line] && $__rx_clk == "void" } { [regexp {(.*)rising edge-triggered flip-flop clocked by (.*)\)} $_line match str __rx_clk]; continue }
   # if { [string match "*gating*clock*" $_line] && $__rx_clk == "void" } { [regexp {(.*)gating element for clock (.*)\)} $_line match str __rx_clk]; continue }
   # if { [string match "*max_delay*" $_line] } { set _seen_max_delay "true"; continue }
   #}
   #if { [string match "*${from_reg}*" $__tx_reg] <= 0 } {
   # return -code error [format "function arteris_start_pt_end_pt : Tx_reg `%s' is not equal to the specified -from_reg `%s'" $__tx_reg $from_reg]
   #}
   #if { $__tx_clk != $from_clk } {
   # return -code error [format "function arteris_start_pt_end_pt : Tx_clk `%s' is not equal to the specified -from_clk `%s'" $__tx_clk $from_clk]
   #}
   #if { $__rx_clk != $to_clk } {
   # return -code error [format "function arteris_start_pt_end_pt : Rx_clk `%s' is not equal to the specified -to_clk `%s'" $__rx_clk $to_clk]
   #}
   #if { $_seen_max_delay != "true" } {
   # return -code error [format "function arteris_start_pt_end_pt : max_delay constraint is not present between Tx_reg `%s' and Rx_reg `%s'" $__tx_reg $__rx_clk]
   #}
   ###
  }
 }

}
