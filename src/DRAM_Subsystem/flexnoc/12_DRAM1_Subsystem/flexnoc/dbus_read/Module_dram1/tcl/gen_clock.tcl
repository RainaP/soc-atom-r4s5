
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:12

####################################################################

arteris_gen_clock -name "Regime_dram1_Cm_root" -pin "Regime_dram1_Cm_main/root_Clk Regime_dram1_Cm_main/root_Clk_ClkS" -clock_domain "clk_dram1" -spec_domain_clock "/Regime_dram1/Cm/root" -divide_by "1" -source "clk_dram1" -source_period "${clk_dram1_P}" -source_waveform "[expr ${clk_dram1_P}*0.00] [expr ${clk_dram1_P}*0.50]" -user_directive "" -add "FALSE"
