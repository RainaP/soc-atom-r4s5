
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:12

####################################################################

arteris_hierarchy -module "dbus_read_Structure_Module_dram1_Link_m1_astResp001_main" -generator "DatapathLink" -instance_name "Link_m1_astResp001_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_Link_m1_ast_main" -generator "DatapathLink" -instance_name "Link_m1_ast_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_Regime_dram1_Cm_main" -generator "ClockManager" -instance_name "Regime_dram1_Cm_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_clockGaters_Link_m1_astResp001_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_m1_astResp001_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_clockGaters_Link_m1_ast_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_m1_ast_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_clockGaters_dram1_r" -generator "ClockGater" -instance_name "clockGaters_dram1_r" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_clockGaters_dram1_r_T" -generator "ClockGater" -instance_name "clockGaters_dram1_r_T" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_dram1_r_T_main" -generator "T2G" -instance_name "dram1_r_T_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_dram1_r_main" -generator "G2S" -instance_name "dram1_r_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_z_T_C_S_C_L_R_A_4_1" -generator "gate" -instance_name "uAnd4" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1_z_T_C_S_C_L_R_O_4_1" -generator "gate" -instance_name "uOr4" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram1" -generator "" -instance_name "dbus_read_Structure_Module_dram1" -level "1"
