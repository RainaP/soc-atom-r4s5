
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:12

####################################################################

netlist clock Regime_dram1_Cm_main.root_Clk -module dbus_read_Structure_Module_dram1 -group clk_dram1 -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_dram1_Cm_main.root_Clk_ClkS -module dbus_read_Structure_Module_dram1 -group clk_dram1 -period 1.000 -waveform {0.000 0.500}
netlist clock clk_dram1 -module dbus_read_Structure_Module_dram1 -group clk_dram1 -period 1.000 -waveform {0.000 0.500}

