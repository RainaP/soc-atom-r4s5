
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:01

////////////////////////////////////////////////////////////////////

module dbus_write_Structure_Module_dram1_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_write_Structure_Module_dram1 -severity waived -scheme reconvergence -from_signals Link_m1_astResp001_main.DtpTxClkAdapt_Link_m1_asiResp001_Async.WrCnt
// 0in set_cdc_report -module dbus_write_Structure_Module_dram1 -severity waived -scheme reconvergence -from_signals Link_m1_ast_main.DtpRxClkAdapt_Link_m1_asi_Async.RdCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_write_Structure_Module_dram1 -severity waived -scheme multi_sync_mux_select -from Link_m1_ast_main.DtpRxClkAdapt_Link_m1_asi_Async.uRegSync.instSynchronizerCell*

endmodule
