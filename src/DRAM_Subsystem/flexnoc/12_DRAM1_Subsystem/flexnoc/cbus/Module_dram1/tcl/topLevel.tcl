#Top level TCL file


set CUSTOMER_HIERARCHY ""
set ARTERIS_INPUT_DIR    [format "%s" [sh pwd]]
set ARTERIS_OUTPUT_DIR    [format "%s" [sh pwd]]
set arteris_dflt_ARRIVAL_PERCENT           30
set arteris_dflt_REQUIRED_PERCENT          30
set arteris_comb_ARRIVAL_PERCENT           30
set arteris_comb_REQUIRED_PERCENT          40
set arteris_internal_dflt_ARRIVAL_PERCENT  70
set arteris_internal_dflt_REQUIRED_PERCENT 30
set arteris_internal_comb_ARRIVAL_PERCENT  40
set arteris_internal_comb_REQUIRED_PERCENT 60
set arteris_dflt_CLOCK_UNCERTAINTY_PERCENT  0
set GLOBAL_ICG_ENABLE_SETUP "100ps"
set IO_FILE $ARTERIS_OUTPUT_DIR/io_fanin_fanout_cone.rpt
source $ARTERIS_INPUT_DIR/include_dctopo.tcl
if {[file exists $ARTERIS_INPUT_DIR/include_arteris.tcl]} { source $ARTERIS_INPUT_DIR/include_arteris.tcl }
set T_Flex2library 1
set {clk_dram1_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_cbus_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_cbus_ls_east_P} [ expr 5.000 * $T_Flex2library ]
set {Regime_cbus_ls_west_P} [ expr 5.000 * $T_Flex2library ]
set {Regime_cbus_south_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_cpu_P} [ expr 0.667 * $T_Flex2library ]
set {Regime_dcluster0_P} [ expr 0.667 * $T_Flex2library ]
set {Regime_dcluster1_P} [ expr 0.667 * $T_Flex2library ]
set {Regime_dram0_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_dram2_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_dram3_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_pcie_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_peri_P} [ expr 5.000 * $T_Flex2library ]
set {Regime_cbus_Cm_center_root_P} [ expr ${Regime_cbus_P}*1 ]
set {Regime_cbus_Cm_east_root_P} [ expr ${Regime_cbus_P}*1 ]
set {Regime_cbus_ls_east_Cm_root_P} [ expr ${Regime_cbus_ls_east_P}*1 ]
set {Regime_cbus_ls_west_Cm_root_P} [ expr ${Regime_cbus_ls_west_P}*1 ]
set {Regime_cbus_south_Cm_root_P} [ expr ${Regime_cbus_south_P}*1 ]
set {Regime_cpu_Cm_root_P} [ expr ${Regime_cpu_P}*1 ]
set {Regime_dcluster0_Cm_root_P} [ expr ${Regime_dcluster0_P}*1 ]
set {Regime_dcluster1_Cm_root_P} [ expr ${Regime_dcluster1_P}*1 ]
set {Regime_dram0_Cm_root_P} [ expr ${Regime_dram0_P}*1 ]
set {Regime_dram2_Cm_root_P} [ expr ${Regime_dram2_P}*1 ]
set {Regime_dram3_Cm_root_P} [ expr ${Regime_dram3_P}*1 ]
set {Regime_pcie_Cm_root_P} [ expr ${Regime_pcie_P}*1 ]
set {Regime_peri_Cm_root_P} [ expr ${Regime_peri_P}*1 ]
set {Regime_dram1_Cm_root_P} [ expr ${clk_dram1_P}*1 ]

set all_clk [ list \
clk_dram1 \
Regime_cbus \
Regime_cbus_ls_east \
Regime_cbus_ls_west \
Regime_cbus_south \
Regime_cpu \
Regime_dcluster0 \
Regime_dcluster1 \
Regime_dram0 \
Regime_dram2 \
Regime_dram3 \
Regime_pcie \
Regime_peri \
Regime_cbus_Cm_center_root \
Regime_cbus_Cm_east_root \
Regime_cbus_ls_east_Cm_root \
Regime_cbus_ls_west_Cm_root \
Regime_cbus_south_Cm_root \
Regime_cpu_Cm_root \
Regime_dcluster0_Cm_root \
Regime_dcluster1_Cm_root \
Regime_dram0_Cm_root \
Regime_dram2_Cm_root \
Regime_dram3_Cm_root \
Regime_pcie_Cm_root \
Regime_peri_Cm_root \
Regime_dram1_Cm_root \
]
]set l_clk [ list \
Regime_dram1_Cm_root \
puts "rename arteris_create_clock arteris_virtual_clock"
rename arteris_create_clock arteris_virtual_clock
puts "source $ARTERIS_INPUT_DIR/tcl/virtual_clock.tcl"
source $ARTERIS_INPUT_DIR/tcl/virtual_clock.tcl
rename arteris_virtual_clock arteris_create_clock
puts "rename arteris_create_clock arteris_gen_virtual_clock"
rename arteris_create_clock arteris_gen_virtual_clock
puts "source $ARTERIS_INPUT_DIR/tcl/gen_virtual_clock.tcl"
source $ARTERIS_INPUT_DIR/tcl/gen_virtual_clock.tcl
rename arteris_gen_virtual_clock arteris_create_clock
puts "rename arteris_create_clock arteris_clock"
rename arteris_create_clock arteris_clock
source $ARTERIS_INPUT_DIR/tcl/clock.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/clock.tcl"
rename arteris_clock arteris_create_clock
puts "rename arteris_create_clock arteris_gen_clock"
rename arteris_create_clock arteris_gen_clock
source $ARTERIS_INPUT_DIR/tcl/gen_clock.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/gen_clock.tcl"
rename arteris_gen_clock arteris_create_clock
puts "rename arteris_set_clock_gating_check_setup arteris_customer_cell"
rename arteris_set_clock_gating_check_setup arteris_customer_cell
source $ARTERIS_INPUT_DIR/tcl/customer_cell.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/customer_cell.tcl"
rename arteris_customer_cell arteris_set_clock_gating_check_setup
puts "rename arteris_set_clock_uncertainty arteris_clock"
rename arteris_set_clock_uncertainty arteris_clock
source $ARTERIS_INPUT_DIR/tcl/clock.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/clock.tcl"
rename arteris_clock arteris_set_clock_uncertainty
rename arteris_set_clock_uncertainty arteris_gen_clock
source $ARTERIS_INPUT_DIR/tcl/gen_clock.tcl
rename arteris_gen_clock arteris_set_clock_uncertainty
set IO_FILE $ARTERIS_OUTPUT_DIR/io_fanin_fanout_cone.rpt
arteris_list_io_fanin_fanout_cone -ofile $IO_FILE
puts "rename arteris_set_input_delay arteris_input"
rename arteris_set_input_delay arteris_input
source $ARTERIS_INPUT_DIR/tcl/input.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/input.tcl"
rename arteris_input arteris_set_input_delay
puts "rename arteris_set_ideal_network arteris_reset"
rename arteris_set_ideal_network arteris_reset
source $ARTERIS_INPUT_DIR/tcl/reset.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/reset.tcl"
rename arteris_reset arteris_set_ideal_network
puts "rename arteris_set_ideal_network arteris_gen_reset"
rename arteris_set_ideal_network arteris_gen_reset
source $ARTERIS_INPUT_DIR/tcl/gen_reset.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/gen_reset.tcl"
rename arteris_gen_reset arteris_set_ideal_network
puts "rename arteris_set_ideal_network arteris_testMode"
rename arteris_set_ideal_network arteris_testMode
source $ARTERIS_INPUT_DIR/tcl/testMode.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/testMode.tcl"
rename arteris_testMode arteris_set_ideal_network
puts "rename arteris_set_output_delay arteris_output"
rename arteris_set_output_delay arteris_output
source $ARTERIS_INPUT_DIR/tcl/output.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/output.tcl"
rename arteris_output arteris_set_output_delay
puts "rename arteris_set_inter_clock_domain_constraints arteris_inter_clock_domain"
rename arteris_set_inter_clock_domain_constraints arteris_inter_clock_domain
source $ARTERIS_INPUT_DIR/tcl/inter_clock_domain.tcl
puts "source $ARTERIS_INPUT_DIR/tcl/inter_clock_domain.tcl"
rename arteris_inter_clock_domain arteris_set_inter_clock_domain_constraints
puts "rename arteris_check_fanin_of_global_clock_gater_EN arteris_customer_cell"
rename arteris_check_fanin_of_global_clock_gater_EN arteris_customer_cell
source $ARTERIS_INPUT_DIR/tcl/customer_cell.tcl > $ARTERIS_OUTPUT_DIR/map.check_fanin_of_global_clock_gater.rpt
puts "source $ARTERIS_INPUT_DIR/tcl/customer_cell.tcl > $ARTERIS_OUTPUT_DIR/map.check_fanin_of_global_clock_gater.rpt"
rename arteris_customer_cell arteris_check_fanin_of_global_clock_gater_EN
