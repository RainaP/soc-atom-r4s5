
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:15

####################################################################

arteris_clock -name "clk_dram1" -clock_domain "clk_dram1" -port "clk_dram1" -period "${clk_dram1_P}" -waveform "[expr ${clk_dram1_P}*0.00] [expr ${clk_dram1_P}*0.50]" -edge "R" -user_directive ""
