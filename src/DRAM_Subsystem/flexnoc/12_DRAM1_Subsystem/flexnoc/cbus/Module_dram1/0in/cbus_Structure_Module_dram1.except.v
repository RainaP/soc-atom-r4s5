
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:15

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_dram1_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module cbus_Structure_Module_dram1 -severity waived -scheme reconvergence -from_signals Link_m1ctrl_astResp001_main.DtpTxClkAdapt_Link_m1ctrl_asiResp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_dram1 -severity waived -scheme reconvergence -from_signals Link_m1ctrl_ast_main.DtpRxClkAdapt_Link_m1ctrl_asi_Async.RdCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module cbus_Structure_Module_dram1 -severity waived -scheme multi_sync_mux_select -from Link_m1ctrl_ast_main.DtpRxClkAdapt_Link_m1ctrl_asi_Async.uRegSync.instSynchronizerCell*

endmodule
