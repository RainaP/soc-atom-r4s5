
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:15

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_m1ctrl_astResp001_main.DtpTxClkAdapt_Link_m1ctrl_asiResp001_Async.WrCnt -graycode -module cbus_Structure_Module_dram1
cdc signal Link_m1ctrl_ast_main.DtpRxClkAdapt_Link_m1ctrl_asi_Async.RdCnt -graycode -module cbus_Structure_Module_dram1

