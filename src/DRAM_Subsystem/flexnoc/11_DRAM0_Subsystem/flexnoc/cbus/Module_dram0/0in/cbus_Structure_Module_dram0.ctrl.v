
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:10

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_dram0_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module cbus_Structure_Module_dram0 Link_m0ctrl_astResp001_main.DtpTxClkAdapt_Link_m0ctrl_asiResp001_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_dram0 Link_m0ctrl_astResp001_main.DtpTxClkAdapt_Link_m0ctrl_asiResp001_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_dram0 Link_m0ctrl_astResp001_main.DtpTxClkAdapt_Link_m0ctrl_asiResp001_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_dram0 Link_m0ctrl_astResp001_main.DtpTxClkAdapt_Link_m0ctrl_asiResp001_Async.RegData_3

endmodule
