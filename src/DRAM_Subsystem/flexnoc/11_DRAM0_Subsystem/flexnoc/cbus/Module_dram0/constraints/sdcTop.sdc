# Design variables used later in constraints
set CUSTOMER_HIERARCHY ""
set arteris_dflt_ARRIVAL_PERCENT           30
set arteris_dflt_REQUIRED_PERCENT          30
set arteris_comb_ARRIVAL_PERCENT           30
set arteris_comb_REQUIRED_PERCENT          40
set arteris_internal_dflt_ARRIVAL_PERCENT  70
set arteris_internal_dflt_REQUIRED_PERCENT 30
set arteris_internal_comb_ARRIVAL_PERCENT  40
set arteris_internal_comb_REQUIRED_PERCENT 60
set arteris_dflt_CLOCK_UNCERTAINTY          0
set T_Flex2library 1
set {clk_dram0_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_cbus_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_cbus_ls_east_P} [ expr 5.000 * $T_Flex2library ]
set {Regime_cbus_ls_west_P} [ expr 5.000 * $T_Flex2library ]
set {Regime_cbus_south_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_cpu_P} [ expr 0.667 * $T_Flex2library ]
set {Regime_dcluster0_P} [ expr 0.667 * $T_Flex2library ]
set {Regime_dcluster1_P} [ expr 0.667 * $T_Flex2library ]
set {Regime_dram1_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_dram2_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_dram3_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_pcie_P} [ expr 1.000 * $T_Flex2library ]
set {Regime_peri_P} [ expr 5.000 * $T_Flex2library ]
set {Regime_cbus_Cm_center_root_P} [ expr ${Regime_cbus_P}*1 ]
set {Regime_cbus_Cm_east_root_P} [ expr ${Regime_cbus_P}*1 ]
set {Regime_cbus_ls_east_Cm_root_P} [ expr ${Regime_cbus_ls_east_P}*1 ]
set {Regime_cbus_ls_west_Cm_root_P} [ expr ${Regime_cbus_ls_west_P}*1 ]
set {Regime_cbus_south_Cm_root_P} [ expr ${Regime_cbus_south_P}*1 ]
set {Regime_cpu_Cm_root_P} [ expr ${Regime_cpu_P}*1 ]
set {Regime_dcluster0_Cm_root_P} [ expr ${Regime_dcluster0_P}*1 ]
set {Regime_dcluster1_Cm_root_P} [ expr ${Regime_dcluster1_P}*1 ]
set {Regime_dram1_Cm_root_P} [ expr ${Regime_dram1_P}*1 ]
set {Regime_dram2_Cm_root_P} [ expr ${Regime_dram2_P}*1 ]
set {Regime_dram3_Cm_root_P} [ expr ${Regime_dram3_P}*1 ]
set {Regime_pcie_Cm_root_P} [ expr ${Regime_pcie_P}*1 ]
set {Regime_peri_Cm_root_P} [ expr ${Regime_peri_P}*1 ]
set {Regime_dram0_Cm_root_P} [ expr ${clk_dram0_P}*1 ]

source "constraints/internalConstraints.sdc"
source "constraints/externalConstraints.sdc"

