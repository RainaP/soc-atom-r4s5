
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:10

####################################################################

arteris_gen_clock -name "Regime_dram0_Cm_root" -pin "Regime_dram0_Cm_main/root_Clk Regime_dram0_Cm_main/root_Clk_ClkS" -clock_domain "clk_dram0" -spec_domain_clock "/Regime_dram0/Cm/root" -divide_by "1" -source "clk_dram0" -source_period "${clk_dram0_P}" -source_waveform "[expr ${clk_dram0_P}*0.00] [expr ${clk_dram0_P}*0.50]" -user_directive "" -add "FALSE"
