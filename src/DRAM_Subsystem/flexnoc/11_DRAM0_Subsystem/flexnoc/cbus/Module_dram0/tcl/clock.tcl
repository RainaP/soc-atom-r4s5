
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:10

####################################################################

arteris_clock -name "clk_dram0" -clock_domain "clk_dram0" -port "clk_dram0" -period "${clk_dram0_P}" -waveform "[expr ${clk_dram0_P}*0.00] [expr ${clk_dram0_P}*0.50]" -edge "R" -user_directive ""
