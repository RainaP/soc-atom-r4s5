
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:01

####################################################################

arteris_gen_reset -name "Regime_dram0_Cm_root" -clock_domain "clk_dram0" -clock "Regime_dram0_Cm_root" -spec_domain_clock "/Regime_dram0/Cm/root" -clock_period "${Regime_dram0_Cm_root_P}" -clock_waveform "[expr ${Regime_dram0_Cm_root_P}*0.00] [expr ${Regime_dram0_Cm_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_dram0_Cm_main/root_Clk_RstN" -lsb "" -msb ""
arteris_gen_reset -name "Regime_dram0_Cm_root" -clock_domain "clk_dram0" -clock "Regime_dram0_Cm_root" -spec_domain_clock "/Regime_dram0/Cm/root" -clock_period "${Regime_dram0_Cm_root_P}" -clock_waveform "[expr ${Regime_dram0_Cm_root_P}*0.00] [expr ${Regime_dram0_Cm_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_dram0_Cm_main/root_Clk_RstN" -lsb "" -msb ""
