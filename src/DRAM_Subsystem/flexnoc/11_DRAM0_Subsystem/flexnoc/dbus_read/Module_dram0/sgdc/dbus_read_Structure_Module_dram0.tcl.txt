
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:01

####################################################################




####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:01

####################################################################

# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_Link_m0_astResp001_main_Sys.SysOut_Pwr_Idle connected to pin clockGaters_Link_m0_astResp001_main_Sys.ClockGater.SysO_0_Pwr_Idle (attached to instance clockGaters_Link_m0_astResp001_main_Sys.ClockGater from module: dbus_read_Structure_Module_dram0_z_H_R_U_Cg_U_a69fc25d)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_Link_m0_astResp001_main_Sys.SysOut_Pwr_WakeUp connected to pin clockGaters_Link_m0_astResp001_main_Sys.ClockGater.SysO_0_Pwr_WakeUp (attached to instance clockGaters_Link_m0_astResp001_main_Sys.ClockGater from module: dbus_read_Structure_Module_dram0_z_H_R_U_Cg_U_a69fc25d)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_Link_m0_ast_main_Sys.SysOut_Pwr_Idle connected to pin clockGaters_Link_m0_ast_main_Sys.ClockGater.SysO_0_Pwr_Idle (attached to instance clockGaters_Link_m0_ast_main_Sys.ClockGater from module: dbus_read_Structure_Module_dram0_z_H_R_U_Cg_U_a69fc25d)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_Link_m0_ast_main_Sys.SysOut_Pwr_WakeUp connected to pin clockGaters_Link_m0_ast_main_Sys.ClockGater.SysO_0_Pwr_WakeUp (attached to instance clockGaters_Link_m0_ast_main_Sys.ClockGater from module: dbus_read_Structure_Module_dram0_z_H_R_U_Cg_U_a69fc25d)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_dram0_r.SysOut_Pwr_Idle connected to pin clockGaters_dram0_r.ClockGater.SysO_0_Pwr_Idle (attached to instance clockGaters_dram0_r.ClockGater from module: dbus_read_Structure_Module_dram0_z_H_R_U_Cg_U_a69fc25d)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_dram0_r.SysOut_Pwr_WakeUp connected to pin clockGaters_dram0_r.ClockGater.SysO_0_Pwr_WakeUp (attached to instance clockGaters_dram0_r.ClockGater from module: dbus_read_Structure_Module_dram0_z_H_R_U_Cg_U_a69fc25d)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_dram0_r_T.SysOut_Pwr_Idle connected to pin clockGaters_dram0_r_T.ClockGater.SysO_0_Pwr_Idle (attached to instance clockGaters_dram0_r_T.ClockGater from module: dbus_read_Structure_Module_dram0_z_H_R_U_Cg_U_a69fc25d)
# set_parameter no_convergence_check on clkGater(Sys_Pwr_[Idle|WakeUp]): net clockGaters_dram0_r_T.SysOut_Pwr_WakeUp connected to pin clockGaters_dram0_r_T.ClockGater.SysO_0_Pwr_WakeUp (attached to instance clockGaters_dram0_r_T.ClockGater from module: dbus_read_Structure_Module_dram0_z_H_R_U_Cg_U_a69fc25d)
# set_parameter no_convergence_check on g2g|g2mg|g2t|mg2g|mt2g|t2g(Pwr_Idle): net dram0_r_T_main.u_6276 connected to pin dram0_r_T_main.TransportToGeneric.Sys_Pwr_Idle (attached to instance dram0_r_T_main.TransportToGeneric from module: dbus_read_Structure_Module_dram0_z_H_R_G_T2_U_U_8191bd06)
