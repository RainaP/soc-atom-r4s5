# Validated with 0in (TM) version Version 10.4c_6 linux_x86_64 21 Dec 2015
# vlib work; vmap -c work work; vlog -skipsynthoffregion *.v
# qverify -c -do 0in/do.tcl
#

cdc preference -vectorize_nl

# turn on fifo synchronization recognition
#
cdc preference -fifo_scheme
#cdc preference fifo -sync_effort high
#cdc preference fifo -effort high

# turn on reconvergence
#
cdc reconvergence on
cdc preference reconvergence -depth 0 -divergence_depth 0

do 0in/dbus_read_Structure_Module_dram0.clkgrp.tcl
do 0in/dbus_read_Structure_Module_dram0.rst.tcl
do 0in/dbus_read_Structure_Module_dram0.io.tcl

do 0in/dbus_read_Structure_Module_dram0.ctrl.tcl
do 0in/dbus_read_Structure_Module_dram0.except.tcl

onerror continue

 cdc run -d dbus_read_Structure_Module_dram0 -auto_black_box -hcdc -process_dead_end -cr dbus_read_Structure_Module_dram0.rpt
 cdc generate report cdc_detail.rpt

exit

