
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:01

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_m0_astResp001_main.DtpTxClkAdapt_Link_m0_asiResp001_Async.WrCnt -graycode -module dbus_read_Structure_Module_dram0
cdc signal Link_m0_ast_main.DtpRxClkAdapt_Link_m0_asi_Async.RdCnt -graycode -module dbus_read_Structure_Module_dram0

