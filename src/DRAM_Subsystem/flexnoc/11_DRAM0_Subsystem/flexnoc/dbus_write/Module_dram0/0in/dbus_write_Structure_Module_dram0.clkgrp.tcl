
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:21:48

####################################################################

netlist clock Regime_dram0_Cm_main.root_Clk -module dbus_write_Structure_Module_dram0 -group clk_dram0 -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_dram0_Cm_main.root_Clk_ClkS -module dbus_write_Structure_Module_dram0 -group clk_dram0 -period 1.000 -waveform {0.000 0.500}
netlist clock clk_dram0 -module dbus_write_Structure_Module_dram0 -group clk_dram0 -period 1.000 -waveform {0.000 0.500}

