
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:21:48

####################################################################

arteris_hierarchy -module "dbus_write_Structure_Module_dram0_Link_m0_astResp001_main" -generator "DatapathLink" -instance_name "Link_m0_astResp001_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_Link_m0_ast_main" -generator "DatapathLink" -instance_name "Link_m0_ast_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_Regime_dram0_Cm_main" -generator "ClockManager" -instance_name "Regime_dram0_Cm_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_clockGaters_Link_m0_astResp001_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_m0_astResp001_main_Sys" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_clockGaters_Link_m0_ast_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_m0_ast_main_Sys" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_clockGaters_dram0_w" -generator "ClockGater" -instance_name "clockGaters_dram0_w" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_clockGaters_dram0_w_T" -generator "ClockGater" -instance_name "clockGaters_dram0_w_T" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_dram0_w_T_main" -generator "T2G" -instance_name "dram0_w_T_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_dram0_w_main" -generator "G2S" -instance_name "dram0_w_main" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_z_T_C_S_C_L_R_A_4_1" -generator "gate" -instance_name "uAnd4" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0_z_T_C_S_C_L_R_O_4_1" -generator "gate" -instance_name "uOr4" -level "2"
arteris_hierarchy -module "dbus_write_Structure_Module_dram0" -generator "" -instance_name "dbus_write_Structure_Module_dram0" -level "1"
