
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:21:59

####################################################################

# Create Clocks

create_clock -name "clk_dram0" [get_ports "clk_dram0"] -period "${clk_dram0_P}" -waveform "[expr ${clk_dram0_P}*0.00] [expr ${clk_dram0_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks clk_dram0] 
# Create Virtual Clocks

create_clock -name "Regime_bus" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_cpu" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]"
create_clock -name "Regime_dcluster0" -period "${Regime_dcluster0_P}" -waveform "[expr ${Regime_dcluster0_P}*0.00] [expr ${Regime_dcluster0_P}*0.50]"
create_clock -name "Regime_dcluster1" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]"
create_clock -name "Regime_dram1" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]"
create_clock -name "Regime_dram2" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]"
create_clock -name "Regime_dram3" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]"
create_clock -name "Regime_pcie" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cpu] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster0] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster1] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram1] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram2] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram3] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_pcie] 
# Create Generated Virtual Clocks 

create_clock -name "Regime_bus_Cm_center_root" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_bus_Cm_east_root" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_bus_Cm_south_root" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_bus_Cm_west_root" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_cpu_Cm_root" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]"
create_clock -name "Regime_dcluster0_Cm_root" -period "${Regime_dcluster0_P}" -waveform "[expr ${Regime_dcluster0_P}*0.00] [expr ${Regime_dcluster0_P}*0.50]"
create_clock -name "Regime_dcluster1_Cm_root" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]"
create_clock -name "Regime_dram1_Cm_root" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]"
create_clock -name "Regime_dram2_Cm_root" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]"
create_clock -name "Regime_dram3_Cm_root" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]"
create_clock -name "Regime_pcie_Cm_root" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_center_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_east_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_south_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_west_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cpu_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster0_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster1_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram1_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram2_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram3_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_pcie_Cm_root] 
# Create Generated Clocks

create_generated_clock -name "Regime_dram0_Cm_root" [get_pins "${CUSTOMER_HIERARCHY}Regime_dram0_Cm_main/root_Clk ${CUSTOMER_HIERARCHY}Regime_dram0_Cm_main/root_Clk_ClkS "] -source "clk_dram0" -divide_by "1"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram0_Cm_root] 

#warning:  uncomment below line will disabled inter-clock constraint(s) by considering them as false path(s)
#set_clock_groups -asynchronous  -group { Regime_dcluster1_Cm_root }  -group { Regime_bus_Cm_west_root }  -group { Regime_dram3_Cm_root }  -group { Regime_bus_Cm_center_root }  -group { Regime_dram2_Cm_root }  -group { Regime_dcluster0 }  -group { Regime_dcluster1 }  -group { clk_dram0 Regime_dram0_Cm_root }  -group { Regime_bus_Cm_south_root }  -group { Regime_dcluster0_Cm_root }  -group { Regime_dram1_Cm_root }  -group { Regime_bus }  -group { Regime_bus_Cm_east_root }  -group { Regime_cpu }  -group { Regime_pcie_Cm_root }  -group { Regime_pcie }  -group { Regime_cpu_Cm_root }  -group { Regime_dram1 }  -group { Regime_dram2 }  -group { Regime_dram3 } 
# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_dram0"]
# Clock Gating Checks 

set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_m0_astResp001_main_Sys/ClockGater/usce6557b897/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_m0_ast_main_Sys/ClockGater/usce6557b897/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dram0_w/ClockGater/usce6557b897/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_dram0_w_T/ClockGater/usce6557b897/instGaterCell/EN"]


