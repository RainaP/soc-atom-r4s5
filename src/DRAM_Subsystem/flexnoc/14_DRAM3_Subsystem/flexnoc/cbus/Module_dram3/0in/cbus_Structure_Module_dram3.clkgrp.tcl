
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:25

####################################################################

netlist clock Regime_dram3_Cm_main.root_Clk -module cbus_Structure_Module_dram3 -group clk_dram3 -period 1.000 -waveform {0.000 0.500}
netlist clock Regime_dram3_Cm_main.root_Clk_ClkS -module cbus_Structure_Module_dram3 -group clk_dram3 -period 1.000 -waveform {0.000 0.500}
netlist clock clk_dram3 -module cbus_Structure_Module_dram3 -group clk_dram3 -period 1.000 -waveform {0.000 0.500}

