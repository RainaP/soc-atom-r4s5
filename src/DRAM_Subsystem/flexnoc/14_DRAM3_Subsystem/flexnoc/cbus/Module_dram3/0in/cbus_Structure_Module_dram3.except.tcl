
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:25

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_m3ctrl_astResp001_main.DtpTxClkAdapt_Link10_Async.WrCnt -graycode -module cbus_Structure_Module_dram3
cdc signal Link_m3ctrl_ast_main.DtpRxClkAdapt_Link_m3ctrl_asi_Async.RdCnt -graycode -module cbus_Structure_Module_dram3

