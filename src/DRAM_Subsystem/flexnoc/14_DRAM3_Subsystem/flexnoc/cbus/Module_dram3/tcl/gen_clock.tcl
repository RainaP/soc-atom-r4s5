
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:25

####################################################################

arteris_gen_clock -name "Regime_dram3_Cm_root" -pin "Regime_dram3_Cm_main/root_Clk Regime_dram3_Cm_main/root_Clk_ClkS" -clock_domain "clk_dram3" -spec_domain_clock "/Regime_dram3/Cm/root" -divide_by "1" -source "clk_dram3" -source_period "${clk_dram3_P}" -source_waveform "[expr ${clk_dram3_P}*0.00] [expr ${clk_dram3_P}*0.50]" -user_directive "" -add "FALSE"
