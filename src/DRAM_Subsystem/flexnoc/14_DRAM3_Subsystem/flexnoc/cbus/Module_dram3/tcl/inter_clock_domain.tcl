
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:25

####################################################################

# variable CUSTOMER_HIERARCHY is present to adjust the RTL hierarchy with the final design hierarchy

# SYNC_LINES



# ASYNC_LINES


# PSEUDO_STATIC_LINES


# COMMENTED_LINES
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3ctrl_astResp001_main/DtpTxClkAdapt_Link10_Async/RegData_0" -from_lsb "0" -from_msb "57" -from_clk "Regime_dram3_Cm_root" -from_clk_P "${Regime_dram3_Cm_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3ctrl_astResp001_main/DtpTxClkAdapt_Link10_Async/RegData_1" -from_lsb "0" -from_msb "57" -from_clk "Regime_dram3_Cm_root" -from_clk_P "${Regime_dram3_Cm_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3ctrl_astResp001_main/DtpTxClkAdapt_Link10_Async/RegData_2" -from_lsb "0" -from_msb "57" -from_clk "Regime_dram3_Cm_root" -from_clk_P "${Regime_dram3_Cm_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
# arteris_inter_clock_domain -from_reg "${CUSTOMER_HIERARCHY}Link_m3ctrl_astResp001_main/DtpTxClkAdapt_Link10_Async/RegData_3" -from_lsb "0" -from_msb "57" -from_clk "Regime_dram3_Cm_root" -from_clk_P "${Regime_dram3_Cm_root_P}" -to_clk "Regime_cbus_ls_east_Cm_root" -to_clk_P "${Regime_cbus_ls_east_Cm_root_P}" -delay "[expr 2*${Regime_cbus_ls_east_Cm_root_P}]"
