
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:05:25

####################################################################

arteris_clock -name "clk_dram3" -clock_domain "clk_dram3" -port "clk_dram3" -period "${clk_dram3_P}" -waveform "[expr ${clk_dram3_P}*0.00] [expr ${clk_dram3_P}*0.50]" -edge "R" -user_directive ""
