
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:13:36

####################################################################

arteris_hierarchy -module "dbus_read_Structure_Module_dram3_Link_m3_astResp001_main" -generator "DatapathLink" -instance_name "Link_m3_astResp001_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_Link_m3_ast_main" -generator "DatapathLink" -instance_name "Link_m3_ast_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_Regime_dram3_Cm_main" -generator "ClockManager" -instance_name "Regime_dram3_Cm_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_clockGaters_Link_m3_astResp001_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_m3_astResp001_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_clockGaters_Link_m3_ast_main_Sys" -generator "ClockGater" -instance_name "clockGaters_Link_m3_ast_main_Sys" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_clockGaters_dram3_r" -generator "ClockGater" -instance_name "clockGaters_dram3_r" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_clockGaters_dram3_r_T" -generator "ClockGater" -instance_name "clockGaters_dram3_r_T" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_dram3_r_T_main" -generator "T2G" -instance_name "dram3_r_T_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_dram3_r_main" -generator "G2S" -instance_name "dram3_r_main" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_z_T_C_S_C_L_R_A_4_1" -generator "gate" -instance_name "uAnd4" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3_z_T_C_S_C_L_R_O_4_1" -generator "gate" -instance_name "uOr4" -level "2"
arteris_hierarchy -module "dbus_read_Structure_Module_dram3" -generator "" -instance_name "dbus_read_Structure_Module_dram3" -level "1"
