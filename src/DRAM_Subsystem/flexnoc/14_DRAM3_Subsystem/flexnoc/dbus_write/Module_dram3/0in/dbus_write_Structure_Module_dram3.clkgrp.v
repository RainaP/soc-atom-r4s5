
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:29

////////////////////////////////////////////////////////////////////

module dbus_write_Structure_Module_dram3_clkgrp;

// 0in set_cdc_clock Regime_dram3_Cm_main.root_Clk -module dbus_write_Structure_Module_dram3 -group clk_dram3 -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock Regime_dram3_Cm_main.root_Clk_ClkS -module dbus_write_Structure_Module_dram3 -group clk_dram3 -period 1.000 -waveform {0.000 0.500}
// 0in set_cdc_clock clk_dram3 -module dbus_write_Structure_Module_dram3 -group clk_dram3 -period 1.000 -waveform {0.000 0.500}

endmodule
