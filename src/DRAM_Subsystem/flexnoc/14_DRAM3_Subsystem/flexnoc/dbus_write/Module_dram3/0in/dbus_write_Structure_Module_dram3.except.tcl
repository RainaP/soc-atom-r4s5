
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:29

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_m3_astResp001_main.DtpTxClkAdapt_Link_m3_asiResp001_Async.WrCnt -graycode -module dbus_write_Structure_Module_dram3
cdc signal Link_m3_ast_main.DtpRxClkAdapt_Link_m3_asi_Async.RdCnt -graycode -module dbus_write_Structure_Module_dram3

