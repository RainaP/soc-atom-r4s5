
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:22:40

####################################################################

# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_dram3"]

# Create Test Mode 

set_ideal_network -no_propagate [get_ports "TM"]

set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram3_w_Aw_Ready"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram3_w_B_Id"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram3_w_B_Resp"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram3_w_B_User"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram3_w_B_Valid"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dram3_w_W_Ready"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m3_asi_to_Link_m3_ast_Data"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRst"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m3_asi_to_Link_m3_ast_WrCnt"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdCnt"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_internal_comb_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m3_astResp001_to_Link_m3_asiResp001_RdPtr"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRstAck"]
set_input_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*($arteris_internal_dflt_ARRIVAL_PERCENT/100.0)] [get_ports "dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Addr"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Burst"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Cache"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Id"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Len"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Lock"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Prot"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Size"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_User"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_Aw_Valid"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_B_Ready"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_W_Data"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_W_Last"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_W_Strb"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dram3_w_W_Valid"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m3_asi_to_Link_m3_ast_RdCnt"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m3_asi_to_Link_m3_ast_RdPtr"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m3_asi_to_Link_m3_ast_RxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m3_asi_to_Link_m3_ast_TxCtl_PwrOnRst"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_internal_comb_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m3_astResp001_to_Link_m3_asiResp001_Data"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m3_astResp001_to_Link_m3_asiResp001_RxCtl_PwrOnRst"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m3_astResp001_to_Link_m3_asiResp001_TxCtl_PwrOnRstAck"]
set_output_delay -clock Regime_dram3_Cm_root [expr ${Regime_dram3_Cm_root_P}*(1-($arteris_internal_dflt_REQUIRED_PERCENT/100.0))] [get_ports "dp_Link_m3_astResp001_to_Link_m3_asiResp001_WrCnt"]

