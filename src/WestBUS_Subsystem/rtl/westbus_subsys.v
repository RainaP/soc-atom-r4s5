// This file is generated from Magillem
// Generation : Wed Aug 11 13:20:14 KST 2021 by miock from project atom
// Component : rebellions atom westbus_subsys 0.0
// Design : rebellions atom westbus_subsys_arch 0.0
//  u_cbus_Structure_Module_cbus_ls_west arteris.com FLEXNOC cbus_Structure_Module_cbus_ls_west 4.5.1 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/cbus_Structure_Module_cbus_ls_west_1.xml 
// Magillem Release : 5.2021.1


module westbus_subsys(
   input  wire        TM,
   input  wire        aclk,
   input  wire        arstn,
   input  wire [2:0]  dp_Switch_westResp001_to_Link13Resp001_rdcnt,
   input  wire        dp_Switch_westResp001_to_Link13Resp001_rxctl_pwronrstack,
   output wire        dp_Switch_westResp001_to_Link13Resp001_rxctl_pwronrst,
   input  wire [1:0]  dp_Switch_westResp001_to_Link13Resp001_rdptr,
   output wire        dp_Switch_westResp001_to_Link13Resp001_txctl_pwronrstack,
   input  wire        dp_Switch_westResp001_to_Link13Resp001_txctl_pwronrst,
   output wire [57:0] dp_Switch_westResp001_to_Link13Resp001_data,
   output wire [2:0]  dp_Switch_westResp001_to_Link13Resp001_wrcnt,
   output wire [2:0]  dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_rdcnt,
   output wire        dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_rxctl_pwronrstack,
   input  wire        dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_rxctl_pwronrst,
   output wire [1:0]  dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_rdptr,
   input  wire        dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_txctl_pwronrstack,
   output wire        dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_txctl_pwronrst,
   input  wire [57:0] dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_data,
   input  wire [2:0]  dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_wrcnt,
   input  wire [2:0]  dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_rdcnt,
   input  wire        dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_rxctl_pwronrstack,
   output wire        dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_rxctl_pwronrst,
   input  wire [1:0]  dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_rdptr,
   output wire        dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_txctl_pwronrstack,
   input  wire        dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_txctl_pwronrst,
   output wire [57:0] dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_data,
   output wire [2:0]  dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_wrcnt,
   output wire [2:0]  dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_rdcnt,
   output wire        dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_rxctl_pwronrstack,
   input  wire        dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_rxctl_pwronrst,
   output wire [1:0]  dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_rdptr,
   input  wire        dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_txctl_pwronrstack,
   output wire        dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_txctl_pwronrst,
   input  wire [57:0] dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_data,
   input  wire [2:0]  dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_wrcnt,
   input  wire [2:0]  dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_rdcnt,
   input  wire        dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_rxctl_pwronrstack,
   output wire        dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_rxctl_pwronrst,
   input  wire [1:0]  dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_rdptr,
   output wire        dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_txctl_pwronrstack,
   input  wire        dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_txctl_pwronrst,
   output wire [57:0] dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_data,
   output wire [2:0]  dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_wrcnt,
   output wire [2:0]  dp_Link2_to_Switch_west_rdcnt,
   output wire        dp_Link2_to_Switch_west_rxctl_pwronrstack,
   input  wire        dp_Link2_to_Switch_west_rxctl_pwronrst,
   output wire [1:0]  dp_Link2_to_Switch_west_rdptr,
   input  wire        dp_Link2_to_Switch_west_txctl_pwronrstack,
   output wire        dp_Link2_to_Switch_west_txctl_pwronrst,
   input  wire [57:0] dp_Link2_to_Switch_west_data,
   input  wire [2:0]  dp_Link2_to_Switch_west_wrcnt 
);




cbus_Structure_Module_cbus_ls_west u_cbus_Structure_Module_cbus_ls_west (
      .dp_Switch_westResp001_to_Link13Resp001_RdCnt(                             dp_Switch_westResp001_to_Link13Resp001_rdcnt                          ),
      .dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRstAck(                 dp_Switch_westResp001_to_Link13Resp001_rxctl_pwronrstack              ),
      .dp_Switch_westResp001_to_Link13Resp001_RxCtl_PwrOnRst(                    dp_Switch_westResp001_to_Link13Resp001_rxctl_pwronrst                 ),
      .dp_Switch_westResp001_to_Link13Resp001_RdPtr(                             dp_Switch_westResp001_to_Link13Resp001_rdptr                          ),
      .dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRstAck(                 dp_Switch_westResp001_to_Link13Resp001_txctl_pwronrstack              ),
      .dp_Switch_westResp001_to_Link13Resp001_TxCtl_PwrOnRst(                    dp_Switch_westResp001_to_Link13Resp001_txctl_pwronrst                 ),
      .dp_Switch_westResp001_to_Link13Resp001_Data(                              dp_Switch_westResp001_to_Link13Resp001_data                           ),
      .dp_Switch_westResp001_to_Link13Resp001_WrCnt(                             dp_Switch_westResp001_to_Link13Resp001_wrcnt                          ),
      .dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdCnt(                dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_rdcnt             ),
      .dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_rxctl_pwronrstack ),
      .dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RxCtl_PwrOnRst(       dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_rxctl_pwronrst    ),
      .dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_RdPtr(                dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_rdptr             ),
      .dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_txctl_pwronrstack ),
      .dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_TxCtl_PwrOnRst(       dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_txctl_pwronrst    ),
      .dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_Data(                 dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_data              ),
      .dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_WrCnt(                dp_Link_m1ctrl_astResp001_to_Link_m1ctrl_asiResp001_wrcnt             ),
      .dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdCnt(                              dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_rdcnt                           ),
      .dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRstAck(                  dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_rxctl_pwronrstack               ),
      .dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RxCtl_PwrOnRst(                     dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_rxctl_pwronrst                  ),
      .dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_RdPtr(                              dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_rdptr                           ),
      .dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRstAck(                  dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_txctl_pwronrstack               ),
      .dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_TxCtl_PwrOnRst(                     dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_txctl_pwronrst                  ),
      .dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_Data(                               dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_data                            ),
      .dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_WrCnt(                              dp_Link_m1ctrl_asi_to_Link_m1ctrl_ast_wrcnt                           ),
      .dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdCnt(                dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_rdcnt             ),
      .dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_rxctl_pwronrstack ),
      .dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RxCtl_PwrOnRst(       dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_rxctl_pwronrst    ),
      .dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_RdPtr(                dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_rdptr             ),
      .dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_txctl_pwronrstack ),
      .dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_TxCtl_PwrOnRst(       dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_txctl_pwronrst    ),
      .dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_Data(                 dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_data              ),
      .dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_WrCnt(                dp_Link_m0ctrl_astResp001_to_Link_m0ctrl_asiResp001_wrcnt             ),
      .dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdCnt(                              dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_rdcnt                           ),
      .dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRstAck(                  dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_rxctl_pwronrstack               ),
      .dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RxCtl_PwrOnRst(                     dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_rxctl_pwronrst                  ),
      .dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_RdPtr(                              dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_rdptr                           ),
      .dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRstAck(                  dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_txctl_pwronrstack               ),
      .dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_TxCtl_PwrOnRst(                     dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_txctl_pwronrst                  ),
      .dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_Data(                               dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_data                            ),
      .dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_WrCnt(                              dp_Link_m0ctrl_asi_to_Link_m0ctrl_ast_wrcnt                           ),
      .dp_Link2_to_Switch_west_RdCnt(                                            dp_Link2_to_Switch_west_rdcnt                                         ),
      .dp_Link2_to_Switch_west_RxCtl_PwrOnRstAck(                                dp_Link2_to_Switch_west_rxctl_pwronrstack                             ),
      .dp_Link2_to_Switch_west_RxCtl_PwrOnRst(                                   dp_Link2_to_Switch_west_rxctl_pwronrst                                ),
      .dp_Link2_to_Switch_west_RdPtr(                                            dp_Link2_to_Switch_west_rdptr                                         ),
      .dp_Link2_to_Switch_west_TxCtl_PwrOnRstAck(                                dp_Link2_to_Switch_west_txctl_pwronrstack                             ),
      .dp_Link2_to_Switch_west_TxCtl_PwrOnRst(                                   dp_Link2_to_Switch_west_txctl_pwronrst                                ),
      .dp_Link2_to_Switch_west_Data(                                             dp_Link2_to_Switch_west_data                                          ),
      .dp_Link2_to_Switch_west_WrCnt(                                            dp_Link2_to_Switch_west_wrcnt                                         ),
      .clk_cbus_ls_west(                                                         aclk                                                                  ),
      .arstn_cbus_ls_west(                                                       arstn                                                                 ),
      .TM(                                                                       TM                                                                    ) 
      );



// constant signals initialisation
endmodule
