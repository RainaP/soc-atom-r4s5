
//
//
`timescale 1ns/1ps
module atom_bus_SynchronizerCell( CLK , I , O , RSTN );
	input   CLK  ;
	input   I    ;
	output  O    ;
	input   RSTN ;

// synthesis translate_off
`define USER_BEHAV_GATES
// synthesis translate_on


`ifdef USER_BEHAV_GATES
	wire  LClk      ;
	wire  LClk_RstN ;
	reg   OAsync    ;
	reg   O         ;
	assign LClk = CLK;
	assign LClk_RstN = RSTN;
	always @( posedge LClk or negedge LClk_RstN )
		if ( ! LClk_RstN )
			OAsync <= #0.001 ( 1'b0 );
		else	OAsync <= #0.001 ( I );
	always @( posedge LClk or negedge LClk_RstN )
		if ( ! LClk_RstN )
			O <= #0.001 ( 1'b0 );
		else	O <= #0.001 ( OAsync );

`else
SDFFYRPQ3D_X2N_A6ZTR_C10 sdffy(.Q(O), .CK(CLK), .D(I), .R(~RSTN), .SE(1'b0), .SI(1'b0));
`endif


endmodule


module VD_bus_SynchronizerCell_RstAsync ( input CLK , input I , output O , input RSTN );
	atom_bus_SynchronizerCell u_atom_bus_SynchronizerCell(.CLK (CLK), .I(I), .O(O), .RSTN(RSTN));
endmodule


module VD_cpu_SynchronizerCell_RstAsync ( input CLK , input I , output O , input RSTN );
	atom_bus_SynchronizerCell u_atom_bus_SynchronizerCell(.CLK (CLK), .I(I), .O(O), .RSTN(RSTN));
endmodule


module VD_dcluster0_SynchronizerCell_RstAsync ( input CLK , input I , output O , input RSTN );
	atom_bus_SynchronizerCell u_atom_bus_SynchronizerCell(.CLK (CLK), .I(I), .O(O), .RSTN(RSTN));
endmodule


module VD_dcluster1_SynchronizerCell_RstAsync ( input CLK , input I , output O , input RSTN );
	atom_bus_SynchronizerCell u_atom_bus_SynchronizerCell(.CLK (CLK), .I(I), .O(O), .RSTN(RSTN));
endmodule


module VD_dram_left_SynchronizerCell_RstAsync ( input CLK , input I , output O , input RSTN );
	atom_bus_SynchronizerCell u_atom_bus_SynchronizerCell(.CLK (CLK), .I(I), .O(O), .RSTN(RSTN));
endmodule


module VD_dram_right_SynchronizerCell_RstAsync ( input CLK , input I , output O , input RSTN );
	atom_bus_SynchronizerCell u_atom_bus_SynchronizerCell(.CLK (CLK), .I(I), .O(O), .RSTN(RSTN));
endmodule


module VD_pcie_SynchronizerCell_RstAsync ( input CLK , input I , output O , input RSTN );
	atom_bus_SynchronizerCell u_atom_bus_SynchronizerCell(.CLK (CLK), .I(I), .O(O), .RSTN(RSTN));
endmodule
