
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:28

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_m0ctrl_asiResp001_main.DtpRxClkAdapt_Link_m0ctrl_astResp001_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_west
cdc signal Link_m0ctrl_asi_main.DtpTxClkAdapt_Link_m0ctrl_ast_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_west
cdc signal Link_m1ctrl_asiResp001_main.DtpRxClkAdapt_Link_m1ctrl_astResp001_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_west
cdc signal Link_m1ctrl_asi_main.DtpTxClkAdapt_Link_m1ctrl_ast_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_west
cdc signal Switch_westResp001_main.DtpTxClkAdapt_Link13Resp001_Async.WrCnt -graycode -module cbus_Structure_Module_cbus_ls_west
cdc signal Switch_west_main.DtpRxClkAdapt_Link2_Async.RdCnt -graycode -module cbus_Structure_Module_cbus_ls_west

