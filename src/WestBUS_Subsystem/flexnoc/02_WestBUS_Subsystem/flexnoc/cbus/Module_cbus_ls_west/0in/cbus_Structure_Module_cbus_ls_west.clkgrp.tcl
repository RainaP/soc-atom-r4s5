
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:28

####################################################################

netlist clock Regime_cbus_ls_west_Cm_main.root_Clk -module cbus_Structure_Module_cbus_ls_west -group clk_cbus_ls_west -period 5.000 -waveform {0.000 2.500}
netlist clock Regime_cbus_ls_west_Cm_main.root_Clk_ClkS -module cbus_Structure_Module_cbus_ls_west -group clk_cbus_ls_west -period 5.000 -waveform {0.000 2.500}
netlist clock clk_cbus_ls_west -module cbus_Structure_Module_cbus_ls_west -group clk_cbus_ls_west -period 5.000 -waveform {0.000 2.500}

