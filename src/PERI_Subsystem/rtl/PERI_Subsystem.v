//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module PERI_Subsystem (
	 input         TEST_MODE
	,input         CLK__PERI
	,input         RSTN__PERI
	,output        EastBUS__INTG_0__IRQ
	,input         DFT__SCAN_MODE
	,output        EastBUS__WDT_0__IRQ
	,output        EastBUS__RSTSRC__WDT
	,input  [14:0] PVT_0__VSENSE_TS_ANALOG
	,input  [14:0] PVT_0__VOL_TS_ANALOG
	,output        PVT_0__VREFT_FLAG_TS
	,output        PVT_0__VBE_FLAG_TS
	,output [14:0] PVT_0__IBIAS_TS_ANALOG
	,inout         PVT_0__TEST_OUT_TS_ANALOG
	,output        TIMER_0__IO__PWM1
	,output        TIMER_0__IO__PWM2
	,output        TIMER_0__IO__PWM3
	,output        TIMER_0__IO__PWM4
	,output [ 3:0] EastBUS__TIMER_0__IRQ
	,input  [15:0] GPIO_0__IO__A_A
	,input  [15:0] GPIO_0__IO__B_A
	,input  [15:0] GPIO_0__IO__C_A
	,input  [15:0] GPIO_0__IO__D_A
	,output [15:0] GPIO_0__IO__A_Y
	,output [15:0] GPIO_0__IO__B_Y
	,output [15:0] GPIO_0__IO__C_Y
	,output [15:0] GPIO_0__IO__D_Y
	,output [15:0] GPIO_0__IO__A_EN
	,output [15:0] GPIO_0__IO__B_EN
	,output [15:0] GPIO_0__IO__C_EN
	,output [15:0] GPIO_0__IO__D_EN
	,output [15:0] EastBUS__GPIO_0__IRQ
	,input         EastBUS__UART_0__DMA__TX_ACK_N
	,input         EastBUS__UART_0__DMA__RX_ACK_N
	,output        EastBUS__UART_0__DMA__TX_REQ_N
	,output        EastBUS__UART_0__DMA__TX_SINGLE_N
	,output        EastBUS__UART_0__DMA__RX_REQ_N
	,output        EastBUS__UART_0__DMA__RX_SINGLE_N
	,input         UART_0__IO__RX
	,output        UART_0__IO__TX
	,output        EastBUS__UART_0__IRQ
	,input         EastBUS__UART_1__DMA__TX_ACK_N
	,input         EastBUS__UART_1__DMA__RX_ACK_N
	,output        EastBUS__UART_1__DMA__TX_REQ_N
	,output        EastBUS__UART_1__DMA__TX_SINGLE_N
	,output        EastBUS__UART_1__DMA__RX_REQ_N
	,output        EastBUS__UART_1__DMA__RX_SINGLE_N
	,input         UART_1__IO__RX
	,output        UART_1__IO__TX
	,output        EastBUS__UART_1__IRQ
	,input         EastBUS__I2C_0__DMA__TX_ACK
	,output        EastBUS__I2C_0__DMA__TX_REQ
	,output        EastBUS__I2C_0__DMA__TX_SINGLE
	,input         EastBUS__I2C_0__DMA__RX_ACK
	,output        EastBUS__I2C_0__DMA__RX_REQ
	,output        EastBUS__I2C_0__DMA__RX_SINGLE
	,input         I2C_0__IO__SCL
	,input         I2C_0__IO__SDA
	,output        EastBUS__I2C_0__IRQ
	,input         EastBUS__I2C_1__DMA__TX_ACK
	,output        EastBUS__I2C_1__DMA__TX_REQ
	,output        EastBUS__I2C_1__DMA__TX_SINGLE
	,input         EastBUS__I2C_1__DMA__RX_ACK
	,output        EastBUS__I2C_1__DMA__RX_REQ
	,output        EastBUS__I2C_1__DMA__RX_SINGLE
	,input         I2C_1__IO__SCL
	,input         I2C_1__IO__SDA
	,output        EastBUS__I2C_1__IRQ
	,input         EastBUS__I2C_2__DMA__TX_ACK
	,output        EastBUS__I2C_2__DMA__TX_REQ
	,output        EastBUS__I2C_2__DMA__TX_SINGLE
	,input         EastBUS__I2C_2__DMA__RX_ACK
	,output        EastBUS__I2C_2__DMA__RX_REQ
	,output        EastBUS__I2C_2__DMA__RX_SINGLE
	,input         I2C_2__IO__SCL
	,input         I2C_2__IO__SDA
	,output        EastBUS__I2C_2__IRQ
	,input         EastBUS__I2C_3__DMA__TX_ACK
	,output        EastBUS__I2C_3__DMA__TX_REQ
	,output        EastBUS__I2C_3__DMA__TX_SINGLE
	,input         EastBUS__I2C_3__DMA__RX_ACK
	,output        EastBUS__I2C_3__DMA__RX_REQ
	,output        EastBUS__I2C_3__DMA__RX_SINGLE
	,input         I2C_3__IO__SCL
	,input         I2C_3__IO__SDA
	,output        EastBUS__I2C_3__IRQ
	,input         EastBUS__SPI_0__DMA__TX_ACK
	,input         EastBUS__SPI_0__DMA__RX_ACK
	,output        EastBUS__SPI_0__DMA__TX_REQ
	,output        EastBUS__SPI_0__DMA__RX_REQ
	,output        EastBUS__SPI_0__DMA__TX_SINGLE
	,output        EastBUS__SPI_0__DMA__RX_SINGLE
	,output        SPI_0__IO__CLK
	,output        SPI_0__IO__CS
	,input  [ 7:0] SPI_0__IO__DAT_A
	,output [ 7:0] SPI_0__IO__DAT_Y
	,output [ 7:0] SPI_0__IO__DAT_EN
	,output        EastBUS__SPI_0__IRQ
	,input         EastBUS__SPI_1__DMA__TX_ACK
	,input         EastBUS__SPI_1__DMA__RX_ACK
	,output        EastBUS__SPI_1__DMA__TX_REQ
	,output        EastBUS__SPI_1__DMA__RX_REQ
	,output        EastBUS__SPI_1__DMA__TX_SINGLE
	,output        EastBUS__SPI_1__DMA__RX_SINGLE
	,output        SPI_1__IO__CLK
	,output        SPI_1__IO__CS
	,input  [ 7:0] SPI_1__IO__DAT_A
	,output [ 7:0] SPI_1__IO__DAT_Y
	,output [ 7:0] SPI_1__IO__DAT_EN
	,output        EastBUS__SPI_1__IRQ
	,input         EastBUS__SPI_2__DMA__TX_ACK
	,input         EastBUS__SPI_2__DMA__RX_ACK
	,output        EastBUS__SPI_2__DMA__TX_REQ
	,output        EastBUS__SPI_2__DMA__RX_REQ
	,output        EastBUS__SPI_2__DMA__TX_SINGLE
	,output        EastBUS__SPI_2__DMA__RX_SINGLE
	,output        SPI_2__IO__CLK
	,output        SPI_2__IO__CS
	,input  [ 7:0] SPI_2__IO__DAT_A
	,output [ 7:0] SPI_2__IO__DAT_Y
	,output [ 7:0] SPI_2__IO__DAT_EN
	,output        EastBUS__SPI_2__IRQ
	,input         EastBUS__SPI_3__DMA__TX_ACK
	,input         EastBUS__SPI_3__DMA__RX_ACK
	,output        EastBUS__SPI_3__DMA__TX_REQ
	,output        EastBUS__SPI_3__DMA__RX_REQ
	,output        EastBUS__SPI_3__DMA__TX_SINGLE
	,output        EastBUS__SPI_3__DMA__RX_SINGLE
	,output        SPI_3__IO__CLK
	,output        SPI_3__IO__CS
	,input  [ 7:0] SPI_3__IO__DAT_A
	,output [ 7:0] SPI_3__IO__DAT_Y
	,output [ 7:0] SPI_3__IO__DAT_EN
	,output        EastBUS__SPI_3__IRQ
	,output [57:0] EastBUS__dp_Link31_to_Switch_ctrl_tResp001_Data
	,input  [ 2:0] EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RdCnt
	,input  [ 2:0] EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RdPtr
	,output        EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst
	,input         EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck
	,input         EastBUS__dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst
	,output        EastBUS__dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck
	,output [ 2:0] EastBUS__dp_Link31_to_Switch_ctrl_tResp001_WrCnt
	,output [57:0] EastBUS__dp_Link32_to_Switch_ctrl_tResp001_Data
	,input  [ 2:0] EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RdCnt
	,input  [ 2:0] EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RdPtr
	,output        EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst
	,input         EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck
	,input         EastBUS__dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst
	,output        EastBUS__dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck
	,output [ 2:0] EastBUS__dp_Link32_to_Switch_ctrl_tResp001_WrCnt
	,output [57:0] EastBUS__dp_Link33_to_Switch_ctrl_tResp001_Data
	,input  [ 2:0] EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RdCnt
	,input  [ 2:0] EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RdPtr
	,output        EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst
	,input         EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck
	,input         EastBUS__dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst
	,output        EastBUS__dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck
	,output [ 2:0] EastBUS__dp_Link33_to_Switch_ctrl_tResp001_WrCnt
	,output [57:0] EastBUS__dp_Link34_to_Switch_ctrl_tResp001_Data
	,input  [ 2:0] EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RdCnt
	,input  [ 2:0] EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RdPtr
	,output        EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst
	,input         EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck
	,input         EastBUS__dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst
	,output        EastBUS__dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck
	,output [ 2:0] EastBUS__dp_Link34_to_Switch_ctrl_tResp001_WrCnt
	,input  [57:0] EastBUS__dp_Switch_ctrl_t_to_Link27_Data
	,output [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link27_RdCnt
	,output [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link27_RdPtr
	,input         EastBUS__dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst
	,output        EastBUS__dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck
	,output        EastBUS__dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst
	,input         EastBUS__dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck
	,input  [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link27_WrCnt
	,input  [57:0] EastBUS__dp_Switch_ctrl_t_to_Link28_Data
	,output [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link28_RdCnt
	,output [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link28_RdPtr
	,input         EastBUS__dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst
	,output        EastBUS__dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck
	,output        EastBUS__dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst
	,input         EastBUS__dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck
	,input  [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link28_WrCnt
	,input  [57:0] EastBUS__dp_Switch_ctrl_t_to_Link29_Data
	,output [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link29_RdCnt
	,output [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link29_RdPtr
	,input         EastBUS__dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst
	,output        EastBUS__dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck
	,output        EastBUS__dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst
	,input         EastBUS__dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck
	,input  [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link29_WrCnt
	,input  [57:0] EastBUS__dp_Switch_ctrl_t_to_Link30_Data
	,output [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link30_RdCnt
	,output [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link30_RdPtr
	,input         EastBUS__dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst
	,output        EastBUS__dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck
	,output        EastBUS__dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst
	,input         EastBUS__dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck
	,input  [ 2:0] EastBUS__dp_Switch_ctrl_t_to_Link30_WrCnt
	,output [57:0] MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_Data
	,input  [ 2:0] MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RdCnt
	,input  [ 1:0] MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RdPtr
	,output        MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst
	,input         MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck
	,input         MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst
	,output        MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck
	,output [ 2:0] MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_WrCnt
	,input  [57:0] MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_Data
	,output [ 2:0] MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt
	,output [ 1:0] MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr
	,input         MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst
	,output        MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck
	,output        MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst
	,input         MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck
	,input  [ 2:0] MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_PERI_Subsystem
	assign EastBUS__INTG_0__IRQ = 1'h0;
	assign EastBUS__WDT_0__IRQ = 1'h0;
	assign EastBUS__RSTSRC__WDT = 1'h0;
	assign PVT_0__VREFT_FLAG_TS = 1'h0;
	assign PVT_0__VBE_FLAG_TS = 1'h0;
	assign PVT_0__IBIAS_TS_ANALOG = 15'h0;
	assign TIMER_0__IO__PWM1 = 1'h0;
	assign TIMER_0__IO__PWM2 = 1'h0;
	assign TIMER_0__IO__PWM3 = 1'h0;
	assign TIMER_0__IO__PWM4 = 1'h0;
	assign EastBUS__TIMER_0__IRQ = 4'h0;
	assign GPIO_0__IO__A_Y = 16'h0;
	assign GPIO_0__IO__B_Y = 16'h0;
	assign GPIO_0__IO__C_Y = 16'h0;
	assign GPIO_0__IO__D_Y = 16'h0;
	assign GPIO_0__IO__A_EN = 16'h0;
	assign GPIO_0__IO__B_EN = 16'h0;
	assign GPIO_0__IO__C_EN = 16'h0;
	assign GPIO_0__IO__D_EN = 16'h0;
	assign EastBUS__GPIO_0__IRQ = 16'h0;
	assign EastBUS__UART_0__DMA__TX_REQ_N = 1'h0;
	assign EastBUS__UART_0__DMA__TX_SINGLE_N = 1'h0;
	assign EastBUS__UART_0__DMA__RX_REQ_N = 1'h0;
	assign EastBUS__UART_0__DMA__RX_SINGLE_N = 1'h0;
	assign UART_0__IO__TX = 1'h0;
	assign EastBUS__UART_0__IRQ = 1'h0;
	assign EastBUS__UART_1__DMA__TX_REQ_N = 1'h0;
	assign EastBUS__UART_1__DMA__TX_SINGLE_N = 1'h0;
	assign EastBUS__UART_1__DMA__RX_REQ_N = 1'h0;
	assign EastBUS__UART_1__DMA__RX_SINGLE_N = 1'h0;
	assign UART_1__IO__TX = 1'h0;
	assign EastBUS__UART_1__IRQ = 1'h0;
	assign EastBUS__I2C_0__DMA__TX_REQ = 1'h0;
	assign EastBUS__I2C_0__DMA__TX_SINGLE = 1'h0;
	assign EastBUS__I2C_0__DMA__RX_REQ = 1'h0;
	assign EastBUS__I2C_0__DMA__RX_SINGLE = 1'h0;
	assign EastBUS__I2C_0__IRQ = 1'h0;
	assign EastBUS__I2C_1__DMA__TX_REQ = 1'h0;
	assign EastBUS__I2C_1__DMA__TX_SINGLE = 1'h0;
	assign EastBUS__I2C_1__DMA__RX_REQ = 1'h0;
	assign EastBUS__I2C_1__DMA__RX_SINGLE = 1'h0;
	assign EastBUS__I2C_1__IRQ = 1'h0;
	assign EastBUS__I2C_2__DMA__TX_REQ = 1'h0;
	assign EastBUS__I2C_2__DMA__TX_SINGLE = 1'h0;
	assign EastBUS__I2C_2__DMA__RX_REQ = 1'h0;
	assign EastBUS__I2C_2__DMA__RX_SINGLE = 1'h0;
	assign EastBUS__I2C_2__IRQ = 1'h0;
	assign EastBUS__I2C_3__DMA__TX_REQ = 1'h0;
	assign EastBUS__I2C_3__DMA__TX_SINGLE = 1'h0;
	assign EastBUS__I2C_3__DMA__RX_REQ = 1'h0;
	assign EastBUS__I2C_3__DMA__RX_SINGLE = 1'h0;
	assign EastBUS__I2C_3__IRQ = 1'h0;
	assign EastBUS__SPI_0__DMA__TX_REQ = 1'h0;
	assign EastBUS__SPI_0__DMA__RX_REQ = 1'h0;
	assign EastBUS__SPI_0__DMA__TX_SINGLE = 1'h0;
	assign EastBUS__SPI_0__DMA__RX_SINGLE = 1'h0;
	assign SPI_0__IO__CLK = 1'h0;
	assign SPI_0__IO__CS = 1'h0;
	assign SPI_0__IO__DAT_Y = 8'h0;
	assign SPI_0__IO__DAT_EN = 8'h0;
	assign EastBUS__SPI_0__IRQ = 1'h0;
	assign EastBUS__SPI_1__DMA__TX_REQ = 1'h0;
	assign EastBUS__SPI_1__DMA__RX_REQ = 1'h0;
	assign EastBUS__SPI_1__DMA__TX_SINGLE = 1'h0;
	assign EastBUS__SPI_1__DMA__RX_SINGLE = 1'h0;
	assign SPI_1__IO__CLK = 1'h0;
	assign SPI_1__IO__CS = 1'h0;
	assign SPI_1__IO__DAT_Y = 8'h0;
	assign SPI_1__IO__DAT_EN = 8'h0;
	assign EastBUS__SPI_1__IRQ = 1'h0;
	assign EastBUS__SPI_2__DMA__TX_REQ = 1'h0;
	assign EastBUS__SPI_2__DMA__RX_REQ = 1'h0;
	assign EastBUS__SPI_2__DMA__TX_SINGLE = 1'h0;
	assign EastBUS__SPI_2__DMA__RX_SINGLE = 1'h0;
	assign SPI_2__IO__CLK = 1'h0;
	assign SPI_2__IO__CS = 1'h0;
	assign SPI_2__IO__DAT_Y = 8'h0;
	assign SPI_2__IO__DAT_EN = 8'h0;
	assign EastBUS__SPI_2__IRQ = 1'h0;
	assign EastBUS__SPI_3__DMA__TX_REQ = 1'h0;
	assign EastBUS__SPI_3__DMA__RX_REQ = 1'h0;
	assign EastBUS__SPI_3__DMA__TX_SINGLE = 1'h0;
	assign EastBUS__SPI_3__DMA__RX_SINGLE = 1'h0;
	assign SPI_3__IO__CLK = 1'h0;
	assign SPI_3__IO__CS = 1'h0;
	assign SPI_3__IO__DAT_Y = 8'h0;
	assign SPI_3__IO__DAT_EN = 8'h0;
	assign EastBUS__SPI_3__IRQ = 1'h0;
	assign EastBUS__dp_Link31_to_Switch_ctrl_tResp001_Data = 58'h0;
	assign EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Link31_to_Switch_ctrl_tResp001_WrCnt = 3'h0;
	assign EastBUS__dp_Link32_to_Switch_ctrl_tResp001_Data = 58'h0;
	assign EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Link32_to_Switch_ctrl_tResp001_WrCnt = 3'h0;
	assign EastBUS__dp_Link33_to_Switch_ctrl_tResp001_Data = 58'h0;
	assign EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Link33_to_Switch_ctrl_tResp001_WrCnt = 3'h0;
	assign EastBUS__dp_Link34_to_Switch_ctrl_tResp001_Data = 58'h0;
	assign EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Link34_to_Switch_ctrl_tResp001_WrCnt = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link27_RdCnt = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link27_RdPtr = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link28_RdCnt = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link28_RdPtr = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link29_RdCnt = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link29_RdPtr = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst = 1'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link30_RdCnt = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link30_RdPtr = 3'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck = 1'h0;
	assign EastBUS__dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_Data = 58'h0;
	assign MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst = 1'h0;
	assign MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_WrCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt = 3'h0;
	assign MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr = 2'h0;
	assign MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck = 1'h0;
	assign MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst = 1'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_PERI_Subsystem
	wire [23:0] AXI2APB_0__PERI_PBUS_0__apb_mst0_PAddr;
	wire        AXI2APB_0__PERI_PBUS_0__apb_mst0_PEnable;
	wire [ 2:0] AXI2APB_0__PERI_PBUS_0__apb_mst0_PProt;
	wire [31:0] AXI2APB_0__PERI_PBUS_0__apb_mst0_PRData;
	wire        AXI2APB_0__PERI_PBUS_0__apb_mst0_PReady;
	wire        AXI2APB_0__PERI_PBUS_0__apb_mst0_PSel;
	wire        AXI2APB_0__PERI_PBUS_0__apb_mst0_PSlvErr;
	wire [ 3:0] AXI2APB_0__PERI_PBUS_0__apb_mst0_PStrb;
	wire [31:0] AXI2APB_0__PERI_PBUS_0__apb_mst0_PWData;
	wire        AXI2APB_0__PERI_PBUS_0__apb_mst0_PWrite;
	wire        SCU_0__PERI_PBUS_0__APB__PENABLE;
	wire        SCU_0__PERI_PBUS_0__APB__PSEL;
	wire        SCU_0__PERI_PBUS_0__APB__PREADY;
	wire        SCU_0__PERI_PBUS_0__APB__PSLVERR;
	wire        SCU_0__PERI_PBUS_0__APB__PWRITE;
	wire [ 2:0] SCU_0__PERI_PBUS_0__APB__PPROT;
	wire [11:0] SCU_0__PERI_PBUS_0__APB__PADDR;
	wire [ 3:0] SCU_0__PERI_PBUS_0__APB__PSTRB;
	wire [31:0] SCU_0__PERI_PBUS_0__APB__PWDATA;
	wire [31:0] SCU_0__PERI_PBUS_0__APB__PRDATA;
	wire        MBOX_0__PERI_PBUS_0__APB__PENABLE;
	wire        MBOX_0__PERI_PBUS_0__APB__PSEL;
	wire        MBOX_0__PERI_PBUS_0__APB__PREADY;
	wire        MBOX_0__PERI_PBUS_0__APB__PSLVERR;
	wire        MBOX_0__PERI_PBUS_0__APB__PWRITE;
	wire [ 2:0] MBOX_0__PERI_PBUS_0__APB__PPROT;
	wire [ 7:0] MBOX_0__PERI_PBUS_0__APB__PADDR;
	wire [ 3:0] MBOX_0__PERI_PBUS_0__APB__PSTRB;
	wire [31:0] MBOX_0__PERI_PBUS_0__APB__PWDATA;
	wire [31:0] MBOX_0__PERI_PBUS_0__APB__PRDATA;
	wire        WDT_0__PERI_PBUS_0__APB__PENABLE;
	wire        WDT_0__PERI_PBUS_0__APB__PWRITE;
	wire [31:0] WDT_0__PERI_PBUS_0__APB__PWDATA;
	wire [ 7:0] WDT_0__PERI_PBUS_0__APB__PADDR;
	wire        WDT_0__PERI_PBUS_0__APB__PSEL;
	wire        WDT_0__PERI_PBUS_0__APB__PREADY;
	wire        WDT_0__PERI_PBUS_0__APB__PSLVERR;
	wire [31:0] WDT_0__PERI_PBUS_0__APB__PRDATA;
	wire        OTP_0__PERI_PBUS_0__PSEL;
	wire        OTP_0__PERI_PBUS_0__PENABLE;
	wire        OTP_0__PERI_PBUS_0__PWRITE;
	wire [15:0] OTP_0__PERI_PBUS_0__PADDR;
	wire [31:0] OTP_0__PERI_PBUS_0__PWDATA;
	wire        OTP_0__PERI_PBUS_0__PREADY;
	wire [31:0] OTP_0__PERI_PBUS_0__PRDATA;
	wire        OTP_0__PERI_PBUS_0__PSLVERR;
	wire        PVT_0__PERI_PBUS_0__PSEL;
	wire        PVT_0__PERI_PBUS_0__PENABLE;
	wire        PVT_0__PERI_PBUS_0__PWRITE;
	wire [11:0] PVT_0__PERI_PBUS_0__PADDR;
	wire [31:0] PVT_0__PERI_PBUS_0__PWDATA;
	wire [ 3:0] PVT_0__PERI_PBUS_0__PSTRB;
	wire        PVT_0__PERI_PBUS_0__PREADY;
	wire [31:0] PVT_0__PERI_PBUS_0__PRDATA;
	wire        PVT_0__PERI_PBUS_0__PSLVERR;
	wire        TIMER_0__PERI_PBUS_0__APB__PENABLE;
	wire        TIMER_0__PERI_PBUS_0__APB__PSEL;
	wire        TIMER_0__PERI_PBUS_0__APB__PWRITE;
	wire [ 7:0] TIMER_0__PERI_PBUS_0__APB__PADDR;
	wire [31:0] TIMER_0__PERI_PBUS_0__APB__PWDATA;
	wire        TIMER_0__PERI_PBUS_0__APB__PREADY;
	wire        TIMER_0__PERI_PBUS_0__APB__PSLVERR;
	wire [31:0] TIMER_0__PERI_PBUS_0__APB__PRDATA;
	wire        GPIO_0__PERI_PBUS_0__APB__PREADY;
	wire        GPIO_0__PERI_PBUS_0__APB__PSLVERR;
	wire        GPIO_0__PERI_PBUS_0__APB__PENABLE;
	wire        GPIO_0__PERI_PBUS_0__APB__PWRITE;
	wire [31:0] GPIO_0__PERI_PBUS_0__APB__PWDATA;
	wire [ 6:0] GPIO_0__PERI_PBUS_0__APB__PADDR;
	wire        GPIO_0__PERI_PBUS_0__APB__PSEL;
	wire [31:0] GPIO_0__PERI_PBUS_0__APB__PRDATA;
	wire        UART_0__PERI_PBUS_0__APB__PENABLE;
	wire        UART_0__PERI_PBUS_0__APB__PWRITE;
	wire [31:0] UART_0__PERI_PBUS_0__APB__PWDATA;
	wire [ 7:0] UART_0__PERI_PBUS_0__APB__PADDR;
	wire        UART_0__PERI_PBUS_0__APB__PSEL;
	wire [31:0] UART_0__PERI_PBUS_0__APB__PRDATA;
	wire        UART_0__PERI_PBUS_0__APB__PREADY;
	wire        UART_0__PERI_PBUS_0__APB__PSLVERR;
	wire        UART_1__PERI_PBUS_0__APB__PENABLE;
	wire        UART_1__PERI_PBUS_0__APB__PWRITE;
	wire [31:0] UART_1__PERI_PBUS_0__APB__PWDATA;
	wire [ 7:0] UART_1__PERI_PBUS_0__APB__PADDR;
	wire        UART_1__PERI_PBUS_0__APB__PSEL;
	wire [31:0] UART_1__PERI_PBUS_0__APB__PRDATA;
	wire        UART_1__PERI_PBUS_0__APB__PREADY;
	wire        UART_1__PERI_PBUS_0__APB__PSLVERR;
	wire        I2C_0__PERI_PBUS_0__APB__PSEL;
	wire        I2C_0__PERI_PBUS_0__APB__PENABLE;
	wire        I2C_0__PERI_PBUS_0__APB__PWRITE;
	wire [ 7:0] I2C_0__PERI_PBUS_0__APB__PADDR;
	wire [31:0] I2C_0__PERI_PBUS_0__APB__PWDATA;
	wire [31:0] I2C_0__PERI_PBUS_0__APB__PRDATA;
	wire        I2C_0__PERI_PBUS_0__APB__PREADY;
	wire        I2C_0__PERI_PBUS_0__APB__PSLVERR;
	wire        I2C_1__PERI_PBUS_0__APB__PSEL;
	wire        I2C_1__PERI_PBUS_0__APB__PENABLE;
	wire        I2C_1__PERI_PBUS_0__APB__PWRITE;
	wire [ 7:0] I2C_1__PERI_PBUS_0__APB__PADDR;
	wire [31:0] I2C_1__PERI_PBUS_0__APB__PWDATA;
	wire [31:0] I2C_1__PERI_PBUS_0__APB__PRDATA;
	wire        I2C_1__PERI_PBUS_0__APB__PREADY;
	wire        I2C_1__PERI_PBUS_0__APB__PSLVERR;
	wire        I2C_2__PERI_PBUS_0__APB__PSEL;
	wire        I2C_2__PERI_PBUS_0__APB__PENABLE;
	wire        I2C_2__PERI_PBUS_0__APB__PWRITE;
	wire [ 7:0] I2C_2__PERI_PBUS_0__APB__PADDR;
	wire [31:0] I2C_2__PERI_PBUS_0__APB__PWDATA;
	wire [31:0] I2C_2__PERI_PBUS_0__APB__PRDATA;
	wire        I2C_2__PERI_PBUS_0__APB__PREADY;
	wire        I2C_2__PERI_PBUS_0__APB__PSLVERR;
	wire        I2C_3__PERI_PBUS_0__APB__PSEL;
	wire        I2C_3__PERI_PBUS_0__APB__PENABLE;
	wire        I2C_3__PERI_PBUS_0__APB__PWRITE;
	wire [ 7:0] I2C_3__PERI_PBUS_0__APB__PADDR;
	wire [31:0] I2C_3__PERI_PBUS_0__APB__PWDATA;
	wire [31:0] I2C_3__PERI_PBUS_0__APB__PRDATA;
	wire        I2C_3__PERI_PBUS_0__APB__PREADY;
	wire        I2C_3__PERI_PBUS_0__APB__PSLVERR;
	wire        SPI_0__PERI_PBUS_0__APB__PSEL;
	wire        SPI_0__PERI_PBUS_0__APB__PENABLE;
	wire        SPI_0__PERI_PBUS_0__APB__PWRITE;
	wire [31:0] SPI_0__PERI_PBUS_0__APB__PADDR;
	wire [31:0] SPI_0__PERI_PBUS_0__APB__PWDATA;
	wire        SPI_0__PERI_PBUS_0__APB__PREADY;
	wire        SPI_0__PERI_PBUS_0__APB__PSLVERR;
	wire [31:0] SPI_0__PERI_PBUS_0__APB__PRDATA;
	wire        SPI_1__PERI_PBUS_0__APB__PSEL;
	wire        SPI_1__PERI_PBUS_0__APB__PENABLE;
	wire        SPI_1__PERI_PBUS_0__APB__PWRITE;
	wire [31:0] SPI_1__PERI_PBUS_0__APB__PADDR;
	wire [31:0] SPI_1__PERI_PBUS_0__APB__PWDATA;
	wire        SPI_1__PERI_PBUS_0__APB__PREADY;
	wire        SPI_1__PERI_PBUS_0__APB__PSLVERR;
	wire [31:0] SPI_1__PERI_PBUS_0__APB__PRDATA;
	wire        SPI_2__PERI_PBUS_0__APB__PSEL;
	wire        SPI_2__PERI_PBUS_0__APB__PENABLE;
	wire        SPI_2__PERI_PBUS_0__APB__PWRITE;
	wire [31:0] SPI_2__PERI_PBUS_0__APB__PADDR;
	wire [31:0] SPI_2__PERI_PBUS_0__APB__PWDATA;
	wire        SPI_2__PERI_PBUS_0__APB__PREADY;
	wire        SPI_2__PERI_PBUS_0__APB__PSLVERR;
	wire [31:0] SPI_2__PERI_PBUS_0__APB__PRDATA;
	wire        SPI_3__PERI_PBUS_0__APB__PSEL;
	wire        SPI_3__PERI_PBUS_0__APB__PENABLE;
	wire        SPI_3__PERI_PBUS_0__APB__PWRITE;
	wire [31:0] SPI_3__PERI_PBUS_0__APB__PADDR;
	wire [31:0] SPI_3__PERI_PBUS_0__APB__PWDATA;
	wire        SPI_3__PERI_PBUS_0__APB__PREADY;
	wire        SPI_3__PERI_PBUS_0__APB__PSLVERR;
	wire [31:0] SPI_3__PERI_PBUS_0__APB__PRDATA;
	wire [23:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Addr;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Burst;
	wire [ 3:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Cache;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Lock;
	wire [ 2:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Prot;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Ready;
	wire [ 2:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Size;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Valid;
	wire [23:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Addr;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Burst;
	wire [ 3:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Cache;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Lock;
	wire [ 2:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Prot;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Ready;
	wire [ 2:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Size;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Valid;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Ready;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Resp;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Valid;
	wire [31:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Data;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Last;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Ready;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Resp;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Valid;
	wire [31:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Data;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Last;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Ready;
	wire [ 3:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Strb;
	wire        cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Valid;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Id;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Id;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Id;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Id;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Len;
	wire [ 1:0] cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Len;

	PERI_BUS #(
		 .ABITS(24)
	) PERI_PBUS_0 (
		 .CLK__APB             (CLK__PERI)
		,.RSTN__APB            (RSTN__PERI)
		,.APB__PSEL            (AXI2APB_0__PERI_PBUS_0__apb_mst0_PSel)
		,.APB__PENABLE         (AXI2APB_0__PERI_PBUS_0__apb_mst0_PEnable)
		,.APB__PWRITE          (AXI2APB_0__PERI_PBUS_0__apb_mst0_PWrite)
		,.APB__PPROT           (AXI2APB_0__PERI_PBUS_0__apb_mst0_PProt)
		,.APB__PADDR           (AXI2APB_0__PERI_PBUS_0__apb_mst0_PAddr)
		,.APB__PSTRB           (AXI2APB_0__PERI_PBUS_0__apb_mst0_PStrb)
		,.APB__PWDATA          (AXI2APB_0__PERI_PBUS_0__apb_mst0_PWData)
		,.APB__PREADY          (AXI2APB_0__PERI_PBUS_0__apb_mst0_PReady)
		,.APB__PSLVERR         (AXI2APB_0__PERI_PBUS_0__apb_mst0_PSlvErr)
		,.APB__PRDATA          (AXI2APB_0__PERI_PBUS_0__apb_mst0_PRData)
		,.SCU_0__APB__PSEL     (SCU_0__PERI_PBUS_0__APB__PSEL)
		,.SCU_0__APB__PENABLE  (SCU_0__PERI_PBUS_0__APB__PENABLE)
		,.SCU_0__APB__PWRITE   (SCU_0__PERI_PBUS_0__APB__PWRITE)
		,.SCU_0__APB__PPROT    (SCU_0__PERI_PBUS_0__APB__PPROT)
		,.SCU_0__APB__PADDR    (SCU_0__PERI_PBUS_0__APB__PADDR)
		,.SCU_0__APB__PSTRB    (SCU_0__PERI_PBUS_0__APB__PSTRB)
		,.SCU_0__APB__PWDATA   (SCU_0__PERI_PBUS_0__APB__PWDATA)
		,.SCU_0__APB__PREADY   (SCU_0__PERI_PBUS_0__APB__PREADY)
		,.SCU_0__APB__PSLVERR  (SCU_0__PERI_PBUS_0__APB__PSLVERR)
		,.SCU_0__APB__PRDATA   (SCU_0__PERI_PBUS_0__APB__PRDATA)
		,.MBOX_0__APB__PSEL    (MBOX_0__PERI_PBUS_0__APB__PSEL)
		,.MBOX_0__APB__PENABLE (MBOX_0__PERI_PBUS_0__APB__PENABLE)
		,.MBOX_0__APB__PWRITE  (MBOX_0__PERI_PBUS_0__APB__PWRITE)
		,.MBOX_0__APB__PPROT   (MBOX_0__PERI_PBUS_0__APB__PPROT)
		,.MBOX_0__APB__PADDR   (MBOX_0__PERI_PBUS_0__APB__PADDR)
		,.MBOX_0__APB__PSTRB   (MBOX_0__PERI_PBUS_0__APB__PSTRB)
		,.MBOX_0__APB__PWDATA  (MBOX_0__PERI_PBUS_0__APB__PWDATA)
		,.MBOX_0__APB__PREADY  (MBOX_0__PERI_PBUS_0__APB__PREADY)
		,.MBOX_0__APB__PSLVERR (MBOX_0__PERI_PBUS_0__APB__PSLVERR)
		,.MBOX_0__APB__PRDATA  (MBOX_0__PERI_PBUS_0__APB__PRDATA)
		,.WDT_0__APB__PSEL     (WDT_0__PERI_PBUS_0__APB__PSEL)
		,.WDT_0__APB__PENABLE  (WDT_0__PERI_PBUS_0__APB__PENABLE)
		,.WDT_0__APB__PWRITE   (WDT_0__PERI_PBUS_0__APB__PWRITE)
		,.WDT_0__APB__PPROT    ()
		,.WDT_0__APB__PADDR    (WDT_0__PERI_PBUS_0__APB__PADDR)
		,.WDT_0__APB__PSTRB    ()
		,.WDT_0__APB__PWDATA   (WDT_0__PERI_PBUS_0__APB__PWDATA)
		,.WDT_0__APB__PREADY   (WDT_0__PERI_PBUS_0__APB__PREADY)
		,.WDT_0__APB__PSLVERR  (WDT_0__PERI_PBUS_0__APB__PSLVERR)
		,.WDT_0__APB__PRDATA   (WDT_0__PERI_PBUS_0__APB__PRDATA)
		,.OTP_0__APB__PSEL     (OTP_0__PERI_PBUS_0__PSEL)
		,.OTP_0__APB__PENABLE  (OTP_0__PERI_PBUS_0__PENABLE)
		,.OTP_0__APB__PWRITE   (OTP_0__PERI_PBUS_0__PWRITE)
		,.OTP_0__APB__PPROT    ()
		,.OTP_0__APB__PADDR    (OTP_0__PERI_PBUS_0__PADDR)
		,.OTP_0__APB__PSTRB    ()
		,.OTP_0__APB__PWDATA   (OTP_0__PERI_PBUS_0__PWDATA)
		,.OTP_0__APB__PREADY   (OTP_0__PERI_PBUS_0__PREADY)
		,.OTP_0__APB__PSLVERR  (OTP_0__PERI_PBUS_0__PSLVERR)
		,.OTP_0__APB__PRDATA   (OTP_0__PERI_PBUS_0__PRDATA)
		,.PVT_0__APB__PSEL     (PVT_0__PERI_PBUS_0__PSEL)
		,.PVT_0__APB__PENABLE  (PVT_0__PERI_PBUS_0__PENABLE)
		,.PVT_0__APB__PWRITE   (PVT_0__PERI_PBUS_0__PWRITE)
		,.PVT_0__APB__PPROT    ()
		,.PVT_0__APB__PADDR    (PVT_0__PERI_PBUS_0__PADDR)
		,.PVT_0__APB__PSTRB    (PVT_0__PERI_PBUS_0__PSTRB)
		,.PVT_0__APB__PWDATA   (PVT_0__PERI_PBUS_0__PWDATA)
		,.PVT_0__APB__PREADY   (PVT_0__PERI_PBUS_0__PREADY)
		,.PVT_0__APB__PSLVERR  (PVT_0__PERI_PBUS_0__PSLVERR)
		,.PVT_0__APB__PRDATA   (PVT_0__PERI_PBUS_0__PRDATA)
		,.TIMER_0__APB__PSEL   (TIMER_0__PERI_PBUS_0__APB__PSEL)
		,.TIMER_0__APB__PENABLE(TIMER_0__PERI_PBUS_0__APB__PENABLE)
		,.TIMER_0__APB__PWRITE (TIMER_0__PERI_PBUS_0__APB__PWRITE)
		,.TIMER_0__APB__PPROT  ()
		,.TIMER_0__APB__PADDR  (TIMER_0__PERI_PBUS_0__APB__PADDR)
		,.TIMER_0__APB__PSTRB  ()
		,.TIMER_0__APB__PWDATA (TIMER_0__PERI_PBUS_0__APB__PWDATA)
		,.TIMER_0__APB__PREADY (TIMER_0__PERI_PBUS_0__APB__PREADY)
		,.TIMER_0__APB__PSLVERR(TIMER_0__PERI_PBUS_0__APB__PSLVERR)
		,.TIMER_0__APB__PRDATA (TIMER_0__PERI_PBUS_0__APB__PRDATA)
		,.GPIO_0__APB__PSEL    (GPIO_0__PERI_PBUS_0__APB__PSEL)
		,.GPIO_0__APB__PENABLE (GPIO_0__PERI_PBUS_0__APB__PENABLE)
		,.GPIO_0__APB__PWRITE  (GPIO_0__PERI_PBUS_0__APB__PWRITE)
		,.GPIO_0__APB__PPROT   ()
		,.GPIO_0__APB__PADDR   (GPIO_0__PERI_PBUS_0__APB__PADDR)
		,.GPIO_0__APB__PSTRB   ()
		,.GPIO_0__APB__PWDATA  (GPIO_0__PERI_PBUS_0__APB__PWDATA)
		,.GPIO_0__APB__PREADY  (GPIO_0__PERI_PBUS_0__APB__PREADY)
		,.GPIO_0__APB__PSLVERR (GPIO_0__PERI_PBUS_0__APB__PSLVERR)
		,.GPIO_0__APB__PRDATA  (GPIO_0__PERI_PBUS_0__APB__PRDATA)
		,.UART_0__APB__PSEL    (UART_0__PERI_PBUS_0__APB__PSEL)
		,.UART_0__APB__PENABLE (UART_0__PERI_PBUS_0__APB__PENABLE)
		,.UART_0__APB__PWRITE  (UART_0__PERI_PBUS_0__APB__PWRITE)
		,.UART_0__APB__PPROT   ()
		,.UART_0__APB__PADDR   (UART_0__PERI_PBUS_0__APB__PADDR)
		,.UART_0__APB__PSTRB   ()
		,.UART_0__APB__PWDATA  (UART_0__PERI_PBUS_0__APB__PWDATA)
		,.UART_0__APB__PREADY  (UART_0__PERI_PBUS_0__APB__PREADY)
		,.UART_0__APB__PSLVERR (UART_0__PERI_PBUS_0__APB__PSLVERR)
		,.UART_0__APB__PRDATA  (UART_0__PERI_PBUS_0__APB__PRDATA)
		,.UART_1__APB__PSEL    (UART_1__PERI_PBUS_0__APB__PSEL)
		,.UART_1__APB__PENABLE (UART_1__PERI_PBUS_0__APB__PENABLE)
		,.UART_1__APB__PWRITE  (UART_1__PERI_PBUS_0__APB__PWRITE)
		,.UART_1__APB__PPROT   ()
		,.UART_1__APB__PADDR   (UART_1__PERI_PBUS_0__APB__PADDR)
		,.UART_1__APB__PSTRB   ()
		,.UART_1__APB__PWDATA  (UART_1__PERI_PBUS_0__APB__PWDATA)
		,.UART_1__APB__PREADY  (UART_1__PERI_PBUS_0__APB__PREADY)
		,.UART_1__APB__PSLVERR (UART_1__PERI_PBUS_0__APB__PSLVERR)
		,.UART_1__APB__PRDATA  (UART_1__PERI_PBUS_0__APB__PRDATA)
		,.I2C_0__APB__PSEL     (I2C_0__PERI_PBUS_0__APB__PSEL)
		,.I2C_0__APB__PENABLE  (I2C_0__PERI_PBUS_0__APB__PENABLE)
		,.I2C_0__APB__PWRITE   (I2C_0__PERI_PBUS_0__APB__PWRITE)
		,.I2C_0__APB__PPROT    ()
		,.I2C_0__APB__PADDR    (I2C_0__PERI_PBUS_0__APB__PADDR)
		,.I2C_0__APB__PSTRB    ()
		,.I2C_0__APB__PWDATA   (I2C_0__PERI_PBUS_0__APB__PWDATA)
		,.I2C_0__APB__PREADY   (I2C_0__PERI_PBUS_0__APB__PREADY)
		,.I2C_0__APB__PSLVERR  (I2C_0__PERI_PBUS_0__APB__PSLVERR)
		,.I2C_0__APB__PRDATA   (I2C_0__PERI_PBUS_0__APB__PRDATA)
		,.I2C_1__APB__PSEL     (I2C_1__PERI_PBUS_0__APB__PSEL)
		,.I2C_1__APB__PENABLE  (I2C_1__PERI_PBUS_0__APB__PENABLE)
		,.I2C_1__APB__PWRITE   (I2C_1__PERI_PBUS_0__APB__PWRITE)
		,.I2C_1__APB__PPROT    ()
		,.I2C_1__APB__PADDR    (I2C_1__PERI_PBUS_0__APB__PADDR)
		,.I2C_1__APB__PSTRB    ()
		,.I2C_1__APB__PWDATA   (I2C_1__PERI_PBUS_0__APB__PWDATA)
		,.I2C_1__APB__PREADY   (I2C_1__PERI_PBUS_0__APB__PREADY)
		,.I2C_1__APB__PSLVERR  (I2C_1__PERI_PBUS_0__APB__PSLVERR)
		,.I2C_1__APB__PRDATA   (I2C_1__PERI_PBUS_0__APB__PRDATA)
		,.I2C_2__APB__PSEL     (I2C_2__PERI_PBUS_0__APB__PSEL)
		,.I2C_2__APB__PENABLE  (I2C_2__PERI_PBUS_0__APB__PENABLE)
		,.I2C_2__APB__PWRITE   (I2C_2__PERI_PBUS_0__APB__PWRITE)
		,.I2C_2__APB__PPROT    ()
		,.I2C_2__APB__PADDR    (I2C_2__PERI_PBUS_0__APB__PADDR)
		,.I2C_2__APB__PSTRB    ()
		,.I2C_2__APB__PWDATA   (I2C_2__PERI_PBUS_0__APB__PWDATA)
		,.I2C_2__APB__PREADY   (I2C_2__PERI_PBUS_0__APB__PREADY)
		,.I2C_2__APB__PSLVERR  (I2C_2__PERI_PBUS_0__APB__PSLVERR)
		,.I2C_2__APB__PRDATA   (I2C_2__PERI_PBUS_0__APB__PRDATA)
		,.I2C_3__APB__PSEL     (I2C_3__PERI_PBUS_0__APB__PSEL)
		,.I2C_3__APB__PENABLE  (I2C_3__PERI_PBUS_0__APB__PENABLE)
		,.I2C_3__APB__PWRITE   (I2C_3__PERI_PBUS_0__APB__PWRITE)
		,.I2C_3__APB__PPROT    ()
		,.I2C_3__APB__PADDR    (I2C_3__PERI_PBUS_0__APB__PADDR)
		,.I2C_3__APB__PSTRB    ()
		,.I2C_3__APB__PWDATA   (I2C_3__PERI_PBUS_0__APB__PWDATA)
		,.I2C_3__APB__PREADY   (I2C_3__PERI_PBUS_0__APB__PREADY)
		,.I2C_3__APB__PSLVERR  (I2C_3__PERI_PBUS_0__APB__PSLVERR)
		,.I2C_3__APB__PRDATA   (I2C_3__PERI_PBUS_0__APB__PRDATA)
		,.SPI_0__APB__PSEL     (SPI_0__PERI_PBUS_0__APB__PSEL)
		,.SPI_0__APB__PENABLE  (SPI_0__PERI_PBUS_0__APB__PENABLE)
		,.SPI_0__APB__PWRITE   (SPI_0__PERI_PBUS_0__APB__PWRITE)
		,.SPI_0__APB__PPROT    ()
		,.SPI_0__APB__PADDR    (SPI_0__PERI_PBUS_0__APB__PADDR)
		,.SPI_0__APB__PSTRB    ()
		,.SPI_0__APB__PWDATA   (SPI_0__PERI_PBUS_0__APB__PWDATA)
		,.SPI_0__APB__PREADY   (SPI_0__PERI_PBUS_0__APB__PREADY)
		,.SPI_0__APB__PSLVERR  (SPI_0__PERI_PBUS_0__APB__PSLVERR)
		,.SPI_0__APB__PRDATA   (SPI_0__PERI_PBUS_0__APB__PRDATA)
		,.SPI_1__APB__PSEL     (SPI_1__PERI_PBUS_0__APB__PSEL)
		,.SPI_1__APB__PENABLE  (SPI_1__PERI_PBUS_0__APB__PENABLE)
		,.SPI_1__APB__PWRITE   (SPI_1__PERI_PBUS_0__APB__PWRITE)
		,.SPI_1__APB__PPROT    ()
		,.SPI_1__APB__PADDR    (SPI_1__PERI_PBUS_0__APB__PADDR)
		,.SPI_1__APB__PSTRB    ()
		,.SPI_1__APB__PWDATA   (SPI_1__PERI_PBUS_0__APB__PWDATA)
		,.SPI_1__APB__PREADY   (SPI_1__PERI_PBUS_0__APB__PREADY)
		,.SPI_1__APB__PSLVERR  (SPI_1__PERI_PBUS_0__APB__PSLVERR)
		,.SPI_1__APB__PRDATA   (SPI_1__PERI_PBUS_0__APB__PRDATA)
		,.SPI_2__APB__PSEL     (SPI_2__PERI_PBUS_0__APB__PSEL)
		,.SPI_2__APB__PENABLE  (SPI_2__PERI_PBUS_0__APB__PENABLE)
		,.SPI_2__APB__PWRITE   (SPI_2__PERI_PBUS_0__APB__PWRITE)
		,.SPI_2__APB__PPROT    ()
		,.SPI_2__APB__PADDR    (SPI_2__PERI_PBUS_0__APB__PADDR)
		,.SPI_2__APB__PSTRB    ()
		,.SPI_2__APB__PWDATA   (SPI_2__PERI_PBUS_0__APB__PWDATA)
		,.SPI_2__APB__PREADY   (SPI_2__PERI_PBUS_0__APB__PREADY)
		,.SPI_2__APB__PSLVERR  (SPI_2__PERI_PBUS_0__APB__PSLVERR)
		,.SPI_2__APB__PRDATA   (SPI_2__PERI_PBUS_0__APB__PRDATA)
		,.SPI_3__APB__PSEL     (SPI_3__PERI_PBUS_0__APB__PSEL)
		,.SPI_3__APB__PENABLE  (SPI_3__PERI_PBUS_0__APB__PENABLE)
		,.SPI_3__APB__PWRITE   (SPI_3__PERI_PBUS_0__APB__PWRITE)
		,.SPI_3__APB__PPROT    ()
		,.SPI_3__APB__PADDR    (SPI_3__PERI_PBUS_0__APB__PADDR)
		,.SPI_3__APB__PSTRB    ()
		,.SPI_3__APB__PWDATA   (SPI_3__PERI_PBUS_0__APB__PWDATA)
		,.SPI_3__APB__PREADY   (SPI_3__PERI_PBUS_0__APB__PREADY)
		,.SPI_3__APB__PSLVERR  (SPI_3__PERI_PBUS_0__APB__PSLVERR)
		,.SPI_3__APB__PRDATA   (SPI_3__PERI_PBUS_0__APB__PRDATA)
	);

	wire [  7:  2] __opened__AXI2APB_0__axi_slv0_B_Id_msb_7_lsb_2;
	wire [  7:  2] __opened__AXI2APB_0__axi_slv0_R_Id_msb_7_lsb_2;
	axi2apb_bridge
	AXI2APB_0 (
		 .TM               (TEST_MODE)
		,.apb_mst0_PAddr   (AXI2APB_0__PERI_PBUS_0__apb_mst0_PAddr)
		,.apb_mst0_PEnable (AXI2APB_0__PERI_PBUS_0__apb_mst0_PEnable)
		,.apb_mst0_PProt   (AXI2APB_0__PERI_PBUS_0__apb_mst0_PProt)
		,.apb_mst0_PRData  (AXI2APB_0__PERI_PBUS_0__apb_mst0_PRData)
		,.apb_mst0_PReady  (AXI2APB_0__PERI_PBUS_0__apb_mst0_PReady)
		,.apb_mst0_PSel    (AXI2APB_0__PERI_PBUS_0__apb_mst0_PSel)
		,.apb_mst0_PSlvErr (AXI2APB_0__PERI_PBUS_0__apb_mst0_PSlvErr)
		,.apb_mst0_PStrb   (AXI2APB_0__PERI_PBUS_0__apb_mst0_PStrb)
		,.apb_mst0_PWData  (AXI2APB_0__PERI_PBUS_0__apb_mst0_PWData)
		,.apb_mst0_PWrite  (AXI2APB_0__PERI_PBUS_0__apb_mst0_PWrite)
		,.apb_mst1_PAddr   ()
		,.apb_mst1_PEnable ()
		,.apb_mst1_PProt   ()
		,.apb_mst1_PRData  (32'h0)
		,.apb_mst1_PReady  (1'h0)
		,.apb_mst1_PSel    ()
		,.apb_mst1_PSlvErr (1'h0)
		,.apb_mst1_PStrb   ()
		,.apb_mst1_PWData  ()
		,.apb_mst1_PWrite  ()
		,.apb_mst2_PAddr   ()
		,.apb_mst2_PEnable ()
		,.apb_mst2_PProt   ()
		,.apb_mst2_PRData  (32'h0)
		,.apb_mst2_PReady  (1'h0)
		,.apb_mst2_PSel    ()
		,.apb_mst2_PSlvErr (1'h0)
		,.apb_mst2_PStrb   ()
		,.apb_mst2_PWData  ()
		,.apb_mst2_PWrite  ()
		,.apb_mst3_PAddr   ()
		,.apb_mst3_PEnable ()
		,.apb_mst3_PProt   ()
		,.apb_mst3_PRData  (32'h0)
		,.apb_mst3_PReady  (1'h0)
		,.apb_mst3_PSel    ()
		,.apb_mst3_PSlvErr (1'h0)
		,.apb_mst3_PStrb   ()
		,.apb_mst3_PWData  ()
		,.apb_mst3_PWrite  ()
		,.apb_mst4_PAddr   ()
		,.apb_mst4_PEnable ()
		,.apb_mst4_PProt   ()
		,.apb_mst4_PRData  (32'h0)
		,.apb_mst4_PReady  (1'h0)
		,.apb_mst4_PSel    ()
		,.apb_mst4_PSlvErr (1'h0)
		,.apb_mst4_PStrb   ()
		,.apb_mst4_PWData  ()
		,.apb_mst4_PWrite  ()
		,.apb_mst5_PAddr   ()
		,.apb_mst5_PEnable ()
		,.apb_mst5_PProt   ()
		,.apb_mst5_PRData  (32'h0)
		,.apb_mst5_PReady  (1'h0)
		,.apb_mst5_PSel    ()
		,.apb_mst5_PSlvErr (1'h0)
		,.apb_mst5_PStrb   ()
		,.apb_mst5_PWData  ()
		,.apb_mst5_PWrite  ()
		,.apb_mst6_PAddr   ()
		,.apb_mst6_PEnable ()
		,.apb_mst6_PProt   ()
		,.apb_mst6_PRData  (32'h0)
		,.apb_mst6_PReady  (1'h0)
		,.apb_mst6_PSel    ()
		,.apb_mst6_PSlvErr (1'h0)
		,.apb_mst6_PStrb   ()
		,.apb_mst6_PWData  ()
		,.apb_mst6_PWrite  ()
		,.apb_mst7_PAddr   ()
		,.apb_mst7_PEnable ()
		,.apb_mst7_PProt   ()
		,.apb_mst7_PRData  (32'h0)
		,.apb_mst7_PReady  (1'h0)
		,.apb_mst7_PSel    ()
		,.apb_mst7_PSlvErr (1'h0)
		,.apb_mst7_PStrb   ()
		,.apb_mst7_PWData  ()
		,.apb_mst7_PWrite  ()
		,.arstn            (RSTN__PERI)
		,.axi_slv0_Ar_Addr (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Addr)
		,.axi_slv0_Ar_Burst(cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Burst)
		,.axi_slv0_Ar_Cache(cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Cache)
		,.axi_slv0_Ar_Id   ({
			6'h0
			,cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Id})
		,.axi_slv0_Ar_Len  ({
			2'h0
			,cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Len})
		,.axi_slv0_Ar_Lock (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Lock)
		,.axi_slv0_Ar_Prot (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Prot)
		,.axi_slv0_Ar_Ready(cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Ready)
		,.axi_slv0_Ar_Size (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Size)
		,.axi_slv0_Ar_Valid(cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Valid)
		,.axi_slv0_Aw_Addr (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Addr)
		,.axi_slv0_Aw_Burst(cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Burst)
		,.axi_slv0_Aw_Cache(cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Cache)
		,.axi_slv0_Aw_Id   ({
			6'h0
			,cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Id})
		,.axi_slv0_Aw_Len  ({
			2'h0
			,cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Len})
		,.axi_slv0_Aw_Lock (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Lock)
		,.axi_slv0_Aw_Prot (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Prot)
		,.axi_slv0_Aw_Ready(cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Ready)
		,.axi_slv0_Aw_Size (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Size)
		,.axi_slv0_Aw_Valid(cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Valid)
		,.axi_slv0_B_Id    ({
			__opened__AXI2APB_0__axi_slv0_B_Id_msb_7_lsb_2
			,cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Id})
		,.axi_slv0_B_Ready (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Ready)
		,.axi_slv0_B_Resp  (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Resp)
		,.axi_slv0_B_Valid (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Valid)
		,.axi_slv0_R_Data  (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Data)
		,.axi_slv0_R_Id    ({
			__opened__AXI2APB_0__axi_slv0_R_Id_msb_7_lsb_2
			,cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Id})
		,.axi_slv0_R_Last  (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Last)
		,.axi_slv0_R_Ready (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Ready)
		,.axi_slv0_R_Resp  (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Resp)
		,.axi_slv0_R_Valid (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Valid)
		,.axi_slv0_W_Data  (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Data)
		,.axi_slv0_W_Last  (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Last)
		,.axi_slv0_W_Ready (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Ready)
		,.axi_slv0_W_Strb  (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Strb)
		,.axi_slv0_W_Valid (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Valid)
		,.clk              (CLK__PERI)
	);

	semifive_apb_dummy
	SCU_0 (
		 .CLK__APB    (CLK__PERI)
		,.RSTN__APB   (RSTN__PERI)
		,.APB__PENABLE(SCU_0__PERI_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (SCU_0__PERI_PBUS_0__APB__PSEL)
		,.APB__PREADY (SCU_0__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(SCU_0__PERI_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (SCU_0__PERI_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (SCU_0__PERI_PBUS_0__APB__PPROT)
		,.APB__PADDR  (SCU_0__PERI_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (SCU_0__PERI_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (SCU_0__PERI_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (SCU_0__PERI_PBUS_0__APB__PRDATA)
	);

	semifive_apb_mailbox
	MBOX_0 (
		 .CLK__APB    (CLK__PERI)
		,.RSTN__APB   (RSTN__PERI)
		,.IRQ         (EastBUS__INTG_0__IRQ)
		,.APB__PENABLE(MBOX_0__PERI_PBUS_0__APB__PENABLE)
		,.APB__PSEL   (MBOX_0__PERI_PBUS_0__APB__PSEL)
		,.APB__PREADY (MBOX_0__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR(MBOX_0__PERI_PBUS_0__APB__PSLVERR)
		,.APB__PWRITE (MBOX_0__PERI_PBUS_0__APB__PWRITE)
		,.APB__PPROT  (MBOX_0__PERI_PBUS_0__APB__PPROT)
		,.APB__PADDR  (MBOX_0__PERI_PBUS_0__APB__PADDR)
		,.APB__PSTRB  (MBOX_0__PERI_PBUS_0__APB__PSTRB)
		,.APB__PWDATA (MBOX_0__PERI_PBUS_0__APB__PWDATA)
		,.APB__PRDATA (MBOX_0__PERI_PBUS_0__APB__PRDATA)
	);

	DW_apb_wdt_s5wrapper
	WDT_0 (
		 .IRQ           (EastBUS__WDT_0__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PENABLE  (WDT_0__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (WDT_0__PERI_PBUS_0__APB__PWRITE)
		,.APB__PWDATA   (WDT_0__PERI_PBUS_0__APB__PWDATA)
		,.APB__PADDR    (WDT_0__PERI_PBUS_0__APB__PADDR)
		,.APB__PSEL     (WDT_0__PERI_PBUS_0__APB__PSEL)
		,.APB__PREADY   (WDT_0__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (WDT_0__PERI_PBUS_0__APB__PSLVERR)
		,.APB__PRDATA   (WDT_0__PERI_PBUS_0__APB__PRDATA)
		,.DFT__SCAN_MODE(DFT__SCAN_MODE)
		,.RSTSRC__WDT   (EastBUS__RSTSRC__WDT)
	);

	OTP_TOP
	OTP_0 (
		 .PCLK   (CLK__PERI)
		,.PRESETn(RSTN__PERI)
		,.PSEL   (OTP_0__PERI_PBUS_0__PSEL)
		,.PENABLE(OTP_0__PERI_PBUS_0__PENABLE)
		,.PWRITE (OTP_0__PERI_PBUS_0__PWRITE)
		,.PADDR  (OTP_0__PERI_PBUS_0__PADDR)
		,.PWDATA (OTP_0__PERI_PBUS_0__PWDATA)
		,.PREADY (OTP_0__PERI_PBUS_0__PREADY)
		,.PRDATA (OTP_0__PERI_PBUS_0__PRDATA)
		,.PSLVERR(OTP_0__PERI_PBUS_0__PSLVERR)
	);

	PVT_TOP
	PVT_0 (
		 .PCLK              (CLK__PERI)
		,.PRESETn           (RSTN__PERI)
		,.PSEL              (PVT_0__PERI_PBUS_0__PSEL)
		,.PENABLE           (PVT_0__PERI_PBUS_0__PENABLE)
		,.PWRITE            (PVT_0__PERI_PBUS_0__PWRITE)
		,.PADDR             (PVT_0__PERI_PBUS_0__PADDR)
		,.PWDATA            (PVT_0__PERI_PBUS_0__PWDATA)
		,.PSTRB             (PVT_0__PERI_PBUS_0__PSTRB)
		,.PREADY            (PVT_0__PERI_PBUS_0__PREADY)
		,.PRDATA            (PVT_0__PERI_PBUS_0__PRDATA)
		,.PSLVERR           (PVT_0__PERI_PBUS_0__PSLVERR)
		,.VSENSE_TS_ANALOG  (PVT_0__VSENSE_TS_ANALOG)
		,.VOL_TS_ANALOG     (PVT_0__VOL_TS_ANALOG)
		,.VREFT_FLAG_TS     (PVT_0__VREFT_FLAG_TS)
		,.VBE_FLAG_TS       (PVT_0__VBE_FLAG_TS)
		,.IBIAS_TS_ANALOG   (PVT_0__IBIAS_TS_ANALOG)
		,.TEST_OUT_TS_ANALOG(PVT_0__TEST_OUT_TS_ANALOG)
	);

	DW_apb_timers_s5wrapper
	TIMER_0 (
		 .IRQ           (EastBUS__TIMER_0__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PENABLE  (TIMER_0__PERI_PBUS_0__APB__PENABLE)
		,.APB__PSEL     (TIMER_0__PERI_PBUS_0__APB__PSEL)
		,.APB__PWRITE   (TIMER_0__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (TIMER_0__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (TIMER_0__PERI_PBUS_0__APB__PWDATA)
		,.APB__PREADY   (TIMER_0__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (TIMER_0__PERI_PBUS_0__APB__PSLVERR)
		,.APB__PRDATA   (TIMER_0__PERI_PBUS_0__APB__PRDATA)
		,.DFT__SCAN_MODE(DFT__SCAN_MODE)
		,.CLK__TIMER1   (CLK__PERI)
		,.CLK__TIMER2   (CLK__PERI)
		,.CLK__TIMER3   (CLK__PERI)
		,.CLK__TIMER4   (CLK__PERI)
		,.RSTN__TIMER1  (RSTN__PERI)
		,.RSTN__TIMER2  (RSTN__PERI)
		,.RSTN__TIMER3  (RSTN__PERI)
		,.RSTN__TIMER4  (RSTN__PERI)
		,.IO__PWM1      (TIMER_0__IO__PWM1)
		,.IO__PWM2      (TIMER_0__IO__PWM2)
		,.IO__PWM3      (TIMER_0__IO__PWM3)
		,.IO__PWM4      (TIMER_0__IO__PWM4)
	);

	DW_apb_gpio_s5wrapper
	GPIO_0 (
		 .APB__PREADY   (GPIO_0__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (GPIO_0__PERI_PBUS_0__APB__PSLVERR)
		,.IRQ           (EastBUS__GPIO_0__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PENABLE  (GPIO_0__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (GPIO_0__PERI_PBUS_0__APB__PWRITE)
		,.APB__PWDATA   (GPIO_0__PERI_PBUS_0__APB__PWDATA)
		,.APB__PADDR    (GPIO_0__PERI_PBUS_0__APB__PADDR)
		,.APB__PSEL     (GPIO_0__PERI_PBUS_0__APB__PSEL)
		,.APB__PRDATA   (GPIO_0__PERI_PBUS_0__APB__PRDATA)
		,.DFT__SCAN_MODE(DFT__SCAN_MODE)
		,.CLK__INTR     (CLK__PERI)
		,.CLK__DB       (CLK__PERI)
		,.RSTN__DB      (RSTN__PERI)
		,.SCU__CLKREQ   (/* !!! UNCONNECTED !!! */)
		,.IO__A_A       (GPIO_0__IO__A_A)
		,.IO__B_A       (GPIO_0__IO__B_A)
		,.IO__C_A       (GPIO_0__IO__C_A)
		,.IO__D_A       (GPIO_0__IO__D_A)
		,.IO__A_Y       (GPIO_0__IO__A_Y)
		,.IO__B_Y       (GPIO_0__IO__B_Y)
		,.IO__C_Y       (GPIO_0__IO__C_Y)
		,.IO__D_Y       (GPIO_0__IO__D_Y)
		,.IO__A_EN      (GPIO_0__IO__A_EN)
		,.IO__B_EN      (GPIO_0__IO__B_EN)
		,.IO__C_EN      (GPIO_0__IO__C_EN)
		,.IO__D_EN      (GPIO_0__IO__D_EN)
	);

	DW_apb_uart_s5wrapper
	UART_0 (
		 .IRQ             (EastBUS__UART_0__IRQ)
		,.CLK__APB        (CLK__PERI)
		,.RSTN__APB       (RSTN__PERI)
		,.APB__PENABLE    (UART_0__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE     (UART_0__PERI_PBUS_0__APB__PWRITE)
		,.APB__PWDATA     (UART_0__PERI_PBUS_0__APB__PWDATA)
		,.APB__PADDR      (UART_0__PERI_PBUS_0__APB__PADDR)
		,.APB__PSEL       (UART_0__PERI_PBUS_0__APB__PSEL)
		,.APB__PRDATA     (UART_0__PERI_PBUS_0__APB__PRDATA)
		,.APB__PREADY     (UART_0__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR    (UART_0__PERI_PBUS_0__APB__PSLVERR)
		,.DMA__TX_ACK_N   (EastBUS__UART_0__DMA__TX_ACK_N)
		,.DMA__RX_ACK_N   (EastBUS__UART_0__DMA__RX_ACK_N)
		,.DMA__TX_REQ_N   (EastBUS__UART_0__DMA__TX_REQ_N)
		,.DMA__TX_SINGLE_N(EastBUS__UART_0__DMA__TX_SINGLE_N)
		,.DMA__RX_REQ_N   (EastBUS__UART_0__DMA__RX_REQ_N)
		,.DMA__RX_SINGLE_N(EastBUS__UART_0__DMA__RX_SINGLE_N)
		,.DFT__SCAN_MODE  (DFT__SCAN_MODE)
		,.IO__RX          (UART_0__IO__RX)
		,.IO__TX          (UART_0__IO__TX)
	);

	DW_apb_uart_s5wrapper
	UART_1 (
		 .IRQ             (EastBUS__UART_1__IRQ)
		,.CLK__APB        (CLK__PERI)
		,.RSTN__APB       (RSTN__PERI)
		,.APB__PENABLE    (UART_1__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE     (UART_1__PERI_PBUS_0__APB__PWRITE)
		,.APB__PWDATA     (UART_1__PERI_PBUS_0__APB__PWDATA)
		,.APB__PADDR      (UART_1__PERI_PBUS_0__APB__PADDR)
		,.APB__PSEL       (UART_1__PERI_PBUS_0__APB__PSEL)
		,.APB__PRDATA     (UART_1__PERI_PBUS_0__APB__PRDATA)
		,.APB__PREADY     (UART_1__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR    (UART_1__PERI_PBUS_0__APB__PSLVERR)
		,.DMA__TX_ACK_N   (EastBUS__UART_1__DMA__TX_ACK_N)
		,.DMA__RX_ACK_N   (EastBUS__UART_1__DMA__RX_ACK_N)
		,.DMA__TX_REQ_N   (EastBUS__UART_1__DMA__TX_REQ_N)
		,.DMA__TX_SINGLE_N(EastBUS__UART_1__DMA__TX_SINGLE_N)
		,.DMA__RX_REQ_N   (EastBUS__UART_1__DMA__RX_REQ_N)
		,.DMA__RX_SINGLE_N(EastBUS__UART_1__DMA__RX_SINGLE_N)
		,.DFT__SCAN_MODE  (DFT__SCAN_MODE)
		,.IO__RX          (UART_1__IO__RX)
		,.IO__TX          (UART_1__IO__TX)
	);

	DW_apb_i2c_s5wrapper
	I2C_0 (
		 .IRQ           (EastBUS__I2C_0__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PSEL     (I2C_0__PERI_PBUS_0__APB__PSEL)
		,.APB__PENABLE  (I2C_0__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (I2C_0__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (I2C_0__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (I2C_0__PERI_PBUS_0__APB__PWDATA)
		,.APB__PRDATA   (I2C_0__PERI_PBUS_0__APB__PRDATA)
		,.APB__PREADY   (I2C_0__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (I2C_0__PERI_PBUS_0__APB__PSLVERR)
		,.DMA__TX_ACK   (EastBUS__I2C_0__DMA__TX_ACK)
		,.DMA__TX_REQ   (EastBUS__I2C_0__DMA__TX_REQ)
		,.DMA__TX_SINGLE(EastBUS__I2C_0__DMA__TX_SINGLE)
		,.DMA__RX_ACK   (EastBUS__I2C_0__DMA__RX_ACK)
		,.DMA__RX_REQ   (EastBUS__I2C_0__DMA__RX_REQ)
		,.DMA__RX_SINGLE(EastBUS__I2C_0__DMA__RX_SINGLE)
		,.CLK__I2C      (CLK__PERI)
		,.RSTN__I2C     (RSTN__PERI)
		,.IO__SCL       (I2C_0__IO__SCL)
		,.IO__SDA       (I2C_0__IO__SDA)
	);

	DW_apb_i2c_s5wrapper
	I2C_1 (
		 .IRQ           (EastBUS__I2C_1__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PSEL     (I2C_1__PERI_PBUS_0__APB__PSEL)
		,.APB__PENABLE  (I2C_1__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (I2C_1__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (I2C_1__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (I2C_1__PERI_PBUS_0__APB__PWDATA)
		,.APB__PRDATA   (I2C_1__PERI_PBUS_0__APB__PRDATA)
		,.APB__PREADY   (I2C_1__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (I2C_1__PERI_PBUS_0__APB__PSLVERR)
		,.DMA__TX_ACK   (EastBUS__I2C_1__DMA__TX_ACK)
		,.DMA__TX_REQ   (EastBUS__I2C_1__DMA__TX_REQ)
		,.DMA__TX_SINGLE(EastBUS__I2C_1__DMA__TX_SINGLE)
		,.DMA__RX_ACK   (EastBUS__I2C_1__DMA__RX_ACK)
		,.DMA__RX_REQ   (EastBUS__I2C_1__DMA__RX_REQ)
		,.DMA__RX_SINGLE(EastBUS__I2C_1__DMA__RX_SINGLE)
		,.CLK__I2C      (CLK__PERI)
		,.RSTN__I2C     (RSTN__PERI)
		,.IO__SCL       (I2C_1__IO__SCL)
		,.IO__SDA       (I2C_1__IO__SDA)
	);

	DW_apb_i2c_s5wrapper
	I2C_2 (
		 .IRQ           (EastBUS__I2C_2__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PSEL     (I2C_2__PERI_PBUS_0__APB__PSEL)
		,.APB__PENABLE  (I2C_2__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (I2C_2__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (I2C_2__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (I2C_2__PERI_PBUS_0__APB__PWDATA)
		,.APB__PRDATA   (I2C_2__PERI_PBUS_0__APB__PRDATA)
		,.APB__PREADY   (I2C_2__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (I2C_2__PERI_PBUS_0__APB__PSLVERR)
		,.DMA__TX_ACK   (EastBUS__I2C_2__DMA__TX_ACK)
		,.DMA__TX_REQ   (EastBUS__I2C_2__DMA__TX_REQ)
		,.DMA__TX_SINGLE(EastBUS__I2C_2__DMA__TX_SINGLE)
		,.DMA__RX_ACK   (EastBUS__I2C_2__DMA__RX_ACK)
		,.DMA__RX_REQ   (EastBUS__I2C_2__DMA__RX_REQ)
		,.DMA__RX_SINGLE(EastBUS__I2C_2__DMA__RX_SINGLE)
		,.CLK__I2C      (CLK__PERI)
		,.RSTN__I2C     (RSTN__PERI)
		,.IO__SCL       (I2C_2__IO__SCL)
		,.IO__SDA       (I2C_2__IO__SDA)
	);

	DW_apb_i2c_s5wrapper
	I2C_3 (
		 .IRQ           (EastBUS__I2C_3__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PSEL     (I2C_3__PERI_PBUS_0__APB__PSEL)
		,.APB__PENABLE  (I2C_3__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (I2C_3__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (I2C_3__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (I2C_3__PERI_PBUS_0__APB__PWDATA)
		,.APB__PRDATA   (I2C_3__PERI_PBUS_0__APB__PRDATA)
		,.APB__PREADY   (I2C_3__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (I2C_3__PERI_PBUS_0__APB__PSLVERR)
		,.DMA__TX_ACK   (EastBUS__I2C_3__DMA__TX_ACK)
		,.DMA__TX_REQ   (EastBUS__I2C_3__DMA__TX_REQ)
		,.DMA__TX_SINGLE(EastBUS__I2C_3__DMA__TX_SINGLE)
		,.DMA__RX_ACK   (EastBUS__I2C_3__DMA__RX_ACK)
		,.DMA__RX_REQ   (EastBUS__I2C_3__DMA__RX_REQ)
		,.DMA__RX_SINGLE(EastBUS__I2C_3__DMA__RX_SINGLE)
		,.CLK__I2C      (CLK__PERI)
		,.RSTN__I2C     (RSTN__PERI)
		,.IO__SCL       (I2C_3__IO__SCL)
		,.IO__SDA       (I2C_3__IO__SDA)
	);

	DW_apb_ssi_master_s5wrapper
	SPI_0 (
		 .IRQ           (EastBUS__SPI_0__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PSEL     (SPI_0__PERI_PBUS_0__APB__PSEL)
		,.APB__PENABLE  (SPI_0__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (SPI_0__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (SPI_0__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (SPI_0__PERI_PBUS_0__APB__PWDATA)
		,.APB__PREADY   (SPI_0__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (SPI_0__PERI_PBUS_0__APB__PSLVERR)
		,.APB__PRDATA   (SPI_0__PERI_PBUS_0__APB__PRDATA)
		,.DMA__TX_ACK   (EastBUS__SPI_0__DMA__TX_ACK)
		,.DMA__RX_ACK   (EastBUS__SPI_0__DMA__RX_ACK)
		,.DMA__TX_REQ   (EastBUS__SPI_0__DMA__TX_REQ)
		,.DMA__RX_REQ   (EastBUS__SPI_0__DMA__RX_REQ)
		,.DMA__TX_SINGLE(EastBUS__SPI_0__DMA__TX_SINGLE)
		,.DMA__RX_SINGLE(EastBUS__SPI_0__DMA__RX_SINGLE)
		,.IO__CLK       (SPI_0__IO__CLK)
		,.IO__CS        (SPI_0__IO__CS)
		,.IO__DAT_A     (SPI_0__IO__DAT_A)
		,.IO__DAT_Y     (SPI_0__IO__DAT_Y)
		,.IO__DAT_EN    (SPI_0__IO__DAT_EN)
	);

	DW_apb_ssi_master_s5wrapper
	SPI_1 (
		 .IRQ           (EastBUS__SPI_1__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PSEL     (SPI_1__PERI_PBUS_0__APB__PSEL)
		,.APB__PENABLE  (SPI_1__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (SPI_1__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (SPI_1__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (SPI_1__PERI_PBUS_0__APB__PWDATA)
		,.APB__PREADY   (SPI_1__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (SPI_1__PERI_PBUS_0__APB__PSLVERR)
		,.APB__PRDATA   (SPI_1__PERI_PBUS_0__APB__PRDATA)
		,.DMA__TX_ACK   (EastBUS__SPI_1__DMA__TX_ACK)
		,.DMA__RX_ACK   (EastBUS__SPI_1__DMA__RX_ACK)
		,.DMA__TX_REQ   (EastBUS__SPI_1__DMA__TX_REQ)
		,.DMA__RX_REQ   (EastBUS__SPI_1__DMA__RX_REQ)
		,.DMA__TX_SINGLE(EastBUS__SPI_1__DMA__TX_SINGLE)
		,.DMA__RX_SINGLE(EastBUS__SPI_1__DMA__RX_SINGLE)
		,.IO__CLK       (SPI_1__IO__CLK)
		,.IO__CS        (SPI_1__IO__CS)
		,.IO__DAT_A     (SPI_1__IO__DAT_A)
		,.IO__DAT_Y     (SPI_1__IO__DAT_Y)
		,.IO__DAT_EN    (SPI_1__IO__DAT_EN)
	);

	DW_apb_ssi_master_s5wrapper
	SPI_2 (
		 .IRQ           (EastBUS__SPI_2__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PSEL     (SPI_2__PERI_PBUS_0__APB__PSEL)
		,.APB__PENABLE  (SPI_2__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (SPI_2__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (SPI_2__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (SPI_2__PERI_PBUS_0__APB__PWDATA)
		,.APB__PREADY   (SPI_2__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (SPI_2__PERI_PBUS_0__APB__PSLVERR)
		,.APB__PRDATA   (SPI_2__PERI_PBUS_0__APB__PRDATA)
		,.DMA__TX_ACK   (EastBUS__SPI_2__DMA__TX_ACK)
		,.DMA__RX_ACK   (EastBUS__SPI_2__DMA__RX_ACK)
		,.DMA__TX_REQ   (EastBUS__SPI_2__DMA__TX_REQ)
		,.DMA__RX_REQ   (EastBUS__SPI_2__DMA__RX_REQ)
		,.DMA__TX_SINGLE(EastBUS__SPI_2__DMA__TX_SINGLE)
		,.DMA__RX_SINGLE(EastBUS__SPI_2__DMA__RX_SINGLE)
		,.IO__CLK       (SPI_2__IO__CLK)
		,.IO__CS        (SPI_2__IO__CS)
		,.IO__DAT_A     (SPI_2__IO__DAT_A)
		,.IO__DAT_Y     (SPI_2__IO__DAT_Y)
		,.IO__DAT_EN    (SPI_2__IO__DAT_EN)
	);

	DW_apb_ssi_slave_s5wrapper
	SPI_3 (
		 .IRQ           (EastBUS__SPI_3__IRQ)
		,.CLK__APB      (CLK__PERI)
		,.RSTN__APB     (RSTN__PERI)
		,.APB__PSEL     (SPI_3__PERI_PBUS_0__APB__PSEL)
		,.APB__PENABLE  (SPI_3__PERI_PBUS_0__APB__PENABLE)
		,.APB__PWRITE   (SPI_3__PERI_PBUS_0__APB__PWRITE)
		,.APB__PADDR    (SPI_3__PERI_PBUS_0__APB__PADDR)
		,.APB__PWDATA   (SPI_3__PERI_PBUS_0__APB__PWDATA)
		,.APB__PREADY   (SPI_3__PERI_PBUS_0__APB__PREADY)
		,.APB__PSLVERR  (SPI_3__PERI_PBUS_0__APB__PSLVERR)
		,.APB__PRDATA   (SPI_3__PERI_PBUS_0__APB__PRDATA)
		,.DMA__TX_ACK   (EastBUS__SPI_3__DMA__TX_ACK)
		,.DMA__RX_ACK   (EastBUS__SPI_3__DMA__RX_ACK)
		,.DMA__TX_REQ   (EastBUS__SPI_3__DMA__TX_REQ)
		,.DMA__RX_REQ   (EastBUS__SPI_3__DMA__RX_REQ)
		,.DMA__TX_SINGLE(EastBUS__SPI_3__DMA__TX_SINGLE)
		,.DMA__RX_SINGLE(EastBUS__SPI_3__DMA__RX_SINGLE)
		,.IO__CLK       (SPI_3__IO__CLK)
		,.IO__CS        (SPI_3__IO__CS)
		,.IO__DAT_A     (SPI_3__IO__DAT_A)
		,.IO__DAT_Y     (SPI_3__IO__DAT_Y)
		,.IO__DAT_EN    (SPI_3__IO__DAT_EN)
	);

	cbus_Structure_Module_peri
	cbus_Structure_Module_peri (
		 .TM                                                       (TEST_MODE)
		,.arstn_peri                                               (RSTN__PERI)
		,.clk_peri                                                 (CLK__PERI)
		,.dbg_master_Ar_Addr                                       (40'h0)
		,.dbg_master_Ar_Burst                                      (2'h0)
		,.dbg_master_Ar_Cache                                      (4'h0)
		,.dbg_master_Ar_Id                                         (3'h0)
		,.dbg_master_Ar_Len                                        (2'h0)
		,.dbg_master_Ar_Lock                                       (1'h0)
		,.dbg_master_Ar_Prot                                       (3'h0)
		,.dbg_master_Ar_Ready                                      ()
		,.dbg_master_Ar_Size                                       (3'h0)
		,.dbg_master_Ar_Valid                                      (1'h0)
		,.dbg_master_Aw_Addr                                       (40'h0)
		,.dbg_master_Aw_Burst                                      (2'h0)
		,.dbg_master_Aw_Cache                                      (4'h0)
		,.dbg_master_Aw_Id                                         (3'h0)
		,.dbg_master_Aw_Len                                        (2'h0)
		,.dbg_master_Aw_Lock                                       (1'h0)
		,.dbg_master_Aw_Prot                                       (3'h0)
		,.dbg_master_Aw_Ready                                      ()
		,.dbg_master_Aw_Size                                       (3'h0)
		,.dbg_master_Aw_Valid                                      (1'h0)
		,.dbg_master_B_Id                                          ()
		,.dbg_master_B_Ready                                       (1'h0)
		,.dbg_master_B_Resp                                        ()
		,.dbg_master_B_Valid                                       ()
		,.dbg_master_R_Data                                        ()
		,.dbg_master_R_Id                                          ()
		,.dbg_master_R_Last                                        ()
		,.dbg_master_R_Ready                                       (1'h0)
		,.dbg_master_R_Resp                                        ()
		,.dbg_master_R_Valid                                       ()
		,.dbg_master_W_Data                                        (32'h0)
		,.dbg_master_W_Last                                        (1'h0)
		,.dbg_master_W_Ready                                       ()
		,.dbg_master_W_Strb                                        (4'h0)
		,.dbg_master_W_Valid                                       (1'h0)
		,.dp_Link31_to_Switch_ctrl_tResp001_Data                   (EastBUS__dp_Link31_to_Switch_ctrl_tResp001_Data)
		,.dp_Link31_to_Switch_ctrl_tResp001_RdCnt                  (EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RdCnt)
		,.dp_Link31_to_Switch_ctrl_tResp001_RdPtr                  (EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RdPtr)
		,.dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst         (EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst)
		,.dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck      (EastBUS__dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck)
		,.dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst         (EastBUS__dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst)
		,.dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck      (EastBUS__dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck)
		,.dp_Link31_to_Switch_ctrl_tResp001_WrCnt                  (EastBUS__dp_Link31_to_Switch_ctrl_tResp001_WrCnt)
		,.dp_Link32_to_Switch_ctrl_tResp001_Data                   (EastBUS__dp_Link32_to_Switch_ctrl_tResp001_Data)
		,.dp_Link32_to_Switch_ctrl_tResp001_RdCnt                  (EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RdCnt)
		,.dp_Link32_to_Switch_ctrl_tResp001_RdPtr                  (EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RdPtr)
		,.dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst         (EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst)
		,.dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck      (EastBUS__dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck)
		,.dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst         (EastBUS__dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst)
		,.dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck      (EastBUS__dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck)
		,.dp_Link32_to_Switch_ctrl_tResp001_WrCnt                  (EastBUS__dp_Link32_to_Switch_ctrl_tResp001_WrCnt)
		,.dp_Link33_to_Switch_ctrl_tResp001_Data                   (EastBUS__dp_Link33_to_Switch_ctrl_tResp001_Data)
		,.dp_Link33_to_Switch_ctrl_tResp001_RdCnt                  (EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RdCnt)
		,.dp_Link33_to_Switch_ctrl_tResp001_RdPtr                  (EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RdPtr)
		,.dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst         (EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst)
		,.dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck      (EastBUS__dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck)
		,.dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst         (EastBUS__dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst)
		,.dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck      (EastBUS__dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck)
		,.dp_Link33_to_Switch_ctrl_tResp001_WrCnt                  (EastBUS__dp_Link33_to_Switch_ctrl_tResp001_WrCnt)
		,.dp_Link34_to_Switch_ctrl_tResp001_Data                   (EastBUS__dp_Link34_to_Switch_ctrl_tResp001_Data)
		,.dp_Link34_to_Switch_ctrl_tResp001_RdCnt                  (EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RdCnt)
		,.dp_Link34_to_Switch_ctrl_tResp001_RdPtr                  (EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RdPtr)
		,.dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst         (EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst)
		,.dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck      (EastBUS__dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck)
		,.dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst         (EastBUS__dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst)
		,.dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck      (EastBUS__dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck)
		,.dp_Link34_to_Switch_ctrl_tResp001_WrCnt                  (EastBUS__dp_Link34_to_Switch_ctrl_tResp001_WrCnt)
		,.dp_Switch_ctrl_iResp001_to_dbg_master_I_Data             (MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_Data)
		,.dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt            (MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt)
		,.dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr            (MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr)
		,.dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst   (MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst)
		,.dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck(MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst   (MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst)
		,.dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck(MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt            (MustBeConnected_To_EastBUS__dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt)
		,.dp_Switch_ctrl_t_to_Link27_Data                          (EastBUS__dp_Switch_ctrl_t_to_Link27_Data)
		,.dp_Switch_ctrl_t_to_Link27_RdCnt                         (EastBUS__dp_Switch_ctrl_t_to_Link27_RdCnt)
		,.dp_Switch_ctrl_t_to_Link27_RdPtr                         (EastBUS__dp_Switch_ctrl_t_to_Link27_RdPtr)
		,.dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst                (EastBUS__dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst)
		,.dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck             (EastBUS__dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst                (EastBUS__dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst)
		,.dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck             (EastBUS__dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_t_to_Link27_WrCnt                         (EastBUS__dp_Switch_ctrl_t_to_Link27_WrCnt)
		,.dp_Switch_ctrl_t_to_Link28_Data                          (EastBUS__dp_Switch_ctrl_t_to_Link28_Data)
		,.dp_Switch_ctrl_t_to_Link28_RdCnt                         (EastBUS__dp_Switch_ctrl_t_to_Link28_RdCnt)
		,.dp_Switch_ctrl_t_to_Link28_RdPtr                         (EastBUS__dp_Switch_ctrl_t_to_Link28_RdPtr)
		,.dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst                (EastBUS__dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst)
		,.dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck             (EastBUS__dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst                (EastBUS__dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst)
		,.dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck             (EastBUS__dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_t_to_Link28_WrCnt                         (EastBUS__dp_Switch_ctrl_t_to_Link28_WrCnt)
		,.dp_Switch_ctrl_t_to_Link29_Data                          (EastBUS__dp_Switch_ctrl_t_to_Link29_Data)
		,.dp_Switch_ctrl_t_to_Link29_RdCnt                         (EastBUS__dp_Switch_ctrl_t_to_Link29_RdCnt)
		,.dp_Switch_ctrl_t_to_Link29_RdPtr                         (EastBUS__dp_Switch_ctrl_t_to_Link29_RdPtr)
		,.dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst                (EastBUS__dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst)
		,.dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck             (EastBUS__dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst                (EastBUS__dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst)
		,.dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck             (EastBUS__dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_t_to_Link29_WrCnt                         (EastBUS__dp_Switch_ctrl_t_to_Link29_WrCnt)
		,.dp_Switch_ctrl_t_to_Link30_Data                          (EastBUS__dp_Switch_ctrl_t_to_Link30_Data)
		,.dp_Switch_ctrl_t_to_Link30_RdCnt                         (EastBUS__dp_Switch_ctrl_t_to_Link30_RdCnt)
		,.dp_Switch_ctrl_t_to_Link30_RdPtr                         (EastBUS__dp_Switch_ctrl_t_to_Link30_RdPtr)
		,.dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst                (EastBUS__dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst)
		,.dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck             (EastBUS__dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst                (EastBUS__dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst)
		,.dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck             (EastBUS__dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck)
		,.dp_Switch_ctrl_t_to_Link30_WrCnt                         (EastBUS__dp_Switch_ctrl_t_to_Link30_WrCnt)
		,.dp_dbg_master_I_to_Link1_Data                            (MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_Data)
		,.dp_dbg_master_I_to_Link1_RdCnt                           (MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RdCnt)
		,.dp_dbg_master_I_to_Link1_RdPtr                           (MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RdPtr)
		,.dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst                  (MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst)
		,.dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck               (MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck)
		,.dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst                  (MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst)
		,.dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck               (MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck)
		,.dp_dbg_master_I_to_Link1_WrCnt                           (MustBeConnected_To_EastBUS__dp_dbg_master_I_to_Link1_WrCnt)
		,.peri0_ctrl_Ar_Addr                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Addr)
		,.peri0_ctrl_Ar_Burst                                      (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Burst)
		,.peri0_ctrl_Ar_Cache                                      (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Cache)
		,.peri0_ctrl_Ar_Id                                         (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Id)
		,.peri0_ctrl_Ar_Len                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Len)
		,.peri0_ctrl_Ar_Lock                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Lock)
		,.peri0_ctrl_Ar_Prot                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Prot)
		,.peri0_ctrl_Ar_Ready                                      (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Ready)
		,.peri0_ctrl_Ar_Size                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Size)
		,.peri0_ctrl_Ar_Valid                                      (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Ar_Valid)
		,.peri0_ctrl_Aw_Addr                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Addr)
		,.peri0_ctrl_Aw_Burst                                      (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Burst)
		,.peri0_ctrl_Aw_Cache                                      (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Cache)
		,.peri0_ctrl_Aw_Id                                         (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Id)
		,.peri0_ctrl_Aw_Len                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Len)
		,.peri0_ctrl_Aw_Lock                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Lock)
		,.peri0_ctrl_Aw_Prot                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Prot)
		,.peri0_ctrl_Aw_Ready                                      (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Ready)
		,.peri0_ctrl_Aw_Size                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Size)
		,.peri0_ctrl_Aw_Valid                                      (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_Aw_Valid)
		,.peri0_ctrl_B_Id                                          (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Id)
		,.peri0_ctrl_B_Ready                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Ready)
		,.peri0_ctrl_B_Resp                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Resp)
		,.peri0_ctrl_B_Valid                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_B_Valid)
		,.peri0_ctrl_R_Data                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Data)
		,.peri0_ctrl_R_Id                                          (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Id)
		,.peri0_ctrl_R_Last                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Last)
		,.peri0_ctrl_R_Ready                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Ready)
		,.peri0_ctrl_R_Resp                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Resp)
		,.peri0_ctrl_R_Valid                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_R_Valid)
		,.peri0_ctrl_W_Data                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Data)
		,.peri0_ctrl_W_Last                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Last)
		,.peri0_ctrl_W_Ready                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Ready)
		,.peri0_ctrl_W_Strb                                        (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Strb)
		,.peri0_ctrl_W_Valid                                       (cbus_Structure_Module_peri__AXI2APB_0__peri0_ctrl_W_Valid)
		,.peri1_ctrl_Ar_Addr                                       ()
		,.peri1_ctrl_Ar_Burst                                      ()
		,.peri1_ctrl_Ar_Cache                                      ()
		,.peri1_ctrl_Ar_Id                                         ()
		,.peri1_ctrl_Ar_Len                                        ()
		,.peri1_ctrl_Ar_Lock                                       ()
		,.peri1_ctrl_Ar_Prot                                       ()
		,.peri1_ctrl_Ar_Ready                                      (1'h0)
		,.peri1_ctrl_Ar_Size                                       ()
		,.peri1_ctrl_Ar_Valid                                      ()
		,.peri1_ctrl_Aw_Addr                                       ()
		,.peri1_ctrl_Aw_Burst                                      ()
		,.peri1_ctrl_Aw_Cache                                      ()
		,.peri1_ctrl_Aw_Id                                         ()
		,.peri1_ctrl_Aw_Len                                        ()
		,.peri1_ctrl_Aw_Lock                                       ()
		,.peri1_ctrl_Aw_Prot                                       ()
		,.peri1_ctrl_Aw_Ready                                      (1'h0)
		,.peri1_ctrl_Aw_Size                                       ()
		,.peri1_ctrl_Aw_Valid                                      ()
		,.peri1_ctrl_B_Id                                          (2'h0)
		,.peri1_ctrl_B_Ready                                       ()
		,.peri1_ctrl_B_Resp                                        (2'h0)
		,.peri1_ctrl_B_Valid                                       (1'h0)
		,.peri1_ctrl_R_Data                                        (32'h0)
		,.peri1_ctrl_R_Id                                          (2'h0)
		,.peri1_ctrl_R_Last                                        (1'h0)
		,.peri1_ctrl_R_Ready                                       ()
		,.peri1_ctrl_R_Resp                                        (2'h0)
		,.peri1_ctrl_R_Valid                                       (1'h0)
		,.peri1_ctrl_W_Data                                        ()
		,.peri1_ctrl_W_Last                                        ()
		,.peri1_ctrl_W_Ready                                       (1'h0)
		,.peri1_ctrl_W_Strb                                        ()
		,.peri1_ctrl_W_Valid                                       ()
		,.sys0_ctrl_Ar_Addr                                        ()
		,.sys0_ctrl_Ar_Burst                                       ()
		,.sys0_ctrl_Ar_Cache                                       ()
		,.sys0_ctrl_Ar_Id                                          ()
		,.sys0_ctrl_Ar_Len                                         ()
		,.sys0_ctrl_Ar_Lock                                        ()
		,.sys0_ctrl_Ar_Prot                                        ()
		,.sys0_ctrl_Ar_Ready                                       (1'h0)
		,.sys0_ctrl_Ar_Size                                        ()
		,.sys0_ctrl_Ar_Valid                                       ()
		,.sys0_ctrl_Aw_Addr                                        ()
		,.sys0_ctrl_Aw_Burst                                       ()
		,.sys0_ctrl_Aw_Cache                                       ()
		,.sys0_ctrl_Aw_Id                                          ()
		,.sys0_ctrl_Aw_Len                                         ()
		,.sys0_ctrl_Aw_Lock                                        ()
		,.sys0_ctrl_Aw_Prot                                        ()
		,.sys0_ctrl_Aw_Ready                                       (1'h0)
		,.sys0_ctrl_Aw_Size                                        ()
		,.sys0_ctrl_Aw_Valid                                       ()
		,.sys0_ctrl_B_Id                                           (2'h0)
		,.sys0_ctrl_B_Ready                                        ()
		,.sys0_ctrl_B_Resp                                         (2'h0)
		,.sys0_ctrl_B_Valid                                        (1'h0)
		,.sys0_ctrl_R_Data                                         (32'h0)
		,.sys0_ctrl_R_Id                                           (2'h0)
		,.sys0_ctrl_R_Last                                         (1'h0)
		,.sys0_ctrl_R_Ready                                        ()
		,.sys0_ctrl_R_Resp                                         (2'h0)
		,.sys0_ctrl_R_Valid                                        (1'h0)
		,.sys0_ctrl_W_Data                                         ()
		,.sys0_ctrl_W_Last                                         ()
		,.sys0_ctrl_W_Ready                                        (1'h0)
		,.sys0_ctrl_W_Strb                                         ()
		,.sys0_ctrl_W_Valid                                        ()
		,.sys1_ctrl_Ar_Addr                                        ()
		,.sys1_ctrl_Ar_Burst                                       ()
		,.sys1_ctrl_Ar_Cache                                       ()
		,.sys1_ctrl_Ar_Id                                          ()
		,.sys1_ctrl_Ar_Len                                         ()
		,.sys1_ctrl_Ar_Lock                                        ()
		,.sys1_ctrl_Ar_Prot                                        ()
		,.sys1_ctrl_Ar_Ready                                       (1'h0)
		,.sys1_ctrl_Ar_Size                                        ()
		,.sys1_ctrl_Ar_Valid                                       ()
		,.sys1_ctrl_Aw_Addr                                        ()
		,.sys1_ctrl_Aw_Burst                                       ()
		,.sys1_ctrl_Aw_Cache                                       ()
		,.sys1_ctrl_Aw_Id                                          ()
		,.sys1_ctrl_Aw_Len                                         ()
		,.sys1_ctrl_Aw_Lock                                        ()
		,.sys1_ctrl_Aw_Prot                                        ()
		,.sys1_ctrl_Aw_Ready                                       (1'h0)
		,.sys1_ctrl_Aw_Size                                        ()
		,.sys1_ctrl_Aw_Valid                                       ()
		,.sys1_ctrl_B_Id                                           (2'h0)
		,.sys1_ctrl_B_Ready                                        ()
		,.sys1_ctrl_B_Resp                                         (2'h0)
		,.sys1_ctrl_B_Valid                                        (1'h0)
		,.sys1_ctrl_R_Data                                         (32'h0)
		,.sys1_ctrl_R_Id                                           (2'h0)
		,.sys1_ctrl_R_Last                                         (1'h0)
		,.sys1_ctrl_R_Ready                                        ()
		,.sys1_ctrl_R_Resp                                         (2'h0)
		,.sys1_ctrl_R_Valid                                        (1'h0)
		,.sys1_ctrl_W_Data                                         ()
		,.sys1_ctrl_W_Last                                         ()
		,.sys1_ctrl_W_Ready                                        (1'h0)
		,.sys1_ctrl_W_Strb                                         ()
		,.sys1_ctrl_W_Valid                                        ()
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_PERI_Subsystem
endmodule 
