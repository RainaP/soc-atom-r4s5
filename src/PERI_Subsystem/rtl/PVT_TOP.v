
module PVT_TOP (
  input         PCLK,
  input         PRESETn,

// APB I/F
  input         PSEL,
  input         PENABLE,
  input         PWRITE,
  input  [11:0] PADDR,
  input  [31:0] PWDATA,
  input  [3:0]  PSTRB,
  output        PREADY,
  output [31:0] PRDATA,
  output        PSLVERR,

// PVT SIGNAL
  input  [14:0] VSENSE_TS_ANALOG,   // From temp probe
  input  [14:0] VOL_TS_ANALOG,      // From voltage node
  output        VREFT_FLAG_TS,      // To chip top
  output        VBE_FLAG_TS,        // To chip top
  output [14:0] IBIAS_TS_ANALOG,    // To temp probe
  inout         TEST_OUT_TS_ANALOG  // To chip top
);

  assign PREADY          = 1;
  assign PRDATA          = 32'hDEADC0DE;
  assign PSLVERR         = 0;
  assign VREFT_FLAG_TS   = 0;
  assign VBE_FLAG_TS     = 0;
  assign IBIAS_TS_ANALOG = 0;

endmodule
