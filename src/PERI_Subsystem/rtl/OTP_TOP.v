
module OTP_TOP(
  input         PCLK,
  input         PRESETn,

// APB I/F
  input         PSEL,
  input         PENABLE,
  input         PWRITE,
  input  [15:0] PADDR,
  input  [31:0] PWDATA,
  output        PREADY,
  output [31:0] PRDATA,
  output        PSLVERR
);
  assign PREADY  = 1;
  assign PRDATA  = 32'hDEADC0DE;
  assign PSLVERR = 0;

endmodule
