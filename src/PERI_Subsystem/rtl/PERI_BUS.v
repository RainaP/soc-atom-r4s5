//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module PERI_BUS
#(
      parameter ABITS = 24
)
(
     input              CLK__APB
    ,input              RSTN__APB

    ,input              APB__PSEL
    ,input              APB__PENABLE
    ,input              APB__PWRITE
    ,input  [2:0]       APB__PPROT
    ,input  [ABITS-1:0] APB__PADDR
    ,input  [ 3:0]      APB__PSTRB
    ,input  [31:0]      APB__PWDATA
    ,output             APB__PREADY
    ,output             APB__PSLVERR
    ,output [31:0]      APB__PRDATA

    ,output             SCU_0__APB__PSEL
    ,output             SCU_0__APB__PENABLE
    ,output             SCU_0__APB__PWRITE
    ,output [2:0]       SCU_0__APB__PPROT
    ,output [12-1:0]    SCU_0__APB__PADDR
    ,output [ 3:0]      SCU_0__APB__PSTRB
    ,output [31:0]      SCU_0__APB__PWDATA
    ,input              SCU_0__APB__PREADY
    ,input              SCU_0__APB__PSLVERR
    ,input  [31:0]      SCU_0__APB__PRDATA

    ,output             MBOX_0__APB__PSEL
    ,output             MBOX_0__APB__PENABLE
    ,output             MBOX_0__APB__PWRITE
    ,output [2:0]       MBOX_0__APB__PPROT
    ,output [8-1:0]    MBOX_0__APB__PADDR
    ,output [ 3:0]      MBOX_0__APB__PSTRB
    ,output [31:0]      MBOX_0__APB__PWDATA
    ,input              MBOX_0__APB__PREADY
    ,input              MBOX_0__APB__PSLVERR
    ,input  [31:0]      MBOX_0__APB__PRDATA

    ,output             WDT_0__APB__PSEL
    ,output             WDT_0__APB__PENABLE
    ,output             WDT_0__APB__PWRITE
    ,output [2:0]       WDT_0__APB__PPROT
    ,output [8-1:0]    WDT_0__APB__PADDR
    ,output [ 3:0]      WDT_0__APB__PSTRB
    ,output [31:0]      WDT_0__APB__PWDATA
    ,input              WDT_0__APB__PREADY
    ,input              WDT_0__APB__PSLVERR
    ,input  [31:0]      WDT_0__APB__PRDATA

    ,output             OTP_0__APB__PSEL
    ,output             OTP_0__APB__PENABLE
    ,output             OTP_0__APB__PWRITE
    ,output [2:0]       OTP_0__APB__PPROT
    ,output [16-1:0]    OTP_0__APB__PADDR
    ,output [ 3:0]      OTP_0__APB__PSTRB
    ,output [31:0]      OTP_0__APB__PWDATA
    ,input              OTP_0__APB__PREADY
    ,input              OTP_0__APB__PSLVERR
    ,input  [31:0]      OTP_0__APB__PRDATA

    ,output             PVT_0__APB__PSEL
    ,output             PVT_0__APB__PENABLE
    ,output             PVT_0__APB__PWRITE
    ,output [2:0]       PVT_0__APB__PPROT
    ,output [12-1:0]    PVT_0__APB__PADDR
    ,output [ 3:0]      PVT_0__APB__PSTRB
    ,output [31:0]      PVT_0__APB__PWDATA
    ,input              PVT_0__APB__PREADY
    ,input              PVT_0__APB__PSLVERR
    ,input  [31:0]      PVT_0__APB__PRDATA

    ,output             TIMER_0__APB__PSEL
    ,output             TIMER_0__APB__PENABLE
    ,output             TIMER_0__APB__PWRITE
    ,output [2:0]       TIMER_0__APB__PPROT
    ,output [8-1:0]    TIMER_0__APB__PADDR
    ,output [ 3:0]      TIMER_0__APB__PSTRB
    ,output [31:0]      TIMER_0__APB__PWDATA
    ,input              TIMER_0__APB__PREADY
    ,input              TIMER_0__APB__PSLVERR
    ,input  [31:0]      TIMER_0__APB__PRDATA

    ,output             GPIO_0__APB__PSEL
    ,output             GPIO_0__APB__PENABLE
    ,output             GPIO_0__APB__PWRITE
    ,output [2:0]       GPIO_0__APB__PPROT
    ,output [7-1:0]    GPIO_0__APB__PADDR
    ,output [ 3:0]      GPIO_0__APB__PSTRB
    ,output [31:0]      GPIO_0__APB__PWDATA
    ,input              GPIO_0__APB__PREADY
    ,input              GPIO_0__APB__PSLVERR
    ,input  [31:0]      GPIO_0__APB__PRDATA

    ,output             UART_0__APB__PSEL
    ,output             UART_0__APB__PENABLE
    ,output             UART_0__APB__PWRITE
    ,output [2:0]       UART_0__APB__PPROT
    ,output [8-1:0]    UART_0__APB__PADDR
    ,output [ 3:0]      UART_0__APB__PSTRB
    ,output [31:0]      UART_0__APB__PWDATA
    ,input              UART_0__APB__PREADY
    ,input              UART_0__APB__PSLVERR
    ,input  [31:0]      UART_0__APB__PRDATA

    ,output             UART_1__APB__PSEL
    ,output             UART_1__APB__PENABLE
    ,output             UART_1__APB__PWRITE
    ,output [2:0]       UART_1__APB__PPROT
    ,output [8-1:0]    UART_1__APB__PADDR
    ,output [ 3:0]      UART_1__APB__PSTRB
    ,output [31:0]      UART_1__APB__PWDATA
    ,input              UART_1__APB__PREADY
    ,input              UART_1__APB__PSLVERR
    ,input  [31:0]      UART_1__APB__PRDATA

    ,output             I2C_0__APB__PSEL
    ,output             I2C_0__APB__PENABLE
    ,output             I2C_0__APB__PWRITE
    ,output [2:0]       I2C_0__APB__PPROT
    ,output [8-1:0]    I2C_0__APB__PADDR
    ,output [ 3:0]      I2C_0__APB__PSTRB
    ,output [31:0]      I2C_0__APB__PWDATA
    ,input              I2C_0__APB__PREADY
    ,input              I2C_0__APB__PSLVERR
    ,input  [31:0]      I2C_0__APB__PRDATA

    ,output             I2C_1__APB__PSEL
    ,output             I2C_1__APB__PENABLE
    ,output             I2C_1__APB__PWRITE
    ,output [2:0]       I2C_1__APB__PPROT
    ,output [8-1:0]    I2C_1__APB__PADDR
    ,output [ 3:0]      I2C_1__APB__PSTRB
    ,output [31:0]      I2C_1__APB__PWDATA
    ,input              I2C_1__APB__PREADY
    ,input              I2C_1__APB__PSLVERR
    ,input  [31:0]      I2C_1__APB__PRDATA

    ,output             I2C_2__APB__PSEL
    ,output             I2C_2__APB__PENABLE
    ,output             I2C_2__APB__PWRITE
    ,output [2:0]       I2C_2__APB__PPROT
    ,output [8-1:0]    I2C_2__APB__PADDR
    ,output [ 3:0]      I2C_2__APB__PSTRB
    ,output [31:0]      I2C_2__APB__PWDATA
    ,input              I2C_2__APB__PREADY
    ,input              I2C_2__APB__PSLVERR
    ,input  [31:0]      I2C_2__APB__PRDATA

    ,output             I2C_3__APB__PSEL
    ,output             I2C_3__APB__PENABLE
    ,output             I2C_3__APB__PWRITE
    ,output [2:0]       I2C_3__APB__PPROT
    ,output [8-1:0]    I2C_3__APB__PADDR
    ,output [ 3:0]      I2C_3__APB__PSTRB
    ,output [31:0]      I2C_3__APB__PWDATA
    ,input              I2C_3__APB__PREADY
    ,input              I2C_3__APB__PSLVERR
    ,input  [31:0]      I2C_3__APB__PRDATA

    ,output             SPI_0__APB__PSEL
    ,output             SPI_0__APB__PENABLE
    ,output             SPI_0__APB__PWRITE
    ,output [2:0]       SPI_0__APB__PPROT
    ,output [32-1:0]    SPI_0__APB__PADDR
    ,output [ 3:0]      SPI_0__APB__PSTRB
    ,output [31:0]      SPI_0__APB__PWDATA
    ,input              SPI_0__APB__PREADY
    ,input              SPI_0__APB__PSLVERR
    ,input  [31:0]      SPI_0__APB__PRDATA

    ,output             SPI_1__APB__PSEL
    ,output             SPI_1__APB__PENABLE
    ,output             SPI_1__APB__PWRITE
    ,output [2:0]       SPI_1__APB__PPROT
    ,output [32-1:0]    SPI_1__APB__PADDR
    ,output [ 3:0]      SPI_1__APB__PSTRB
    ,output [31:0]      SPI_1__APB__PWDATA
    ,input              SPI_1__APB__PREADY
    ,input              SPI_1__APB__PSLVERR
    ,input  [31:0]      SPI_1__APB__PRDATA

    ,output             SPI_2__APB__PSEL
    ,output             SPI_2__APB__PENABLE
    ,output             SPI_2__APB__PWRITE
    ,output [2:0]       SPI_2__APB__PPROT
    ,output [32-1:0]    SPI_2__APB__PADDR
    ,output [ 3:0]      SPI_2__APB__PSTRB
    ,output [31:0]      SPI_2__APB__PWDATA
    ,input              SPI_2__APB__PREADY
    ,input              SPI_2__APB__PSLVERR
    ,input  [31:0]      SPI_2__APB__PRDATA

    ,output             SPI_3__APB__PSEL
    ,output             SPI_3__APB__PENABLE
    ,output             SPI_3__APB__PWRITE
    ,output [2:0]       SPI_3__APB__PPROT
    ,output [32-1:0]    SPI_3__APB__PADDR
    ,output [ 3:0]      SPI_3__APB__PSTRB
    ,output [31:0]      SPI_3__APB__PWDATA
    ,input              SPI_3__APB__PREADY
    ,input              SPI_3__APB__PSLVERR
    ,input  [31:0]      SPI_3__APB__PRDATA

);
    localparam DECBIT = 12;
    localparam DECMASK= (1<<(16-12+1))-1;

    wire [17-1:0] psels;
    assign psels[  0] = ( ((('h0_0000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // SCU_0
    assign psels[  1] = ( ((('h0_1000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // MBOX_0
    assign psels[  2] = ( ((('h0_2000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // WDT_0
    assign psels[  3] = ( ((('h0_3000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // OTP_0
    assign psels[  4] = ( ((('h0_4000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // PVT_0
    assign psels[  5] = ( ((('h0_5000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // TIMER_0
    assign psels[  6] = ( ((('h0_6000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // GPIO_0
    assign psels[  7] = ( ((('h0_7000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // UART_0
    assign psels[  8] = ( ((('h0_8000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // UART_1
    assign psels[  9] = ( ((('h0_9000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // I2C_0
    assign psels[ 10] = ( ((('h0_A000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // I2C_1
    assign psels[ 11] = ( ((('h0_B000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // I2C_2
    assign psels[ 12] = ( ((('h0_C000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // I2C_3
    assign psels[ 13] = ( ((('h0_D000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // SPI_0
    assign psels[ 14] = ( ((('h0_E000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // SPI_1
    assign psels[ 15] = ( ((('h0_F000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // SPI_2
    assign psels[ 16] = ( ((('h1_0000)>>DECBIT)&DECMASK) == APB__PADDR[16:12]); // SPI_3

    assign SCU_0__APB__PSEL    = APB__PSEL && psels[  0];
    assign SCU_0__APB__PENABLE = APB__PENABLE;
    assign SCU_0__APB__PWRITE  = APB__PWRITE ;
    assign SCU_0__APB__PPROT   = APB__PPROT  ;
    assign SCU_0__APB__PADDR   = APB__PADDR  ;
    assign SCU_0__APB__PSTRB   = APB__PSTRB  ;
    assign SCU_0__APB__PWDATA  = APB__PWDATA ;

    assign MBOX_0__APB__PSEL    = APB__PSEL && psels[  1];
    assign MBOX_0__APB__PENABLE = APB__PENABLE;
    assign MBOX_0__APB__PWRITE  = APB__PWRITE ;
    assign MBOX_0__APB__PPROT   = APB__PPROT  ;
    assign MBOX_0__APB__PADDR   = APB__PADDR  ;
    assign MBOX_0__APB__PSTRB   = APB__PSTRB  ;
    assign MBOX_0__APB__PWDATA  = APB__PWDATA ;

    assign WDT_0__APB__PSEL    = APB__PSEL && psels[  2];
    assign WDT_0__APB__PENABLE = APB__PENABLE;
    assign WDT_0__APB__PWRITE  = APB__PWRITE ;
    assign WDT_0__APB__PPROT   = APB__PPROT  ;
    assign WDT_0__APB__PADDR   = APB__PADDR  ;
    assign WDT_0__APB__PSTRB   = APB__PSTRB  ;
    assign WDT_0__APB__PWDATA  = APB__PWDATA ;

    assign OTP_0__APB__PSEL    = APB__PSEL && psels[  3];
    assign OTP_0__APB__PENABLE = APB__PENABLE;
    assign OTP_0__APB__PWRITE  = APB__PWRITE ;
    assign OTP_0__APB__PPROT   = APB__PPROT  ;
    assign OTP_0__APB__PADDR   = APB__PADDR  ;
    assign OTP_0__APB__PSTRB   = APB__PSTRB  ;
    assign OTP_0__APB__PWDATA  = APB__PWDATA ;

    assign PVT_0__APB__PSEL    = APB__PSEL && psels[  4];
    assign PVT_0__APB__PENABLE = APB__PENABLE;
    assign PVT_0__APB__PWRITE  = APB__PWRITE ;
    assign PVT_0__APB__PPROT   = APB__PPROT  ;
    assign PVT_0__APB__PADDR   = APB__PADDR  ;
    assign PVT_0__APB__PSTRB   = APB__PSTRB  ;
    assign PVT_0__APB__PWDATA  = APB__PWDATA ;

    assign TIMER_0__APB__PSEL    = APB__PSEL && psels[  5];
    assign TIMER_0__APB__PENABLE = APB__PENABLE;
    assign TIMER_0__APB__PWRITE  = APB__PWRITE ;
    assign TIMER_0__APB__PPROT   = APB__PPROT  ;
    assign TIMER_0__APB__PADDR   = APB__PADDR  ;
    assign TIMER_0__APB__PSTRB   = APB__PSTRB  ;
    assign TIMER_0__APB__PWDATA  = APB__PWDATA ;

    assign GPIO_0__APB__PSEL    = APB__PSEL && psels[  6];
    assign GPIO_0__APB__PENABLE = APB__PENABLE;
    assign GPIO_0__APB__PWRITE  = APB__PWRITE ;
    assign GPIO_0__APB__PPROT   = APB__PPROT  ;
    assign GPIO_0__APB__PADDR   = APB__PADDR  ;
    assign GPIO_0__APB__PSTRB   = APB__PSTRB  ;
    assign GPIO_0__APB__PWDATA  = APB__PWDATA ;

    assign UART_0__APB__PSEL    = APB__PSEL && psels[  7];
    assign UART_0__APB__PENABLE = APB__PENABLE;
    assign UART_0__APB__PWRITE  = APB__PWRITE ;
    assign UART_0__APB__PPROT   = APB__PPROT  ;
    assign UART_0__APB__PADDR   = APB__PADDR  ;
    assign UART_0__APB__PSTRB   = APB__PSTRB  ;
    assign UART_0__APB__PWDATA  = APB__PWDATA ;

    assign UART_1__APB__PSEL    = APB__PSEL && psels[  8];
    assign UART_1__APB__PENABLE = APB__PENABLE;
    assign UART_1__APB__PWRITE  = APB__PWRITE ;
    assign UART_1__APB__PPROT   = APB__PPROT  ;
    assign UART_1__APB__PADDR   = APB__PADDR  ;
    assign UART_1__APB__PSTRB   = APB__PSTRB  ;
    assign UART_1__APB__PWDATA  = APB__PWDATA ;

    assign I2C_0__APB__PSEL    = APB__PSEL && psels[  9];
    assign I2C_0__APB__PENABLE = APB__PENABLE;
    assign I2C_0__APB__PWRITE  = APB__PWRITE ;
    assign I2C_0__APB__PPROT   = APB__PPROT  ;
    assign I2C_0__APB__PADDR   = APB__PADDR  ;
    assign I2C_0__APB__PSTRB   = APB__PSTRB  ;
    assign I2C_0__APB__PWDATA  = APB__PWDATA ;

    assign I2C_1__APB__PSEL    = APB__PSEL && psels[ 10];
    assign I2C_1__APB__PENABLE = APB__PENABLE;
    assign I2C_1__APB__PWRITE  = APB__PWRITE ;
    assign I2C_1__APB__PPROT   = APB__PPROT  ;
    assign I2C_1__APB__PADDR   = APB__PADDR  ;
    assign I2C_1__APB__PSTRB   = APB__PSTRB  ;
    assign I2C_1__APB__PWDATA  = APB__PWDATA ;

    assign I2C_2__APB__PSEL    = APB__PSEL && psels[ 11];
    assign I2C_2__APB__PENABLE = APB__PENABLE;
    assign I2C_2__APB__PWRITE  = APB__PWRITE ;
    assign I2C_2__APB__PPROT   = APB__PPROT  ;
    assign I2C_2__APB__PADDR   = APB__PADDR  ;
    assign I2C_2__APB__PSTRB   = APB__PSTRB  ;
    assign I2C_2__APB__PWDATA  = APB__PWDATA ;

    assign I2C_3__APB__PSEL    = APB__PSEL && psels[ 12];
    assign I2C_3__APB__PENABLE = APB__PENABLE;
    assign I2C_3__APB__PWRITE  = APB__PWRITE ;
    assign I2C_3__APB__PPROT   = APB__PPROT  ;
    assign I2C_3__APB__PADDR   = APB__PADDR  ;
    assign I2C_3__APB__PSTRB   = APB__PSTRB  ;
    assign I2C_3__APB__PWDATA  = APB__PWDATA ;

    assign SPI_0__APB__PSEL    = APB__PSEL && psels[ 13];
    assign SPI_0__APB__PENABLE = APB__PENABLE;
    assign SPI_0__APB__PWRITE  = APB__PWRITE ;
    assign SPI_0__APB__PPROT   = APB__PPROT  ;
    assign SPI_0__APB__PADDR   = APB__PADDR  ;
    assign SPI_0__APB__PSTRB   = APB__PSTRB  ;
    assign SPI_0__APB__PWDATA  = APB__PWDATA ;

    assign SPI_1__APB__PSEL    = APB__PSEL && psels[ 14];
    assign SPI_1__APB__PENABLE = APB__PENABLE;
    assign SPI_1__APB__PWRITE  = APB__PWRITE ;
    assign SPI_1__APB__PPROT   = APB__PPROT  ;
    assign SPI_1__APB__PADDR   = APB__PADDR  ;
    assign SPI_1__APB__PSTRB   = APB__PSTRB  ;
    assign SPI_1__APB__PWDATA  = APB__PWDATA ;

    assign SPI_2__APB__PSEL    = APB__PSEL && psels[ 15];
    assign SPI_2__APB__PENABLE = APB__PENABLE;
    assign SPI_2__APB__PWRITE  = APB__PWRITE ;
    assign SPI_2__APB__PPROT   = APB__PPROT  ;
    assign SPI_2__APB__PADDR   = APB__PADDR  ;
    assign SPI_2__APB__PSTRB   = APB__PSTRB  ;
    assign SPI_2__APB__PWDATA  = APB__PWDATA ;

    assign SPI_3__APB__PSEL    = APB__PSEL && psels[ 16];
    assign SPI_3__APB__PENABLE = APB__PENABLE;
    assign SPI_3__APB__PWRITE  = APB__PWRITE ;
    assign SPI_3__APB__PPROT   = APB__PPROT  ;
    assign SPI_3__APB__PADDR   = APB__PADDR  ;
    assign SPI_3__APB__PSTRB   = APB__PSTRB  ;
    assign SPI_3__APB__PWDATA  = APB__PWDATA ;

    assign APB__PREADY = (0==APB__PSEL) 
        | (psels[  0] ? SCU_0__APB__PREADY : 0)
        | (psels[  1] ? MBOX_0__APB__PREADY : 0)
        | (psels[  2] ? WDT_0__APB__PREADY : 0)
        | (psels[  3] ? OTP_0__APB__PREADY : 0)
        | (psels[  4] ? PVT_0__APB__PREADY : 0)
        | (psels[  5] ? TIMER_0__APB__PREADY : 0)
        | (psels[  6] ? GPIO_0__APB__PREADY : 0)
        | (psels[  7] ? UART_0__APB__PREADY : 0)
        | (psels[  8] ? UART_1__APB__PREADY : 0)
        | (psels[  9] ? I2C_0__APB__PREADY : 0)
        | (psels[ 10] ? I2C_1__APB__PREADY : 0)
        | (psels[ 11] ? I2C_2__APB__PREADY : 0)
        | (psels[ 12] ? I2C_3__APB__PREADY : 0)
        | (psels[ 13] ? SPI_0__APB__PREADY : 0)
        | (psels[ 14] ? SPI_1__APB__PREADY : 0)
        | (psels[ 15] ? SPI_2__APB__PREADY : 0)
        | (psels[ 16] ? SPI_3__APB__PREADY : 0)
        ;
    assign APB__PSLVERR = 0 
        | (psels[  0] ? SCU_0__APB__PSLVERR : 0)
        | (psels[  1] ? MBOX_0__APB__PSLVERR : 0)
        | (psels[  2] ? WDT_0__APB__PSLVERR : 0)
        | (psels[  3] ? OTP_0__APB__PSLVERR : 0)
        | (psels[  4] ? PVT_0__APB__PSLVERR : 0)
        | (psels[  5] ? TIMER_0__APB__PSLVERR : 0)
        | (psels[  6] ? GPIO_0__APB__PSLVERR : 0)
        | (psels[  7] ? UART_0__APB__PSLVERR : 0)
        | (psels[  8] ? UART_1__APB__PSLVERR : 0)
        | (psels[  9] ? I2C_0__APB__PSLVERR : 0)
        | (psels[ 10] ? I2C_1__APB__PSLVERR : 0)
        | (psels[ 11] ? I2C_2__APB__PSLVERR : 0)
        | (psels[ 12] ? I2C_3__APB__PSLVERR : 0)
        | (psels[ 13] ? SPI_0__APB__PSLVERR : 0)
        | (psels[ 14] ? SPI_1__APB__PSLVERR : 0)
        | (psels[ 15] ? SPI_2__APB__PSLVERR : 0)
        | (psels[ 16] ? SPI_3__APB__PSLVERR : 0)
        ;
    assign APB__PRDATA = 0 
        | (psels[  0] ? SCU_0__APB__PRDATA : 0)
        | (psels[  1] ? MBOX_0__APB__PRDATA : 0)
        | (psels[  2] ? WDT_0__APB__PRDATA : 0)
        | (psels[  3] ? OTP_0__APB__PRDATA : 0)
        | (psels[  4] ? PVT_0__APB__PRDATA : 0)
        | (psels[  5] ? TIMER_0__APB__PRDATA : 0)
        | (psels[  6] ? GPIO_0__APB__PRDATA : 0)
        | (psels[  7] ? UART_0__APB__PRDATA : 0)
        | (psels[  8] ? UART_1__APB__PRDATA : 0)
        | (psels[  9] ? I2C_0__APB__PRDATA : 0)
        | (psels[ 10] ? I2C_1__APB__PRDATA : 0)
        | (psels[ 11] ? I2C_2__APB__PRDATA : 0)
        | (psels[ 12] ? I2C_3__APB__PRDATA : 0)
        | (psels[ 13] ? SPI_0__APB__PRDATA : 0)
        | (psels[ 14] ? SPI_1__APB__PRDATA : 0)
        | (psels[ 15] ? SPI_2__APB__PRDATA : 0)
        | (psels[ 16] ? SPI_3__APB__PRDATA : 0)
        ;

`ifndef SYNTHESIS
    always@(posedge CLK__APB or negedge RSTN__APB )
        if( RSTN__APB && (0!=APB__PSEL) && (0==psels) ) begin
            $display( "*E:%s(%d): (%m)", `__FILE__, `__LINE__,$time );
            $display( "*E:Invalid address : %x", APB__PADDR );
            #100;
            $finish();
        end
`endif //SYNTHESIS

endmodule
