//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module DW_apb_i2c_s5wrapper (
	 output        IRQ
	,input         CLK__APB
	,input         RSTN__APB
	,input         APB__PSEL
	,input         APB__PENABLE
	,input         APB__PWRITE
	,input  [ 7:0] APB__PADDR
	,input  [31:0] APB__PWDATA
	,output [31:0] APB__PRDATA
	,output        APB__PREADY
	,output        APB__PSLVERR
	,input         DMA__TX_ACK
	,output        DMA__TX_REQ
	,output        DMA__TX_SINGLE
	,input         DMA__RX_ACK
	,output        DMA__RX_REQ
	,output        DMA__RX_SINGLE
	,input         CLK__I2C
	,input         RSTN__I2C
	,input         IO__SCL
	,input         IO__SDA
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_DW_apb_i2c_s5wrapper
	assign IRQ = 1'h0;
	assign APB__PRDATA = 32'h0;
	assign APB__PREADY = 1'h1;
	assign APB__PSLVERR = 1'h0;
	assign DMA__TX_REQ = 1'h0;
	assign DMA__TX_SINGLE = 1'h0;
	assign DMA__RX_REQ = 1'h0;
	assign DMA__RX_SINGLE = 1'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_DW_apb_i2c_s5wrapper
	DW_apb_i2c
	DW_apb_i2c (
		 .ic_intr          (IRQ)
		,.ic_current_src_en()
		,.pclk             (CLK__APB)
		,.presetn          (RSTN__APB)
		,.psel             (APB__PSEL)
		,.penable          (APB__PENABLE)
		,.pwrite           (APB__PWRITE)
		,.paddr            (APB__PADDR)
		,.pwdata           (APB__PWDATA)
		,.prdata           (APB__PRDATA)
		,.pready           (APB__PREADY)
		,.pslverr          (APB__PSLVERR)
		,.ic_clk           (CLK__I2C)
		,.ic_clk_in_a      (IO__SCL)
		,.ic_data_in_a     (IO__SDA)
		,.ic_rst_n         (RSTN__I2C)
		,.dma_tx_ack       (DMA__TX_ACK)
		,.dma_tx_req       (DMA__TX_REQ)
		,.dma_tx_single    (DMA__TX_SINGLE)
		,.dma_rx_ack       (DMA__RX_ACK)
		,.dma_rx_req       (DMA__RX_REQ)
		,.dma_rx_single    (DMA__RX_SINGLE)
		,.debug_s_gen      ()
		,.debug_p_gen      ()
		,.debug_data       ()
		,.debug_addr       ()
		,.debug_rd         ()
		,.debug_wr         ()
		,.debug_hs         ()
		,.debug_master_act ()
		,.debug_slave_act  ()
		,.debug_addr_10bit ()
		,.debug_mst_cstate ()
		,.debug_slv_cstate ()
		,.ic_clk_oe        ()
		,.ic_data_oe       ()
		,.ic_en            ()
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_DW_apb_i2c_s5wrapper
endmodule
