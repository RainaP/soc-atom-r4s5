//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module DW_apb_uart_s5wrapper (
	 output        IRQ
	,input         CLK__APB
	,input         RSTN__APB
	,input         APB__PENABLE
	,input         APB__PWRITE
	,input  [31:0] APB__PWDATA
	,input  [ 7:0] APB__PADDR
	,input         APB__PSEL
	,output [31:0] APB__PRDATA
	,output        APB__PREADY
	,output        APB__PSLVERR
	,input         DMA__TX_ACK_N
	,input         DMA__RX_ACK_N
	,output        DMA__TX_REQ_N
	,output        DMA__TX_SINGLE_N
	,output        DMA__RX_REQ_N
	,output        DMA__RX_SINGLE_N
	,input         DFT__SCAN_MODE
	,input         IO__RX
	,output        IO__TX
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_DW_apb_uart_s5wrapper
	assign IRQ = 1'h0;
	assign APB__PRDATA = 32'h0;
	assign APB__PREADY = 1'h1;
	assign APB__PSLVERR = 1'h0;
	assign DMA__TX_REQ_N = 1'h0;
	assign DMA__TX_SINGLE_N = 1'h0;
	assign DMA__RX_REQ_N = 1'h0;
	assign DMA__RX_SINGLE_N = 1'h0;
	assign IO__TX = 1'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_DW_apb_uart_s5wrapper
	DW_apb_uart
	DW_apb_uart (
		 .pclk            (CLK__APB)
		,.presetn         (RSTN__APB)
		,.penable         (APB__PENABLE)
		,.pwrite          (APB__PWRITE)
		,.pwdata          (APB__PWDATA)
		,.paddr           (APB__PADDR)
		,.psel            (APB__PSEL)
		,.scan_mode       (DFT__SCAN_MODE)
		,.cts_n           (1'h0)
		,.dsr_n           (1'h0)
		,.dcd_n           (1'h0)
		,.ri_n            (1'h0)
		,.dma_tx_ack_n    (DMA__TX_ACK_N)
		,.dma_rx_ack_n    (DMA__RX_ACK_N)
		,.sin             (IO__RX)
		,.prdata          (APB__PRDATA)
		,.pready          (APB__PREADY)
		,.pslverr         (APB__PSLVERR)
		,.dtr_n           ()
		,.rts_n           ()
		,.out2_n          ()
		,.out1_n          ()
		,.dma_tx_req_n    (DMA__TX_REQ_N)
		,.dma_tx_single_n (DMA__TX_SINGLE_N)
		,.dma_rx_req_n    (DMA__RX_REQ_N)
		,.dma_rx_single_n (DMA__RX_SINGLE_N)
		,.sout            (IO__TX)
		,.uart_lp_req_pclk()
		,.baudout_n       ()
		,.debug           ()
		,.re              ()
		,.de              ()
		,.rs485_en        ()
		,.intr            (IRQ)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_DW_apb_uart_s5wrapper
endmodule
