//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module DW_apb_gpio_s5wrapper (
	 output        APB__PREADY
	,output        APB__PSLVERR
	,output [15:0] IRQ
	,input         CLK__APB
	,input         RSTN__APB
	,input         APB__PENABLE
	,input         APB__PWRITE
	,input  [31:0] APB__PWDATA
	,input  [ 6:0] APB__PADDR
	,input         APB__PSEL
	,output [31:0] APB__PRDATA
	,input         DFT__SCAN_MODE
	,input         CLK__INTR
	,input         CLK__DB
	,input         RSTN__DB
	,output        SCU__CLKREQ
	,input  [15:0] IO__A_A
	,input  [15:0] IO__B_A
	,input  [15:0] IO__C_A
	,input  [15:0] IO__D_A
	,output [15:0] IO__A_Y
	,output [15:0] IO__B_Y
	,output [15:0] IO__C_Y
	,output [15:0] IO__D_Y
	,output [15:0] IO__A_EN
	,output [15:0] IO__B_EN
	,output [15:0] IO__C_EN
	,output [15:0] IO__D_EN
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_DW_apb_gpio_s5wrapper
	assign APB__PREADY = 1'h1;
	assign APB__PSLVERR = 1'h0;
	assign IRQ = 16'h0;
	assign APB__PRDATA = 32'h0;
	assign SCU__CLKREQ = 1'h0;
	assign IO__A_Y = 16'h0;
	assign IO__B_Y = 16'h0;
	assign IO__C_Y = 16'h0;
	assign IO__D_Y = 16'h0;
	assign IO__A_EN = 16'h0;
	assign IO__B_EN = 16'h0;
	assign IO__C_EN = 16'h0;
	assign IO__D_EN = 16'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_DW_apb_gpio_s5wrapper
	assign APB__PREADY = 1'h0;
	assign APB__PSLVERR = 1'h0;

	DW_apb_gpio
	DW_apb_gpio (
		 .pclk           (CLK__APB)
		,.pclk_intr      (CLK__INTR)
		,.presetn        (RSTN__APB)
		,.penable        (APB__PENABLE)
		,.pwrite         (APB__PWRITE)
		,.pwdata         (APB__PWDATA)
		,.paddr          (APB__PADDR)
		,.psel           (APB__PSEL)
		,.dbclk          (CLK__DB)
		,.dbclk_res_n    (RSTN__DB)
		,.scan_mode      (DFT__SCAN_MODE)
		,.aux_porta_out  (16'h0)
		,.aux_porta_en   (16'h0)
		,.aux_porta_in   ()
		,.gpio_ext_porta (IO__A_A)
		,.gpio_porta_dr  (IO__A_Y)
		,.gpio_porta_ddr (IO__A_EN)
		,.aux_portb_out  (16'h0)
		,.aux_portb_en   (16'h0)
		,.aux_portb_in   ()
		,.gpio_ext_portb (IO__B_A)
		,.gpio_portb_dr  (IO__B_Y)
		,.gpio_portb_ddr (IO__B_EN)
		,.aux_portc_out  (16'h0)
		,.aux_portc_en   (16'h0)
		,.aux_portc_in   ()
		,.gpio_ext_portc (IO__C_A)
		,.gpio_portc_dr  (IO__C_Y)
		,.gpio_portc_ddr (IO__C_EN)
		,.aux_portd_out  (16'h0)
		,.aux_portd_en   (16'h0)
		,.aux_portd_in   ()
		,.gpio_ext_portd (IO__D_A)
		,.gpio_portd_dr  (IO__D_Y)
		,.gpio_portd_ddr (IO__D_EN)
		,.gpio_intr_n    (IRQ)
		,.gpio_intrclk_en(SCU__CLKREQ)
		,.prdata         (APB__PRDATA)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_DW_apb_gpio_s5wrapper
endmodule
