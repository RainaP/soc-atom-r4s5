//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module DW_apb_ssi_slave_s5wrapper (
	 output        IRQ
	,input         CLK__APB
	,input         RSTN__APB
	,input         APB__PSEL
	,input         APB__PENABLE
	,input         APB__PWRITE
	,input  [31:0] APB__PADDR
	,input  [31:0] APB__PWDATA
	,output        APB__PREADY
	,output        APB__PSLVERR
	,output [31:0] APB__PRDATA
	,input         DMA__TX_ACK
	,input         DMA__RX_ACK
	,output        DMA__TX_REQ
	,output        DMA__RX_REQ
	,output        DMA__TX_SINGLE
	,output        DMA__RX_SINGLE
	,output        IO__CLK
	,output        IO__CS
	,input  [ 7:0] IO__DAT_A
	,output [ 7:0] IO__DAT_Y
	,output [ 7:0] IO__DAT_EN
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_DW_apb_ssi_slave_s5wrapper
	assign IRQ = 1'h0;
	assign APB__PREADY = 1'h1;
	assign APB__PSLVERR = 1'h0;
	assign APB__PRDATA = 32'h0;
	assign DMA__TX_REQ = 1'h0;
	assign DMA__RX_REQ = 1'h0;
	assign DMA__TX_SINGLE = 1'h0;
	assign DMA__RX_SINGLE = 1'h0;
	assign IO__CLK = 1'h0;
	assign IO__CS = 1'h0;
	assign IO__DAT_Y = 8'h0;
	assign IO__DAT_EN = 8'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_DW_apb_ssi_slave_s5wrapper
	DW_apb_ssi
	DW_apb_ssi (
		 .pclk         (CLK__APB)
		,.presetn      (RSTN__APB)
		,.psel         (APB__PSEL)
		,.penable      (APB__PENABLE)
		,.pwrite       (APB__PWRITE)
		,.paddr        (APB__PADDR)
		,.pwdata       (APB__PWDATA)
		,.xip_en       (1'h0)
		,.rxd          (IO__DAT_A)
		,.dma_tx_ack   (DMA__TX_ACK)
		,.dma_rx_ack   (DMA__RX_ACK)
		,.ss_in_n      (1'h0)
		,.ssi_clk      (1'h0)
		,.ssi_clk_en   (1'h0)
		,.ssi_rst_n    (1'h0)
		,.pready       (APB__PREADY)
		,.pslverr      (APB__PSLVERR)
		,.prdata       (APB__PRDATA)
		,.dma_tx_req   (DMA__TX_REQ)
		,.dma_rx_req   (DMA__RX_REQ)
		,.dma_tx_single(DMA__TX_SINGLE)
		,.dma_rx_single(DMA__RX_SINGLE)
		,.ssi_sleep    ()
		,.ssi_busy     ()
		,.txd          (IO__DAT_Y)
		,.sclk_out     (IO__CLK)
		,.ss_0_n       (IO__CS)
		,.ssi_oe_n     (IO__DAT_EN)
		,.spi_mode     ()
		,.ssi_intr_n   (IRQ)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_DW_apb_ssi_slave_s5wrapper
endmodule
