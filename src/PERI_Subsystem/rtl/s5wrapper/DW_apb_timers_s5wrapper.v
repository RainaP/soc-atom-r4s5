//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module DW_apb_timers_s5wrapper (
	 output [ 3:0] IRQ
	,input         CLK__APB
	,input         RSTN__APB
	,input         APB__PENABLE
	,input         APB__PSEL
	,input         APB__PWRITE
	,input  [ 7:0] APB__PADDR
	,input  [31:0] APB__PWDATA
	,output        APB__PREADY
	,output        APB__PSLVERR
	,output [31:0] APB__PRDATA
	,input         DFT__SCAN_MODE
	,input         CLK__TIMER1
	,input         CLK__TIMER2
	,input         CLK__TIMER3
	,input         CLK__TIMER4
	,input         RSTN__TIMER1
	,input         RSTN__TIMER2
	,input         RSTN__TIMER3
	,input         RSTN__TIMER4
	,output        IO__PWM1
	,output        IO__PWM2
	,output        IO__PWM3
	,output        IO__PWM4
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_DW_apb_timers_s5wrapper
	assign IRQ = 4'h0;
	assign APB__PREADY = 1'h1;
	assign APB__PSLVERR = 1'h0;
	assign APB__PRDATA = 32'h0;
	assign IO__PWM1 = 1'h0;
	assign IO__PWM2 = 1'h0;
	assign IO__PWM3 = 1'h0;
	assign IO__PWM4 = 1'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_DW_apb_timers_s5wrapper
	DW_apb_timers
	DW_apb_timers (
		 .pclk          (CLK__APB)
		,.presetn       (RSTN__APB)
		,.penable       (APB__PENABLE)
		,.psel          (APB__PSEL)
		,.pwrite        (APB__PWRITE)
		,.paddr         (APB__PADDR)
		,.pwdata        (APB__PWDATA)
		,.scan_mode     (DFT__SCAN_MODE)
		,.timer_1_clk   (CLK__TIMER1)
		,.timer_2_clk   (CLK__TIMER2)
		,.timer_3_clk   (CLK__TIMER3)
		,.timer_4_clk   (CLK__TIMER4)
		,.timer_1_resetn(RSTN__TIMER1)
		,.timer_2_resetn(RSTN__TIMER2)
		,.timer_3_resetn(RSTN__TIMER3)
		,.timer_4_resetn(RSTN__TIMER4)
		,.timer_1_pause (1'h0)
		,.timer_2_pause (1'h0)
		,.timer_3_pause (1'h0)
		,.timer_4_pause (1'h0)
		,.timer_en      ()
		,.timer_intr    (IRQ)
		,.timer_1_toggle(IO__PWM1)
		,.timer_2_toggle(IO__PWM2)
		,.timer_3_toggle(IO__PWM3)
		,.timer_4_toggle(IO__PWM4)
		,.pready        (APB__PREADY)
		,.pslverr       (APB__PSLVERR)
		,.prdata        (APB__PRDATA)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_DW_apb_timers_s5wrapper
endmodule
