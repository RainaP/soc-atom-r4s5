//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module DW_apb_wdt_s5wrapper (
	 output        IRQ
	,input         CLK__APB
	,input         RSTN__APB
	,input         APB__PENABLE
	,input         APB__PWRITE
	,input  [31:0] APB__PWDATA
	,input  [ 7:0] APB__PADDR
	,input         APB__PSEL
	,output        APB__PREADY
	,output        APB__PSLVERR
	,output [31:0] APB__PRDATA
	,input         DFT__SCAN_MODE
	,output        RSTSRC__WDT
);
`ifdef SEMIFIVE_MAKE_BLACKBOX_DW_apb_wdt_s5wrapper
	assign IRQ = 1'h0;
	assign APB__PREADY = 1'h1;
	assign APB__PSLVERR = 1'h0;
	assign APB__PRDATA = 32'h0;
	assign RSTSRC__WDT = 1'h0;
`else // SEMIFIVE_MAKE_BLACKBOX_DW_apb_wdt_s5wrapper
	DW_apb_wdt
	WDT_0 (
		 .pclk       (CLK__APB)
		,.presetn    (RSTN__APB)
		,.penable    (APB__PENABLE)
		,.pwrite     (APB__PWRITE)
		,.pwdata     (APB__PWDATA)
		,.paddr      (APB__PADDR)
		,.psel       (APB__PSEL)
		,.pause      (1'h0)
		,.speed_up   (1'h0)
		,.scan_mode  (DFT__SCAN_MODE)
		,.wdt_intr   (IRQ)
		,.wdt_sys_rst(RSTSRC__WDT)
		,.pready     (APB__PREADY)
		,.pslverr    (APB__PSLVERR)
		,.prdata     (APB__PRDATA)
	);

`endif // SEMIFIVE_MAKE_BLACKBOX_DW_apb_wdt_s5wrapper
endmodule
