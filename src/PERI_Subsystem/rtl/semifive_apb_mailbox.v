// See LICENSE.SEMIFIVE for license details.

module semifive_apb_mailbox
(
	 input                   CLK__APB
	,input                   RSTN__APB

	,output                  IRQ

    ,input                   APB__PENABLE
    ,input                   APB__PSEL
    ,output                  APB__PREADY
    ,output                  APB__PSLVERR
    ,input                   APB__PWRITE
    ,input  [ 2:0]           APB__PPROT
    ,input  [ 7:0]           APB__PADDR
    ,input  [ 3:0]           APB__PSTRB
    ,input  [31:0]           APB__PWDATA
    ,output [31:0]           APB__PRDATA
);
    wire       APB_RE  = APB__PSEL && APB__PENABLE && (0==APB__PWRITE);
    wire       APB_WE  = APB__PSEL && APB__PENABLE && (1==APB__PWRITE);
    reg [31:0] rPRDATA  ;

    assign APB__PREADY  = 1;
    assign APB__PSLVERR = 0;
    assign APB__PRDATA = rPRDATA;

	//--------------------------------------------------------------------
	reg[31:0] PEND;
	reg[31:0] MESSAGE_0;
	reg[31:0] MESSAGE_1;
	reg[31:0] MESSAGE_2;
	reg[31:0] MESSAGE_3;
	reg[31:0] MESSAGE_4;
	reg[31:0] MESSAGE_5;
	reg[31:0] MESSAGE_6;
	reg[31:0] MESSAGE_7;
	reg[31:0] MESSAGE_8;
	reg[31:0] MESSAGE_9;
	reg[31:0] MESSAGE_A;
	reg[31:0] MESSAGE_B;
	reg[31:0] MESSAGE_C;
	reg[31:0] MESSAGE_D;
	reg[31:0] MESSAGE_E;
	reg[31:0] MESSAGE_F;

`define REG_IMP(name, address) \
	always @(posedge CLK__APB or negedge RSTN__APB) \
		if (~RSTN__APB)                                                       name[7+b*8:b*8] <= 0; \
		else if(APB_WE && ({APB__PADDR[7:2],2'b0}==address) && APB__PSTRB[b]) name[7+b*8:b*8] <= APB__PWDATA[7+b*8:b*8] ;

	genvar b;
	generate
    for( b=0; b<4; b=b+1 )
    begin : PSTRB
		always @(posedge CLK__APB or negedge RSTN__APB)
			if (~RSTN__APB)                                                    PEND[7+b*8:b*8] <= 0;
			else if(APB_WE && ({APB__PADDR[7:2],2'b0}=='h00) && APB__PSTRB[b]) PEND[7+b*8:b*8] <= PEND[7+b*8:b*8] |   APB__PWDATA[7+b*8:b*8] ;
			else if(APB_WE && ({APB__PADDR[7:2],2'b0}=='h04) && APB__PSTRB[b]) PEND[7+b*8:b*8] <= PEND[7+b*8:b*8] & (~APB__PWDATA[7+b*8:b*8]);

		`REG_IMP( MESSAGE_0, 'h40 )
		`REG_IMP( MESSAGE_1, 'h44 )
		`REG_IMP( MESSAGE_2, 'h48 )
		`REG_IMP( MESSAGE_3, 'h4C )
		`REG_IMP( MESSAGE_4, 'h50 )
		`REG_IMP( MESSAGE_5, 'h54 )
		`REG_IMP( MESSAGE_6, 'h58 )
		`REG_IMP( MESSAGE_7, 'h5C )
		`REG_IMP( MESSAGE_8, 'h60 )
		`REG_IMP( MESSAGE_9, 'h64 )
		`REG_IMP( MESSAGE_A, 'h68 )
		`REG_IMP( MESSAGE_B, 'h6C )
		`REG_IMP( MESSAGE_C, 'h70 )
		`REG_IMP( MESSAGE_D, 'h74 )
		`REG_IMP( MESSAGE_E, 'h78 )
		`REG_IMP( MESSAGE_F, 'h7C )
    end
    endgenerate

	//--------------------------------------------------------------------
	always @( * )
		case (APB__PADDR)
		'h00:    rPRDATA = PEND ;
		'h04:    rPRDATA = PEND ;

		'h40:    rPRDATA = MESSAGE_0 ;
		'h44:    rPRDATA = MESSAGE_1 ;
		'h48:    rPRDATA = MESSAGE_2 ;
		'h4C:    rPRDATA = MESSAGE_3 ;
		'h50:    rPRDATA = MESSAGE_4 ;
		'h54:    rPRDATA = MESSAGE_5 ;
		'h58:    rPRDATA = MESSAGE_6 ;
		'h5C:    rPRDATA = MESSAGE_7 ;
		'h60:    rPRDATA = MESSAGE_8 ;
		'h64:    rPRDATA = MESSAGE_9 ;
		'h68:    rPRDATA = MESSAGE_A ;
		'h6C:    rPRDATA = MESSAGE_B ;
		'h70:    rPRDATA = MESSAGE_C ;
		'h74:    rPRDATA = MESSAGE_D ;
		'h78:    rPRDATA = MESSAGE_E ;
		'h7C:    rPRDATA = MESSAGE_F ;
		default: rPRDATA = 'hDEAD_C0DE;
		endcase

	assign IRQ = (0!=PEND);
endmodule

