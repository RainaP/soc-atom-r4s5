
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:00

####################################################################

# FIFO module definition
cdc fifo -module cbus_Structure_Module_peri Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_*
cdc fifo -module cbus_Structure_Module_peri Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_*
cdc fifo -module cbus_Structure_Module_peri Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_*
cdc fifo -module cbus_Structure_Module_peri Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_*
cdc fifo -module cbus_Structure_Module_peri dbg_master_I_main.DtpTxClkAdapt_Link1_Async.RegData_*

