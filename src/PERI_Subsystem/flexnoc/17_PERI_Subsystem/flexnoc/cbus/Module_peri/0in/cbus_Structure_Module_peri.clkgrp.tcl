
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:00

####################################################################

netlist clock Regime_peri_Cm_main.root_Clk -module cbus_Structure_Module_peri -group clk_peri -period 5.000 -waveform {0.000 2.500}
netlist clock Regime_peri_Cm_main.root_Clk_ClkS -module cbus_Structure_Module_peri -group clk_peri -period 5.000 -waveform {0.000 2.500}
netlist clock clk_peri -module cbus_Structure_Module_peri -group clk_peri -period 5.000 -waveform {0.000 2.500}

