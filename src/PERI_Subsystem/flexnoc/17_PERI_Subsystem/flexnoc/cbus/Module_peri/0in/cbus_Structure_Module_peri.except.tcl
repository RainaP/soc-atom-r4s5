
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:00

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link27_main.DtpRxClkAdapt_Switch_ctrl_t_Async.RdCnt -graycode -module cbus_Structure_Module_peri
cdc signal Link28_main.DtpRxClkAdapt_Switch_ctrl_t_Async.RdCnt -graycode -module cbus_Structure_Module_peri
cdc signal Link29_main.DtpRxClkAdapt_Switch_ctrl_t_Async.RdCnt -graycode -module cbus_Structure_Module_peri
cdc signal Link30_main.DtpRxClkAdapt_Switch_ctrl_t_Async.RdCnt -graycode -module cbus_Structure_Module_peri
cdc signal Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.WrCnt -graycode -module cbus_Structure_Module_peri
cdc signal Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.WrCnt -graycode -module cbus_Structure_Module_peri
cdc signal Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.WrCnt -graycode -module cbus_Structure_Module_peri
cdc signal Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.WrCnt -graycode -module cbus_Structure_Module_peri
cdc signal dbg_master_I_main.DtpRxClkAdapt_Switch_ctrl_iResp001_Async.RdCnt -graycode -module cbus_Structure_Module_peri
cdc signal dbg_master_I_main.DtpTxClkAdapt_Link1_Async.WrCnt -graycode -module cbus_Structure_Module_peri

