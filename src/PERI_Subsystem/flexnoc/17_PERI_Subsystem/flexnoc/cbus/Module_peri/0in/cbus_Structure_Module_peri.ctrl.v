
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:00

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_peri_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_3
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_4
// 0in set_cdc_fifo -module cbus_Structure_Module_peri Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.RegData_5
// 0in set_cdc_fifo -module cbus_Structure_Module_peri dbg_master_I_main.DtpTxClkAdapt_Link1_Async.RegData_0
// 0in set_cdc_fifo -module cbus_Structure_Module_peri dbg_master_I_main.DtpTxClkAdapt_Link1_Async.RegData_1
// 0in set_cdc_fifo -module cbus_Structure_Module_peri dbg_master_I_main.DtpTxClkAdapt_Link1_Async.RegData_2
// 0in set_cdc_fifo -module cbus_Structure_Module_peri dbg_master_I_main.DtpTxClkAdapt_Link1_Async.RegData_3

endmodule
