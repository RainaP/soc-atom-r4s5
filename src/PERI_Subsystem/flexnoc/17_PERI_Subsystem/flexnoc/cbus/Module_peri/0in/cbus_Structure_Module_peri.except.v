
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:00

////////////////////////////////////////////////////////////////////

module cbus_Structure_Module_peri_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals Link27_main.DtpRxClkAdapt_Switch_ctrl_t_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals Link28_main.DtpRxClkAdapt_Switch_ctrl_t_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals Link29_main.DtpRxClkAdapt_Switch_ctrl_t_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals Link30_main.DtpRxClkAdapt_Switch_ctrl_t_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals Link31_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals Link32_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals Link33_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals Link34_main.DtpTxClkAdapt_Switch_ctrl_tResp001_Async.WrCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals dbg_master_I_main.DtpRxClkAdapt_Switch_ctrl_iResp001_Async.RdCnt
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme reconvergence -from_signals dbg_master_I_main.DtpTxClkAdapt_Link1_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme multi_sync_mux_select -from Link27_main.DtpRxClkAdapt_Switch_ctrl_t_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme multi_sync_mux_select -from Link28_main.DtpRxClkAdapt_Switch_ctrl_t_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme multi_sync_mux_select -from Link29_main.DtpRxClkAdapt_Switch_ctrl_t_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme multi_sync_mux_select -from Link30_main.DtpRxClkAdapt_Switch_ctrl_t_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module cbus_Structure_Module_peri -severity waived -scheme multi_sync_mux_select -from dbg_master_I_main.DtpRxClkAdapt_Switch_ctrl_iResp001_Async.uRegSync.instSynchronizerCell*

endmodule
