
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:00

####################################################################

arteris_clock -name "clk_peri" -clock_domain "clk_peri" -port "clk_peri" -period "${clk_peri_P}" -waveform "[expr ${clk_peri_P}*0.00] [expr ${clk_peri_P}*0.50]" -edge "R" -user_directive ""
