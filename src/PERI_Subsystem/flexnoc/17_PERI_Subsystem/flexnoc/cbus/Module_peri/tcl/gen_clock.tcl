
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:06:00

####################################################################

arteris_gen_clock -name "Regime_peri_Cm_root" -pin "Regime_peri_Cm_main/root_Clk Regime_peri_Cm_main/root_Clk_ClkS" -clock_domain "clk_peri" -spec_domain_clock "/Regime_peri/Cm/root" -divide_by "1" -source "clk_peri" -source_period "${clk_peri_P}" -source_waveform "[expr ${clk_peri_P}*0.00] [expr ${clk_peri_P}*0.50]" -user_directive "" -add "FALSE"
