//--------------------------------------------------------------------------------
//
//   Rebellions Inc. Confidential
//   All Rights Reserved -- Property of Rebellions Inc. 
//
//   Description : 
//
//   Author : jsyoon@rebellions.ai
//
//   date : 2021/08/07
//
//--------------------------------------------------------------------------------

module PERI_Subsystem(

	input         TM                                                        ,
	input         arstn_peri                                                ,
	input         clk_peri                                                  ,

	output [57:0] ctrl_dp_Link31_to_Switch_ctrl_tResp001_Data                    ,
	input  [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdCnt                   ,
	input  [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdPtr                   ,
	output        ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst          ,
	input         ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck       ,
	input         ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst          ,
	output        ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck       ,
	output [2:0]  ctrl_dp_Link31_to_Switch_ctrl_tResp001_WrCnt                   ,
	output [57:0] ctrl_dp_Link32_to_Switch_ctrl_tResp001_Data                    ,
	input  [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdCnt                   ,
	input  [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdPtr                   ,
	output        ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst          ,
	input         ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck       ,
	input         ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst          ,
	output        ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck       ,
	output [2:0]  ctrl_dp_Link32_to_Switch_ctrl_tResp001_WrCnt                   ,
	output [57:0] ctrl_dp_Link33_to_Switch_ctrl_tResp001_Data                    ,
	input  [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdCnt                   ,
	input  [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdPtr                   ,
	output        ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst          ,
	input         ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck       ,
	input         ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst          ,
	output        ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck       ,
	output [2:0]  ctrl_dp_Link33_to_Switch_ctrl_tResp001_WrCnt                   ,
	output [57:0] ctrl_dp_Link34_to_Switch_ctrl_tResp001_Data                    ,
	input  [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdCnt                   ,
	input  [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdPtr                   ,
	output        ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst          ,
	input         ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck       ,
	input         ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst          ,
	output        ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck       ,
	output [2:0]  ctrl_dp_Link34_to_Switch_ctrl_tResp001_WrCnt                   ,
	input  [57:0] ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_Data              ,
	output [2:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt             ,
	output [1:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr             ,
	input         ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst    ,
	output        ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck ,
	output        ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst    ,
	input         ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck ,
	input  [2:0]  ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt             ,
	input  [57:0] ctrl_dp_Switch_ctrl_t_to_Link27_Data                           ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_RdCnt                          ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_RdPtr                          ,
	input         ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst                 ,
	output        ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck              ,
	output        ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst                 ,
	input         ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck              ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link27_WrCnt                          ,
	input  [57:0] ctrl_dp_Switch_ctrl_t_to_Link28_Data                           ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_RdCnt                          ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_RdPtr                          ,
	input         ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst                 ,
	output        ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck              ,
	output        ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst                 ,
	input         ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck              ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link28_WrCnt                          ,
	input  [57:0] ctrl_dp_Switch_ctrl_t_to_Link29_Data                           ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_RdCnt                          ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_RdPtr                          ,
	input         ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst                 ,
	output        ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck              ,
	output        ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst                 ,
	input         ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck              ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link29_WrCnt                          ,
	input  [57:0] ctrl_dp_Switch_ctrl_t_to_Link30_Data                           ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_RdCnt                          ,
	output [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_RdPtr                          ,
	input         ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst                 ,
	output        ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck              ,
	output        ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst                 ,
	input         ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck              ,
	input  [2:0]  ctrl_dp_Switch_ctrl_t_to_Link30_WrCnt                          ,
	output [57:0] ctrl_dp_dbg_master_I_to_Link1_Data                             ,
	input  [2:0]  ctrl_dp_dbg_master_I_to_Link1_RdCnt                            ,
	input  [1:0]  ctrl_dp_dbg_master_I_to_Link1_RdPtr                            ,
	output        ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst                   ,
	input         ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck                ,
	input         ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst                   ,
	output        ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck                ,
	output [2:0]  ctrl_dp_dbg_master_I_to_Link1_WrCnt                            




);


	cbus_Structure_Module_peri u_cbus_Structure_Module_peri(
		.TM( TM )
	,	.arstn_peri( arstn_peri )
	,	.clk_peri( clk_peri )
	,	.dbg_master_Ar_Addr( dbg_master_Ar_Addr )
	,	.dbg_master_Ar_Burst( dbg_master_Ar_Burst )
	,	.dbg_master_Ar_Cache( dbg_master_Ar_Cache )
	,	.dbg_master_Ar_Id( dbg_master_Ar_Id )
	,	.dbg_master_Ar_Len( dbg_master_Ar_Len )
	,	.dbg_master_Ar_Lock( dbg_master_Ar_Lock )
	,	.dbg_master_Ar_Prot( dbg_master_Ar_Prot )
	,	.dbg_master_Ar_Ready( dbg_master_Ar_Ready )
	,	.dbg_master_Ar_Size( dbg_master_Ar_Size )
	,	.dbg_master_Ar_Valid( dbg_master_Ar_Valid )
	,	.dbg_master_Aw_Addr( dbg_master_Aw_Addr )
	,	.dbg_master_Aw_Burst( dbg_master_Aw_Burst )
	,	.dbg_master_Aw_Cache( dbg_master_Aw_Cache )
	,	.dbg_master_Aw_Id( dbg_master_Aw_Id )
	,	.dbg_master_Aw_Len( dbg_master_Aw_Len )
	,	.dbg_master_Aw_Lock( dbg_master_Aw_Lock )
	,	.dbg_master_Aw_Prot( dbg_master_Aw_Prot )
	,	.dbg_master_Aw_Ready( dbg_master_Aw_Ready )
	,	.dbg_master_Aw_Size( dbg_master_Aw_Size )
	,	.dbg_master_Aw_Valid( dbg_master_Aw_Valid )
	,	.dbg_master_B_Id( dbg_master_B_Id )
	,	.dbg_master_B_Ready( dbg_master_B_Ready )
	,	.dbg_master_B_Resp( dbg_master_B_Resp )
	,	.dbg_master_B_Valid( dbg_master_B_Valid )
	,	.dbg_master_R_Data( dbg_master_R_Data )
	,	.dbg_master_R_Id( dbg_master_R_Id )
	,	.dbg_master_R_Last( dbg_master_R_Last )
	,	.dbg_master_R_Ready( dbg_master_R_Ready )
	,	.dbg_master_R_Resp( dbg_master_R_Resp )
	,	.dbg_master_R_Valid( dbg_master_R_Valid )
	,	.dbg_master_W_Data( dbg_master_W_Data )
	,	.dbg_master_W_Last( dbg_master_W_Last )
	,	.dbg_master_W_Ready( dbg_master_W_Ready )
	,	.dbg_master_W_Strb( dbg_master_W_Strb )
	,	.dbg_master_W_Valid( dbg_master_W_Valid )
	,	.dp_Link31_to_Switch_ctrl_tResp001_Data( ctrl_dp_Link31_to_Switch_ctrl_tResp001_Data )
	,	.dp_Link31_to_Switch_ctrl_tResp001_RdCnt( ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdCnt )
	,	.dp_Link31_to_Switch_ctrl_tResp001_RdPtr( ctrl_dp_Link31_to_Switch_ctrl_tResp001_RdPtr )
	,	.dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst( ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst )
	,	.dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link31_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst( ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst )
	,	.dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link31_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link31_to_Switch_ctrl_tResp001_WrCnt( ctrl_dp_Link31_to_Switch_ctrl_tResp001_WrCnt )
	,	.dp_Link32_to_Switch_ctrl_tResp001_Data( ctrl_dp_Link32_to_Switch_ctrl_tResp001_Data )
	,	.dp_Link32_to_Switch_ctrl_tResp001_RdCnt( ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdCnt )
	,	.dp_Link32_to_Switch_ctrl_tResp001_RdPtr( ctrl_dp_Link32_to_Switch_ctrl_tResp001_RdPtr )
	,	.dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst( ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst )
	,	.dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link32_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst( ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst )
	,	.dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link32_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link32_to_Switch_ctrl_tResp001_WrCnt( ctrl_dp_Link32_to_Switch_ctrl_tResp001_WrCnt )
	,	.dp_Link33_to_Switch_ctrl_tResp001_Data( ctrl_dp_Link33_to_Switch_ctrl_tResp001_Data )
	,	.dp_Link33_to_Switch_ctrl_tResp001_RdCnt( ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdCnt )
	,	.dp_Link33_to_Switch_ctrl_tResp001_RdPtr( ctrl_dp_Link33_to_Switch_ctrl_tResp001_RdPtr )
	,	.dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst( ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst )
	,	.dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link33_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst( ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst )
	,	.dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link33_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link33_to_Switch_ctrl_tResp001_WrCnt( ctrl_dp_Link33_to_Switch_ctrl_tResp001_WrCnt )
	,	.dp_Link34_to_Switch_ctrl_tResp001_Data( ctrl_dp_Link34_to_Switch_ctrl_tResp001_Data )
	,	.dp_Link34_to_Switch_ctrl_tResp001_RdCnt( ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdCnt )
	,	.dp_Link34_to_Switch_ctrl_tResp001_RdPtr( ctrl_dp_Link34_to_Switch_ctrl_tResp001_RdPtr )
	,	.dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst( ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRst )
	,	.dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck( ctrl_dp_Link34_to_Switch_ctrl_tResp001_RxCtl_PwrOnRstAck )
	,	.dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst( ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRst )
	,	.dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck( ctrl_dp_Link34_to_Switch_ctrl_tResp001_TxCtl_PwrOnRstAck )
	,	.dp_Link34_to_Switch_ctrl_tResp001_WrCnt( ctrl_dp_Link34_to_Switch_ctrl_tResp001_WrCnt )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_Data( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_Data )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdCnt )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RdPtr )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt( ctrl_dp_Switch_ctrl_iResp001_to_dbg_master_I_WrCnt )
	,	.dp_Switch_ctrl_t_to_Link27_Data( ctrl_dp_Switch_ctrl_t_to_Link27_Data )
	,	.dp_Switch_ctrl_t_to_Link27_RdCnt( ctrl_dp_Switch_ctrl_t_to_Link27_RdCnt )
	,	.dp_Switch_ctrl_t_to_Link27_RdPtr( ctrl_dp_Switch_ctrl_t_to_Link27_RdPtr )
	,	.dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link27_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link27_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link27_WrCnt( ctrl_dp_Switch_ctrl_t_to_Link27_WrCnt )
	,	.dp_Switch_ctrl_t_to_Link28_Data( ctrl_dp_Switch_ctrl_t_to_Link28_Data )
	,	.dp_Switch_ctrl_t_to_Link28_RdCnt( ctrl_dp_Switch_ctrl_t_to_Link28_RdCnt )
	,	.dp_Switch_ctrl_t_to_Link28_RdPtr( ctrl_dp_Switch_ctrl_t_to_Link28_RdPtr )
	,	.dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link28_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link28_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link28_WrCnt( ctrl_dp_Switch_ctrl_t_to_Link28_WrCnt )
	,	.dp_Switch_ctrl_t_to_Link29_Data( ctrl_dp_Switch_ctrl_t_to_Link29_Data )
	,	.dp_Switch_ctrl_t_to_Link29_RdCnt( ctrl_dp_Switch_ctrl_t_to_Link29_RdCnt )
	,	.dp_Switch_ctrl_t_to_Link29_RdPtr( ctrl_dp_Switch_ctrl_t_to_Link29_RdPtr )
	,	.dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link29_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link29_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link29_WrCnt( ctrl_dp_Switch_ctrl_t_to_Link29_WrCnt )
	,	.dp_Switch_ctrl_t_to_Link30_Data( ctrl_dp_Switch_ctrl_t_to_Link30_Data )
	,	.dp_Switch_ctrl_t_to_Link30_RdCnt( ctrl_dp_Switch_ctrl_t_to_Link30_RdCnt )
	,	.dp_Switch_ctrl_t_to_Link30_RdPtr( ctrl_dp_Switch_ctrl_t_to_Link30_RdPtr )
	,	.dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link30_RxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst( ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRst )
	,	.dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck( ctrl_dp_Switch_ctrl_t_to_Link30_TxCtl_PwrOnRstAck )
	,	.dp_Switch_ctrl_t_to_Link30_WrCnt( ctrl_dp_Switch_ctrl_t_to_Link30_WrCnt )
	,	.dp_dbg_master_I_to_Link1_Data( ctrl_dp_dbg_master_I_to_Link1_Data )
	,	.dp_dbg_master_I_to_Link1_RdCnt( ctrl_dp_dbg_master_I_to_Link1_RdCnt )
	,	.dp_dbg_master_I_to_Link1_RdPtr( ctrl_dp_dbg_master_I_to_Link1_RdPtr )
	,	.dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst( ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRst )
	,	.dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck( ctrl_dp_dbg_master_I_to_Link1_RxCtl_PwrOnRstAck )
	,	.dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst( ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRst )
	,	.dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck( ctrl_dp_dbg_master_I_to_Link1_TxCtl_PwrOnRstAck )
	,	.dp_dbg_master_I_to_Link1_WrCnt( ctrl_dp_dbg_master_I_to_Link1_WrCnt )
	,	.peri0_ctrl_Ar_Addr( peri0_ctrl_Ar_Addr )
	,	.peri0_ctrl_Ar_Burst( peri0_ctrl_Ar_Burst )
	,	.peri0_ctrl_Ar_Cache( peri0_ctrl_Ar_Cache )
	,	.peri0_ctrl_Ar_Id( peri0_ctrl_Ar_Id )
	,	.peri0_ctrl_Ar_Len( peri0_ctrl_Ar_Len )
	,	.peri0_ctrl_Ar_Lock( peri0_ctrl_Ar_Lock )
	,	.peri0_ctrl_Ar_Prot( peri0_ctrl_Ar_Prot )
	,	.peri0_ctrl_Ar_Ready( peri0_ctrl_Ar_Ready )
	,	.peri0_ctrl_Ar_Size( peri0_ctrl_Ar_Size )
	,	.peri0_ctrl_Ar_Valid( peri0_ctrl_Ar_Valid )
	,	.peri0_ctrl_Aw_Addr( peri0_ctrl_Aw_Addr )
	,	.peri0_ctrl_Aw_Burst( peri0_ctrl_Aw_Burst )
	,	.peri0_ctrl_Aw_Cache( peri0_ctrl_Aw_Cache )
	,	.peri0_ctrl_Aw_Id( peri0_ctrl_Aw_Id )
	,	.peri0_ctrl_Aw_Len( peri0_ctrl_Aw_Len )
	,	.peri0_ctrl_Aw_Lock( peri0_ctrl_Aw_Lock )
	,	.peri0_ctrl_Aw_Prot( peri0_ctrl_Aw_Prot )
	,	.peri0_ctrl_Aw_Ready( peri0_ctrl_Aw_Ready )
	,	.peri0_ctrl_Aw_Size( peri0_ctrl_Aw_Size )
	,	.peri0_ctrl_Aw_Valid( peri0_ctrl_Aw_Valid )
	,	.peri0_ctrl_B_Id( peri0_ctrl_B_Id )
	,	.peri0_ctrl_B_Ready( peri0_ctrl_B_Ready )
	,	.peri0_ctrl_B_Resp( peri0_ctrl_B_Resp )
	,	.peri0_ctrl_B_Valid( peri0_ctrl_B_Valid )
	,	.peri0_ctrl_R_Data( peri0_ctrl_R_Data )
	,	.peri0_ctrl_R_Id( peri0_ctrl_R_Id )
	,	.peri0_ctrl_R_Last( peri0_ctrl_R_Last )
	,	.peri0_ctrl_R_Ready( peri0_ctrl_R_Ready )
	,	.peri0_ctrl_R_Resp( peri0_ctrl_R_Resp )
	,	.peri0_ctrl_R_Valid( peri0_ctrl_R_Valid )
	,	.peri0_ctrl_W_Data( peri0_ctrl_W_Data )
	,	.peri0_ctrl_W_Last( peri0_ctrl_W_Last )
	,	.peri0_ctrl_W_Ready( peri0_ctrl_W_Ready )
	,	.peri0_ctrl_W_Strb( peri0_ctrl_W_Strb )
	,	.peri0_ctrl_W_Valid( peri0_ctrl_W_Valid )
	,	.peri1_ctrl_Ar_Addr( peri1_ctrl_Ar_Addr )
	,	.peri1_ctrl_Ar_Burst( peri1_ctrl_Ar_Burst )
	,	.peri1_ctrl_Ar_Cache( peri1_ctrl_Ar_Cache )
	,	.peri1_ctrl_Ar_Id( peri1_ctrl_Ar_Id )
	,	.peri1_ctrl_Ar_Len( peri1_ctrl_Ar_Len )
	,	.peri1_ctrl_Ar_Lock( peri1_ctrl_Ar_Lock )
	,	.peri1_ctrl_Ar_Prot( peri1_ctrl_Ar_Prot )
	,	.peri1_ctrl_Ar_Ready( peri1_ctrl_Ar_Ready )
	,	.peri1_ctrl_Ar_Size( peri1_ctrl_Ar_Size )
	,	.peri1_ctrl_Ar_Valid( peri1_ctrl_Ar_Valid )
	,	.peri1_ctrl_Aw_Addr( peri1_ctrl_Aw_Addr )
	,	.peri1_ctrl_Aw_Burst( peri1_ctrl_Aw_Burst )
	,	.peri1_ctrl_Aw_Cache( peri1_ctrl_Aw_Cache )
	,	.peri1_ctrl_Aw_Id( peri1_ctrl_Aw_Id )
	,	.peri1_ctrl_Aw_Len( peri1_ctrl_Aw_Len )
	,	.peri1_ctrl_Aw_Lock( peri1_ctrl_Aw_Lock )
	,	.peri1_ctrl_Aw_Prot( peri1_ctrl_Aw_Prot )
	,	.peri1_ctrl_Aw_Ready( peri1_ctrl_Aw_Ready )
	,	.peri1_ctrl_Aw_Size( peri1_ctrl_Aw_Size )
	,	.peri1_ctrl_Aw_Valid( peri1_ctrl_Aw_Valid )
	,	.peri1_ctrl_B_Id( peri1_ctrl_B_Id )
	,	.peri1_ctrl_B_Ready( peri1_ctrl_B_Ready )
	,	.peri1_ctrl_B_Resp( peri1_ctrl_B_Resp )
	,	.peri1_ctrl_B_Valid( peri1_ctrl_B_Valid )
	,	.peri1_ctrl_R_Data( peri1_ctrl_R_Data )
	,	.peri1_ctrl_R_Id( peri1_ctrl_R_Id )
	,	.peri1_ctrl_R_Last( peri1_ctrl_R_Last )
	,	.peri1_ctrl_R_Ready( peri1_ctrl_R_Ready )
	,	.peri1_ctrl_R_Resp( peri1_ctrl_R_Resp )
	,	.peri1_ctrl_R_Valid( peri1_ctrl_R_Valid )
	,	.peri1_ctrl_W_Data( peri1_ctrl_W_Data )
	,	.peri1_ctrl_W_Last( peri1_ctrl_W_Last )
	,	.peri1_ctrl_W_Ready( peri1_ctrl_W_Ready )
	,	.peri1_ctrl_W_Strb( peri1_ctrl_W_Strb )
	,	.peri1_ctrl_W_Valid( peri1_ctrl_W_Valid )
	,	.sys0_ctrl_Ar_Addr( sys0_ctrl_Ar_Addr )
	,	.sys0_ctrl_Ar_Burst( sys0_ctrl_Ar_Burst )
	,	.sys0_ctrl_Ar_Cache( sys0_ctrl_Ar_Cache )
	,	.sys0_ctrl_Ar_Id( sys0_ctrl_Ar_Id )
	,	.sys0_ctrl_Ar_Len( sys0_ctrl_Ar_Len )
	,	.sys0_ctrl_Ar_Lock( sys0_ctrl_Ar_Lock )
	,	.sys0_ctrl_Ar_Prot( sys0_ctrl_Ar_Prot )
	,	.sys0_ctrl_Ar_Ready( sys0_ctrl_Ar_Ready )
	,	.sys0_ctrl_Ar_Size( sys0_ctrl_Ar_Size )
	,	.sys0_ctrl_Ar_Valid( sys0_ctrl_Ar_Valid )
	,	.sys0_ctrl_Aw_Addr( sys0_ctrl_Aw_Addr )
	,	.sys0_ctrl_Aw_Burst( sys0_ctrl_Aw_Burst )
	,	.sys0_ctrl_Aw_Cache( sys0_ctrl_Aw_Cache )
	,	.sys0_ctrl_Aw_Id( sys0_ctrl_Aw_Id )
	,	.sys0_ctrl_Aw_Len( sys0_ctrl_Aw_Len )
	,	.sys0_ctrl_Aw_Lock( sys0_ctrl_Aw_Lock )
	,	.sys0_ctrl_Aw_Prot( sys0_ctrl_Aw_Prot )
	,	.sys0_ctrl_Aw_Ready( sys0_ctrl_Aw_Ready )
	,	.sys0_ctrl_Aw_Size( sys0_ctrl_Aw_Size )
	,	.sys0_ctrl_Aw_Valid( sys0_ctrl_Aw_Valid )
	,	.sys0_ctrl_B_Id( sys0_ctrl_B_Id )
	,	.sys0_ctrl_B_Ready( sys0_ctrl_B_Ready )
	,	.sys0_ctrl_B_Resp( sys0_ctrl_B_Resp )
	,	.sys0_ctrl_B_Valid( sys0_ctrl_B_Valid )
	,	.sys0_ctrl_R_Data( sys0_ctrl_R_Data )
	,	.sys0_ctrl_R_Id( sys0_ctrl_R_Id )
	,	.sys0_ctrl_R_Last( sys0_ctrl_R_Last )
	,	.sys0_ctrl_R_Ready( sys0_ctrl_R_Ready )
	,	.sys0_ctrl_R_Resp( sys0_ctrl_R_Resp )
	,	.sys0_ctrl_R_Valid( sys0_ctrl_R_Valid )
	,	.sys0_ctrl_W_Data( sys0_ctrl_W_Data )
	,	.sys0_ctrl_W_Last( sys0_ctrl_W_Last )
	,	.sys0_ctrl_W_Ready( sys0_ctrl_W_Ready )
	,	.sys0_ctrl_W_Strb( sys0_ctrl_W_Strb )
	,	.sys0_ctrl_W_Valid( sys0_ctrl_W_Valid )
	,	.sys1_ctrl_Ar_Addr( sys1_ctrl_Ar_Addr )
	,	.sys1_ctrl_Ar_Burst( sys1_ctrl_Ar_Burst )
	,	.sys1_ctrl_Ar_Cache( sys1_ctrl_Ar_Cache )
	,	.sys1_ctrl_Ar_Id( sys1_ctrl_Ar_Id )
	,	.sys1_ctrl_Ar_Len( sys1_ctrl_Ar_Len )
	,	.sys1_ctrl_Ar_Lock( sys1_ctrl_Ar_Lock )
	,	.sys1_ctrl_Ar_Prot( sys1_ctrl_Ar_Prot )
	,	.sys1_ctrl_Ar_Ready( sys1_ctrl_Ar_Ready )
	,	.sys1_ctrl_Ar_Size( sys1_ctrl_Ar_Size )
	,	.sys1_ctrl_Ar_Valid( sys1_ctrl_Ar_Valid )
	,	.sys1_ctrl_Aw_Addr( sys1_ctrl_Aw_Addr )
	,	.sys1_ctrl_Aw_Burst( sys1_ctrl_Aw_Burst )
	,	.sys1_ctrl_Aw_Cache( sys1_ctrl_Aw_Cache )
	,	.sys1_ctrl_Aw_Id( sys1_ctrl_Aw_Id )
	,	.sys1_ctrl_Aw_Len( sys1_ctrl_Aw_Len )
	,	.sys1_ctrl_Aw_Lock( sys1_ctrl_Aw_Lock )
	,	.sys1_ctrl_Aw_Prot( sys1_ctrl_Aw_Prot )
	,	.sys1_ctrl_Aw_Ready( sys1_ctrl_Aw_Ready )
	,	.sys1_ctrl_Aw_Size( sys1_ctrl_Aw_Size )
	,	.sys1_ctrl_Aw_Valid( sys1_ctrl_Aw_Valid )
	,	.sys1_ctrl_B_Id( sys1_ctrl_B_Id )
	,	.sys1_ctrl_B_Ready( sys1_ctrl_B_Ready )
	,	.sys1_ctrl_B_Resp( sys1_ctrl_B_Resp )
	,	.sys1_ctrl_B_Valid( sys1_ctrl_B_Valid )
	,	.sys1_ctrl_R_Data( sys1_ctrl_R_Data )
	,	.sys1_ctrl_R_Id( sys1_ctrl_R_Id )
	,	.sys1_ctrl_R_Last( sys1_ctrl_R_Last )
	,	.sys1_ctrl_R_Ready( sys1_ctrl_R_Ready )
	,	.sys1_ctrl_R_Resp( sys1_ctrl_R_Resp )
	,	.sys1_ctrl_R_Valid( sys1_ctrl_R_Valid )
	,	.sys1_ctrl_W_Data( sys1_ctrl_W_Data )
	,	.sys1_ctrl_W_Last( sys1_ctrl_W_Last )
	,	.sys1_ctrl_W_Ready( sys1_ctrl_W_Ready )
	,	.sys1_ctrl_W_Strb( sys1_ctrl_W_Strb )
	,	.sys1_ctrl_W_Valid( sys1_ctrl_W_Valid )
	);



endmodule
