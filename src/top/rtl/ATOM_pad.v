//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module ATOM_pad (
	 output        PAD__TEST_MODE
	,output        PAD__CLK__TEMP
	,output        PAD__RSTN__TEMP
	,input         PAD__TIMER_0__IO__PWM1
	,input         PAD__TIMER_0__IO__PWM2
	,input         PAD__TIMER_0__IO__PWM3
	,input         PAD__TIMER_0__IO__PWM4
	,output [15:0] PAD__GPIO_0__IO__A_A
	,output [15:0] PAD__GPIO_0__IO__B_A
	,output [15:0] PAD__GPIO_0__IO__C_A
	,output [15:0] PAD__GPIO_0__IO__D_A
	,input  [15:0] PAD__GPIO_0__IO__A_Y
	,input  [15:0] PAD__GPIO_0__IO__B_Y
	,input  [15:0] PAD__GPIO_0__IO__C_Y
	,input  [15:0] PAD__GPIO_0__IO__D_Y
	,input  [15:0] PAD__GPIO_0__IO__A_EN
	,input  [15:0] PAD__GPIO_0__IO__B_EN
	,input  [15:0] PAD__GPIO_0__IO__C_EN
	,input  [15:0] PAD__GPIO_0__IO__D_EN
	,output        PAD__UART_0__IO__RX
	,input         PAD__UART_0__IO__TX
	,output        PAD__UART_1__IO__RX
	,input         PAD__UART_1__IO__TX
	,output        PAD__I2C_0__IO__SCL
	,output        PAD__I2C_0__IO__SDA
	,output        PAD__I2C_1__IO__SCL
	,output        PAD__I2C_1__IO__SDA
	,output        PAD__I2C_2__IO__SCL
	,output        PAD__I2C_2__IO__SDA
	,output        PAD__I2C_3__IO__SCL
	,output        PAD__I2C_3__IO__SDA
	,input         PAD__SPI_0__IO__CLK
	,input         PAD__SPI_0__IO__CS
	,output [ 7:0] PAD__SPI_0__IO__DAT_A
	,input  [ 7:0] PAD__SPI_0__IO__DAT_Y
	,input  [ 7:0] PAD__SPI_0__IO__DAT_EN
	,input         PAD__SPI_1__IO__CLK
	,input         PAD__SPI_1__IO__CS
	,output [ 7:0] PAD__SPI_1__IO__DAT_A
	,input  [ 7:0] PAD__SPI_1__IO__DAT_Y
	,input  [ 7:0] PAD__SPI_1__IO__DAT_EN
	,input         PAD__SPI_2__IO__CLK
	,input         PAD__SPI_2__IO__CS
	,output [ 7:0] PAD__SPI_2__IO__DAT_A
	,input  [ 7:0] PAD__SPI_2__IO__DAT_Y
	,input  [ 7:0] PAD__SPI_2__IO__DAT_EN
	,input         PAD__SPI_3__IO__CLK
	,input         PAD__SPI_3__IO__CS
	,output [ 7:0] PAD__SPI_3__IO__DAT_A
	,input  [ 7:0] PAD__SPI_3__IO__DAT_Y
	,input  [ 7:0] PAD__SPI_3__IO__DAT_EN
	,input         IO__TEST_MODE
	,input         IO__CLK__TEMP
	,input         IO__RSTN__TEMP
	,output        IO__TIMER_0__IO__PWM1
	,output        IO__TIMER_0__IO__PWM2
	,output        IO__TIMER_0__IO__PWM3
	,output        IO__TIMER_0__IO__PWM4
	,input  [15:0] IO__GPIO_0__IO__A_A
	,input  [15:0] IO__GPIO_0__IO__B_A
	,input  [15:0] IO__GPIO_0__IO__C_A
	,input  [15:0] IO__GPIO_0__IO__D_A
	,output [15:0] IO__GPIO_0__IO__A_Y
	,output [15:0] IO__GPIO_0__IO__B_Y
	,output [15:0] IO__GPIO_0__IO__C_Y
	,output [15:0] IO__GPIO_0__IO__D_Y
	,output [15:0] IO__GPIO_0__IO__A_EN
	,output [15:0] IO__GPIO_0__IO__B_EN
	,output [15:0] IO__GPIO_0__IO__C_EN
	,output [15:0] IO__GPIO_0__IO__D_EN
	,input         IO__UART_0__IO__RX
	,output        IO__UART_0__IO__TX
	,input         IO__UART_1__IO__RX
	,output        IO__UART_1__IO__TX
	,input         IO__I2C_0__IO__SCL
	,input         IO__I2C_0__IO__SDA
	,input         IO__I2C_1__IO__SCL
	,input         IO__I2C_1__IO__SDA
	,input         IO__I2C_2__IO__SCL
	,input         IO__I2C_2__IO__SDA
	,input         IO__I2C_3__IO__SCL
	,input         IO__I2C_3__IO__SDA
	,output        IO__SPI_0__IO__CLK
	,output        IO__SPI_0__IO__CS
	,input  [ 7:0] IO__SPI_0__IO__DAT_A
	,output [ 7:0] IO__SPI_0__IO__DAT_Y
	,output [ 7:0] IO__SPI_0__IO__DAT_EN
	,output        IO__SPI_1__IO__CLK
	,output        IO__SPI_1__IO__CS
	,input  [ 7:0] IO__SPI_1__IO__DAT_A
	,output [ 7:0] IO__SPI_1__IO__DAT_Y
	,output [ 7:0] IO__SPI_1__IO__DAT_EN
	,output        IO__SPI_2__IO__CLK
	,output        IO__SPI_2__IO__CS
	,input  [ 7:0] IO__SPI_2__IO__DAT_A
	,output [ 7:0] IO__SPI_2__IO__DAT_Y
	,output [ 7:0] IO__SPI_2__IO__DAT_EN
	,output        IO__SPI_3__IO__CLK
	,output        IO__SPI_3__IO__CS
	,input  [ 7:0] IO__SPI_3__IO__DAT_A
	,output [ 7:0] IO__SPI_3__IO__DAT_Y
	,output [ 7:0] IO__SPI_3__IO__DAT_EN
);
	assign PAD__TEST_MODE = IO__TEST_MODE;
	assign PAD__CLK__TEMP = IO__CLK__TEMP;
	assign PAD__RSTN__TEMP = IO__RSTN__TEMP;
	assign IO__TIMER_0__IO__PWM1 = PAD__TIMER_0__IO__PWM1;
	assign IO__TIMER_0__IO__PWM2 = PAD__TIMER_0__IO__PWM2;
	assign IO__TIMER_0__IO__PWM3 = PAD__TIMER_0__IO__PWM3;
	assign IO__TIMER_0__IO__PWM4 = PAD__TIMER_0__IO__PWM4;
	assign PAD__GPIO_0__IO__A_A = IO__GPIO_0__IO__A_A;
	assign PAD__GPIO_0__IO__B_A = IO__GPIO_0__IO__B_A;
	assign PAD__GPIO_0__IO__C_A = IO__GPIO_0__IO__C_A;
	assign PAD__GPIO_0__IO__D_A = IO__GPIO_0__IO__D_A;
	assign IO__GPIO_0__IO__A_Y = PAD__GPIO_0__IO__A_Y;
	assign IO__GPIO_0__IO__B_Y = PAD__GPIO_0__IO__B_Y;
	assign IO__GPIO_0__IO__C_Y = PAD__GPIO_0__IO__C_Y;
	assign IO__GPIO_0__IO__D_Y = PAD__GPIO_0__IO__D_Y;
	assign IO__GPIO_0__IO__A_EN = PAD__GPIO_0__IO__A_EN;
	assign IO__GPIO_0__IO__B_EN = PAD__GPIO_0__IO__B_EN;
	assign IO__GPIO_0__IO__C_EN = PAD__GPIO_0__IO__C_EN;
	assign IO__GPIO_0__IO__D_EN = PAD__GPIO_0__IO__D_EN;
	assign PAD__UART_0__IO__RX = IO__UART_0__IO__RX;
	assign IO__UART_0__IO__TX = PAD__UART_0__IO__TX;
	assign PAD__UART_1__IO__RX = IO__UART_1__IO__RX;
	assign IO__UART_1__IO__TX = PAD__UART_1__IO__TX;
	assign PAD__I2C_0__IO__SCL = IO__I2C_0__IO__SCL;
	assign PAD__I2C_0__IO__SDA = IO__I2C_0__IO__SDA;
	assign PAD__I2C_1__IO__SCL = IO__I2C_1__IO__SCL;
	assign PAD__I2C_1__IO__SDA = IO__I2C_1__IO__SDA;
	assign PAD__I2C_2__IO__SCL = IO__I2C_2__IO__SCL;
	assign PAD__I2C_2__IO__SDA = IO__I2C_2__IO__SDA;
	assign PAD__I2C_3__IO__SCL = IO__I2C_3__IO__SCL;
	assign PAD__I2C_3__IO__SDA = IO__I2C_3__IO__SDA;
	assign IO__SPI_0__IO__CLK = PAD__SPI_0__IO__CLK;
	assign IO__SPI_0__IO__CS = PAD__SPI_0__IO__CS;
	assign PAD__SPI_0__IO__DAT_A = IO__SPI_0__IO__DAT_A;
	assign IO__SPI_0__IO__DAT_Y = PAD__SPI_0__IO__DAT_Y;
	assign IO__SPI_0__IO__DAT_EN = PAD__SPI_0__IO__DAT_EN;
	assign IO__SPI_1__IO__CLK = PAD__SPI_1__IO__CLK;
	assign IO__SPI_1__IO__CS = PAD__SPI_1__IO__CS;
	assign PAD__SPI_1__IO__DAT_A = IO__SPI_1__IO__DAT_A;
	assign IO__SPI_1__IO__DAT_Y = PAD__SPI_1__IO__DAT_Y;
	assign IO__SPI_1__IO__DAT_EN = PAD__SPI_1__IO__DAT_EN;
	assign IO__SPI_2__IO__CLK = PAD__SPI_2__IO__CLK;
	assign IO__SPI_2__IO__CS = PAD__SPI_2__IO__CS;
	assign PAD__SPI_2__IO__DAT_A = IO__SPI_2__IO__DAT_A;
	assign IO__SPI_2__IO__DAT_Y = PAD__SPI_2__IO__DAT_Y;
	assign IO__SPI_2__IO__DAT_EN = PAD__SPI_2__IO__DAT_EN;
	assign IO__SPI_3__IO__CLK = PAD__SPI_3__IO__CLK;
	assign IO__SPI_3__IO__CS = PAD__SPI_3__IO__CS;
	assign PAD__SPI_3__IO__DAT_A = IO__SPI_3__IO__DAT_A;
	assign IO__SPI_3__IO__DAT_Y = PAD__SPI_3__IO__DAT_Y;
	assign IO__SPI_3__IO__DAT_EN = PAD__SPI_3__IO__DAT_EN;

endmodule 
