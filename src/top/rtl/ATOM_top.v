//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module ATOM_top (
	 input         TEST_MODE
	,input         CLK__TEMP
	,input         RSTN__TEMP
	,output        TIMER_0__IO__PWM1
	,output        TIMER_0__IO__PWM2
	,output        TIMER_0__IO__PWM3
	,output        TIMER_0__IO__PWM4
	,input  [15:0] GPIO_0__IO__A_A
	,input  [15:0] GPIO_0__IO__B_A
	,input  [15:0] GPIO_0__IO__C_A
	,input  [15:0] GPIO_0__IO__D_A
	,output [15:0] GPIO_0__IO__A_Y
	,output [15:0] GPIO_0__IO__B_Y
	,output [15:0] GPIO_0__IO__C_Y
	,output [15:0] GPIO_0__IO__D_Y
	,output [15:0] GPIO_0__IO__A_EN
	,output [15:0] GPIO_0__IO__B_EN
	,output [15:0] GPIO_0__IO__C_EN
	,output [15:0] GPIO_0__IO__D_EN
	,input         UART_0__IO__RX
	,output        UART_0__IO__TX
	,input         UART_1__IO__RX
	,output        UART_1__IO__TX
	,input         I2C_0__IO__SCL
	,input         I2C_0__IO__SDA
	,input         I2C_1__IO__SCL
	,input         I2C_1__IO__SDA
	,input         I2C_2__IO__SCL
	,input         I2C_2__IO__SDA
	,input         I2C_3__IO__SCL
	,input         I2C_3__IO__SDA
	,output        SPI_0__IO__CLK
	,output        SPI_0__IO__CS
	,input  [ 7:0] SPI_0__IO__DAT_A
	,output [ 7:0] SPI_0__IO__DAT_Y
	,output [ 7:0] SPI_0__IO__DAT_EN
	,output        SPI_1__IO__CLK
	,output        SPI_1__IO__CS
	,input  [ 7:0] SPI_1__IO__DAT_A
	,output [ 7:0] SPI_1__IO__DAT_Y
	,output [ 7:0] SPI_1__IO__DAT_EN
	,output        SPI_2__IO__CLK
	,output        SPI_2__IO__CS
	,input  [ 7:0] SPI_2__IO__DAT_A
	,output [ 7:0] SPI_2__IO__DAT_Y
	,output [ 7:0] SPI_2__IO__DAT_EN
	,output        SPI_3__IO__CLK
	,output        SPI_3__IO__CS
	,input  [ 7:0] SPI_3__IO__DAT_A
	,output [ 7:0] SPI_3__IO__DAT_Y
	,output [ 7:0] SPI_3__IO__DAT_EN
);
	wire        ATOM_design__ATOM_iomux__TEST_MODE;
	wire        ATOM_design__ATOM_iomux__CLK__TEMP;
	wire        ATOM_design__ATOM_iomux__RSTN__TEMP;
	wire        ATOM_design__ATOM_iomux__TIMER_0__IO__PWM1;
	wire        ATOM_design__ATOM_iomux__TIMER_0__IO__PWM2;
	wire        ATOM_design__ATOM_iomux__TIMER_0__IO__PWM3;
	wire        ATOM_design__ATOM_iomux__TIMER_0__IO__PWM4;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__A_A;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__B_A;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__C_A;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__D_A;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__A_Y;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__B_Y;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__C_Y;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__D_Y;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__A_EN;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__B_EN;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__C_EN;
	wire [15:0] ATOM_design__ATOM_iomux__GPIO_0__IO__D_EN;
	wire        ATOM_design__ATOM_iomux__UART_0__IO__RX;
	wire        ATOM_design__ATOM_iomux__UART_0__IO__TX;
	wire        ATOM_design__ATOM_iomux__UART_1__IO__RX;
	wire        ATOM_design__ATOM_iomux__UART_1__IO__TX;
	wire        ATOM_design__ATOM_iomux__I2C_0__IO__SCL;
	wire        ATOM_design__ATOM_iomux__I2C_0__IO__SDA;
	wire        ATOM_design__ATOM_iomux__I2C_1__IO__SCL;
	wire        ATOM_design__ATOM_iomux__I2C_1__IO__SDA;
	wire        ATOM_design__ATOM_iomux__I2C_2__IO__SCL;
	wire        ATOM_design__ATOM_iomux__I2C_2__IO__SDA;
	wire        ATOM_design__ATOM_iomux__I2C_3__IO__SCL;
	wire        ATOM_design__ATOM_iomux__I2C_3__IO__SDA;
	wire        ATOM_design__ATOM_iomux__SPI_0__IO__CLK;
	wire        ATOM_design__ATOM_iomux__SPI_0__IO__CS;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_0__IO__DAT_A;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_0__IO__DAT_Y;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_0__IO__DAT_EN;
	wire        ATOM_design__ATOM_iomux__SPI_1__IO__CLK;
	wire        ATOM_design__ATOM_iomux__SPI_1__IO__CS;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_1__IO__DAT_A;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_1__IO__DAT_Y;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_1__IO__DAT_EN;
	wire        ATOM_design__ATOM_iomux__SPI_2__IO__CLK;
	wire        ATOM_design__ATOM_iomux__SPI_2__IO__CS;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_2__IO__DAT_A;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_2__IO__DAT_Y;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_2__IO__DAT_EN;
	wire        ATOM_design__ATOM_iomux__SPI_3__IO__CLK;
	wire        ATOM_design__ATOM_iomux__SPI_3__IO__CS;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_3__IO__DAT_A;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_3__IO__DAT_Y;
	wire [ 7:0] ATOM_design__ATOM_iomux__SPI_3__IO__DAT_EN;
	wire        ATOM_iomux__ATOM_pad__PAD__TEST_MODE;
	wire        ATOM_iomux__ATOM_pad__PAD__CLK__TEMP;
	wire        ATOM_iomux__ATOM_pad__PAD__RSTN__TEMP;
	wire        ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM1;
	wire        ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM2;
	wire        ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM3;
	wire        ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM4;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_A;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_A;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_A;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_A;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_Y;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_Y;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_Y;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_Y;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_EN;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_EN;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_EN;
	wire [15:0] ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_EN;
	wire        ATOM_iomux__ATOM_pad__PAD__UART_0__IO__RX;
	wire        ATOM_iomux__ATOM_pad__PAD__UART_0__IO__TX;
	wire        ATOM_iomux__ATOM_pad__PAD__UART_1__IO__RX;
	wire        ATOM_iomux__ATOM_pad__PAD__UART_1__IO__TX;
	wire        ATOM_iomux__ATOM_pad__PAD__I2C_0__IO__SCL;
	wire        ATOM_iomux__ATOM_pad__PAD__I2C_0__IO__SDA;
	wire        ATOM_iomux__ATOM_pad__PAD__I2C_1__IO__SCL;
	wire        ATOM_iomux__ATOM_pad__PAD__I2C_1__IO__SDA;
	wire        ATOM_iomux__ATOM_pad__PAD__I2C_2__IO__SCL;
	wire        ATOM_iomux__ATOM_pad__PAD__I2C_2__IO__SDA;
	wire        ATOM_iomux__ATOM_pad__PAD__I2C_3__IO__SCL;
	wire        ATOM_iomux__ATOM_pad__PAD__I2C_3__IO__SDA;
	wire        ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__CLK;
	wire        ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__CS;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_A;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_Y;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_EN;
	wire        ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__CLK;
	wire        ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__CS;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_A;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_Y;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_EN;
	wire        ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__CLK;
	wire        ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__CS;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_A;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_Y;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_EN;
	wire        ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__CLK;
	wire        ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__CS;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_A;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_Y;
	wire [ 7:0] ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_EN;

	//ATOM_design
	atom_core
	ATOM_design (
		.TEST_MODE        (ATOM_design__ATOM_iomux__TEST_MODE)
		,.CLK__TEMP        (ATOM_design__ATOM_iomux__CLK__TEMP)
		,.RSTN__TEMP       (ATOM_design__ATOM_iomux__RSTN__TEMP)
		,.TIMER_0__IO__PWM1(ATOM_design__ATOM_iomux__TIMER_0__IO__PWM1)
		,.TIMER_0__IO__PWM2(ATOM_design__ATOM_iomux__TIMER_0__IO__PWM2)
		,.TIMER_0__IO__PWM3(ATOM_design__ATOM_iomux__TIMER_0__IO__PWM3)
		,.TIMER_0__IO__PWM4(ATOM_design__ATOM_iomux__TIMER_0__IO__PWM4)
		,.GPIO_0__IO__A_A  (ATOM_design__ATOM_iomux__GPIO_0__IO__A_A)
		,.GPIO_0__IO__B_A  (ATOM_design__ATOM_iomux__GPIO_0__IO__B_A)
		,.GPIO_0__IO__C_A  (ATOM_design__ATOM_iomux__GPIO_0__IO__C_A)
		,.GPIO_0__IO__D_A  (ATOM_design__ATOM_iomux__GPIO_0__IO__D_A)
		,.GPIO_0__IO__A_Y  (ATOM_design__ATOM_iomux__GPIO_0__IO__A_Y)
		,.GPIO_0__IO__B_Y  (ATOM_design__ATOM_iomux__GPIO_0__IO__B_Y)
		,.GPIO_0__IO__C_Y  (ATOM_design__ATOM_iomux__GPIO_0__IO__C_Y)
		,.GPIO_0__IO__D_Y  (ATOM_design__ATOM_iomux__GPIO_0__IO__D_Y)
		,.GPIO_0__IO__A_EN (ATOM_design__ATOM_iomux__GPIO_0__IO__A_EN)
		,.GPIO_0__IO__B_EN (ATOM_design__ATOM_iomux__GPIO_0__IO__B_EN)
		,.GPIO_0__IO__C_EN (ATOM_design__ATOM_iomux__GPIO_0__IO__C_EN)
		,.GPIO_0__IO__D_EN (ATOM_design__ATOM_iomux__GPIO_0__IO__D_EN)
		,.UART_0__IO__RX   (ATOM_design__ATOM_iomux__UART_0__IO__RX)
		,.UART_0__IO__TX   (ATOM_design__ATOM_iomux__UART_0__IO__TX)
		,.UART_1__IO__RX   (ATOM_design__ATOM_iomux__UART_1__IO__RX)
		,.UART_1__IO__TX   (ATOM_design__ATOM_iomux__UART_1__IO__TX)
		,.I2C_0__IO__SCL   (ATOM_design__ATOM_iomux__I2C_0__IO__SCL)
		,.I2C_0__IO__SDA   (ATOM_design__ATOM_iomux__I2C_0__IO__SDA)
		,.I2C_1__IO__SCL   (ATOM_design__ATOM_iomux__I2C_1__IO__SCL)
		,.I2C_1__IO__SDA   (ATOM_design__ATOM_iomux__I2C_1__IO__SDA)
		,.I2C_2__IO__SCL   (ATOM_design__ATOM_iomux__I2C_2__IO__SCL)
		,.I2C_2__IO__SDA   (ATOM_design__ATOM_iomux__I2C_2__IO__SDA)
		,.I2C_3__IO__SCL   (ATOM_design__ATOM_iomux__I2C_3__IO__SCL)
		,.I2C_3__IO__SDA   (ATOM_design__ATOM_iomux__I2C_3__IO__SDA)
		,.SPI_0__IO__CLK   (ATOM_design__ATOM_iomux__SPI_0__IO__CLK)
		,.SPI_0__IO__CS    (ATOM_design__ATOM_iomux__SPI_0__IO__CS)
		,.SPI_0__IO__DAT_A (ATOM_design__ATOM_iomux__SPI_0__IO__DAT_A)
		,.SPI_0__IO__DAT_Y (ATOM_design__ATOM_iomux__SPI_0__IO__DAT_Y)
		,.SPI_0__IO__DAT_EN(ATOM_design__ATOM_iomux__SPI_0__IO__DAT_EN)
		,.SPI_1__IO__CLK   (ATOM_design__ATOM_iomux__SPI_1__IO__CLK)
		,.SPI_1__IO__CS    (ATOM_design__ATOM_iomux__SPI_1__IO__CS)
		,.SPI_1__IO__DAT_A (ATOM_design__ATOM_iomux__SPI_1__IO__DAT_A)
		,.SPI_1__IO__DAT_Y (ATOM_design__ATOM_iomux__SPI_1__IO__DAT_Y)
		,.SPI_1__IO__DAT_EN(ATOM_design__ATOM_iomux__SPI_1__IO__DAT_EN)
		,.SPI_2__IO__CLK   (ATOM_design__ATOM_iomux__SPI_2__IO__CLK)
		,.SPI_2__IO__CS    (ATOM_design__ATOM_iomux__SPI_2__IO__CS)
		,.SPI_2__IO__DAT_A (ATOM_design__ATOM_iomux__SPI_2__IO__DAT_A)
		,.SPI_2__IO__DAT_Y (ATOM_design__ATOM_iomux__SPI_2__IO__DAT_Y)
		,.SPI_2__IO__DAT_EN(ATOM_design__ATOM_iomux__SPI_2__IO__DAT_EN)
		,.SPI_3__IO__CLK   (ATOM_design__ATOM_iomux__SPI_3__IO__CLK)
		,.SPI_3__IO__CS    (ATOM_design__ATOM_iomux__SPI_3__IO__CS)
		,.SPI_3__IO__DAT_A (ATOM_design__ATOM_iomux__SPI_3__IO__DAT_A)
		,.SPI_3__IO__DAT_Y (ATOM_design__ATOM_iomux__SPI_3__IO__DAT_Y)
		,.SPI_3__IO__DAT_EN(ATOM_design__ATOM_iomux__SPI_3__IO__DAT_EN)
	);

	ATOM_iomux
	ATOM_iomux (
		 .DESIGN__TEST_MODE        (ATOM_design__ATOM_iomux__TEST_MODE)
		,.DESIGN__CLK__TEMP        (ATOM_design__ATOM_iomux__CLK__TEMP)
		,.DESIGN__RSTN__TEMP       (ATOM_design__ATOM_iomux__RSTN__TEMP)
		,.DESIGN__TIMER_0__IO__PWM1(ATOM_design__ATOM_iomux__TIMER_0__IO__PWM1)
		,.DESIGN__TIMER_0__IO__PWM2(ATOM_design__ATOM_iomux__TIMER_0__IO__PWM2)
		,.DESIGN__TIMER_0__IO__PWM3(ATOM_design__ATOM_iomux__TIMER_0__IO__PWM3)
		,.DESIGN__TIMER_0__IO__PWM4(ATOM_design__ATOM_iomux__TIMER_0__IO__PWM4)
		,.DESIGN__GPIO_0__IO__A_A  (ATOM_design__ATOM_iomux__GPIO_0__IO__A_A)
		,.DESIGN__GPIO_0__IO__B_A  (ATOM_design__ATOM_iomux__GPIO_0__IO__B_A)
		,.DESIGN__GPIO_0__IO__C_A  (ATOM_design__ATOM_iomux__GPIO_0__IO__C_A)
		,.DESIGN__GPIO_0__IO__D_A  (ATOM_design__ATOM_iomux__GPIO_0__IO__D_A)
		,.DESIGN__GPIO_0__IO__A_Y  (ATOM_design__ATOM_iomux__GPIO_0__IO__A_Y)
		,.DESIGN__GPIO_0__IO__B_Y  (ATOM_design__ATOM_iomux__GPIO_0__IO__B_Y)
		,.DESIGN__GPIO_0__IO__C_Y  (ATOM_design__ATOM_iomux__GPIO_0__IO__C_Y)
		,.DESIGN__GPIO_0__IO__D_Y  (ATOM_design__ATOM_iomux__GPIO_0__IO__D_Y)
		,.DESIGN__GPIO_0__IO__A_EN (ATOM_design__ATOM_iomux__GPIO_0__IO__A_EN)
		,.DESIGN__GPIO_0__IO__B_EN (ATOM_design__ATOM_iomux__GPIO_0__IO__B_EN)
		,.DESIGN__GPIO_0__IO__C_EN (ATOM_design__ATOM_iomux__GPIO_0__IO__C_EN)
		,.DESIGN__GPIO_0__IO__D_EN (ATOM_design__ATOM_iomux__GPIO_0__IO__D_EN)
		,.DESIGN__UART_0__IO__RX   (ATOM_design__ATOM_iomux__UART_0__IO__RX)
		,.DESIGN__UART_0__IO__TX   (ATOM_design__ATOM_iomux__UART_0__IO__TX)
		,.DESIGN__UART_1__IO__RX   (ATOM_design__ATOM_iomux__UART_1__IO__RX)
		,.DESIGN__UART_1__IO__TX   (ATOM_design__ATOM_iomux__UART_1__IO__TX)
		,.DESIGN__I2C_0__IO__SCL   (ATOM_design__ATOM_iomux__I2C_0__IO__SCL)
		,.DESIGN__I2C_0__IO__SDA   (ATOM_design__ATOM_iomux__I2C_0__IO__SDA)
		,.DESIGN__I2C_1__IO__SCL   (ATOM_design__ATOM_iomux__I2C_1__IO__SCL)
		,.DESIGN__I2C_1__IO__SDA   (ATOM_design__ATOM_iomux__I2C_1__IO__SDA)
		,.DESIGN__I2C_2__IO__SCL   (ATOM_design__ATOM_iomux__I2C_2__IO__SCL)
		,.DESIGN__I2C_2__IO__SDA   (ATOM_design__ATOM_iomux__I2C_2__IO__SDA)
		,.DESIGN__I2C_3__IO__SCL   (ATOM_design__ATOM_iomux__I2C_3__IO__SCL)
		,.DESIGN__I2C_3__IO__SDA   (ATOM_design__ATOM_iomux__I2C_3__IO__SDA)
		,.DESIGN__SPI_0__IO__CLK   (ATOM_design__ATOM_iomux__SPI_0__IO__CLK)
		,.DESIGN__SPI_0__IO__CS    (ATOM_design__ATOM_iomux__SPI_0__IO__CS)
		,.DESIGN__SPI_0__IO__DAT_A (ATOM_design__ATOM_iomux__SPI_0__IO__DAT_A)
		,.DESIGN__SPI_0__IO__DAT_Y (ATOM_design__ATOM_iomux__SPI_0__IO__DAT_Y)
		,.DESIGN__SPI_0__IO__DAT_EN(ATOM_design__ATOM_iomux__SPI_0__IO__DAT_EN)
		,.DESIGN__SPI_1__IO__CLK   (ATOM_design__ATOM_iomux__SPI_1__IO__CLK)
		,.DESIGN__SPI_1__IO__CS    (ATOM_design__ATOM_iomux__SPI_1__IO__CS)
		,.DESIGN__SPI_1__IO__DAT_A (ATOM_design__ATOM_iomux__SPI_1__IO__DAT_A)
		,.DESIGN__SPI_1__IO__DAT_Y (ATOM_design__ATOM_iomux__SPI_1__IO__DAT_Y)
		,.DESIGN__SPI_1__IO__DAT_EN(ATOM_design__ATOM_iomux__SPI_1__IO__DAT_EN)
		,.DESIGN__SPI_2__IO__CLK   (ATOM_design__ATOM_iomux__SPI_2__IO__CLK)
		,.DESIGN__SPI_2__IO__CS    (ATOM_design__ATOM_iomux__SPI_2__IO__CS)
		,.DESIGN__SPI_2__IO__DAT_A (ATOM_design__ATOM_iomux__SPI_2__IO__DAT_A)
		,.DESIGN__SPI_2__IO__DAT_Y (ATOM_design__ATOM_iomux__SPI_2__IO__DAT_Y)
		,.DESIGN__SPI_2__IO__DAT_EN(ATOM_design__ATOM_iomux__SPI_2__IO__DAT_EN)
		,.DESIGN__SPI_3__IO__CLK   (ATOM_design__ATOM_iomux__SPI_3__IO__CLK)
		,.DESIGN__SPI_3__IO__CS    (ATOM_design__ATOM_iomux__SPI_3__IO__CS)
		,.DESIGN__SPI_3__IO__DAT_A (ATOM_design__ATOM_iomux__SPI_3__IO__DAT_A)
		,.DESIGN__SPI_3__IO__DAT_Y (ATOM_design__ATOM_iomux__SPI_3__IO__DAT_Y)
		,.DESIGN__SPI_3__IO__DAT_EN(ATOM_design__ATOM_iomux__SPI_3__IO__DAT_EN)
		,.PAD__TEST_MODE           (ATOM_iomux__ATOM_pad__PAD__TEST_MODE)
		,.PAD__CLK__TEMP           (ATOM_iomux__ATOM_pad__PAD__CLK__TEMP)
		,.PAD__RSTN__TEMP          (ATOM_iomux__ATOM_pad__PAD__RSTN__TEMP)
		,.PAD__TIMER_0__IO__PWM1   (ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM1)
		,.PAD__TIMER_0__IO__PWM2   (ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM2)
		,.PAD__TIMER_0__IO__PWM3   (ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM3)
		,.PAD__TIMER_0__IO__PWM4   (ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM4)
		,.PAD__GPIO_0__IO__A_A     (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_A)
		,.PAD__GPIO_0__IO__B_A     (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_A)
		,.PAD__GPIO_0__IO__C_A     (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_A)
		,.PAD__GPIO_0__IO__D_A     (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_A)
		,.PAD__GPIO_0__IO__A_Y     (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_Y)
		,.PAD__GPIO_0__IO__B_Y     (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_Y)
		,.PAD__GPIO_0__IO__C_Y     (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_Y)
		,.PAD__GPIO_0__IO__D_Y     (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_Y)
		,.PAD__GPIO_0__IO__A_EN    (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_EN)
		,.PAD__GPIO_0__IO__B_EN    (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_EN)
		,.PAD__GPIO_0__IO__C_EN    (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_EN)
		,.PAD__GPIO_0__IO__D_EN    (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_EN)
		,.PAD__UART_0__IO__RX      (ATOM_iomux__ATOM_pad__PAD__UART_0__IO__RX)
		,.PAD__UART_0__IO__TX      (ATOM_iomux__ATOM_pad__PAD__UART_0__IO__TX)
		,.PAD__UART_1__IO__RX      (ATOM_iomux__ATOM_pad__PAD__UART_1__IO__RX)
		,.PAD__UART_1__IO__TX      (ATOM_iomux__ATOM_pad__PAD__UART_1__IO__TX)
		,.PAD__I2C_0__IO__SCL      (ATOM_iomux__ATOM_pad__PAD__I2C_0__IO__SCL)
		,.PAD__I2C_0__IO__SDA      (ATOM_iomux__ATOM_pad__PAD__I2C_0__IO__SDA)
		,.PAD__I2C_1__IO__SCL      (ATOM_iomux__ATOM_pad__PAD__I2C_1__IO__SCL)
		,.PAD__I2C_1__IO__SDA      (ATOM_iomux__ATOM_pad__PAD__I2C_1__IO__SDA)
		,.PAD__I2C_2__IO__SCL      (ATOM_iomux__ATOM_pad__PAD__I2C_2__IO__SCL)
		,.PAD__I2C_2__IO__SDA      (ATOM_iomux__ATOM_pad__PAD__I2C_2__IO__SDA)
		,.PAD__I2C_3__IO__SCL      (ATOM_iomux__ATOM_pad__PAD__I2C_3__IO__SCL)
		,.PAD__I2C_3__IO__SDA      (ATOM_iomux__ATOM_pad__PAD__I2C_3__IO__SDA)
		,.PAD__SPI_0__IO__CLK      (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__CLK)
		,.PAD__SPI_0__IO__CS       (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__CS)
		,.PAD__SPI_0__IO__DAT_A    (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_A)
		,.PAD__SPI_0__IO__DAT_Y    (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_Y)
		,.PAD__SPI_0__IO__DAT_EN   (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_EN)
		,.PAD__SPI_1__IO__CLK      (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__CLK)
		,.PAD__SPI_1__IO__CS       (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__CS)
		,.PAD__SPI_1__IO__DAT_A    (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_A)
		,.PAD__SPI_1__IO__DAT_Y    (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_Y)
		,.PAD__SPI_1__IO__DAT_EN   (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_EN)
		,.PAD__SPI_2__IO__CLK      (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__CLK)
		,.PAD__SPI_2__IO__CS       (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__CS)
		,.PAD__SPI_2__IO__DAT_A    (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_A)
		,.PAD__SPI_2__IO__DAT_Y    (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_Y)
		,.PAD__SPI_2__IO__DAT_EN   (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_EN)
		,.PAD__SPI_3__IO__CLK      (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__CLK)
		,.PAD__SPI_3__IO__CS       (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__CS)
		,.PAD__SPI_3__IO__DAT_A    (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_A)
		,.PAD__SPI_3__IO__DAT_Y    (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_Y)
		,.PAD__SPI_3__IO__DAT_EN   (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_EN)
	);

	ATOM_pad
	ATOM_pad (
		 .PAD__TEST_MODE        (ATOM_iomux__ATOM_pad__PAD__TEST_MODE)
		,.PAD__CLK__TEMP        (ATOM_iomux__ATOM_pad__PAD__CLK__TEMP)
		,.PAD__RSTN__TEMP       (ATOM_iomux__ATOM_pad__PAD__RSTN__TEMP)
		,.PAD__TIMER_0__IO__PWM1(ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM1)
		,.PAD__TIMER_0__IO__PWM2(ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM2)
		,.PAD__TIMER_0__IO__PWM3(ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM3)
		,.PAD__TIMER_0__IO__PWM4(ATOM_iomux__ATOM_pad__PAD__TIMER_0__IO__PWM4)
		,.PAD__GPIO_0__IO__A_A  (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_A)
		,.PAD__GPIO_0__IO__B_A  (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_A)
		,.PAD__GPIO_0__IO__C_A  (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_A)
		,.PAD__GPIO_0__IO__D_A  (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_A)
		,.PAD__GPIO_0__IO__A_Y  (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_Y)
		,.PAD__GPIO_0__IO__B_Y  (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_Y)
		,.PAD__GPIO_0__IO__C_Y  (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_Y)
		,.PAD__GPIO_0__IO__D_Y  (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_Y)
		,.PAD__GPIO_0__IO__A_EN (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__A_EN)
		,.PAD__GPIO_0__IO__B_EN (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__B_EN)
		,.PAD__GPIO_0__IO__C_EN (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__C_EN)
		,.PAD__GPIO_0__IO__D_EN (ATOM_iomux__ATOM_pad__PAD__GPIO_0__IO__D_EN)
		,.PAD__UART_0__IO__RX   (ATOM_iomux__ATOM_pad__PAD__UART_0__IO__RX)
		,.PAD__UART_0__IO__TX   (ATOM_iomux__ATOM_pad__PAD__UART_0__IO__TX)
		,.PAD__UART_1__IO__RX   (ATOM_iomux__ATOM_pad__PAD__UART_1__IO__RX)
		,.PAD__UART_1__IO__TX   (ATOM_iomux__ATOM_pad__PAD__UART_1__IO__TX)
		,.PAD__I2C_0__IO__SCL   (ATOM_iomux__ATOM_pad__PAD__I2C_0__IO__SCL)
		,.PAD__I2C_0__IO__SDA   (ATOM_iomux__ATOM_pad__PAD__I2C_0__IO__SDA)
		,.PAD__I2C_1__IO__SCL   (ATOM_iomux__ATOM_pad__PAD__I2C_1__IO__SCL)
		,.PAD__I2C_1__IO__SDA   (ATOM_iomux__ATOM_pad__PAD__I2C_1__IO__SDA)
		,.PAD__I2C_2__IO__SCL   (ATOM_iomux__ATOM_pad__PAD__I2C_2__IO__SCL)
		,.PAD__I2C_2__IO__SDA   (ATOM_iomux__ATOM_pad__PAD__I2C_2__IO__SDA)
		,.PAD__I2C_3__IO__SCL   (ATOM_iomux__ATOM_pad__PAD__I2C_3__IO__SCL)
		,.PAD__I2C_3__IO__SDA   (ATOM_iomux__ATOM_pad__PAD__I2C_3__IO__SDA)
		,.PAD__SPI_0__IO__CLK   (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__CLK)
		,.PAD__SPI_0__IO__CS    (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__CS)
		,.PAD__SPI_0__IO__DAT_A (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_A)
		,.PAD__SPI_0__IO__DAT_Y (ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_Y)
		,.PAD__SPI_0__IO__DAT_EN(ATOM_iomux__ATOM_pad__PAD__SPI_0__IO__DAT_EN)
		,.PAD__SPI_1__IO__CLK   (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__CLK)
		,.PAD__SPI_1__IO__CS    (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__CS)
		,.PAD__SPI_1__IO__DAT_A (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_A)
		,.PAD__SPI_1__IO__DAT_Y (ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_Y)
		,.PAD__SPI_1__IO__DAT_EN(ATOM_iomux__ATOM_pad__PAD__SPI_1__IO__DAT_EN)
		,.PAD__SPI_2__IO__CLK   (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__CLK)
		,.PAD__SPI_2__IO__CS    (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__CS)
		,.PAD__SPI_2__IO__DAT_A (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_A)
		,.PAD__SPI_2__IO__DAT_Y (ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_Y)
		,.PAD__SPI_2__IO__DAT_EN(ATOM_iomux__ATOM_pad__PAD__SPI_2__IO__DAT_EN)
		,.PAD__SPI_3__IO__CLK   (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__CLK)
		,.PAD__SPI_3__IO__CS    (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__CS)
		,.PAD__SPI_3__IO__DAT_A (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_A)
		,.PAD__SPI_3__IO__DAT_Y (ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_Y)
		,.PAD__SPI_3__IO__DAT_EN(ATOM_iomux__ATOM_pad__PAD__SPI_3__IO__DAT_EN)
		,.IO__TEST_MODE         (TEST_MODE)
		,.IO__CLK__TEMP         (CLK__TEMP)
		,.IO__RSTN__TEMP        (RSTN__TEMP)
		,.IO__TIMER_0__IO__PWM1 (TIMER_0__IO__PWM1)
		,.IO__TIMER_0__IO__PWM2 (TIMER_0__IO__PWM2)
		,.IO__TIMER_0__IO__PWM3 (TIMER_0__IO__PWM3)
		,.IO__TIMER_0__IO__PWM4 (TIMER_0__IO__PWM4)
		,.IO__GPIO_0__IO__A_A   (GPIO_0__IO__A_A)
		,.IO__GPIO_0__IO__B_A   (GPIO_0__IO__B_A)
		,.IO__GPIO_0__IO__C_A   (GPIO_0__IO__C_A)
		,.IO__GPIO_0__IO__D_A   (GPIO_0__IO__D_A)
		,.IO__GPIO_0__IO__A_Y   (GPIO_0__IO__A_Y)
		,.IO__GPIO_0__IO__B_Y   (GPIO_0__IO__B_Y)
		,.IO__GPIO_0__IO__C_Y   (GPIO_0__IO__C_Y)
		,.IO__GPIO_0__IO__D_Y   (GPIO_0__IO__D_Y)
		,.IO__GPIO_0__IO__A_EN  (GPIO_0__IO__A_EN)
		,.IO__GPIO_0__IO__B_EN  (GPIO_0__IO__B_EN)
		,.IO__GPIO_0__IO__C_EN  (GPIO_0__IO__C_EN)
		,.IO__GPIO_0__IO__D_EN  (GPIO_0__IO__D_EN)
		,.IO__UART_0__IO__RX    (UART_0__IO__RX)
		,.IO__UART_0__IO__TX    (UART_0__IO__TX)
		,.IO__UART_1__IO__RX    (UART_1__IO__RX)
		,.IO__UART_1__IO__TX    (UART_1__IO__TX)
		,.IO__I2C_0__IO__SCL    (I2C_0__IO__SCL)
		,.IO__I2C_0__IO__SDA    (I2C_0__IO__SDA)
		,.IO__I2C_1__IO__SCL    (I2C_1__IO__SCL)
		,.IO__I2C_1__IO__SDA    (I2C_1__IO__SDA)
		,.IO__I2C_2__IO__SCL    (I2C_2__IO__SCL)
		,.IO__I2C_2__IO__SDA    (I2C_2__IO__SDA)
		,.IO__I2C_3__IO__SCL    (I2C_3__IO__SCL)
		,.IO__I2C_3__IO__SDA    (I2C_3__IO__SDA)
		,.IO__SPI_0__IO__CLK    (SPI_0__IO__CLK)
		,.IO__SPI_0__IO__CS     (SPI_0__IO__CS)
		,.IO__SPI_0__IO__DAT_A  (SPI_0__IO__DAT_A)
		,.IO__SPI_0__IO__DAT_Y  (SPI_0__IO__DAT_Y)
		,.IO__SPI_0__IO__DAT_EN (SPI_0__IO__DAT_EN)
		,.IO__SPI_1__IO__CLK    (SPI_1__IO__CLK)
		,.IO__SPI_1__IO__CS     (SPI_1__IO__CS)
		,.IO__SPI_1__IO__DAT_A  (SPI_1__IO__DAT_A)
		,.IO__SPI_1__IO__DAT_Y  (SPI_1__IO__DAT_Y)
		,.IO__SPI_1__IO__DAT_EN (SPI_1__IO__DAT_EN)
		,.IO__SPI_2__IO__CLK    (SPI_2__IO__CLK)
		,.IO__SPI_2__IO__CS     (SPI_2__IO__CS)
		,.IO__SPI_2__IO__DAT_A  (SPI_2__IO__DAT_A)
		,.IO__SPI_2__IO__DAT_Y  (SPI_2__IO__DAT_Y)
		,.IO__SPI_2__IO__DAT_EN (SPI_2__IO__DAT_EN)
		,.IO__SPI_3__IO__CLK    (SPI_3__IO__CLK)
		,.IO__SPI_3__IO__CS     (SPI_3__IO__CS)
		,.IO__SPI_3__IO__DAT_A  (SPI_3__IO__DAT_A)
		,.IO__SPI_3__IO__DAT_Y  (SPI_3__IO__DAT_Y)
		,.IO__SPI_3__IO__DAT_EN (SPI_3__IO__DAT_EN)
	);

endmodule 
