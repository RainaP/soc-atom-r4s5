//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade 
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module ATOM_iomux (
	 output        DESIGN__TEST_MODE
	,output        DESIGN__CLK__TEMP
	,output        DESIGN__RSTN__TEMP
	,input         DESIGN__TIMER_0__IO__PWM1
	,input         DESIGN__TIMER_0__IO__PWM2
	,input         DESIGN__TIMER_0__IO__PWM3
	,input         DESIGN__TIMER_0__IO__PWM4
	,output [15:0] DESIGN__GPIO_0__IO__A_A
	,output [15:0] DESIGN__GPIO_0__IO__B_A
	,output [15:0] DESIGN__GPIO_0__IO__C_A
	,output [15:0] DESIGN__GPIO_0__IO__D_A
	,input  [15:0] DESIGN__GPIO_0__IO__A_Y
	,input  [15:0] DESIGN__GPIO_0__IO__B_Y
	,input  [15:0] DESIGN__GPIO_0__IO__C_Y
	,input  [15:0] DESIGN__GPIO_0__IO__D_Y
	,input  [15:0] DESIGN__GPIO_0__IO__A_EN
	,input  [15:0] DESIGN__GPIO_0__IO__B_EN
	,input  [15:0] DESIGN__GPIO_0__IO__C_EN
	,input  [15:0] DESIGN__GPIO_0__IO__D_EN
	,output        DESIGN__UART_0__IO__RX
	,input         DESIGN__UART_0__IO__TX
	,output        DESIGN__UART_1__IO__RX
	,input         DESIGN__UART_1__IO__TX
	,output        DESIGN__I2C_0__IO__SCL
	,output        DESIGN__I2C_0__IO__SDA
	,output        DESIGN__I2C_1__IO__SCL
	,output        DESIGN__I2C_1__IO__SDA
	,output        DESIGN__I2C_2__IO__SCL
	,output        DESIGN__I2C_2__IO__SDA
	,output        DESIGN__I2C_3__IO__SCL
	,output        DESIGN__I2C_3__IO__SDA
	,input         DESIGN__SPI_0__IO__CLK
	,input         DESIGN__SPI_0__IO__CS
	,output [ 7:0] DESIGN__SPI_0__IO__DAT_A
	,input  [ 7:0] DESIGN__SPI_0__IO__DAT_Y
	,input  [ 7:0] DESIGN__SPI_0__IO__DAT_EN
	,input         DESIGN__SPI_1__IO__CLK
	,input         DESIGN__SPI_1__IO__CS
	,output [ 7:0] DESIGN__SPI_1__IO__DAT_A
	,input  [ 7:0] DESIGN__SPI_1__IO__DAT_Y
	,input  [ 7:0] DESIGN__SPI_1__IO__DAT_EN
	,input         DESIGN__SPI_2__IO__CLK
	,input         DESIGN__SPI_2__IO__CS
	,output [ 7:0] DESIGN__SPI_2__IO__DAT_A
	,input  [ 7:0] DESIGN__SPI_2__IO__DAT_Y
	,input  [ 7:0] DESIGN__SPI_2__IO__DAT_EN
	,input         DESIGN__SPI_3__IO__CLK
	,input         DESIGN__SPI_3__IO__CS
	,output [ 7:0] DESIGN__SPI_3__IO__DAT_A
	,input  [ 7:0] DESIGN__SPI_3__IO__DAT_Y
	,input  [ 7:0] DESIGN__SPI_3__IO__DAT_EN
	,input         PAD__TEST_MODE
	,input         PAD__CLK__TEMP
	,input         PAD__RSTN__TEMP
	,output        PAD__TIMER_0__IO__PWM1
	,output        PAD__TIMER_0__IO__PWM2
	,output        PAD__TIMER_0__IO__PWM3
	,output        PAD__TIMER_0__IO__PWM4
	,input  [15:0] PAD__GPIO_0__IO__A_A
	,input  [15:0] PAD__GPIO_0__IO__B_A
	,input  [15:0] PAD__GPIO_0__IO__C_A
	,input  [15:0] PAD__GPIO_0__IO__D_A
	,output [15:0] PAD__GPIO_0__IO__A_Y
	,output [15:0] PAD__GPIO_0__IO__B_Y
	,output [15:0] PAD__GPIO_0__IO__C_Y
	,output [15:0] PAD__GPIO_0__IO__D_Y
	,output [15:0] PAD__GPIO_0__IO__A_EN
	,output [15:0] PAD__GPIO_0__IO__B_EN
	,output [15:0] PAD__GPIO_0__IO__C_EN
	,output [15:0] PAD__GPIO_0__IO__D_EN
	,input         PAD__UART_0__IO__RX
	,output        PAD__UART_0__IO__TX
	,input         PAD__UART_1__IO__RX
	,output        PAD__UART_1__IO__TX
	,input         PAD__I2C_0__IO__SCL
	,input         PAD__I2C_0__IO__SDA
	,input         PAD__I2C_1__IO__SCL
	,input         PAD__I2C_1__IO__SDA
	,input         PAD__I2C_2__IO__SCL
	,input         PAD__I2C_2__IO__SDA
	,input         PAD__I2C_3__IO__SCL
	,input         PAD__I2C_3__IO__SDA
	,output        PAD__SPI_0__IO__CLK
	,output        PAD__SPI_0__IO__CS
	,input  [ 7:0] PAD__SPI_0__IO__DAT_A
	,output [ 7:0] PAD__SPI_0__IO__DAT_Y
	,output [ 7:0] PAD__SPI_0__IO__DAT_EN
	,output        PAD__SPI_1__IO__CLK
	,output        PAD__SPI_1__IO__CS
	,input  [ 7:0] PAD__SPI_1__IO__DAT_A
	,output [ 7:0] PAD__SPI_1__IO__DAT_Y
	,output [ 7:0] PAD__SPI_1__IO__DAT_EN
	,output        PAD__SPI_2__IO__CLK
	,output        PAD__SPI_2__IO__CS
	,input  [ 7:0] PAD__SPI_2__IO__DAT_A
	,output [ 7:0] PAD__SPI_2__IO__DAT_Y
	,output [ 7:0] PAD__SPI_2__IO__DAT_EN
	,output        PAD__SPI_3__IO__CLK
	,output        PAD__SPI_3__IO__CS
	,input  [ 7:0] PAD__SPI_3__IO__DAT_A
	,output [ 7:0] PAD__SPI_3__IO__DAT_Y
	,output [ 7:0] PAD__SPI_3__IO__DAT_EN
);
	assign DESIGN__TEST_MODE = PAD__TEST_MODE;
	assign DESIGN__CLK__TEMP = PAD__CLK__TEMP;
	assign DESIGN__RSTN__TEMP = PAD__RSTN__TEMP;
	assign PAD__TIMER_0__IO__PWM1 = DESIGN__TIMER_0__IO__PWM1;
	assign PAD__TIMER_0__IO__PWM2 = DESIGN__TIMER_0__IO__PWM2;
	assign PAD__TIMER_0__IO__PWM3 = DESIGN__TIMER_0__IO__PWM3;
	assign PAD__TIMER_0__IO__PWM4 = DESIGN__TIMER_0__IO__PWM4;
	assign DESIGN__GPIO_0__IO__A_A = PAD__GPIO_0__IO__A_A;
	assign DESIGN__GPIO_0__IO__B_A = PAD__GPIO_0__IO__B_A;
	assign DESIGN__GPIO_0__IO__C_A = PAD__GPIO_0__IO__C_A;
	assign DESIGN__GPIO_0__IO__D_A = PAD__GPIO_0__IO__D_A;
	assign PAD__GPIO_0__IO__A_Y = DESIGN__GPIO_0__IO__A_Y;
	assign PAD__GPIO_0__IO__B_Y = DESIGN__GPIO_0__IO__B_Y;
	assign PAD__GPIO_0__IO__C_Y = DESIGN__GPIO_0__IO__C_Y;
	assign PAD__GPIO_0__IO__D_Y = DESIGN__GPIO_0__IO__D_Y;
	assign PAD__GPIO_0__IO__A_EN = DESIGN__GPIO_0__IO__A_EN;
	assign PAD__GPIO_0__IO__B_EN = DESIGN__GPIO_0__IO__B_EN;
	assign PAD__GPIO_0__IO__C_EN = DESIGN__GPIO_0__IO__C_EN;
	assign PAD__GPIO_0__IO__D_EN = DESIGN__GPIO_0__IO__D_EN;
	assign DESIGN__UART_0__IO__RX = PAD__UART_0__IO__RX;
	assign PAD__UART_0__IO__TX = DESIGN__UART_0__IO__TX;
	assign DESIGN__UART_1__IO__RX = PAD__UART_1__IO__RX;
	assign PAD__UART_1__IO__TX = DESIGN__UART_1__IO__TX;
	assign DESIGN__I2C_0__IO__SCL = PAD__I2C_0__IO__SCL;
	assign DESIGN__I2C_0__IO__SDA = PAD__I2C_0__IO__SDA;
	assign DESIGN__I2C_1__IO__SCL = PAD__I2C_1__IO__SCL;
	assign DESIGN__I2C_1__IO__SDA = PAD__I2C_1__IO__SDA;
	assign DESIGN__I2C_2__IO__SCL = PAD__I2C_2__IO__SCL;
	assign DESIGN__I2C_2__IO__SDA = PAD__I2C_2__IO__SDA;
	assign DESIGN__I2C_3__IO__SCL = PAD__I2C_3__IO__SCL;
	assign DESIGN__I2C_3__IO__SDA = PAD__I2C_3__IO__SDA;
	assign PAD__SPI_0__IO__CLK = DESIGN__SPI_0__IO__CLK;
	assign PAD__SPI_0__IO__CS = DESIGN__SPI_0__IO__CS;
	assign DESIGN__SPI_0__IO__DAT_A = PAD__SPI_0__IO__DAT_A;
	assign PAD__SPI_0__IO__DAT_Y = DESIGN__SPI_0__IO__DAT_Y;
	assign PAD__SPI_0__IO__DAT_EN = DESIGN__SPI_0__IO__DAT_EN;
	assign PAD__SPI_1__IO__CLK = DESIGN__SPI_1__IO__CLK;
	assign PAD__SPI_1__IO__CS = DESIGN__SPI_1__IO__CS;
	assign DESIGN__SPI_1__IO__DAT_A = PAD__SPI_1__IO__DAT_A;
	assign PAD__SPI_1__IO__DAT_Y = DESIGN__SPI_1__IO__DAT_Y;
	assign PAD__SPI_1__IO__DAT_EN = DESIGN__SPI_1__IO__DAT_EN;
	assign PAD__SPI_2__IO__CLK = DESIGN__SPI_2__IO__CLK;
	assign PAD__SPI_2__IO__CS = DESIGN__SPI_2__IO__CS;
	assign DESIGN__SPI_2__IO__DAT_A = PAD__SPI_2__IO__DAT_A;
	assign PAD__SPI_2__IO__DAT_Y = DESIGN__SPI_2__IO__DAT_Y;
	assign PAD__SPI_2__IO__DAT_EN = DESIGN__SPI_2__IO__DAT_EN;
	assign PAD__SPI_3__IO__CLK = DESIGN__SPI_3__IO__CLK;
	assign PAD__SPI_3__IO__CS = DESIGN__SPI_3__IO__CS;
	assign DESIGN__SPI_3__IO__DAT_A = PAD__SPI_3__IO__DAT_A;
	assign PAD__SPI_3__IO__DAT_Y = DESIGN__SPI_3__IO__DAT_Y;
	assign PAD__SPI_3__IO__DAT_EN = DESIGN__SPI_3__IO__DAT_EN;

endmodule 
