-f ${DESIGN_DIR}/src/CP_Subsystem/rtl_meta/CP_Subsystem.top.f
-f ${DESIGN_DIR}/src/Dcluster_Subsystem/rtl_meta/Dcluster_Subsystem.top.f
-f ${DESIGN_DIR}/src/DRAM_Subsystem/rtl_meta/DRAM_Subsystem.top.f
-f ${DESIGN_DIR}/src/MainBUS_Subsystem/rtl_meta/MainBUS_Subsystem.top.f
-f ${DESIGN_DIR}/src/PERI_Subsystem/rtl_meta/PERI_Subsystem.top.f
-f ${DESIGN_DIR}/src/PCIE_Subsystem/rtl_meta/PCIE_Subsystem.top.f
-f ${DESIGN_DIR}/src/SHM_Subsystem/rtl_meta/SHM_Subsystem.top.f
-f ${DESIGN_DIR}/src/SouthBUS_Subsystem/rtl_meta/SouthBUS_Subsystem.top.f
-f ${DESIGN_DIR}/src/EastBUS_Subsystem/rtl_meta/EastBUS_Subsystem.top.f
-f ${DESIGN_DIR}/src/WestBUS_Subsystem/rtl_meta/WestBUS_Subsystem.top.f

-f ${DESIGN_DIR}/src/shared/rtl_meta/shared.top.f

${DESIGN_DIR}/src/top/rtl/atom_core.v
${DESIGN_DIR}/src/top/rtl/ATOM_iomux.v
${DESIGN_DIR}/src/top/rtl/ATOM_pad.v
${DESIGN_DIR}/src/top/rtl/ATOM_top.v
