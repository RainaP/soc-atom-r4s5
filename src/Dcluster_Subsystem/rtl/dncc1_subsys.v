// This file is generated from Magillem
// Generation : Wed Aug 11 13:20:08 KST 2021 by miock from project atom
// Component : rebellions atom dncc1_subsys 0.0
// Design : rebellions atom dncc1_subsys_arch 0.0
//  u_dnc0                                  rebellions  atom    dnc                                   0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_0.0.xml 
//  u_dnc1                                  rebellions  atom    dnc                                   0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_0.0.xml 
//  u_dnc2                                  rebellions  atom    dnc                                   0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_0.0.xml 
//  u_dnc3                                  rebellions  atom    dnc                                   0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_0.0.xml 
//  u_dnc_cc                                rebellions  atom    dnc_cc                                0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_cc_0.0.xml 
//  u_cbus_Structure_Module_dcluster1       arteris.com FLEXNOC cbus_Structure_Module_dcluster1       4.5.1 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/cbus_Structure_Module_dcluster1_1.xml 
//  u_dbus_read_Structure_Module_dcluster1  arteris.com FLEXNOC dbus_read_Structure_Module_dcluster1  4.5.1 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/dbus_read_Structure_Module_dcluster1_1.xml 
//  u_dbus_write_Structure_Module_dcluster1 arteris.com FLEXNOC dbus_write_Structure_Module_dcluster1 4.5.1 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/dbus_write_Structure_Module_dcluster1_1.xml 
// Magillem Release : 5.2021.1


module dncc1_subsys(
   input  wire          TM,
   input  wire          aclk,
   input  wire          arstn,
   input  wire [2:0]    dp_Link_n47_astResp001_to_Link_n47_asiResp001_rdcnt,
   input  wire          dp_Link_n47_astResp001_to_Link_n47_asiResp001_rxctl_pwronrstack,
   output wire          dp_Link_n47_astResp001_to_Link_n47_asiResp001_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n47_astResp001_to_Link_n47_asiResp001_rdptr,
   output wire          dp_Link_n47_astResp001_to_Link_n47_asiResp001_txctl_pwronrstack,
   input  wire          dp_Link_n47_astResp001_to_Link_n47_asiResp001_txctl_pwronrst,
   output wire [57:0]   dp_Link_n47_astResp001_to_Link_n47_asiResp001_data,
   output wire [2:0]    dp_Link_n47_astResp001_to_Link_n47_asiResp001_wrcnt,
   output wire [2:0]    dp_Link_n47_asi_to_Link_n47_ast_rdcnt,
   output wire          dp_Link_n47_asi_to_Link_n47_ast_rxctl_pwronrstack,
   input  wire          dp_Link_n47_asi_to_Link_n47_ast_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n47_asi_to_Link_n47_ast_rdptr,
   input  wire          dp_Link_n47_asi_to_Link_n47_ast_txctl_pwronrstack,
   output wire          dp_Link_n47_asi_to_Link_n47_ast_txctl_pwronrst,
   input  wire [57:0]   dp_Link_n47_asi_to_Link_n47_ast_data,
   input  wire [2:0]    dp_Link_n47_asi_to_Link_n47_ast_wrcnt,
   output wire [2:0]    dp_Link_n7_astResp001_to_Link_n7_asiResp001_rdcnt,
   output wire          dp_Link_n7_astResp001_to_Link_n7_asiResp001_rxctl_pwronrstack,
   input  wire          dp_Link_n7_astResp001_to_Link_n7_asiResp001_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n7_astResp001_to_Link_n7_asiResp001_rdptr,
   input  wire          dp_Link_n7_astResp001_to_Link_n7_asiResp001_txctl_pwronrstack,
   output wire          dp_Link_n7_astResp001_to_Link_n7_asiResp001_txctl_pwronrst,
   input  wire [1281:0] dp_Link_n7_astResp001_to_Link_n7_asiResp001_data,
   input  wire [2:0]    dp_Link_n7_astResp001_to_Link_n7_asiResp001_wrcnt,
   input  wire [2:0]    dp_Link_n7_asi_to_Link_n7_ast_r_rdcnt,
   input  wire          dp_Link_n7_asi_to_Link_n7_ast_r_rxctl_pwronrstack,
   output wire          dp_Link_n7_asi_to_Link_n7_ast_r_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n7_asi_to_Link_n7_ast_r_rdptr,
   output wire          dp_Link_n7_asi_to_Link_n7_ast_r_txctl_pwronrstack,
   input  wire          dp_Link_n7_asi_to_Link_n7_ast_r_txctl_pwronrst,
   output wire [128:0]  dp_Link_n7_asi_to_Link_n7_ast_r_data,
   output wire [2:0]    dp_Link_n7_asi_to_Link_n7_ast_r_wrcnt,
   output wire [2:0]    dp_Link_n6_astResp001_to_Link_n6_asiResp001_rdcnt,
   output wire          dp_Link_n6_astResp001_to_Link_n6_asiResp001_rxctl_pwronrstack,
   input  wire          dp_Link_n6_astResp001_to_Link_n6_asiResp001_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n6_astResp001_to_Link_n6_asiResp001_rdptr,
   input  wire          dp_Link_n6_astResp001_to_Link_n6_asiResp001_txctl_pwronrstack,
   output wire          dp_Link_n6_astResp001_to_Link_n6_asiResp001_txctl_pwronrst,
   input  wire [1281:0] dp_Link_n6_astResp001_to_Link_n6_asiResp001_data,
   input  wire [2:0]    dp_Link_n6_astResp001_to_Link_n6_asiResp001_wrcnt,
   input  wire [2:0]    dp_Link_n6_asi_to_Link_n6_ast_r_rdcnt,
   input  wire          dp_Link_n6_asi_to_Link_n6_ast_r_rxctl_pwronrstack,
   output wire          dp_Link_n6_asi_to_Link_n6_ast_r_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n6_asi_to_Link_n6_ast_r_rdptr,
   output wire          dp_Link_n6_asi_to_Link_n6_ast_r_txctl_pwronrstack,
   input  wire          dp_Link_n6_asi_to_Link_n6_ast_r_txctl_pwronrst,
   output wire [128:0]  dp_Link_n6_asi_to_Link_n6_ast_r_data,
   output wire [2:0]    dp_Link_n6_asi_to_Link_n6_ast_r_wrcnt,
   output wire [2:0]    dp_Link_n5_astResp001_to_Link_n5_asiResp001_rdcnt,
   output wire          dp_Link_n5_astResp001_to_Link_n5_asiResp001_rxctl_pwronrstack,
   input  wire          dp_Link_n5_astResp001_to_Link_n5_asiResp001_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n5_astResp001_to_Link_n5_asiResp001_rdptr,
   input  wire          dp_Link_n5_astResp001_to_Link_n5_asiResp001_txctl_pwronrstack,
   output wire          dp_Link_n5_astResp001_to_Link_n5_asiResp001_txctl_pwronrst,
   input  wire [1281:0] dp_Link_n5_astResp001_to_Link_n5_asiResp001_data,
   input  wire [2:0]    dp_Link_n5_astResp001_to_Link_n5_asiResp001_wrcnt,
   input  wire [2:0]    dp_Link_n5_asi_to_Link_n5_ast_r_rdcnt,
   input  wire          dp_Link_n5_asi_to_Link_n5_ast_r_rxctl_pwronrstack,
   output wire          dp_Link_n5_asi_to_Link_n5_ast_r_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n5_asi_to_Link_n5_ast_r_rdptr,
   output wire          dp_Link_n5_asi_to_Link_n5_ast_r_txctl_pwronrstack,
   input  wire          dp_Link_n5_asi_to_Link_n5_ast_r_txctl_pwronrst,
   output wire [128:0]  dp_Link_n5_asi_to_Link_n5_ast_r_data,
   output wire [2:0]    dp_Link_n5_asi_to_Link_n5_ast_r_wrcnt,
   output wire [2:0]    dp_Link_n4_astResp001_to_Link_n4_asiResp001_rdcnt,
   output wire          dp_Link_n4_astResp001_to_Link_n4_asiResp001_rxctl_pwronrstack,
   input  wire          dp_Link_n4_astResp001_to_Link_n4_asiResp001_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n4_astResp001_to_Link_n4_asiResp001_rdptr,
   input  wire          dp_Link_n4_astResp001_to_Link_n4_asiResp001_txctl_pwronrstack,
   output wire          dp_Link_n4_astResp001_to_Link_n4_asiResp001_txctl_pwronrst,
   input  wire [1281:0] dp_Link_n4_astResp001_to_Link_n4_asiResp001_data,
   input  wire [2:0]    dp_Link_n4_astResp001_to_Link_n4_asiResp001_wrcnt,
   input  wire [2:0]    dp_Link_n4_asi_to_Link_n4_ast_r_rdcnt,
   input  wire          dp_Link_n4_asi_to_Link_n4_ast_r_rxctl_pwronrstack,
   output wire          dp_Link_n4_asi_to_Link_n4_ast_r_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n4_asi_to_Link_n4_ast_r_rdptr,
   output wire          dp_Link_n4_asi_to_Link_n4_ast_r_txctl_pwronrstack,
   input  wire          dp_Link_n4_asi_to_Link_n4_ast_r_txctl_pwronrst,
   output wire [128:0]  dp_Link_n4_asi_to_Link_n4_ast_r_data,
   output wire [2:0]    dp_Link_n4_asi_to_Link_n4_ast_r_wrcnt,
   output wire [2:0]    dp_Link_n7_astResp_to_Link_n7_asiResp_rdcnt,
   output wire          dp_Link_n7_astResp_to_Link_n7_asiResp_rxctl_pwronrstack,
   input  wire          dp_Link_n7_astResp_to_Link_n7_asiResp_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n7_astResp_to_Link_n7_asiResp_rdptr,
   input  wire          dp_Link_n7_astResp_to_Link_n7_asiResp_txctl_pwronrstack,
   output wire          dp_Link_n7_astResp_to_Link_n7_asiResp_txctl_pwronrst,
   input  wire [125:0]  dp_Link_n7_astResp_to_Link_n7_asiResp_data,
   input  wire [2:0]    dp_Link_n7_astResp_to_Link_n7_asiResp_wrcnt,
   input  wire [2:0]    dp_Link_n7_asi_to_Link_n7_ast_w_rdcnt,
   input  wire          dp_Link_n7_asi_to_Link_n7_ast_w_rxctl_pwronrstack,
   output wire          dp_Link_n7_asi_to_Link_n7_ast_w_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n7_asi_to_Link_n7_ast_w_rdptr,
   output wire          dp_Link_n7_asi_to_Link_n7_ast_w_txctl_pwronrstack,
   input  wire          dp_Link_n7_asi_to_Link_n7_ast_w_txctl_pwronrst,
   output wire [1278:0] dp_Link_n7_asi_to_Link_n7_ast_w_data,
   output wire [2:0]    dp_Link_n7_asi_to_Link_n7_ast_w_wrcnt,
   output wire [2:0]    dp_Link_n6_astResp_to_Link_n6_asiResp_rdcnt,
   output wire          dp_Link_n6_astResp_to_Link_n6_asiResp_rxctl_pwronrstack,
   input  wire          dp_Link_n6_astResp_to_Link_n6_asiResp_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n6_astResp_to_Link_n6_asiResp_rdptr,
   input  wire          dp_Link_n6_astResp_to_Link_n6_asiResp_txctl_pwronrstack,
   output wire          dp_Link_n6_astResp_to_Link_n6_asiResp_txctl_pwronrst,
   input  wire [125:0]  dp_Link_n6_astResp_to_Link_n6_asiResp_data,
   input  wire [2:0]    dp_Link_n6_astResp_to_Link_n6_asiResp_wrcnt,
   input  wire [2:0]    dp_Link_n6_asi_to_Link_n6_ast_w_rdcnt,
   input  wire          dp_Link_n6_asi_to_Link_n6_ast_w_rxctl_pwronrstack,
   output wire          dp_Link_n6_asi_to_Link_n6_ast_w_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n6_asi_to_Link_n6_ast_w_rdptr,
   output wire          dp_Link_n6_asi_to_Link_n6_ast_w_txctl_pwronrstack,
   input  wire          dp_Link_n6_asi_to_Link_n6_ast_w_txctl_pwronrst,
   output wire [1278:0] dp_Link_n6_asi_to_Link_n6_ast_w_data,
   output wire [2:0]    dp_Link_n6_asi_to_Link_n6_ast_w_wrcnt,
   output wire [2:0]    dp_Link_n5_astResp_to_Link_n5_asiResp_rdcnt,
   output wire          dp_Link_n5_astResp_to_Link_n5_asiResp_rxctl_pwronrstack,
   input  wire          dp_Link_n5_astResp_to_Link_n5_asiResp_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n5_astResp_to_Link_n5_asiResp_rdptr,
   input  wire          dp_Link_n5_astResp_to_Link_n5_asiResp_txctl_pwronrstack,
   output wire          dp_Link_n5_astResp_to_Link_n5_asiResp_txctl_pwronrst,
   input  wire [125:0]  dp_Link_n5_astResp_to_Link_n5_asiResp_data,
   input  wire [2:0]    dp_Link_n5_astResp_to_Link_n5_asiResp_wrcnt,
   input  wire [2:0]    dp_Link_n5_asi_to_Link_n5_ast_w_rdcnt,
   input  wire          dp_Link_n5_asi_to_Link_n5_ast_w_rxctl_pwronrstack,
   output wire          dp_Link_n5_asi_to_Link_n5_ast_w_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n5_asi_to_Link_n5_ast_w_rdptr,
   output wire          dp_Link_n5_asi_to_Link_n5_ast_w_txctl_pwronrstack,
   input  wire          dp_Link_n5_asi_to_Link_n5_ast_w_txctl_pwronrst,
   output wire [1278:0] dp_Link_n5_asi_to_Link_n5_ast_w_data,
   output wire [2:0]    dp_Link_n5_asi_to_Link_n5_ast_w_wrcnt,
   output wire [2:0]    dp_Link_n4_astResp_to_Link_n4_asiResp_rdcnt,
   output wire          dp_Link_n4_astResp_to_Link_n4_asiResp_rxctl_pwronrstack,
   input  wire          dp_Link_n4_astResp_to_Link_n4_asiResp_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n4_astResp_to_Link_n4_asiResp_rdptr,
   input  wire          dp_Link_n4_astResp_to_Link_n4_asiResp_txctl_pwronrstack,
   output wire          dp_Link_n4_astResp_to_Link_n4_asiResp_txctl_pwronrst,
   input  wire [125:0]  dp_Link_n4_astResp_to_Link_n4_asiResp_data,
   input  wire [2:0]    dp_Link_n4_astResp_to_Link_n4_asiResp_wrcnt,
   input  wire [2:0]    dp_Link_n4_asi_to_Link_n4_ast_w_rdcnt,
   input  wire          dp_Link_n4_asi_to_Link_n4_ast_w_rxctl_pwronrstack,
   output wire          dp_Link_n4_asi_to_Link_n4_ast_w_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n4_asi_to_Link_n4_ast_w_rdptr,
   output wire          dp_Link_n4_asi_to_Link_n4_ast_w_txctl_pwronrstack,
   input  wire          dp_Link_n4_asi_to_Link_n4_ast_w_txctl_pwronrst,
   output wire [1278:0] dp_Link_n4_asi_to_Link_n4_ast_w_data,
   output wire [2:0]    dp_Link_n4_asi_to_Link_n4_ast_w_wrcnt 
);



wire               dnc_cbus_s0_arready;
wire               dnc_cbus_s0_rvalid;
wire               dnc_cbus_s0_rlast;
wire  [ 1    : 0 ] dnc_cbus_s0_rid;
wire  [ 31   : 0 ] dnc_cbus_s0_rdata;
wire  [ 1    : 0 ] dnc_cbus_s0_rresp;
wire               dnc_cbus_s0_awready;
wire               dnc_cbus_s0_wready;
wire  [ 1    : 0 ] dnc_cbus_s0_bid;
wire  [ 1    : 0 ] dnc_cbus_s0_bresp;
wire               dnc_cbus_s0_bvalid;
wire               dnc_dbus_s0_arready;
wire               dnc_dbus_s0_rvalid;
wire               dnc_dbus_s0_rlast;
wire  [ 6    : 0 ] dnc_dbus_s0_rid;
wire  [ 1023 : 0 ] dnc_dbus_s0_rdata;
wire  [ 3    : 0 ] dnc_dbus_s0_ruser;
wire  [ 1    : 0 ] dnc_dbus_s0_rresp;
wire               dnc_dbus_m0_arvalid;
wire  [ 19   : 0 ] dnc_dbus_m0_araddr;
wire  [ 6    : 0 ] dnc_dbus_m0_arid;
wire  [ 2    : 0 ] dnc_dbus_m0_arlen;
wire  [ 1    : 0 ] dnc_dbus_m0_arburst;
wire  [ 3    : 0 ] dnc_dbus_m0_arcache;
wire               dnc_dbus_m0_arlock;
wire  [ 2    : 0 ] dnc_dbus_m0_arprot;
wire  [ 2    : 0 ] dnc_dbus_m0_arsize;
wire  [ 3    : 0 ] dnc_dbus_m0_aruser;
wire               dnc_dbus_m0_rready;
wire  [ 19   : 0 ] dnc_dbus_m0_awaddr;
wire  [ 1    : 0 ] dnc_dbus_m0_awburst;
wire  [ 3    : 0 ] dnc_dbus_m0_awcache;
wire  [ 3    : 0 ] dnc_dbus_m0_awid;
wire  [ 2    : 0 ] dnc_dbus_m0_awlen;
wire               dnc_dbus_m0_awlock;
wire  [ 2    : 0 ] dnc_dbus_m0_awprot;
wire  [ 2    : 0 ] dnc_dbus_m0_awsize;
wire  [ 3    : 0 ] dnc_dbus_m0_awuser;
wire               dnc_dbus_m0_awvalid;
wire               dnc_dbus_m0_bready;
wire  [ 1023 : 0 ] dnc_dbus_m0_wdata;
wire               dnc_dbus_m0_wlast;
wire  [ 127  : 0 ] dnc_dbus_m0_wstrb;
wire               dnc_dbus_m0_wvalid;
wire               dnc_cbus_s0_arready_0;
wire               dnc_cbus_s0_rvalid_0;
wire               dnc_cbus_s0_rlast_0;
wire  [ 1    : 0 ] dnc_cbus_s0_rid_0;
wire  [ 31   : 0 ] dnc_cbus_s0_rdata_0;
wire  [ 1    : 0 ] dnc_cbus_s0_rresp_0;
wire               dnc_cbus_s0_awready_0;
wire               dnc_cbus_s0_wready_0;
wire  [ 1    : 0 ] dnc_cbus_s0_bid_0;
wire  [ 1    : 0 ] dnc_cbus_s0_bresp_0;
wire               dnc_cbus_s0_bvalid_0;
wire               dnc_dbus_s0_arready_0;
wire               dnc_dbus_s0_rvalid_0;
wire               dnc_dbus_s0_rlast_0;
wire  [ 6    : 0 ] dnc_dbus_s0_rid_0;
wire  [ 1023 : 0 ] dnc_dbus_s0_rdata_0;
wire  [ 3    : 0 ] dnc_dbus_s0_ruser_0;
wire  [ 1    : 0 ] dnc_dbus_s0_rresp_0;
wire               dnc_dbus_m0_arvalid_0;
wire  [ 19   : 0 ] dnc_dbus_m0_araddr_0;
wire  [ 6    : 0 ] dnc_dbus_m0_arid_0;
wire  [ 2    : 0 ] dnc_dbus_m0_arlen_0;
wire  [ 1    : 0 ] dnc_dbus_m0_arburst_0;
wire  [ 3    : 0 ] dnc_dbus_m0_arcache_0;
wire               dnc_dbus_m0_arlock_0;
wire  [ 2    : 0 ] dnc_dbus_m0_arprot_0;
wire  [ 2    : 0 ] dnc_dbus_m0_arsize_0;
wire  [ 3    : 0 ] dnc_dbus_m0_aruser_0;
wire               dnc_dbus_m0_rready_0;
wire  [ 19   : 0 ] dnc_dbus_m0_awaddr_0;
wire  [ 1    : 0 ] dnc_dbus_m0_awburst_0;
wire  [ 3    : 0 ] dnc_dbus_m0_awcache_0;
wire  [ 3    : 0 ] dnc_dbus_m0_awid_0;
wire  [ 2    : 0 ] dnc_dbus_m0_awlen_0;
wire               dnc_dbus_m0_awlock_0;
wire  [ 2    : 0 ] dnc_dbus_m0_awprot_0;
wire  [ 2    : 0 ] dnc_dbus_m0_awsize_0;
wire  [ 3    : 0 ] dnc_dbus_m0_awuser_0;
wire               dnc_dbus_m0_awvalid_0;
wire               dnc_dbus_m0_bready_0;
wire  [ 1023 : 0 ] dnc_dbus_m0_wdata_0;
wire               dnc_dbus_m0_wlast_0;
wire  [ 127  : 0 ] dnc_dbus_m0_wstrb_0;
wire               dnc_dbus_m0_wvalid_0;
wire               dnc_cbus_s0_arready_1;
wire               dnc_cbus_s0_rvalid_1;
wire               dnc_cbus_s0_rlast_1;
wire  [ 1    : 0 ] dnc_cbus_s0_rid_1;
wire  [ 31   : 0 ] dnc_cbus_s0_rdata_1;
wire  [ 1    : 0 ] dnc_cbus_s0_rresp_1;
wire               dnc_cbus_s0_awready_1;
wire               dnc_cbus_s0_wready_1;
wire  [ 1    : 0 ] dnc_cbus_s0_bid_1;
wire  [ 1    : 0 ] dnc_cbus_s0_bresp_1;
wire               dnc_cbus_s0_bvalid_1;
wire               dnc_dbus_s0_arready_1;
wire               dnc_dbus_s0_rvalid_1;
wire               dnc_dbus_s0_rlast_1;
wire  [ 6    : 0 ] dnc_dbus_s0_rid_1;
wire  [ 1023 : 0 ] dnc_dbus_s0_rdata_1;
wire  [ 3    : 0 ] dnc_dbus_s0_ruser_1;
wire  [ 1    : 0 ] dnc_dbus_s0_rresp_1;
wire               dnc_dbus_m0_arvalid_1;
wire  [ 19   : 0 ] dnc_dbus_m0_araddr_1;
wire  [ 6    : 0 ] dnc_dbus_m0_arid_1;
wire  [ 2    : 0 ] dnc_dbus_m0_arlen_1;
wire  [ 1    : 0 ] dnc_dbus_m0_arburst_1;
wire  [ 3    : 0 ] dnc_dbus_m0_arcache_1;
wire               dnc_dbus_m0_arlock_1;
wire  [ 2    : 0 ] dnc_dbus_m0_arprot_1;
wire  [ 2    : 0 ] dnc_dbus_m0_arsize_1;
wire  [ 3    : 0 ] dnc_dbus_m0_aruser_1;
wire               dnc_dbus_m0_rready_1;
wire  [ 19   : 0 ] dnc_dbus_m0_awaddr_1;
wire  [ 1    : 0 ] dnc_dbus_m0_awburst_1;
wire  [ 3    : 0 ] dnc_dbus_m0_awcache_1;
wire  [ 3    : 0 ] dnc_dbus_m0_awid_1;
wire  [ 2    : 0 ] dnc_dbus_m0_awlen_1;
wire               dnc_dbus_m0_awlock_1;
wire  [ 2    : 0 ] dnc_dbus_m0_awprot_1;
wire  [ 2    : 0 ] dnc_dbus_m0_awsize_1;
wire  [ 3    : 0 ] dnc_dbus_m0_awuser_1;
wire               dnc_dbus_m0_awvalid_1;
wire               dnc_dbus_m0_bready_1;
wire  [ 1023 : 0 ] dnc_dbus_m0_wdata_1;
wire               dnc_dbus_m0_wlast_1;
wire  [ 127  : 0 ] dnc_dbus_m0_wstrb_1;
wire               dnc_dbus_m0_wvalid_1;
wire               dnc_cbus_s0_arready_2;
wire               dnc_cbus_s0_rvalid_2;
wire               dnc_cbus_s0_rlast_2;
wire  [ 1    : 0 ] dnc_cbus_s0_rid_2;
wire  [ 31   : 0 ] dnc_cbus_s0_rdata_2;
wire  [ 1    : 0 ] dnc_cbus_s0_rresp_2;
wire               dnc_cbus_s0_awready_2;
wire               dnc_cbus_s0_wready_2;
wire  [ 1    : 0 ] dnc_cbus_s0_bid_2;
wire  [ 1    : 0 ] dnc_cbus_s0_bresp_2;
wire               dnc_cbus_s0_bvalid_2;
wire               dnc_dbus_s0_arready_2;
wire               dnc_dbus_s0_rvalid_2;
wire               dnc_dbus_s0_rlast_2;
wire  [ 6    : 0 ] dnc_dbus_s0_rid_2;
wire  [ 1023 : 0 ] dnc_dbus_s0_rdata_2;
wire  [ 3    : 0 ] dnc_dbus_s0_ruser_2;
wire  [ 1    : 0 ] dnc_dbus_s0_rresp_2;
wire               dnc_dbus_m0_arvalid_2;
wire  [ 19   : 0 ] dnc_dbus_m0_araddr_2;
wire  [ 6    : 0 ] dnc_dbus_m0_arid_2;
wire  [ 2    : 0 ] dnc_dbus_m0_arlen_2;
wire  [ 1    : 0 ] dnc_dbus_m0_arburst_2;
wire  [ 3    : 0 ] dnc_dbus_m0_arcache_2;
wire               dnc_dbus_m0_arlock_2;
wire  [ 2    : 0 ] dnc_dbus_m0_arprot_2;
wire  [ 2    : 0 ] dnc_dbus_m0_arsize_2;
wire  [ 3    : 0 ] dnc_dbus_m0_aruser_2;
wire               dnc_dbus_m0_rready_2;
wire  [ 19   : 0 ] dnc_dbus_m0_awaddr_2;
wire  [ 1    : 0 ] dnc_dbus_m0_awburst_2;
wire  [ 3    : 0 ] dnc_dbus_m0_awcache_2;
wire  [ 3    : 0 ] dnc_dbus_m0_awid_2;
wire  [ 2    : 0 ] dnc_dbus_m0_awlen_2;
wire               dnc_dbus_m0_awlock_2;
wire  [ 2    : 0 ] dnc_dbus_m0_awprot_2;
wire  [ 2    : 0 ] dnc_dbus_m0_awsize_2;
wire  [ 3    : 0 ] dnc_dbus_m0_awuser_2;
wire               dnc_dbus_m0_awvalid_2;
wire               dnc_dbus_m0_bready_2;
wire  [ 1023 : 0 ] dnc_dbus_m0_wdata_2;
wire               dnc_dbus_m0_wlast_2;
wire  [ 127  : 0 ] dnc_dbus_m0_wstrb_2;
wire               dnc_dbus_m0_wvalid_2;
wire               cbus_s0_arready;
wire               cbus_s0_rvalid;
wire               cbus_s0_rlast;
wire  [ 1    : 0 ] cbus_s0_rid;
wire  [ 31   : 0 ] cbus_s0_rdata;
wire  [ 1    : 0 ] cbus_s0_rresp;
wire               cbus_s0_awready;
wire               cbus_s0_wready;
wire  [ 1    : 0 ] cbus_s0_bid;
wire  [ 1    : 0 ] cbus_s0_bresp;
wire               cbus_s0_bvalid;
wire               dbus_s0_arready;
wire               dbus_s0_rvalid;
wire               dbus_s0_rlast;
wire  [ 6    : 0 ] dbus_s0_rid;
wire  [ 1023 : 0 ] dbus_s0_rdata;
wire  [ 3    : 0 ] dbus_s0_ruser;
wire  [ 1    : 0 ] dbus_s0_rresp;
wire               dbus_s1_arready;
wire               dbus_s1_rvalid;
wire               dbus_s1_rlast;
wire  [ 6    : 0 ] dbus_s1_rid;
wire  [ 1023 : 0 ] dbus_s1_rdata;
wire  [ 3    : 0 ] dbus_s1_ruser;
wire  [ 1    : 0 ] dbus_s1_rresp;
wire               dbus_s2_arready;
wire               dbus_s2_rvalid;
wire               dbus_s2_rlast;
wire  [ 6    : 0 ] dbus_s2_rid;
wire  [ 1023 : 0 ] dbus_s2_rdata;
wire  [ 3    : 0 ] dbus_s2_ruser;
wire  [ 1    : 0 ] dbus_s2_rresp;
wire               dbus_s3_arready;
wire               dbus_s3_rvalid;
wire               dbus_s3_rlast;
wire  [ 6    : 0 ] dbus_s3_rid;
wire  [ 1023 : 0 ] dbus_s3_rdata;
wire  [ 3    : 0 ] dbus_s3_ruser;
wire  [ 1    : 0 ] dbus_s3_rresp;
wire               dbus_m0_arvalid;
wire  [ 36   : 0 ] dbus_m0_araddr;
wire  [ 6    : 0 ] dbus_m0_arid;
wire  [ 4    : 0 ] dbus_m0_arlen;
wire  [ 1    : 0 ] dbus_m0_arburst;
wire  [ 3    : 0 ] dbus_m0_arcache;
wire               dbus_m0_arlock;
wire  [ 2    : 0 ] dbus_m0_arprot;
wire  [ 2    : 0 ] dbus_m0_arsize;
wire               dbus_m0_rready;
wire               dbus_m1_arvalid;
wire  [ 36   : 0 ] dbus_m1_araddr;
wire  [ 6    : 0 ] dbus_m1_arid;
wire  [ 4    : 0 ] dbus_m1_arlen;
wire  [ 1    : 0 ] dbus_m1_arburst;
wire  [ 3    : 0 ] dbus_m1_arcache;
wire               dbus_m1_arlock;
wire  [ 2    : 0 ] dbus_m1_arprot;
wire  [ 2    : 0 ] dbus_m1_arsize;
wire               dbus_m1_rready;
wire               dbus_m2_arvalid;
wire  [ 36   : 0 ] dbus_m2_araddr;
wire  [ 6    : 0 ] dbus_m2_arid;
wire  [ 4    : 0 ] dbus_m2_arlen;
wire  [ 1    : 0 ] dbus_m2_arburst;
wire  [ 3    : 0 ] dbus_m2_arcache;
wire               dbus_m2_arlock;
wire  [ 2    : 0 ] dbus_m2_arprot;
wire  [ 2    : 0 ] dbus_m2_arsize;
wire               dbus_m2_rready;
wire               dbus_m3_arvalid;
wire  [ 36   : 0 ] dbus_m3_araddr;
wire  [ 6    : 0 ] dbus_m3_arid;
wire  [ 4    : 0 ] dbus_m3_arlen;
wire  [ 1    : 0 ] dbus_m3_arburst;
wire  [ 3    : 0 ] dbus_m3_arcache;
wire               dbus_m3_arlock;
wire  [ 2    : 0 ] dbus_m3_arprot;
wire  [ 2    : 0 ] dbus_m3_arsize;
wire               dbus_m3_rready;
wire               dnc7_ctrl_R_Ready;
wire               dnc7_ctrl_B_Ready;
wire  [ 2    : 0 ] dnc7_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dnc7_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dnc7_ctrl_Aw_Burst;
wire               dnc7_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dnc7_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dnc7_ctrl_Aw_Len;
wire               dnc7_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dnc7_ctrl_Aw_Id;
wire  [ 2    : 0 ] dnc7_ctrl_Aw_Size;
wire               dnc7_ctrl_W_Last;
wire               dnc7_ctrl_W_Valid;
wire  [ 3    : 0 ] dnc7_ctrl_W_Strb;
wire  [ 31   : 0 ] dnc7_ctrl_W_Data;
wire  [ 2    : 0 ] dnc7_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dnc7_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dnc7_ctrl_Ar_Burst;
wire               dnc7_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dnc7_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dnc7_ctrl_Ar_Len;
wire               dnc7_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dnc7_ctrl_Ar_Id;
wire  [ 2    : 0 ] dnc7_ctrl_Ar_Size;
wire               dnc6_ctrl_R_Ready;
wire               dnc6_ctrl_B_Ready;
wire  [ 2    : 0 ] dnc6_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dnc6_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dnc6_ctrl_Aw_Burst;
wire               dnc6_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dnc6_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dnc6_ctrl_Aw_Len;
wire               dnc6_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dnc6_ctrl_Aw_Id;
wire  [ 2    : 0 ] dnc6_ctrl_Aw_Size;
wire               dnc6_ctrl_W_Last;
wire               dnc6_ctrl_W_Valid;
wire  [ 3    : 0 ] dnc6_ctrl_W_Strb;
wire  [ 31   : 0 ] dnc6_ctrl_W_Data;
wire  [ 2    : 0 ] dnc6_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dnc6_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dnc6_ctrl_Ar_Burst;
wire               dnc6_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dnc6_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dnc6_ctrl_Ar_Len;
wire               dnc6_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dnc6_ctrl_Ar_Id;
wire  [ 2    : 0 ] dnc6_ctrl_Ar_Size;
wire               dnc5_ctrl_R_Ready;
wire               dnc5_ctrl_B_Ready;
wire  [ 2    : 0 ] dnc5_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dnc5_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dnc5_ctrl_Aw_Burst;
wire               dnc5_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dnc5_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dnc5_ctrl_Aw_Len;
wire               dnc5_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dnc5_ctrl_Aw_Id;
wire  [ 2    : 0 ] dnc5_ctrl_Aw_Size;
wire               dnc5_ctrl_W_Last;
wire               dnc5_ctrl_W_Valid;
wire  [ 3    : 0 ] dnc5_ctrl_W_Strb;
wire  [ 31   : 0 ] dnc5_ctrl_W_Data;
wire  [ 2    : 0 ] dnc5_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dnc5_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dnc5_ctrl_Ar_Burst;
wire               dnc5_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dnc5_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dnc5_ctrl_Ar_Len;
wire               dnc5_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dnc5_ctrl_Ar_Id;
wire  [ 2    : 0 ] dnc5_ctrl_Ar_Size;
wire               dnc4_ctrl_R_Ready;
wire               dnc4_ctrl_B_Ready;
wire  [ 2    : 0 ] dnc4_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dnc4_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dnc4_ctrl_Aw_Burst;
wire               dnc4_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dnc4_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dnc4_ctrl_Aw_Len;
wire               dnc4_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dnc4_ctrl_Aw_Id;
wire  [ 2    : 0 ] dnc4_ctrl_Aw_Size;
wire               dnc4_ctrl_W_Last;
wire               dnc4_ctrl_W_Valid;
wire  [ 3    : 0 ] dnc4_ctrl_W_Strb;
wire  [ 31   : 0 ] dnc4_ctrl_W_Data;
wire  [ 2    : 0 ] dnc4_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dnc4_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dnc4_ctrl_Ar_Burst;
wire               dnc4_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dnc4_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dnc4_ctrl_Ar_Len;
wire               dnc4_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dnc4_ctrl_Ar_Id;
wire  [ 2    : 0 ] dnc4_ctrl_Ar_Size;
wire               dcluster1_ctrl_R_Ready;
wire               dcluster1_ctrl_B_Ready;
wire  [ 2    : 0 ] dcluster1_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dcluster1_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dcluster1_ctrl_Aw_Burst;
wire               dcluster1_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dcluster1_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dcluster1_ctrl_Aw_Len;
wire               dcluster1_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dcluster1_ctrl_Aw_Id;
wire  [ 2    : 0 ] dcluster1_ctrl_Aw_Size;
wire               dcluster1_ctrl_W_Last;
wire               dcluster1_ctrl_W_Valid;
wire  [ 3    : 0 ] dcluster1_ctrl_W_Strb;
wire  [ 31   : 0 ] dcluster1_ctrl_W_Data;
wire  [ 2    : 0 ] dcluster1_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dcluster1_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dcluster1_ctrl_Ar_Burst;
wire               dcluster1_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dcluster1_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dcluster1_ctrl_Ar_Len;
wire               dcluster1_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dcluster1_ctrl_Ar_Id;
wire  [ 2    : 0 ] dcluster1_ctrl_Ar_Size;
wire               dnc7_slv_r_R_Ready;
wire  [ 2    : 0 ] dnc7_slv_r_Ar_Prot;
wire  [ 19   : 0 ] dnc7_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dnc7_slv_r_Ar_Burst;
wire               dnc7_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dnc7_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dnc7_slv_r_Ar_Len;
wire               dnc7_slv_r_Ar_Valid;
wire  [ 6    : 0 ] dnc7_slv_r_Ar_Id;
wire  [ 2    : 0 ] dnc7_slv_r_Ar_Size;
wire               dnc7_r_R_Last;
wire  [ 1    : 0 ] dnc7_r_R_Resp;
wire               dnc7_r_R_Valid;
wire  [ 3    : 0 ] dnc7_r_R_User;
wire  [ 1023 : 0 ] dnc7_r_R_Data;
wire  [ 6    : 0 ] dnc7_r_R_Id;
wire               dnc7_r_Ar_Ready;
wire               dnc6_slv_r_R_Ready;
wire  [ 2    : 0 ] dnc6_slv_r_Ar_Prot;
wire  [ 19   : 0 ] dnc6_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dnc6_slv_r_Ar_Burst;
wire               dnc6_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dnc6_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dnc6_slv_r_Ar_Len;
wire               dnc6_slv_r_Ar_Valid;
wire  [ 6    : 0 ] dnc6_slv_r_Ar_Id;
wire  [ 2    : 0 ] dnc6_slv_r_Ar_Size;
wire               dnc6_r_R_Last;
wire  [ 1    : 0 ] dnc6_r_R_Resp;
wire               dnc6_r_R_Valid;
wire  [ 3    : 0 ] dnc6_r_R_User;
wire  [ 1023 : 0 ] dnc6_r_R_Data;
wire  [ 6    : 0 ] dnc6_r_R_Id;
wire               dnc6_r_Ar_Ready;
wire               dnc5_slv_r_R_Ready;
wire  [ 2    : 0 ] dnc5_slv_r_Ar_Prot;
wire  [ 19   : 0 ] dnc5_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dnc5_slv_r_Ar_Burst;
wire               dnc5_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dnc5_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dnc5_slv_r_Ar_Len;
wire               dnc5_slv_r_Ar_Valid;
wire  [ 6    : 0 ] dnc5_slv_r_Ar_Id;
wire  [ 2    : 0 ] dnc5_slv_r_Ar_Size;
wire               dnc5_r_R_Last;
wire  [ 1    : 0 ] dnc5_r_R_Resp;
wire               dnc5_r_R_Valid;
wire  [ 3    : 0 ] dnc5_r_R_User;
wire  [ 1023 : 0 ] dnc5_r_R_Data;
wire  [ 6    : 0 ] dnc5_r_R_Id;
wire               dnc5_r_Ar_Ready;
wire               dnc4_slv_r_R_Ready;
wire  [ 2    : 0 ] dnc4_slv_r_Ar_Prot;
wire  [ 19   : 0 ] dnc4_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dnc4_slv_r_Ar_Burst;
wire               dnc4_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dnc4_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dnc4_slv_r_Ar_Len;
wire               dnc4_slv_r_Ar_Valid;
wire  [ 6    : 0 ] dnc4_slv_r_Ar_Id;
wire  [ 2    : 0 ] dnc4_slv_r_Ar_Size;
wire               dnc4_r_R_Last;
wire  [ 1    : 0 ] dnc4_r_R_Resp;
wire               dnc4_r_R_Valid;
wire  [ 3    : 0 ] dnc4_r_R_User;
wire  [ 1023 : 0 ] dnc4_r_R_Data;
wire  [ 6    : 0 ] dnc4_r_R_Id;
wire               dnc4_r_Ar_Ready;
wire               dcluster1_cc3_slv_r_R_Ready;
wire  [ 2    : 0 ] dcluster1_cc3_slv_r_Ar_Prot;
wire  [ 36   : 0 ] dcluster1_cc3_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dcluster1_cc3_slv_r_Ar_Burst;
wire               dcluster1_cc3_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dcluster1_cc3_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dcluster1_cc3_slv_r_Ar_Len;
wire               dcluster1_cc3_slv_r_Ar_Valid;
wire  [ 3    : 0 ] dcluster1_cc3_slv_r_Ar_User;
wire  [ 6    : 0 ] dcluster1_cc3_slv_r_Ar_Id;
wire  [ 2    : 0 ] dcluster1_cc3_slv_r_Ar_Size;
wire               dcluster1_cc3_r_R_Last;
wire  [ 1    : 0 ] dcluster1_cc3_r_R_Resp;
wire               dcluster1_cc3_r_R_Valid;
wire  [ 3    : 0 ] dcluster1_cc3_r_R_User;
wire  [ 1023 : 0 ] dcluster1_cc3_r_R_Data;
wire  [ 6    : 0 ] dcluster1_cc3_r_R_Id;
wire               dcluster1_cc3_r_Ar_Ready;
wire               dcluster1_cc2_slv_r_R_Ready;
wire  [ 2    : 0 ] dcluster1_cc2_slv_r_Ar_Prot;
wire  [ 36   : 0 ] dcluster1_cc2_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dcluster1_cc2_slv_r_Ar_Burst;
wire               dcluster1_cc2_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dcluster1_cc2_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dcluster1_cc2_slv_r_Ar_Len;
wire               dcluster1_cc2_slv_r_Ar_Valid;
wire  [ 3    : 0 ] dcluster1_cc2_slv_r_Ar_User;
wire  [ 6    : 0 ] dcluster1_cc2_slv_r_Ar_Id;
wire  [ 2    : 0 ] dcluster1_cc2_slv_r_Ar_Size;
wire               dcluster1_cc2_r_R_Last;
wire  [ 1    : 0 ] dcluster1_cc2_r_R_Resp;
wire               dcluster1_cc2_r_R_Valid;
wire  [ 3    : 0 ] dcluster1_cc2_r_R_User;
wire  [ 1023 : 0 ] dcluster1_cc2_r_R_Data;
wire  [ 6    : 0 ] dcluster1_cc2_r_R_Id;
wire               dcluster1_cc2_r_Ar_Ready;
wire               dcluster1_cc1_slv_r_R_Ready;
wire  [ 2    : 0 ] dcluster1_cc1_slv_r_Ar_Prot;
wire  [ 36   : 0 ] dcluster1_cc1_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dcluster1_cc1_slv_r_Ar_Burst;
wire               dcluster1_cc1_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dcluster1_cc1_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dcluster1_cc1_slv_r_Ar_Len;
wire               dcluster1_cc1_slv_r_Ar_Valid;
wire  [ 3    : 0 ] dcluster1_cc1_slv_r_Ar_User;
wire  [ 6    : 0 ] dcluster1_cc1_slv_r_Ar_Id;
wire  [ 2    : 0 ] dcluster1_cc1_slv_r_Ar_Size;
wire               dcluster1_cc1_r_R_Last;
wire  [ 1    : 0 ] dcluster1_cc1_r_R_Resp;
wire               dcluster1_cc1_r_R_Valid;
wire  [ 3    : 0 ] dcluster1_cc1_r_R_User;
wire  [ 1023 : 0 ] dcluster1_cc1_r_R_Data;
wire  [ 6    : 0 ] dcluster1_cc1_r_R_Id;
wire               dcluster1_cc1_r_Ar_Ready;
wire               dcluster1_cc0_slv_r_R_Ready;
wire  [ 2    : 0 ] dcluster1_cc0_slv_r_Ar_Prot;
wire  [ 36   : 0 ] dcluster1_cc0_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dcluster1_cc0_slv_r_Ar_Burst;
wire               dcluster1_cc0_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dcluster1_cc0_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dcluster1_cc0_slv_r_Ar_Len;
wire               dcluster1_cc0_slv_r_Ar_Valid;
wire  [ 3    : 0 ] dcluster1_cc0_slv_r_Ar_User;
wire  [ 6    : 0 ] dcluster1_cc0_slv_r_Ar_Id;
wire  [ 2    : 0 ] dcluster1_cc0_slv_r_Ar_Size;
wire               dcluster1_cc0_r_R_Last;
wire  [ 1    : 0 ] dcluster1_cc0_r_R_Resp;
wire               dcluster1_cc0_r_R_Valid;
wire  [ 3    : 0 ] dcluster1_cc0_r_R_User;
wire  [ 1023 : 0 ] dcluster1_cc0_r_R_Data;
wire  [ 6    : 0 ] dcluster1_cc0_r_R_Id;
wire               dcluster1_cc0_r_Ar_Ready;
wire  [ 1    : 0 ] dnc7_w_B_Resp;
wire               dnc7_w_B_Valid;
wire  [ 3    : 0 ] dnc7_w_B_User;
wire  [ 6    : 0 ] dnc7_w_B_Id;
wire               dnc7_w_Aw_Ready;
wire               dnc7_w_W_Ready;
wire  [ 1    : 0 ] dnc6_w_B_Resp;
wire               dnc6_w_B_Valid;
wire  [ 3    : 0 ] dnc6_w_B_User;
wire  [ 6    : 0 ] dnc6_w_B_Id;
wire               dnc6_w_Aw_Ready;
wire               dnc6_w_W_Ready;
wire  [ 1    : 0 ] dnc5_w_B_Resp;
wire               dnc5_w_B_Valid;
wire  [ 3    : 0 ] dnc5_w_B_User;
wire  [ 6    : 0 ] dnc5_w_B_Id;
wire               dnc5_w_Aw_Ready;
wire               dnc5_w_W_Ready;
wire  [ 1    : 0 ] dnc4_w_B_Resp;
wire               dnc4_w_B_Valid;
wire  [ 3    : 0 ] dnc4_w_B_User;
wire  [ 6    : 0 ] dnc4_w_B_Id;
wire               dnc4_w_Aw_Ready;
wire               dnc4_w_W_Ready;

dnc #(
      .C_AXI_DATA_WIDTH_P   (32),
      .C_AXI_ADDR_WIDTH_P   (24),
      .C_AXI_STRB_WIDTH_P   (4),
      .C_AXI_ID_WIDTH_P     (2),
      .C_AXI_USER_WIDTH_P   (0),
      .C_AXI_LEN_WIDTH_P    (2),
      .DNC_AXI_DATA_WIDTH_P (1024),
      .DNC_AXI_ADDR_WIDTH_P (20),
      .DNC_AXI_STRB_WIDTH_P (128),
      .DNC_AXI_ID_WIDTH_P   (7),
      .DNC_AXI_USER_WIDTH_P (4),
      .DNC_AXI_LEN_WIDTH_P  (3) 
      ) 
u_dnc0 (
      .clk(                    aclk                   ),
      .arstn(                  arstn                  ),
      .dnc_cbus_s0_arready(    dnc_cbus_s0_arready    ),
      .dnc_cbus_s0_arvalid(    dnc4_ctrl_Ar_Valid     ),
      .dnc_cbus_s0_araddr(     dnc4_ctrl_Ar_Addr      ),
      .dnc_cbus_s0_arid(       dnc4_ctrl_Ar_Id        ),
      .dnc_cbus_s0_arlen(      dnc4_ctrl_Ar_Len       ),
      .dnc_cbus_s0_arburst(    dnc4_ctrl_Ar_Burst     ),
      .dnc_cbus_s0_arcache(    dnc4_ctrl_Ar_Cache     ),
      .dnc_cbus_s0_arlock(     dnc4_ctrl_Ar_Lock      ),
      .dnc_cbus_s0_arprot(     dnc4_ctrl_Ar_Prot      ),
      .dnc_cbus_s0_arsize(     dnc4_ctrl_Ar_Size      ),
      .dnc_cbus_s0_rready(     dnc4_ctrl_R_Ready      ),
      .dnc_cbus_s0_rvalid(     dnc_cbus_s0_rvalid     ),
      .dnc_cbus_s0_rlast(      dnc_cbus_s0_rlast      ),
      .dnc_cbus_s0_rid(        dnc_cbus_s0_rid        ),
      .dnc_cbus_s0_rdata(      dnc_cbus_s0_rdata      ),
      .dnc_cbus_s0_rresp(      dnc_cbus_s0_rresp      ),
      .dnc_cbus_s0_awready(    dnc_cbus_s0_awready    ),
      .dnc_cbus_s0_awvalid(    dnc4_ctrl_Aw_Valid     ),
      .dnc_cbus_s0_awaddr(     dnc4_ctrl_Aw_Addr      ),
      .dnc_cbus_s0_awid(       dnc4_ctrl_Aw_Id        ),
      .dnc_cbus_s0_awlen(      dnc4_ctrl_Aw_Len       ),
      .dnc_cbus_s0_awburst(    dnc4_ctrl_Aw_Burst     ),
      .dnc_cbus_s0_awcache(    dnc4_ctrl_Aw_Cache     ),
      .dnc_cbus_s0_awlock(     dnc4_ctrl_Aw_Lock      ),
      .dnc_cbus_s0_awprot(     dnc4_ctrl_Aw_Prot      ),
      .dnc_cbus_s0_awsize(     dnc4_ctrl_Aw_Size      ),
      .dnc_cbus_s0_wready(     dnc_cbus_s0_wready     ),
      .dnc_cbus_s0_wvalid(     dnc4_ctrl_W_Valid      ),
      .dnc_cbus_s0_wdata(      dnc4_ctrl_W_Data       ),
      .dnc_cbus_s0_wlast(      dnc4_ctrl_W_Last       ),
      .dnc_cbus_s0_wstrb(      dnc4_ctrl_W_Strb       ),
      .dnc_cbus_s0_bready(     dnc4_ctrl_B_Ready      ),
      .dnc_cbus_s0_bid(        dnc_cbus_s0_bid        ),
      .dnc_cbus_s0_bresp(      dnc_cbus_s0_bresp      ),
      .dnc_cbus_s0_bvalid(     dnc_cbus_s0_bvalid     ),
      .dnc_dbus_s0_arready(    dnc_dbus_s0_arready    ),
      .dnc_dbus_s0_arvalid(    dnc4_slv_r_Ar_Valid    ),
      .dnc_dbus_s0_araddr(     dnc4_slv_r_Ar_Addr     ),
      .dnc_dbus_s0_arid(       dnc4_slv_r_Ar_Id       ),
      .dnc_dbus_s0_arlen(      dnc4_slv_r_Ar_Len[2:0] ),
      .dnc_dbus_s0_arburst(    dnc4_slv_r_Ar_Burst    ),
      .dnc_dbus_s0_arcache(    dnc4_slv_r_Ar_Cache    ),
      .dnc_dbus_s0_arlock(     dnc4_slv_r_Ar_Lock     ),
      .dnc_dbus_s0_arprot(     dnc4_slv_r_Ar_Prot     ),
      .dnc_dbus_s0_arsize(     dnc4_slv_r_Ar_Size     ),
      .dnc_dbus_s0_aruser(     4'b0000                ),
      .dnc_dbus_s0_rready(     dnc4_slv_r_R_Ready     ),
      .dnc_dbus_s0_rvalid(     dnc_dbus_s0_rvalid     ),
      .dnc_dbus_s0_rlast(      dnc_dbus_s0_rlast      ),
      .dnc_dbus_s0_rid(        dnc_dbus_s0_rid        ),
      .dnc_dbus_s0_rdata(      dnc_dbus_s0_rdata      ),
      .dnc_dbus_s0_ruser(      dnc_dbus_s0_ruser      ),
      .dnc_dbus_s0_rresp(      dnc_dbus_s0_rresp      ),
      .dnc_dbus_m0_arready(    dnc4_r_Ar_Ready        ),
      .dnc_dbus_m0_arvalid(    dnc_dbus_m0_arvalid    ),
      .dnc_dbus_m0_araddr(     dnc_dbus_m0_araddr     ),
      .dnc_dbus_m0_arid(       dnc_dbus_m0_arid       ),
      .dnc_dbus_m0_arlen(      dnc_dbus_m0_arlen      ),
      .dnc_dbus_m0_arburst(    dnc_dbus_m0_arburst    ),
      .dnc_dbus_m0_arcache(    dnc_dbus_m0_arcache    ),
      .dnc_dbus_m0_arlock(     dnc_dbus_m0_arlock     ),
      .dnc_dbus_m0_arprot(     dnc_dbus_m0_arprot     ),
      .dnc_dbus_m0_arsize(     dnc_dbus_m0_arsize     ),
      .dnc_dbus_m0_aruser(     dnc_dbus_m0_aruser     ),
      .dnc_dbus_m0_rready(     dnc_dbus_m0_rready     ),
      .dnc_dbus_m0_rvalid(     dnc4_r_R_Valid         ),
      .dnc_dbus_m0_rlast(      dnc4_r_R_Last          ),
      .dnc_dbus_m0_rid(        dnc4_r_R_Id            ),
      .dnc_dbus_m0_rdata(      dnc4_r_R_Data          ),
      .dnc_dbus_m0_ruser(      dnc4_r_R_User          ),
      .dnc_dbus_m0_rresp(      dnc4_r_R_Resp          ),
      .dnc_dbus_m0_awready(    dnc4_w_Aw_Ready        ),
      .dnc_dbus_m0_awaddr(     dnc_dbus_m0_awaddr     ),
      .dnc_dbus_m0_awburst(    dnc_dbus_m0_awburst    ),
      .dnc_dbus_m0_awcache(    dnc_dbus_m0_awcache    ),
      .dnc_dbus_m0_awid(       dnc_dbus_m0_awid       ),
      .dnc_dbus_m0_awlen(      dnc_dbus_m0_awlen      ),
      .dnc_dbus_m0_awlock(     dnc_dbus_m0_awlock     ),
      .dnc_dbus_m0_awprot(     dnc_dbus_m0_awprot     ),
      .dnc_dbus_m0_awsize(     dnc_dbus_m0_awsize     ),
      .dnc_dbus_m0_awuser(     dnc_dbus_m0_awuser     ),
      .dnc_dbus_m0_awvalid(    dnc_dbus_m0_awvalid    ),
      .dnc_dbus_m0_bready(     dnc_dbus_m0_bready     ),
      .dnc_dbus_m0_bid(        dnc4_w_B_Id            ),
      .dnc_dbus_m0_bresp(      dnc4_w_B_Resp          ),
      .dnc_dbus_m0_buser(      dnc4_w_B_User          ),
      .dnc_dbus_m0_bvalid(     dnc4_w_B_Valid         ),
      .dnc_dbus_m0_wready(     dnc4_w_W_Ready         ),
      .dnc_dbus_m0_wdata(      dnc_dbus_m0_wdata      ),
      .dnc_dbus_m0_wlast(      dnc_dbus_m0_wlast      ),
      .dnc_dbus_m0_wstrb(      dnc_dbus_m0_wstrb      ),
      .dnc_dbus_m0_wvalid(     dnc_dbus_m0_wvalid     ) 
      );


dnc #(
      .C_AXI_DATA_WIDTH_P   (32),
      .C_AXI_ADDR_WIDTH_P   (24),
      .C_AXI_STRB_WIDTH_P   (4),
      .C_AXI_ID_WIDTH_P     (2),
      .C_AXI_USER_WIDTH_P   (0),
      .C_AXI_LEN_WIDTH_P    (2),
      .DNC_AXI_DATA_WIDTH_P (1024),
      .DNC_AXI_ADDR_WIDTH_P (20),
      .DNC_AXI_STRB_WIDTH_P (128),
      .DNC_AXI_ID_WIDTH_P   (7),
      .DNC_AXI_USER_WIDTH_P (4),
      .DNC_AXI_LEN_WIDTH_P  (3) 
      ) 
u_dnc1 (
      .clk(                    aclk                   ),
      .arstn(                  arstn                  ),
      .dnc_cbus_s0_arready(    dnc_cbus_s0_arready_0  ),
      .dnc_cbus_s0_arvalid(    dnc5_ctrl_Ar_Valid     ),
      .dnc_cbus_s0_araddr(     dnc5_ctrl_Ar_Addr      ),
      .dnc_cbus_s0_arid(       dnc5_ctrl_Ar_Id        ),
      .dnc_cbus_s0_arlen(      dnc5_ctrl_Ar_Len       ),
      .dnc_cbus_s0_arburst(    dnc5_ctrl_Ar_Burst     ),
      .dnc_cbus_s0_arcache(    dnc5_ctrl_Ar_Cache     ),
      .dnc_cbus_s0_arlock(     dnc5_ctrl_Ar_Lock      ),
      .dnc_cbus_s0_arprot(     dnc5_ctrl_Ar_Prot      ),
      .dnc_cbus_s0_arsize(     dnc5_ctrl_Ar_Size      ),
      .dnc_cbus_s0_rready(     dnc5_ctrl_R_Ready      ),
      .dnc_cbus_s0_rvalid(     dnc_cbus_s0_rvalid_0   ),
      .dnc_cbus_s0_rlast(      dnc_cbus_s0_rlast_0    ),
      .dnc_cbus_s0_rid(        dnc_cbus_s0_rid_0      ),
      .dnc_cbus_s0_rdata(      dnc_cbus_s0_rdata_0    ),
      .dnc_cbus_s0_rresp(      dnc_cbus_s0_rresp_0    ),
      .dnc_cbus_s0_awready(    dnc_cbus_s0_awready_0  ),
      .dnc_cbus_s0_awvalid(    dnc5_ctrl_Aw_Valid     ),
      .dnc_cbus_s0_awaddr(     dnc5_ctrl_Aw_Addr      ),
      .dnc_cbus_s0_awid(       dnc5_ctrl_Aw_Id        ),
      .dnc_cbus_s0_awlen(      dnc5_ctrl_Aw_Len       ),
      .dnc_cbus_s0_awburst(    dnc5_ctrl_Aw_Burst     ),
      .dnc_cbus_s0_awcache(    dnc5_ctrl_Aw_Cache     ),
      .dnc_cbus_s0_awlock(     dnc5_ctrl_Aw_Lock      ),
      .dnc_cbus_s0_awprot(     dnc5_ctrl_Aw_Prot      ),
      .dnc_cbus_s0_awsize(     dnc5_ctrl_Aw_Size      ),
      .dnc_cbus_s0_wready(     dnc_cbus_s0_wready_0   ),
      .dnc_cbus_s0_wvalid(     dnc5_ctrl_W_Valid      ),
      .dnc_cbus_s0_wdata(      dnc5_ctrl_W_Data       ),
      .dnc_cbus_s0_wlast(      dnc5_ctrl_W_Last       ),
      .dnc_cbus_s0_wstrb(      dnc5_ctrl_W_Strb       ),
      .dnc_cbus_s0_bready(     dnc5_ctrl_B_Ready      ),
      .dnc_cbus_s0_bid(        dnc_cbus_s0_bid_0      ),
      .dnc_cbus_s0_bresp(      dnc_cbus_s0_bresp_0    ),
      .dnc_cbus_s0_bvalid(     dnc_cbus_s0_bvalid_0   ),
      .dnc_dbus_s0_arready(    dnc_dbus_s0_arready_0  ),
      .dnc_dbus_s0_arvalid(    dnc5_slv_r_Ar_Valid    ),
      .dnc_dbus_s0_araddr(     dnc5_slv_r_Ar_Addr     ),
      .dnc_dbus_s0_arid(       dnc5_slv_r_Ar_Id       ),
      .dnc_dbus_s0_arlen(      dnc5_slv_r_Ar_Len[2:0] ),
      .dnc_dbus_s0_arburst(    dnc5_slv_r_Ar_Burst    ),
      .dnc_dbus_s0_arcache(    dnc5_slv_r_Ar_Cache    ),
      .dnc_dbus_s0_arlock(     dnc5_slv_r_Ar_Lock     ),
      .dnc_dbus_s0_arprot(     dnc5_slv_r_Ar_Prot     ),
      .dnc_dbus_s0_arsize(     dnc5_slv_r_Ar_Size     ),
      .dnc_dbus_s0_aruser(     4'b0000                ),
      .dnc_dbus_s0_rready(     dnc5_slv_r_R_Ready     ),
      .dnc_dbus_s0_rvalid(     dnc_dbus_s0_rvalid_0   ),
      .dnc_dbus_s0_rlast(      dnc_dbus_s0_rlast_0    ),
      .dnc_dbus_s0_rid(        dnc_dbus_s0_rid_0      ),
      .dnc_dbus_s0_rdata(      dnc_dbus_s0_rdata_0    ),
      .dnc_dbus_s0_ruser(      dnc_dbus_s0_ruser_0    ),
      .dnc_dbus_s0_rresp(      dnc_dbus_s0_rresp_0    ),
      .dnc_dbus_m0_arready(    dnc5_r_Ar_Ready        ),
      .dnc_dbus_m0_arvalid(    dnc_dbus_m0_arvalid_0  ),
      .dnc_dbus_m0_araddr(     dnc_dbus_m0_araddr_0   ),
      .dnc_dbus_m0_arid(       dnc_dbus_m0_arid_0     ),
      .dnc_dbus_m0_arlen(      dnc_dbus_m0_arlen_0    ),
      .dnc_dbus_m0_arburst(    dnc_dbus_m0_arburst_0  ),
      .dnc_dbus_m0_arcache(    dnc_dbus_m0_arcache_0  ),
      .dnc_dbus_m0_arlock(     dnc_dbus_m0_arlock_0   ),
      .dnc_dbus_m0_arprot(     dnc_dbus_m0_arprot_0   ),
      .dnc_dbus_m0_arsize(     dnc_dbus_m0_arsize_0   ),
      .dnc_dbus_m0_aruser(     dnc_dbus_m0_aruser_0   ),
      .dnc_dbus_m0_rready(     dnc_dbus_m0_rready_0   ),
      .dnc_dbus_m0_rvalid(     dnc5_r_R_Valid         ),
      .dnc_dbus_m0_rlast(      dnc5_r_R_Last          ),
      .dnc_dbus_m0_rid(        dnc5_r_R_Id            ),
      .dnc_dbus_m0_rdata(      dnc5_r_R_Data          ),
      .dnc_dbus_m0_ruser(      dnc5_r_R_User          ),
      .dnc_dbus_m0_rresp(      dnc5_r_R_Resp          ),
      .dnc_dbus_m0_awready(    dnc5_w_Aw_Ready        ),
      .dnc_dbus_m0_awaddr(     dnc_dbus_m0_awaddr_0   ),
      .dnc_dbus_m0_awburst(    dnc_dbus_m0_awburst_0  ),
      .dnc_dbus_m0_awcache(    dnc_dbus_m0_awcache_0  ),
      .dnc_dbus_m0_awid(       dnc_dbus_m0_awid_0     ),
      .dnc_dbus_m0_awlen(      dnc_dbus_m0_awlen_0    ),
      .dnc_dbus_m0_awlock(     dnc_dbus_m0_awlock_0   ),
      .dnc_dbus_m0_awprot(     dnc_dbus_m0_awprot_0   ),
      .dnc_dbus_m0_awsize(     dnc_dbus_m0_awsize_0   ),
      .dnc_dbus_m0_awuser(     dnc_dbus_m0_awuser_0   ),
      .dnc_dbus_m0_awvalid(    dnc_dbus_m0_awvalid_0  ),
      .dnc_dbus_m0_bready(     dnc_dbus_m0_bready_0   ),
      .dnc_dbus_m0_bid(        dnc5_w_B_Id            ),
      .dnc_dbus_m0_bresp(      dnc5_w_B_Resp          ),
      .dnc_dbus_m0_buser(      dnc5_w_B_User          ),
      .dnc_dbus_m0_bvalid(     dnc5_w_B_Valid         ),
      .dnc_dbus_m0_wready(     dnc5_w_W_Ready         ),
      .dnc_dbus_m0_wdata(      dnc_dbus_m0_wdata_0    ),
      .dnc_dbus_m0_wlast(      dnc_dbus_m0_wlast_0    ),
      .dnc_dbus_m0_wstrb(      dnc_dbus_m0_wstrb_0    ),
      .dnc_dbus_m0_wvalid(     dnc_dbus_m0_wvalid_0   ) 
      );


dnc #(
      .C_AXI_DATA_WIDTH_P   (32),
      .C_AXI_ADDR_WIDTH_P   (24),
      .C_AXI_STRB_WIDTH_P   (4),
      .C_AXI_ID_WIDTH_P     (2),
      .C_AXI_USER_WIDTH_P   (0),
      .C_AXI_LEN_WIDTH_P    (2),
      .DNC_AXI_DATA_WIDTH_P (1024),
      .DNC_AXI_ADDR_WIDTH_P (20),
      .DNC_AXI_STRB_WIDTH_P (128),
      .DNC_AXI_ID_WIDTH_P   (7),
      .DNC_AXI_USER_WIDTH_P (4),
      .DNC_AXI_LEN_WIDTH_P  (3) 
      ) 
u_dnc2 (
      .clk(                    aclk                   ),
      .arstn(                  arstn                  ),
      .dnc_cbus_s0_arready(    dnc_cbus_s0_arready_1  ),
      .dnc_cbus_s0_arvalid(    dnc6_ctrl_Ar_Valid     ),
      .dnc_cbus_s0_araddr(     dnc6_ctrl_Ar_Addr      ),
      .dnc_cbus_s0_arid(       dnc6_ctrl_Ar_Id        ),
      .dnc_cbus_s0_arlen(      dnc6_ctrl_Ar_Len       ),
      .dnc_cbus_s0_arburst(    dnc6_ctrl_Ar_Burst     ),
      .dnc_cbus_s0_arcache(    dnc6_ctrl_Ar_Cache     ),
      .dnc_cbus_s0_arlock(     dnc6_ctrl_Ar_Lock      ),
      .dnc_cbus_s0_arprot(     dnc6_ctrl_Ar_Prot      ),
      .dnc_cbus_s0_arsize(     dnc6_ctrl_Ar_Size      ),
      .dnc_cbus_s0_rready(     dnc6_ctrl_R_Ready      ),
      .dnc_cbus_s0_rvalid(     dnc_cbus_s0_rvalid_1   ),
      .dnc_cbus_s0_rlast(      dnc_cbus_s0_rlast_1    ),
      .dnc_cbus_s0_rid(        dnc_cbus_s0_rid_1      ),
      .dnc_cbus_s0_rdata(      dnc_cbus_s0_rdata_1    ),
      .dnc_cbus_s0_rresp(      dnc_cbus_s0_rresp_1    ),
      .dnc_cbus_s0_awready(    dnc_cbus_s0_awready_1  ),
      .dnc_cbus_s0_awvalid(    dnc6_ctrl_Aw_Valid     ),
      .dnc_cbus_s0_awaddr(     dnc6_ctrl_Aw_Addr      ),
      .dnc_cbus_s0_awid(       dnc6_ctrl_Aw_Id        ),
      .dnc_cbus_s0_awlen(      dnc6_ctrl_Aw_Len       ),
      .dnc_cbus_s0_awburst(    dnc6_ctrl_Aw_Burst     ),
      .dnc_cbus_s0_awcache(    dnc6_ctrl_Aw_Cache     ),
      .dnc_cbus_s0_awlock(     dnc6_ctrl_Aw_Lock      ),
      .dnc_cbus_s0_awprot(     dnc6_ctrl_Aw_Prot      ),
      .dnc_cbus_s0_awsize(     dnc6_ctrl_Aw_Size      ),
      .dnc_cbus_s0_wready(     dnc_cbus_s0_wready_1   ),
      .dnc_cbus_s0_wvalid(     dnc6_ctrl_W_Valid      ),
      .dnc_cbus_s0_wdata(      dnc6_ctrl_W_Data       ),
      .dnc_cbus_s0_wlast(      dnc6_ctrl_W_Last       ),
      .dnc_cbus_s0_wstrb(      dnc6_ctrl_W_Strb       ),
      .dnc_cbus_s0_bready(     dnc6_ctrl_B_Ready      ),
      .dnc_cbus_s0_bid(        dnc_cbus_s0_bid_1      ),
      .dnc_cbus_s0_bresp(      dnc_cbus_s0_bresp_1    ),
      .dnc_cbus_s0_bvalid(     dnc_cbus_s0_bvalid_1   ),
      .dnc_dbus_s0_arready(    dnc_dbus_s0_arready_1  ),
      .dnc_dbus_s0_arvalid(    dnc6_slv_r_Ar_Valid    ),
      .dnc_dbus_s0_araddr(     dnc6_slv_r_Ar_Addr     ),
      .dnc_dbus_s0_arid(       dnc6_slv_r_Ar_Id       ),
      .dnc_dbus_s0_arlen(      dnc6_slv_r_Ar_Len[2:0] ),
      .dnc_dbus_s0_arburst(    dnc6_slv_r_Ar_Burst    ),
      .dnc_dbus_s0_arcache(    dnc6_slv_r_Ar_Cache    ),
      .dnc_dbus_s0_arlock(     dnc6_slv_r_Ar_Lock     ),
      .dnc_dbus_s0_arprot(     dnc6_slv_r_Ar_Prot     ),
      .dnc_dbus_s0_arsize(     dnc6_slv_r_Ar_Size     ),
      .dnc_dbus_s0_aruser(     4'b0000                ),
      .dnc_dbus_s0_rready(     dnc6_slv_r_R_Ready     ),
      .dnc_dbus_s0_rvalid(     dnc_dbus_s0_rvalid_1   ),
      .dnc_dbus_s0_rlast(      dnc_dbus_s0_rlast_1    ),
      .dnc_dbus_s0_rid(        dnc_dbus_s0_rid_1      ),
      .dnc_dbus_s0_rdata(      dnc_dbus_s0_rdata_1    ),
      .dnc_dbus_s0_ruser(      dnc_dbus_s0_ruser_1    ),
      .dnc_dbus_s0_rresp(      dnc_dbus_s0_rresp_1    ),
      .dnc_dbus_m0_arready(    dnc6_r_Ar_Ready        ),
      .dnc_dbus_m0_arvalid(    dnc_dbus_m0_arvalid_1  ),
      .dnc_dbus_m0_araddr(     dnc_dbus_m0_araddr_1   ),
      .dnc_dbus_m0_arid(       dnc_dbus_m0_arid_1     ),
      .dnc_dbus_m0_arlen(      dnc_dbus_m0_arlen_1    ),
      .dnc_dbus_m0_arburst(    dnc_dbus_m0_arburst_1  ),
      .dnc_dbus_m0_arcache(    dnc_dbus_m0_arcache_1  ),
      .dnc_dbus_m0_arlock(     dnc_dbus_m0_arlock_1   ),
      .dnc_dbus_m0_arprot(     dnc_dbus_m0_arprot_1   ),
      .dnc_dbus_m0_arsize(     dnc_dbus_m0_arsize_1   ),
      .dnc_dbus_m0_aruser(     dnc_dbus_m0_aruser_1   ),
      .dnc_dbus_m0_rready(     dnc_dbus_m0_rready_1   ),
      .dnc_dbus_m0_rvalid(     dnc6_r_R_Valid         ),
      .dnc_dbus_m0_rlast(      dnc6_r_R_Last          ),
      .dnc_dbus_m0_rid(        dnc6_r_R_Id            ),
      .dnc_dbus_m0_rdata(      dnc6_r_R_Data          ),
      .dnc_dbus_m0_ruser(      dnc6_r_R_User          ),
      .dnc_dbus_m0_rresp(      dnc6_r_R_Resp          ),
      .dnc_dbus_m0_awready(    dnc6_w_Aw_Ready        ),
      .dnc_dbus_m0_awaddr(     dnc_dbus_m0_awaddr_1   ),
      .dnc_dbus_m0_awburst(    dnc_dbus_m0_awburst_1  ),
      .dnc_dbus_m0_awcache(    dnc_dbus_m0_awcache_1  ),
      .dnc_dbus_m0_awid(       dnc_dbus_m0_awid_1     ),
      .dnc_dbus_m0_awlen(      dnc_dbus_m0_awlen_1    ),
      .dnc_dbus_m0_awlock(     dnc_dbus_m0_awlock_1   ),
      .dnc_dbus_m0_awprot(     dnc_dbus_m0_awprot_1   ),
      .dnc_dbus_m0_awsize(     dnc_dbus_m0_awsize_1   ),
      .dnc_dbus_m0_awuser(     dnc_dbus_m0_awuser_1   ),
      .dnc_dbus_m0_awvalid(    dnc_dbus_m0_awvalid_1  ),
      .dnc_dbus_m0_bready(     dnc_dbus_m0_bready_1   ),
      .dnc_dbus_m0_bid(        dnc6_w_B_Id            ),
      .dnc_dbus_m0_bresp(      dnc6_w_B_Resp          ),
      .dnc_dbus_m0_buser(      dnc6_w_B_User          ),
      .dnc_dbus_m0_bvalid(     dnc6_w_B_Valid         ),
      .dnc_dbus_m0_wready(     dnc6_w_W_Ready         ),
      .dnc_dbus_m0_wdata(      dnc_dbus_m0_wdata_1    ),
      .dnc_dbus_m0_wlast(      dnc_dbus_m0_wlast_1    ),
      .dnc_dbus_m0_wstrb(      dnc_dbus_m0_wstrb_1    ),
      .dnc_dbus_m0_wvalid(     dnc_dbus_m0_wvalid_1   ) 
      );


dnc #(
      .C_AXI_DATA_WIDTH_P   (32),
      .C_AXI_ADDR_WIDTH_P   (24),
      .C_AXI_STRB_WIDTH_P   (4),
      .C_AXI_ID_WIDTH_P     (2),
      .C_AXI_USER_WIDTH_P   (0),
      .C_AXI_LEN_WIDTH_P    (2),
      .DNC_AXI_DATA_WIDTH_P (1024),
      .DNC_AXI_ADDR_WIDTH_P (20),
      .DNC_AXI_STRB_WIDTH_P (128),
      .DNC_AXI_ID_WIDTH_P   (7),
      .DNC_AXI_USER_WIDTH_P (4),
      .DNC_AXI_LEN_WIDTH_P  (3) 
      ) 
u_dnc3 (
      .clk(                    aclk                   ),
      .arstn(                  arstn                  ),
      .dnc_cbus_s0_arready(    dnc_cbus_s0_arready_2  ),
      .dnc_cbus_s0_arvalid(    dnc7_ctrl_Ar_Valid     ),
      .dnc_cbus_s0_araddr(     dnc7_ctrl_Ar_Addr      ),
      .dnc_cbus_s0_arid(       dnc7_ctrl_Ar_Id        ),
      .dnc_cbus_s0_arlen(      dnc7_ctrl_Ar_Len       ),
      .dnc_cbus_s0_arburst(    dnc7_ctrl_Ar_Burst     ),
      .dnc_cbus_s0_arcache(    dnc7_ctrl_Ar_Cache     ),
      .dnc_cbus_s0_arlock(     dnc7_ctrl_Ar_Lock      ),
      .dnc_cbus_s0_arprot(     dnc7_ctrl_Ar_Prot      ),
      .dnc_cbus_s0_arsize(     dnc7_ctrl_Ar_Size      ),
      .dnc_cbus_s0_rready(     dnc7_ctrl_R_Ready      ),
      .dnc_cbus_s0_rvalid(     dnc_cbus_s0_rvalid_2   ),
      .dnc_cbus_s0_rlast(      dnc_cbus_s0_rlast_2    ),
      .dnc_cbus_s0_rid(        dnc_cbus_s0_rid_2      ),
      .dnc_cbus_s0_rdata(      dnc_cbus_s0_rdata_2    ),
      .dnc_cbus_s0_rresp(      dnc_cbus_s0_rresp_2    ),
      .dnc_cbus_s0_awready(    dnc_cbus_s0_awready_2  ),
      .dnc_cbus_s0_awvalid(    dnc7_ctrl_Aw_Valid     ),
      .dnc_cbus_s0_awaddr(     dnc7_ctrl_Aw_Addr      ),
      .dnc_cbus_s0_awid(       dnc7_ctrl_Aw_Id        ),
      .dnc_cbus_s0_awlen(      dnc7_ctrl_Aw_Len       ),
      .dnc_cbus_s0_awburst(    dnc7_ctrl_Aw_Burst     ),
      .dnc_cbus_s0_awcache(    dnc7_ctrl_Aw_Cache     ),
      .dnc_cbus_s0_awlock(     dnc7_ctrl_Aw_Lock      ),
      .dnc_cbus_s0_awprot(     dnc7_ctrl_Aw_Prot      ),
      .dnc_cbus_s0_awsize(     dnc7_ctrl_Aw_Size      ),
      .dnc_cbus_s0_wready(     dnc_cbus_s0_wready_2   ),
      .dnc_cbus_s0_wvalid(     dnc7_ctrl_W_Valid      ),
      .dnc_cbus_s0_wdata(      dnc7_ctrl_W_Data       ),
      .dnc_cbus_s0_wlast(      dnc7_ctrl_W_Last       ),
      .dnc_cbus_s0_wstrb(      dnc7_ctrl_W_Strb       ),
      .dnc_cbus_s0_bready(     dnc7_ctrl_B_Ready      ),
      .dnc_cbus_s0_bid(        dnc_cbus_s0_bid_2      ),
      .dnc_cbus_s0_bresp(      dnc_cbus_s0_bresp_2    ),
      .dnc_cbus_s0_bvalid(     dnc_cbus_s0_bvalid_2   ),
      .dnc_dbus_s0_arready(    dnc_dbus_s0_arready_2  ),
      .dnc_dbus_s0_arvalid(    dnc7_slv_r_Ar_Valid    ),
      .dnc_dbus_s0_araddr(     dnc7_slv_r_Ar_Addr     ),
      .dnc_dbus_s0_arid(       dnc7_slv_r_Ar_Id       ),
      .dnc_dbus_s0_arlen(      dnc7_slv_r_Ar_Len[2:0] ),
      .dnc_dbus_s0_arburst(    dnc7_slv_r_Ar_Burst    ),
      .dnc_dbus_s0_arcache(    dnc7_slv_r_Ar_Cache    ),
      .dnc_dbus_s0_arlock(     dnc7_slv_r_Ar_Lock     ),
      .dnc_dbus_s0_arprot(     dnc7_slv_r_Ar_Prot     ),
      .dnc_dbus_s0_arsize(     dnc7_slv_r_Ar_Size     ),
      .dnc_dbus_s0_aruser(     4'b0000                ),
      .dnc_dbus_s0_rready(     dnc7_slv_r_R_Ready     ),
      .dnc_dbus_s0_rvalid(     dnc_dbus_s0_rvalid_2   ),
      .dnc_dbus_s0_rlast(      dnc_dbus_s0_rlast_2    ),
      .dnc_dbus_s0_rid(        dnc_dbus_s0_rid_2      ),
      .dnc_dbus_s0_rdata(      dnc_dbus_s0_rdata_2    ),
      .dnc_dbus_s0_ruser(      dnc_dbus_s0_ruser_2    ),
      .dnc_dbus_s0_rresp(      dnc_dbus_s0_rresp_2    ),
      .dnc_dbus_m0_arready(    dnc7_r_Ar_Ready        ),
      .dnc_dbus_m0_arvalid(    dnc_dbus_m0_arvalid_2  ),
      .dnc_dbus_m0_araddr(     dnc_dbus_m0_araddr_2   ),
      .dnc_dbus_m0_arid(       dnc_dbus_m0_arid_2     ),
      .dnc_dbus_m0_arlen(      dnc_dbus_m0_arlen_2    ),
      .dnc_dbus_m0_arburst(    dnc_dbus_m0_arburst_2  ),
      .dnc_dbus_m0_arcache(    dnc_dbus_m0_arcache_2  ),
      .dnc_dbus_m0_arlock(     dnc_dbus_m0_arlock_2   ),
      .dnc_dbus_m0_arprot(     dnc_dbus_m0_arprot_2   ),
      .dnc_dbus_m0_arsize(     dnc_dbus_m0_arsize_2   ),
      .dnc_dbus_m0_aruser(     dnc_dbus_m0_aruser_2   ),
      .dnc_dbus_m0_rready(     dnc_dbus_m0_rready_2   ),
      .dnc_dbus_m0_rvalid(     dnc7_r_R_Valid         ),
      .dnc_dbus_m0_rlast(      dnc7_r_R_Last          ),
      .dnc_dbus_m0_rid(        dnc7_r_R_Id            ),
      .dnc_dbus_m0_rdata(      dnc7_r_R_Data          ),
      .dnc_dbus_m0_ruser(      dnc7_r_R_User          ),
      .dnc_dbus_m0_rresp(      dnc7_r_R_Resp          ),
      .dnc_dbus_m0_awready(    dnc7_w_Aw_Ready        ),
      .dnc_dbus_m0_awaddr(     dnc_dbus_m0_awaddr_2   ),
      .dnc_dbus_m0_awburst(    dnc_dbus_m0_awburst_2  ),
      .dnc_dbus_m0_awcache(    dnc_dbus_m0_awcache_2  ),
      .dnc_dbus_m0_awid(       dnc_dbus_m0_awid_2     ),
      .dnc_dbus_m0_awlen(      dnc_dbus_m0_awlen_2    ),
      .dnc_dbus_m0_awlock(     dnc_dbus_m0_awlock_2   ),
      .dnc_dbus_m0_awprot(     dnc_dbus_m0_awprot_2   ),
      .dnc_dbus_m0_awsize(     dnc_dbus_m0_awsize_2   ),
      .dnc_dbus_m0_awuser(     dnc_dbus_m0_awuser_2   ),
      .dnc_dbus_m0_awvalid(    dnc_dbus_m0_awvalid_2  ),
      .dnc_dbus_m0_bready(     dnc_dbus_m0_bready_2   ),
      .dnc_dbus_m0_bid(        dnc7_w_B_Id            ),
      .dnc_dbus_m0_bresp(      dnc7_w_B_Resp          ),
      .dnc_dbus_m0_buser(      dnc7_w_B_User          ),
      .dnc_dbus_m0_bvalid(     dnc7_w_B_Valid         ),
      .dnc_dbus_m0_wready(     dnc7_w_W_Ready         ),
      .dnc_dbus_m0_wdata(      dnc_dbus_m0_wdata_2    ),
      .dnc_dbus_m0_wlast(      dnc_dbus_m0_wlast_2    ),
      .dnc_dbus_m0_wstrb(      dnc_dbus_m0_wstrb_2    ),
      .dnc_dbus_m0_wvalid(     dnc_dbus_m0_wvalid_2   ) 
      );


atom_dcluster_dcc #(
      .C_AXI_DATA_WIDTH_P (32),
      .C_AXI_ADDR_WIDTH_P (24),
      .C_AXI_STRB_WIDTH_P (4),
      .C_AXI_ID_WIDTH_P   (2),
      .C_AXI_USER_WIDTH_P (0),
      .C_AXI_LEN_WIDTH_P  (2),
      .D_AXI_DATA_WIDTH_P (1024),
      .D_AXI_ADDR_WIDTH_P (37),
      .D_AXI_STRB_WIDTH_P (128),
      .D_AXI_ID_WIDTH_P   (7),
      .D_AXI_USER_WIDTH_P (4),
      .D_AXI_LEN_WIDTH_P  (5) 
      ) 
u_dnc_cc (
      .clk(                aclk                         ),
      .arstn(              arstn                        ),
      .cbus_s0_arready(    cbus_s0_arready              ),
      .cbus_s0_arvalid(    dcluster1_ctrl_Ar_Valid      ),
      .cbus_s0_araddr(     dcluster1_ctrl_Ar_Addr       ),
      .cbus_s0_arid(       dcluster1_ctrl_Ar_Id         ),
      .cbus_s0_arlen(      dcluster1_ctrl_Ar_Len        ),
      .cbus_s0_arburst(    dcluster1_ctrl_Ar_Burst      ),
      .cbus_s0_arcache(    dcluster1_ctrl_Ar_Cache      ),
      .cbus_s0_arlock(     dcluster1_ctrl_Ar_Lock       ),
      .cbus_s0_arprot(     dcluster1_ctrl_Ar_Prot       ),
      .cbus_s0_arsize(     dcluster1_ctrl_Ar_Size       ),
      .cbus_s0_rready(     dcluster1_ctrl_R_Ready       ),
      .cbus_s0_rvalid(     cbus_s0_rvalid               ),
      .cbus_s0_rlast(      cbus_s0_rlast                ),
      .cbus_s0_rid(        cbus_s0_rid                  ),
      .cbus_s0_rdata(      cbus_s0_rdata                ),
      .cbus_s0_rresp(      cbus_s0_rresp                ),
      .cbus_s0_awready(    cbus_s0_awready              ),
      .cbus_s0_awvalid(    dcluster1_ctrl_Aw_Valid      ),
      .cbus_s0_awaddr(     dcluster1_ctrl_Aw_Addr       ),
      .cbus_s0_awid(       dcluster1_ctrl_Aw_Id         ),
      .cbus_s0_awlen(      dcluster1_ctrl_Aw_Len        ),
      .cbus_s0_awburst(    dcluster1_ctrl_Aw_Burst      ),
      .cbus_s0_awcache(    dcluster1_ctrl_Aw_Cache      ),
      .cbus_s0_awlock(     dcluster1_ctrl_Aw_Lock       ),
      .cbus_s0_awprot(     dcluster1_ctrl_Aw_Prot       ),
      .cbus_s0_awsize(     dcluster1_ctrl_Aw_Size       ),
      .cbus_s0_wready(     cbus_s0_wready               ),
      .cbus_s0_wvalid(     dcluster1_ctrl_W_Valid       ),
      .cbus_s0_wdata(      dcluster1_ctrl_W_Data        ),
      .cbus_s0_wlast(      dcluster1_ctrl_W_Last        ),
      .cbus_s0_wstrb(      dcluster1_ctrl_W_Strb        ),
      .cbus_s0_bready(     dcluster1_ctrl_B_Ready       ),
      .cbus_s0_bid(        cbus_s0_bid                  ),
      .cbus_s0_bresp(      cbus_s0_bresp                ),
      .cbus_s0_bvalid(     cbus_s0_bvalid               ),
      .dbus_s0_arready(    dbus_s0_arready              ),
      .dbus_s0_arvalid(    dcluster1_cc0_slv_r_Ar_Valid ),
      .dbus_s0_araddr(     dcluster1_cc0_slv_r_Ar_Addr  ),
      .dbus_s0_arid(       dcluster1_cc0_slv_r_Ar_Id    ),
      .dbus_s0_arlen(      dcluster1_cc0_slv_r_Ar_Len   ),
      .dbus_s0_arburst(    dcluster1_cc0_slv_r_Ar_Burst ),
      .dbus_s0_arcache(    dcluster1_cc0_slv_r_Ar_Cache ),
      .dbus_s0_arlock(     dcluster1_cc0_slv_r_Ar_Lock  ),
      .dbus_s0_arprot(     dcluster1_cc0_slv_r_Ar_Prot  ),
      .dbus_s0_arsize(     dcluster1_cc0_slv_r_Ar_Size  ),
      .dbus_s0_aruser(     dcluster1_cc0_slv_r_Ar_User  ),
      .dbus_s0_rready(     dcluster1_cc0_slv_r_R_Ready  ),
      .dbus_s0_rvalid(     dbus_s0_rvalid               ),
      .dbus_s0_rlast(      dbus_s0_rlast                ),
      .dbus_s0_rid(        dbus_s0_rid                  ),
      .dbus_s0_rdata(      dbus_s0_rdata                ),
      .dbus_s0_ruser(      dbus_s0_ruser                ),
      .dbus_s0_rresp(      dbus_s0_rresp                ),
      .dbus_s1_arready(    dbus_s1_arready              ),
      .dbus_s1_arvalid(    dcluster1_cc1_slv_r_Ar_Valid ),
      .dbus_s1_araddr(     dcluster1_cc1_slv_r_Ar_Addr  ),
      .dbus_s1_arid(       dcluster1_cc1_slv_r_Ar_Id    ),
      .dbus_s1_arlen(      dcluster1_cc1_slv_r_Ar_Len   ),
      .dbus_s1_arburst(    dcluster1_cc1_slv_r_Ar_Burst ),
      .dbus_s1_arcache(    dcluster1_cc1_slv_r_Ar_Cache ),
      .dbus_s1_arlock(     dcluster1_cc1_slv_r_Ar_Lock  ),
      .dbus_s1_arprot(     dcluster1_cc1_slv_r_Ar_Prot  ),
      .dbus_s1_arsize(     dcluster1_cc1_slv_r_Ar_Size  ),
      .dbus_s1_aruser(     dcluster1_cc1_slv_r_Ar_User  ),
      .dbus_s1_rready(     dcluster1_cc1_slv_r_R_Ready  ),
      .dbus_s1_rvalid(     dbus_s1_rvalid               ),
      .dbus_s1_rlast(      dbus_s1_rlast                ),
      .dbus_s1_rid(        dbus_s1_rid                  ),
      .dbus_s1_rdata(      dbus_s1_rdata                ),
      .dbus_s1_ruser(      dbus_s1_ruser                ),
      .dbus_s1_rresp(      dbus_s1_rresp                ),
      .dbus_s2_arready(    dbus_s2_arready              ),
      .dbus_s2_arvalid(    dcluster1_cc2_slv_r_Ar_Valid ),
      .dbus_s2_araddr(     dcluster1_cc2_slv_r_Ar_Addr  ),
      .dbus_s2_arid(       dcluster1_cc2_slv_r_Ar_Id    ),
      .dbus_s2_arlen(      dcluster1_cc2_slv_r_Ar_Len   ),
      .dbus_s2_arburst(    dcluster1_cc2_slv_r_Ar_Burst ),
      .dbus_s2_arcache(    dcluster1_cc2_slv_r_Ar_Cache ),
      .dbus_s2_arlock(     dcluster1_cc2_slv_r_Ar_Lock  ),
      .dbus_s2_arprot(     dcluster1_cc2_slv_r_Ar_Prot  ),
      .dbus_s2_arsize(     dcluster1_cc2_slv_r_Ar_Size  ),
      .dbus_s2_aruser(     dcluster1_cc2_slv_r_Ar_User  ),
      .dbus_s2_rready(     dcluster1_cc2_slv_r_R_Ready  ),
      .dbus_s2_rvalid(     dbus_s2_rvalid               ),
      .dbus_s2_rlast(      dbus_s2_rlast                ),
      .dbus_s2_rid(        dbus_s2_rid                  ),
      .dbus_s2_rdata(      dbus_s2_rdata                ),
      .dbus_s2_ruser(      dbus_s2_ruser                ),
      .dbus_s2_rresp(      dbus_s2_rresp                ),
      .dbus_s3_arready(    dbus_s3_arready              ),
      .dbus_s3_arvalid(    dcluster1_cc3_slv_r_Ar_Valid ),
      .dbus_s3_araddr(     dcluster1_cc3_slv_r_Ar_Addr  ),
      .dbus_s3_arid(       dcluster1_cc3_slv_r_Ar_Id    ),
      .dbus_s3_arlen(      dcluster1_cc3_slv_r_Ar_Len   ),
      .dbus_s3_arburst(    dcluster1_cc3_slv_r_Ar_Burst ),
      .dbus_s3_arcache(    dcluster1_cc3_slv_r_Ar_Cache ),
      .dbus_s3_arlock(     dcluster1_cc3_slv_r_Ar_Lock  ),
      .dbus_s3_arprot(     dcluster1_cc3_slv_r_Ar_Prot  ),
      .dbus_s3_arsize(     dcluster1_cc3_slv_r_Ar_Size  ),
      .dbus_s3_aruser(     dcluster1_cc3_slv_r_Ar_User  ),
      .dbus_s3_rready(     dcluster1_cc3_slv_r_R_Ready  ),
      .dbus_s3_rvalid(     dbus_s3_rvalid               ),
      .dbus_s3_rlast(      dbus_s3_rlast                ),
      .dbus_s3_rid(        dbus_s3_rid                  ),
      .dbus_s3_rdata(      dbus_s3_rdata                ),
      .dbus_s3_ruser(      dbus_s3_ruser                ),
      .dbus_s3_rresp(      dbus_s3_rresp                ),
      .dbus_m0_arready(    dcluster1_cc0_r_Ar_Ready     ),
      .dbus_m0_arvalid(    dbus_m0_arvalid              ),
      .dbus_m0_araddr(     dbus_m0_araddr               ),
      .dbus_m0_arid(       dbus_m0_arid                 ),
      .dbus_m0_arlen(      dbus_m0_arlen                ),
      .dbus_m0_arburst(    dbus_m0_arburst              ),
      .dbus_m0_arcache(    dbus_m0_arcache              ),
      .dbus_m0_arlock(     dbus_m0_arlock               ),
      .dbus_m0_arprot(     dbus_m0_arprot               ),
      .dbus_m0_arsize(     dbus_m0_arsize               ),
      .dbus_m0_aruser(                                  ),
      .dbus_m0_rready(     dbus_m0_rready               ),
      .dbus_m0_rvalid(     dcluster1_cc0_r_R_Valid      ),
      .dbus_m0_rlast(      dcluster1_cc0_r_R_Last       ),
      .dbus_m0_rid(        dcluster1_cc0_r_R_Id         ),
      .dbus_m0_rdata(      dcluster1_cc0_r_R_Data       ),
      .dbus_m0_ruser(      dcluster1_cc0_r_R_User       ),
      .dbus_m0_rresp(      dcluster1_cc0_r_R_Resp       ),
      .dbus_m1_arready(    dcluster1_cc1_r_Ar_Ready     ),
      .dbus_m1_arvalid(    dbus_m1_arvalid              ),
      .dbus_m1_araddr(     dbus_m1_araddr               ),
      .dbus_m1_arid(       dbus_m1_arid                 ),
      .dbus_m1_arlen(      dbus_m1_arlen                ),
      .dbus_m1_arburst(    dbus_m1_arburst              ),
      .dbus_m1_arcache(    dbus_m1_arcache              ),
      .dbus_m1_arlock(     dbus_m1_arlock               ),
      .dbus_m1_arprot(     dbus_m1_arprot               ),
      .dbus_m1_arsize(     dbus_m1_arsize               ),
      .dbus_m1_aruser(                                  ),
      .dbus_m1_rready(     dbus_m1_rready               ),
      .dbus_m1_rvalid(     dcluster1_cc1_r_R_Valid      ),
      .dbus_m1_rlast(      dcluster1_cc1_r_R_Last       ),
      .dbus_m1_rid(        dcluster1_cc1_r_R_Id         ),
      .dbus_m1_rdata(      dcluster1_cc1_r_R_Data       ),
      .dbus_m1_ruser(      dcluster1_cc1_r_R_User       ),
      .dbus_m1_rresp(      dcluster1_cc1_r_R_Resp       ),
      .dbus_m2_arready(    dcluster1_cc2_r_Ar_Ready     ),
      .dbus_m2_arvalid(    dbus_m2_arvalid              ),
      .dbus_m2_araddr(     dbus_m2_araddr               ),
      .dbus_m2_arid(       dbus_m2_arid                 ),
      .dbus_m2_arlen(      dbus_m2_arlen                ),
      .dbus_m2_arburst(    dbus_m2_arburst              ),
      .dbus_m2_arcache(    dbus_m2_arcache              ),
      .dbus_m2_arlock(     dbus_m2_arlock               ),
      .dbus_m2_arprot(     dbus_m2_arprot               ),
      .dbus_m2_arsize(     dbus_m2_arsize               ),
      .dbus_m2_aruser(                                  ),
      .dbus_m2_rready(     dbus_m2_rready               ),
      .dbus_m2_rvalid(     dcluster1_cc2_r_R_Valid      ),
      .dbus_m2_rlast(      dcluster1_cc2_r_R_Last       ),
      .dbus_m2_rid(        dcluster1_cc2_r_R_Id         ),
      .dbus_m2_rdata(      dcluster1_cc2_r_R_Data       ),
      .dbus_m2_ruser(      dcluster1_cc2_r_R_User       ),
      .dbus_m2_rresp(      dcluster1_cc2_r_R_Resp       ),
      .dbus_m3_arready(    dcluster1_cc3_r_Ar_Ready     ),
      .dbus_m3_arvalid(    dbus_m3_arvalid              ),
      .dbus_m3_araddr(     dbus_m3_araddr               ),
      .dbus_m3_arid(       dbus_m3_arid                 ),
      .dbus_m3_arlen(      dbus_m3_arlen                ),
      .dbus_m3_arburst(    dbus_m3_arburst              ),
      .dbus_m3_arcache(    dbus_m3_arcache              ),
      .dbus_m3_arlock(     dbus_m3_arlock               ),
      .dbus_m3_arprot(     dbus_m3_arprot               ),
      .dbus_m3_arsize(     dbus_m3_arsize               ),
      .dbus_m3_aruser(                                  ),
      .dbus_m3_rready(     dbus_m3_rready               ),
      .dbus_m3_rvalid(     dcluster1_cc3_r_R_Valid      ),
      .dbus_m3_rlast(      dcluster1_cc3_r_R_Last       ),
      .dbus_m3_rid(        dcluster1_cc3_r_R_Id         ),
      .dbus_m3_rdata(      dcluster1_cc3_r_R_Data       ),
      .dbus_m3_ruser(      dcluster1_cc3_r_R_User       ),
      .dbus_m3_rresp(      dcluster1_cc3_r_R_Resp       ) 
      );


cbus_Structure_Module_dcluster1 u_cbus_Structure_Module_dcluster1 (
      .dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdCnt(                dp_Link_n47_astResp001_to_Link_n47_asiResp001_rdcnt             ),
      .dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n47_astResp001_to_Link_n47_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n47_astResp001_to_Link_n47_asiResp001_RxCtl_PwrOnRst(       dp_Link_n47_astResp001_to_Link_n47_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n47_astResp001_to_Link_n47_asiResp001_RdPtr(                dp_Link_n47_astResp001_to_Link_n47_asiResp001_rdptr             ),
      .dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n47_astResp001_to_Link_n47_asiResp001_txctl_pwronrstack ),
      .dp_Link_n47_astResp001_to_Link_n47_asiResp001_TxCtl_PwrOnRst(       dp_Link_n47_astResp001_to_Link_n47_asiResp001_txctl_pwronrst    ),
      .dp_Link_n47_astResp001_to_Link_n47_asiResp001_Data(                 dp_Link_n47_astResp001_to_Link_n47_asiResp001_data              ),
      .dp_Link_n47_astResp001_to_Link_n47_asiResp001_WrCnt(                dp_Link_n47_astResp001_to_Link_n47_asiResp001_wrcnt             ),
      .dp_Link_n47_asi_to_Link_n47_ast_RdCnt(                              dp_Link_n47_asi_to_Link_n47_ast_rdcnt                           ),
      .dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRstAck(                  dp_Link_n47_asi_to_Link_n47_ast_rxctl_pwronrstack               ),
      .dp_Link_n47_asi_to_Link_n47_ast_RxCtl_PwrOnRst(                     dp_Link_n47_asi_to_Link_n47_ast_rxctl_pwronrst                  ),
      .dp_Link_n47_asi_to_Link_n47_ast_RdPtr(                              dp_Link_n47_asi_to_Link_n47_ast_rdptr                           ),
      .dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRstAck(                  dp_Link_n47_asi_to_Link_n47_ast_txctl_pwronrstack               ),
      .dp_Link_n47_asi_to_Link_n47_ast_TxCtl_PwrOnRst(                     dp_Link_n47_asi_to_Link_n47_ast_txctl_pwronrst                  ),
      .dp_Link_n47_asi_to_Link_n47_ast_Data(                               dp_Link_n47_asi_to_Link_n47_ast_data                            ),
      .dp_Link_n47_asi_to_Link_n47_ast_WrCnt(                              dp_Link_n47_asi_to_Link_n47_ast_wrcnt                           ),
      .dnc7_ctrl_R_Last(                                                   dnc_cbus_s0_rlast_2                                             ),
      .dnc7_ctrl_R_Resp(                                                   dnc_cbus_s0_rresp_2                                             ),
      .dnc7_ctrl_R_Valid(                                                  dnc_cbus_s0_rvalid_2                                            ),
      .dnc7_ctrl_R_Ready(                                                  dnc7_ctrl_R_Ready                                               ),
      .dnc7_ctrl_R_Data(                                                   dnc_cbus_s0_rdata_2                                             ),
      .dnc7_ctrl_R_Id(                                                     dnc_cbus_s0_rid_2                                               ),
      .dnc7_ctrl_B_Ready(                                                  dnc7_ctrl_B_Ready                                               ),
      .dnc7_ctrl_B_Resp(                                                   dnc_cbus_s0_bresp_2                                             ),
      .dnc7_ctrl_B_Valid(                                                  dnc_cbus_s0_bvalid_2                                            ),
      .dnc7_ctrl_B_Id(                                                     dnc_cbus_s0_bid_2                                               ),
      .dnc7_ctrl_Aw_Prot(                                                  dnc7_ctrl_Aw_Prot                                               ),
      .dnc7_ctrl_Aw_Addr(                                                  dnc7_ctrl_Aw_Addr                                               ),
      .dnc7_ctrl_Aw_Burst(                                                 dnc7_ctrl_Aw_Burst                                              ),
      .dnc7_ctrl_Aw_Lock(                                                  dnc7_ctrl_Aw_Lock                                               ),
      .dnc7_ctrl_Aw_Cache(                                                 dnc7_ctrl_Aw_Cache                                              ),
      .dnc7_ctrl_Aw_Len(                                                   dnc7_ctrl_Aw_Len                                                ),
      .dnc7_ctrl_Aw_Valid(                                                 dnc7_ctrl_Aw_Valid                                              ),
      .dnc7_ctrl_Aw_Ready(                                                 dnc_cbus_s0_awready_2                                           ),
      .dnc7_ctrl_Aw_Id(                                                    dnc7_ctrl_Aw_Id                                                 ),
      .dnc7_ctrl_Aw_Size(                                                  dnc7_ctrl_Aw_Size                                               ),
      .dnc7_ctrl_W_Last(                                                   dnc7_ctrl_W_Last                                                ),
      .dnc7_ctrl_W_Valid(                                                  dnc7_ctrl_W_Valid                                               ),
      .dnc7_ctrl_W_Ready(                                                  dnc_cbus_s0_wready_2                                            ),
      .dnc7_ctrl_W_Strb(                                                   dnc7_ctrl_W_Strb                                                ),
      .dnc7_ctrl_W_Data(                                                   dnc7_ctrl_W_Data                                                ),
      .dnc7_ctrl_Ar_Prot(                                                  dnc7_ctrl_Ar_Prot                                               ),
      .dnc7_ctrl_Ar_Addr(                                                  dnc7_ctrl_Ar_Addr                                               ),
      .dnc7_ctrl_Ar_Burst(                                                 dnc7_ctrl_Ar_Burst                                              ),
      .dnc7_ctrl_Ar_Lock(                                                  dnc7_ctrl_Ar_Lock                                               ),
      .dnc7_ctrl_Ar_Cache(                                                 dnc7_ctrl_Ar_Cache                                              ),
      .dnc7_ctrl_Ar_Len(                                                   dnc7_ctrl_Ar_Len                                                ),
      .dnc7_ctrl_Ar_Valid(                                                 dnc7_ctrl_Ar_Valid                                              ),
      .dnc7_ctrl_Ar_Ready(                                                 dnc_cbus_s0_arready_2                                           ),
      .dnc7_ctrl_Ar_Id(                                                    dnc7_ctrl_Ar_Id                                                 ),
      .dnc7_ctrl_Ar_Size(                                                  dnc7_ctrl_Ar_Size                                               ),
      .dnc6_ctrl_R_Last(                                                   dnc_cbus_s0_rlast_1                                             ),
      .dnc6_ctrl_R_Resp(                                                   dnc_cbus_s0_rresp_1                                             ),
      .dnc6_ctrl_R_Valid(                                                  dnc_cbus_s0_rvalid_1                                            ),
      .dnc6_ctrl_R_Ready(                                                  dnc6_ctrl_R_Ready                                               ),
      .dnc6_ctrl_R_Data(                                                   dnc_cbus_s0_rdata_1                                             ),
      .dnc6_ctrl_R_Id(                                                     dnc_cbus_s0_rid_1                                               ),
      .dnc6_ctrl_B_Ready(                                                  dnc6_ctrl_B_Ready                                               ),
      .dnc6_ctrl_B_Resp(                                                   dnc_cbus_s0_bresp_1                                             ),
      .dnc6_ctrl_B_Valid(                                                  dnc_cbus_s0_bvalid_1                                            ),
      .dnc6_ctrl_B_Id(                                                     dnc_cbus_s0_bid_1                                               ),
      .dnc6_ctrl_Aw_Prot(                                                  dnc6_ctrl_Aw_Prot                                               ),
      .dnc6_ctrl_Aw_Addr(                                                  dnc6_ctrl_Aw_Addr                                               ),
      .dnc6_ctrl_Aw_Burst(                                                 dnc6_ctrl_Aw_Burst                                              ),
      .dnc6_ctrl_Aw_Lock(                                                  dnc6_ctrl_Aw_Lock                                               ),
      .dnc6_ctrl_Aw_Cache(                                                 dnc6_ctrl_Aw_Cache                                              ),
      .dnc6_ctrl_Aw_Len(                                                   dnc6_ctrl_Aw_Len                                                ),
      .dnc6_ctrl_Aw_Valid(                                                 dnc6_ctrl_Aw_Valid                                              ),
      .dnc6_ctrl_Aw_Ready(                                                 dnc_cbus_s0_awready_1                                           ),
      .dnc6_ctrl_Aw_Id(                                                    dnc6_ctrl_Aw_Id                                                 ),
      .dnc6_ctrl_Aw_Size(                                                  dnc6_ctrl_Aw_Size                                               ),
      .dnc6_ctrl_W_Last(                                                   dnc6_ctrl_W_Last                                                ),
      .dnc6_ctrl_W_Valid(                                                  dnc6_ctrl_W_Valid                                               ),
      .dnc6_ctrl_W_Ready(                                                  dnc_cbus_s0_wready_1                                            ),
      .dnc6_ctrl_W_Strb(                                                   dnc6_ctrl_W_Strb                                                ),
      .dnc6_ctrl_W_Data(                                                   dnc6_ctrl_W_Data                                                ),
      .dnc6_ctrl_Ar_Prot(                                                  dnc6_ctrl_Ar_Prot                                               ),
      .dnc6_ctrl_Ar_Addr(                                                  dnc6_ctrl_Ar_Addr                                               ),
      .dnc6_ctrl_Ar_Burst(                                                 dnc6_ctrl_Ar_Burst                                              ),
      .dnc6_ctrl_Ar_Lock(                                                  dnc6_ctrl_Ar_Lock                                               ),
      .dnc6_ctrl_Ar_Cache(                                                 dnc6_ctrl_Ar_Cache                                              ),
      .dnc6_ctrl_Ar_Len(                                                   dnc6_ctrl_Ar_Len                                                ),
      .dnc6_ctrl_Ar_Valid(                                                 dnc6_ctrl_Ar_Valid                                              ),
      .dnc6_ctrl_Ar_Ready(                                                 dnc_cbus_s0_arready_1                                           ),
      .dnc6_ctrl_Ar_Id(                                                    dnc6_ctrl_Ar_Id                                                 ),
      .dnc6_ctrl_Ar_Size(                                                  dnc6_ctrl_Ar_Size                                               ),
      .dnc5_ctrl_R_Last(                                                   dnc_cbus_s0_rlast_0                                             ),
      .dnc5_ctrl_R_Resp(                                                   dnc_cbus_s0_rresp_0                                             ),
      .dnc5_ctrl_R_Valid(                                                  dnc_cbus_s0_rvalid_0                                            ),
      .dnc5_ctrl_R_Ready(                                                  dnc5_ctrl_R_Ready                                               ),
      .dnc5_ctrl_R_Data(                                                   dnc_cbus_s0_rdata_0                                             ),
      .dnc5_ctrl_R_Id(                                                     dnc_cbus_s0_rid_0                                               ),
      .dnc5_ctrl_B_Ready(                                                  dnc5_ctrl_B_Ready                                               ),
      .dnc5_ctrl_B_Resp(                                                   dnc_cbus_s0_bresp_0                                             ),
      .dnc5_ctrl_B_Valid(                                                  dnc_cbus_s0_bvalid_0                                            ),
      .dnc5_ctrl_B_Id(                                                     dnc_cbus_s0_bid_0                                               ),
      .dnc5_ctrl_Aw_Prot(                                                  dnc5_ctrl_Aw_Prot                                               ),
      .dnc5_ctrl_Aw_Addr(                                                  dnc5_ctrl_Aw_Addr                                               ),
      .dnc5_ctrl_Aw_Burst(                                                 dnc5_ctrl_Aw_Burst                                              ),
      .dnc5_ctrl_Aw_Lock(                                                  dnc5_ctrl_Aw_Lock                                               ),
      .dnc5_ctrl_Aw_Cache(                                                 dnc5_ctrl_Aw_Cache                                              ),
      .dnc5_ctrl_Aw_Len(                                                   dnc5_ctrl_Aw_Len                                                ),
      .dnc5_ctrl_Aw_Valid(                                                 dnc5_ctrl_Aw_Valid                                              ),
      .dnc5_ctrl_Aw_Ready(                                                 dnc_cbus_s0_awready_0                                           ),
      .dnc5_ctrl_Aw_Id(                                                    dnc5_ctrl_Aw_Id                                                 ),
      .dnc5_ctrl_Aw_Size(                                                  dnc5_ctrl_Aw_Size                                               ),
      .dnc5_ctrl_W_Last(                                                   dnc5_ctrl_W_Last                                                ),
      .dnc5_ctrl_W_Valid(                                                  dnc5_ctrl_W_Valid                                               ),
      .dnc5_ctrl_W_Ready(                                                  dnc_cbus_s0_wready_0                                            ),
      .dnc5_ctrl_W_Strb(                                                   dnc5_ctrl_W_Strb                                                ),
      .dnc5_ctrl_W_Data(                                                   dnc5_ctrl_W_Data                                                ),
      .dnc5_ctrl_Ar_Prot(                                                  dnc5_ctrl_Ar_Prot                                               ),
      .dnc5_ctrl_Ar_Addr(                                                  dnc5_ctrl_Ar_Addr                                               ),
      .dnc5_ctrl_Ar_Burst(                                                 dnc5_ctrl_Ar_Burst                                              ),
      .dnc5_ctrl_Ar_Lock(                                                  dnc5_ctrl_Ar_Lock                                               ),
      .dnc5_ctrl_Ar_Cache(                                                 dnc5_ctrl_Ar_Cache                                              ),
      .dnc5_ctrl_Ar_Len(                                                   dnc5_ctrl_Ar_Len                                                ),
      .dnc5_ctrl_Ar_Valid(                                                 dnc5_ctrl_Ar_Valid                                              ),
      .dnc5_ctrl_Ar_Ready(                                                 dnc_cbus_s0_arready_0                                           ),
      .dnc5_ctrl_Ar_Id(                                                    dnc5_ctrl_Ar_Id                                                 ),
      .dnc5_ctrl_Ar_Size(                                                  dnc5_ctrl_Ar_Size                                               ),
      .dnc4_ctrl_R_Last(                                                   dnc_cbus_s0_rlast                                               ),
      .dnc4_ctrl_R_Resp(                                                   dnc_cbus_s0_rresp                                               ),
      .dnc4_ctrl_R_Valid(                                                  dnc_cbus_s0_rvalid                                              ),
      .dnc4_ctrl_R_Ready(                                                  dnc4_ctrl_R_Ready                                               ),
      .dnc4_ctrl_R_Data(                                                   dnc_cbus_s0_rdata                                               ),
      .dnc4_ctrl_R_Id(                                                     dnc_cbus_s0_rid                                                 ),
      .dnc4_ctrl_B_Ready(                                                  dnc4_ctrl_B_Ready                                               ),
      .dnc4_ctrl_B_Resp(                                                   dnc_cbus_s0_bresp                                               ),
      .dnc4_ctrl_B_Valid(                                                  dnc_cbus_s0_bvalid                                              ),
      .dnc4_ctrl_B_Id(                                                     dnc_cbus_s0_bid                                                 ),
      .dnc4_ctrl_Aw_Prot(                                                  dnc4_ctrl_Aw_Prot                                               ),
      .dnc4_ctrl_Aw_Addr(                                                  dnc4_ctrl_Aw_Addr                                               ),
      .dnc4_ctrl_Aw_Burst(                                                 dnc4_ctrl_Aw_Burst                                              ),
      .dnc4_ctrl_Aw_Lock(                                                  dnc4_ctrl_Aw_Lock                                               ),
      .dnc4_ctrl_Aw_Cache(                                                 dnc4_ctrl_Aw_Cache                                              ),
      .dnc4_ctrl_Aw_Len(                                                   dnc4_ctrl_Aw_Len                                                ),
      .dnc4_ctrl_Aw_Valid(                                                 dnc4_ctrl_Aw_Valid                                              ),
      .dnc4_ctrl_Aw_Ready(                                                 dnc_cbus_s0_awready                                             ),
      .dnc4_ctrl_Aw_Id(                                                    dnc4_ctrl_Aw_Id                                                 ),
      .dnc4_ctrl_Aw_Size(                                                  dnc4_ctrl_Aw_Size                                               ),
      .dnc4_ctrl_W_Last(                                                   dnc4_ctrl_W_Last                                                ),
      .dnc4_ctrl_W_Valid(                                                  dnc4_ctrl_W_Valid                                               ),
      .dnc4_ctrl_W_Ready(                                                  dnc_cbus_s0_wready                                              ),
      .dnc4_ctrl_W_Strb(                                                   dnc4_ctrl_W_Strb                                                ),
      .dnc4_ctrl_W_Data(                                                   dnc4_ctrl_W_Data                                                ),
      .dnc4_ctrl_Ar_Prot(                                                  dnc4_ctrl_Ar_Prot                                               ),
      .dnc4_ctrl_Ar_Addr(                                                  dnc4_ctrl_Ar_Addr                                               ),
      .dnc4_ctrl_Ar_Burst(                                                 dnc4_ctrl_Ar_Burst                                              ),
      .dnc4_ctrl_Ar_Lock(                                                  dnc4_ctrl_Ar_Lock                                               ),
      .dnc4_ctrl_Ar_Cache(                                                 dnc4_ctrl_Ar_Cache                                              ),
      .dnc4_ctrl_Ar_Len(                                                   dnc4_ctrl_Ar_Len                                                ),
      .dnc4_ctrl_Ar_Valid(                                                 dnc4_ctrl_Ar_Valid                                              ),
      .dnc4_ctrl_Ar_Ready(                                                 dnc_cbus_s0_arready                                             ),
      .dnc4_ctrl_Ar_Id(                                                    dnc4_ctrl_Ar_Id                                                 ),
      .dnc4_ctrl_Ar_Size(                                                  dnc4_ctrl_Ar_Size                                               ),
      .dcluster1_ctrl_R_Last(                                              cbus_s0_rlast                                                   ),
      .dcluster1_ctrl_R_Resp(                                              cbus_s0_rresp                                                   ),
      .dcluster1_ctrl_R_Valid(                                             cbus_s0_rvalid                                                  ),
      .dcluster1_ctrl_R_Ready(                                             dcluster1_ctrl_R_Ready                                          ),
      .dcluster1_ctrl_R_Data(                                              cbus_s0_rdata                                                   ),
      .dcluster1_ctrl_R_Id(                                                cbus_s0_rid                                                     ),
      .dcluster1_ctrl_B_Ready(                                             dcluster1_ctrl_B_Ready                                          ),
      .dcluster1_ctrl_B_Resp(                                              cbus_s0_bresp                                                   ),
      .dcluster1_ctrl_B_Valid(                                             cbus_s0_bvalid                                                  ),
      .dcluster1_ctrl_B_Id(                                                cbus_s0_bid                                                     ),
      .dcluster1_ctrl_Aw_Prot(                                             dcluster1_ctrl_Aw_Prot                                          ),
      .dcluster1_ctrl_Aw_Addr(                                             dcluster1_ctrl_Aw_Addr                                          ),
      .dcluster1_ctrl_Aw_Burst(                                            dcluster1_ctrl_Aw_Burst                                         ),
      .dcluster1_ctrl_Aw_Lock(                                             dcluster1_ctrl_Aw_Lock                                          ),
      .dcluster1_ctrl_Aw_Cache(                                            dcluster1_ctrl_Aw_Cache                                         ),
      .dcluster1_ctrl_Aw_Len(                                              dcluster1_ctrl_Aw_Len                                           ),
      .dcluster1_ctrl_Aw_Valid(                                            dcluster1_ctrl_Aw_Valid                                         ),
      .dcluster1_ctrl_Aw_Ready(                                            cbus_s0_awready                                                 ),
      .dcluster1_ctrl_Aw_Id(                                               dcluster1_ctrl_Aw_Id                                            ),
      .dcluster1_ctrl_Aw_Size(                                             dcluster1_ctrl_Aw_Size                                          ),
      .dcluster1_ctrl_W_Last(                                              dcluster1_ctrl_W_Last                                           ),
      .dcluster1_ctrl_W_Valid(                                             dcluster1_ctrl_W_Valid                                          ),
      .dcluster1_ctrl_W_Ready(                                             cbus_s0_wready                                                  ),
      .dcluster1_ctrl_W_Strb(                                              dcluster1_ctrl_W_Strb                                           ),
      .dcluster1_ctrl_W_Data(                                              dcluster1_ctrl_W_Data                                           ),
      .dcluster1_ctrl_Ar_Prot(                                             dcluster1_ctrl_Ar_Prot                                          ),
      .dcluster1_ctrl_Ar_Addr(                                             dcluster1_ctrl_Ar_Addr                                          ),
      .dcluster1_ctrl_Ar_Burst(                                            dcluster1_ctrl_Ar_Burst                                         ),
      .dcluster1_ctrl_Ar_Lock(                                             dcluster1_ctrl_Ar_Lock                                          ),
      .dcluster1_ctrl_Ar_Cache(                                            dcluster1_ctrl_Ar_Cache                                         ),
      .dcluster1_ctrl_Ar_Len(                                              dcluster1_ctrl_Ar_Len                                           ),
      .dcluster1_ctrl_Ar_Valid(                                            dcluster1_ctrl_Ar_Valid                                         ),
      .dcluster1_ctrl_Ar_Ready(                                            cbus_s0_arready                                                 ),
      .dcluster1_ctrl_Ar_Id(                                               dcluster1_ctrl_Ar_Id                                            ),
      .dcluster1_ctrl_Ar_Size(                                             dcluster1_ctrl_Ar_Size                                          ),
      .clk_dcluster1(                                                      aclk                                                            ),
      .arstn_dcluster1(                                                    arstn                                                           ),
      .TM(                                                                 TM                                                              ) 
      );


dbus_read_Structure_Module_dcluster1 u_dbus_read_Structure_Module_dcluster1 (
      .dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdCnt(                dp_Link_n7_astResp001_to_Link_n7_asiResp001_rdcnt             ),
      .dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n7_astResp001_to_Link_n7_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n7_astResp001_to_Link_n7_asiResp001_RxCtl_PwrOnRst(       dp_Link_n7_astResp001_to_Link_n7_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n7_astResp001_to_Link_n7_asiResp001_RdPtr(                dp_Link_n7_astResp001_to_Link_n7_asiResp001_rdptr             ),
      .dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n7_astResp001_to_Link_n7_asiResp001_txctl_pwronrstack ),
      .dp_Link_n7_astResp001_to_Link_n7_asiResp001_TxCtl_PwrOnRst(       dp_Link_n7_astResp001_to_Link_n7_asiResp001_txctl_pwronrst    ),
      .dp_Link_n7_astResp001_to_Link_n7_asiResp001_Data(                 dp_Link_n7_astResp001_to_Link_n7_asiResp001_data              ),
      .dp_Link_n7_astResp001_to_Link_n7_asiResp001_WrCnt(                dp_Link_n7_astResp001_to_Link_n7_asiResp001_wrcnt             ),
      .dp_Link_n7_asi_to_Link_n7_ast_RdCnt(                              dp_Link_n7_asi_to_Link_n7_ast_r_rdcnt                         ),
      .dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck(                  dp_Link_n7_asi_to_Link_n7_ast_r_rxctl_pwronrstack             ),
      .dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst(                     dp_Link_n7_asi_to_Link_n7_ast_r_rxctl_pwronrst                ),
      .dp_Link_n7_asi_to_Link_n7_ast_RdPtr(                              dp_Link_n7_asi_to_Link_n7_ast_r_rdptr                         ),
      .dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck(                  dp_Link_n7_asi_to_Link_n7_ast_r_txctl_pwronrstack             ),
      .dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst(                     dp_Link_n7_asi_to_Link_n7_ast_r_txctl_pwronrst                ),
      .dp_Link_n7_asi_to_Link_n7_ast_Data(                               dp_Link_n7_asi_to_Link_n7_ast_r_data                          ),
      .dp_Link_n7_asi_to_Link_n7_ast_WrCnt(                              dp_Link_n7_asi_to_Link_n7_ast_r_wrcnt                         ),
      .dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdCnt(                dp_Link_n6_astResp001_to_Link_n6_asiResp001_rdcnt             ),
      .dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n6_astResp001_to_Link_n6_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n6_astResp001_to_Link_n6_asiResp001_RxCtl_PwrOnRst(       dp_Link_n6_astResp001_to_Link_n6_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n6_astResp001_to_Link_n6_asiResp001_RdPtr(                dp_Link_n6_astResp001_to_Link_n6_asiResp001_rdptr             ),
      .dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n6_astResp001_to_Link_n6_asiResp001_txctl_pwronrstack ),
      .dp_Link_n6_astResp001_to_Link_n6_asiResp001_TxCtl_PwrOnRst(       dp_Link_n6_astResp001_to_Link_n6_asiResp001_txctl_pwronrst    ),
      .dp_Link_n6_astResp001_to_Link_n6_asiResp001_Data(                 dp_Link_n6_astResp001_to_Link_n6_asiResp001_data              ),
      .dp_Link_n6_astResp001_to_Link_n6_asiResp001_WrCnt(                dp_Link_n6_astResp001_to_Link_n6_asiResp001_wrcnt             ),
      .dp_Link_n6_asi_to_Link_n6_ast_RdCnt(                              dp_Link_n6_asi_to_Link_n6_ast_r_rdcnt                         ),
      .dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck(                  dp_Link_n6_asi_to_Link_n6_ast_r_rxctl_pwronrstack             ),
      .dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst(                     dp_Link_n6_asi_to_Link_n6_ast_r_rxctl_pwronrst                ),
      .dp_Link_n6_asi_to_Link_n6_ast_RdPtr(                              dp_Link_n6_asi_to_Link_n6_ast_r_rdptr                         ),
      .dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck(                  dp_Link_n6_asi_to_Link_n6_ast_r_txctl_pwronrstack             ),
      .dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst(                     dp_Link_n6_asi_to_Link_n6_ast_r_txctl_pwronrst                ),
      .dp_Link_n6_asi_to_Link_n6_ast_Data(                               dp_Link_n6_asi_to_Link_n6_ast_r_data                          ),
      .dp_Link_n6_asi_to_Link_n6_ast_WrCnt(                              dp_Link_n6_asi_to_Link_n6_ast_r_wrcnt                         ),
      .dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdCnt(                dp_Link_n5_astResp001_to_Link_n5_asiResp001_rdcnt             ),
      .dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n5_astResp001_to_Link_n5_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n5_astResp001_to_Link_n5_asiResp001_RxCtl_PwrOnRst(       dp_Link_n5_astResp001_to_Link_n5_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n5_astResp001_to_Link_n5_asiResp001_RdPtr(                dp_Link_n5_astResp001_to_Link_n5_asiResp001_rdptr             ),
      .dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n5_astResp001_to_Link_n5_asiResp001_txctl_pwronrstack ),
      .dp_Link_n5_astResp001_to_Link_n5_asiResp001_TxCtl_PwrOnRst(       dp_Link_n5_astResp001_to_Link_n5_asiResp001_txctl_pwronrst    ),
      .dp_Link_n5_astResp001_to_Link_n5_asiResp001_Data(                 dp_Link_n5_astResp001_to_Link_n5_asiResp001_data              ),
      .dp_Link_n5_astResp001_to_Link_n5_asiResp001_WrCnt(                dp_Link_n5_astResp001_to_Link_n5_asiResp001_wrcnt             ),
      .dp_Link_n5_asi_to_Link_n5_ast_RdCnt(                              dp_Link_n5_asi_to_Link_n5_ast_r_rdcnt                         ),
      .dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck(                  dp_Link_n5_asi_to_Link_n5_ast_r_rxctl_pwronrstack             ),
      .dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst(                     dp_Link_n5_asi_to_Link_n5_ast_r_rxctl_pwronrst                ),
      .dp_Link_n5_asi_to_Link_n5_ast_RdPtr(                              dp_Link_n5_asi_to_Link_n5_ast_r_rdptr                         ),
      .dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck(                  dp_Link_n5_asi_to_Link_n5_ast_r_txctl_pwronrstack             ),
      .dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst(                     dp_Link_n5_asi_to_Link_n5_ast_r_txctl_pwronrst                ),
      .dp_Link_n5_asi_to_Link_n5_ast_Data(                               dp_Link_n5_asi_to_Link_n5_ast_r_data                          ),
      .dp_Link_n5_asi_to_Link_n5_ast_WrCnt(                              dp_Link_n5_asi_to_Link_n5_ast_r_wrcnt                         ),
      .dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdCnt(                dp_Link_n4_astResp001_to_Link_n4_asiResp001_rdcnt             ),
      .dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n4_astResp001_to_Link_n4_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n4_astResp001_to_Link_n4_asiResp001_RxCtl_PwrOnRst(       dp_Link_n4_astResp001_to_Link_n4_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n4_astResp001_to_Link_n4_asiResp001_RdPtr(                dp_Link_n4_astResp001_to_Link_n4_asiResp001_rdptr             ),
      .dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n4_astResp001_to_Link_n4_asiResp001_txctl_pwronrstack ),
      .dp_Link_n4_astResp001_to_Link_n4_asiResp001_TxCtl_PwrOnRst(       dp_Link_n4_astResp001_to_Link_n4_asiResp001_txctl_pwronrst    ),
      .dp_Link_n4_astResp001_to_Link_n4_asiResp001_Data(                 dp_Link_n4_astResp001_to_Link_n4_asiResp001_data              ),
      .dp_Link_n4_astResp001_to_Link_n4_asiResp001_WrCnt(                dp_Link_n4_astResp001_to_Link_n4_asiResp001_wrcnt             ),
      .dp_Link_n4_asi_to_Link_n4_ast_RdCnt(                              dp_Link_n4_asi_to_Link_n4_ast_r_rdcnt                         ),
      .dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck(                  dp_Link_n4_asi_to_Link_n4_ast_r_rxctl_pwronrstack             ),
      .dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst(                     dp_Link_n4_asi_to_Link_n4_ast_r_rxctl_pwronrst                ),
      .dp_Link_n4_asi_to_Link_n4_ast_RdPtr(                              dp_Link_n4_asi_to_Link_n4_ast_r_rdptr                         ),
      .dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck(                  dp_Link_n4_asi_to_Link_n4_ast_r_txctl_pwronrstack             ),
      .dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst(                     dp_Link_n4_asi_to_Link_n4_ast_r_txctl_pwronrst                ),
      .dp_Link_n4_asi_to_Link_n4_ast_Data(                               dp_Link_n4_asi_to_Link_n4_ast_r_data                          ),
      .dp_Link_n4_asi_to_Link_n4_ast_WrCnt(                              dp_Link_n4_asi_to_Link_n4_ast_r_wrcnt                         ),
      .dnc7_slv_r_R_Last(                                                dnc_dbus_s0_rlast_2                                           ),
      .dnc7_slv_r_R_Resp(                                                dnc_dbus_s0_rresp_2                                           ),
      .dnc7_slv_r_R_Valid(                                               dnc_dbus_s0_rvalid_2                                          ),
      .dnc7_slv_r_R_Ready(                                               dnc7_slv_r_R_Ready                                            ),
      .dnc7_slv_r_R_Data(                                                dnc_dbus_s0_rdata_2                                           ),
      .dnc7_slv_r_R_Id(                                                  dnc_dbus_s0_rid_2                                             ),
      .dnc7_slv_r_Ar_Prot(                                               dnc7_slv_r_Ar_Prot                                            ),
      .dnc7_slv_r_Ar_Addr(                                               dnc7_slv_r_Ar_Addr                                            ),
      .dnc7_slv_r_Ar_Burst(                                              dnc7_slv_r_Ar_Burst                                           ),
      .dnc7_slv_r_Ar_Lock(                                               dnc7_slv_r_Ar_Lock                                            ),
      .dnc7_slv_r_Ar_Cache(                                              dnc7_slv_r_Ar_Cache                                           ),
      .dnc7_slv_r_Ar_Len(                                                dnc7_slv_r_Ar_Len                                             ),
      .dnc7_slv_r_Ar_Valid(                                              dnc7_slv_r_Ar_Valid                                           ),
      .dnc7_slv_r_Ar_Ready(                                              dnc_dbus_s0_arready_2                                         ),
      .dnc7_slv_r_Ar_Id(                                                 dnc7_slv_r_Ar_Id                                              ),
      .dnc7_slv_r_Ar_Size(                                               dnc7_slv_r_Ar_Size                                            ),
      .dnc7_r_R_Last(                                                    dnc7_r_R_Last                                                 ),
      .dnc7_r_R_Resp(                                                    dnc7_r_R_Resp                                                 ),
      .dnc7_r_R_Valid(                                                   dnc7_r_R_Valid                                                ),
      .dnc7_r_R_User(                                                    dnc7_r_R_User                                                 ),
      .dnc7_r_R_Ready(                                                   dnc_dbus_m0_rready_2                                          ),
      .dnc7_r_R_Data(                                                    dnc7_r_R_Data                                                 ),
      .dnc7_r_R_Id(                                                      dnc7_r_R_Id                                                   ),
      .dnc7_r_Ar_Prot(                                                   dnc_dbus_m0_arprot_2                                          ),
      .dnc7_r_Ar_Addr(                                                 { 18'b000000000000000000,
                                                                         dnc_dbus_m0_araddr_2 }                                        ),
      .dnc7_r_Ar_Burst(                                                  dnc_dbus_m0_arburst_2                                         ),
      .dnc7_r_Ar_Lock(                                                   dnc_dbus_m0_arlock_2                                          ),
      .dnc7_r_Ar_Cache(                                                  dnc_dbus_m0_arcache_2                                         ),
      .dnc7_r_Ar_Len(                                                  { 2'b00,
                                                                         dnc_dbus_m0_arlen_2 }                                         ),
      .dnc7_r_Ar_Valid(                                                  dnc_dbus_m0_arvalid_2                                         ),
      .dnc7_r_Ar_User(                                                   dnc_dbus_m0_aruser_2                                          ),
      .dnc7_r_Ar_Ready(                                                  dnc7_r_Ar_Ready                                               ),
      .dnc7_r_Ar_Id(                                                     dnc_dbus_m0_arid_2                                            ),
      .dnc7_r_Ar_Size(                                                   dnc_dbus_m0_arsize_2                                          ),
      .dnc6_slv_r_R_Last(                                                dnc_dbus_s0_rlast_1                                           ),
      .dnc6_slv_r_R_Resp(                                                dnc_dbus_s0_rresp_1                                           ),
      .dnc6_slv_r_R_Valid(                                               dnc_dbus_s0_rvalid_1                                          ),
      .dnc6_slv_r_R_Ready(                                               dnc6_slv_r_R_Ready                                            ),
      .dnc6_slv_r_R_Data(                                                dnc_dbus_s0_rdata_1                                           ),
      .dnc6_slv_r_R_Id(                                                  dnc_dbus_s0_rid_1                                             ),
      .dnc6_slv_r_Ar_Prot(                                               dnc6_slv_r_Ar_Prot                                            ),
      .dnc6_slv_r_Ar_Addr(                                               dnc6_slv_r_Ar_Addr                                            ),
      .dnc6_slv_r_Ar_Burst(                                              dnc6_slv_r_Ar_Burst                                           ),
      .dnc6_slv_r_Ar_Lock(                                               dnc6_slv_r_Ar_Lock                                            ),
      .dnc6_slv_r_Ar_Cache(                                              dnc6_slv_r_Ar_Cache                                           ),
      .dnc6_slv_r_Ar_Len(                                                dnc6_slv_r_Ar_Len                                             ),
      .dnc6_slv_r_Ar_Valid(                                              dnc6_slv_r_Ar_Valid                                           ),
      .dnc6_slv_r_Ar_Ready(                                              dnc_dbus_s0_arready_1                                         ),
      .dnc6_slv_r_Ar_Id(                                                 dnc6_slv_r_Ar_Id                                              ),
      .dnc6_slv_r_Ar_Size(                                               dnc6_slv_r_Ar_Size                                            ),
      .dnc6_r_R_Last(                                                    dnc6_r_R_Last                                                 ),
      .dnc6_r_R_Resp(                                                    dnc6_r_R_Resp                                                 ),
      .dnc6_r_R_Valid(                                                   dnc6_r_R_Valid                                                ),
      .dnc6_r_R_User(                                                    dnc6_r_R_User                                                 ),
      .dnc6_r_R_Ready(                                                   dnc_dbus_m0_rready_1                                          ),
      .dnc6_r_R_Data(                                                    dnc6_r_R_Data                                                 ),
      .dnc6_r_R_Id(                                                      dnc6_r_R_Id                                                   ),
      .dnc6_r_Ar_Prot(                                                   dnc_dbus_m0_arprot_1                                          ),
      .dnc6_r_Ar_Addr(                                                 { 18'b000000000000000000,
                                                                         dnc_dbus_m0_araddr_1 }                                        ),
      .dnc6_r_Ar_Burst(                                                  dnc_dbus_m0_arburst_1                                         ),
      .dnc6_r_Ar_Lock(                                                   dnc_dbus_m0_arlock_1                                          ),
      .dnc6_r_Ar_Cache(                                                  dnc_dbus_m0_arcache_1                                         ),
      .dnc6_r_Ar_Len(                                                  { 2'b00,
                                                                         dnc_dbus_m0_arlen_1 }                                         ),
      .dnc6_r_Ar_Valid(                                                  dnc_dbus_m0_arvalid_1                                         ),
      .dnc6_r_Ar_User(                                                   dnc_dbus_m0_aruser_1                                          ),
      .dnc6_r_Ar_Ready(                                                  dnc6_r_Ar_Ready                                               ),
      .dnc6_r_Ar_Id(                                                     dnc_dbus_m0_arid_1                                            ),
      .dnc6_r_Ar_Size(                                                   dnc_dbus_m0_arsize_1                                          ),
      .dnc5_slv_r_R_Last(                                                dnc_dbus_s0_rlast_0                                           ),
      .dnc5_slv_r_R_Resp(                                                dnc_dbus_s0_rresp_0                                           ),
      .dnc5_slv_r_R_Valid(                                               dnc_dbus_s0_rvalid_0                                          ),
      .dnc5_slv_r_R_Ready(                                               dnc5_slv_r_R_Ready                                            ),
      .dnc5_slv_r_R_Data(                                                dnc_dbus_s0_rdata_0                                           ),
      .dnc5_slv_r_R_Id(                                                  dnc_dbus_s0_rid_0                                             ),
      .dnc5_slv_r_Ar_Prot(                                               dnc5_slv_r_Ar_Prot                                            ),
      .dnc5_slv_r_Ar_Addr(                                               dnc5_slv_r_Ar_Addr                                            ),
      .dnc5_slv_r_Ar_Burst(                                              dnc5_slv_r_Ar_Burst                                           ),
      .dnc5_slv_r_Ar_Lock(                                               dnc5_slv_r_Ar_Lock                                            ),
      .dnc5_slv_r_Ar_Cache(                                              dnc5_slv_r_Ar_Cache                                           ),
      .dnc5_slv_r_Ar_Len(                                                dnc5_slv_r_Ar_Len                                             ),
      .dnc5_slv_r_Ar_Valid(                                              dnc5_slv_r_Ar_Valid                                           ),
      .dnc5_slv_r_Ar_Ready(                                              dnc_dbus_s0_arready_0                                         ),
      .dnc5_slv_r_Ar_Id(                                                 dnc5_slv_r_Ar_Id                                              ),
      .dnc5_slv_r_Ar_Size(                                               dnc5_slv_r_Ar_Size                                            ),
      .dnc5_r_R_Last(                                                    dnc5_r_R_Last                                                 ),
      .dnc5_r_R_Resp(                                                    dnc5_r_R_Resp                                                 ),
      .dnc5_r_R_Valid(                                                   dnc5_r_R_Valid                                                ),
      .dnc5_r_R_User(                                                    dnc5_r_R_User                                                 ),
      .dnc5_r_R_Ready(                                                   dnc_dbus_m0_rready_0                                          ),
      .dnc5_r_R_Data(                                                    dnc5_r_R_Data                                                 ),
      .dnc5_r_R_Id(                                                      dnc5_r_R_Id                                                   ),
      .dnc5_r_Ar_Prot(                                                   dnc_dbus_m0_arprot_0                                          ),
      .dnc5_r_Ar_Addr(                                                 { 18'b000000000000000000,
                                                                         dnc_dbus_m0_araddr_0 }                                        ),
      .dnc5_r_Ar_Burst(                                                  dnc_dbus_m0_arburst_0                                         ),
      .dnc5_r_Ar_Lock(                                                   dnc_dbus_m0_arlock_0                                          ),
      .dnc5_r_Ar_Cache(                                                  dnc_dbus_m0_arcache_0                                         ),
      .dnc5_r_Ar_Len(                                                  { 2'b00,
                                                                         dnc_dbus_m0_arlen_0 }                                         ),
      .dnc5_r_Ar_Valid(                                                  dnc_dbus_m0_arvalid_0                                         ),
      .dnc5_r_Ar_User(                                                   dnc_dbus_m0_aruser_0                                          ),
      .dnc5_r_Ar_Ready(                                                  dnc5_r_Ar_Ready                                               ),
      .dnc5_r_Ar_Id(                                                     dnc_dbus_m0_arid_0                                            ),
      .dnc5_r_Ar_Size(                                                   dnc_dbus_m0_arsize_0                                          ),
      .dnc4_slv_r_R_Last(                                                dnc_dbus_s0_rlast                                             ),
      .dnc4_slv_r_R_Resp(                                                dnc_dbus_s0_rresp                                             ),
      .dnc4_slv_r_R_Valid(                                               dnc_dbus_s0_rvalid                                            ),
      .dnc4_slv_r_R_Ready(                                               dnc4_slv_r_R_Ready                                            ),
      .dnc4_slv_r_R_Data(                                                dnc_dbus_s0_rdata                                             ),
      .dnc4_slv_r_R_Id(                                                  dnc_dbus_s0_rid                                               ),
      .dnc4_slv_r_Ar_Prot(                                               dnc4_slv_r_Ar_Prot                                            ),
      .dnc4_slv_r_Ar_Addr(                                               dnc4_slv_r_Ar_Addr                                            ),
      .dnc4_slv_r_Ar_Burst(                                              dnc4_slv_r_Ar_Burst                                           ),
      .dnc4_slv_r_Ar_Lock(                                               dnc4_slv_r_Ar_Lock                                            ),
      .dnc4_slv_r_Ar_Cache(                                              dnc4_slv_r_Ar_Cache                                           ),
      .dnc4_slv_r_Ar_Len(                                                dnc4_slv_r_Ar_Len                                             ),
      .dnc4_slv_r_Ar_Valid(                                              dnc4_slv_r_Ar_Valid                                           ),
      .dnc4_slv_r_Ar_Ready(                                              dnc_dbus_s0_arready                                           ),
      .dnc4_slv_r_Ar_Id(                                                 dnc4_slv_r_Ar_Id                                              ),
      .dnc4_slv_r_Ar_Size(                                               dnc4_slv_r_Ar_Size                                            ),
      .dnc4_r_R_Last(                                                    dnc4_r_R_Last                                                 ),
      .dnc4_r_R_Resp(                                                    dnc4_r_R_Resp                                                 ),
      .dnc4_r_R_Valid(                                                   dnc4_r_R_Valid                                                ),
      .dnc4_r_R_User(                                                    dnc4_r_R_User                                                 ),
      .dnc4_r_R_Ready(                                                   dnc_dbus_m0_rready                                            ),
      .dnc4_r_R_Data(                                                    dnc4_r_R_Data                                                 ),
      .dnc4_r_R_Id(                                                      dnc4_r_R_Id                                                   ),
      .dnc4_r_Ar_Prot(                                                   dnc_dbus_m0_arprot                                            ),
      .dnc4_r_Ar_Addr(                                                 { 18'b000000000000000000,
                                                                         dnc_dbus_m0_araddr }                                          ),
      .dnc4_r_Ar_Burst(                                                  dnc_dbus_m0_arburst                                           ),
      .dnc4_r_Ar_Lock(                                                   dnc_dbus_m0_arlock                                            ),
      .dnc4_r_Ar_Cache(                                                  dnc_dbus_m0_arcache                                           ),
      .dnc4_r_Ar_Len(                                                  { 2'b00,
                                                                         dnc_dbus_m0_arlen }                                           ),
      .dnc4_r_Ar_Valid(                                                  dnc_dbus_m0_arvalid                                           ),
      .dnc4_r_Ar_User(                                                   dnc_dbus_m0_aruser                                            ),
      .dnc4_r_Ar_Ready(                                                  dnc4_r_Ar_Ready                                               ),
      .dnc4_r_Ar_Id(                                                     dnc_dbus_m0_arid                                              ),
      .dnc4_r_Ar_Size(                                                   dnc_dbus_m0_arsize                                            ),
      .dcluster1_cc3_slv_r_R_Last(                                       dbus_s3_rlast                                                 ),
      .dcluster1_cc3_slv_r_R_Resp(                                       dbus_s3_rresp                                                 ),
      .dcluster1_cc3_slv_r_R_Valid(                                      dbus_s3_rvalid                                                ),
      .dcluster1_cc3_slv_r_R_User(                                       dbus_s3_ruser                                                 ),
      .dcluster1_cc3_slv_r_R_Ready(                                      dcluster1_cc3_slv_r_R_Ready                                   ),
      .dcluster1_cc3_slv_r_R_Data(                                       dbus_s3_rdata                                                 ),
      .dcluster1_cc3_slv_r_R_Id(                                         dbus_s3_rid                                                   ),
      .dcluster1_cc3_slv_r_Ar_Prot(                                      dcluster1_cc3_slv_r_Ar_Prot                                   ),
      .dcluster1_cc3_slv_r_Ar_Addr(                                      dcluster1_cc3_slv_r_Ar_Addr                                   ),
      .dcluster1_cc3_slv_r_Ar_Burst(                                     dcluster1_cc3_slv_r_Ar_Burst                                  ),
      .dcluster1_cc3_slv_r_Ar_Lock(                                      dcluster1_cc3_slv_r_Ar_Lock                                   ),
      .dcluster1_cc3_slv_r_Ar_Cache(                                     dcluster1_cc3_slv_r_Ar_Cache                                  ),
      .dcluster1_cc3_slv_r_Ar_Len(                                       dcluster1_cc3_slv_r_Ar_Len                                    ),
      .dcluster1_cc3_slv_r_Ar_Valid(                                     dcluster1_cc3_slv_r_Ar_Valid                                  ),
      .dcluster1_cc3_slv_r_Ar_User(                                      dcluster1_cc3_slv_r_Ar_User                                   ),
      .dcluster1_cc3_slv_r_Ar_Ready(                                     dbus_s3_arready                                               ),
      .dcluster1_cc3_slv_r_Ar_Id(                                        dcluster1_cc3_slv_r_Ar_Id                                     ),
      .dcluster1_cc3_slv_r_Ar_Size(                                      dcluster1_cc3_slv_r_Ar_Size                                   ),
      .dcluster1_cc3_r_R_Last(                                           dcluster1_cc3_r_R_Last                                        ),
      .dcluster1_cc3_r_R_Resp(                                           dcluster1_cc3_r_R_Resp                                        ),
      .dcluster1_cc3_r_R_Valid(                                          dcluster1_cc3_r_R_Valid                                       ),
      .dcluster1_cc3_r_R_User(                                           dcluster1_cc3_r_R_User                                        ),
      .dcluster1_cc3_r_R_Ready(                                          dbus_m3_rready                                                ),
      .dcluster1_cc3_r_R_Data(                                           dcluster1_cc3_r_R_Data                                        ),
      .dcluster1_cc3_r_R_Id(                                             dcluster1_cc3_r_R_Id                                          ),
      .dcluster1_cc3_r_Ar_Prot(                                          dbus_m3_arprot                                                ),
      .dcluster1_cc3_r_Ar_Addr(                                          dbus_m3_araddr                                                ),
      .dcluster1_cc3_r_Ar_Burst(                                         dbus_m3_arburst                                               ),
      .dcluster1_cc3_r_Ar_Lock(                                          dbus_m3_arlock                                                ),
      .dcluster1_cc3_r_Ar_Cache(                                         dbus_m3_arcache                                               ),
      .dcluster1_cc3_r_Ar_Len(                                           dbus_m3_arlen                                                 ),
      .dcluster1_cc3_r_Ar_Valid(                                         dbus_m3_arvalid                                               ),
      .dcluster1_cc3_r_Ar_User(                                          4'b0000                                                       ),
      .dcluster1_cc3_r_Ar_Ready(                                         dcluster1_cc3_r_Ar_Ready                                      ),
      .dcluster1_cc3_r_Ar_Id(                                            dbus_m3_arid                                                  ),
      .dcluster1_cc3_r_Ar_Size(                                          dbus_m3_arsize                                                ),
      .dcluster1_cc2_slv_r_R_Last(                                       dbus_s2_rlast                                                 ),
      .dcluster1_cc2_slv_r_R_Resp(                                       dbus_s2_rresp                                                 ),
      .dcluster1_cc2_slv_r_R_Valid(                                      dbus_s2_rvalid                                                ),
      .dcluster1_cc2_slv_r_R_User(                                       dbus_s2_ruser                                                 ),
      .dcluster1_cc2_slv_r_R_Ready(                                      dcluster1_cc2_slv_r_R_Ready                                   ),
      .dcluster1_cc2_slv_r_R_Data(                                       dbus_s2_rdata                                                 ),
      .dcluster1_cc2_slv_r_R_Id(                                         dbus_s2_rid                                                   ),
      .dcluster1_cc2_slv_r_Ar_Prot(                                      dcluster1_cc2_slv_r_Ar_Prot                                   ),
      .dcluster1_cc2_slv_r_Ar_Addr(                                      dcluster1_cc2_slv_r_Ar_Addr                                   ),
      .dcluster1_cc2_slv_r_Ar_Burst(                                     dcluster1_cc2_slv_r_Ar_Burst                                  ),
      .dcluster1_cc2_slv_r_Ar_Lock(                                      dcluster1_cc2_slv_r_Ar_Lock                                   ),
      .dcluster1_cc2_slv_r_Ar_Cache(                                     dcluster1_cc2_slv_r_Ar_Cache                                  ),
      .dcluster1_cc2_slv_r_Ar_Len(                                       dcluster1_cc2_slv_r_Ar_Len                                    ),
      .dcluster1_cc2_slv_r_Ar_Valid(                                     dcluster1_cc2_slv_r_Ar_Valid                                  ),
      .dcluster1_cc2_slv_r_Ar_User(                                      dcluster1_cc2_slv_r_Ar_User                                   ),
      .dcluster1_cc2_slv_r_Ar_Ready(                                     dbus_s2_arready                                               ),
      .dcluster1_cc2_slv_r_Ar_Id(                                        dcluster1_cc2_slv_r_Ar_Id                                     ),
      .dcluster1_cc2_slv_r_Ar_Size(                                      dcluster1_cc2_slv_r_Ar_Size                                   ),
      .dcluster1_cc2_r_R_Last(                                           dcluster1_cc2_r_R_Last                                        ),
      .dcluster1_cc2_r_R_Resp(                                           dcluster1_cc2_r_R_Resp                                        ),
      .dcluster1_cc2_r_R_Valid(                                          dcluster1_cc2_r_R_Valid                                       ),
      .dcluster1_cc2_r_R_User(                                           dcluster1_cc2_r_R_User                                        ),
      .dcluster1_cc2_r_R_Ready(                                          dbus_m2_rready                                                ),
      .dcluster1_cc2_r_R_Data(                                           dcluster1_cc2_r_R_Data                                        ),
      .dcluster1_cc2_r_R_Id(                                             dcluster1_cc2_r_R_Id                                          ),
      .dcluster1_cc2_r_Ar_Prot(                                          dbus_m2_arprot                                                ),
      .dcluster1_cc2_r_Ar_Addr(                                          dbus_m2_araddr                                                ),
      .dcluster1_cc2_r_Ar_Burst(                                         dbus_m2_arburst                                               ),
      .dcluster1_cc2_r_Ar_Lock(                                          dbus_m2_arlock                                                ),
      .dcluster1_cc2_r_Ar_Cache(                                         dbus_m2_arcache                                               ),
      .dcluster1_cc2_r_Ar_Len(                                           dbus_m2_arlen                                                 ),
      .dcluster1_cc2_r_Ar_Valid(                                         dbus_m2_arvalid                                               ),
      .dcluster1_cc2_r_Ar_User(                                          4'b0000                                                       ),
      .dcluster1_cc2_r_Ar_Ready(                                         dcluster1_cc2_r_Ar_Ready                                      ),
      .dcluster1_cc2_r_Ar_Id(                                            dbus_m2_arid                                                  ),
      .dcluster1_cc2_r_Ar_Size(                                          dbus_m2_arsize                                                ),
      .dcluster1_cc1_slv_r_R_Last(                                       dbus_s1_rlast                                                 ),
      .dcluster1_cc1_slv_r_R_Resp(                                       dbus_s1_rresp                                                 ),
      .dcluster1_cc1_slv_r_R_Valid(                                      dbus_s1_rvalid                                                ),
      .dcluster1_cc1_slv_r_R_User(                                       dbus_s1_ruser                                                 ),
      .dcluster1_cc1_slv_r_R_Ready(                                      dcluster1_cc1_slv_r_R_Ready                                   ),
      .dcluster1_cc1_slv_r_R_Data(                                       dbus_s1_rdata                                                 ),
      .dcluster1_cc1_slv_r_R_Id(                                         dbus_s1_rid                                                   ),
      .dcluster1_cc1_slv_r_Ar_Prot(                                      dcluster1_cc1_slv_r_Ar_Prot                                   ),
      .dcluster1_cc1_slv_r_Ar_Addr(                                      dcluster1_cc1_slv_r_Ar_Addr                                   ),
      .dcluster1_cc1_slv_r_Ar_Burst(                                     dcluster1_cc1_slv_r_Ar_Burst                                  ),
      .dcluster1_cc1_slv_r_Ar_Lock(                                      dcluster1_cc1_slv_r_Ar_Lock                                   ),
      .dcluster1_cc1_slv_r_Ar_Cache(                                     dcluster1_cc1_slv_r_Ar_Cache                                  ),
      .dcluster1_cc1_slv_r_Ar_Len(                                       dcluster1_cc1_slv_r_Ar_Len                                    ),
      .dcluster1_cc1_slv_r_Ar_Valid(                                     dcluster1_cc1_slv_r_Ar_Valid                                  ),
      .dcluster1_cc1_slv_r_Ar_User(                                      dcluster1_cc1_slv_r_Ar_User                                   ),
      .dcluster1_cc1_slv_r_Ar_Ready(                                     dbus_s1_arready                                               ),
      .dcluster1_cc1_slv_r_Ar_Id(                                        dcluster1_cc1_slv_r_Ar_Id                                     ),
      .dcluster1_cc1_slv_r_Ar_Size(                                      dcluster1_cc1_slv_r_Ar_Size                                   ),
      .dcluster1_cc1_r_R_Last(                                           dcluster1_cc1_r_R_Last                                        ),
      .dcluster1_cc1_r_R_Resp(                                           dcluster1_cc1_r_R_Resp                                        ),
      .dcluster1_cc1_r_R_Valid(                                          dcluster1_cc1_r_R_Valid                                       ),
      .dcluster1_cc1_r_R_User(                                           dcluster1_cc1_r_R_User                                        ),
      .dcluster1_cc1_r_R_Ready(                                          dbus_m1_rready                                                ),
      .dcluster1_cc1_r_R_Data(                                           dcluster1_cc1_r_R_Data                                        ),
      .dcluster1_cc1_r_R_Id(                                             dcluster1_cc1_r_R_Id                                          ),
      .dcluster1_cc1_r_Ar_Prot(                                          dbus_m1_arprot                                                ),
      .dcluster1_cc1_r_Ar_Addr(                                          dbus_m1_araddr                                                ),
      .dcluster1_cc1_r_Ar_Burst(                                         dbus_m1_arburst                                               ),
      .dcluster1_cc1_r_Ar_Lock(                                          dbus_m1_arlock                                                ),
      .dcluster1_cc1_r_Ar_Cache(                                         dbus_m1_arcache                                               ),
      .dcluster1_cc1_r_Ar_Len(                                           dbus_m1_arlen                                                 ),
      .dcluster1_cc1_r_Ar_Valid(                                         dbus_m1_arvalid                                               ),
      .dcluster1_cc1_r_Ar_User(                                          4'b0000                                                       ),
      .dcluster1_cc1_r_Ar_Ready(                                         dcluster1_cc1_r_Ar_Ready                                      ),
      .dcluster1_cc1_r_Ar_Id(                                            dbus_m1_arid                                                  ),
      .dcluster1_cc1_r_Ar_Size(                                          dbus_m1_arsize                                                ),
      .dcluster1_cc0_slv_r_R_Last(                                       dbus_s0_rlast                                                 ),
      .dcluster1_cc0_slv_r_R_Resp(                                       dbus_s0_rresp                                                 ),
      .dcluster1_cc0_slv_r_R_Valid(                                      dbus_s0_rvalid                                                ),
      .dcluster1_cc0_slv_r_R_User(                                       dbus_s0_ruser                                                 ),
      .dcluster1_cc0_slv_r_R_Ready(                                      dcluster1_cc0_slv_r_R_Ready                                   ),
      .dcluster1_cc0_slv_r_R_Data(                                       dbus_s0_rdata                                                 ),
      .dcluster1_cc0_slv_r_R_Id(                                         dbus_s0_rid                                                   ),
      .dcluster1_cc0_slv_r_Ar_Prot(                                      dcluster1_cc0_slv_r_Ar_Prot                                   ),
      .dcluster1_cc0_slv_r_Ar_Addr(                                      dcluster1_cc0_slv_r_Ar_Addr                                   ),
      .dcluster1_cc0_slv_r_Ar_Burst(                                     dcluster1_cc0_slv_r_Ar_Burst                                  ),
      .dcluster1_cc0_slv_r_Ar_Lock(                                      dcluster1_cc0_slv_r_Ar_Lock                                   ),
      .dcluster1_cc0_slv_r_Ar_Cache(                                     dcluster1_cc0_slv_r_Ar_Cache                                  ),
      .dcluster1_cc0_slv_r_Ar_Len(                                       dcluster1_cc0_slv_r_Ar_Len                                    ),
      .dcluster1_cc0_slv_r_Ar_Valid(                                     dcluster1_cc0_slv_r_Ar_Valid                                  ),
      .dcluster1_cc0_slv_r_Ar_User(                                      dcluster1_cc0_slv_r_Ar_User                                   ),
      .dcluster1_cc0_slv_r_Ar_Ready(                                     dbus_s0_arready                                               ),
      .dcluster1_cc0_slv_r_Ar_Id(                                        dcluster1_cc0_slv_r_Ar_Id                                     ),
      .dcluster1_cc0_slv_r_Ar_Size(                                      dcluster1_cc0_slv_r_Ar_Size                                   ),
      .dcluster1_cc0_r_R_Last(                                           dcluster1_cc0_r_R_Last                                        ),
      .dcluster1_cc0_r_R_Resp(                                           dcluster1_cc0_r_R_Resp                                        ),
      .dcluster1_cc0_r_R_Valid(                                          dcluster1_cc0_r_R_Valid                                       ),
      .dcluster1_cc0_r_R_User(                                           dcluster1_cc0_r_R_User                                        ),
      .dcluster1_cc0_r_R_Ready(                                          dbus_m0_rready                                                ),
      .dcluster1_cc0_r_R_Data(                                           dcluster1_cc0_r_R_Data                                        ),
      .dcluster1_cc0_r_R_Id(                                             dcluster1_cc0_r_R_Id                                          ),
      .dcluster1_cc0_r_Ar_Prot(                                          dbus_m0_arprot                                                ),
      .dcluster1_cc0_r_Ar_Addr(                                          dbus_m0_araddr                                                ),
      .dcluster1_cc0_r_Ar_Burst(                                         dbus_m0_arburst                                               ),
      .dcluster1_cc0_r_Ar_Lock(                                          dbus_m0_arlock                                                ),
      .dcluster1_cc0_r_Ar_Cache(                                         dbus_m0_arcache                                               ),
      .dcluster1_cc0_r_Ar_Len(                                           dbus_m0_arlen                                                 ),
      .dcluster1_cc0_r_Ar_Valid(                                         dbus_m0_arvalid                                               ),
      .dcluster1_cc0_r_Ar_User(                                          4'b0000                                                       ),
      .dcluster1_cc0_r_Ar_Ready(                                         dcluster1_cc0_r_Ar_Ready                                      ),
      .dcluster1_cc0_r_Ar_Id(                                            dbus_m0_arid                                                  ),
      .dcluster1_cc0_r_Ar_Size(                                          dbus_m0_arsize                                                ),
      .clk_dcluster1(                                                    aclk                                                          ),
      .arstn_dcluster1(                                                  arstn                                                         ),
      .TM(                                                               TM                                                            ) 
      );


dbus_write_Structure_Module_dcluster1 u_dbus_write_Structure_Module_dcluster1 (
      .dp_Link_n7_astResp_to_Link_n7_asiResp_RdCnt(                dp_Link_n7_astResp_to_Link_n7_asiResp_rdcnt             ),
      .dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRstAck(    dp_Link_n7_astResp_to_Link_n7_asiResp_rxctl_pwronrstack ),
      .dp_Link_n7_astResp_to_Link_n7_asiResp_RxCtl_PwrOnRst(       dp_Link_n7_astResp_to_Link_n7_asiResp_rxctl_pwronrst    ),
      .dp_Link_n7_astResp_to_Link_n7_asiResp_RdPtr(                dp_Link_n7_astResp_to_Link_n7_asiResp_rdptr             ),
      .dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRstAck(    dp_Link_n7_astResp_to_Link_n7_asiResp_txctl_pwronrstack ),
      .dp_Link_n7_astResp_to_Link_n7_asiResp_TxCtl_PwrOnRst(       dp_Link_n7_astResp_to_Link_n7_asiResp_txctl_pwronrst    ),
      .dp_Link_n7_astResp_to_Link_n7_asiResp_Data(                 dp_Link_n7_astResp_to_Link_n7_asiResp_data              ),
      .dp_Link_n7_astResp_to_Link_n7_asiResp_WrCnt(                dp_Link_n7_astResp_to_Link_n7_asiResp_wrcnt             ),
      .dp_Link_n7_asi_to_Link_n7_ast_RdCnt(                        dp_Link_n7_asi_to_Link_n7_ast_w_rdcnt                   ),
      .dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRstAck(            dp_Link_n7_asi_to_Link_n7_ast_w_rxctl_pwronrstack       ),
      .dp_Link_n7_asi_to_Link_n7_ast_RxCtl_PwrOnRst(               dp_Link_n7_asi_to_Link_n7_ast_w_rxctl_pwronrst          ),
      .dp_Link_n7_asi_to_Link_n7_ast_RdPtr(                        dp_Link_n7_asi_to_Link_n7_ast_w_rdptr                   ),
      .dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRstAck(            dp_Link_n7_asi_to_Link_n7_ast_w_txctl_pwronrstack       ),
      .dp_Link_n7_asi_to_Link_n7_ast_TxCtl_PwrOnRst(               dp_Link_n7_asi_to_Link_n7_ast_w_txctl_pwronrst          ),
      .dp_Link_n7_asi_to_Link_n7_ast_Data(                         dp_Link_n7_asi_to_Link_n7_ast_w_data                    ),
      .dp_Link_n7_asi_to_Link_n7_ast_WrCnt(                        dp_Link_n7_asi_to_Link_n7_ast_w_wrcnt                   ),
      .dp_Link_n6_astResp_to_Link_n6_asiResp_RdCnt(                dp_Link_n6_astResp_to_Link_n6_asiResp_rdcnt             ),
      .dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRstAck(    dp_Link_n6_astResp_to_Link_n6_asiResp_rxctl_pwronrstack ),
      .dp_Link_n6_astResp_to_Link_n6_asiResp_RxCtl_PwrOnRst(       dp_Link_n6_astResp_to_Link_n6_asiResp_rxctl_pwronrst    ),
      .dp_Link_n6_astResp_to_Link_n6_asiResp_RdPtr(                dp_Link_n6_astResp_to_Link_n6_asiResp_rdptr             ),
      .dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRstAck(    dp_Link_n6_astResp_to_Link_n6_asiResp_txctl_pwronrstack ),
      .dp_Link_n6_astResp_to_Link_n6_asiResp_TxCtl_PwrOnRst(       dp_Link_n6_astResp_to_Link_n6_asiResp_txctl_pwronrst    ),
      .dp_Link_n6_astResp_to_Link_n6_asiResp_Data(                 dp_Link_n6_astResp_to_Link_n6_asiResp_data              ),
      .dp_Link_n6_astResp_to_Link_n6_asiResp_WrCnt(                dp_Link_n6_astResp_to_Link_n6_asiResp_wrcnt             ),
      .dp_Link_n6_asi_to_Link_n6_ast_RdCnt(                        dp_Link_n6_asi_to_Link_n6_ast_w_rdcnt                   ),
      .dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRstAck(            dp_Link_n6_asi_to_Link_n6_ast_w_rxctl_pwronrstack       ),
      .dp_Link_n6_asi_to_Link_n6_ast_RxCtl_PwrOnRst(               dp_Link_n6_asi_to_Link_n6_ast_w_rxctl_pwronrst          ),
      .dp_Link_n6_asi_to_Link_n6_ast_RdPtr(                        dp_Link_n6_asi_to_Link_n6_ast_w_rdptr                   ),
      .dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRstAck(            dp_Link_n6_asi_to_Link_n6_ast_w_txctl_pwronrstack       ),
      .dp_Link_n6_asi_to_Link_n6_ast_TxCtl_PwrOnRst(               dp_Link_n6_asi_to_Link_n6_ast_w_txctl_pwronrst          ),
      .dp_Link_n6_asi_to_Link_n6_ast_Data(                         dp_Link_n6_asi_to_Link_n6_ast_w_data                    ),
      .dp_Link_n6_asi_to_Link_n6_ast_WrCnt(                        dp_Link_n6_asi_to_Link_n6_ast_w_wrcnt                   ),
      .dp_Link_n5_astResp_to_Link_n5_asiResp_RdCnt(                dp_Link_n5_astResp_to_Link_n5_asiResp_rdcnt             ),
      .dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRstAck(    dp_Link_n5_astResp_to_Link_n5_asiResp_rxctl_pwronrstack ),
      .dp_Link_n5_astResp_to_Link_n5_asiResp_RxCtl_PwrOnRst(       dp_Link_n5_astResp_to_Link_n5_asiResp_rxctl_pwronrst    ),
      .dp_Link_n5_astResp_to_Link_n5_asiResp_RdPtr(                dp_Link_n5_astResp_to_Link_n5_asiResp_rdptr             ),
      .dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRstAck(    dp_Link_n5_astResp_to_Link_n5_asiResp_txctl_pwronrstack ),
      .dp_Link_n5_astResp_to_Link_n5_asiResp_TxCtl_PwrOnRst(       dp_Link_n5_astResp_to_Link_n5_asiResp_txctl_pwronrst    ),
      .dp_Link_n5_astResp_to_Link_n5_asiResp_Data(                 dp_Link_n5_astResp_to_Link_n5_asiResp_data              ),
      .dp_Link_n5_astResp_to_Link_n5_asiResp_WrCnt(                dp_Link_n5_astResp_to_Link_n5_asiResp_wrcnt             ),
      .dp_Link_n5_asi_to_Link_n5_ast_RdCnt(                        dp_Link_n5_asi_to_Link_n5_ast_w_rdcnt                   ),
      .dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRstAck(            dp_Link_n5_asi_to_Link_n5_ast_w_rxctl_pwronrstack       ),
      .dp_Link_n5_asi_to_Link_n5_ast_RxCtl_PwrOnRst(               dp_Link_n5_asi_to_Link_n5_ast_w_rxctl_pwronrst          ),
      .dp_Link_n5_asi_to_Link_n5_ast_RdPtr(                        dp_Link_n5_asi_to_Link_n5_ast_w_rdptr                   ),
      .dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRstAck(            dp_Link_n5_asi_to_Link_n5_ast_w_txctl_pwronrstack       ),
      .dp_Link_n5_asi_to_Link_n5_ast_TxCtl_PwrOnRst(               dp_Link_n5_asi_to_Link_n5_ast_w_txctl_pwronrst          ),
      .dp_Link_n5_asi_to_Link_n5_ast_Data(                         dp_Link_n5_asi_to_Link_n5_ast_w_data                    ),
      .dp_Link_n5_asi_to_Link_n5_ast_WrCnt(                        dp_Link_n5_asi_to_Link_n5_ast_w_wrcnt                   ),
      .dp_Link_n4_astResp_to_Link_n4_asiResp_RdCnt(                dp_Link_n4_astResp_to_Link_n4_asiResp_rdcnt             ),
      .dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRstAck(    dp_Link_n4_astResp_to_Link_n4_asiResp_rxctl_pwronrstack ),
      .dp_Link_n4_astResp_to_Link_n4_asiResp_RxCtl_PwrOnRst(       dp_Link_n4_astResp_to_Link_n4_asiResp_rxctl_pwronrst    ),
      .dp_Link_n4_astResp_to_Link_n4_asiResp_RdPtr(                dp_Link_n4_astResp_to_Link_n4_asiResp_rdptr             ),
      .dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRstAck(    dp_Link_n4_astResp_to_Link_n4_asiResp_txctl_pwronrstack ),
      .dp_Link_n4_astResp_to_Link_n4_asiResp_TxCtl_PwrOnRst(       dp_Link_n4_astResp_to_Link_n4_asiResp_txctl_pwronrst    ),
      .dp_Link_n4_astResp_to_Link_n4_asiResp_Data(                 dp_Link_n4_astResp_to_Link_n4_asiResp_data              ),
      .dp_Link_n4_astResp_to_Link_n4_asiResp_WrCnt(                dp_Link_n4_astResp_to_Link_n4_asiResp_wrcnt             ),
      .dp_Link_n4_asi_to_Link_n4_ast_RdCnt(                        dp_Link_n4_asi_to_Link_n4_ast_w_rdcnt                   ),
      .dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRstAck(            dp_Link_n4_asi_to_Link_n4_ast_w_rxctl_pwronrstack       ),
      .dp_Link_n4_asi_to_Link_n4_ast_RxCtl_PwrOnRst(               dp_Link_n4_asi_to_Link_n4_ast_w_rxctl_pwronrst          ),
      .dp_Link_n4_asi_to_Link_n4_ast_RdPtr(                        dp_Link_n4_asi_to_Link_n4_ast_w_rdptr                   ),
      .dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRstAck(            dp_Link_n4_asi_to_Link_n4_ast_w_txctl_pwronrstack       ),
      .dp_Link_n4_asi_to_Link_n4_ast_TxCtl_PwrOnRst(               dp_Link_n4_asi_to_Link_n4_ast_w_txctl_pwronrst          ),
      .dp_Link_n4_asi_to_Link_n4_ast_Data(                         dp_Link_n4_asi_to_Link_n4_ast_w_data                    ),
      .dp_Link_n4_asi_to_Link_n4_ast_WrCnt(                        dp_Link_n4_asi_to_Link_n4_ast_w_wrcnt                   ),
      .dnc7_w_B_Ready(                                             dnc_dbus_m0_bready_2                                    ),
      .dnc7_w_B_Resp(                                              dnc7_w_B_Resp                                           ),
      .dnc7_w_B_Valid(                                             dnc7_w_B_Valid                                          ),
      .dnc7_w_B_User(                                              dnc7_w_B_User                                           ),
      .dnc7_w_B_Id(                                                dnc7_w_B_Id                                             ),
      .dnc7_w_Aw_Prot(                                             dnc_dbus_m0_awprot_2                                    ),
      .dnc7_w_Aw_Addr(                                           { 17'b00000000000000000,
                                                                   dnc_dbus_m0_awaddr_2 }                                  ),
      .dnc7_w_Aw_Burst(                                            dnc_dbus_m0_awburst_2                                   ),
      .dnc7_w_Aw_Lock(                                             dnc_dbus_m0_awlock_2                                    ),
      .dnc7_w_Aw_Cache(                                            dnc_dbus_m0_awcache_2                                   ),
      .dnc7_w_Aw_Len(                                            { 2'b00,
                                                                   dnc_dbus_m0_awlen_2 }                                   ),
      .dnc7_w_Aw_Valid(                                            dnc_dbus_m0_awvalid_2                                   ),
      .dnc7_w_Aw_User(                                             dnc_dbus_m0_awuser_2                                    ),
      .dnc7_w_Aw_Ready(                                            dnc7_w_Aw_Ready                                         ),
      .dnc7_w_Aw_Id(                                             { 3'b000,
                                                                   dnc_dbus_m0_awid_2 }                                    ),
      .dnc7_w_Aw_Size(                                             dnc_dbus_m0_awsize_2                                    ),
      .dnc7_w_W_Last(                                              dnc_dbus_m0_wlast_2                                     ),
      .dnc7_w_W_Valid(                                             dnc_dbus_m0_wvalid_2                                    ),
      .dnc7_w_W_Ready(                                             dnc7_w_W_Ready                                          ),
      .dnc7_w_W_Strb(                                              dnc_dbus_m0_wstrb_2                                     ),
      .dnc7_w_W_Data(                                              dnc_dbus_m0_wdata_2                                     ),
      .dnc6_w_B_Ready(                                             dnc_dbus_m0_bready_1                                    ),
      .dnc6_w_B_Resp(                                              dnc6_w_B_Resp                                           ),
      .dnc6_w_B_Valid(                                             dnc6_w_B_Valid                                          ),
      .dnc6_w_B_User(                                              dnc6_w_B_User                                           ),
      .dnc6_w_B_Id(                                                dnc6_w_B_Id                                             ),
      .dnc6_w_Aw_Prot(                                             dnc_dbus_m0_awprot_1                                    ),
      .dnc6_w_Aw_Addr(                                           { 17'b00000000000000000,
                                                                   dnc_dbus_m0_awaddr_1 }                                  ),
      .dnc6_w_Aw_Burst(                                            dnc_dbus_m0_awburst_1                                   ),
      .dnc6_w_Aw_Lock(                                             dnc_dbus_m0_awlock_1                                    ),
      .dnc6_w_Aw_Cache(                                            dnc_dbus_m0_awcache_1                                   ),
      .dnc6_w_Aw_Len(                                            { 2'b00,
                                                                   dnc_dbus_m0_awlen_1 }                                   ),
      .dnc6_w_Aw_Valid(                                            dnc_dbus_m0_awvalid_1                                   ),
      .dnc6_w_Aw_User(                                             dnc_dbus_m0_awuser_1                                    ),
      .dnc6_w_Aw_Ready(                                            dnc6_w_Aw_Ready                                         ),
      .dnc6_w_Aw_Id(                                             { 3'b000,
                                                                   dnc_dbus_m0_awid_1 }                                    ),
      .dnc6_w_Aw_Size(                                             dnc_dbus_m0_awsize_1                                    ),
      .dnc6_w_W_Last(                                              dnc_dbus_m0_wlast_1                                     ),
      .dnc6_w_W_Valid(                                             dnc_dbus_m0_wvalid_1                                    ),
      .dnc6_w_W_Ready(                                             dnc6_w_W_Ready                                          ),
      .dnc6_w_W_Strb(                                              dnc_dbus_m0_wstrb_1                                     ),
      .dnc6_w_W_Data(                                              dnc_dbus_m0_wdata_1                                     ),
      .dnc5_w_B_Ready(                                             dnc_dbus_m0_bready_0                                    ),
      .dnc5_w_B_Resp(                                              dnc5_w_B_Resp                                           ),
      .dnc5_w_B_Valid(                                             dnc5_w_B_Valid                                          ),
      .dnc5_w_B_User(                                              dnc5_w_B_User                                           ),
      .dnc5_w_B_Id(                                                dnc5_w_B_Id                                             ),
      .dnc5_w_Aw_Prot(                                             dnc_dbus_m0_awprot_0                                    ),
      .dnc5_w_Aw_Addr(                                           { 17'b00000000000000000,
                                                                   dnc_dbus_m0_awaddr_0 }                                  ),
      .dnc5_w_Aw_Burst(                                            dnc_dbus_m0_awburst_0                                   ),
      .dnc5_w_Aw_Lock(                                             dnc_dbus_m0_awlock_0                                    ),
      .dnc5_w_Aw_Cache(                                            dnc_dbus_m0_awcache_0                                   ),
      .dnc5_w_Aw_Len(                                            { 2'b00,
                                                                   dnc_dbus_m0_awlen_0 }                                   ),
      .dnc5_w_Aw_Valid(                                            dnc_dbus_m0_awvalid_0                                   ),
      .dnc5_w_Aw_User(                                             dnc_dbus_m0_awuser_0                                    ),
      .dnc5_w_Aw_Ready(                                            dnc5_w_Aw_Ready                                         ),
      .dnc5_w_Aw_Id(                                             { 3'b000,
                                                                   dnc_dbus_m0_awid_0 }                                    ),
      .dnc5_w_Aw_Size(                                             dnc_dbus_m0_awsize_0                                    ),
      .dnc5_w_W_Last(                                              dnc_dbus_m0_wlast_0                                     ),
      .dnc5_w_W_Valid(                                             dnc_dbus_m0_wvalid_0                                    ),
      .dnc5_w_W_Ready(                                             dnc5_w_W_Ready                                          ),
      .dnc5_w_W_Strb(                                              dnc_dbus_m0_wstrb_0                                     ),
      .dnc5_w_W_Data(                                              dnc_dbus_m0_wdata_0                                     ),
      .dnc4_w_B_Ready(                                             dnc_dbus_m0_bready                                      ),
      .dnc4_w_B_Resp(                                              dnc4_w_B_Resp                                           ),
      .dnc4_w_B_Valid(                                             dnc4_w_B_Valid                                          ),
      .dnc4_w_B_User(                                              dnc4_w_B_User                                           ),
      .dnc4_w_B_Id(                                                dnc4_w_B_Id                                             ),
      .dnc4_w_Aw_Prot(                                             dnc_dbus_m0_awprot                                      ),
      .dnc4_w_Aw_Addr(                                           { 17'b00000000000000000,
                                                                   dnc_dbus_m0_awaddr }                                    ),
      .dnc4_w_Aw_Burst(                                            dnc_dbus_m0_awburst                                     ),
      .dnc4_w_Aw_Lock(                                             dnc_dbus_m0_awlock                                      ),
      .dnc4_w_Aw_Cache(                                            dnc_dbus_m0_awcache                                     ),
      .dnc4_w_Aw_Len(                                            { 2'b00,
                                                                   dnc_dbus_m0_awlen }                                     ),
      .dnc4_w_Aw_Valid(                                            dnc_dbus_m0_awvalid                                     ),
      .dnc4_w_Aw_User(                                             dnc_dbus_m0_awuser                                      ),
      .dnc4_w_Aw_Ready(                                            dnc4_w_Aw_Ready                                         ),
      .dnc4_w_Aw_Id(                                             { 3'b000,
                                                                   dnc_dbus_m0_awid }                                      ),
      .dnc4_w_Aw_Size(                                             dnc_dbus_m0_awsize                                      ),
      .dnc4_w_W_Last(                                              dnc_dbus_m0_wlast                                       ),
      .dnc4_w_W_Valid(                                             dnc_dbus_m0_wvalid                                      ),
      .dnc4_w_W_Ready(                                             dnc4_w_W_Ready                                          ),
      .dnc4_w_W_Strb(                                              dnc_dbus_m0_wstrb                                       ),
      .dnc4_w_W_Data(                                              dnc_dbus_m0_wdata                                       ),
      .clk_dcluster1(                                              aclk                                                    ),
      .arstn_dcluster1(                                            arstn                                                   ),
      .TM(                                                         TM                                                      ) 
      );



// constant signals initialisation
endmodule
