
module dnc
#(
	// cbus
	parameter C_AXI_DATA_WIDTH_P      = 32,
	parameter C_AXI_ADDR_WIDTH_P      = 12,
	parameter C_AXI_STRB_WIDTH_P      = (C_AXI_DATA_WIDTH_P/8),
	parameter C_AXI_ID_WIDTH_P        = 4,
	parameter C_AXI_USER_WIDTH_P      = 0,
	parameter C_AXI_LEN_WIDTH_P       = 3,
	// dnc
	parameter DNC_AXI_DATA_WIDTH_P    = 1024,
	parameter DNC_AXI_ADDR_WIDTH_P    = 37,
	parameter DNC_AXI_STRB_WIDTH_P    = (DNC_AXI_DATA_WIDTH_P/8),
	parameter DNC_AXI_ID_WIDTH_P      = 7,
	parameter DNC_AXI_USER_WIDTH_P    = 4,
	parameter DNC_AXI_LEN_WIDTH_P     = 3
)(

    input                               clk,
    input                               arstn,

// -------------------------------------------------------
//  0. Cbus Slave Interface
// -------------------------------------------------------
    output                              dnc_cbus_s0_arready ,
    input                               dnc_cbus_s0_arvalid ,
    input  [C_AXI_ADDR_WIDTH_P-1 :0]    dnc_cbus_s0_araddr  ,
    input  [C_AXI_ID_WIDTH_P-1 :0]      dnc_cbus_s0_arid    ,
    input  [C_AXI_LEN_WIDTH_P-1 :0]     dnc_cbus_s0_arlen   ,// 0~15
    input  [1:0]                        dnc_cbus_s0_arburst ,// 'b01
    input  [3:0]                        dnc_cbus_s0_arcache ,// 'b0
    input                               dnc_cbus_s0_arlock  ,// 'b0
    input  [2:0]                        dnc_cbus_s0_arprot  ,// 'b0
    input  [2:0]                        dnc_cbus_s0_arsize  ,// 'b010

    input                               dnc_cbus_s0_rready  ,
    output                              dnc_cbus_s0_rvalid  ,
    output                              dnc_cbus_s0_rlast   ,
    output [C_AXI_ID_WIDTH_P-1 :0]      dnc_cbus_s0_rid     ,
    output [C_AXI_DATA_WIDTH_P-1 :0]    dnc_cbus_s0_rdata   ,
    output [1:0]                        dnc_cbus_s0_rresp   ,

    output                              dnc_cbus_s0_awready ,
    input                               dnc_cbus_s0_awvalid ,
    input  [C_AXI_ADDR_WIDTH_P-1 :0]    dnc_cbus_s0_awaddr  ,
    input  [C_AXI_ID_WIDTH_P-1 :0]      dnc_cbus_s0_awid    ,
    input  [C_AXI_LEN_WIDTH_P-1:0]       dnc_cbus_s0_awlen   ,
    input  [1:0]                        dnc_cbus_s0_awburst ,
    input  [3:0]                        dnc_cbus_s0_awcache ,
    input                               dnc_cbus_s0_awlock  ,
    input  [2:0]                        dnc_cbus_s0_awprot  ,
    input  [2:0]                        dnc_cbus_s0_awsize  ,

    output                              dnc_cbus_s0_wready  ,
    input                               dnc_cbus_s0_wvalid  ,
    input  [C_AXI_DATA_WIDTH_P-1 :0]    dnc_cbus_s0_wdata   ,
    input                               dnc_cbus_s0_wlast   ,
    input  [C_AXI_STRB_WIDTH_P-1 :0]    dnc_cbus_s0_wstrb   ,

    input                               dnc_cbus_s0_bready  ,
    output [C_AXI_ID_WIDTH_P-1 :0]      dnc_cbus_s0_bid     ,
    output [1:0]                        dnc_cbus_s0_bresp   ,
    output                              dnc_cbus_s0_bvalid  ,


// -------------------------------------------------------
//  1. Dbus Slave Interface (Read Only)
//  s0 : Main data channel
// -------------------------------------------------------
    output                              dnc_dbus_s0_arready ,
    input                               dnc_dbus_s0_arvalid ,
    input  [DNC_AXI_ADDR_WIDTH_P-1 :0]  dnc_dbus_s0_araddr  ,
    input  [DNC_AXI_ID_WIDTH_P-1 :0]    dnc_dbus_s0_arid    ,
    input  [DNC_AXI_LEN_WIDTH_P-1 :0]   dnc_dbus_s0_arlen   ,
    input  [1:0]                        dnc_dbus_s0_arburst ,
    input  [3:0]                        dnc_dbus_s0_arcache ,
    input                               dnc_dbus_s0_arlock  ,
    input  [2:0]                        dnc_dbus_s0_arprot  ,
    input  [2:0]                        dnc_dbus_s0_arsize  ,
    input  [DNC_AXI_USER_WIDTH_P-1 :0]  dnc_dbus_s0_aruser  ,

    input                               dnc_dbus_s0_rready  ,// always 1
    output                              dnc_dbus_s0_rvalid  ,
    output                              dnc_dbus_s0_rlast   ,
    output [DNC_AXI_ID_WIDTH_P-1 :0]    dnc_dbus_s0_rid     ,
    output [DNC_AXI_DATA_WIDTH_P-1 :0]  dnc_dbus_s0_rdata   ,
    output [DNC_AXI_USER_WIDTH_P-1 :0]  dnc_dbus_s0_ruser   ,
    output [1:0]                        dnc_dbus_s0_rresp   ,

// -------------------------------------------------------
//  2. Dbus Master Interface
//  m0 : NC LU/SU
//  m1 : uDMA
// -------------------------------------------------------

//  m0 : NC LU/SU
    input                               dnc_dbus_m0_arready ,
    output                              dnc_dbus_m0_arvalid ,
    output [DNC_AXI_ADDR_WIDTH_P-1 :0]  dnc_dbus_m0_araddr  ,
    output [DNC_AXI_ID_WIDTH_P-1 :0]    dnc_dbus_m0_arid    ,
    output [DNC_AXI_LEN_WIDTH_P-1 :0]   dnc_dbus_m0_arlen   ,// 0 or 3
    output [1:0]                        dnc_dbus_m0_arburst ,// 'b01
    output [3:0]                        dnc_dbus_m0_arcache ,// 'b0
    output                              dnc_dbus_m0_arlock  ,// 'b0
    output [2:0]                        dnc_dbus_m0_arprot  ,// 'b0
    output [2:0]                        dnc_dbus_m0_arsize  ,// 'b111
    output [DNC_AXI_USER_WIDTH_P-1 :0]  dnc_dbus_m0_aruser  ,

    output                              dnc_dbus_m0_rready  ,// always 1
    input                               dnc_dbus_m0_rvalid  ,
    input                               dnc_dbus_m0_rlast   ,
    input  [DNC_AXI_ID_WIDTH_P-1 :0]    dnc_dbus_m0_rid     ,
    input  [DNC_AXI_DATA_WIDTH_P-1 :0]  dnc_dbus_m0_rdata   ,
    input  [DNC_AXI_USER_WIDTH_P-1 :0]  dnc_dbus_m0_ruser   ,
    input  [1:0]                        dnc_dbus_m0_rresp   ,

    input                               dnc_dbus_m0_awready ,
    output [DNC_AXI_ADDR_WIDTH_P-1:0]   dnc_dbus_m0_awaddr  ,
    output [1:0]                        dnc_dbus_m0_awburst ,
    output [3:0]                        dnc_dbus_m0_awcache ,
    output [DNC_AXI_USER_WIDTH_P-1:0]   dnc_dbus_m0_awid    ,
    output [DNC_AXI_LEN_WIDTH_P-1:0]    dnc_dbus_m0_awlen   ,
    output                              dnc_dbus_m0_awlock  ,
    output [2:0]                        dnc_dbus_m0_awprot  ,
    output [2:0]                        dnc_dbus_m0_awsize  ,
    output [DNC_AXI_USER_WIDTH_P-1:0]   dnc_dbus_m0_awuser  ,
    output                              dnc_dbus_m0_awvalid ,

    output                              dnc_dbus_m0_bready  ,
    input  [DNC_AXI_ID_WIDTH_P-1:0]     dnc_dbus_m0_bid     ,
    input  [1:0]                        dnc_dbus_m0_bresp   ,
    input  [DNC_AXI_USER_WIDTH_P-1:0]   dnc_dbus_m0_buser   ,
    input                               dnc_dbus_m0_bvalid  ,

    input                               dnc_dbus_m0_wready  ,
    output [DNC_AXI_DATA_WIDTH_P-1:0]   dnc_dbus_m0_wdata   ,
    output                              dnc_dbus_m0_wlast   ,
    output [DNC_AXI_STRB_WIDTH_P-1:0]   dnc_dbus_m0_wstrb   ,
    output                              dnc_dbus_m0_wvalid

);
    assign dnc_cbus_s0_arready  = '1;
    assign dnc_cbus_s0_rvalid   = '0;
    assign dnc_cbus_s0_rlast    = '0;
    assign dnc_cbus_s0_rid      = '0;
    assign dnc_cbus_s0_rdata    = '0;
    assign dnc_cbus_s0_rresp    = '0;
    assign dnc_cbus_s0_awready  = '1;
    assign dnc_cbus_s0_wready   = '1;
    assign dnc_cbus_s0_bid      = '0;
    assign dnc_cbus_s0_bresp    = '0;
    assign dnc_cbus_s0_bvalid   = '0;
    assign dnc_dbus_s0_arready  = '1;
    assign dnc_dbus_s0_rvalid   = '0;
    assign dnc_dbus_s0_rlast    = '0;
    assign dnc_dbus_s0_rid      = '0;
    assign dnc_dbus_s0_rdata    = '0;
    assign dnc_dbus_s0_ruser    = '0;
    assign dnc_dbus_s0_rresp    = '0;


    assign dnc_dbus_m0_arvalid  = '0;
    assign dnc_dbus_m0_araddr   = '0;
    assign dnc_dbus_m0_arid     = '0;
    assign dnc_dbus_m0_arlen    = '0;// 0 or 3
    assign dnc_dbus_m0_arburst  = '0;// 'b01
    assign dnc_dbus_m0_arcache  = '0;// 'b0
    assign dnc_dbus_m0_arlock   = '0;// 'b0
    assign dnc_dbus_m0_arprot   = '0;// 'b0
    assign dnc_dbus_m0_arsize   = '0;// 'b111
    assign dnc_dbus_m0_aruser   = '0;
    assign dnc_dbus_m0_rready   = '1;// always 1
    assign dnc_dbus_m0_awaddr   = '0;
    assign dnc_dbus_m0_awburst  = '0;
    assign dnc_dbus_m0_awcache  = '0;
    assign dnc_dbus_m0_awid     = '0;
    assign dnc_dbus_m0_awlen    = '0;
    assign dnc_dbus_m0_awlock   = '0;
    assign dnc_dbus_m0_awprot   = '0;
    assign dnc_dbus_m0_awsize   = '0;
    assign dnc_dbus_m0_awuser   = '0;
    assign dnc_dbus_m0_awvalid  = '0;
    assign dnc_dbus_m0_bready   = '1;
    assign dnc_dbus_m0_wdata    = '0;
    assign dnc_dbus_m0_wlast    = '0;
    assign dnc_dbus_m0_wstrb    = '0;
    assign dnc_dbus_m0_wvalid   = '0;
endmodule

