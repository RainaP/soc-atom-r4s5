
module atom_dcluster_dcc#(
	// cbus
	parameter C_AXI_DATA_WIDTH_P      = 32,
	parameter C_AXI_ADDR_WIDTH_P      = 12,
	parameter C_AXI_STRB_WIDTH_P      = (C_AXI_DATA_WIDTH_P/8), 
	parameter C_AXI_ID_WIDTH_P        = 4,
	parameter C_AXI_USER_WIDTH_P      = 0, 
	parameter C_AXI_LEN_WIDTH_P       = 3,

	// dbus
	parameter D_AXI_DATA_WIDTH_P      = 1024,
	parameter D_AXI_ADDR_WIDTH_P      = 37,
	parameter D_AXI_STRB_WIDTH_P      = (D_AXI_DATA_WIDTH_P/8), 
	parameter D_AXI_ID_WIDTH_P        = 7,
	parameter D_AXI_USER_WIDTH_P      = 4,
	parameter D_AXI_LEN_WIDTH_P       = 5

)(

    input                               clk,
    input                               arstn,

    //ctrl_axi_slave
    output                              cbus_s0_arready ,
    input                               cbus_s0_arvalid ,
    input  [C_AXI_ADDR_WIDTH_P-1 :0]    cbus_s0_araddr  ,
    input  [C_AXI_ID_WIDTH_P-1 :0]      cbus_s0_arid    ,
    input  [C_AXI_LEN_WIDTH_P-1 :0]     cbus_s0_arlen   ,// 0~15
    input  [1:0]                        cbus_s0_arburst ,// 'b01
    input  [3:0]                        cbus_s0_arcache ,// 'b0
    input                               cbus_s0_arlock  ,// 'b0
    input  [2:0]                        cbus_s0_arprot  ,// 'b0
    input  [2:0]                        cbus_s0_arsize  ,// 'b010
                                                         
    input                               cbus_s0_rready  ,
    output                              cbus_s0_rvalid  ,
    output                              cbus_s0_rlast   ,
    output [C_AXI_ID_WIDTH_P-1 :0]      cbus_s0_rid     ,
    output [C_AXI_DATA_WIDTH_P-1 :0]    cbus_s0_rdata   ,
    output [1:0]                        cbus_s0_rresp   ,
                                                         
    output                              cbus_s0_awready ,
    input                               cbus_s0_awvalid ,
    input  [C_AXI_ADDR_WIDTH_P-1 :0]    cbus_s0_awaddr  ,
    input  [C_AXI_ID_WIDTH_P-1 :0]      cbus_s0_awid    ,
    input  [C_AXI_LEN_WIDTH_P-1 :0]       cbus_s0_awlen   ,
    input  [1:0]                        cbus_s0_awburst ,
    input  [3:0]                        cbus_s0_awcache ,
    input                               cbus_s0_awlock  ,
    input  [2:0]                        cbus_s0_awprot  ,
    input  [2:0]                        cbus_s0_awsize  ,
                                                         
    output                              cbus_s0_wready  ,
    input                               cbus_s0_wvalid  ,
    input  [C_AXI_DATA_WIDTH_P-1 :0]    cbus_s0_wdata   ,
    input                               cbus_s0_wlast   ,
    input  [C_AXI_STRB_WIDTH_P-1 :0]    cbus_s0_wstrb   ,
                                                         
    input                               cbus_s0_bready  ,
    output [C_AXI_ID_WIDTH_P-1 :0]      cbus_s0_bid     ,
    output [1:0]                        cbus_s0_bresp   ,
    output                              cbus_s0_bvalid  ,



    //data_axi_slave (DNC0~3 flexNoC->Cluster Cache)
    output                              dbus_s0_arready ,
    input                               dbus_s0_arvalid ,
    input  [D_AXI_ADDR_WIDTH_P-1 :0]    dbus_s0_araddr  ,
    input  [D_AXI_ID_WIDTH_P-1 :0]      dbus_s0_arid    ,
    input  [D_AXI_LEN_WIDTH_P-1 :0]     dbus_s0_arlen   ,
    input  [1:0]                        dbus_s0_arburst ,
    input  [3:0]                        dbus_s0_arcache ,
    input                               dbus_s0_arlock  ,
    input  [2:0]                        dbus_s0_arprot  ,
    input  [2:0]                        dbus_s0_arsize  ,
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_s0_aruser  ,
                                                         
    input                               dbus_s0_rready  ,// always 1
    output                              dbus_s0_rvalid  ,
    output                              dbus_s0_rlast   ,
    output [D_AXI_ID_WIDTH_P-1 :0]      dbus_s0_rid     ,
    output [D_AXI_DATA_WIDTH_P-1 :0]    dbus_s0_rdata   ,
    output [D_AXI_USER_WIDTH_P-1 :0]    dbus_s0_ruser   ,
    output [1:0]                        dbus_s0_rresp   ,

    //slave1
    output                              dbus_s1_arready ,
    input                               dbus_s1_arvalid ,
    input  [D_AXI_ADDR_WIDTH_P-1 :0]    dbus_s1_araddr  ,
    input  [D_AXI_ID_WIDTH_P-1 :0]      dbus_s1_arid    ,
    input  [D_AXI_LEN_WIDTH_P-1 :0]     dbus_s1_arlen   ,
    input  [1:0]                        dbus_s1_arburst ,
    input  [3:0]                        dbus_s1_arcache ,
    input                               dbus_s1_arlock  ,
    input  [2:0]                        dbus_s1_arprot  ,
    input  [2:0]                        dbus_s1_arsize  ,
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_s1_aruser  ,
                                                         
    input                               dbus_s1_rready  ,// always 1
    output                              dbus_s1_rvalid  ,
    output                              dbus_s1_rlast   ,
    output [D_AXI_ID_WIDTH_P-1 :0]      dbus_s1_rid     ,
    output [D_AXI_DATA_WIDTH_P-1 :0]    dbus_s1_rdata   ,
    output [D_AXI_USER_WIDTH_P-1 :0]    dbus_s1_ruser   ,
    output [1:0]                        dbus_s1_rresp   ,
    //slave2
    output                              dbus_s2_arready ,
    input                               dbus_s2_arvalid ,
    input  [D_AXI_ADDR_WIDTH_P-1 :0]    dbus_s2_araddr  ,
    input  [D_AXI_ID_WIDTH_P-1 :0]      dbus_s2_arid    ,
    input  [D_AXI_LEN_WIDTH_P-1 :0]     dbus_s2_arlen   ,
    input  [1:0]                        dbus_s2_arburst ,
    input  [3:0]                        dbus_s2_arcache ,
    input                               dbus_s2_arlock  ,
    input  [2:0]                        dbus_s2_arprot  ,
    input  [2:0]                        dbus_s2_arsize  ,
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_s2_aruser  ,
                                                         
    input                               dbus_s2_rready  ,// always 1
    output                              dbus_s2_rvalid  ,
    output                              dbus_s2_rlast   ,
    output [D_AXI_ID_WIDTH_P-1 :0]      dbus_s2_rid     ,
    output [D_AXI_DATA_WIDTH_P-1 :0]    dbus_s2_rdata   ,
    output [D_AXI_USER_WIDTH_P-1 :0]    dbus_s2_ruser   ,
    output [1:0]                        dbus_s2_rresp   ,
    //slave3
    output                              dbus_s3_arready ,
    input                               dbus_s3_arvalid ,
    input  [D_AXI_ADDR_WIDTH_P-1 :0]    dbus_s3_araddr  ,
    input  [D_AXI_ID_WIDTH_P-1 :0]      dbus_s3_arid    ,
    input  [D_AXI_LEN_WIDTH_P-1 :0]     dbus_s3_arlen   ,
    input  [1:0]                        dbus_s3_arburst ,
    input  [3:0]                        dbus_s3_arcache ,
    input                               dbus_s3_arlock  ,
    input  [2:0]                        dbus_s3_arprot  ,
    input  [2:0]                        dbus_s3_arsize  ,
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_s3_aruser  ,
                                                         
    input                               dbus_s3_rready  ,// always 1
    output                              dbus_s3_rvalid  ,
    output                              dbus_s3_rlast   ,
    output [D_AXI_ID_WIDTH_P-1 :0]      dbus_s3_rid     ,
    output [D_AXI_DATA_WIDTH_P-1 :0]    dbus_s3_rdata   ,
    output [D_AXI_USER_WIDTH_P-1 :0]    dbus_s3_ruser   ,
    output [1:0]                        dbus_s3_rresp   ,
           
                                             
    //data_axi_master (Cluster Cache -> flexNoC of SHM&DRAM)                                   
    input                               dbus_m0_arready ,
    output                              dbus_m0_arvalid ,
    output [D_AXI_ADDR_WIDTH_P-1 :0]    dbus_m0_araddr  ,
    output [D_AXI_ID_WIDTH_P-1 :0]      dbus_m0_arid    ,
    output [D_AXI_LEN_WIDTH_P-1 :0]     dbus_m0_arlen   ,// 0 or 3
    output [1:0]                        dbus_m0_arburst ,// 'b01
    output [3:0]                        dbus_m0_arcache ,// 'b0
    output                              dbus_m0_arlock  ,// 'b0
    output [2:0]                        dbus_m0_arprot  ,// 'b0
    output [2:0]                        dbus_m0_arsize  ,// 'b111
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_m0_aruser  ,
                                                         
    output                              dbus_m0_rready  ,// always 1
    input                               dbus_m0_rvalid  ,
    input                               dbus_m0_rlast   ,
    input  [D_AXI_ID_WIDTH_P-1 :0]      dbus_m0_rid     ,
    input  [D_AXI_DATA_WIDTH_P-1 :0]    dbus_m0_rdata   ,
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_m0_ruser   ,
    input  [1:0]                        dbus_m0_rresp   ,
    //master1                                    
    input                               dbus_m1_arready ,
    output                              dbus_m1_arvalid ,
    output [D_AXI_ADDR_WIDTH_P-1 :0]    dbus_m1_araddr  ,
    output [D_AXI_ID_WIDTH_P-1 :0]      dbus_m1_arid    ,
    output [D_AXI_LEN_WIDTH_P-1 :0]     dbus_m1_arlen   ,// 0 or 3
    output [1:0]                        dbus_m1_arburst ,// 'b01
    output [3:0]                        dbus_m1_arcache ,// 'b0
    output                              dbus_m1_arlock  ,// 'b0
    output [2:0]                        dbus_m1_arprot  ,// 'b0
    output [2:0]                        dbus_m1_arsize  ,// 'b111
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_m1_aruser  ,
                                                         
    output                              dbus_m1_rready  ,// always 1
    input                               dbus_m1_rvalid  ,
    input                               dbus_m1_rlast   ,
    input  [D_AXI_ID_WIDTH_P-1 :0]      dbus_m1_rid     ,
    input  [D_AXI_DATA_WIDTH_P-1 :0]    dbus_m1_rdata   ,
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_m1_ruser   ,
    input  [1:0]                        dbus_m1_rresp   ,
    //master2                                    
    input                               dbus_m2_arready ,
    output                              dbus_m2_arvalid ,
    output [D_AXI_ADDR_WIDTH_P-1 :0]    dbus_m2_araddr  ,
    output [D_AXI_ID_WIDTH_P-1 :0]      dbus_m2_arid    ,
    output [D_AXI_LEN_WIDTH_P-1 :0]     dbus_m2_arlen   ,// 0 or 3
    output [1:0]                        dbus_m2_arburst ,// 'b01
    output [3:0]                        dbus_m2_arcache ,// 'b0
    output                              dbus_m2_arlock  ,// 'b0
    output [2:0]                        dbus_m2_arprot  ,// 'b0
    output [2:0]                        dbus_m2_arsize  ,// 'b111
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_m2_aruser  ,
                                                         
    output                              dbus_m2_rready  ,// always 1
    input                               dbus_m2_rvalid  ,
    input                               dbus_m2_rlast   ,
    input  [D_AXI_ID_WIDTH_P-1 :0]      dbus_m2_rid     ,
    input  [D_AXI_DATA_WIDTH_P-1 :0]    dbus_m2_rdata   ,
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_m2_ruser   ,
    input  [1:0]                        dbus_m2_rresp   ,
    //master3                                    
    input                               dbus_m3_arready ,
    output                              dbus_m3_arvalid ,
    output [D_AXI_ADDR_WIDTH_P-1 :0]    dbus_m3_araddr  ,
    output [D_AXI_ID_WIDTH_P-1 :0]      dbus_m3_arid    ,
    output [D_AXI_LEN_WIDTH_P-1 :0]     dbus_m3_arlen   ,// 0 or 3
    output [1:0]                        dbus_m3_arburst ,// 'b01
    output [3:0]                        dbus_m3_arcache ,// 'b0
    output                              dbus_m3_arlock  ,// 'b0
    output [2:0]                        dbus_m3_arprot  ,// 'b0
    output [2:0]                        dbus_m3_arsize  ,// 'b111
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_m3_aruser  ,
                                                         
    output                              dbus_m3_rready  ,// always 1
    input                               dbus_m3_rvalid  ,
    input                               dbus_m3_rlast   ,
    input  [D_AXI_ID_WIDTH_P-1 :0]      dbus_m3_rid     ,
    input  [D_AXI_DATA_WIDTH_P-1 :0]    dbus_m3_rdata   ,
    input  [D_AXI_USER_WIDTH_P-1 :0]    dbus_m3_ruser   ,
    input  [1:0]                        dbus_m3_rresp   
);







    //ctrl_axi_slave
    assign cbus_s0_arready  = '1;
    assign cbus_s0_rvalid   = '0;
    assign cbus_s0_rlast    = '0;
    assign cbus_s0_rid      = '0;
    assign cbus_s0_rdata    = '0;
    assign cbus_s0_rresp    = '0;
    assign cbus_s0_awready  = '1;
    assign cbus_s0_wready   = '1;
    assign cbus_s0_bid      = '0;
    assign cbus_s0_bresp    = '0;
    assign cbus_s0_bvalid   = '0;
    assign dbus_s0_arready  = '1;
    assign dbus_s0_rvalid   = '0;
    assign dbus_s0_rlast    = '0;
    assign dbus_s0_rid      = '0;
    assign dbus_s0_rdata    = '0;
    assign dbus_s0_ruser    = '0;
    assign dbus_s0_rresp    = '0;
    assign dbus_s1_arready  = '1;
    assign dbus_s1_rvalid   = '0;
    assign dbus_s1_rlast    = '0;
    assign dbus_s1_rid      = '0;
    assign dbus_s1_rdata    = '0;
    assign dbus_s1_ruser    = '0;
    assign dbus_s1_rresp    = '0;
    assign dbus_s2_arready  = '1;
    assign dbus_s2_rvalid   = '0;
    assign dbus_s2_rlast    = '0;
    assign dbus_s2_rid      = '0;
    assign dbus_s2_rdata    = '0;
    assign dbus_s2_ruser    = '0;
    assign dbus_s2_rresp    = '0;
    assign dbus_s3_arready  = '1;
    assign dbus_s3_rvalid   = '0;
    assign dbus_s3_rlast    = '0;
    assign dbus_s3_rid      = '0;
    assign dbus_s3_rdata    = '0;
    assign dbus_s3_ruser    = '0;
    assign dbus_s3_rresp    = '0;
    assign dbus_m0_arvalid  = '0;
    assign dbus_m0_araddr   = '0;
    assign dbus_m0_arid     = '0;
    assign dbus_m0_arlen    = '0;// 0 or 3
    assign dbus_m0_arburst  = '0;// 'b01
    assign dbus_m0_arcache  = '0;// 'b0
    assign dbus_m0_arlock   = '0;// 'b0
    assign dbus_m0_arprot   = '0;// 'b0
    assign dbus_m0_arsize   = '0;// 'b111
    assign dbus_m0_rready   = '1;// always 1
    assign dbus_m1_arvalid  = '0;
    assign dbus_m1_araddr   = '0;
    assign dbus_m1_arid     = '0;
    assign dbus_m1_arlen    = '0;// 0 or 3
    assign dbus_m1_arburst  = '0;// 'b01
    assign dbus_m1_arcache  = '0;// 'b0
    assign dbus_m1_arlock   = '0;// 'b0
    assign dbus_m1_arprot   = '0;// 'b0
    assign dbus_m1_arsize   = '0;// 'b111
    assign dbus_m1_rready   = '1;// always 1
    assign dbus_m2_arvalid  = '0;
    assign dbus_m2_araddr   = '0;
    assign dbus_m2_arid     = '0;
    assign dbus_m2_arlen    = '0;// 0 or 3
    assign dbus_m2_arburst  = '0;// 'b01
    assign dbus_m2_arcache  = '0;// 'b0
    assign dbus_m2_arlock   = '0;// 'b0
    assign dbus_m2_arprot   = '0;// 'b0
    assign dbus_m2_arsize   = '0;// 'b111
    assign dbus_m2_rready   = '1;// always 1
    assign dbus_m3_arvalid  = '0;
    assign dbus_m3_araddr   = '0;
    assign dbus_m3_arid     = '0;
    assign dbus_m3_arlen    = '0;// 0 or 3
    assign dbus_m3_arburst  = '0;// 'b01
    assign dbus_m3_arcache  = '0;// 'b0
    assign dbus_m3_arlock   = '0;// 'b0
    assign dbus_m3_arprot   = '0;// 'b0
    assign dbus_m3_arsize   = '0;// 'b111
    assign dbus_m3_rready   = '1;// always 1















endmodule

