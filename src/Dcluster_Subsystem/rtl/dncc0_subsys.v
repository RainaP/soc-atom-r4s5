// This file is generated from Magillem
// Generation : Wed Aug 11 13:20:04 KST 2021 by miock from project atom
// Component : rebellions atom dncc0_subsys 0.0
// Design : rebellions atom dncc0_subsys_arch 0.0
//  u_dnc0                                  rebellions  atom    dnc                                   0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_0.0.xml 
//  u_dnc1                                  rebellions  atom    dnc                                   0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_0.0.xml 
//  u_dnc2                                  rebellions  atom    dnc                                   0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_0.0.xml 
//  u_dnc3                                  rebellions  atom    dnc                                   0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_0.0.xml 
//  u_dnc_cc                                rebellions  atom    dnc_cc                                0.0   /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/rebellions_atom_dnc_cc_0.0.xml 
//  u_cbus_Structure_Module_dcluster0       arteris.com FLEXNOC cbus_Structure_Module_dcluster0       4.5.1 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/cbus_Structure_Module_dcluster0_1.xml 
//  u_dbus_read_Structure_Module_dcluster0  arteris.com FLEXNOC dbus_read_Structure_Module_dcluster0  4.5.1 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/dbus_read_Structure_Module_dcluster0_1.xml 
//  u_dbus_write_Structure_Module_dcluster0 arteris.com FLEXNOC dbus_write_Structure_Module_dcluster0 4.5.1 /home/miock/projects/atom_tools/1.MAGILLEM/pre_alpha/atom/dbus_write_Structure_Module_dcluster0_1.xml 
// Magillem Release : 5.2021.1


module dncc0_subsys(
   input  wire          TM,
   input  wire          aclk,
   input  wire          arstn,
   input  wire [2:0]    dp_Link_n03_astResp001_to_Link_n03_asiResp001_rdcnt,
   input  wire          dp_Link_n03_astResp001_to_Link_n03_asiResp001_rxctl_pwronrstack,
   output wire          dp_Link_n03_astResp001_to_Link_n03_asiResp001_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n03_astResp001_to_Link_n03_asiResp001_rdptr,
   output wire          dp_Link_n03_astResp001_to_Link_n03_asiResp001_txctl_pwronrstack,
   input  wire          dp_Link_n03_astResp001_to_Link_n03_asiResp001_txctl_pwronrst,
   output wire [57:0]   dp_Link_n03_astResp001_to_Link_n03_asiResp001_data,
   output wire [2:0]    dp_Link_n03_astResp001_to_Link_n03_asiResp001_wrcnt,
   output wire [2:0]    dp_Link_n03_asi_to_Link_n03_ast_rdcnt,
   output wire          dp_Link_n03_asi_to_Link_n03_ast_rxctl_pwronrstack,
   input  wire          dp_Link_n03_asi_to_Link_n03_ast_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n03_asi_to_Link_n03_ast_rdptr,
   input  wire          dp_Link_n03_asi_to_Link_n03_ast_txctl_pwronrstack,
   output wire          dp_Link_n03_asi_to_Link_n03_ast_txctl_pwronrst,
   input  wire [57:0]   dp_Link_n03_asi_to_Link_n03_ast_data,
   input  wire [2:0]    dp_Link_n03_asi_to_Link_n03_ast_wrcnt,
   output wire [2:0]    dp_Link_n3_astResp001_to_Link_n3_asiResp001_rdcnt,
   output wire          dp_Link_n3_astResp001_to_Link_n3_asiResp001_rxctl_pwronrstack,
   input  wire          dp_Link_n3_astResp001_to_Link_n3_asiResp001_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n3_astResp001_to_Link_n3_asiResp001_rdptr,
   input  wire          dp_Link_n3_astResp001_to_Link_n3_asiResp001_txctl_pwronrstack,
   output wire          dp_Link_n3_astResp001_to_Link_n3_asiResp001_txctl_pwronrst,
   input  wire [1281:0] dp_Link_n3_astResp001_to_Link_n3_asiResp001_data,
   input  wire [2:0]    dp_Link_n3_astResp001_to_Link_n3_asiResp001_wrcnt,
   input  wire [2:0]    dp_Link_n3_asi_to_Link_n3_ast_r_rdcnt,
   input  wire          dp_Link_n3_asi_to_Link_n3_ast_r_rxctl_pwronrstack,
   output wire          dp_Link_n3_asi_to_Link_n3_ast_r_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n3_asi_to_Link_n3_ast_r_rdptr,
   output wire          dp_Link_n3_asi_to_Link_n3_ast_r_txctl_pwronrstack,
   input  wire          dp_Link_n3_asi_to_Link_n3_ast_r_txctl_pwronrst,
   output wire [128:0]  dp_Link_n3_asi_to_Link_n3_ast_r_data,
   output wire [2:0]    dp_Link_n3_asi_to_Link_n3_ast_r_wrcnt,
   output wire [2:0]    dp_Link_n2_astResp001_to_Link_n2_asiResp001_rdcnt,
   output wire          dp_Link_n2_astResp001_to_Link_n2_asiResp001_rxctl_pwronrstack,
   input  wire          dp_Link_n2_astResp001_to_Link_n2_asiResp001_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n2_astResp001_to_Link_n2_asiResp001_rdptr,
   input  wire          dp_Link_n2_astResp001_to_Link_n2_asiResp001_txctl_pwronrstack,
   output wire          dp_Link_n2_astResp001_to_Link_n2_asiResp001_txctl_pwronrst,
   input  wire [1281:0] dp_Link_n2_astResp001_to_Link_n2_asiResp001_data,
   input  wire [2:0]    dp_Link_n2_astResp001_to_Link_n2_asiResp001_wrcnt,
   input  wire [2:0]    dp_Link_n2_asi_to_Link_n2_ast_r_rdcnt,
   input  wire          dp_Link_n2_asi_to_Link_n2_ast_r_rxctl_pwronrstack,
   output wire          dp_Link_n2_asi_to_Link_n2_ast_r_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n2_asi_to_Link_n2_ast_r_rdptr,
   output wire          dp_Link_n2_asi_to_Link_n2_ast_r_txctl_pwronrstack,
   input  wire          dp_Link_n2_asi_to_Link_n2_ast_r_txctl_pwronrst,
   output wire [128:0]  dp_Link_n2_asi_to_Link_n2_ast_r_data,
   output wire [2:0]    dp_Link_n2_asi_to_Link_n2_ast_r_wrcnt,
   output wire [2:0]    dp_Link_n1_astResp001_to_Link_n1_asiResp001_rdcnt,
   output wire          dp_Link_n1_astResp001_to_Link_n1_asiResp001_rxctl_pwronrstack,
   input  wire          dp_Link_n1_astResp001_to_Link_n1_asiResp001_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n1_astResp001_to_Link_n1_asiResp001_rdptr,
   input  wire          dp_Link_n1_astResp001_to_Link_n1_asiResp001_txctl_pwronrstack,
   output wire          dp_Link_n1_astResp001_to_Link_n1_asiResp001_txctl_pwronrst,
   input  wire [1281:0] dp_Link_n1_astResp001_to_Link_n1_asiResp001_data,
   input  wire [2:0]    dp_Link_n1_astResp001_to_Link_n1_asiResp001_wrcnt,
   input  wire [2:0]    dp_Link_n1_asi_to_Link_n1_ast_r_rdcnt,
   input  wire          dp_Link_n1_asi_to_Link_n1_ast_r_rxctl_pwronrstack,
   output wire          dp_Link_n1_asi_to_Link_n1_ast_r_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n1_asi_to_Link_n1_ast_r_rdptr,
   output wire          dp_Link_n1_asi_to_Link_n1_ast_r_txctl_pwronrstack,
   input  wire          dp_Link_n1_asi_to_Link_n1_ast_r_txctl_pwronrst,
   output wire [128:0]  dp_Link_n1_asi_to_Link_n1_ast_r_data,
   output wire [2:0]    dp_Link_n1_asi_to_Link_n1_ast_r_wrcnt,
   output wire [2:0]    dp_Link_n0_astResp001_to_Link_n0_asiResp001_rdcnt,
   output wire          dp_Link_n0_astResp001_to_Link_n0_asiResp001_rxctl_pwronrstack,
   input  wire          dp_Link_n0_astResp001_to_Link_n0_asiResp001_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n0_astResp001_to_Link_n0_asiResp001_rdptr,
   input  wire          dp_Link_n0_astResp001_to_Link_n0_asiResp001_txctl_pwronrstack,
   output wire          dp_Link_n0_astResp001_to_Link_n0_asiResp001_txctl_pwronrst,
   input  wire [1281:0] dp_Link_n0_astResp001_to_Link_n0_asiResp001_data,
   input  wire [2:0]    dp_Link_n0_astResp001_to_Link_n0_asiResp001_wrcnt,
   input  wire [2:0]    dp_Link_n0_asi_to_Link_n0_ast_r_rdcnt,
   input  wire          dp_Link_n0_asi_to_Link_n0_ast_r_rxctl_pwronrstack,
   output wire          dp_Link_n0_asi_to_Link_n0_ast_r_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n0_asi_to_Link_n0_ast_r_rdptr,
   output wire          dp_Link_n0_asi_to_Link_n0_ast_r_txctl_pwronrstack,
   input  wire          dp_Link_n0_asi_to_Link_n0_ast_r_txctl_pwronrst,
   output wire [128:0]  dp_Link_n0_asi_to_Link_n0_ast_r_data,
   output wire [2:0]    dp_Link_n0_asi_to_Link_n0_ast_r_wrcnt,
   output wire [2:0]    dp_Link_n3_astResp_to_Link_n3_asiResp_rdcnt,
   output wire          dp_Link_n3_astResp_to_Link_n3_asiResp_rxctl_pwronrstack,
   input  wire          dp_Link_n3_astResp_to_Link_n3_asiResp_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n3_astResp_to_Link_n3_asiResp_rdptr,
   input  wire          dp_Link_n3_astResp_to_Link_n3_asiResp_txctl_pwronrstack,
   output wire          dp_Link_n3_astResp_to_Link_n3_asiResp_txctl_pwronrst,
   input  wire [125:0]  dp_Link_n3_astResp_to_Link_n3_asiResp_data,
   input  wire [2:0]    dp_Link_n3_astResp_to_Link_n3_asiResp_wrcnt,
   input  wire [2:0]    dp_Link_n3_asi_to_Link_n3_ast_w_rdcnt,
   input  wire          dp_Link_n3_asi_to_Link_n3_ast_w_rxctl_pwronrstack,
   output wire          dp_Link_n3_asi_to_Link_n3_ast_w_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n3_asi_to_Link_n3_ast_w_rdptr,
   output wire          dp_Link_n3_asi_to_Link_n3_ast_w_txctl_pwronrstack,
   input  wire          dp_Link_n3_asi_to_Link_n3_ast_w_txctl_pwronrst,
   output wire [1278:0] dp_Link_n3_asi_to_Link_n3_ast_w_data,
   output wire [2:0]    dp_Link_n3_asi_to_Link_n3_ast_w_wrcnt,
   output wire [2:0]    dp_Link_n2_astResp_to_Link_n2_asiResp_rdcnt,
   output wire          dp_Link_n2_astResp_to_Link_n2_asiResp_rxctl_pwronrstack,
   input  wire          dp_Link_n2_astResp_to_Link_n2_asiResp_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n2_astResp_to_Link_n2_asiResp_rdptr,
   input  wire          dp_Link_n2_astResp_to_Link_n2_asiResp_txctl_pwronrstack,
   output wire          dp_Link_n2_astResp_to_Link_n2_asiResp_txctl_pwronrst,
   input  wire [125:0]  dp_Link_n2_astResp_to_Link_n2_asiResp_data,
   input  wire [2:0]    dp_Link_n2_astResp_to_Link_n2_asiResp_wrcnt,
   input  wire [2:0]    dp_Link_n2_asi_to_Link_n2_ast_w_rdcnt,
   input  wire          dp_Link_n2_asi_to_Link_n2_ast_w_rxctl_pwronrstack,
   output wire          dp_Link_n2_asi_to_Link_n2_ast_w_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n2_asi_to_Link_n2_ast_w_rdptr,
   output wire          dp_Link_n2_asi_to_Link_n2_ast_w_txctl_pwronrstack,
   input  wire          dp_Link_n2_asi_to_Link_n2_ast_w_txctl_pwronrst,
   output wire [1278:0] dp_Link_n2_asi_to_Link_n2_ast_w_data,
   output wire [2:0]    dp_Link_n2_asi_to_Link_n2_ast_w_wrcnt,
   output wire [2:0]    dp_Link_n1_astResp_to_Link_n1_asiResp_rdcnt,
   output wire          dp_Link_n1_astResp_to_Link_n1_asiResp_rxctl_pwronrstack,
   input  wire          dp_Link_n1_astResp_to_Link_n1_asiResp_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n1_astResp_to_Link_n1_asiResp_rdptr,
   input  wire          dp_Link_n1_astResp_to_Link_n1_asiResp_txctl_pwronrstack,
   output wire          dp_Link_n1_astResp_to_Link_n1_asiResp_txctl_pwronrst,
   input  wire [125:0]  dp_Link_n1_astResp_to_Link_n1_asiResp_data,
   input  wire [2:0]    dp_Link_n1_astResp_to_Link_n1_asiResp_wrcnt,
   input  wire [2:0]    dp_Link_n1_asi_to_Link_n1_ast_w_rdcnt,
   input  wire          dp_Link_n1_asi_to_Link_n1_ast_w_rxctl_pwronrstack,
   output wire          dp_Link_n1_asi_to_Link_n1_ast_w_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n1_asi_to_Link_n1_ast_w_rdptr,
   output wire          dp_Link_n1_asi_to_Link_n1_ast_w_txctl_pwronrstack,
   input  wire          dp_Link_n1_asi_to_Link_n1_ast_w_txctl_pwronrst,
   output wire [1278:0] dp_Link_n1_asi_to_Link_n1_ast_w_data,
   output wire [2:0]    dp_Link_n1_asi_to_Link_n1_ast_w_wrcnt,
   output wire [2:0]    dp_Link_n0_astResp_to_Link_n0_asiResp_rdcnt,
   output wire          dp_Link_n0_astResp_to_Link_n0_asiResp_rxctl_pwronrstack,
   input  wire          dp_Link_n0_astResp_to_Link_n0_asiResp_rxctl_pwronrst,
   output wire [2:0]    dp_Link_n0_astResp_to_Link_n0_asiResp_rdptr,
   input  wire          dp_Link_n0_astResp_to_Link_n0_asiResp_txctl_pwronrstack,
   output wire          dp_Link_n0_astResp_to_Link_n0_asiResp_txctl_pwronrst,
   input  wire [125:0]  dp_Link_n0_astResp_to_Link_n0_asiResp_data,
   input  wire [2:0]    dp_Link_n0_astResp_to_Link_n0_asiResp_wrcnt,
   input  wire [2:0]    dp_Link_n0_asi_to_Link_n0_ast_w_rdcnt,
   input  wire          dp_Link_n0_asi_to_Link_n0_ast_w_rxctl_pwronrstack,
   output wire          dp_Link_n0_asi_to_Link_n0_ast_w_rxctl_pwronrst,
   input  wire [2:0]    dp_Link_n0_asi_to_Link_n0_ast_w_rdptr,
   output wire          dp_Link_n0_asi_to_Link_n0_ast_w_txctl_pwronrstack,
   input  wire          dp_Link_n0_asi_to_Link_n0_ast_w_txctl_pwronrst,
   output wire [1278:0] dp_Link_n0_asi_to_Link_n0_ast_w_data,
   output wire [2:0]    dp_Link_n0_asi_to_Link_n0_ast_w_wrcnt 
);



wire               dnc_cbus_s0_arready;
wire               dnc_cbus_s0_rvalid;
wire               dnc_cbus_s0_rlast;
wire  [ 1    : 0 ] dnc_cbus_s0_rid;
wire  [ 31   : 0 ] dnc_cbus_s0_rdata;
wire  [ 1    : 0 ] dnc_cbus_s0_rresp;
wire               dnc_cbus_s0_awready;
wire               dnc_cbus_s0_wready;
wire  [ 1    : 0 ] dnc_cbus_s0_bid;
wire  [ 1    : 0 ] dnc_cbus_s0_bresp;
wire               dnc_cbus_s0_bvalid;
wire               dnc_dbus_s0_arready;
wire               dnc_dbus_s0_rvalid;
wire               dnc_dbus_s0_rlast;
wire  [ 6    : 0 ] dnc_dbus_s0_rid;
wire  [ 1023 : 0 ] dnc_dbus_s0_rdata;
wire  [ 3    : 0 ] dnc_dbus_s0_ruser;
wire  [ 1    : 0 ] dnc_dbus_s0_rresp;
wire               dnc_dbus_m0_arvalid;
wire  [ 19   : 0 ] dnc_dbus_m0_araddr;
wire  [ 6    : 0 ] dnc_dbus_m0_arid;
wire  [ 2    : 0 ] dnc_dbus_m0_arlen;
wire  [ 1    : 0 ] dnc_dbus_m0_arburst;
wire  [ 3    : 0 ] dnc_dbus_m0_arcache;
wire               dnc_dbus_m0_arlock;
wire  [ 2    : 0 ] dnc_dbus_m0_arprot;
wire  [ 2    : 0 ] dnc_dbus_m0_arsize;
wire  [ 3    : 0 ] dnc_dbus_m0_aruser;
wire               dnc_dbus_m0_rready;
wire  [ 19   : 0 ] dnc_dbus_m0_awaddr;
wire  [ 1    : 0 ] dnc_dbus_m0_awburst;
wire  [ 3    : 0 ] dnc_dbus_m0_awcache;
wire  [ 3    : 0 ] dnc_dbus_m0_awid;
wire  [ 2    : 0 ] dnc_dbus_m0_awlen;
wire               dnc_dbus_m0_awlock;
wire  [ 2    : 0 ] dnc_dbus_m0_awprot;
wire  [ 2    : 0 ] dnc_dbus_m0_awsize;
wire  [ 3    : 0 ] dnc_dbus_m0_awuser;
wire               dnc_dbus_m0_awvalid;
wire               dnc_dbus_m0_bready;
wire  [ 1023 : 0 ] dnc_dbus_m0_wdata;
wire               dnc_dbus_m0_wlast;
wire  [ 127  : 0 ] dnc_dbus_m0_wstrb;
wire               dnc_dbus_m0_wvalid;
wire               dnc_cbus_s0_arready_0;
wire               dnc_cbus_s0_rvalid_0;
wire               dnc_cbus_s0_rlast_0;
wire  [ 1    : 0 ] dnc_cbus_s0_rid_0;
wire  [ 31   : 0 ] dnc_cbus_s0_rdata_0;
wire  [ 1    : 0 ] dnc_cbus_s0_rresp_0;
wire               dnc_cbus_s0_awready_0;
wire               dnc_cbus_s0_wready_0;
wire  [ 1    : 0 ] dnc_cbus_s0_bid_0;
wire  [ 1    : 0 ] dnc_cbus_s0_bresp_0;
wire               dnc_cbus_s0_bvalid_0;
wire               dnc_dbus_s0_arready_0;
wire               dnc_dbus_s0_rvalid_0;
wire               dnc_dbus_s0_rlast_0;
wire  [ 6    : 0 ] dnc_dbus_s0_rid_0;
wire  [ 1023 : 0 ] dnc_dbus_s0_rdata_0;
wire  [ 3    : 0 ] dnc_dbus_s0_ruser_0;
wire  [ 1    : 0 ] dnc_dbus_s0_rresp_0;
wire               dnc_dbus_m0_arvalid_0;
wire  [ 19   : 0 ] dnc_dbus_m0_araddr_0;
wire  [ 6    : 0 ] dnc_dbus_m0_arid_0;
wire  [ 2    : 0 ] dnc_dbus_m0_arlen_0;
wire  [ 1    : 0 ] dnc_dbus_m0_arburst_0;
wire  [ 3    : 0 ] dnc_dbus_m0_arcache_0;
wire               dnc_dbus_m0_arlock_0;
wire  [ 2    : 0 ] dnc_dbus_m0_arprot_0;
wire  [ 2    : 0 ] dnc_dbus_m0_arsize_0;
wire  [ 3    : 0 ] dnc_dbus_m0_aruser_0;
wire               dnc_dbus_m0_rready_0;
wire  [ 19   : 0 ] dnc_dbus_m0_awaddr_0;
wire  [ 1    : 0 ] dnc_dbus_m0_awburst_0;
wire  [ 3    : 0 ] dnc_dbus_m0_awcache_0;
wire  [ 3    : 0 ] dnc_dbus_m0_awid_0;
wire  [ 2    : 0 ] dnc_dbus_m0_awlen_0;
wire               dnc_dbus_m0_awlock_0;
wire  [ 2    : 0 ] dnc_dbus_m0_awprot_0;
wire  [ 2    : 0 ] dnc_dbus_m0_awsize_0;
wire  [ 3    : 0 ] dnc_dbus_m0_awuser_0;
wire               dnc_dbus_m0_awvalid_0;
wire               dnc_dbus_m0_bready_0;
wire  [ 1023 : 0 ] dnc_dbus_m0_wdata_0;
wire               dnc_dbus_m0_wlast_0;
wire  [ 127  : 0 ] dnc_dbus_m0_wstrb_0;
wire               dnc_dbus_m0_wvalid_0;
wire               dnc_cbus_s0_arready_1;
wire               dnc_cbus_s0_rvalid_1;
wire               dnc_cbus_s0_rlast_1;
wire  [ 1    : 0 ] dnc_cbus_s0_rid_1;
wire  [ 31   : 0 ] dnc_cbus_s0_rdata_1;
wire  [ 1    : 0 ] dnc_cbus_s0_rresp_1;
wire               dnc_cbus_s0_awready_1;
wire               dnc_cbus_s0_wready_1;
wire  [ 1    : 0 ] dnc_cbus_s0_bid_1;
wire  [ 1    : 0 ] dnc_cbus_s0_bresp_1;
wire               dnc_cbus_s0_bvalid_1;
wire               dnc_dbus_s0_arready_1;
wire               dnc_dbus_s0_rvalid_1;
wire               dnc_dbus_s0_rlast_1;
wire  [ 6    : 0 ] dnc_dbus_s0_rid_1;
wire  [ 1023 : 0 ] dnc_dbus_s0_rdata_1;
wire  [ 3    : 0 ] dnc_dbus_s0_ruser_1;
wire  [ 1    : 0 ] dnc_dbus_s0_rresp_1;
wire               dnc_dbus_m0_arvalid_1;
wire  [ 19   : 0 ] dnc_dbus_m0_araddr_1;
wire  [ 6    : 0 ] dnc_dbus_m0_arid_1;
wire  [ 2    : 0 ] dnc_dbus_m0_arlen_1;
wire  [ 1    : 0 ] dnc_dbus_m0_arburst_1;
wire  [ 3    : 0 ] dnc_dbus_m0_arcache_1;
wire               dnc_dbus_m0_arlock_1;
wire  [ 2    : 0 ] dnc_dbus_m0_arprot_1;
wire  [ 2    : 0 ] dnc_dbus_m0_arsize_1;
wire  [ 3    : 0 ] dnc_dbus_m0_aruser_1;
wire               dnc_dbus_m0_rready_1;
wire  [ 19   : 0 ] dnc_dbus_m0_awaddr_1;
wire  [ 1    : 0 ] dnc_dbus_m0_awburst_1;
wire  [ 3    : 0 ] dnc_dbus_m0_awcache_1;
wire  [ 3    : 0 ] dnc_dbus_m0_awid_1;
wire  [ 2    : 0 ] dnc_dbus_m0_awlen_1;
wire               dnc_dbus_m0_awlock_1;
wire  [ 2    : 0 ] dnc_dbus_m0_awprot_1;
wire  [ 2    : 0 ] dnc_dbus_m0_awsize_1;
wire  [ 3    : 0 ] dnc_dbus_m0_awuser_1;
wire               dnc_dbus_m0_awvalid_1;
wire               dnc_dbus_m0_bready_1;
wire  [ 1023 : 0 ] dnc_dbus_m0_wdata_1;
wire               dnc_dbus_m0_wlast_1;
wire  [ 127  : 0 ] dnc_dbus_m0_wstrb_1;
wire               dnc_dbus_m0_wvalid_1;
wire               dnc_cbus_s0_arready_2;
wire               dnc_cbus_s0_rvalid_2;
wire               dnc_cbus_s0_rlast_2;
wire  [ 1    : 0 ] dnc_cbus_s0_rid_2;
wire  [ 31   : 0 ] dnc_cbus_s0_rdata_2;
wire  [ 1    : 0 ] dnc_cbus_s0_rresp_2;
wire               dnc_cbus_s0_awready_2;
wire               dnc_cbus_s0_wready_2;
wire  [ 1    : 0 ] dnc_cbus_s0_bid_2;
wire  [ 1    : 0 ] dnc_cbus_s0_bresp_2;
wire               dnc_cbus_s0_bvalid_2;
wire               dnc_dbus_s0_arready_2;
wire               dnc_dbus_s0_rvalid_2;
wire               dnc_dbus_s0_rlast_2;
wire  [ 6    : 0 ] dnc_dbus_s0_rid_2;
wire  [ 1023 : 0 ] dnc_dbus_s0_rdata_2;
wire  [ 3    : 0 ] dnc_dbus_s0_ruser_2;
wire  [ 1    : 0 ] dnc_dbus_s0_rresp_2;
wire               dnc_dbus_m0_arvalid_2;
wire  [ 19   : 0 ] dnc_dbus_m0_araddr_2;
wire  [ 6    : 0 ] dnc_dbus_m0_arid_2;
wire  [ 2    : 0 ] dnc_dbus_m0_arlen_2;
wire  [ 1    : 0 ] dnc_dbus_m0_arburst_2;
wire  [ 3    : 0 ] dnc_dbus_m0_arcache_2;
wire               dnc_dbus_m0_arlock_2;
wire  [ 2    : 0 ] dnc_dbus_m0_arprot_2;
wire  [ 2    : 0 ] dnc_dbus_m0_arsize_2;
wire  [ 3    : 0 ] dnc_dbus_m0_aruser_2;
wire               dnc_dbus_m0_rready_2;
wire  [ 19   : 0 ] dnc_dbus_m0_awaddr_2;
wire  [ 1    : 0 ] dnc_dbus_m0_awburst_2;
wire  [ 3    : 0 ] dnc_dbus_m0_awcache_2;
wire  [ 3    : 0 ] dnc_dbus_m0_awid_2;
wire  [ 2    : 0 ] dnc_dbus_m0_awlen_2;
wire               dnc_dbus_m0_awlock_2;
wire  [ 2    : 0 ] dnc_dbus_m0_awprot_2;
wire  [ 2    : 0 ] dnc_dbus_m0_awsize_2;
wire  [ 3    : 0 ] dnc_dbus_m0_awuser_2;
wire               dnc_dbus_m0_awvalid_2;
wire               dnc_dbus_m0_bready_2;
wire  [ 1023 : 0 ] dnc_dbus_m0_wdata_2;
wire               dnc_dbus_m0_wlast_2;
wire  [ 127  : 0 ] dnc_dbus_m0_wstrb_2;
wire               dnc_dbus_m0_wvalid_2;
wire               cbus_s0_arready;
wire               cbus_s0_rvalid;
wire               cbus_s0_rlast;
wire  [ 1    : 0 ] cbus_s0_rid;
wire  [ 31   : 0 ] cbus_s0_rdata;
wire  [ 1    : 0 ] cbus_s0_rresp;
wire               cbus_s0_awready;
wire               cbus_s0_wready;
wire  [ 1    : 0 ] cbus_s0_bid;
wire  [ 1    : 0 ] cbus_s0_bresp;
wire               cbus_s0_bvalid;
wire               dbus_s0_arready;
wire               dbus_s0_rvalid;
wire               dbus_s0_rlast;
wire  [ 6    : 0 ] dbus_s0_rid;
wire  [ 1023 : 0 ] dbus_s0_rdata;
wire  [ 3    : 0 ] dbus_s0_ruser;
wire  [ 1    : 0 ] dbus_s0_rresp;
wire               dbus_s1_arready;
wire               dbus_s1_rvalid;
wire               dbus_s1_rlast;
wire  [ 6    : 0 ] dbus_s1_rid;
wire  [ 1023 : 0 ] dbus_s1_rdata;
wire  [ 3    : 0 ] dbus_s1_ruser;
wire  [ 1    : 0 ] dbus_s1_rresp;
wire               dbus_s2_arready;
wire               dbus_s2_rvalid;
wire               dbus_s2_rlast;
wire  [ 6    : 0 ] dbus_s2_rid;
wire  [ 1023 : 0 ] dbus_s2_rdata;
wire  [ 3    : 0 ] dbus_s2_ruser;
wire  [ 1    : 0 ] dbus_s2_rresp;
wire               dbus_s3_arready;
wire               dbus_s3_rvalid;
wire               dbus_s3_rlast;
wire  [ 6    : 0 ] dbus_s3_rid;
wire  [ 1023 : 0 ] dbus_s3_rdata;
wire  [ 3    : 0 ] dbus_s3_ruser;
wire  [ 1    : 0 ] dbus_s3_rresp;
wire               dbus_m0_arvalid;
wire  [ 36   : 0 ] dbus_m0_araddr;
wire  [ 6    : 0 ] dbus_m0_arid;
wire  [ 4    : 0 ] dbus_m0_arlen;
wire  [ 1    : 0 ] dbus_m0_arburst;
wire  [ 3    : 0 ] dbus_m0_arcache;
wire               dbus_m0_arlock;
wire  [ 2    : 0 ] dbus_m0_arprot;
wire  [ 2    : 0 ] dbus_m0_arsize;
wire               dbus_m0_rready;
wire               dbus_m1_arvalid;
wire  [ 36   : 0 ] dbus_m1_araddr;
wire  [ 6    : 0 ] dbus_m1_arid;
wire  [ 4    : 0 ] dbus_m1_arlen;
wire  [ 1    : 0 ] dbus_m1_arburst;
wire  [ 3    : 0 ] dbus_m1_arcache;
wire               dbus_m1_arlock;
wire  [ 2    : 0 ] dbus_m1_arprot;
wire  [ 2    : 0 ] dbus_m1_arsize;
wire               dbus_m1_rready;
wire               dbus_m2_arvalid;
wire  [ 36   : 0 ] dbus_m2_araddr;
wire  [ 6    : 0 ] dbus_m2_arid;
wire  [ 4    : 0 ] dbus_m2_arlen;
wire  [ 1    : 0 ] dbus_m2_arburst;
wire  [ 3    : 0 ] dbus_m2_arcache;
wire               dbus_m2_arlock;
wire  [ 2    : 0 ] dbus_m2_arprot;
wire  [ 2    : 0 ] dbus_m2_arsize;
wire               dbus_m2_rready;
wire               dbus_m3_arvalid;
wire  [ 36   : 0 ] dbus_m3_araddr;
wire  [ 6    : 0 ] dbus_m3_arid;
wire  [ 4    : 0 ] dbus_m3_arlen;
wire  [ 1    : 0 ] dbus_m3_arburst;
wire  [ 3    : 0 ] dbus_m3_arcache;
wire               dbus_m3_arlock;
wire  [ 2    : 0 ] dbus_m3_arprot;
wire  [ 2    : 0 ] dbus_m3_arsize;
wire               dbus_m3_rready;
wire               dnc3_ctrl_R_Ready;
wire               dnc3_ctrl_B_Ready;
wire  [ 2    : 0 ] dnc3_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dnc3_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dnc3_ctrl_Aw_Burst;
wire               dnc3_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dnc3_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dnc3_ctrl_Aw_Len;
wire               dnc3_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dnc3_ctrl_Aw_Id;
wire  [ 2    : 0 ] dnc3_ctrl_Aw_Size;
wire               dnc3_ctrl_W_Last;
wire               dnc3_ctrl_W_Valid;
wire  [ 3    : 0 ] dnc3_ctrl_W_Strb;
wire  [ 31   : 0 ] dnc3_ctrl_W_Data;
wire  [ 2    : 0 ] dnc3_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dnc3_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dnc3_ctrl_Ar_Burst;
wire               dnc3_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dnc3_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dnc3_ctrl_Ar_Len;
wire               dnc3_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dnc3_ctrl_Ar_Id;
wire  [ 2    : 0 ] dnc3_ctrl_Ar_Size;
wire               dnc2_ctrl_R_Ready;
wire               dnc2_ctrl_B_Ready;
wire  [ 2    : 0 ] dnc2_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dnc2_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dnc2_ctrl_Aw_Burst;
wire               dnc2_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dnc2_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dnc2_ctrl_Aw_Len;
wire               dnc2_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dnc2_ctrl_Aw_Id;
wire  [ 2    : 0 ] dnc2_ctrl_Aw_Size;
wire               dnc2_ctrl_W_Last;
wire               dnc2_ctrl_W_Valid;
wire  [ 3    : 0 ] dnc2_ctrl_W_Strb;
wire  [ 31   : 0 ] dnc2_ctrl_W_Data;
wire  [ 2    : 0 ] dnc2_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dnc2_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dnc2_ctrl_Ar_Burst;
wire               dnc2_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dnc2_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dnc2_ctrl_Ar_Len;
wire               dnc2_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dnc2_ctrl_Ar_Id;
wire  [ 2    : 0 ] dnc2_ctrl_Ar_Size;
wire               dnc1_ctrl_R_Ready;
wire               dnc1_ctrl_B_Ready;
wire  [ 2    : 0 ] dnc1_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dnc1_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dnc1_ctrl_Aw_Burst;
wire               dnc1_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dnc1_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dnc1_ctrl_Aw_Len;
wire               dnc1_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dnc1_ctrl_Aw_Id;
wire  [ 2    : 0 ] dnc1_ctrl_Aw_Size;
wire               dnc1_ctrl_W_Last;
wire               dnc1_ctrl_W_Valid;
wire  [ 3    : 0 ] dnc1_ctrl_W_Strb;
wire  [ 31   : 0 ] dnc1_ctrl_W_Data;
wire  [ 2    : 0 ] dnc1_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dnc1_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dnc1_ctrl_Ar_Burst;
wire               dnc1_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dnc1_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dnc1_ctrl_Ar_Len;
wire               dnc1_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dnc1_ctrl_Ar_Id;
wire  [ 2    : 0 ] dnc1_ctrl_Ar_Size;
wire               dnc0_ctrl_R_Ready;
wire               dnc0_ctrl_B_Ready;
wire  [ 2    : 0 ] dnc0_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dnc0_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dnc0_ctrl_Aw_Burst;
wire               dnc0_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dnc0_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dnc0_ctrl_Aw_Len;
wire               dnc0_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dnc0_ctrl_Aw_Id;
wire  [ 2    : 0 ] dnc0_ctrl_Aw_Size;
wire               dnc0_ctrl_W_Last;
wire               dnc0_ctrl_W_Valid;
wire  [ 3    : 0 ] dnc0_ctrl_W_Strb;
wire  [ 31   : 0 ] dnc0_ctrl_W_Data;
wire  [ 2    : 0 ] dnc0_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dnc0_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dnc0_ctrl_Ar_Burst;
wire               dnc0_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dnc0_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dnc0_ctrl_Ar_Len;
wire               dnc0_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dnc0_ctrl_Ar_Id;
wire  [ 2    : 0 ] dnc0_ctrl_Ar_Size;
wire               dcluster0_ctrl_R_Ready;
wire               dcluster0_ctrl_B_Ready;
wire  [ 2    : 0 ] dcluster0_ctrl_Aw_Prot;
wire  [ 23   : 0 ] dcluster0_ctrl_Aw_Addr;
wire  [ 1    : 0 ] dcluster0_ctrl_Aw_Burst;
wire               dcluster0_ctrl_Aw_Lock;
wire  [ 3    : 0 ] dcluster0_ctrl_Aw_Cache;
wire  [ 1    : 0 ] dcluster0_ctrl_Aw_Len;
wire               dcluster0_ctrl_Aw_Valid;
wire  [ 1    : 0 ] dcluster0_ctrl_Aw_Id;
wire  [ 2    : 0 ] dcluster0_ctrl_Aw_Size;
wire               dcluster0_ctrl_W_Last;
wire               dcluster0_ctrl_W_Valid;
wire  [ 3    : 0 ] dcluster0_ctrl_W_Strb;
wire  [ 31   : 0 ] dcluster0_ctrl_W_Data;
wire  [ 2    : 0 ] dcluster0_ctrl_Ar_Prot;
wire  [ 23   : 0 ] dcluster0_ctrl_Ar_Addr;
wire  [ 1    : 0 ] dcluster0_ctrl_Ar_Burst;
wire               dcluster0_ctrl_Ar_Lock;
wire  [ 3    : 0 ] dcluster0_ctrl_Ar_Cache;
wire  [ 1    : 0 ] dcluster0_ctrl_Ar_Len;
wire               dcluster0_ctrl_Ar_Valid;
wire  [ 1    : 0 ] dcluster0_ctrl_Ar_Id;
wire  [ 2    : 0 ] dcluster0_ctrl_Ar_Size;
wire               dnc3_slv_r_R_Ready;
wire  [ 2    : 0 ] dnc3_slv_r_Ar_Prot;
wire  [ 19   : 0 ] dnc3_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dnc3_slv_r_Ar_Burst;
wire               dnc3_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dnc3_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dnc3_slv_r_Ar_Len;
wire               dnc3_slv_r_Ar_Valid;
wire  [ 6    : 0 ] dnc3_slv_r_Ar_Id;
wire  [ 2    : 0 ] dnc3_slv_r_Ar_Size;
wire               dnc3_r_R_Last;
wire  [ 1    : 0 ] dnc3_r_R_Resp;
wire               dnc3_r_R_Valid;
wire  [ 3    : 0 ] dnc3_r_R_User;
wire  [ 1023 : 0 ] dnc3_r_R_Data;
wire  [ 6    : 0 ] dnc3_r_R_Id;
wire               dnc3_r_Ar_Ready;
wire               dnc2_slv_r_R_Ready;
wire  [ 2    : 0 ] dnc2_slv_r_Ar_Prot;
wire  [ 19   : 0 ] dnc2_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dnc2_slv_r_Ar_Burst;
wire               dnc2_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dnc2_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dnc2_slv_r_Ar_Len;
wire               dnc2_slv_r_Ar_Valid;
wire  [ 6    : 0 ] dnc2_slv_r_Ar_Id;
wire  [ 2    : 0 ] dnc2_slv_r_Ar_Size;
wire               dnc2_r_R_Last;
wire  [ 1    : 0 ] dnc2_r_R_Resp;
wire               dnc2_r_R_Valid;
wire  [ 3    : 0 ] dnc2_r_R_User;
wire  [ 1023 : 0 ] dnc2_r_R_Data;
wire  [ 6    : 0 ] dnc2_r_R_Id;
wire               dnc2_r_Ar_Ready;
wire               dnc1_slv_r_R_Ready;
wire  [ 2    : 0 ] dnc1_slv_r_Ar_Prot;
wire  [ 19   : 0 ] dnc1_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dnc1_slv_r_Ar_Burst;
wire               dnc1_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dnc1_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dnc1_slv_r_Ar_Len;
wire               dnc1_slv_r_Ar_Valid;
wire  [ 6    : 0 ] dnc1_slv_r_Ar_Id;
wire  [ 2    : 0 ] dnc1_slv_r_Ar_Size;
wire               dnc1_r_R_Last;
wire  [ 1    : 0 ] dnc1_r_R_Resp;
wire               dnc1_r_R_Valid;
wire  [ 3    : 0 ] dnc1_r_R_User;
wire  [ 1023 : 0 ] dnc1_r_R_Data;
wire  [ 6    : 0 ] dnc1_r_R_Id;
wire               dnc1_r_Ar_Ready;
wire               dnc0_slv_r_R_Ready;
wire  [ 2    : 0 ] dnc0_slv_r_Ar_Prot;
wire  [ 19   : 0 ] dnc0_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dnc0_slv_r_Ar_Burst;
wire               dnc0_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dnc0_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dnc0_slv_r_Ar_Len;
wire               dnc0_slv_r_Ar_Valid;
wire  [ 6    : 0 ] dnc0_slv_r_Ar_Id;
wire  [ 2    : 0 ] dnc0_slv_r_Ar_Size;
wire               dnc0_r_R_Last;
wire  [ 1    : 0 ] dnc0_r_R_Resp;
wire               dnc0_r_R_Valid;
wire  [ 3    : 0 ] dnc0_r_R_User;
wire  [ 1023 : 0 ] dnc0_r_R_Data;
wire  [ 6    : 0 ] dnc0_r_R_Id;
wire               dnc0_r_Ar_Ready;
wire               dcluster0_cc3_slv_r_R_Ready;
wire  [ 2    : 0 ] dcluster0_cc3_slv_r_Ar_Prot;
wire  [ 36   : 0 ] dcluster0_cc3_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dcluster0_cc3_slv_r_Ar_Burst;
wire               dcluster0_cc3_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dcluster0_cc3_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dcluster0_cc3_slv_r_Ar_Len;
wire               dcluster0_cc3_slv_r_Ar_Valid;
wire  [ 3    : 0 ] dcluster0_cc3_slv_r_Ar_User;
wire  [ 6    : 0 ] dcluster0_cc3_slv_r_Ar_Id;
wire  [ 2    : 0 ] dcluster0_cc3_slv_r_Ar_Size;
wire               dcluster0_cc3_r_R_Last;
wire  [ 1    : 0 ] dcluster0_cc3_r_R_Resp;
wire               dcluster0_cc3_r_R_Valid;
wire  [ 3    : 0 ] dcluster0_cc3_r_R_User;
wire  [ 1023 : 0 ] dcluster0_cc3_r_R_Data;
wire  [ 6    : 0 ] dcluster0_cc3_r_R_Id;
wire               dcluster0_cc3_r_Ar_Ready;
wire               dcluster0_cc2_slv_r_R_Ready;
wire  [ 2    : 0 ] dcluster0_cc2_slv_r_Ar_Prot;
wire  [ 36   : 0 ] dcluster0_cc2_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dcluster0_cc2_slv_r_Ar_Burst;
wire               dcluster0_cc2_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dcluster0_cc2_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dcluster0_cc2_slv_r_Ar_Len;
wire               dcluster0_cc2_slv_r_Ar_Valid;
wire  [ 3    : 0 ] dcluster0_cc2_slv_r_Ar_User;
wire  [ 6    : 0 ] dcluster0_cc2_slv_r_Ar_Id;
wire  [ 2    : 0 ] dcluster0_cc2_slv_r_Ar_Size;
wire               dcluster0_cc2_r_R_Last;
wire  [ 1    : 0 ] dcluster0_cc2_r_R_Resp;
wire               dcluster0_cc2_r_R_Valid;
wire  [ 3    : 0 ] dcluster0_cc2_r_R_User;
wire  [ 1023 : 0 ] dcluster0_cc2_r_R_Data;
wire  [ 6    : 0 ] dcluster0_cc2_r_R_Id;
wire               dcluster0_cc2_r_Ar_Ready;
wire               dcluster0_cc1_slv_r_R_Ready;
wire  [ 2    : 0 ] dcluster0_cc1_slv_r_Ar_Prot;
wire  [ 36   : 0 ] dcluster0_cc1_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dcluster0_cc1_slv_r_Ar_Burst;
wire               dcluster0_cc1_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dcluster0_cc1_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dcluster0_cc1_slv_r_Ar_Len;
wire               dcluster0_cc1_slv_r_Ar_Valid;
wire  [ 3    : 0 ] dcluster0_cc1_slv_r_Ar_User;
wire  [ 6    : 0 ] dcluster0_cc1_slv_r_Ar_Id;
wire  [ 2    : 0 ] dcluster0_cc1_slv_r_Ar_Size;
wire               dcluster0_cc1_r_R_Last;
wire  [ 1    : 0 ] dcluster0_cc1_r_R_Resp;
wire               dcluster0_cc1_r_R_Valid;
wire  [ 3    : 0 ] dcluster0_cc1_r_R_User;
wire  [ 1023 : 0 ] dcluster0_cc1_r_R_Data;
wire  [ 6    : 0 ] dcluster0_cc1_r_R_Id;
wire               dcluster0_cc1_r_Ar_Ready;
wire               dcluster0_cc0_slv_r_R_Ready;
wire  [ 2    : 0 ] dcluster0_cc0_slv_r_Ar_Prot;
wire  [ 36   : 0 ] dcluster0_cc0_slv_r_Ar_Addr;
wire  [ 1    : 0 ] dcluster0_cc0_slv_r_Ar_Burst;
wire               dcluster0_cc0_slv_r_Ar_Lock;
wire  [ 3    : 0 ] dcluster0_cc0_slv_r_Ar_Cache;
wire  [ 4    : 0 ] dcluster0_cc0_slv_r_Ar_Len;
wire               dcluster0_cc0_slv_r_Ar_Valid;
wire  [ 3    : 0 ] dcluster0_cc0_slv_r_Ar_User;
wire  [ 6    : 0 ] dcluster0_cc0_slv_r_Ar_Id;
wire  [ 2    : 0 ] dcluster0_cc0_slv_r_Ar_Size;
wire               dcluster0_cc0_r_R_Last;
wire  [ 1    : 0 ] dcluster0_cc0_r_R_Resp;
wire               dcluster0_cc0_r_R_Valid;
wire  [ 3    : 0 ] dcluster0_cc0_r_R_User;
wire  [ 1023 : 0 ] dcluster0_cc0_r_R_Data;
wire  [ 6    : 0 ] dcluster0_cc0_r_R_Id;
wire               dcluster0_cc0_r_Ar_Ready;
wire  [ 1    : 0 ] dnc3_w_B_Resp;
wire               dnc3_w_B_Valid;
wire  [ 3    : 0 ] dnc3_w_B_User;
wire  [ 6    : 0 ] dnc3_w_B_Id;
wire               dnc3_w_Aw_Ready;
wire               dnc3_w_W_Ready;
wire  [ 1    : 0 ] dnc2_w_B_Resp;
wire               dnc2_w_B_Valid;
wire  [ 3    : 0 ] dnc2_w_B_User;
wire  [ 6    : 0 ] dnc2_w_B_Id;
wire               dnc2_w_Aw_Ready;
wire               dnc2_w_W_Ready;
wire  [ 1    : 0 ] dnc1_w_B_Resp;
wire               dnc1_w_B_Valid;
wire  [ 3    : 0 ] dnc1_w_B_User;
wire  [ 6    : 0 ] dnc1_w_B_Id;
wire               dnc1_w_Aw_Ready;
wire               dnc1_w_W_Ready;
wire  [ 1    : 0 ] dnc0_w_B_Resp;
wire               dnc0_w_B_Valid;
wire  [ 3    : 0 ] dnc0_w_B_User;
wire  [ 6    : 0 ] dnc0_w_B_Id;
wire               dnc0_w_Aw_Ready;
wire               dnc0_w_W_Ready;

dnc #(
      .C_AXI_DATA_WIDTH_P   (32),
      .C_AXI_ADDR_WIDTH_P   (24),
      .C_AXI_STRB_WIDTH_P   (4),
      .C_AXI_ID_WIDTH_P     (2),
      .C_AXI_USER_WIDTH_P   (0),
      .C_AXI_LEN_WIDTH_P    (2),
      .DNC_AXI_DATA_WIDTH_P (1024),
      .DNC_AXI_ADDR_WIDTH_P (20),
      .DNC_AXI_STRB_WIDTH_P (128),
      .DNC_AXI_ID_WIDTH_P   (7),
      .DNC_AXI_USER_WIDTH_P (4),
      .DNC_AXI_LEN_WIDTH_P  (3) 
      ) 
u_dnc0 (
      .clk(                    aclk                   ),
      .arstn(                  arstn                  ),
      .dnc_cbus_s0_arready(    dnc_cbus_s0_arready    ),
      .dnc_cbus_s0_arvalid(    dnc0_ctrl_Ar_Valid     ),
      .dnc_cbus_s0_araddr(     dnc0_ctrl_Ar_Addr      ),
      .dnc_cbus_s0_arid(       dnc0_ctrl_Ar_Id        ),
      .dnc_cbus_s0_arlen(      dnc0_ctrl_Ar_Len       ),
      .dnc_cbus_s0_arburst(    dnc0_ctrl_Ar_Burst     ),
      .dnc_cbus_s0_arcache(    dnc0_ctrl_Ar_Cache     ),
      .dnc_cbus_s0_arlock(     dnc0_ctrl_Ar_Lock      ),
      .dnc_cbus_s0_arprot(     dnc0_ctrl_Ar_Prot      ),
      .dnc_cbus_s0_arsize(     dnc0_ctrl_Ar_Size      ),
      .dnc_cbus_s0_rready(     dnc0_ctrl_R_Ready      ),
      .dnc_cbus_s0_rvalid(     dnc_cbus_s0_rvalid     ),
      .dnc_cbus_s0_rlast(      dnc_cbus_s0_rlast      ),
      .dnc_cbus_s0_rid(        dnc_cbus_s0_rid        ),
      .dnc_cbus_s0_rdata(      dnc_cbus_s0_rdata      ),
      .dnc_cbus_s0_rresp(      dnc_cbus_s0_rresp      ),
      .dnc_cbus_s0_awready(    dnc_cbus_s0_awready    ),
      .dnc_cbus_s0_awvalid(    dnc0_ctrl_Aw_Valid     ),
      .dnc_cbus_s0_awaddr(     dnc0_ctrl_Aw_Addr      ),
      .dnc_cbus_s0_awid(       dnc0_ctrl_Aw_Id        ),
      .dnc_cbus_s0_awlen(      dnc0_ctrl_Aw_Len       ),
      .dnc_cbus_s0_awburst(    dnc0_ctrl_Aw_Burst     ),
      .dnc_cbus_s0_awcache(    dnc0_ctrl_Aw_Cache     ),
      .dnc_cbus_s0_awlock(     dnc0_ctrl_Aw_Lock      ),
      .dnc_cbus_s0_awprot(     dnc0_ctrl_Aw_Prot      ),
      .dnc_cbus_s0_awsize(     dnc0_ctrl_Aw_Size      ),
      .dnc_cbus_s0_wready(     dnc_cbus_s0_wready     ),
      .dnc_cbus_s0_wvalid(     dnc0_ctrl_W_Valid      ),
      .dnc_cbus_s0_wdata(      dnc0_ctrl_W_Data       ),
      .dnc_cbus_s0_wlast(      dnc0_ctrl_W_Last       ),
      .dnc_cbus_s0_wstrb(      dnc0_ctrl_W_Strb       ),
      .dnc_cbus_s0_bready(     dnc0_ctrl_B_Ready      ),
      .dnc_cbus_s0_bid(        dnc_cbus_s0_bid        ),
      .dnc_cbus_s0_bresp(      dnc_cbus_s0_bresp      ),
      .dnc_cbus_s0_bvalid(     dnc_cbus_s0_bvalid     ),
      .dnc_dbus_s0_arready(    dnc_dbus_s0_arready    ),
      .dnc_dbus_s0_arvalid(    dnc0_slv_r_Ar_Valid    ),
      .dnc_dbus_s0_araddr(     dnc0_slv_r_Ar_Addr     ),
      .dnc_dbus_s0_arid(       dnc0_slv_r_Ar_Id       ),
      .dnc_dbus_s0_arlen(      dnc0_slv_r_Ar_Len[2:0] ),
      .dnc_dbus_s0_arburst(    dnc0_slv_r_Ar_Burst    ),
      .dnc_dbus_s0_arcache(    dnc0_slv_r_Ar_Cache    ),
      .dnc_dbus_s0_arlock(     dnc0_slv_r_Ar_Lock     ),
      .dnc_dbus_s0_arprot(     dnc0_slv_r_Ar_Prot     ),
      .dnc_dbus_s0_arsize(     dnc0_slv_r_Ar_Size     ),
      .dnc_dbus_s0_aruser(     4'b0000                ),
      .dnc_dbus_s0_rready(     dnc0_slv_r_R_Ready     ),
      .dnc_dbus_s0_rvalid(     dnc_dbus_s0_rvalid     ),
      .dnc_dbus_s0_rlast(      dnc_dbus_s0_rlast      ),
      .dnc_dbus_s0_rid(        dnc_dbus_s0_rid        ),
      .dnc_dbus_s0_rdata(      dnc_dbus_s0_rdata      ),
      .dnc_dbus_s0_ruser(      dnc_dbus_s0_ruser      ),
      .dnc_dbus_s0_rresp(      dnc_dbus_s0_rresp      ),
      .dnc_dbus_m0_arready(    dnc0_r_Ar_Ready        ),
      .dnc_dbus_m0_arvalid(    dnc_dbus_m0_arvalid    ),
      .dnc_dbus_m0_araddr(     dnc_dbus_m0_araddr     ),
      .dnc_dbus_m0_arid(       dnc_dbus_m0_arid       ),
      .dnc_dbus_m0_arlen(      dnc_dbus_m0_arlen      ),
      .dnc_dbus_m0_arburst(    dnc_dbus_m0_arburst    ),
      .dnc_dbus_m0_arcache(    dnc_dbus_m0_arcache    ),
      .dnc_dbus_m0_arlock(     dnc_dbus_m0_arlock     ),
      .dnc_dbus_m0_arprot(     dnc_dbus_m0_arprot     ),
      .dnc_dbus_m0_arsize(     dnc_dbus_m0_arsize     ),
      .dnc_dbus_m0_aruser(     dnc_dbus_m0_aruser     ),
      .dnc_dbus_m0_rready(     dnc_dbus_m0_rready     ),
      .dnc_dbus_m0_rvalid(     dnc0_r_R_Valid         ),
      .dnc_dbus_m0_rlast(      dnc0_r_R_Last          ),
      .dnc_dbus_m0_rid(        dnc0_r_R_Id            ),
      .dnc_dbus_m0_rdata(      dnc0_r_R_Data          ),
      .dnc_dbus_m0_ruser(      dnc0_r_R_User          ),
      .dnc_dbus_m0_rresp(      dnc0_r_R_Resp          ),
      .dnc_dbus_m0_awready(    dnc0_w_Aw_Ready        ),
      .dnc_dbus_m0_awaddr(     dnc_dbus_m0_awaddr     ),
      .dnc_dbus_m0_awburst(    dnc_dbus_m0_awburst    ),
      .dnc_dbus_m0_awcache(    dnc_dbus_m0_awcache    ),
      .dnc_dbus_m0_awid(       dnc_dbus_m0_awid       ),
      .dnc_dbus_m0_awlen(      dnc_dbus_m0_awlen      ),
      .dnc_dbus_m0_awlock(     dnc_dbus_m0_awlock     ),
      .dnc_dbus_m0_awprot(     dnc_dbus_m0_awprot     ),
      .dnc_dbus_m0_awsize(     dnc_dbus_m0_awsize     ),
      .dnc_dbus_m0_awuser(     dnc_dbus_m0_awuser     ),
      .dnc_dbus_m0_awvalid(    dnc_dbus_m0_awvalid    ),
      .dnc_dbus_m0_bready(     dnc_dbus_m0_bready     ),
      .dnc_dbus_m0_bid(        dnc0_w_B_Id            ),
      .dnc_dbus_m0_bresp(      dnc0_w_B_Resp          ),
      .dnc_dbus_m0_buser(      dnc0_w_B_User          ),
      .dnc_dbus_m0_bvalid(     dnc0_w_B_Valid         ),
      .dnc_dbus_m0_wready(     dnc0_w_W_Ready         ),
      .dnc_dbus_m0_wdata(      dnc_dbus_m0_wdata      ),
      .dnc_dbus_m0_wlast(      dnc_dbus_m0_wlast      ),
      .dnc_dbus_m0_wstrb(      dnc_dbus_m0_wstrb      ),
      .dnc_dbus_m0_wvalid(     dnc_dbus_m0_wvalid     ) 
      );


dnc #(
      .C_AXI_DATA_WIDTH_P   (32),
      .C_AXI_ADDR_WIDTH_P   (24),
      .C_AXI_STRB_WIDTH_P   (4),
      .C_AXI_ID_WIDTH_P     (2),
      .C_AXI_USER_WIDTH_P   (0),
      .C_AXI_LEN_WIDTH_P    (2),
      .DNC_AXI_DATA_WIDTH_P (1024),
      .DNC_AXI_ADDR_WIDTH_P (20),
      .DNC_AXI_STRB_WIDTH_P (128),
      .DNC_AXI_ID_WIDTH_P   (7),
      .DNC_AXI_USER_WIDTH_P (4),
      .DNC_AXI_LEN_WIDTH_P  (3) 
      ) 
u_dnc1 (
      .clk(                    aclk                   ),
      .arstn(                  arstn                  ),
      .dnc_cbus_s0_arready(    dnc_cbus_s0_arready_0  ),
      .dnc_cbus_s0_arvalid(    dnc1_ctrl_Ar_Valid     ),
      .dnc_cbus_s0_araddr(     dnc1_ctrl_Ar_Addr      ),
      .dnc_cbus_s0_arid(       dnc1_ctrl_Ar_Id        ),
      .dnc_cbus_s0_arlen(      dnc1_ctrl_Ar_Len       ),
      .dnc_cbus_s0_arburst(    dnc1_ctrl_Ar_Burst     ),
      .dnc_cbus_s0_arcache(    dnc1_ctrl_Ar_Cache     ),
      .dnc_cbus_s0_arlock(     dnc1_ctrl_Ar_Lock      ),
      .dnc_cbus_s0_arprot(     dnc1_ctrl_Ar_Prot      ),
      .dnc_cbus_s0_arsize(     dnc1_ctrl_Ar_Size      ),
      .dnc_cbus_s0_rready(     dnc1_ctrl_R_Ready      ),
      .dnc_cbus_s0_rvalid(     dnc_cbus_s0_rvalid_0   ),
      .dnc_cbus_s0_rlast(      dnc_cbus_s0_rlast_0    ),
      .dnc_cbus_s0_rid(        dnc_cbus_s0_rid_0      ),
      .dnc_cbus_s0_rdata(      dnc_cbus_s0_rdata_0    ),
      .dnc_cbus_s0_rresp(      dnc_cbus_s0_rresp_0    ),
      .dnc_cbus_s0_awready(    dnc_cbus_s0_awready_0  ),
      .dnc_cbus_s0_awvalid(    dnc1_ctrl_Aw_Valid     ),
      .dnc_cbus_s0_awaddr(     dnc1_ctrl_Aw_Addr      ),
      .dnc_cbus_s0_awid(       dnc1_ctrl_Aw_Id        ),
      .dnc_cbus_s0_awlen(      dnc1_ctrl_Aw_Len       ),
      .dnc_cbus_s0_awburst(    dnc1_ctrl_Aw_Burst     ),
      .dnc_cbus_s0_awcache(    dnc1_ctrl_Aw_Cache     ),
      .dnc_cbus_s0_awlock(     dnc1_ctrl_Aw_Lock      ),
      .dnc_cbus_s0_awprot(     dnc1_ctrl_Aw_Prot      ),
      .dnc_cbus_s0_awsize(     dnc1_ctrl_Aw_Size      ),
      .dnc_cbus_s0_wready(     dnc_cbus_s0_wready_0   ),
      .dnc_cbus_s0_wvalid(     dnc1_ctrl_W_Valid      ),
      .dnc_cbus_s0_wdata(      dnc1_ctrl_W_Data       ),
      .dnc_cbus_s0_wlast(      dnc1_ctrl_W_Last       ),
      .dnc_cbus_s0_wstrb(      dnc1_ctrl_W_Strb       ),
      .dnc_cbus_s0_bready(     dnc1_ctrl_B_Ready      ),
      .dnc_cbus_s0_bid(        dnc_cbus_s0_bid_0      ),
      .dnc_cbus_s0_bresp(      dnc_cbus_s0_bresp_0    ),
      .dnc_cbus_s0_bvalid(     dnc_cbus_s0_bvalid_0   ),
      .dnc_dbus_s0_arready(    dnc_dbus_s0_arready_0  ),
      .dnc_dbus_s0_arvalid(    dnc1_slv_r_Ar_Valid    ),
      .dnc_dbus_s0_araddr(     dnc1_slv_r_Ar_Addr     ),
      .dnc_dbus_s0_arid(       dnc1_slv_r_Ar_Id       ),
      .dnc_dbus_s0_arlen(      dnc1_slv_r_Ar_Len[2:0] ),
      .dnc_dbus_s0_arburst(    dnc1_slv_r_Ar_Burst    ),
      .dnc_dbus_s0_arcache(    dnc1_slv_r_Ar_Cache    ),
      .dnc_dbus_s0_arlock(     dnc1_slv_r_Ar_Lock     ),
      .dnc_dbus_s0_arprot(     dnc1_slv_r_Ar_Prot     ),
      .dnc_dbus_s0_arsize(     dnc1_slv_r_Ar_Size     ),
      .dnc_dbus_s0_aruser(     4'b0000                ),
      .dnc_dbus_s0_rready(     dnc1_slv_r_R_Ready     ),
      .dnc_dbus_s0_rvalid(     dnc_dbus_s0_rvalid_0   ),
      .dnc_dbus_s0_rlast(      dnc_dbus_s0_rlast_0    ),
      .dnc_dbus_s0_rid(        dnc_dbus_s0_rid_0      ),
      .dnc_dbus_s0_rdata(      dnc_dbus_s0_rdata_0    ),
      .dnc_dbus_s0_ruser(      dnc_dbus_s0_ruser_0    ),
      .dnc_dbus_s0_rresp(      dnc_dbus_s0_rresp_0    ),
      .dnc_dbus_m0_arready(    dnc1_r_Ar_Ready        ),
      .dnc_dbus_m0_arvalid(    dnc_dbus_m0_arvalid_0  ),
      .dnc_dbus_m0_araddr(     dnc_dbus_m0_araddr_0   ),
      .dnc_dbus_m0_arid(       dnc_dbus_m0_arid_0     ),
      .dnc_dbus_m0_arlen(      dnc_dbus_m0_arlen_0    ),
      .dnc_dbus_m0_arburst(    dnc_dbus_m0_arburst_0  ),
      .dnc_dbus_m0_arcache(    dnc_dbus_m0_arcache_0  ),
      .dnc_dbus_m0_arlock(     dnc_dbus_m0_arlock_0   ),
      .dnc_dbus_m0_arprot(     dnc_dbus_m0_arprot_0   ),
      .dnc_dbus_m0_arsize(     dnc_dbus_m0_arsize_0   ),
      .dnc_dbus_m0_aruser(     dnc_dbus_m0_aruser_0   ),
      .dnc_dbus_m0_rready(     dnc_dbus_m0_rready_0   ),
      .dnc_dbus_m0_rvalid(     dnc1_r_R_Valid         ),
      .dnc_dbus_m0_rlast(      dnc1_r_R_Last          ),
      .dnc_dbus_m0_rid(        dnc1_r_R_Id            ),
      .dnc_dbus_m0_rdata(      dnc1_r_R_Data          ),
      .dnc_dbus_m0_ruser(      dnc1_r_R_User          ),
      .dnc_dbus_m0_rresp(      dnc1_r_R_Resp          ),
      .dnc_dbus_m0_awready(    dnc1_w_Aw_Ready        ),
      .dnc_dbus_m0_awaddr(     dnc_dbus_m0_awaddr_0   ),
      .dnc_dbus_m0_awburst(    dnc_dbus_m0_awburst_0  ),
      .dnc_dbus_m0_awcache(    dnc_dbus_m0_awcache_0  ),
      .dnc_dbus_m0_awid(       dnc_dbus_m0_awid_0     ),
      .dnc_dbus_m0_awlen(      dnc_dbus_m0_awlen_0    ),
      .dnc_dbus_m0_awlock(     dnc_dbus_m0_awlock_0   ),
      .dnc_dbus_m0_awprot(     dnc_dbus_m0_awprot_0   ),
      .dnc_dbus_m0_awsize(     dnc_dbus_m0_awsize_0   ),
      .dnc_dbus_m0_awuser(     dnc_dbus_m0_awuser_0   ),
      .dnc_dbus_m0_awvalid(    dnc_dbus_m0_awvalid_0  ),
      .dnc_dbus_m0_bready(     dnc_dbus_m0_bready_0   ),
      .dnc_dbus_m0_bid(        dnc1_w_B_Id            ),
      .dnc_dbus_m0_bresp(      dnc1_w_B_Resp          ),
      .dnc_dbus_m0_buser(      dnc1_w_B_User          ),
      .dnc_dbus_m0_bvalid(     dnc1_w_B_Valid         ),
      .dnc_dbus_m0_wready(     dnc1_w_W_Ready         ),
      .dnc_dbus_m0_wdata(      dnc_dbus_m0_wdata_0    ),
      .dnc_dbus_m0_wlast(      dnc_dbus_m0_wlast_0    ),
      .dnc_dbus_m0_wstrb(      dnc_dbus_m0_wstrb_0    ),
      .dnc_dbus_m0_wvalid(     dnc_dbus_m0_wvalid_0   ) 
      );


dnc #(
      .C_AXI_DATA_WIDTH_P   (32),
      .C_AXI_ADDR_WIDTH_P   (24),
      .C_AXI_STRB_WIDTH_P   (4),
      .C_AXI_ID_WIDTH_P     (2),
      .C_AXI_USER_WIDTH_P   (0),
      .C_AXI_LEN_WIDTH_P    (2),
      .DNC_AXI_DATA_WIDTH_P (1024),
      .DNC_AXI_ADDR_WIDTH_P (20),
      .DNC_AXI_STRB_WIDTH_P (128),
      .DNC_AXI_ID_WIDTH_P   (7),
      .DNC_AXI_USER_WIDTH_P (4),
      .DNC_AXI_LEN_WIDTH_P  (3) 
      ) 
u_dnc2 (
      .clk(                    aclk                   ),
      .arstn(                  arstn                  ),
      .dnc_cbus_s0_arready(    dnc_cbus_s0_arready_1  ),
      .dnc_cbus_s0_arvalid(    dnc2_ctrl_Ar_Valid     ),
      .dnc_cbus_s0_araddr(     dnc2_ctrl_Ar_Addr      ),
      .dnc_cbus_s0_arid(       dnc2_ctrl_Ar_Id        ),
      .dnc_cbus_s0_arlen(      dnc2_ctrl_Ar_Len       ),
      .dnc_cbus_s0_arburst(    dnc2_ctrl_Ar_Burst     ),
      .dnc_cbus_s0_arcache(    dnc2_ctrl_Ar_Cache     ),
      .dnc_cbus_s0_arlock(     dnc2_ctrl_Ar_Lock      ),
      .dnc_cbus_s0_arprot(     dnc2_ctrl_Ar_Prot      ),
      .dnc_cbus_s0_arsize(     dnc2_ctrl_Ar_Size      ),
      .dnc_cbus_s0_rready(     dnc2_ctrl_R_Ready      ),
      .dnc_cbus_s0_rvalid(     dnc_cbus_s0_rvalid_1   ),
      .dnc_cbus_s0_rlast(      dnc_cbus_s0_rlast_1    ),
      .dnc_cbus_s0_rid(        dnc_cbus_s0_rid_1      ),
      .dnc_cbus_s0_rdata(      dnc_cbus_s0_rdata_1    ),
      .dnc_cbus_s0_rresp(      dnc_cbus_s0_rresp_1    ),
      .dnc_cbus_s0_awready(    dnc_cbus_s0_awready_1  ),
      .dnc_cbus_s0_awvalid(    dnc2_ctrl_Aw_Valid     ),
      .dnc_cbus_s0_awaddr(     dnc2_ctrl_Aw_Addr      ),
      .dnc_cbus_s0_awid(       dnc2_ctrl_Aw_Id        ),
      .dnc_cbus_s0_awlen(      dnc2_ctrl_Aw_Len       ),
      .dnc_cbus_s0_awburst(    dnc2_ctrl_Aw_Burst     ),
      .dnc_cbus_s0_awcache(    dnc2_ctrl_Aw_Cache     ),
      .dnc_cbus_s0_awlock(     dnc2_ctrl_Aw_Lock      ),
      .dnc_cbus_s0_awprot(     dnc2_ctrl_Aw_Prot      ),
      .dnc_cbus_s0_awsize(     dnc2_ctrl_Aw_Size      ),
      .dnc_cbus_s0_wready(     dnc_cbus_s0_wready_1   ),
      .dnc_cbus_s0_wvalid(     dnc2_ctrl_W_Valid      ),
      .dnc_cbus_s0_wdata(      dnc2_ctrl_W_Data       ),
      .dnc_cbus_s0_wlast(      dnc2_ctrl_W_Last       ),
      .dnc_cbus_s0_wstrb(      dnc2_ctrl_W_Strb       ),
      .dnc_cbus_s0_bready(     dnc2_ctrl_B_Ready      ),
      .dnc_cbus_s0_bid(        dnc_cbus_s0_bid_1      ),
      .dnc_cbus_s0_bresp(      dnc_cbus_s0_bresp_1    ),
      .dnc_cbus_s0_bvalid(     dnc_cbus_s0_bvalid_1   ),
      .dnc_dbus_s0_arready(    dnc_dbus_s0_arready_1  ),
      .dnc_dbus_s0_arvalid(    dnc2_slv_r_Ar_Valid    ),
      .dnc_dbus_s0_araddr(     dnc2_slv_r_Ar_Addr     ),
      .dnc_dbus_s0_arid(       dnc2_slv_r_Ar_Id       ),
      .dnc_dbus_s0_arlen(      dnc2_slv_r_Ar_Len[2:0] ),
      .dnc_dbus_s0_arburst(    dnc2_slv_r_Ar_Burst    ),
      .dnc_dbus_s0_arcache(    dnc2_slv_r_Ar_Cache    ),
      .dnc_dbus_s0_arlock(     dnc2_slv_r_Ar_Lock     ),
      .dnc_dbus_s0_arprot(     dnc2_slv_r_Ar_Prot     ),
      .dnc_dbus_s0_arsize(     dnc2_slv_r_Ar_Size     ),
      .dnc_dbus_s0_aruser(     4'b0000                ),
      .dnc_dbus_s0_rready(     dnc2_slv_r_R_Ready     ),
      .dnc_dbus_s0_rvalid(     dnc_dbus_s0_rvalid_1   ),
      .dnc_dbus_s0_rlast(      dnc_dbus_s0_rlast_1    ),
      .dnc_dbus_s0_rid(        dnc_dbus_s0_rid_1      ),
      .dnc_dbus_s0_rdata(      dnc_dbus_s0_rdata_1    ),
      .dnc_dbus_s0_ruser(      dnc_dbus_s0_ruser_1    ),
      .dnc_dbus_s0_rresp(      dnc_dbus_s0_rresp_1    ),
      .dnc_dbus_m0_arready(    dnc2_r_Ar_Ready        ),
      .dnc_dbus_m0_arvalid(    dnc_dbus_m0_arvalid_1  ),
      .dnc_dbus_m0_araddr(     dnc_dbus_m0_araddr_1   ),
      .dnc_dbus_m0_arid(       dnc_dbus_m0_arid_1     ),
      .dnc_dbus_m0_arlen(      dnc_dbus_m0_arlen_1    ),
      .dnc_dbus_m0_arburst(    dnc_dbus_m0_arburst_1  ),
      .dnc_dbus_m0_arcache(    dnc_dbus_m0_arcache_1  ),
      .dnc_dbus_m0_arlock(     dnc_dbus_m0_arlock_1   ),
      .dnc_dbus_m0_arprot(     dnc_dbus_m0_arprot_1   ),
      .dnc_dbus_m0_arsize(     dnc_dbus_m0_arsize_1   ),
      .dnc_dbus_m0_aruser(     dnc_dbus_m0_aruser_1   ),
      .dnc_dbus_m0_rready(     dnc_dbus_m0_rready_1   ),
      .dnc_dbus_m0_rvalid(     dnc2_r_R_Valid         ),
      .dnc_dbus_m0_rlast(      dnc2_r_R_Last          ),
      .dnc_dbus_m0_rid(        dnc2_r_R_Id            ),
      .dnc_dbus_m0_rdata(      dnc2_r_R_Data          ),
      .dnc_dbus_m0_ruser(      dnc2_r_R_User          ),
      .dnc_dbus_m0_rresp(      dnc2_r_R_Resp          ),
      .dnc_dbus_m0_awready(    dnc2_w_Aw_Ready        ),
      .dnc_dbus_m0_awaddr(     dnc_dbus_m0_awaddr_1   ),
      .dnc_dbus_m0_awburst(    dnc_dbus_m0_awburst_1  ),
      .dnc_dbus_m0_awcache(    dnc_dbus_m0_awcache_1  ),
      .dnc_dbus_m0_awid(       dnc_dbus_m0_awid_1     ),
      .dnc_dbus_m0_awlen(      dnc_dbus_m0_awlen_1    ),
      .dnc_dbus_m0_awlock(     dnc_dbus_m0_awlock_1   ),
      .dnc_dbus_m0_awprot(     dnc_dbus_m0_awprot_1   ),
      .dnc_dbus_m0_awsize(     dnc_dbus_m0_awsize_1   ),
      .dnc_dbus_m0_awuser(     dnc_dbus_m0_awuser_1   ),
      .dnc_dbus_m0_awvalid(    dnc_dbus_m0_awvalid_1  ),
      .dnc_dbus_m0_bready(     dnc_dbus_m0_bready_1   ),
      .dnc_dbus_m0_bid(        dnc2_w_B_Id            ),
      .dnc_dbus_m0_bresp(      dnc2_w_B_Resp          ),
      .dnc_dbus_m0_buser(      dnc2_w_B_User          ),
      .dnc_dbus_m0_bvalid(     dnc2_w_B_Valid         ),
      .dnc_dbus_m0_wready(     dnc2_w_W_Ready         ),
      .dnc_dbus_m0_wdata(      dnc_dbus_m0_wdata_1    ),
      .dnc_dbus_m0_wlast(      dnc_dbus_m0_wlast_1    ),
      .dnc_dbus_m0_wstrb(      dnc_dbus_m0_wstrb_1    ),
      .dnc_dbus_m0_wvalid(     dnc_dbus_m0_wvalid_1   ) 
      );


dnc #(
      .C_AXI_DATA_WIDTH_P   (32),
      .C_AXI_ADDR_WIDTH_P   (24),
      .C_AXI_STRB_WIDTH_P   (4),
      .C_AXI_ID_WIDTH_P     (2),
      .C_AXI_USER_WIDTH_P   (0),
      .C_AXI_LEN_WIDTH_P    (2),
      .DNC_AXI_DATA_WIDTH_P (1024),
      .DNC_AXI_ADDR_WIDTH_P (20),
      .DNC_AXI_STRB_WIDTH_P (128),
      .DNC_AXI_ID_WIDTH_P   (7),
      .DNC_AXI_USER_WIDTH_P (4),
      .DNC_AXI_LEN_WIDTH_P  (3) 
      ) 
u_dnc3 (
      .clk(                    aclk                   ),
      .arstn(                  arstn                  ),
      .dnc_cbus_s0_arready(    dnc_cbus_s0_arready_2  ),
      .dnc_cbus_s0_arvalid(    dnc3_ctrl_Ar_Valid     ),
      .dnc_cbus_s0_araddr(     dnc3_ctrl_Ar_Addr      ),
      .dnc_cbus_s0_arid(       dnc3_ctrl_Ar_Id        ),
      .dnc_cbus_s0_arlen(      dnc3_ctrl_Ar_Len       ),
      .dnc_cbus_s0_arburst(    dnc3_ctrl_Ar_Burst     ),
      .dnc_cbus_s0_arcache(    dnc3_ctrl_Ar_Cache     ),
      .dnc_cbus_s0_arlock(     dnc3_ctrl_Ar_Lock      ),
      .dnc_cbus_s0_arprot(     dnc3_ctrl_Ar_Prot      ),
      .dnc_cbus_s0_arsize(     dnc3_ctrl_Ar_Size      ),
      .dnc_cbus_s0_rready(     dnc3_ctrl_R_Ready      ),
      .dnc_cbus_s0_rvalid(     dnc_cbus_s0_rvalid_2   ),
      .dnc_cbus_s0_rlast(      dnc_cbus_s0_rlast_2    ),
      .dnc_cbus_s0_rid(        dnc_cbus_s0_rid_2      ),
      .dnc_cbus_s0_rdata(      dnc_cbus_s0_rdata_2    ),
      .dnc_cbus_s0_rresp(      dnc_cbus_s0_rresp_2    ),
      .dnc_cbus_s0_awready(    dnc_cbus_s0_awready_2  ),
      .dnc_cbus_s0_awvalid(    dnc3_ctrl_Aw_Valid     ),
      .dnc_cbus_s0_awaddr(     dnc3_ctrl_Aw_Addr      ),
      .dnc_cbus_s0_awid(       dnc3_ctrl_Aw_Id        ),
      .dnc_cbus_s0_awlen(      dnc3_ctrl_Aw_Len       ),
      .dnc_cbus_s0_awburst(    dnc3_ctrl_Aw_Burst     ),
      .dnc_cbus_s0_awcache(    dnc3_ctrl_Aw_Cache     ),
      .dnc_cbus_s0_awlock(     dnc3_ctrl_Aw_Lock      ),
      .dnc_cbus_s0_awprot(     dnc3_ctrl_Aw_Prot      ),
      .dnc_cbus_s0_awsize(     dnc3_ctrl_Aw_Size      ),
      .dnc_cbus_s0_wready(     dnc_cbus_s0_wready_2   ),
      .dnc_cbus_s0_wvalid(     dnc3_ctrl_W_Valid      ),
      .dnc_cbus_s0_wdata(      dnc3_ctrl_W_Data       ),
      .dnc_cbus_s0_wlast(      dnc3_ctrl_W_Last       ),
      .dnc_cbus_s0_wstrb(      dnc3_ctrl_W_Strb       ),
      .dnc_cbus_s0_bready(     dnc3_ctrl_B_Ready      ),
      .dnc_cbus_s0_bid(        dnc_cbus_s0_bid_2      ),
      .dnc_cbus_s0_bresp(      dnc_cbus_s0_bresp_2    ),
      .dnc_cbus_s0_bvalid(     dnc_cbus_s0_bvalid_2   ),
      .dnc_dbus_s0_arready(    dnc_dbus_s0_arready_2  ),
      .dnc_dbus_s0_arvalid(    dnc3_slv_r_Ar_Valid    ),
      .dnc_dbus_s0_araddr(     dnc3_slv_r_Ar_Addr     ),
      .dnc_dbus_s0_arid(       dnc3_slv_r_Ar_Id       ),
      .dnc_dbus_s0_arlen(      dnc3_slv_r_Ar_Len[2:0] ),
      .dnc_dbus_s0_arburst(    dnc3_slv_r_Ar_Burst    ),
      .dnc_dbus_s0_arcache(    dnc3_slv_r_Ar_Cache    ),
      .dnc_dbus_s0_arlock(     dnc3_slv_r_Ar_Lock     ),
      .dnc_dbus_s0_arprot(     dnc3_slv_r_Ar_Prot     ),
      .dnc_dbus_s0_arsize(     dnc3_slv_r_Ar_Size     ),
      .dnc_dbus_s0_aruser(     4'b0000                ),
      .dnc_dbus_s0_rready(     dnc3_slv_r_R_Ready     ),
      .dnc_dbus_s0_rvalid(     dnc_dbus_s0_rvalid_2   ),
      .dnc_dbus_s0_rlast(      dnc_dbus_s0_rlast_2    ),
      .dnc_dbus_s0_rid(        dnc_dbus_s0_rid_2      ),
      .dnc_dbus_s0_rdata(      dnc_dbus_s0_rdata_2    ),
      .dnc_dbus_s0_ruser(      dnc_dbus_s0_ruser_2    ),
      .dnc_dbus_s0_rresp(      dnc_dbus_s0_rresp_2    ),
      .dnc_dbus_m0_arready(    dnc3_r_Ar_Ready        ),
      .dnc_dbus_m0_arvalid(    dnc_dbus_m0_arvalid_2  ),
      .dnc_dbus_m0_araddr(     dnc_dbus_m0_araddr_2   ),
      .dnc_dbus_m0_arid(       dnc_dbus_m0_arid_2     ),
      .dnc_dbus_m0_arlen(      dnc_dbus_m0_arlen_2    ),
      .dnc_dbus_m0_arburst(    dnc_dbus_m0_arburst_2  ),
      .dnc_dbus_m0_arcache(    dnc_dbus_m0_arcache_2  ),
      .dnc_dbus_m0_arlock(     dnc_dbus_m0_arlock_2   ),
      .dnc_dbus_m0_arprot(     dnc_dbus_m0_arprot_2   ),
      .dnc_dbus_m0_arsize(     dnc_dbus_m0_arsize_2   ),
      .dnc_dbus_m0_aruser(     dnc_dbus_m0_aruser_2   ),
      .dnc_dbus_m0_rready(     dnc_dbus_m0_rready_2   ),
      .dnc_dbus_m0_rvalid(     dnc3_r_R_Valid         ),
      .dnc_dbus_m0_rlast(      dnc3_r_R_Last          ),
      .dnc_dbus_m0_rid(        dnc3_r_R_Id            ),
      .dnc_dbus_m0_rdata(      dnc3_r_R_Data          ),
      .dnc_dbus_m0_ruser(      dnc3_r_R_User          ),
      .dnc_dbus_m0_rresp(      dnc3_r_R_Resp          ),
      .dnc_dbus_m0_awready(    dnc3_w_Aw_Ready        ),
      .dnc_dbus_m0_awaddr(     dnc_dbus_m0_awaddr_2   ),
      .dnc_dbus_m0_awburst(    dnc_dbus_m0_awburst_2  ),
      .dnc_dbus_m0_awcache(    dnc_dbus_m0_awcache_2  ),
      .dnc_dbus_m0_awid(       dnc_dbus_m0_awid_2     ),
      .dnc_dbus_m0_awlen(      dnc_dbus_m0_awlen_2    ),
      .dnc_dbus_m0_awlock(     dnc_dbus_m0_awlock_2   ),
      .dnc_dbus_m0_awprot(     dnc_dbus_m0_awprot_2   ),
      .dnc_dbus_m0_awsize(     dnc_dbus_m0_awsize_2   ),
      .dnc_dbus_m0_awuser(     dnc_dbus_m0_awuser_2   ),
      .dnc_dbus_m0_awvalid(    dnc_dbus_m0_awvalid_2  ),
      .dnc_dbus_m0_bready(     dnc_dbus_m0_bready_2   ),
      .dnc_dbus_m0_bid(        dnc3_w_B_Id            ),
      .dnc_dbus_m0_bresp(      dnc3_w_B_Resp          ),
      .dnc_dbus_m0_buser(      dnc3_w_B_User          ),
      .dnc_dbus_m0_bvalid(     dnc3_w_B_Valid         ),
      .dnc_dbus_m0_wready(     dnc3_w_W_Ready         ),
      .dnc_dbus_m0_wdata(      dnc_dbus_m0_wdata_2    ),
      .dnc_dbus_m0_wlast(      dnc_dbus_m0_wlast_2    ),
      .dnc_dbus_m0_wstrb(      dnc_dbus_m0_wstrb_2    ),
      .dnc_dbus_m0_wvalid(     dnc_dbus_m0_wvalid_2   ) 
      );


atom_dcluster_dcc #(
      .C_AXI_DATA_WIDTH_P (32),
      .C_AXI_ADDR_WIDTH_P (24),
      .C_AXI_STRB_WIDTH_P (4),
      .C_AXI_ID_WIDTH_P   (2),
      .C_AXI_USER_WIDTH_P (0),
      .C_AXI_LEN_WIDTH_P  (2),
      .D_AXI_DATA_WIDTH_P (1024),
      .D_AXI_ADDR_WIDTH_P (37),
      .D_AXI_STRB_WIDTH_P (128),
      .D_AXI_ID_WIDTH_P   (7),
      .D_AXI_USER_WIDTH_P (4),
      .D_AXI_LEN_WIDTH_P  (5) 
      ) 
u_dnc_cc (
      .clk(                aclk                         ),
      .arstn(              arstn                        ),
      .cbus_s0_arready(    cbus_s0_arready              ),
      .cbus_s0_arvalid(    dcluster0_ctrl_Ar_Valid      ),
      .cbus_s0_araddr(     dcluster0_ctrl_Ar_Addr       ),
      .cbus_s0_arid(       dcluster0_ctrl_Ar_Id         ),
      .cbus_s0_arlen(      dcluster0_ctrl_Ar_Len        ),
      .cbus_s0_arburst(    dcluster0_ctrl_Ar_Burst      ),
      .cbus_s0_arcache(    dcluster0_ctrl_Ar_Cache      ),
      .cbus_s0_arlock(     dcluster0_ctrl_Ar_Lock       ),
      .cbus_s0_arprot(     dcluster0_ctrl_Ar_Prot       ),
      .cbus_s0_arsize(     dcluster0_ctrl_Ar_Size       ),
      .cbus_s0_rready(     dcluster0_ctrl_R_Ready       ),
      .cbus_s0_rvalid(     cbus_s0_rvalid               ),
      .cbus_s0_rlast(      cbus_s0_rlast                ),
      .cbus_s0_rid(        cbus_s0_rid                  ),
      .cbus_s0_rdata(      cbus_s0_rdata                ),
      .cbus_s0_rresp(      cbus_s0_rresp                ),
      .cbus_s0_awready(    cbus_s0_awready              ),
      .cbus_s0_awvalid(    dcluster0_ctrl_Aw_Valid      ),
      .cbus_s0_awaddr(     dcluster0_ctrl_Aw_Addr       ),
      .cbus_s0_awid(       dcluster0_ctrl_Aw_Id         ),
      .cbus_s0_awlen(      dcluster0_ctrl_Aw_Len        ),
      .cbus_s0_awburst(    dcluster0_ctrl_Aw_Burst      ),
      .cbus_s0_awcache(    dcluster0_ctrl_Aw_Cache      ),
      .cbus_s0_awlock(     dcluster0_ctrl_Aw_Lock       ),
      .cbus_s0_awprot(     dcluster0_ctrl_Aw_Prot       ),
      .cbus_s0_awsize(     dcluster0_ctrl_Aw_Size       ),
      .cbus_s0_wready(     cbus_s0_wready               ),
      .cbus_s0_wvalid(     dcluster0_ctrl_W_Valid       ),
      .cbus_s0_wdata(      dcluster0_ctrl_W_Data        ),
      .cbus_s0_wlast(      dcluster0_ctrl_W_Last        ),
      .cbus_s0_wstrb(      dcluster0_ctrl_W_Strb        ),
      .cbus_s0_bready(     dcluster0_ctrl_B_Ready       ),
      .cbus_s0_bid(        cbus_s0_bid                  ),
      .cbus_s0_bresp(      cbus_s0_bresp                ),
      .cbus_s0_bvalid(     cbus_s0_bvalid               ),
      .dbus_s0_arready(    dbus_s0_arready              ),
      .dbus_s0_arvalid(    dcluster0_cc0_slv_r_Ar_Valid ),
      .dbus_s0_araddr(     dcluster0_cc0_slv_r_Ar_Addr  ),
      .dbus_s0_arid(       dcluster0_cc0_slv_r_Ar_Id    ),
      .dbus_s0_arlen(      dcluster0_cc0_slv_r_Ar_Len   ),
      .dbus_s0_arburst(    dcluster0_cc0_slv_r_Ar_Burst ),
      .dbus_s0_arcache(    dcluster0_cc0_slv_r_Ar_Cache ),
      .dbus_s0_arlock(     dcluster0_cc0_slv_r_Ar_Lock  ),
      .dbus_s0_arprot(     dcluster0_cc0_slv_r_Ar_Prot  ),
      .dbus_s0_arsize(     dcluster0_cc0_slv_r_Ar_Size  ),
      .dbus_s0_aruser(     dcluster0_cc0_slv_r_Ar_User  ),
      .dbus_s0_rready(     dcluster0_cc0_slv_r_R_Ready  ),
      .dbus_s0_rvalid(     dbus_s0_rvalid               ),
      .dbus_s0_rlast(      dbus_s0_rlast                ),
      .dbus_s0_rid(        dbus_s0_rid                  ),
      .dbus_s0_rdata(      dbus_s0_rdata                ),
      .dbus_s0_ruser(      dbus_s0_ruser                ),
      .dbus_s0_rresp(      dbus_s0_rresp                ),
      .dbus_s1_arready(    dbus_s1_arready              ),
      .dbus_s1_arvalid(    dcluster0_cc1_slv_r_Ar_Valid ),
      .dbus_s1_araddr(     dcluster0_cc1_slv_r_Ar_Addr  ),
      .dbus_s1_arid(       dcluster0_cc1_slv_r_Ar_Id    ),
      .dbus_s1_arlen(      dcluster0_cc1_slv_r_Ar_Len   ),
      .dbus_s1_arburst(    dcluster0_cc1_slv_r_Ar_Burst ),
      .dbus_s1_arcache(    dcluster0_cc1_slv_r_Ar_Cache ),
      .dbus_s1_arlock(     dcluster0_cc1_slv_r_Ar_Lock  ),
      .dbus_s1_arprot(     dcluster0_cc1_slv_r_Ar_Prot  ),
      .dbus_s1_arsize(     dcluster0_cc1_slv_r_Ar_Size  ),
      .dbus_s1_aruser(     dcluster0_cc1_slv_r_Ar_User  ),
      .dbus_s1_rready(     dcluster0_cc1_slv_r_R_Ready  ),
      .dbus_s1_rvalid(     dbus_s1_rvalid               ),
      .dbus_s1_rlast(      dbus_s1_rlast                ),
      .dbus_s1_rid(        dbus_s1_rid                  ),
      .dbus_s1_rdata(      dbus_s1_rdata                ),
      .dbus_s1_ruser(      dbus_s1_ruser                ),
      .dbus_s1_rresp(      dbus_s1_rresp                ),
      .dbus_s2_arready(    dbus_s2_arready              ),
      .dbus_s2_arvalid(    dcluster0_cc2_slv_r_Ar_Valid ),
      .dbus_s2_araddr(     dcluster0_cc2_slv_r_Ar_Addr  ),
      .dbus_s2_arid(       dcluster0_cc2_slv_r_Ar_Id    ),
      .dbus_s2_arlen(      dcluster0_cc2_slv_r_Ar_Len   ),
      .dbus_s2_arburst(    dcluster0_cc2_slv_r_Ar_Burst ),
      .dbus_s2_arcache(    dcluster0_cc2_slv_r_Ar_Cache ),
      .dbus_s2_arlock(     dcluster0_cc2_slv_r_Ar_Lock  ),
      .dbus_s2_arprot(     dcluster0_cc2_slv_r_Ar_Prot  ),
      .dbus_s2_arsize(     dcluster0_cc2_slv_r_Ar_Size  ),
      .dbus_s2_aruser(     dcluster0_cc2_slv_r_Ar_User  ),
      .dbus_s2_rready(     dcluster0_cc2_slv_r_R_Ready  ),
      .dbus_s2_rvalid(     dbus_s2_rvalid               ),
      .dbus_s2_rlast(      dbus_s2_rlast                ),
      .dbus_s2_rid(        dbus_s2_rid                  ),
      .dbus_s2_rdata(      dbus_s2_rdata                ),
      .dbus_s2_ruser(      dbus_s2_ruser                ),
      .dbus_s2_rresp(      dbus_s2_rresp                ),
      .dbus_s3_arready(    dbus_s3_arready              ),
      .dbus_s3_arvalid(    dcluster0_cc3_slv_r_Ar_Valid ),
      .dbus_s3_araddr(     dcluster0_cc3_slv_r_Ar_Addr  ),
      .dbus_s3_arid(       dcluster0_cc3_slv_r_Ar_Id    ),
      .dbus_s3_arlen(      dcluster0_cc3_slv_r_Ar_Len   ),
      .dbus_s3_arburst(    dcluster0_cc3_slv_r_Ar_Burst ),
      .dbus_s3_arcache(    dcluster0_cc3_slv_r_Ar_Cache ),
      .dbus_s3_arlock(     dcluster0_cc3_slv_r_Ar_Lock  ),
      .dbus_s3_arprot(     dcluster0_cc3_slv_r_Ar_Prot  ),
      .dbus_s3_arsize(     dcluster0_cc3_slv_r_Ar_Size  ),
      .dbus_s3_aruser(     dcluster0_cc3_slv_r_Ar_User  ),
      .dbus_s3_rready(     dcluster0_cc3_slv_r_R_Ready  ),
      .dbus_s3_rvalid(     dbus_s3_rvalid               ),
      .dbus_s3_rlast(      dbus_s3_rlast                ),
      .dbus_s3_rid(        dbus_s3_rid                  ),
      .dbus_s3_rdata(      dbus_s3_rdata                ),
      .dbus_s3_ruser(      dbus_s3_ruser                ),
      .dbus_s3_rresp(      dbus_s3_rresp                ),
      .dbus_m0_arready(    dcluster0_cc0_r_Ar_Ready     ),
      .dbus_m0_arvalid(    dbus_m0_arvalid              ),
      .dbus_m0_araddr(     dbus_m0_araddr               ),
      .dbus_m0_arid(       dbus_m0_arid                 ),
      .dbus_m0_arlen(      dbus_m0_arlen                ),
      .dbus_m0_arburst(    dbus_m0_arburst              ),
      .dbus_m0_arcache(    dbus_m0_arcache              ),
      .dbus_m0_arlock(     dbus_m0_arlock               ),
      .dbus_m0_arprot(     dbus_m0_arprot               ),
      .dbus_m0_arsize(     dbus_m0_arsize               ),
      .dbus_m0_aruser(                                  ),
      .dbus_m0_rready(     dbus_m0_rready               ),
      .dbus_m0_rvalid(     dcluster0_cc0_r_R_Valid      ),
      .dbus_m0_rlast(      dcluster0_cc0_r_R_Last       ),
      .dbus_m0_rid(        dcluster0_cc0_r_R_Id         ),
      .dbus_m0_rdata(      dcluster0_cc0_r_R_Data       ),
      .dbus_m0_ruser(      dcluster0_cc0_r_R_User       ),
      .dbus_m0_rresp(      dcluster0_cc0_r_R_Resp       ),
      .dbus_m1_arready(    dcluster0_cc1_r_Ar_Ready     ),
      .dbus_m1_arvalid(    dbus_m1_arvalid              ),
      .dbus_m1_araddr(     dbus_m1_araddr               ),
      .dbus_m1_arid(       dbus_m1_arid                 ),
      .dbus_m1_arlen(      dbus_m1_arlen                ),
      .dbus_m1_arburst(    dbus_m1_arburst              ),
      .dbus_m1_arcache(    dbus_m1_arcache              ),
      .dbus_m1_arlock(     dbus_m1_arlock               ),
      .dbus_m1_arprot(     dbus_m1_arprot               ),
      .dbus_m1_arsize(     dbus_m1_arsize               ),
      .dbus_m1_aruser(                                  ),
      .dbus_m1_rready(     dbus_m1_rready               ),
      .dbus_m1_rvalid(     dcluster0_cc1_r_R_Valid      ),
      .dbus_m1_rlast(      dcluster0_cc1_r_R_Last       ),
      .dbus_m1_rid(        dcluster0_cc1_r_R_Id         ),
      .dbus_m1_rdata(      dcluster0_cc1_r_R_Data       ),
      .dbus_m1_ruser(      dcluster0_cc1_r_R_User       ),
      .dbus_m1_rresp(      dcluster0_cc1_r_R_Resp       ),
      .dbus_m2_arready(    dcluster0_cc2_r_Ar_Ready     ),
      .dbus_m2_arvalid(    dbus_m2_arvalid              ),
      .dbus_m2_araddr(     dbus_m2_araddr               ),
      .dbus_m2_arid(       dbus_m2_arid                 ),
      .dbus_m2_arlen(      dbus_m2_arlen                ),
      .dbus_m2_arburst(    dbus_m2_arburst              ),
      .dbus_m2_arcache(    dbus_m2_arcache              ),
      .dbus_m2_arlock(     dbus_m2_arlock               ),
      .dbus_m2_arprot(     dbus_m2_arprot               ),
      .dbus_m2_arsize(     dbus_m2_arsize               ),
      .dbus_m2_aruser(                                  ),
      .dbus_m2_rready(     dbus_m2_rready               ),
      .dbus_m2_rvalid(     dcluster0_cc2_r_R_Valid      ),
      .dbus_m2_rlast(      dcluster0_cc2_r_R_Last       ),
      .dbus_m2_rid(        dcluster0_cc2_r_R_Id         ),
      .dbus_m2_rdata(      dcluster0_cc2_r_R_Data       ),
      .dbus_m2_ruser(      dcluster0_cc2_r_R_User       ),
      .dbus_m2_rresp(      dcluster0_cc2_r_R_Resp       ),
      .dbus_m3_arready(    dcluster0_cc3_r_Ar_Ready     ),
      .dbus_m3_arvalid(    dbus_m3_arvalid              ),
      .dbus_m3_araddr(     dbus_m3_araddr               ),
      .dbus_m3_arid(       dbus_m3_arid                 ),
      .dbus_m3_arlen(      dbus_m3_arlen                ),
      .dbus_m3_arburst(    dbus_m3_arburst              ),
      .dbus_m3_arcache(    dbus_m3_arcache              ),
      .dbus_m3_arlock(     dbus_m3_arlock               ),
      .dbus_m3_arprot(     dbus_m3_arprot               ),
      .dbus_m3_arsize(     dbus_m3_arsize               ),
      .dbus_m3_aruser(                                  ),
      .dbus_m3_rready(     dbus_m3_rready               ),
      .dbus_m3_rvalid(     dcluster0_cc3_r_R_Valid      ),
      .dbus_m3_rlast(      dcluster0_cc3_r_R_Last       ),
      .dbus_m3_rid(        dcluster0_cc3_r_R_Id         ),
      .dbus_m3_rdata(      dcluster0_cc3_r_R_Data       ),
      .dbus_m3_ruser(      dcluster0_cc3_r_R_User       ),
      .dbus_m3_rresp(      dcluster0_cc3_r_R_Resp       ) 
      );


cbus_Structure_Module_dcluster0 u_cbus_Structure_Module_dcluster0 (
      .dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdCnt(                dp_Link_n03_astResp001_to_Link_n03_asiResp001_rdcnt             ),
      .dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n03_astResp001_to_Link_n03_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n03_astResp001_to_Link_n03_asiResp001_RxCtl_PwrOnRst(       dp_Link_n03_astResp001_to_Link_n03_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n03_astResp001_to_Link_n03_asiResp001_RdPtr(                dp_Link_n03_astResp001_to_Link_n03_asiResp001_rdptr             ),
      .dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n03_astResp001_to_Link_n03_asiResp001_txctl_pwronrstack ),
      .dp_Link_n03_astResp001_to_Link_n03_asiResp001_TxCtl_PwrOnRst(       dp_Link_n03_astResp001_to_Link_n03_asiResp001_txctl_pwronrst    ),
      .dp_Link_n03_astResp001_to_Link_n03_asiResp001_Data(                 dp_Link_n03_astResp001_to_Link_n03_asiResp001_data              ),
      .dp_Link_n03_astResp001_to_Link_n03_asiResp001_WrCnt(                dp_Link_n03_astResp001_to_Link_n03_asiResp001_wrcnt             ),
      .dp_Link_n03_asi_to_Link_n03_ast_RdCnt(                              dp_Link_n03_asi_to_Link_n03_ast_rdcnt                           ),
      .dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRstAck(                  dp_Link_n03_asi_to_Link_n03_ast_rxctl_pwronrstack               ),
      .dp_Link_n03_asi_to_Link_n03_ast_RxCtl_PwrOnRst(                     dp_Link_n03_asi_to_Link_n03_ast_rxctl_pwronrst                  ),
      .dp_Link_n03_asi_to_Link_n03_ast_RdPtr(                              dp_Link_n03_asi_to_Link_n03_ast_rdptr                           ),
      .dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRstAck(                  dp_Link_n03_asi_to_Link_n03_ast_txctl_pwronrstack               ),
      .dp_Link_n03_asi_to_Link_n03_ast_TxCtl_PwrOnRst(                     dp_Link_n03_asi_to_Link_n03_ast_txctl_pwronrst                  ),
      .dp_Link_n03_asi_to_Link_n03_ast_Data(                               dp_Link_n03_asi_to_Link_n03_ast_data                            ),
      .dp_Link_n03_asi_to_Link_n03_ast_WrCnt(                              dp_Link_n03_asi_to_Link_n03_ast_wrcnt                           ),
      .dnc3_ctrl_R_Last(                                                   dnc_cbus_s0_rlast_2                                             ),
      .dnc3_ctrl_R_Resp(                                                   dnc_cbus_s0_rresp_2                                             ),
      .dnc3_ctrl_R_Valid(                                                  dnc_cbus_s0_rvalid_2                                            ),
      .dnc3_ctrl_R_Ready(                                                  dnc3_ctrl_R_Ready                                               ),
      .dnc3_ctrl_R_Data(                                                   dnc_cbus_s0_rdata_2                                             ),
      .dnc3_ctrl_R_Id(                                                     dnc_cbus_s0_rid_2                                               ),
      .dnc3_ctrl_B_Ready(                                                  dnc3_ctrl_B_Ready                                               ),
      .dnc3_ctrl_B_Resp(                                                   dnc_cbus_s0_bresp_2                                             ),
      .dnc3_ctrl_B_Valid(                                                  dnc_cbus_s0_bvalid_2                                            ),
      .dnc3_ctrl_B_Id(                                                     dnc_cbus_s0_bid_2                                               ),
      .dnc3_ctrl_Aw_Prot(                                                  dnc3_ctrl_Aw_Prot                                               ),
      .dnc3_ctrl_Aw_Addr(                                                  dnc3_ctrl_Aw_Addr                                               ),
      .dnc3_ctrl_Aw_Burst(                                                 dnc3_ctrl_Aw_Burst                                              ),
      .dnc3_ctrl_Aw_Lock(                                                  dnc3_ctrl_Aw_Lock                                               ),
      .dnc3_ctrl_Aw_Cache(                                                 dnc3_ctrl_Aw_Cache                                              ),
      .dnc3_ctrl_Aw_Len(                                                   dnc3_ctrl_Aw_Len                                                ),
      .dnc3_ctrl_Aw_Valid(                                                 dnc3_ctrl_Aw_Valid                                              ),
      .dnc3_ctrl_Aw_Ready(                                                 dnc_cbus_s0_awready_2                                           ),
      .dnc3_ctrl_Aw_Id(                                                    dnc3_ctrl_Aw_Id                                                 ),
      .dnc3_ctrl_Aw_Size(                                                  dnc3_ctrl_Aw_Size                                               ),
      .dnc3_ctrl_W_Last(                                                   dnc3_ctrl_W_Last                                                ),
      .dnc3_ctrl_W_Valid(                                                  dnc3_ctrl_W_Valid                                               ),
      .dnc3_ctrl_W_Ready(                                                  dnc_cbus_s0_wready_2                                            ),
      .dnc3_ctrl_W_Strb(                                                   dnc3_ctrl_W_Strb                                                ),
      .dnc3_ctrl_W_Data(                                                   dnc3_ctrl_W_Data                                                ),
      .dnc3_ctrl_Ar_Prot(                                                  dnc3_ctrl_Ar_Prot                                               ),
      .dnc3_ctrl_Ar_Addr(                                                  dnc3_ctrl_Ar_Addr                                               ),
      .dnc3_ctrl_Ar_Burst(                                                 dnc3_ctrl_Ar_Burst                                              ),
      .dnc3_ctrl_Ar_Lock(                                                  dnc3_ctrl_Ar_Lock                                               ),
      .dnc3_ctrl_Ar_Cache(                                                 dnc3_ctrl_Ar_Cache                                              ),
      .dnc3_ctrl_Ar_Len(                                                   dnc3_ctrl_Ar_Len                                                ),
      .dnc3_ctrl_Ar_Valid(                                                 dnc3_ctrl_Ar_Valid                                              ),
      .dnc3_ctrl_Ar_Ready(                                                 dnc_cbus_s0_arready_2                                           ),
      .dnc3_ctrl_Ar_Id(                                                    dnc3_ctrl_Ar_Id                                                 ),
      .dnc3_ctrl_Ar_Size(                                                  dnc3_ctrl_Ar_Size                                               ),
      .dnc2_ctrl_R_Last(                                                   dnc_cbus_s0_rlast_1                                             ),
      .dnc2_ctrl_R_Resp(                                                   dnc_cbus_s0_rresp_1                                             ),
      .dnc2_ctrl_R_Valid(                                                  dnc_cbus_s0_rvalid_1                                            ),
      .dnc2_ctrl_R_Ready(                                                  dnc2_ctrl_R_Ready                                               ),
      .dnc2_ctrl_R_Data(                                                   dnc_cbus_s0_rdata_1                                             ),
      .dnc2_ctrl_R_Id(                                                     dnc_cbus_s0_rid_1                                               ),
      .dnc2_ctrl_B_Ready(                                                  dnc2_ctrl_B_Ready                                               ),
      .dnc2_ctrl_B_Resp(                                                   dnc_cbus_s0_bresp_1                                             ),
      .dnc2_ctrl_B_Valid(                                                  dnc_cbus_s0_bvalid_1                                            ),
      .dnc2_ctrl_B_Id(                                                     dnc_cbus_s0_bid_1                                               ),
      .dnc2_ctrl_Aw_Prot(                                                  dnc2_ctrl_Aw_Prot                                               ),
      .dnc2_ctrl_Aw_Addr(                                                  dnc2_ctrl_Aw_Addr                                               ),
      .dnc2_ctrl_Aw_Burst(                                                 dnc2_ctrl_Aw_Burst                                              ),
      .dnc2_ctrl_Aw_Lock(                                                  dnc2_ctrl_Aw_Lock                                               ),
      .dnc2_ctrl_Aw_Cache(                                                 dnc2_ctrl_Aw_Cache                                              ),
      .dnc2_ctrl_Aw_Len(                                                   dnc2_ctrl_Aw_Len                                                ),
      .dnc2_ctrl_Aw_Valid(                                                 dnc2_ctrl_Aw_Valid                                              ),
      .dnc2_ctrl_Aw_Ready(                                                 dnc_cbus_s0_awready_1                                           ),
      .dnc2_ctrl_Aw_Id(                                                    dnc2_ctrl_Aw_Id                                                 ),
      .dnc2_ctrl_Aw_Size(                                                  dnc2_ctrl_Aw_Size                                               ),
      .dnc2_ctrl_W_Last(                                                   dnc2_ctrl_W_Last                                                ),
      .dnc2_ctrl_W_Valid(                                                  dnc2_ctrl_W_Valid                                               ),
      .dnc2_ctrl_W_Ready(                                                  dnc_cbus_s0_wready_1                                            ),
      .dnc2_ctrl_W_Strb(                                                   dnc2_ctrl_W_Strb                                                ),
      .dnc2_ctrl_W_Data(                                                   dnc2_ctrl_W_Data                                                ),
      .dnc2_ctrl_Ar_Prot(                                                  dnc2_ctrl_Ar_Prot                                               ),
      .dnc2_ctrl_Ar_Addr(                                                  dnc2_ctrl_Ar_Addr                                               ),
      .dnc2_ctrl_Ar_Burst(                                                 dnc2_ctrl_Ar_Burst                                              ),
      .dnc2_ctrl_Ar_Lock(                                                  dnc2_ctrl_Ar_Lock                                               ),
      .dnc2_ctrl_Ar_Cache(                                                 dnc2_ctrl_Ar_Cache                                              ),
      .dnc2_ctrl_Ar_Len(                                                   dnc2_ctrl_Ar_Len                                                ),
      .dnc2_ctrl_Ar_Valid(                                                 dnc2_ctrl_Ar_Valid                                              ),
      .dnc2_ctrl_Ar_Ready(                                                 dnc_cbus_s0_arready_1                                           ),
      .dnc2_ctrl_Ar_Id(                                                    dnc2_ctrl_Ar_Id                                                 ),
      .dnc2_ctrl_Ar_Size(                                                  dnc2_ctrl_Ar_Size                                               ),
      .dnc1_ctrl_R_Last(                                                   dnc_cbus_s0_rlast_0                                             ),
      .dnc1_ctrl_R_Resp(                                                   dnc_cbus_s0_rresp_0                                             ),
      .dnc1_ctrl_R_Valid(                                                  dnc_cbus_s0_rvalid_0                                            ),
      .dnc1_ctrl_R_Ready(                                                  dnc1_ctrl_R_Ready                                               ),
      .dnc1_ctrl_R_Data(                                                   dnc_cbus_s0_rdata_0                                             ),
      .dnc1_ctrl_R_Id(                                                     dnc_cbus_s0_rid_0                                               ),
      .dnc1_ctrl_B_Ready(                                                  dnc1_ctrl_B_Ready                                               ),
      .dnc1_ctrl_B_Resp(                                                   dnc_cbus_s0_bresp_0                                             ),
      .dnc1_ctrl_B_Valid(                                                  dnc_cbus_s0_bvalid_0                                            ),
      .dnc1_ctrl_B_Id(                                                     dnc_cbus_s0_bid_0                                               ),
      .dnc1_ctrl_Aw_Prot(                                                  dnc1_ctrl_Aw_Prot                                               ),
      .dnc1_ctrl_Aw_Addr(                                                  dnc1_ctrl_Aw_Addr                                               ),
      .dnc1_ctrl_Aw_Burst(                                                 dnc1_ctrl_Aw_Burst                                              ),
      .dnc1_ctrl_Aw_Lock(                                                  dnc1_ctrl_Aw_Lock                                               ),
      .dnc1_ctrl_Aw_Cache(                                                 dnc1_ctrl_Aw_Cache                                              ),
      .dnc1_ctrl_Aw_Len(                                                   dnc1_ctrl_Aw_Len                                                ),
      .dnc1_ctrl_Aw_Valid(                                                 dnc1_ctrl_Aw_Valid                                              ),
      .dnc1_ctrl_Aw_Ready(                                                 dnc_cbus_s0_awready_0                                           ),
      .dnc1_ctrl_Aw_Id(                                                    dnc1_ctrl_Aw_Id                                                 ),
      .dnc1_ctrl_Aw_Size(                                                  dnc1_ctrl_Aw_Size                                               ),
      .dnc1_ctrl_W_Last(                                                   dnc1_ctrl_W_Last                                                ),
      .dnc1_ctrl_W_Valid(                                                  dnc1_ctrl_W_Valid                                               ),
      .dnc1_ctrl_W_Ready(                                                  dnc_cbus_s0_wready_0                                            ),
      .dnc1_ctrl_W_Strb(                                                   dnc1_ctrl_W_Strb                                                ),
      .dnc1_ctrl_W_Data(                                                   dnc1_ctrl_W_Data                                                ),
      .dnc1_ctrl_Ar_Prot(                                                  dnc1_ctrl_Ar_Prot                                               ),
      .dnc1_ctrl_Ar_Addr(                                                  dnc1_ctrl_Ar_Addr                                               ),
      .dnc1_ctrl_Ar_Burst(                                                 dnc1_ctrl_Ar_Burst                                              ),
      .dnc1_ctrl_Ar_Lock(                                                  dnc1_ctrl_Ar_Lock                                               ),
      .dnc1_ctrl_Ar_Cache(                                                 dnc1_ctrl_Ar_Cache                                              ),
      .dnc1_ctrl_Ar_Len(                                                   dnc1_ctrl_Ar_Len                                                ),
      .dnc1_ctrl_Ar_Valid(                                                 dnc1_ctrl_Ar_Valid                                              ),
      .dnc1_ctrl_Ar_Ready(                                                 dnc_cbus_s0_arready_0                                           ),
      .dnc1_ctrl_Ar_Id(                                                    dnc1_ctrl_Ar_Id                                                 ),
      .dnc1_ctrl_Ar_Size(                                                  dnc1_ctrl_Ar_Size                                               ),
      .dnc0_ctrl_R_Last(                                                   dnc_cbus_s0_rlast                                               ),
      .dnc0_ctrl_R_Resp(                                                   dnc_cbus_s0_rresp                                               ),
      .dnc0_ctrl_R_Valid(                                                  dnc_cbus_s0_rvalid                                              ),
      .dnc0_ctrl_R_Ready(                                                  dnc0_ctrl_R_Ready                                               ),
      .dnc0_ctrl_R_Data(                                                   dnc_cbus_s0_rdata                                               ),
      .dnc0_ctrl_R_Id(                                                     dnc_cbus_s0_rid                                                 ),
      .dnc0_ctrl_B_Ready(                                                  dnc0_ctrl_B_Ready                                               ),
      .dnc0_ctrl_B_Resp(                                                   dnc_cbus_s0_bresp                                               ),
      .dnc0_ctrl_B_Valid(                                                  dnc_cbus_s0_bvalid                                              ),
      .dnc0_ctrl_B_Id(                                                     dnc_cbus_s0_bid                                                 ),
      .dnc0_ctrl_Aw_Prot(                                                  dnc0_ctrl_Aw_Prot                                               ),
      .dnc0_ctrl_Aw_Addr(                                                  dnc0_ctrl_Aw_Addr                                               ),
      .dnc0_ctrl_Aw_Burst(                                                 dnc0_ctrl_Aw_Burst                                              ),
      .dnc0_ctrl_Aw_Lock(                                                  dnc0_ctrl_Aw_Lock                                               ),
      .dnc0_ctrl_Aw_Cache(                                                 dnc0_ctrl_Aw_Cache                                              ),
      .dnc0_ctrl_Aw_Len(                                                   dnc0_ctrl_Aw_Len                                                ),
      .dnc0_ctrl_Aw_Valid(                                                 dnc0_ctrl_Aw_Valid                                              ),
      .dnc0_ctrl_Aw_Ready(                                                 dnc_cbus_s0_awready                                             ),
      .dnc0_ctrl_Aw_Id(                                                    dnc0_ctrl_Aw_Id                                                 ),
      .dnc0_ctrl_Aw_Size(                                                  dnc0_ctrl_Aw_Size                                               ),
      .dnc0_ctrl_W_Last(                                                   dnc0_ctrl_W_Last                                                ),
      .dnc0_ctrl_W_Valid(                                                  dnc0_ctrl_W_Valid                                               ),
      .dnc0_ctrl_W_Ready(                                                  dnc_cbus_s0_wready                                              ),
      .dnc0_ctrl_W_Strb(                                                   dnc0_ctrl_W_Strb                                                ),
      .dnc0_ctrl_W_Data(                                                   dnc0_ctrl_W_Data                                                ),
      .dnc0_ctrl_Ar_Prot(                                                  dnc0_ctrl_Ar_Prot                                               ),
      .dnc0_ctrl_Ar_Addr(                                                  dnc0_ctrl_Ar_Addr                                               ),
      .dnc0_ctrl_Ar_Burst(                                                 dnc0_ctrl_Ar_Burst                                              ),
      .dnc0_ctrl_Ar_Lock(                                                  dnc0_ctrl_Ar_Lock                                               ),
      .dnc0_ctrl_Ar_Cache(                                                 dnc0_ctrl_Ar_Cache                                              ),
      .dnc0_ctrl_Ar_Len(                                                   dnc0_ctrl_Ar_Len                                                ),
      .dnc0_ctrl_Ar_Valid(                                                 dnc0_ctrl_Ar_Valid                                              ),
      .dnc0_ctrl_Ar_Ready(                                                 dnc_cbus_s0_arready                                             ),
      .dnc0_ctrl_Ar_Id(                                                    dnc0_ctrl_Ar_Id                                                 ),
      .dnc0_ctrl_Ar_Size(                                                  dnc0_ctrl_Ar_Size                                               ),
      .dcluster0_ctrl_R_Last(                                              cbus_s0_rlast                                                   ),
      .dcluster0_ctrl_R_Resp(                                              cbus_s0_rresp                                                   ),
      .dcluster0_ctrl_R_Valid(                                             cbus_s0_rvalid                                                  ),
      .dcluster0_ctrl_R_Ready(                                             dcluster0_ctrl_R_Ready                                          ),
      .dcluster0_ctrl_R_Data(                                              cbus_s0_rdata                                                   ),
      .dcluster0_ctrl_R_Id(                                                cbus_s0_rid                                                     ),
      .dcluster0_ctrl_B_Ready(                                             dcluster0_ctrl_B_Ready                                          ),
      .dcluster0_ctrl_B_Resp(                                              cbus_s0_bresp                                                   ),
      .dcluster0_ctrl_B_Valid(                                             cbus_s0_bvalid                                                  ),
      .dcluster0_ctrl_B_Id(                                                cbus_s0_bid                                                     ),
      .dcluster0_ctrl_Aw_Prot(                                             dcluster0_ctrl_Aw_Prot                                          ),
      .dcluster0_ctrl_Aw_Addr(                                             dcluster0_ctrl_Aw_Addr                                          ),
      .dcluster0_ctrl_Aw_Burst(                                            dcluster0_ctrl_Aw_Burst                                         ),
      .dcluster0_ctrl_Aw_Lock(                                             dcluster0_ctrl_Aw_Lock                                          ),
      .dcluster0_ctrl_Aw_Cache(                                            dcluster0_ctrl_Aw_Cache                                         ),
      .dcluster0_ctrl_Aw_Len(                                              dcluster0_ctrl_Aw_Len                                           ),
      .dcluster0_ctrl_Aw_Valid(                                            dcluster0_ctrl_Aw_Valid                                         ),
      .dcluster0_ctrl_Aw_Ready(                                            cbus_s0_awready                                                 ),
      .dcluster0_ctrl_Aw_Id(                                               dcluster0_ctrl_Aw_Id                                            ),
      .dcluster0_ctrl_Aw_Size(                                             dcluster0_ctrl_Aw_Size                                          ),
      .dcluster0_ctrl_W_Last(                                              dcluster0_ctrl_W_Last                                           ),
      .dcluster0_ctrl_W_Valid(                                             dcluster0_ctrl_W_Valid                                          ),
      .dcluster0_ctrl_W_Ready(                                             cbus_s0_wready                                                  ),
      .dcluster0_ctrl_W_Strb(                                              dcluster0_ctrl_W_Strb                                           ),
      .dcluster0_ctrl_W_Data(                                              dcluster0_ctrl_W_Data                                           ),
      .dcluster0_ctrl_Ar_Prot(                                             dcluster0_ctrl_Ar_Prot                                          ),
      .dcluster0_ctrl_Ar_Addr(                                             dcluster0_ctrl_Ar_Addr                                          ),
      .dcluster0_ctrl_Ar_Burst(                                            dcluster0_ctrl_Ar_Burst                                         ),
      .dcluster0_ctrl_Ar_Lock(                                             dcluster0_ctrl_Ar_Lock                                          ),
      .dcluster0_ctrl_Ar_Cache(                                            dcluster0_ctrl_Ar_Cache                                         ),
      .dcluster0_ctrl_Ar_Len(                                              dcluster0_ctrl_Ar_Len                                           ),
      .dcluster0_ctrl_Ar_Valid(                                            dcluster0_ctrl_Ar_Valid                                         ),
      .dcluster0_ctrl_Ar_Ready(                                            cbus_s0_arready                                                 ),
      .dcluster0_ctrl_Ar_Id(                                               dcluster0_ctrl_Ar_Id                                            ),
      .dcluster0_ctrl_Ar_Size(                                             dcluster0_ctrl_Ar_Size                                          ),
      .clk_dcluster0(                                                      aclk                                                            ),
      .arstn_dcluster0(                                                    arstn                                                           ),
      .TM(                                                                 TM                                                              ) 
      );


dbus_read_Structure_Module_dcluster0 u_dbus_read_Structure_Module_dcluster0 (
      .dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdCnt(                dp_Link_n3_astResp001_to_Link_n3_asiResp001_rdcnt             ),
      .dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n3_astResp001_to_Link_n3_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n3_astResp001_to_Link_n3_asiResp001_RxCtl_PwrOnRst(       dp_Link_n3_astResp001_to_Link_n3_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n3_astResp001_to_Link_n3_asiResp001_RdPtr(                dp_Link_n3_astResp001_to_Link_n3_asiResp001_rdptr             ),
      .dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n3_astResp001_to_Link_n3_asiResp001_txctl_pwronrstack ),
      .dp_Link_n3_astResp001_to_Link_n3_asiResp001_TxCtl_PwrOnRst(       dp_Link_n3_astResp001_to_Link_n3_asiResp001_txctl_pwronrst    ),
      .dp_Link_n3_astResp001_to_Link_n3_asiResp001_Data(                 dp_Link_n3_astResp001_to_Link_n3_asiResp001_data              ),
      .dp_Link_n3_astResp001_to_Link_n3_asiResp001_WrCnt(                dp_Link_n3_astResp001_to_Link_n3_asiResp001_wrcnt             ),
      .dp_Link_n3_asi_to_Link_n3_ast_RdCnt(                              dp_Link_n3_asi_to_Link_n3_ast_r_rdcnt                         ),
      .dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck(                  dp_Link_n3_asi_to_Link_n3_ast_r_rxctl_pwronrstack             ),
      .dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst(                     dp_Link_n3_asi_to_Link_n3_ast_r_rxctl_pwronrst                ),
      .dp_Link_n3_asi_to_Link_n3_ast_RdPtr(                              dp_Link_n3_asi_to_Link_n3_ast_r_rdptr                         ),
      .dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck(                  dp_Link_n3_asi_to_Link_n3_ast_r_txctl_pwronrstack             ),
      .dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst(                     dp_Link_n3_asi_to_Link_n3_ast_r_txctl_pwronrst                ),
      .dp_Link_n3_asi_to_Link_n3_ast_Data(                               dp_Link_n3_asi_to_Link_n3_ast_r_data                          ),
      .dp_Link_n3_asi_to_Link_n3_ast_WrCnt(                              dp_Link_n3_asi_to_Link_n3_ast_r_wrcnt                         ),
      .dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdCnt(                dp_Link_n2_astResp001_to_Link_n2_asiResp001_rdcnt             ),
      .dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n2_astResp001_to_Link_n2_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n2_astResp001_to_Link_n2_asiResp001_RxCtl_PwrOnRst(       dp_Link_n2_astResp001_to_Link_n2_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n2_astResp001_to_Link_n2_asiResp001_RdPtr(                dp_Link_n2_astResp001_to_Link_n2_asiResp001_rdptr             ),
      .dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n2_astResp001_to_Link_n2_asiResp001_txctl_pwronrstack ),
      .dp_Link_n2_astResp001_to_Link_n2_asiResp001_TxCtl_PwrOnRst(       dp_Link_n2_astResp001_to_Link_n2_asiResp001_txctl_pwronrst    ),
      .dp_Link_n2_astResp001_to_Link_n2_asiResp001_Data(                 dp_Link_n2_astResp001_to_Link_n2_asiResp001_data              ),
      .dp_Link_n2_astResp001_to_Link_n2_asiResp001_WrCnt(                dp_Link_n2_astResp001_to_Link_n2_asiResp001_wrcnt             ),
      .dp_Link_n2_asi_to_Link_n2_ast_RdCnt(                              dp_Link_n2_asi_to_Link_n2_ast_r_rdcnt                         ),
      .dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck(                  dp_Link_n2_asi_to_Link_n2_ast_r_rxctl_pwronrstack             ),
      .dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst(                     dp_Link_n2_asi_to_Link_n2_ast_r_rxctl_pwronrst                ),
      .dp_Link_n2_asi_to_Link_n2_ast_RdPtr(                              dp_Link_n2_asi_to_Link_n2_ast_r_rdptr                         ),
      .dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck(                  dp_Link_n2_asi_to_Link_n2_ast_r_txctl_pwronrstack             ),
      .dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst(                     dp_Link_n2_asi_to_Link_n2_ast_r_txctl_pwronrst                ),
      .dp_Link_n2_asi_to_Link_n2_ast_Data(                               dp_Link_n2_asi_to_Link_n2_ast_r_data                          ),
      .dp_Link_n2_asi_to_Link_n2_ast_WrCnt(                              dp_Link_n2_asi_to_Link_n2_ast_r_wrcnt                         ),
      .dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdCnt(                dp_Link_n1_astResp001_to_Link_n1_asiResp001_rdcnt             ),
      .dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n1_astResp001_to_Link_n1_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n1_astResp001_to_Link_n1_asiResp001_RxCtl_PwrOnRst(       dp_Link_n1_astResp001_to_Link_n1_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n1_astResp001_to_Link_n1_asiResp001_RdPtr(                dp_Link_n1_astResp001_to_Link_n1_asiResp001_rdptr             ),
      .dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n1_astResp001_to_Link_n1_asiResp001_txctl_pwronrstack ),
      .dp_Link_n1_astResp001_to_Link_n1_asiResp001_TxCtl_PwrOnRst(       dp_Link_n1_astResp001_to_Link_n1_asiResp001_txctl_pwronrst    ),
      .dp_Link_n1_astResp001_to_Link_n1_asiResp001_Data(                 dp_Link_n1_astResp001_to_Link_n1_asiResp001_data              ),
      .dp_Link_n1_astResp001_to_Link_n1_asiResp001_WrCnt(                dp_Link_n1_astResp001_to_Link_n1_asiResp001_wrcnt             ),
      .dp_Link_n1_asi_to_Link_n1_ast_RdCnt(                              dp_Link_n1_asi_to_Link_n1_ast_r_rdcnt                         ),
      .dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck(                  dp_Link_n1_asi_to_Link_n1_ast_r_rxctl_pwronrstack             ),
      .dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst(                     dp_Link_n1_asi_to_Link_n1_ast_r_rxctl_pwronrst                ),
      .dp_Link_n1_asi_to_Link_n1_ast_RdPtr(                              dp_Link_n1_asi_to_Link_n1_ast_r_rdptr                         ),
      .dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck(                  dp_Link_n1_asi_to_Link_n1_ast_r_txctl_pwronrstack             ),
      .dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst(                     dp_Link_n1_asi_to_Link_n1_ast_r_txctl_pwronrst                ),
      .dp_Link_n1_asi_to_Link_n1_ast_Data(                               dp_Link_n1_asi_to_Link_n1_ast_r_data                          ),
      .dp_Link_n1_asi_to_Link_n1_ast_WrCnt(                              dp_Link_n1_asi_to_Link_n1_ast_r_wrcnt                         ),
      .dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdCnt(                dp_Link_n0_astResp001_to_Link_n0_asiResp001_rdcnt             ),
      .dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRstAck(    dp_Link_n0_astResp001_to_Link_n0_asiResp001_rxctl_pwronrstack ),
      .dp_Link_n0_astResp001_to_Link_n0_asiResp001_RxCtl_PwrOnRst(       dp_Link_n0_astResp001_to_Link_n0_asiResp001_rxctl_pwronrst    ),
      .dp_Link_n0_astResp001_to_Link_n0_asiResp001_RdPtr(                dp_Link_n0_astResp001_to_Link_n0_asiResp001_rdptr             ),
      .dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRstAck(    dp_Link_n0_astResp001_to_Link_n0_asiResp001_txctl_pwronrstack ),
      .dp_Link_n0_astResp001_to_Link_n0_asiResp001_TxCtl_PwrOnRst(       dp_Link_n0_astResp001_to_Link_n0_asiResp001_txctl_pwronrst    ),
      .dp_Link_n0_astResp001_to_Link_n0_asiResp001_Data(                 dp_Link_n0_astResp001_to_Link_n0_asiResp001_data              ),
      .dp_Link_n0_astResp001_to_Link_n0_asiResp001_WrCnt(                dp_Link_n0_astResp001_to_Link_n0_asiResp001_wrcnt             ),
      .dp_Link_n0_asi_to_Link_n0_ast_RdCnt(                              dp_Link_n0_asi_to_Link_n0_ast_r_rdcnt                         ),
      .dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck(                  dp_Link_n0_asi_to_Link_n0_ast_r_rxctl_pwronrstack             ),
      .dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst(                     dp_Link_n0_asi_to_Link_n0_ast_r_rxctl_pwronrst                ),
      .dp_Link_n0_asi_to_Link_n0_ast_RdPtr(                              dp_Link_n0_asi_to_Link_n0_ast_r_rdptr                         ),
      .dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck(                  dp_Link_n0_asi_to_Link_n0_ast_r_txctl_pwronrstack             ),
      .dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst(                     dp_Link_n0_asi_to_Link_n0_ast_r_txctl_pwronrst                ),
      .dp_Link_n0_asi_to_Link_n0_ast_Data(                               dp_Link_n0_asi_to_Link_n0_ast_r_data                          ),
      .dp_Link_n0_asi_to_Link_n0_ast_WrCnt(                              dp_Link_n0_asi_to_Link_n0_ast_r_wrcnt                         ),
      .dnc3_slv_r_R_Last(                                                dnc_dbus_s0_rlast_2                                           ),
      .dnc3_slv_r_R_Resp(                                                dnc_dbus_s0_rresp_2                                           ),
      .dnc3_slv_r_R_Valid(                                               dnc_dbus_s0_rvalid_2                                          ),
      .dnc3_slv_r_R_Ready(                                               dnc3_slv_r_R_Ready                                            ),
      .dnc3_slv_r_R_Data(                                                dnc_dbus_s0_rdata_2                                           ),
      .dnc3_slv_r_R_Id(                                                  dnc_dbus_s0_rid_2                                             ),
      .dnc3_slv_r_Ar_Prot(                                               dnc3_slv_r_Ar_Prot                                            ),
      .dnc3_slv_r_Ar_Addr(                                               dnc3_slv_r_Ar_Addr                                            ),
      .dnc3_slv_r_Ar_Burst(                                              dnc3_slv_r_Ar_Burst                                           ),
      .dnc3_slv_r_Ar_Lock(                                               dnc3_slv_r_Ar_Lock                                            ),
      .dnc3_slv_r_Ar_Cache(                                              dnc3_slv_r_Ar_Cache                                           ),
      .dnc3_slv_r_Ar_Len(                                                dnc3_slv_r_Ar_Len                                             ),
      .dnc3_slv_r_Ar_Valid(                                              dnc3_slv_r_Ar_Valid                                           ),
      .dnc3_slv_r_Ar_Ready(                                              dnc_dbus_s0_arready_2                                         ),
      .dnc3_slv_r_Ar_Id(                                                 dnc3_slv_r_Ar_Id                                              ),
      .dnc3_slv_r_Ar_Size(                                               dnc3_slv_r_Ar_Size                                            ),
      .dnc3_r_R_Last(                                                    dnc3_r_R_Last                                                 ),
      .dnc3_r_R_Resp(                                                    dnc3_r_R_Resp                                                 ),
      .dnc3_r_R_Valid(                                                   dnc3_r_R_Valid                                                ),
      .dnc3_r_R_User(                                                    dnc3_r_R_User                                                 ),
      .dnc3_r_R_Ready(                                                   dnc_dbus_m0_rready_2                                          ),
      .dnc3_r_R_Data(                                                    dnc3_r_R_Data                                                 ),
      .dnc3_r_R_Id(                                                      dnc3_r_R_Id                                                   ),
      .dnc3_r_Ar_Prot(                                                   dnc_dbus_m0_arprot_2                                          ),
      .dnc3_r_Ar_Addr(                                                 { 18'b000000000000000000,
                                                                         dnc_dbus_m0_araddr_2 }                                        ),
      .dnc3_r_Ar_Burst(                                                  dnc_dbus_m0_arburst_2                                         ),
      .dnc3_r_Ar_Lock(                                                   dnc_dbus_m0_arlock_2                                          ),
      .dnc3_r_Ar_Cache(                                                  dnc_dbus_m0_arcache_2                                         ),
      .dnc3_r_Ar_Len(                                                  { 2'b00,
                                                                         dnc_dbus_m0_arlen_2 }                                         ),
      .dnc3_r_Ar_Valid(                                                  dnc_dbus_m0_arvalid_2                                         ),
      .dnc3_r_Ar_User(                                                   dnc_dbus_m0_aruser_2                                          ),
      .dnc3_r_Ar_Ready(                                                  dnc3_r_Ar_Ready                                               ),
      .dnc3_r_Ar_Id(                                                     dnc_dbus_m0_arid_2                                            ),
      .dnc3_r_Ar_Size(                                                   dnc_dbus_m0_arsize_2                                          ),
      .dnc2_slv_r_R_Last(                                                dnc_dbus_s0_rlast_1                                           ),
      .dnc2_slv_r_R_Resp(                                                dnc_dbus_s0_rresp_1                                           ),
      .dnc2_slv_r_R_Valid(                                               dnc_dbus_s0_rvalid_1                                          ),
      .dnc2_slv_r_R_Ready(                                               dnc2_slv_r_R_Ready                                            ),
      .dnc2_slv_r_R_Data(                                                dnc_dbus_s0_rdata_1                                           ),
      .dnc2_slv_r_R_Id(                                                  dnc_dbus_s0_rid_1                                             ),
      .dnc2_slv_r_Ar_Prot(                                               dnc2_slv_r_Ar_Prot                                            ),
      .dnc2_slv_r_Ar_Addr(                                               dnc2_slv_r_Ar_Addr                                            ),
      .dnc2_slv_r_Ar_Burst(                                              dnc2_slv_r_Ar_Burst                                           ),
      .dnc2_slv_r_Ar_Lock(                                               dnc2_slv_r_Ar_Lock                                            ),
      .dnc2_slv_r_Ar_Cache(                                              dnc2_slv_r_Ar_Cache                                           ),
      .dnc2_slv_r_Ar_Len(                                                dnc2_slv_r_Ar_Len                                             ),
      .dnc2_slv_r_Ar_Valid(                                              dnc2_slv_r_Ar_Valid                                           ),
      .dnc2_slv_r_Ar_Ready(                                              dnc_dbus_s0_arready_1                                         ),
      .dnc2_slv_r_Ar_Id(                                                 dnc2_slv_r_Ar_Id                                              ),
      .dnc2_slv_r_Ar_Size(                                               dnc2_slv_r_Ar_Size                                            ),
      .dnc2_r_R_Last(                                                    dnc2_r_R_Last                                                 ),
      .dnc2_r_R_Resp(                                                    dnc2_r_R_Resp                                                 ),
      .dnc2_r_R_Valid(                                                   dnc2_r_R_Valid                                                ),
      .dnc2_r_R_User(                                                    dnc2_r_R_User                                                 ),
      .dnc2_r_R_Ready(                                                   dnc_dbus_m0_rready_1                                          ),
      .dnc2_r_R_Data(                                                    dnc2_r_R_Data                                                 ),
      .dnc2_r_R_Id(                                                      dnc2_r_R_Id                                                   ),
      .dnc2_r_Ar_Prot(                                                   dnc_dbus_m0_arprot_1                                          ),
      .dnc2_r_Ar_Addr(                                                 { 18'b000000000000000000,
                                                                         dnc_dbus_m0_araddr_1 }                                        ),
      .dnc2_r_Ar_Burst(                                                  dnc_dbus_m0_arburst_1                                         ),
      .dnc2_r_Ar_Lock(                                                   dnc_dbus_m0_arlock_1                                          ),
      .dnc2_r_Ar_Cache(                                                  dnc_dbus_m0_arcache_1                                         ),
      .dnc2_r_Ar_Len(                                                  { 2'b00,
                                                                         dnc_dbus_m0_arlen_1 }                                         ),
      .dnc2_r_Ar_Valid(                                                  dnc_dbus_m0_arvalid_1                                         ),
      .dnc2_r_Ar_User(                                                   dnc_dbus_m0_aruser_1                                          ),
      .dnc2_r_Ar_Ready(                                                  dnc2_r_Ar_Ready                                               ),
      .dnc2_r_Ar_Id(                                                     dnc_dbus_m0_arid_1                                            ),
      .dnc2_r_Ar_Size(                                                   dnc_dbus_m0_arsize_1                                          ),
      .dnc1_slv_r_R_Last(                                                dnc_dbus_s0_rlast_0                                           ),
      .dnc1_slv_r_R_Resp(                                                dnc_dbus_s0_rresp_0                                           ),
      .dnc1_slv_r_R_Valid(                                               dnc_dbus_s0_rvalid_0                                          ),
      .dnc1_slv_r_R_Ready(                                               dnc1_slv_r_R_Ready                                            ),
      .dnc1_slv_r_R_Data(                                                dnc_dbus_s0_rdata_0                                           ),
      .dnc1_slv_r_R_Id(                                                  dnc_dbus_s0_rid_0                                             ),
      .dnc1_slv_r_Ar_Prot(                                               dnc1_slv_r_Ar_Prot                                            ),
      .dnc1_slv_r_Ar_Addr(                                               dnc1_slv_r_Ar_Addr                                            ),
      .dnc1_slv_r_Ar_Burst(                                              dnc1_slv_r_Ar_Burst                                           ),
      .dnc1_slv_r_Ar_Lock(                                               dnc1_slv_r_Ar_Lock                                            ),
      .dnc1_slv_r_Ar_Cache(                                              dnc1_slv_r_Ar_Cache                                           ),
      .dnc1_slv_r_Ar_Len(                                                dnc1_slv_r_Ar_Len                                             ),
      .dnc1_slv_r_Ar_Valid(                                              dnc1_slv_r_Ar_Valid                                           ),
      .dnc1_slv_r_Ar_Ready(                                              dnc_dbus_s0_arready_0                                         ),
      .dnc1_slv_r_Ar_Id(                                                 dnc1_slv_r_Ar_Id                                              ),
      .dnc1_slv_r_Ar_Size(                                               dnc1_slv_r_Ar_Size                                            ),
      .dnc1_r_R_Last(                                                    dnc1_r_R_Last                                                 ),
      .dnc1_r_R_Resp(                                                    dnc1_r_R_Resp                                                 ),
      .dnc1_r_R_Valid(                                                   dnc1_r_R_Valid                                                ),
      .dnc1_r_R_User(                                                    dnc1_r_R_User                                                 ),
      .dnc1_r_R_Ready(                                                   dnc_dbus_m0_rready_0                                          ),
      .dnc1_r_R_Data(                                                    dnc1_r_R_Data                                                 ),
      .dnc1_r_R_Id(                                                      dnc1_r_R_Id                                                   ),
      .dnc1_r_Ar_Prot(                                                   dnc_dbus_m0_arprot_0                                          ),
      .dnc1_r_Ar_Addr(                                                 { 18'b000000000000000000,
                                                                         dnc_dbus_m0_araddr_0 }                                        ),
      .dnc1_r_Ar_Burst(                                                  dnc_dbus_m0_arburst_0                                         ),
      .dnc1_r_Ar_Lock(                                                   dnc_dbus_m0_arlock_0                                          ),
      .dnc1_r_Ar_Cache(                                                  dnc_dbus_m0_arcache_0                                         ),
      .dnc1_r_Ar_Len(                                                  { 2'b00,
                                                                         dnc_dbus_m0_arlen_0 }                                         ),
      .dnc1_r_Ar_Valid(                                                  dnc_dbus_m0_arvalid_0                                         ),
      .dnc1_r_Ar_User(                                                   dnc_dbus_m0_aruser_0                                          ),
      .dnc1_r_Ar_Ready(                                                  dnc1_r_Ar_Ready                                               ),
      .dnc1_r_Ar_Id(                                                     dnc_dbus_m0_arid_0                                            ),
      .dnc1_r_Ar_Size(                                                   dnc_dbus_m0_arsize_0                                          ),
      .dnc0_slv_r_R_Last(                                                dnc_dbus_s0_rlast                                             ),
      .dnc0_slv_r_R_Resp(                                                dnc_dbus_s0_rresp                                             ),
      .dnc0_slv_r_R_Valid(                                               dnc_dbus_s0_rvalid                                            ),
      .dnc0_slv_r_R_Ready(                                               dnc0_slv_r_R_Ready                                            ),
      .dnc0_slv_r_R_Data(                                                dnc_dbus_s0_rdata                                             ),
      .dnc0_slv_r_R_Id(                                                  dnc_dbus_s0_rid                                               ),
      .dnc0_slv_r_Ar_Prot(                                               dnc0_slv_r_Ar_Prot                                            ),
      .dnc0_slv_r_Ar_Addr(                                               dnc0_slv_r_Ar_Addr                                            ),
      .dnc0_slv_r_Ar_Burst(                                              dnc0_slv_r_Ar_Burst                                           ),
      .dnc0_slv_r_Ar_Lock(                                               dnc0_slv_r_Ar_Lock                                            ),
      .dnc0_slv_r_Ar_Cache(                                              dnc0_slv_r_Ar_Cache                                           ),
      .dnc0_slv_r_Ar_Len(                                                dnc0_slv_r_Ar_Len                                             ),
      .dnc0_slv_r_Ar_Valid(                                              dnc0_slv_r_Ar_Valid                                           ),
      .dnc0_slv_r_Ar_Ready(                                              dnc_dbus_s0_arready                                           ),
      .dnc0_slv_r_Ar_Id(                                                 dnc0_slv_r_Ar_Id                                              ),
      .dnc0_slv_r_Ar_Size(                                               dnc0_slv_r_Ar_Size                                            ),
      .dnc0_r_R_Last(                                                    dnc0_r_R_Last                                                 ),
      .dnc0_r_R_Resp(                                                    dnc0_r_R_Resp                                                 ),
      .dnc0_r_R_Valid(                                                   dnc0_r_R_Valid                                                ),
      .dnc0_r_R_User(                                                    dnc0_r_R_User                                                 ),
      .dnc0_r_R_Ready(                                                   dnc_dbus_m0_rready                                            ),
      .dnc0_r_R_Data(                                                    dnc0_r_R_Data                                                 ),
      .dnc0_r_R_Id(                                                      dnc0_r_R_Id                                                   ),
      .dnc0_r_Ar_Prot(                                                   dnc_dbus_m0_arprot                                            ),
      .dnc0_r_Ar_Addr(                                                 { 18'b000000000000000000,
                                                                         dnc_dbus_m0_araddr }                                          ),
      .dnc0_r_Ar_Burst(                                                  dnc_dbus_m0_arburst                                           ),
      .dnc0_r_Ar_Lock(                                                   dnc_dbus_m0_arlock                                            ),
      .dnc0_r_Ar_Cache(                                                  dnc_dbus_m0_arcache                                           ),
      .dnc0_r_Ar_Len(                                                  { 2'b00,
                                                                         dnc_dbus_m0_arlen }                                           ),
      .dnc0_r_Ar_Valid(                                                  dnc_dbus_m0_arvalid                                           ),
      .dnc0_r_Ar_User(                                                   dnc_dbus_m0_aruser                                            ),
      .dnc0_r_Ar_Ready(                                                  dnc0_r_Ar_Ready                                               ),
      .dnc0_r_Ar_Id(                                                     dnc_dbus_m0_arid                                              ),
      .dnc0_r_Ar_Size(                                                   dnc_dbus_m0_arsize                                            ),
      .dcluster0_cc3_slv_r_R_Last(                                       dbus_s3_rlast                                                 ),
      .dcluster0_cc3_slv_r_R_Resp(                                       dbus_s3_rresp                                                 ),
      .dcluster0_cc3_slv_r_R_Valid(                                      dbus_s3_rvalid                                                ),
      .dcluster0_cc3_slv_r_R_User(                                       dbus_s3_ruser                                                 ),
      .dcluster0_cc3_slv_r_R_Ready(                                      dcluster0_cc3_slv_r_R_Ready                                   ),
      .dcluster0_cc3_slv_r_R_Data(                                       dbus_s3_rdata                                                 ),
      .dcluster0_cc3_slv_r_R_Id(                                         dbus_s3_rid                                                   ),
      .dcluster0_cc3_slv_r_Ar_Prot(                                      dcluster0_cc3_slv_r_Ar_Prot                                   ),
      .dcluster0_cc3_slv_r_Ar_Addr(                                      dcluster0_cc3_slv_r_Ar_Addr                                   ),
      .dcluster0_cc3_slv_r_Ar_Burst(                                     dcluster0_cc3_slv_r_Ar_Burst                                  ),
      .dcluster0_cc3_slv_r_Ar_Lock(                                      dcluster0_cc3_slv_r_Ar_Lock                                   ),
      .dcluster0_cc3_slv_r_Ar_Cache(                                     dcluster0_cc3_slv_r_Ar_Cache                                  ),
      .dcluster0_cc3_slv_r_Ar_Len(                                       dcluster0_cc3_slv_r_Ar_Len                                    ),
      .dcluster0_cc3_slv_r_Ar_Valid(                                     dcluster0_cc3_slv_r_Ar_Valid                                  ),
      .dcluster0_cc3_slv_r_Ar_User(                                      dcluster0_cc3_slv_r_Ar_User                                   ),
      .dcluster0_cc3_slv_r_Ar_Ready(                                     dbus_s3_arready                                               ),
      .dcluster0_cc3_slv_r_Ar_Id(                                        dcluster0_cc3_slv_r_Ar_Id                                     ),
      .dcluster0_cc3_slv_r_Ar_Size(                                      dcluster0_cc3_slv_r_Ar_Size                                   ),
      .dcluster0_cc3_r_R_Last(                                           dcluster0_cc3_r_R_Last                                        ),
      .dcluster0_cc3_r_R_Resp(                                           dcluster0_cc3_r_R_Resp                                        ),
      .dcluster0_cc3_r_R_Valid(                                          dcluster0_cc3_r_R_Valid                                       ),
      .dcluster0_cc3_r_R_User(                                           dcluster0_cc3_r_R_User                                        ),
      .dcluster0_cc3_r_R_Ready(                                          dbus_m3_rready                                                ),
      .dcluster0_cc3_r_R_Data(                                           dcluster0_cc3_r_R_Data                                        ),
      .dcluster0_cc3_r_R_Id(                                             dcluster0_cc3_r_R_Id                                          ),
      .dcluster0_cc3_r_Ar_Prot(                                          dbus_m3_arprot                                                ),
      .dcluster0_cc3_r_Ar_Addr(                                          dbus_m3_araddr                                                ),
      .dcluster0_cc3_r_Ar_Burst(                                         dbus_m3_arburst                                               ),
      .dcluster0_cc3_r_Ar_Lock(                                          dbus_m3_arlock                                                ),
      .dcluster0_cc3_r_Ar_Cache(                                         dbus_m3_arcache                                               ),
      .dcluster0_cc3_r_Ar_Len(                                           dbus_m3_arlen                                                 ),
      .dcluster0_cc3_r_Ar_Valid(                                         dbus_m3_arvalid                                               ),
      .dcluster0_cc3_r_Ar_User(                                          4'b0000                                                       ),
      .dcluster0_cc3_r_Ar_Ready(                                         dcluster0_cc3_r_Ar_Ready                                      ),
      .dcluster0_cc3_r_Ar_Id(                                            dbus_m3_arid                                                  ),
      .dcluster0_cc3_r_Ar_Size(                                          dbus_m3_arsize                                                ),
      .dcluster0_cc2_slv_r_R_Last(                                       dbus_s2_rlast                                                 ),
      .dcluster0_cc2_slv_r_R_Resp(                                       dbus_s2_rresp                                                 ),
      .dcluster0_cc2_slv_r_R_Valid(                                      dbus_s2_rvalid                                                ),
      .dcluster0_cc2_slv_r_R_User(                                       dbus_s2_ruser                                                 ),
      .dcluster0_cc2_slv_r_R_Ready(                                      dcluster0_cc2_slv_r_R_Ready                                   ),
      .dcluster0_cc2_slv_r_R_Data(                                       dbus_s2_rdata                                                 ),
      .dcluster0_cc2_slv_r_R_Id(                                         dbus_s2_rid                                                   ),
      .dcluster0_cc2_slv_r_Ar_Prot(                                      dcluster0_cc2_slv_r_Ar_Prot                                   ),
      .dcluster0_cc2_slv_r_Ar_Addr(                                      dcluster0_cc2_slv_r_Ar_Addr                                   ),
      .dcluster0_cc2_slv_r_Ar_Burst(                                     dcluster0_cc2_slv_r_Ar_Burst                                  ),
      .dcluster0_cc2_slv_r_Ar_Lock(                                      dcluster0_cc2_slv_r_Ar_Lock                                   ),
      .dcluster0_cc2_slv_r_Ar_Cache(                                     dcluster0_cc2_slv_r_Ar_Cache                                  ),
      .dcluster0_cc2_slv_r_Ar_Len(                                       dcluster0_cc2_slv_r_Ar_Len                                    ),
      .dcluster0_cc2_slv_r_Ar_Valid(                                     dcluster0_cc2_slv_r_Ar_Valid                                  ),
      .dcluster0_cc2_slv_r_Ar_User(                                      dcluster0_cc2_slv_r_Ar_User                                   ),
      .dcluster0_cc2_slv_r_Ar_Ready(                                     dbus_s2_arready                                               ),
      .dcluster0_cc2_slv_r_Ar_Id(                                        dcluster0_cc2_slv_r_Ar_Id                                     ),
      .dcluster0_cc2_slv_r_Ar_Size(                                      dcluster0_cc2_slv_r_Ar_Size                                   ),
      .dcluster0_cc2_r_R_Last(                                           dcluster0_cc2_r_R_Last                                        ),
      .dcluster0_cc2_r_R_Resp(                                           dcluster0_cc2_r_R_Resp                                        ),
      .dcluster0_cc2_r_R_Valid(                                          dcluster0_cc2_r_R_Valid                                       ),
      .dcluster0_cc2_r_R_User(                                           dcluster0_cc2_r_R_User                                        ),
      .dcluster0_cc2_r_R_Ready(                                          dbus_m2_rready                                                ),
      .dcluster0_cc2_r_R_Data(                                           dcluster0_cc2_r_R_Data                                        ),
      .dcluster0_cc2_r_R_Id(                                             dcluster0_cc2_r_R_Id                                          ),
      .dcluster0_cc2_r_Ar_Prot(                                          dbus_m2_arprot                                                ),
      .dcluster0_cc2_r_Ar_Addr(                                          dbus_m2_araddr                                                ),
      .dcluster0_cc2_r_Ar_Burst(                                         dbus_m2_arburst                                               ),
      .dcluster0_cc2_r_Ar_Lock(                                          dbus_m2_arlock                                                ),
      .dcluster0_cc2_r_Ar_Cache(                                         dbus_m2_arcache                                               ),
      .dcluster0_cc2_r_Ar_Len(                                           dbus_m2_arlen                                                 ),
      .dcluster0_cc2_r_Ar_Valid(                                         dbus_m2_arvalid                                               ),
      .dcluster0_cc2_r_Ar_User(                                          4'b0000                                                       ),
      .dcluster0_cc2_r_Ar_Ready(                                         dcluster0_cc2_r_Ar_Ready                                      ),
      .dcluster0_cc2_r_Ar_Id(                                            dbus_m2_arid                                                  ),
      .dcluster0_cc2_r_Ar_Size(                                          dbus_m2_arsize                                                ),
      .dcluster0_cc1_slv_r_R_Last(                                       dbus_s1_rlast                                                 ),
      .dcluster0_cc1_slv_r_R_Resp(                                       dbus_s1_rresp                                                 ),
      .dcluster0_cc1_slv_r_R_Valid(                                      dbus_s1_rvalid                                                ),
      .dcluster0_cc1_slv_r_R_User(                                       dbus_s1_ruser                                                 ),
      .dcluster0_cc1_slv_r_R_Ready(                                      dcluster0_cc1_slv_r_R_Ready                                   ),
      .dcluster0_cc1_slv_r_R_Data(                                       dbus_s1_rdata                                                 ),
      .dcluster0_cc1_slv_r_R_Id(                                         dbus_s1_rid                                                   ),
      .dcluster0_cc1_slv_r_Ar_Prot(                                      dcluster0_cc1_slv_r_Ar_Prot                                   ),
      .dcluster0_cc1_slv_r_Ar_Addr(                                      dcluster0_cc1_slv_r_Ar_Addr                                   ),
      .dcluster0_cc1_slv_r_Ar_Burst(                                     dcluster0_cc1_slv_r_Ar_Burst                                  ),
      .dcluster0_cc1_slv_r_Ar_Lock(                                      dcluster0_cc1_slv_r_Ar_Lock                                   ),
      .dcluster0_cc1_slv_r_Ar_Cache(                                     dcluster0_cc1_slv_r_Ar_Cache                                  ),
      .dcluster0_cc1_slv_r_Ar_Len(                                       dcluster0_cc1_slv_r_Ar_Len                                    ),
      .dcluster0_cc1_slv_r_Ar_Valid(                                     dcluster0_cc1_slv_r_Ar_Valid                                  ),
      .dcluster0_cc1_slv_r_Ar_User(                                      dcluster0_cc1_slv_r_Ar_User                                   ),
      .dcluster0_cc1_slv_r_Ar_Ready(                                     dbus_s1_arready                                               ),
      .dcluster0_cc1_slv_r_Ar_Id(                                        dcluster0_cc1_slv_r_Ar_Id                                     ),
      .dcluster0_cc1_slv_r_Ar_Size(                                      dcluster0_cc1_slv_r_Ar_Size                                   ),
      .dcluster0_cc1_r_R_Last(                                           dcluster0_cc1_r_R_Last                                        ),
      .dcluster0_cc1_r_R_Resp(                                           dcluster0_cc1_r_R_Resp                                        ),
      .dcluster0_cc1_r_R_Valid(                                          dcluster0_cc1_r_R_Valid                                       ),
      .dcluster0_cc1_r_R_User(                                           dcluster0_cc1_r_R_User                                        ),
      .dcluster0_cc1_r_R_Ready(                                          dbus_m1_rready                                                ),
      .dcluster0_cc1_r_R_Data(                                           dcluster0_cc1_r_R_Data                                        ),
      .dcluster0_cc1_r_R_Id(                                             dcluster0_cc1_r_R_Id                                          ),
      .dcluster0_cc1_r_Ar_Prot(                                          dbus_m1_arprot                                                ),
      .dcluster0_cc1_r_Ar_Addr(                                          dbus_m1_araddr                                                ),
      .dcluster0_cc1_r_Ar_Burst(                                         dbus_m1_arburst                                               ),
      .dcluster0_cc1_r_Ar_Lock(                                          dbus_m1_arlock                                                ),
      .dcluster0_cc1_r_Ar_Cache(                                         dbus_m1_arcache                                               ),
      .dcluster0_cc1_r_Ar_Len(                                           dbus_m1_arlen                                                 ),
      .dcluster0_cc1_r_Ar_Valid(                                         dbus_m1_arvalid                                               ),
      .dcluster0_cc1_r_Ar_User(                                          4'b0000                                                       ),
      .dcluster0_cc1_r_Ar_Ready(                                         dcluster0_cc1_r_Ar_Ready                                      ),
      .dcluster0_cc1_r_Ar_Id(                                            dbus_m1_arid                                                  ),
      .dcluster0_cc1_r_Ar_Size(                                          dbus_m1_arsize                                                ),
      .dcluster0_cc0_slv_r_R_Last(                                       dbus_s0_rlast                                                 ),
      .dcluster0_cc0_slv_r_R_Resp(                                       dbus_s0_rresp                                                 ),
      .dcluster0_cc0_slv_r_R_Valid(                                      dbus_s0_rvalid                                                ),
      .dcluster0_cc0_slv_r_R_User(                                       dbus_s0_ruser                                                 ),
      .dcluster0_cc0_slv_r_R_Ready(                                      dcluster0_cc0_slv_r_R_Ready                                   ),
      .dcluster0_cc0_slv_r_R_Data(                                       dbus_s0_rdata                                                 ),
      .dcluster0_cc0_slv_r_R_Id(                                         dbus_s0_rid                                                   ),
      .dcluster0_cc0_slv_r_Ar_Prot(                                      dcluster0_cc0_slv_r_Ar_Prot                                   ),
      .dcluster0_cc0_slv_r_Ar_Addr(                                      dcluster0_cc0_slv_r_Ar_Addr                                   ),
      .dcluster0_cc0_slv_r_Ar_Burst(                                     dcluster0_cc0_slv_r_Ar_Burst                                  ),
      .dcluster0_cc0_slv_r_Ar_Lock(                                      dcluster0_cc0_slv_r_Ar_Lock                                   ),
      .dcluster0_cc0_slv_r_Ar_Cache(                                     dcluster0_cc0_slv_r_Ar_Cache                                  ),
      .dcluster0_cc0_slv_r_Ar_Len(                                       dcluster0_cc0_slv_r_Ar_Len                                    ),
      .dcluster0_cc0_slv_r_Ar_Valid(                                     dcluster0_cc0_slv_r_Ar_Valid                                  ),
      .dcluster0_cc0_slv_r_Ar_User(                                      dcluster0_cc0_slv_r_Ar_User                                   ),
      .dcluster0_cc0_slv_r_Ar_Ready(                                     dbus_s0_arready                                               ),
      .dcluster0_cc0_slv_r_Ar_Id(                                        dcluster0_cc0_slv_r_Ar_Id                                     ),
      .dcluster0_cc0_slv_r_Ar_Size(                                      dcluster0_cc0_slv_r_Ar_Size                                   ),
      .dcluster0_cc0_r_R_Last(                                           dcluster0_cc0_r_R_Last                                        ),
      .dcluster0_cc0_r_R_Resp(                                           dcluster0_cc0_r_R_Resp                                        ),
      .dcluster0_cc0_r_R_Valid(                                          dcluster0_cc0_r_R_Valid                                       ),
      .dcluster0_cc0_r_R_User(                                           dcluster0_cc0_r_R_User                                        ),
      .dcluster0_cc0_r_R_Ready(                                          dbus_m0_rready                                                ),
      .dcluster0_cc0_r_R_Data(                                           dcluster0_cc0_r_R_Data                                        ),
      .dcluster0_cc0_r_R_Id(                                             dcluster0_cc0_r_R_Id                                          ),
      .dcluster0_cc0_r_Ar_Prot(                                          dbus_m0_arprot                                                ),
      .dcluster0_cc0_r_Ar_Addr(                                          dbus_m0_araddr                                                ),
      .dcluster0_cc0_r_Ar_Burst(                                         dbus_m0_arburst                                               ),
      .dcluster0_cc0_r_Ar_Lock(                                          dbus_m0_arlock                                                ),
      .dcluster0_cc0_r_Ar_Cache(                                         dbus_m0_arcache                                               ),
      .dcluster0_cc0_r_Ar_Len(                                           dbus_m0_arlen                                                 ),
      .dcluster0_cc0_r_Ar_Valid(                                         dbus_m0_arvalid                                               ),
      .dcluster0_cc0_r_Ar_User(                                          4'b0000                                                       ),
      .dcluster0_cc0_r_Ar_Ready(                                         dcluster0_cc0_r_Ar_Ready                                      ),
      .dcluster0_cc0_r_Ar_Id(                                            dbus_m0_arid                                                  ),
      .dcluster0_cc0_r_Ar_Size(                                          dbus_m0_arsize                                                ),
      .clk_dcluster0(                                                    aclk                                                          ),
      .arstn_dcluster0(                                                  arstn                                                         ),
      .TM(                                                               TM                                                            ) 
      );


dbus_write_Structure_Module_dcluster0 u_dbus_write_Structure_Module_dcluster0 (
      .dp_Link_n3_astResp_to_Link_n3_asiResp_RdCnt(                dp_Link_n3_astResp_to_Link_n3_asiResp_rdcnt             ),
      .dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRstAck(    dp_Link_n3_astResp_to_Link_n3_asiResp_rxctl_pwronrstack ),
      .dp_Link_n3_astResp_to_Link_n3_asiResp_RxCtl_PwrOnRst(       dp_Link_n3_astResp_to_Link_n3_asiResp_rxctl_pwronrst    ),
      .dp_Link_n3_astResp_to_Link_n3_asiResp_RdPtr(                dp_Link_n3_astResp_to_Link_n3_asiResp_rdptr             ),
      .dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRstAck(    dp_Link_n3_astResp_to_Link_n3_asiResp_txctl_pwronrstack ),
      .dp_Link_n3_astResp_to_Link_n3_asiResp_TxCtl_PwrOnRst(       dp_Link_n3_astResp_to_Link_n3_asiResp_txctl_pwronrst    ),
      .dp_Link_n3_astResp_to_Link_n3_asiResp_Data(                 dp_Link_n3_astResp_to_Link_n3_asiResp_data              ),
      .dp_Link_n3_astResp_to_Link_n3_asiResp_WrCnt(                dp_Link_n3_astResp_to_Link_n3_asiResp_wrcnt             ),
      .dp_Link_n3_asi_to_Link_n3_ast_RdCnt(                        dp_Link_n3_asi_to_Link_n3_ast_w_rdcnt                   ),
      .dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRstAck(            dp_Link_n3_asi_to_Link_n3_ast_w_rxctl_pwronrstack       ),
      .dp_Link_n3_asi_to_Link_n3_ast_RxCtl_PwrOnRst(               dp_Link_n3_asi_to_Link_n3_ast_w_rxctl_pwronrst          ),
      .dp_Link_n3_asi_to_Link_n3_ast_RdPtr(                        dp_Link_n3_asi_to_Link_n3_ast_w_rdptr                   ),
      .dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRstAck(            dp_Link_n3_asi_to_Link_n3_ast_w_txctl_pwronrstack       ),
      .dp_Link_n3_asi_to_Link_n3_ast_TxCtl_PwrOnRst(               dp_Link_n3_asi_to_Link_n3_ast_w_txctl_pwronrst          ),
      .dp_Link_n3_asi_to_Link_n3_ast_Data(                         dp_Link_n3_asi_to_Link_n3_ast_w_data                    ),
      .dp_Link_n3_asi_to_Link_n3_ast_WrCnt(                        dp_Link_n3_asi_to_Link_n3_ast_w_wrcnt                   ),
      .dp_Link_n2_astResp_to_Link_n2_asiResp_RdCnt(                dp_Link_n2_astResp_to_Link_n2_asiResp_rdcnt             ),
      .dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRstAck(    dp_Link_n2_astResp_to_Link_n2_asiResp_rxctl_pwronrstack ),
      .dp_Link_n2_astResp_to_Link_n2_asiResp_RxCtl_PwrOnRst(       dp_Link_n2_astResp_to_Link_n2_asiResp_rxctl_pwronrst    ),
      .dp_Link_n2_astResp_to_Link_n2_asiResp_RdPtr(                dp_Link_n2_astResp_to_Link_n2_asiResp_rdptr             ),
      .dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRstAck(    dp_Link_n2_astResp_to_Link_n2_asiResp_txctl_pwronrstack ),
      .dp_Link_n2_astResp_to_Link_n2_asiResp_TxCtl_PwrOnRst(       dp_Link_n2_astResp_to_Link_n2_asiResp_txctl_pwronrst    ),
      .dp_Link_n2_astResp_to_Link_n2_asiResp_Data(                 dp_Link_n2_astResp_to_Link_n2_asiResp_data              ),
      .dp_Link_n2_astResp_to_Link_n2_asiResp_WrCnt(                dp_Link_n2_astResp_to_Link_n2_asiResp_wrcnt             ),
      .dp_Link_n2_asi_to_Link_n2_ast_RdCnt(                        dp_Link_n2_asi_to_Link_n2_ast_w_rdcnt                   ),
      .dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRstAck(            dp_Link_n2_asi_to_Link_n2_ast_w_rxctl_pwronrstack       ),
      .dp_Link_n2_asi_to_Link_n2_ast_RxCtl_PwrOnRst(               dp_Link_n2_asi_to_Link_n2_ast_w_rxctl_pwronrst          ),
      .dp_Link_n2_asi_to_Link_n2_ast_RdPtr(                        dp_Link_n2_asi_to_Link_n2_ast_w_rdptr                   ),
      .dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRstAck(            dp_Link_n2_asi_to_Link_n2_ast_w_txctl_pwronrstack       ),
      .dp_Link_n2_asi_to_Link_n2_ast_TxCtl_PwrOnRst(               dp_Link_n2_asi_to_Link_n2_ast_w_txctl_pwronrst          ),
      .dp_Link_n2_asi_to_Link_n2_ast_Data(                         dp_Link_n2_asi_to_Link_n2_ast_w_data                    ),
      .dp_Link_n2_asi_to_Link_n2_ast_WrCnt(                        dp_Link_n2_asi_to_Link_n2_ast_w_wrcnt                   ),
      .dp_Link_n1_astResp_to_Link_n1_asiResp_RdCnt(                dp_Link_n1_astResp_to_Link_n1_asiResp_rdcnt             ),
      .dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRstAck(    dp_Link_n1_astResp_to_Link_n1_asiResp_rxctl_pwronrstack ),
      .dp_Link_n1_astResp_to_Link_n1_asiResp_RxCtl_PwrOnRst(       dp_Link_n1_astResp_to_Link_n1_asiResp_rxctl_pwronrst    ),
      .dp_Link_n1_astResp_to_Link_n1_asiResp_RdPtr(                dp_Link_n1_astResp_to_Link_n1_asiResp_rdptr             ),
      .dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRstAck(    dp_Link_n1_astResp_to_Link_n1_asiResp_txctl_pwronrstack ),
      .dp_Link_n1_astResp_to_Link_n1_asiResp_TxCtl_PwrOnRst(       dp_Link_n1_astResp_to_Link_n1_asiResp_txctl_pwronrst    ),
      .dp_Link_n1_astResp_to_Link_n1_asiResp_Data(                 dp_Link_n1_astResp_to_Link_n1_asiResp_data              ),
      .dp_Link_n1_astResp_to_Link_n1_asiResp_WrCnt(                dp_Link_n1_astResp_to_Link_n1_asiResp_wrcnt             ),
      .dp_Link_n1_asi_to_Link_n1_ast_RdCnt(                        dp_Link_n1_asi_to_Link_n1_ast_w_rdcnt                   ),
      .dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRstAck(            dp_Link_n1_asi_to_Link_n1_ast_w_rxctl_pwronrstack       ),
      .dp_Link_n1_asi_to_Link_n1_ast_RxCtl_PwrOnRst(               dp_Link_n1_asi_to_Link_n1_ast_w_rxctl_pwronrst          ),
      .dp_Link_n1_asi_to_Link_n1_ast_RdPtr(                        dp_Link_n1_asi_to_Link_n1_ast_w_rdptr                   ),
      .dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRstAck(            dp_Link_n1_asi_to_Link_n1_ast_w_txctl_pwronrstack       ),
      .dp_Link_n1_asi_to_Link_n1_ast_TxCtl_PwrOnRst(               dp_Link_n1_asi_to_Link_n1_ast_w_txctl_pwronrst          ),
      .dp_Link_n1_asi_to_Link_n1_ast_Data(                         dp_Link_n1_asi_to_Link_n1_ast_w_data                    ),
      .dp_Link_n1_asi_to_Link_n1_ast_WrCnt(                        dp_Link_n1_asi_to_Link_n1_ast_w_wrcnt                   ),
      .dp_Link_n0_astResp_to_Link_n0_asiResp_RdCnt(                dp_Link_n0_astResp_to_Link_n0_asiResp_rdcnt             ),
      .dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRstAck(    dp_Link_n0_astResp_to_Link_n0_asiResp_rxctl_pwronrstack ),
      .dp_Link_n0_astResp_to_Link_n0_asiResp_RxCtl_PwrOnRst(       dp_Link_n0_astResp_to_Link_n0_asiResp_rxctl_pwronrst    ),
      .dp_Link_n0_astResp_to_Link_n0_asiResp_RdPtr(                dp_Link_n0_astResp_to_Link_n0_asiResp_rdptr             ),
      .dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRstAck(    dp_Link_n0_astResp_to_Link_n0_asiResp_txctl_pwronrstack ),
      .dp_Link_n0_astResp_to_Link_n0_asiResp_TxCtl_PwrOnRst(       dp_Link_n0_astResp_to_Link_n0_asiResp_txctl_pwronrst    ),
      .dp_Link_n0_astResp_to_Link_n0_asiResp_Data(                 dp_Link_n0_astResp_to_Link_n0_asiResp_data              ),
      .dp_Link_n0_astResp_to_Link_n0_asiResp_WrCnt(                dp_Link_n0_astResp_to_Link_n0_asiResp_wrcnt             ),
      .dp_Link_n0_asi_to_Link_n0_ast_RdCnt(                        dp_Link_n0_asi_to_Link_n0_ast_w_rdcnt                   ),
      .dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRstAck(            dp_Link_n0_asi_to_Link_n0_ast_w_rxctl_pwronrstack       ),
      .dp_Link_n0_asi_to_Link_n0_ast_RxCtl_PwrOnRst(               dp_Link_n0_asi_to_Link_n0_ast_w_rxctl_pwronrst          ),
      .dp_Link_n0_asi_to_Link_n0_ast_RdPtr(                        dp_Link_n0_asi_to_Link_n0_ast_w_rdptr                   ),
      .dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRstAck(            dp_Link_n0_asi_to_Link_n0_ast_w_txctl_pwronrstack       ),
      .dp_Link_n0_asi_to_Link_n0_ast_TxCtl_PwrOnRst(               dp_Link_n0_asi_to_Link_n0_ast_w_txctl_pwronrst          ),
      .dp_Link_n0_asi_to_Link_n0_ast_Data(                         dp_Link_n0_asi_to_Link_n0_ast_w_data                    ),
      .dp_Link_n0_asi_to_Link_n0_ast_WrCnt(                        dp_Link_n0_asi_to_Link_n0_ast_w_wrcnt                   ),
      .dnc3_w_B_Ready(                                             dnc_dbus_m0_bready_2                                    ),
      .dnc3_w_B_Resp(                                              dnc3_w_B_Resp                                           ),
      .dnc3_w_B_Valid(                                             dnc3_w_B_Valid                                          ),
      .dnc3_w_B_User(                                              dnc3_w_B_User                                           ),
      .dnc3_w_B_Id(                                                dnc3_w_B_Id                                             ),
      .dnc3_w_Aw_Prot(                                             dnc_dbus_m0_awprot_2                                    ),
      .dnc3_w_Aw_Addr(                                           { 17'b00000000000000000,
                                                                   dnc_dbus_m0_awaddr_2 }                                  ),
      .dnc3_w_Aw_Burst(                                            dnc_dbus_m0_awburst_2                                   ),
      .dnc3_w_Aw_Lock(                                             dnc_dbus_m0_awlock_2                                    ),
      .dnc3_w_Aw_Cache(                                            dnc_dbus_m0_awcache_2                                   ),
      .dnc3_w_Aw_Len(                                            { 2'b00,
                                                                   dnc_dbus_m0_awlen_2 }                                   ),
      .dnc3_w_Aw_Valid(                                            dnc_dbus_m0_awvalid_2                                   ),
      .dnc3_w_Aw_User(                                             dnc_dbus_m0_awuser_2                                    ),
      .dnc3_w_Aw_Ready(                                            dnc3_w_Aw_Ready                                         ),
      .dnc3_w_Aw_Id(                                             { 3'b000,
                                                                   dnc_dbus_m0_awid_2 }                                    ),
      .dnc3_w_Aw_Size(                                             dnc_dbus_m0_awsize_2                                    ),
      .dnc3_w_W_Last(                                              dnc_dbus_m0_wlast_2                                     ),
      .dnc3_w_W_Valid(                                             dnc_dbus_m0_wvalid_2                                    ),
      .dnc3_w_W_Ready(                                             dnc3_w_W_Ready                                          ),
      .dnc3_w_W_Strb(                                              dnc_dbus_m0_wstrb_2                                     ),
      .dnc3_w_W_Data(                                              dnc_dbus_m0_wdata_2                                     ),
      .dnc2_w_B_Ready(                                             dnc_dbus_m0_bready_1                                    ),
      .dnc2_w_B_Resp(                                              dnc2_w_B_Resp                                           ),
      .dnc2_w_B_Valid(                                             dnc2_w_B_Valid                                          ),
      .dnc2_w_B_User(                                              dnc2_w_B_User                                           ),
      .dnc2_w_B_Id(                                                dnc2_w_B_Id                                             ),
      .dnc2_w_Aw_Prot(                                             dnc_dbus_m0_awprot_1                                    ),
      .dnc2_w_Aw_Addr(                                           { 17'b00000000000000000,
                                                                   dnc_dbus_m0_awaddr_1 }                                  ),
      .dnc2_w_Aw_Burst(                                            dnc_dbus_m0_awburst_1                                   ),
      .dnc2_w_Aw_Lock(                                             dnc_dbus_m0_awlock_1                                    ),
      .dnc2_w_Aw_Cache(                                            dnc_dbus_m0_awcache_1                                   ),
      .dnc2_w_Aw_Len(                                            { 2'b00,
                                                                   dnc_dbus_m0_awlen_1 }                                   ),
      .dnc2_w_Aw_Valid(                                            dnc_dbus_m0_awvalid_1                                   ),
      .dnc2_w_Aw_User(                                             dnc_dbus_m0_awuser_1                                    ),
      .dnc2_w_Aw_Ready(                                            dnc2_w_Aw_Ready                                         ),
      .dnc2_w_Aw_Id(                                             { 3'b000,
                                                                   dnc_dbus_m0_awid_1 }                                    ),
      .dnc2_w_Aw_Size(                                             dnc_dbus_m0_awsize_1                                    ),
      .dnc2_w_W_Last(                                              dnc_dbus_m0_wlast_1                                     ),
      .dnc2_w_W_Valid(                                             dnc_dbus_m0_wvalid_1                                    ),
      .dnc2_w_W_Ready(                                             dnc2_w_W_Ready                                          ),
      .dnc2_w_W_Strb(                                              dnc_dbus_m0_wstrb_1                                     ),
      .dnc2_w_W_Data(                                              dnc_dbus_m0_wdata_1                                     ),
      .dnc1_w_B_Ready(                                             dnc_dbus_m0_bready_0                                    ),
      .dnc1_w_B_Resp(                                              dnc1_w_B_Resp                                           ),
      .dnc1_w_B_Valid(                                             dnc1_w_B_Valid                                          ),
      .dnc1_w_B_User(                                              dnc1_w_B_User                                           ),
      .dnc1_w_B_Id(                                                dnc1_w_B_Id                                             ),
      .dnc1_w_Aw_Prot(                                             dnc_dbus_m0_awprot_0                                    ),
      .dnc1_w_Aw_Addr(                                           { 17'b00000000000000000,
                                                                   dnc_dbus_m0_awaddr_0 }                                  ),
      .dnc1_w_Aw_Burst(                                            dnc_dbus_m0_awburst_0                                   ),
      .dnc1_w_Aw_Lock(                                             dnc_dbus_m0_awlock_0                                    ),
      .dnc1_w_Aw_Cache(                                            dnc_dbus_m0_awcache_0                                   ),
      .dnc1_w_Aw_Len(                                            { 2'b00,
                                                                   dnc_dbus_m0_awlen_0 }                                   ),
      .dnc1_w_Aw_Valid(                                            dnc_dbus_m0_awvalid_0                                   ),
      .dnc1_w_Aw_User(                                             dnc_dbus_m0_awuser_0                                    ),
      .dnc1_w_Aw_Ready(                                            dnc1_w_Aw_Ready                                         ),
      .dnc1_w_Aw_Id(                                             { 3'b000,
                                                                   dnc_dbus_m0_awid_0 }                                    ),
      .dnc1_w_Aw_Size(                                             dnc_dbus_m0_awsize_0                                    ),
      .dnc1_w_W_Last(                                              dnc_dbus_m0_wlast_0                                     ),
      .dnc1_w_W_Valid(                                             dnc_dbus_m0_wvalid_0                                    ),
      .dnc1_w_W_Ready(                                             dnc1_w_W_Ready                                          ),
      .dnc1_w_W_Strb(                                              dnc_dbus_m0_wstrb_0                                     ),
      .dnc1_w_W_Data(                                              dnc_dbus_m0_wdata_0                                     ),
      .dnc0_w_B_Ready(                                             dnc_dbus_m0_bready                                      ),
      .dnc0_w_B_Resp(                                              dnc0_w_B_Resp                                           ),
      .dnc0_w_B_Valid(                                             dnc0_w_B_Valid                                          ),
      .dnc0_w_B_User(                                              dnc0_w_B_User                                           ),
      .dnc0_w_B_Id(                                                dnc0_w_B_Id                                             ),
      .dnc0_w_Aw_Prot(                                             dnc_dbus_m0_awprot                                      ),
      .dnc0_w_Aw_Addr(                                           { 17'b00000000000000000,
                                                                   dnc_dbus_m0_awaddr }                                    ),
      .dnc0_w_Aw_Burst(                                            dnc_dbus_m0_awburst                                     ),
      .dnc0_w_Aw_Lock(                                             dnc_dbus_m0_awlock                                      ),
      .dnc0_w_Aw_Cache(                                            dnc_dbus_m0_awcache                                     ),
      .dnc0_w_Aw_Len(                                            { 2'b00,
                                                                   dnc_dbus_m0_awlen }                                     ),
      .dnc0_w_Aw_Valid(                                            dnc_dbus_m0_awvalid                                     ),
      .dnc0_w_Aw_User(                                             dnc_dbus_m0_awuser                                      ),
      .dnc0_w_Aw_Ready(                                            dnc0_w_Aw_Ready                                         ),
      .dnc0_w_Aw_Id(                                             { 3'b000,
                                                                   dnc_dbus_m0_awid }                                      ),
      .dnc0_w_Aw_Size(                                             dnc_dbus_m0_awsize                                      ),
      .dnc0_w_W_Last(                                              dnc_dbus_m0_wlast                                       ),
      .dnc0_w_W_Valid(                                             dnc_dbus_m0_wvalid                                      ),
      .dnc0_w_W_Ready(                                             dnc0_w_W_Ready                                          ),
      .dnc0_w_W_Strb(                                              dnc_dbus_m0_wstrb                                       ),
      .dnc0_w_W_Data(                                              dnc_dbus_m0_wdata                                       ),
      .clk_dcluster0(                                              aclk                                                    ),
      .arstn_dcluster0(                                            arstn                                                   ),
      .TM(                                                         TM                                                      ) 
      );



// constant signals initialisation
endmodule
