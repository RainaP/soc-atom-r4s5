if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n4Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n4Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n4Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n4Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n4_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n4_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n4_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n4_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n5Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n5Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n5Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n5Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n5_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n5_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n5_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n5_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n6Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n6Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n6Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n6Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n6_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n6_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n6_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n6_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n7Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n7Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n7Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n7Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n7_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n7_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n7_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n7_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n47_aResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n47_aResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n47_aResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n47_aResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n47_a_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n47_a_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n47_a_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n47_a_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n47_astResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n47_astResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n47_astResp001_main/DtpTxClkAdapt_Link_n47_asiResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n47_astResp001_main/DtpTxClkAdapt_Link_n47_asiResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n47_ast_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n47_ast_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n47_ast_main/DtpRxClkAdapt_Link_n47_asi_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n47_ast_main/DtpRxClkAdapt_Link_n47_asi_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dcluster1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dcluster1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n4Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n4Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n4Resp001_main/Mux_Link_2n4Resp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n4Resp001_main/Mux_Link_2n4Resp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n4_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n4_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n4_main/Demux_Link_2n4] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n4_main/Demux_Link_2n4 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n4Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n4Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n4Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n4Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n4_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n4_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n4_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n4_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster1_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster1_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster1_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster1_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster1_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster1_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster1_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster1_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster1_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n5Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n5Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n5_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n5_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n6Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n6Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n6_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n6_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n7Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n7Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n7_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n7_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster1_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster1_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster1_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster1_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n47Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n47Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n47Resp001_main/Mux_Link_n47_aResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n47Resp001_main/Mux_Link_n47_aResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n47_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n47_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n47_main/Demux_Link_n47_a] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n47_main/Demux_Link_n47_a false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n4Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n4Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n4Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n4Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n4_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n4_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n4_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n4_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n5Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n5Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n5Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n5Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n5_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n5_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n5_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n5_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n6Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n6Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n6Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n6Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n6_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n6_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n6_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n6_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n7Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n7Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n7Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n7Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n7_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n7_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n7_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n7_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_aResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_aResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_aResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_aResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_a_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_a_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_a_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_a_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_astResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_astResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_astResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_astResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_ast_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_ast_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_ast_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n47_ast_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n47Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n47Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n47Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n47Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n47_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n47_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n47_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n47_main_Sys/ClockGater false
}
