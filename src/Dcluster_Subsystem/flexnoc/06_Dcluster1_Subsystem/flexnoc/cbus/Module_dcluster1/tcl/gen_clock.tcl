
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:59

####################################################################

arteris_gen_clock -name "Regime_dcluster1_Cm_root" -pin "Regime_dcluster1_Cm_main/root_Clk Regime_dcluster1_Cm_main/root_Clk_ClkS" -clock_domain "clk_dcluster1" -spec_domain_clock "/Regime_dcluster1/Cm/root" -divide_by "1" -source "clk_dcluster1" -source_period "${clk_dcluster1_P}" -source_waveform "[expr ${clk_dcluster1_P}*0.00] [expr ${clk_dcluster1_P}*0.50]" -user_directive "" -add "FALSE"
