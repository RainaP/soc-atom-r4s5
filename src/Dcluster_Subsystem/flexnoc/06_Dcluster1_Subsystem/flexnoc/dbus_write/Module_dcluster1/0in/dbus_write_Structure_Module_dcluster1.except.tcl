
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:21:02

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_n4_asiResp_main.DtpRxClkAdapt_Link_n4_astResp_Async.RdCnt -graycode -module dbus_write_Structure_Module_dcluster1
cdc signal Link_n4_asi_main.DtpTxClkAdapt_Link_n4_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_dcluster1
cdc signal Link_n5_asiResp_main.DtpRxClkAdapt_Link_n5_astResp_Async.RdCnt -graycode -module dbus_write_Structure_Module_dcluster1
cdc signal Link_n5_asi_main.DtpTxClkAdapt_Link_n5_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_dcluster1
cdc signal Link_n6_asiResp_main.DtpRxClkAdapt_Link_n6_astResp_Async.RdCnt -graycode -module dbus_write_Structure_Module_dcluster1
cdc signal Link_n6_asi_main.DtpTxClkAdapt_Link_n6_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_dcluster1
cdc signal Link_n7_asiResp_main.DtpRxClkAdapt_Link_n7_astResp_Async.RdCnt -graycode -module dbus_write_Structure_Module_dcluster1
cdc signal Link_n7_asi_main.DtpTxClkAdapt_Link_n7_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_dcluster1

