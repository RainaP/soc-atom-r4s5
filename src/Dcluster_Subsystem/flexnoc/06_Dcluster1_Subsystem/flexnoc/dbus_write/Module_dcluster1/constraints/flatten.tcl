if {[get_cells ${CUSTOMER_HIERARCHY}Link_n4_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n4_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n4_asiResp_main/DtpRxClkAdapt_Link_n4_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n4_asiResp_main/DtpRxClkAdapt_Link_n4_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n4_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n4_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n4_asi_main/DtpTxClkAdapt_Link_n4_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n4_asi_main/DtpTxClkAdapt_Link_n4_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n5_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n5_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n5_asiResp_main/DtpRxClkAdapt_Link_n5_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n5_asiResp_main/DtpRxClkAdapt_Link_n5_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n5_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n5_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n5_asi_main/DtpTxClkAdapt_Link_n5_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n5_asi_main/DtpTxClkAdapt_Link_n5_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n5_lResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n5_lResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n5_lResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n5_lResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n5_l_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n5_l_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n5_l_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n5_l_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n6_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n6_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n6_asiResp_main/DtpRxClkAdapt_Link_n6_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n6_asiResp_main/DtpRxClkAdapt_Link_n6_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n6_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n6_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n6_asi_main/DtpTxClkAdapt_Link_n6_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n6_asi_main/DtpTxClkAdapt_Link_n6_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n7_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n7_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n7_asiResp_main/DtpRxClkAdapt_Link_n7_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n7_asiResp_main/DtpRxClkAdapt_Link_n7_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n7_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n7_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n7_asi_main/DtpTxClkAdapt_Link_n7_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n7_asi_main/DtpTxClkAdapt_Link_n7_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n7_lResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n7_lResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n7_lResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n7_lResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n7_l_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n7_l_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n7_l_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n7_l_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dcluster1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dcluster1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc4_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/DtpRxSerAdapt_Link_n4_asiResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/DtpRxSerAdapt_Link_n4_asiResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc4_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc5_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/DtpRxSerAdapt_Link_n5_lResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/DtpRxSerAdapt_Link_n5_lResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc5_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc6_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/DtpRxSerAdapt_Link_n6_asiResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/DtpRxSerAdapt_Link_n6_asiResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc6_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc7_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/DtpRxSerAdapt_Link_n7_lResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/DtpRxSerAdapt_Link_n7_lResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc7_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster1_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster1_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster1_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster1_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n4_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n4_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n4_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n4_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n4_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n4_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n4_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n4_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_lResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_lResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_lResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_lResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_l_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_l_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_l_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n5_l_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n6_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n6_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n6_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n6_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n6_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n6_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n6_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n6_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_lResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_lResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_lResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_lResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_l_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_l_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_l_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n7_l_main_Sys/ClockGater false
}
