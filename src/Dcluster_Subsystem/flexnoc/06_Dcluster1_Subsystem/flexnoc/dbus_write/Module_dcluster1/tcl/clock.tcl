
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:21:02

####################################################################

arteris_clock -name "clk_dcluster1" -clock_domain "clk_dcluster1" -port "clk_dcluster1" -period "${clk_dcluster1_P}" -waveform "[expr ${clk_dcluster1_P}*0.00] [expr ${clk_dcluster1_P}*0.50]" -edge "R" -user_directive ""
