# Script dc_shell.tcl



set DESIGN_NAME                               "dbus_write_Structure_Module_dcluster1"
set LIBRARY_DONT_USE_FILE                     ""
set LIBRARY_DONT_USE_PRE_COMPILE_LIST         ""
set LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST    ""
set ADDITIONAL_SEARCH_PATH                    ""
source -echo -verbose 
set_units -time ns -resistance kOhm -capacitance pF -voltage V -current mA
set_app_var synthetic_library dw_foundation.sldb
set_app_var link_library [concat {*} $target_library $synthetic_library]
source -echo -verbose ./dc_setup_filenames.tcl
set_app_var sh_new_variable_message false
set RTL_SOURCE_FILES  "rtl.GaterCell.v rtl.ClockManagerCell.v rtl.SynchronizerCell.v dbus_write_Structure_Module_dcluster1_Module_dnc3.v dbus_write_Structure_Module_dcluster1_Module_dnc2.v dbus_write_Structure_Module_dcluster1_Module_dnc1.v dbus_write_Structure_Module_dcluster1_Module_dnc0.v dbus_write_Structure_Module_dcluster1_commons.v dbus_write_Structure_Module_dcluster1.v"
set REPORTS_DIR  "./reports"
set RESULTS_DIR  "./results"
file mkdir ${REPORTS_DIR}
file mkdir ${RESULTS_DIR}
set CLOCK_GATE_INST_LIST  " clockGaters_Link_n4_asi_main_Sys/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n6_asi_main_Sys/ClockGater/usced609d48c/instGaterCell Module_dnc0/clockGaters_dnc4_w/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n7_lResp_main_Sys/ClockGater/usced609d48c/instGaterCell Module_dnc2/clockGaters_dnc6_w_I/ClockGater/usced609d48c/instGaterCell Module_dnc0/clockGaters_dnc4_w_I/ClockGater/usced609d48c/instGaterCell Module_dnc3/clockGaters_dnc7_w_I/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n7_l_main_Sys/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n7_asiResp_main_Sys/ClockGater/usced609d48c/instGaterCell Module_dnc1/clockGaters_dnc5_w/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n5_asi_main_Sys/ClockGater/usced609d48c/instGaterCell Module_dnc2/clockGaters_dnc6_w/ClockGater/usced609d48c/instGaterCell Module_dnc3/clockGaters_dnc7_w/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n5_l_main_Sys/ClockGater/usced609d48c/instGaterCell Module_dnc1/clockGaters_dnc5_w_I/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n5_lResp_main_Sys/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n6_asiResp_main_Sys/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n5_asiResp_main_Sys/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n4_asiResp_main_Sys/ClockGater/usced609d48c/instGaterCell clockGaters_Link_n7_asi_main_Sys/ClockGater/usced609d48c/instGaterCell "
set OPTIMIZATION_FLOW hplp
set_app_var search_path ". ./sdc ${ADDITIONAL_SEARCH_PATH} $search_path"
set mw_design_library ${DCRM_MW_LIBRARY_NAME}
extend_mw_layers
if {![file isdirectory $mw_design_library ]} {
  create_mw_lib   -technology $MW_TECH_FILE -mw_reference_library $MW_REF_LIB_LIST $mw_design_library
} else {
set_mw_lib_reference $mw_design_library -mw_reference_library $MW_REF_LIB_LIST
}
open_mw_lib     $mw_design_library
check_library > ${REPORTS_DIR}/${DCRM_CHECK_LIBRARY_REPORT}
set_tlu_plus_files -max_tluplus $MAX_TLU_PLUS -min_tluplus $MIN_TLU_PLUS
check_tlu_plus_files
if {[file exists [which ${LIBRARY_DONT_USE_FILE}]]} {
  source -echo -verbose ${LIBRARY_DONT_USE_FILE}
}
