
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:21:02

####################################################################

# Validated with Formality (TM) version G-2012.06-SP2
# fm_shell -f fm.optl.tcl >& fm.optl.log &
#
set ARTERIS_INPUT_DIR    [sh pwd]
set ARTERIS_OUTPUT_DIR   [sh pwd]
puts "Formal_proof started on host: [sh hostname] ([sh date])"

# Sourcing technology information (file name is given from customerTechnoInf present in exportView)
# File: example_customer.inf is provided as example
source 

#
set gui_report_length_limit 300000
set hdlin_warn_on_mismatch_message [list FMR_VHDL-1027]
set verification_failing_point_limit 1000
set verification_clock_gate_hold_mode low
set verification_effort_level low
#set verification_timeout_limit 60:0:0
#
#set_svf $ARTERIS_INPUT_DIR/optl.svf
#set verification_set_undriven_signals synthesis

set ARTERIS_TARGET_LIB ""
foreach { _lib _val } [array get A_LIBRARY_INFO] {
 foreach { __tag __val } [array get $_lib] {
  if { $__tag == "library_path" } { read_db [eval format "%s" $__val] }
 }
}

#
printvar > $ARTERIS_OUTPUT_DIR/fm.optl.printvar
#

read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/rtl.GaterCell.v
read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/rtl.ClockManagerCell.v
read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/rtl.SynchronizerCell.v
read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/dbus_write_Structure_Module_dcluster1_Module_dnc3.v
read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/dbus_write_Structure_Module_dcluster1_Module_dnc2.v
read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/dbus_write_Structure_Module_dcluster1_Module_dnc1.v
read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/dbus_write_Structure_Module_dcluster1_Module_dnc0.v
read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/dbus_write_Structure_Module_dcluster1_commons.v
read_verilog -container r -libname WORK $ARTERIS_INPUT_DIR/dbus_write_Structure_Module_dcluster1.v
set_top r:/WORK/dbus_write_Structure_Module_dcluster1
#

read_verilog -container i -libname WORK $ARTERIS_INPUT_DIR/optl.v
set_top i:/WORK/dbus_write_Structure_Module_dcluster1
#

match
report_unmatched_points > $ARTERIS_OUTPUT_DIR/unmatched_DFF.rpt
set_dont_verify_point -directly_undriven_output
verify
report_aborted_points > $ARTERIS_OUTPUT_DIR/aborted_points.rpt
# In case of not enough CPU time
set verification_effort_level high
verify
report_aborted_points > $ARTERIS_OUTPUT_DIR/aborted_points.high.rpt
#
date
puts "Formal_proof finished on host: [sh hostname] ([sh date])"
puts "Maximum memory usage for this session: [format "%.0f KB" [mem]]"
puts "CPU usage for this session: [format "%.0f seconds" [cputime]]"
exit
#
