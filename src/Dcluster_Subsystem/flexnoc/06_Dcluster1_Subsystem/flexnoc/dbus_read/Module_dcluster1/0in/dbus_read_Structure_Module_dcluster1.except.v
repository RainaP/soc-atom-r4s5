
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:11:23

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_dcluster1_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme reconvergence -from_signals Link_n4_asiResp001_main.DtpRxClkAdapt_Link_n4_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme reconvergence -from_signals Link_n4_asi_main.DtpTxClkAdapt_Link_n4_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme reconvergence -from_signals Link_n5_asiResp001_main.DtpRxClkAdapt_Link_n5_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme reconvergence -from_signals Link_n5_asi_main.DtpTxClkAdapt_Link_n5_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme reconvergence -from_signals Link_n6_asiResp001_main.DtpRxClkAdapt_Link_n6_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme reconvergence -from_signals Link_n6_asi_main.DtpTxClkAdapt_Link_n6_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme reconvergence -from_signals Link_n7_asiResp001_main.DtpRxClkAdapt_Link_n7_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme reconvergence -from_signals Link_n7_asi_main.DtpTxClkAdapt_Link_n7_ast_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme multi_sync_mux_select -from Link_n4_asiResp001_main.DtpRxClkAdapt_Link_n4_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme multi_sync_mux_select -from Link_n5_asiResp001_main.DtpRxClkAdapt_Link_n5_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme multi_sync_mux_select -from Link_n6_asiResp001_main.DtpRxClkAdapt_Link_n6_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster1 -severity waived -scheme multi_sync_mux_select -from Link_n7_asiResp001_main.DtpRxClkAdapt_Link_n7_astResp001_Async.uRegSync.instSynchronizerCell*

endmodule
