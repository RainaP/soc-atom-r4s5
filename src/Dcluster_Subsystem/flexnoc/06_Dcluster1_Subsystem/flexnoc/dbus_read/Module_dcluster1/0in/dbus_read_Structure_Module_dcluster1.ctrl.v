
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:11:23

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_dcluster1_ctrl;

// FIFO module definition
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n4_asi_main.DtpTxClkAdapt_Link_n4_ast_Async.RegData_0
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n4_asi_main.DtpTxClkAdapt_Link_n4_ast_Async.RegData_1
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n4_asi_main.DtpTxClkAdapt_Link_n4_ast_Async.RegData_2
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n4_asi_main.DtpTxClkAdapt_Link_n4_ast_Async.RegData_3
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n4_asi_main.DtpTxClkAdapt_Link_n4_ast_Async.RegData_4
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n4_asi_main.DtpTxClkAdapt_Link_n4_ast_Async.RegData_5
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n5_asi_main.DtpTxClkAdapt_Link_n5_ast_Async.RegData_0
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n5_asi_main.DtpTxClkAdapt_Link_n5_ast_Async.RegData_1
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n5_asi_main.DtpTxClkAdapt_Link_n5_ast_Async.RegData_2
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n5_asi_main.DtpTxClkAdapt_Link_n5_ast_Async.RegData_3
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n5_asi_main.DtpTxClkAdapt_Link_n5_ast_Async.RegData_4
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n5_asi_main.DtpTxClkAdapt_Link_n5_ast_Async.RegData_5
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n6_asi_main.DtpTxClkAdapt_Link_n6_ast_Async.RegData_0
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n6_asi_main.DtpTxClkAdapt_Link_n6_ast_Async.RegData_1
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n6_asi_main.DtpTxClkAdapt_Link_n6_ast_Async.RegData_2
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n6_asi_main.DtpTxClkAdapt_Link_n6_ast_Async.RegData_3
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n6_asi_main.DtpTxClkAdapt_Link_n6_ast_Async.RegData_4
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n6_asi_main.DtpTxClkAdapt_Link_n6_ast_Async.RegData_5
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n7_asi_main.DtpTxClkAdapt_Link_n7_ast_Async.RegData_0
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n7_asi_main.DtpTxClkAdapt_Link_n7_ast_Async.RegData_1
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n7_asi_main.DtpTxClkAdapt_Link_n7_ast_Async.RegData_2
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n7_asi_main.DtpTxClkAdapt_Link_n7_ast_Async.RegData_3
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n7_asi_main.DtpTxClkAdapt_Link_n7_ast_Async.RegData_4
// 0in set_cdc_fifo -module dbus_read_Structure_Module_dcluster1 Link_n7_asi_main.DtpTxClkAdapt_Link_n7_ast_Async.RegData_5

endmodule
