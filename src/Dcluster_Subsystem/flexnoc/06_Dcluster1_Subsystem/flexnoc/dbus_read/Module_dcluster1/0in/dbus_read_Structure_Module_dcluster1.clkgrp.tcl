
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:11:23

####################################################################

netlist clock Regime_dcluster1_Cm_main.root_Clk -module dbus_read_Structure_Module_dcluster1 -group clk_dcluster1 -period 0.667 -waveform {0.000 0.334}
netlist clock Regime_dcluster1_Cm_main.root_Clk_ClkS -module dbus_read_Structure_Module_dcluster1 -group clk_dcluster1 -period 0.667 -waveform {0.000 0.334}
netlist clock clk_dcluster1 -module dbus_read_Structure_Module_dcluster1 -group clk_dcluster1 -period 0.667 -waveform {0.000 0.334}

