set CUSTOMER_HIERARCHY ""

set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/ClockManager/IClockManager_Regime_dcluster0_Cm false 
set_dont_touch ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w_I/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_lResp_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_l_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w_I/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w_I/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w_I/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_lResp_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_l_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp_main_Sys/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w/ClockGater/usce77587fc1/instGaterCell true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main/DtpRxClkAdapt_Link_n0_astResp_Async/urs/Isc1 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main/DtpRxClkAdapt_Link_n1_astResp_Async/urs/Isc1 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main/DtpRxClkAdapt_Link_n3_astResp_Async/urs/Isc1 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main/DtpRxClkAdapt_Link_n2_astResp_Async/urs/Isc1 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main/DtpRxClkAdapt_Link_n0_astResp_Async/urs/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main/DtpRxClkAdapt_Link_n1_astResp_Async/urs/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main/DtpRxClkAdapt_Link_n3_astResp_Async/urs/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main/DtpRxClkAdapt_Link_n2_astResp_Async/urs/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main/DtpRxClkAdapt_Link_n0_astResp_Async/urs0/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main/DtpRxClkAdapt_Link_n1_astResp_Async/urs0/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main/DtpRxClkAdapt_Link_n3_astResp_Async/urs0/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main/DtpRxClkAdapt_Link_n2_astResp_Async/urs0/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main/DtpRxClkAdapt_Link_n0_astResp_Async/urs1/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main/DtpRxClkAdapt_Link_n1_astResp_Async/urs1/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main/DtpRxClkAdapt_Link_n3_astResp_Async/urs1/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main/DtpRxClkAdapt_Link_n2_astResp_Async/urs1/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async/urs1/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async/urs1/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async/urs1/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async/urs1/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async/urs0/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async/urs0/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async/urs0/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async/urs0/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async/urs/Isc2 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async/urs/Isc2 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async/urs/Isc2 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async/urs/Isc2 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main/DtpRxClkAdapt_Link_n0_astResp_Async/urs/Isc2 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main/DtpRxClkAdapt_Link_n1_astResp_Async/urs/Isc2 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main/DtpRxClkAdapt_Link_n3_astResp_Async/urs/Isc2 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main/DtpRxClkAdapt_Link_n2_astResp_Async/urs/Isc2 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async/urs/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async/urs/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async/urs/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async/urs/Isc0 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async/urs/Isc1 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async/urs/Isc1 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async/urs/Isc1 true 
set_dont_touch ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async/urs/Isc1 true 
