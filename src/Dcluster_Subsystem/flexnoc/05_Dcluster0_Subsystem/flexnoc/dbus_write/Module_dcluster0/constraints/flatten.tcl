if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main/DtpRxClkAdapt_Link_n0_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_asiResp_main/DtpRxClkAdapt_Link_n0_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_lResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_lResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_lResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_lResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_l_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_l_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_l_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_l_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main/DtpRxClkAdapt_Link_n1_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_asiResp_main/DtpRxClkAdapt_Link_n1_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main/DtpRxClkAdapt_Link_n2_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_asiResp_main/DtpRxClkAdapt_Link_n2_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_lResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_lResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_lResp_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_lResp_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_l_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_l_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_l_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_l_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main/DtpRxClkAdapt_Link_n3_astResp_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_asiResp_main/DtpRxClkAdapt_Link_n3_astResp_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dcluster0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dcluster0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/DtpRxSerAdapt_Link_n0_lResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/DtpRxSerAdapt_Link_n0_lResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/DtpRxSerAdapt_Link_n1_asiResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/DtpRxSerAdapt_Link_n1_asiResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/DtpRxSerAdapt_Link_n2_lResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/DtpRxSerAdapt_Link_n2_lResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_w_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/DtpRxSerAdapt_Link_n3_asiResp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/DtpRxSerAdapt_Link_n3_asiResp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_w_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_lResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_lResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_lResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_lResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_l_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_l_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_l_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_l_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_lResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_lResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_lResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_lResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_l_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_l_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_l_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_l_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys/ClockGater false
}
