
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:16

####################################################################

arteris_clock -name "clk_dcluster0" -clock_domain "clk_dcluster0" -port "clk_dcluster0" -period "${clk_dcluster0_P}" -waveform "[expr ${clk_dcluster0_P}*0.00] [expr ${clk_dcluster0_P}*0.50]" -edge "R" -user_directive ""
