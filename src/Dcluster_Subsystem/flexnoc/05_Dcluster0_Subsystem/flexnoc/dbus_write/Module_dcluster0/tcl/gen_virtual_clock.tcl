
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:16

####################################################################

arteris_gen_virtual_clock -name "Regime_bus_Cm_center_root" -clock_domain "Regime_bus_Cm_center_root" -spec_domain_clock "/Regime_bus/Cm_center/root" -divide_by "1" -source "NA" -source_period "${Regime_bus_P}" -source_waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_bus_Cm_east_root" -clock_domain "Regime_bus_Cm_east_root" -spec_domain_clock "/Regime_bus/Cm_east/root" -divide_by "1" -source "NA" -source_period "${Regime_bus_P}" -source_waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_bus_Cm_south_root" -clock_domain "Regime_bus_Cm_south_root" -spec_domain_clock "/Regime_bus/Cm_south/root" -divide_by "1" -source "NA" -source_period "${Regime_bus_P}" -source_waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_bus_Cm_west_root" -clock_domain "Regime_bus_Cm_west_root" -spec_domain_clock "/Regime_bus/Cm_west/root" -divide_by "1" -source "NA" -source_period "${Regime_bus_P}" -source_waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_cpu_Cm_root" -clock_domain "Regime_cpu_Cm_root" -spec_domain_clock "/Regime_cpu/Cm/root" -divide_by "1" -source "NA" -source_period "${Regime_cpu_P}" -source_waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_dcluster1_Cm_root" -clock_domain "Regime_dcluster1_Cm_root" -spec_domain_clock "/Regime_dcluster1/Cm/root" -divide_by "1" -source "NA" -source_period "${Regime_dcluster1_P}" -source_waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_dram0_Cm_root" -clock_domain "Regime_dram0_Cm_root" -spec_domain_clock "/Regime_dram0/Cm/root" -divide_by "1" -source "NA" -source_period "${Regime_dram0_P}" -source_waveform "[expr ${Regime_dram0_P}*0.00] [expr ${Regime_dram0_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_dram1_Cm_root" -clock_domain "Regime_dram1_Cm_root" -spec_domain_clock "/Regime_dram1/Cm/root" -divide_by "1" -source "NA" -source_period "${Regime_dram1_P}" -source_waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_dram2_Cm_root" -clock_domain "Regime_dram2_Cm_root" -spec_domain_clock "/Regime_dram2/Cm/root" -divide_by "1" -source "NA" -source_period "${Regime_dram2_P}" -source_waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_dram3_Cm_root" -clock_domain "Regime_dram3_Cm_root" -spec_domain_clock "/Regime_dram3/Cm/root" -divide_by "1" -source "NA" -source_period "${Regime_dram3_P}" -source_waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]" -user_directive ""
arteris_gen_virtual_clock -name "Regime_pcie_Cm_root" -clock_domain "Regime_pcie_Cm_root" -spec_domain_clock "/Regime_pcie/Cm/root" -divide_by "1" -source "NA" -source_period "${Regime_pcie_P}" -source_waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]" -user_directive ""
