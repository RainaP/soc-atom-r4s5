
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:16

####################################################################

# FIFO module definition
cdc fifo -module dbus_write_Structure_Module_dcluster0 Link_n0_asi_main.DtpTxClkAdapt_Link_n0_ast_Async.RegData_*
cdc fifo -module dbus_write_Structure_Module_dcluster0 Link_n1_asi_main.DtpTxClkAdapt_Link_n1_ast_Async.RegData_*
cdc fifo -module dbus_write_Structure_Module_dcluster0 Link_n2_asi_main.DtpTxClkAdapt_Link_n2_ast_Async.RegData_*
cdc fifo -module dbus_write_Structure_Module_dcluster0 Link_n3_asi_main.DtpTxClkAdapt_Link_n3_ast_Async.RegData_*

