
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:20:16

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_n0_asiResp_main.DtpRxClkAdapt_Link_n0_astResp_Async.RdCnt -graycode -module dbus_write_Structure_Module_dcluster0
cdc signal Link_n0_asi_main.DtpTxClkAdapt_Link_n0_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_dcluster0
cdc signal Link_n1_asiResp_main.DtpRxClkAdapt_Link_n1_astResp_Async.RdCnt -graycode -module dbus_write_Structure_Module_dcluster0
cdc signal Link_n1_asi_main.DtpTxClkAdapt_Link_n1_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_dcluster0
cdc signal Link_n2_asiResp_main.DtpRxClkAdapt_Link_n2_astResp_Async.RdCnt -graycode -module dbus_write_Structure_Module_dcluster0
cdc signal Link_n2_asi_main.DtpTxClkAdapt_Link_n2_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_dcluster0
cdc signal Link_n3_asiResp_main.DtpRxClkAdapt_Link_n3_astResp_Async.RdCnt -graycode -module dbus_write_Structure_Module_dcluster0
cdc signal Link_n3_asi_main.DtpTxClkAdapt_Link_n3_ast_Async.WrCnt -graycode -module dbus_write_Structure_Module_dcluster0

