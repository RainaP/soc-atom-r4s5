# Script dc_shell.tcl



set DESIGN_NAME                               "dbus_write_Structure_Module_dcluster0"
set LIBRARY_DONT_USE_FILE                     ""
set LIBRARY_DONT_USE_PRE_COMPILE_LIST         ""
set LIBRARY_DONT_USE_PRE_INCR_COMPILE_LIST    ""
set ADDITIONAL_SEARCH_PATH                    ""
source -echo -verbose 
set_units -time ns -resistance kOhm -capacitance pF -voltage V -current mA
set_app_var synthetic_library dw_foundation.sldb
set_app_var link_library [concat {*} $target_library $synthetic_library]
source -echo -verbose ./dc_setup_filenames.tcl
set_app_var sh_new_variable_message false
set RTL_SOURCE_FILES  "rtl.GaterCell.v rtl.ClockManagerCell.v rtl.SynchronizerCell.v dbus_write_Structure_Module_dcluster0_Module_dnc3.v dbus_write_Structure_Module_dcluster0_Module_dnc2.v dbus_write_Structure_Module_dcluster0_Module_dnc1.v dbus_write_Structure_Module_dcluster0_Module_dnc0.v dbus_write_Structure_Module_dcluster0_commons.v dbus_write_Structure_Module_dcluster0.v"
set REPORTS_DIR  "./reports"
set RESULTS_DIR  "./results"
file mkdir ${REPORTS_DIR}
file mkdir ${RESULTS_DIR}
set CLOCK_GATE_INST_LIST  " Module_dnc2/clockGaters_dnc2_w_I/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n2_lResp_main_Sys/ClockGater/usce77587fc1/instGaterCell Module_dnc0/clockGaters_dnc0_w/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n2_l_main_Sys/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n1_asiResp_main_Sys/ClockGater/usce77587fc1/instGaterCell Module_dnc0/clockGaters_dnc0_w_I/ClockGater/usce77587fc1/instGaterCell Module_dnc1/clockGaters_dnc1_w_I/ClockGater/usce77587fc1/instGaterCell Module_dnc3/clockGaters_dnc3_w_I/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n0_asiResp_main_Sys/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n2_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n0_lResp_main_Sys/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n0_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell Module_dnc1/clockGaters_dnc1_w/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n0_l_main_Sys/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n3_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell Module_dnc2/clockGaters_dnc2_w/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n2_asiResp_main_Sys/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n1_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell clockGaters_Link_n3_asiResp_main_Sys/ClockGater/usce77587fc1/instGaterCell Module_dnc3/clockGaters_dnc3_w/ClockGater/usce77587fc1/instGaterCell "
set OPTIMIZATION_FLOW hplp
set_app_var search_path ". ./sdc ${ADDITIONAL_SEARCH_PATH} $search_path"
set mw_design_library ${DCRM_MW_LIBRARY_NAME}
extend_mw_layers
if {![file isdirectory $mw_design_library ]} {
  create_mw_lib   -technology $MW_TECH_FILE -mw_reference_library $MW_REF_LIB_LIST $mw_design_library
} else {
set_mw_lib_reference $mw_design_library -mw_reference_library $MW_REF_LIB_LIST
}
open_mw_lib     $mw_design_library
check_library > ${REPORTS_DIR}/${DCRM_CHECK_LIBRARY_REPORT}
set_tlu_plus_files -max_tluplus $MAX_TLU_PLUS -min_tluplus $MIN_TLU_PLUS
check_tlu_plus_files
if {[file exists [which ${LIBRARY_DONT_USE_FILE}]]} {
  source -echo -verbose ${LIBRARY_DONT_USE_FILE}
}
