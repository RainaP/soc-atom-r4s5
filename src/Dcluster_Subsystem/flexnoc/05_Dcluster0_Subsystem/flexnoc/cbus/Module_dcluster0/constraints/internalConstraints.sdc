
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:57

####################################################################

# Create Clocks

create_clock -name "clk_dcluster0" [get_ports "clk_dcluster0"] -period "${clk_dcluster0_P}" -waveform "[expr ${clk_dcluster0_P}*0.00] [expr ${clk_dcluster0_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks clk_dcluster0] 
# Create Virtual Clocks

create_clock -name "Regime_cbus" -period "${Regime_cbus_P}" -waveform "[expr ${Regime_cbus_P}*0.00] [expr ${Regime_cbus_P}*0.50]"
create_clock -name "Regime_cbus_ls_east" -period "${Regime_cbus_ls_east_P}" -waveform "[expr ${Regime_cbus_ls_east_P}*0.00] [expr ${Regime_cbus_ls_east_P}*0.50]"
create_clock -name "Regime_cbus_ls_west" -period "${Regime_cbus_ls_west_P}" -waveform "[expr ${Regime_cbus_ls_west_P}*0.00] [expr ${Regime_cbus_ls_west_P}*0.50]"
create_clock -name "Regime_cbus_south" -period "${Regime_cbus_south_P}" -waveform "[expr ${Regime_cbus_south_P}*0.00] [expr ${Regime_cbus_south_P}*0.50]"
create_clock -name "Regime_cpu" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]"
create_clock -name "Regime_dcluster1" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]"
create_clock -name "Regime_dram0" -period "${Regime_dram0_P}" -waveform "[expr ${Regime_dram0_P}*0.00] [expr ${Regime_dram0_P}*0.50]"
create_clock -name "Regime_dram1" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]"
create_clock -name "Regime_dram2" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]"
create_clock -name "Regime_dram3" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]"
create_clock -name "Regime_pcie" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]"
create_clock -name "Regime_peri" -period "${Regime_peri_P}" -waveform "[expr ${Regime_peri_P}*0.00] [expr ${Regime_peri_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus_ls_east] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus_ls_west] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus_south] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cpu] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster1] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram0] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram1] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram2] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram3] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_pcie] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_peri] 
# Create Generated Virtual Clocks 

create_clock -name "Regime_cbus_Cm_center_root" -period "${Regime_cbus_P}" -waveform "[expr ${Regime_cbus_P}*0.00] [expr ${Regime_cbus_P}*0.50]"
create_clock -name "Regime_cbus_Cm_east_root" -period "${Regime_cbus_P}" -waveform "[expr ${Regime_cbus_P}*0.00] [expr ${Regime_cbus_P}*0.50]"
create_clock -name "Regime_cbus_ls_east_Cm_root" -period "${Regime_cbus_ls_east_P}" -waveform "[expr ${Regime_cbus_ls_east_P}*0.00] [expr ${Regime_cbus_ls_east_P}*0.50]"
create_clock -name "Regime_cbus_ls_west_Cm_root" -period "${Regime_cbus_ls_west_P}" -waveform "[expr ${Regime_cbus_ls_west_P}*0.00] [expr ${Regime_cbus_ls_west_P}*0.50]"
create_clock -name "Regime_cbus_south_Cm_root" -period "${Regime_cbus_south_P}" -waveform "[expr ${Regime_cbus_south_P}*0.00] [expr ${Regime_cbus_south_P}*0.50]"
create_clock -name "Regime_cpu_Cm_root" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]"
create_clock -name "Regime_dcluster1_Cm_root" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]"
create_clock -name "Regime_dram0_Cm_root" -period "${Regime_dram0_P}" -waveform "[expr ${Regime_dram0_P}*0.00] [expr ${Regime_dram0_P}*0.50]"
create_clock -name "Regime_dram1_Cm_root" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]"
create_clock -name "Regime_dram2_Cm_root" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]"
create_clock -name "Regime_dram3_Cm_root" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]"
create_clock -name "Regime_pcie_Cm_root" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]"
create_clock -name "Regime_peri_Cm_root" -period "${Regime_peri_P}" -waveform "[expr ${Regime_peri_P}*0.00] [expr ${Regime_peri_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus_Cm_center_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus_Cm_east_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus_ls_east_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus_ls_west_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cbus_south_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cpu_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster1_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram0_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram1_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram2_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram3_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_pcie_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_peri_Cm_root] 
# Create Generated Clocks

create_generated_clock -name "Regime_dcluster0_Cm_root" [get_pins "${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/root_Clk ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/root_Clk_ClkS "] -source "clk_dcluster0" -divide_by "1"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster0_Cm_root] 

#warning:  uncomment below line will disabled inter-clock constraint(s) by considering them as false path(s)
#set_clock_groups -asynchronous  -group { Regime_peri }  -group { Regime_cbus_ls_west }  -group { Regime_cbus_south }  -group { Regime_cpu }  -group { Regime_dram0 }  -group { Regime_dram1 }  -group { Regime_dram2 }  -group { Regime_dram3 }  -group { Regime_cbus_south_Cm_root }  -group { Regime_pcie_Cm_root }  -group { Regime_dram3_Cm_root }  -group { Regime_pcie }  -group { Regime_cbus_Cm_center_root }  -group { Regime_cbus }  -group { Regime_dcluster1 }  -group { Regime_cbus_Cm_east_root }  -group { Regime_dram1_Cm_root }  -group { Regime_cbus_ls_east }  -group { Regime_cpu_Cm_root }  -group { Regime_dram2_Cm_root }  -group { Regime_dcluster1_Cm_root }  -group { clk_dcluster0 Regime_dcluster0_Cm_root }  -group { Regime_cbus_ls_west_Cm_root }  -group { Regime_cbus_ls_east_Cm_root }  -group { Regime_peri_Cm_root }  -group { Regime_dram0_Cm_root } 
# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_dcluster0"]
# Clock Gating Checks 

set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_2n0Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_2n0_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_2n1Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_2n1_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_2n2Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_2n2_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_2n3Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_2n3_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_aResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_a_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_astResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n03_ast_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n03Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]


