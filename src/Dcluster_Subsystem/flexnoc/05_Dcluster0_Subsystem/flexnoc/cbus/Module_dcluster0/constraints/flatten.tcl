if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n0Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n0Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n0Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n0Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n0_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n0_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n0_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n0_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n1Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n1Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n1Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n1Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n1_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n1_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n1_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n1_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n2Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n2Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n2Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n2Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n2_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n2_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n2_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n2_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n3Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n3Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n3Resp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n3Resp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n3_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n3_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_2n3_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_2n3_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n03_aResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n03_aResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n03_aResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n03_aResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n03_a_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n03_a_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n03_a_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n03_a_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n03_astResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n03_astResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n03_astResp001_main/DtpTxClkAdapt_Link_n03_asiResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n03_astResp001_main/DtpTxClkAdapt_Link_n03_asiResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n03_ast_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n03_ast_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n03_ast_main/DtpRxClkAdapt_Link_n03_asi_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n03_ast_main/DtpRxClkAdapt_Link_n03_asi_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dcluster0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dcluster0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0Resp001_main/Mux_Link_2n0Resp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0Resp001_main/Mux_Link_2n0Resp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0_main/Demux_Link_2n0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0_main/Demux_Link_2n0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_ctrl_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric_RxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric_RxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric_TxSerAdapt] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_T_main/TransportToGeneric_TxSerAdapt false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_ctrl_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03Resp001_main/Mux_Link_n03_aResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03Resp001_main/Mux_Link_n03_aResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_main/Demux_Link_n03_a] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_main/Demux_Link_n03_a false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n0Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n0Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n0Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n0Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n0_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n0_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n0_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n0_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n1Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n1Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n1Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n1Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n1_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n1_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n1_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n1_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n2Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n2Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n2Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n2Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n2_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n2_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n2_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n2_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n3Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n3Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n3Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n3Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n3_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n3_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_2n3_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_2n3_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_aResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_aResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_aResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_aResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_a_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_a_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_a_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_a_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_astResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_astResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_astResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_astResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_ast_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_ast_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_ast_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n03_ast_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_main_Sys/ClockGater false
}
