
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:04:48

####################################################################

# Gray-coded read/write count synchronization between Tx/Rx clock domain
cdc signal Link_n03_astResp001_main.DtpTxClkAdapt_Link_n03_asiResp001_Async.WrCnt -graycode -module cbus_Structure_Module_dcluster0
cdc signal Link_n03_ast_main.DtpRxClkAdapt_Link_n03_asi_Async.RdCnt -graycode -module cbus_Structure_Module_dcluster0

