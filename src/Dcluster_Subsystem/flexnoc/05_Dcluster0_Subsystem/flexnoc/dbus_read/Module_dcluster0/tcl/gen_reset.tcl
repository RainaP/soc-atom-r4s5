
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:09:45

####################################################################

arteris_gen_reset -name "Regime_dcluster0_Cm_root" -clock_domain "clk_dcluster0" -clock "Regime_dcluster0_Cm_root" -spec_domain_clock "/Regime_dcluster0/Cm/root" -clock_period "${Regime_dcluster0_Cm_root_P}" -clock_waveform "[expr ${Regime_dcluster0_Cm_root_P}*0.00] [expr ${Regime_dcluster0_Cm_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_dcluster0_Cm_main/root_Clk_RstN" -lsb "" -msb ""
arteris_gen_reset -name "Regime_dcluster0_Cm_root" -clock_domain "clk_dcluster0" -clock "Regime_dcluster0_Cm_root" -spec_domain_clock "/Regime_dcluster0/Cm/root" -clock_period "${Regime_dcluster0_Cm_root_P}" -clock_waveform "[expr ${Regime_dcluster0_Cm_root_P}*0.00] [expr ${Regime_dcluster0_Cm_root_P}*0.50]" -active_level "L" -resetRegisters "All" -resetDFFPin "Asynchronous" -pin "Regime_dcluster0_Cm_main/root_Clk_RstN" -lsb "" -msb ""
