
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:09:45

####################################################################

arteris_gen_clock -name "Regime_dcluster0_Cm_root" -pin "Regime_dcluster0_Cm_main/root_Clk Regime_dcluster0_Cm_main/root_Clk_ClkS" -clock_domain "clk_dcluster0" -spec_domain_clock "/Regime_dcluster0/Cm/root" -divide_by "1" -source "clk_dcluster0" -source_period "${clk_dcluster0_P}" -source_waveform "[expr ${clk_dcluster0_P}*0.00] [expr ${clk_dcluster0_P}*0.50]" -user_directive "" -add "FALSE"
