if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_asiResp001_main/DtpRxClkAdapt_Link_n0_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_asiResp001_main/DtpRxClkAdapt_Link_n0_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_asi_main/DtpTxClkAdapt_Link_n0_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_pResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_pResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_pResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_pResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_p_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_p_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_p_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_p_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_tResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_tResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_tResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_tResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_t_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_t_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n0_t_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n0_t_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_asiResp001_main/DtpRxClkAdapt_Link_n1_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_asiResp001_main/DtpRxClkAdapt_Link_n1_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_asi_main/DtpTxClkAdapt_Link_n1_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_tResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_tResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_tResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_tResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_t_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_t_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n1_t_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n1_t_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_asiResp001_main/DtpRxClkAdapt_Link_n2_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_asiResp001_main/DtpRxClkAdapt_Link_n2_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_asi_main/DtpTxClkAdapt_Link_n2_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_pResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_pResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_pResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_pResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_p_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_p_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_p_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_p_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_tResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_tResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_tResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_tResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_t_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_t_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n2_t_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n2_t_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_asiResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_asiResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_asiResp001_main/DtpRxClkAdapt_Link_n3_astResp001_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_asiResp001_main/DtpRxClkAdapt_Link_n3_astResp001_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_asi_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_asi_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_asi_main/DtpTxClkAdapt_Link_n3_ast_Async false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_tResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_tResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_tResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_tResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_t_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_t_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Link_n3_t_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Link_n3_t_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dcluster0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dcluster0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Link_n0_iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Link_n0_iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Link_n0_iResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Link_n0_iResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Link_n0_i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Link_n0_i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Link_n0_i_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Link_n0_i_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_cc0Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_cc0Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_cc0Resp001_main/Demux_Link_n0_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_cc0Resp001_main/Demux_Link_n0_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_cc0_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_cc0_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_cc0_main/Mux_Link_n0_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_cc0_main/Mux_Link_n0_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0iResp001_main/Demux_dcluster0_cc0_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0iResp001_main/Demux_dcluster0_cc0_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0iResp001_main/Mux_Link_n0_tResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0iResp001_main/Mux_Link_n0_tResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0iResp001_main/Mux_dnc0_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0iResp001_main/Mux_dnc0_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0i_main/Demux_Link_n0_t] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0i_main/Demux_Link_n0_t false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0i_main/Demux_dnc0_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0i_main/Demux_dnc0_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0i_main/Mux_dcluster0_cc0_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/Switch_n0i_main/Mux_dcluster0_cc0_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/DtpTxSerAdapt_Switch_cc0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/DtpTxSerAdapt_Switch_cc0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/DtpRxSerAdapt_Switch_n0i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/DtpRxSerAdapt_Switch_n0i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dcluster0_cc0_slv_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/DtpTxSerAdapt_Switch_n0i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/DtpTxSerAdapt_Switch_n0i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/DtpRxSerAdapt_Switch_n0i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/DtpRxSerAdapt_Switch_n0i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc0/dnc0_slv_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Link_n1_iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Link_n1_iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Link_n1_iResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Link_n1_iResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Link_n1_i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Link_n1_i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Link_n1_i_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Link_n1_i_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_cc1Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_cc1Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_cc1Resp001_main/Demux_Link_n1_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_cc1Resp001_main/Demux_Link_n1_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_cc1_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_cc1_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_cc1_main/Mux_Link_n1_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_cc1_main/Mux_Link_n1_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1iResp001_main/Demux_dcluster0_cc1_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1iResp001_main/Demux_dcluster0_cc1_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1iResp001_main/Mux_Link_n1_tResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1iResp001_main/Mux_Link_n1_tResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1iResp001_main/Mux_dnc1_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1iResp001_main/Mux_dnc1_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1i_main/Demux_Link_n1_t] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1i_main/Demux_Link_n1_t false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1i_main/Demux_dnc1_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1i_main/Demux_dnc1_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1i_main/Mux_dcluster0_cc1_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/Switch_n1i_main/Mux_dcluster0_cc1_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/DtpTxSerAdapt_Switch_cc1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/DtpTxSerAdapt_Switch_cc1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/DtpRxSerAdapt_Switch_n1i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/DtpRxSerAdapt_Switch_n1i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dcluster0_cc1_slv_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/DtpTxSerAdapt_Switch_n1i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/DtpTxSerAdapt_Switch_n1i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/DtpRxSerAdapt_Switch_n1i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/DtpRxSerAdapt_Switch_n1i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc1/dnc1_slv_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Link_n2_iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Link_n2_iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Link_n2_iResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Link_n2_iResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Link_n2_i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Link_n2_i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Link_n2_i_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Link_n2_i_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_cc2Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_cc2Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_cc2Resp001_main/Demux_Link_n2_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_cc2Resp001_main/Demux_Link_n2_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_cc2_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_cc2_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_cc2_main/Mux_Link_n2_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_cc2_main/Mux_Link_n2_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2iResp001_main/Demux_dcluster0_cc2_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2iResp001_main/Demux_dcluster0_cc2_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2iResp001_main/Mux_Link_n2_tResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2iResp001_main/Mux_Link_n2_tResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2iResp001_main/Mux_dnc2_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2iResp001_main/Mux_dnc2_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2i_main/Demux_Link_n2_t] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2i_main/Demux_Link_n2_t false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2i_main/Demux_dnc2_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2i_main/Demux_dnc2_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2i_main/Mux_dcluster0_cc2_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/Switch_n2i_main/Mux_dcluster0_cc2_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/DtpTxSerAdapt_Switch_cc2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/DtpTxSerAdapt_Switch_cc2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/DtpRxSerAdapt_Switch_n2i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/DtpRxSerAdapt_Switch_n2i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dcluster0_cc2_slv_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/DtpTxSerAdapt_Switch_n2i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/DtpTxSerAdapt_Switch_n2i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/DtpRxSerAdapt_Switch_n2i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/DtpRxSerAdapt_Switch_n2i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc2/dnc2_slv_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Link_n3_iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Link_n3_iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Link_n3_iResp001_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Link_n3_iResp001_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Link_n3_i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Link_n3_i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Link_n3_i_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Link_n3_i_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_cc3Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_cc3Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_cc3Resp001_main/Demux_Link_n3_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_cc3Resp001_main/Demux_Link_n3_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_cc3_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_cc3_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_cc3_main/Mux_Link_n3_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_cc3_main/Mux_Link_n3_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3iResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3iResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3iResp001_main/Demux_dcluster0_cc3_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3iResp001_main/Demux_dcluster0_cc3_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3iResp001_main/Mux_Link_n3_tResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3iResp001_main/Mux_Link_n3_tResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3iResp001_main/Mux_dnc3_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3iResp001_main/Mux_dnc3_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3i_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3i_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3i_main/Demux_Link_n3_t] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3i_main/Demux_Link_n3_t false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3i_main/Demux_dnc3_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3i_main/Demux_dnc3_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3i_main/Mux_dcluster0_cc3_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/Switch_n3i_main/Mux_dcluster0_cc3_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3iResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3iResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3iResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3iResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3i_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3i_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3i_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3i_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r_I] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r_I false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r_I/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r_I/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r_T] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r_T false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r_T/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r_T/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/DtpTxSerAdapt_Switch_cc3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/DtpTxSerAdapt_Switch_cc3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/DtpRxSerAdapt_Switch_n3i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/DtpRxSerAdapt_Switch_n3i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dcluster0_cc3_slv_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/DtpTxSerAdapt_Switch_n3i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/DtpTxSerAdapt_Switch_n3i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ia] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ia false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Id] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Id false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ip1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ip1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ip2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ip2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ip3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ip3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ir] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ir false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Irspfp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Irspfp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Is] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Is false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ist] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/GenericToTransport/Ist false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_I_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_main/SpecificToGeneric] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_r_main/SpecificToGeneric false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/DtpRxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/DtpRxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/DtpRxSerAdapt_Switch_n3i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/DtpRxSerAdapt_Switch_n3i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/DtpTxBwdFwdPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/DtpTxBwdFwdPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/GenericPipe] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/GenericPipe false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TranslationTable] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TranslationTable false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ib] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ib false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ic2ci] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ic2ci false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ica] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ica false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/If] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/If false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ifpa] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ifpa false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ip] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Ip false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Irspfpc] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Irspfpc false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Irspp] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/Irspp false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/It] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_T_main/TransportToGeneric/It false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_main/GenericToSpecific] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Module_dnc3/dnc3_slv_r_main/GenericToSpecific false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/ClockManager] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/ClockManager false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Demux_Link_n0_tResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Demux_Link_n0_tResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Demux_Link_n1_tResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Demux_Link_n1_tResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Demux_Link_n2_tResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Demux_Link_n2_tResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Demux_Link_n3_tResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Demux_Link_n3_tResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Mux_Switch_n0Resp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Mux_Switch_n0Resp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Mux_Switch_n1Resp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Mux_Switch_n1Resp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Mux_Switch_n2Resp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Mux_Switch_n2Resp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Mux_Switch_n3Resp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_tResp001_main/Mux_Switch_n3Resp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Demux_Switch_n0] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Demux_Switch_n0 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Demux_Switch_n1] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Demux_Switch_n1 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Demux_Switch_n2] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Demux_Switch_n2 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Demux_Switch_n3] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Demux_Switch_n3 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Mux_Link_n0_t] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Mux_Link_n0_t false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Mux_Link_n1_t] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Mux_Link_n1_t false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Mux_Link_n2_t] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Mux_Link_n2_t false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Mux_Link_n3_t] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n03_t_main/Mux_Link_n3_t false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n0Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n0Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n0Resp001_main/Mux_Link_n0_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n0Resp001_main/Mux_Link_n0_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n0_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n0_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n0_main/Demux_Link_n0_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n0_main/Demux_Link_n0_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n1Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n1Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n1Resp001_main/Mux_Link_n1_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n1Resp001_main/Mux_Link_n1_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n1_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n1_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n1_main/Demux_Link_n1_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n1_main/Demux_Link_n1_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n2Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n2Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n2Resp001_main/Mux_Link_n2_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n2Resp001_main/Mux_Link_n2_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n2_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n2_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n2_main/Demux_Link_n2_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n2_main/Demux_Link_n2_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n3Resp001_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n3Resp001_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n3Resp001_main/Mux_Link_n3_iResp001] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n3Resp001_main/Mux_Link_n3_iResp001 false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n3_main] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n3_main false
}
if {[get_cells ${CUSTOMER_HIERARCHY}Switch_n3_main/Demux_Link_n3_i] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}Switch_n3_main/Demux_Link_n3_i false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_pResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_pResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_pResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_pResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_p_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_p_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_p_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_p_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_tResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_tResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_tResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_tResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_t_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_t_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_t_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n0_t_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_tResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_tResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_tResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_tResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_t_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_t_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_t_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n1_t_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_pResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_pResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_pResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_pResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_p_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_p_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_p_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_p_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_tResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_tResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_tResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_tResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_t_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_t_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_t_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n2_t_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_tResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_tResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_tResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_tResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_t_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_t_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_t_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Link_n3_t_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_tResp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_tResp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_tResp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_tResp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_t_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_t_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_t_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_t_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n0Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n0Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n0Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n0Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n0_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n0_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n0_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n0_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n1Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n1Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n1Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n1Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n1_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n1_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n1_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n1_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n2Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n2Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n2Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n2Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n2_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n2_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n2_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n2_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n3Resp001_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n3Resp001_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n3Resp001_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n3Resp001_main_Sys/ClockGater false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n3_main_Sys] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n3_main_Sys false
}
if {[get_cells ${CUSTOMER_HIERARCHY}clockGaters_Switch_n3_main_Sys/ClockGater] != ""} {
set_boundary_optimization ${CUSTOMER_HIERARCHY}clockGaters_Switch_n3_main_Sys/ClockGater false
}
