
####################################################################

## Created by FlexNoC(4.5.2) script export on 2021-08-07 18:11:15

####################################################################

# Create Clocks

create_clock -name "clk_dcluster0" [get_ports "clk_dcluster0"] -period "${clk_dcluster0_P}" -waveform "[expr ${clk_dcluster0_P}*0.00] [expr ${clk_dcluster0_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks clk_dcluster0] 
# Create Virtual Clocks

create_clock -name "Regime_bus" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_cpu" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]"
create_clock -name "Regime_dcluster1" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]"
create_clock -name "Regime_dram0" -period "${Regime_dram0_P}" -waveform "[expr ${Regime_dram0_P}*0.00] [expr ${Regime_dram0_P}*0.50]"
create_clock -name "Regime_dram1" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]"
create_clock -name "Regime_dram2" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]"
create_clock -name "Regime_dram3" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]"
create_clock -name "Regime_pcie" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cpu] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster1] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram0] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram1] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram2] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram3] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_pcie] 
# Create Generated Virtual Clocks 

create_clock -name "Regime_bus_Cm_center_root" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_bus_Cm_east_root" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_bus_Cm_south_root" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_bus_Cm_west_root" -period "${Regime_bus_P}" -waveform "[expr ${Regime_bus_P}*0.00] [expr ${Regime_bus_P}*0.50]"
create_clock -name "Regime_cpu_Cm_root" -period "${Regime_cpu_P}" -waveform "[expr ${Regime_cpu_P}*0.00] [expr ${Regime_cpu_P}*0.50]"
create_clock -name "Regime_dcluster1_Cm_root" -period "${Regime_dcluster1_P}" -waveform "[expr ${Regime_dcluster1_P}*0.00] [expr ${Regime_dcluster1_P}*0.50]"
create_clock -name "Regime_dram0_Cm_root" -period "${Regime_dram0_P}" -waveform "[expr ${Regime_dram0_P}*0.00] [expr ${Regime_dram0_P}*0.50]"
create_clock -name "Regime_dram1_Cm_root" -period "${Regime_dram1_P}" -waveform "[expr ${Regime_dram1_P}*0.00] [expr ${Regime_dram1_P}*0.50]"
create_clock -name "Regime_dram2_Cm_root" -period "${Regime_dram2_P}" -waveform "[expr ${Regime_dram2_P}*0.00] [expr ${Regime_dram2_P}*0.50]"
create_clock -name "Regime_dram3_Cm_root" -period "${Regime_dram3_P}" -waveform "[expr ${Regime_dram3_P}*0.00] [expr ${Regime_dram3_P}*0.50]"
create_clock -name "Regime_pcie_Cm_root" -period "${Regime_pcie_P}" -waveform "[expr ${Regime_pcie_P}*0.00] [expr ${Regime_pcie_P}*0.50]"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_center_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_east_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_south_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_bus_Cm_west_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_cpu_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster1_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram0_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram1_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram2_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dram3_Cm_root] 
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_pcie_Cm_root] 
# Create Generated Clocks

create_generated_clock -name "Regime_dcluster0_Cm_root" [get_pins "${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/root_Clk ${CUSTOMER_HIERARCHY}Regime_dcluster0_Cm_main/root_Clk_ClkS "] -source "clk_dcluster0" -divide_by "1"
set_clock_uncertainty  ${arteris_dflt_CLOCK_UNCERTAINTY}  [get_clocks Regime_dcluster0_Cm_root] 

#warning:  uncomment below line will disabled inter-clock constraint(s) by considering them as false path(s)
#set_clock_groups -asynchronous  -group { Regime_dcluster1_Cm_root }  -group { Regime_bus_Cm_west_root }  -group { Regime_dram3_Cm_root }  -group { Regime_bus_Cm_center_root }  -group { Regime_dram2_Cm_root }  -group { clk_dcluster0 Regime_dcluster0_Cm_root }  -group { Regime_dcluster1 }  -group { Regime_cpu_Cm_root }  -group { Regime_bus_Cm_south_root }  -group { Regime_dram0_Cm_root }  -group { Regime_dram1_Cm_root }  -group { Regime_bus }  -group { Regime_bus_Cm_east_root }  -group { Regime_cpu }  -group { Regime_pcie_Cm_root }  -group { Regime_pcie }  -group { Regime_dram0 }  -group { Regime_dram1 }  -group { Regime_dram2 }  -group { Regime_dram3 } 
# Create Resets 

set_ideal_network -no_propagate [get_ports "arstn_dcluster0"]
# Clock Gating Checks 

set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_iResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Link_n0_i_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_cc0_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0iResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_Switch_n0i_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_r_I/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dcluster0_cc0_slv_r_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_r_I/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc0/clockGaters_dnc0_slv_r_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_iResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Link_n1_i_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_cc1_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1iResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_Switch_n1i_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_r_I/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dcluster0_cc1_slv_r_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_r_I/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc1/clockGaters_dnc1_slv_r_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_iResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Link_n2_i_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_cc2_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2iResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_Switch_n2i_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_r_I/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dcluster0_cc2_slv_r_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_r_I/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc2/clockGaters_dnc2_slv_r_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_iResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Link_n3_i_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_cc3_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3iResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_Switch_n3i_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_r_I/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dcluster0_cc3_slv_r_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_r_I/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}Module_dnc3/clockGaters_dnc3_slv_r_T/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asiResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_pResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_p_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_tResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n0_t_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asiResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1_tResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n1_t_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asiResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_pResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_p_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_tResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n2_t_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asiResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3_asi_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3_tResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Link_n3_t_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_tResp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n03_t_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n0Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n0_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n1Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n1_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n2Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n2_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n3Resp001_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]
set_clock_gating_check -rise -fall -setup [expr ${T_Flex2library} * 0.1] [all_fanout -endpoints_only -flat -from "${CUSTOMER_HIERARCHY}clockGaters_Switch_n3_main_Sys/ClockGater/usce77587fc1/instGaterCell/EN"]


