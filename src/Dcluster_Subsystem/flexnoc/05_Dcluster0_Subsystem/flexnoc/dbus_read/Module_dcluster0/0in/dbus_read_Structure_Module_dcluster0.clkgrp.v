
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:09:45

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_dcluster0_clkgrp;

// 0in set_cdc_clock Regime_dcluster0_Cm_main.root_Clk -module dbus_read_Structure_Module_dcluster0 -group clk_dcluster0 -period 0.667 -waveform {0.000 0.334}
// 0in set_cdc_clock Regime_dcluster0_Cm_main.root_Clk_ClkS -module dbus_read_Structure_Module_dcluster0 -group clk_dcluster0 -period 0.667 -waveform {0.000 0.334}
// 0in set_cdc_clock clk_dcluster0 -module dbus_read_Structure_Module_dcluster0 -group clk_dcluster0 -period 0.667 -waveform {0.000 0.334}

endmodule
