
////////////////////////////////////////////////////////////////////

// Created by FlexNoC(4.5.2) script export on 2021-08-07 18:09:45

////////////////////////////////////////////////////////////////////

module dbus_read_Structure_Module_dcluster0_except;

// Gray-coded read/write count synchronization between Tx/Rx clock domain
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme reconvergence -from_signals Link_n0_asiResp001_main.DtpRxClkAdapt_Link_n0_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme reconvergence -from_signals Link_n0_asi_main.DtpTxClkAdapt_Link_n0_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme reconvergence -from_signals Link_n1_asiResp001_main.DtpRxClkAdapt_Link_n1_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme reconvergence -from_signals Link_n1_asi_main.DtpTxClkAdapt_Link_n1_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme reconvergence -from_signals Link_n2_asiResp001_main.DtpRxClkAdapt_Link_n2_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme reconvergence -from_signals Link_n2_asi_main.DtpTxClkAdapt_Link_n2_ast_Async.WrCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme reconvergence -from_signals Link_n3_asiResp001_main.DtpRxClkAdapt_Link_n3_astResp001_Async.RdCnt
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme reconvergence -from_signals Link_n3_asi_main.DtpTxClkAdapt_Link_n3_ast_Async.WrCnt

// Fanin of mux selector greated than one
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme multi_sync_mux_select -from Link_n0_asiResp001_main.DtpRxClkAdapt_Link_n0_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme multi_sync_mux_select -from Link_n1_asiResp001_main.DtpRxClkAdapt_Link_n1_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme multi_sync_mux_select -from Link_n2_asiResp001_main.DtpRxClkAdapt_Link_n2_astResp001_Async.uRegSync.instSynchronizerCell*
// 0in set_cdc_report -module dbus_read_Structure_Module_dcluster0 -severity waived -scheme multi_sync_mux_select -from Link_n3_asiResp001_main.DtpRxClkAdapt_Link_n3_astResp001_Async.uRegSync.instSynchronizerCell*

endmodule
