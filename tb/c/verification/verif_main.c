// See LICENSE.SEMIFIVE for license details.
#include <unity_fixture.h>

static void RunAllTests(void)
{
	UnityGroupRunnerRun();
}

int main(int argc, const char *argv[]) {
	int exit_status = UnityMain(argc, argv, RunAllTests);
//	metal_shutdown(exit_status);
	return exit_status;
}
