/* See LICENSE.SEMIFIVE for license details. */

#ifndef __VERIFICATION_API_H__
#define __VERIFICATION_API_H__

#include <stdint.h>
#include <stdio.h>

#define __iomem

#ifdef __cplusplus
extern "C" {
#endif

uint8_t readb(const volatile void __iomem *addr);
uint16_t readw(const volatile void __iomem *addr);
uint32_t readl(const volatile void __iomem *addr);
uint64_t readq(const volatile void __iomem *addr);

void writeb(uint8_t val, volatile void __iomem *addr);
void writew(uint16_t val, volatile void __iomem *addr);
void writel(uint32_t val, volatile void __iomem *addr);
void writeq(uint64_t val, volatile void __iomem *addr);

void writeb_m(uint8_t val, volatile void __iomem *addr, uint8_t mask);
void writew_m(uint16_t val, volatile void __iomem *addr, uint16_t mask);
void writel_m(uint32_t val, volatile void __iomem *addr, uint32_t mask);
void writeq_m(uint64_t val, volatile void __iomem *addr, uint64_t mask);

void simulation_tick(uint64_t ticks);

#define SLOG_ASSERT  printf
#define SLOG_ERROR   printf
#define SLOG_INFO    printf
#define SLOG_DEBUG   printf
#define SLOG_VERBOSE printf
void print_decimal(const unsigned long value);
void print_hex(const unsigned long value);
void print_string(const char* string);
void write_to_mscratch(const uintmax_t value);


struct metal_interrupt {
  uint32_t __interrupt_index;
};
typedef void (*metal_global_interrupt_handler_t)(unsigned int id);

// controller = (struct metal_interrupt){0};
void metal_cpu_enable_interrupts(void);
void metal_cpu_disable_interrupts(void);
int  metal_interrupt_enable(struct metal_interrupt controller, unsigned int id);
int  metal_interrupt_disable(struct metal_interrupt controller, unsigned int id);
void metal_interrupt_register_handler(struct metal_interrupt controller, unsigned int id, metal_global_interrupt_handler_t handler);

metal_global_interrupt_handler_t metal_interrupt_get_handler(unsigned int id);

#ifdef __cplusplus
}
#endif

#endif //__VERIFICATION_API_H__
