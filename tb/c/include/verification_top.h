/*
    verification_top.h
	Copyright 2021 SEMIFIVE, Inc
 */
#ifndef VERIFICATION_TOP_H
#define VERIFICATION_TOP_H

#include <verification_api.h>
#include <unity_fixture.h>

#define NOT_DEFINED 0xDEADBEEFUL

// Clock Frequence ---------------------------------------------------->
#define SYSTEM_CLOCK_IN           100000000UL
#define A53_CORE_CLOCK            100000000UL
#define PERI_CLOCK                100000000UL
// Clock Frequence <----------------------------------------------------

// Memory Map --------------------------------------------------------->
/* CPU subsystem */
#define GIC_BASE_ADDR             0x1F0000000UL
#define GIC_SIZE                  4096

/* Memories */
#define GDDR_BASE_ADDR            0x000000000UL // 2 channel 1024KB interleved
#define GDDR_SIZE                 0x200000000UL // 8GB
#define GPSRAM_BASE_ADDR          NOT_DEFINED
#define GPSRAM_SIZE               NOT_DEFINED
#define SHM_BASE_ADDR             0x200000000UL // 4 channel 128byte interleved
#define SHM_SIZE                  0x2000000UL   // 32MB

/* GDDR6 */
#define GDDR0_SCU_BASE_ADDR       0x1F0900000
#define GDDR0_SCU_SIZE            4096
#define GDDR1_SCU_BASE_ADDR       0x1F0A00000
#define GDDR1_SCU_SIZE            4096
#define GDDR2_SCU_BASE_ADDR       NOT_DEFINED
#define GDDR2_SCU_SIZE            NOT_DEFINED
#define GDDR3_SCU_BASE_ADDR       NOT_DEFINED
#define GDDR3_SCU_SIZE            NOT_DEFINED
#define GDDR0_CON_BASE_ADDR       0x1F0901000
#define GDDR0_CON_SIZE            4096
#define GDDR1_CON_BASE_ADDR       0x1F0A01000
#define GDDR1_CON_SIZE            4096
#define GDDR2_CON_BASE_ADDR       NOT_DEFINED
#define GDDR2_CON_SIZE            NOT_DEFINED
#define GDDR3_CON_BASE_ADDR       NOT_DEFINED
#define GDDR3_CON_SIZE            NOT_DEFINED
#define GDDR0_PHY_BASE_ADDR       0x1F0902000
#define GDDR0_PHY_SIZE            4096
#define GDDR1_PHY_BASE_ADDR       0x1F0A02000
#define GDDR1_PHY_SIZE            4096
#define GDDR2_PHY_BASE_ADDR       NOT_DEFINED
#define GDDR2_PHY_SIZE            NOT_DEFINED
#define GDDR3_PHY_BASE_ADDR       NOT_DEFINED
#define GDDR3_PHY_SIZE            NOT_DEFINED
/* PCIe */
#define PCIE_SCU_BASE_ADDR        0x1F0100000
#define PCIE_SCU_SIZE             4096
#define PCIE_CON_BASE_ADDR        0x1F0101000
#define PCIE_CON_SIZE             4096
#define PCIE_PHY_BASE_ADDR        0x1F0102000
#define PCIE_PHY_SIZE             4096
/* Peripherals */
#define PERI_SCU_BASE_ADDR        0x1F0200000
#define PERI_SCU_SIZE             4096
#define INTG0_BASE_ADDR           0x1F0201000
#define INTG0_SIZE                4096
#define WDT0_BASE_ADDR            0x1F0202000
#define WDT0_SIZE                 4096
#define OTP_BASE_ADDR             0x1F0203000
#define OTP_SIZE                  4096
#define PVT_BASE_ADDR             0x1F0204000
#define PVT_SIZE                  4096
#define PWM0_BASE_ADDR            0x1F0205000
#define PWM0_SIZE                 4096
#define GPIO0_BASE_ADDR           0x1F0206000
#define GPIO0_SIZE                4096
#define UART0_BASE_ADDR           0x1F0207000
#define UART0_SIZE                4096
#define UART1_BASE_ADDR           0x1F0208000
#define UART1_SIZE                4096
#define I2C0_BASE_ADDR            0x1F0209000
#define I2C0_SIZE                 4096
#define I2C1_BASE_ADDR            0x1F020A000
#define I2C1_SIZE                 4096
#define I2C2_BASE_ADDR            0x1F020B000
#define I2C2_SIZE                 4096
#define I2C3_BASE_ADDR            0x1F020C000
#define I2C3_SIZE                 4096
#define SPI0_BASE_ADDR            0x1F020D000
#define SPI0_SIZE                 4096
#define SPI1_BASE_ADDR            0x1F020E000
#define SPI1_SIZE                 4096
#define SPI2_BASE_ADDR            0x1F020F000
#define SPI2_SIZE                 4096
#define SPI3_BASE_ADDR            0x1F0210000   /* SPI Slave */
#define SPI3_SIZE                 4096
// Memory Map <---------------------------------------------------------

// Interrupts --------------------------------------------------------->
#define METAL_INTC ((struct metal_interrupt){0})

typedef enum {
  INT_INTG0=0,
  INT_WDT0,
  INT_UART0,
  INT_UART1,

  INT_GDDR0_CON = 28,
  INT_GDDR0_PHY,
  INT_GDDR1_CON,
  INT_GDDR2_PHY,

  INT_PCIE_cfg_vpd_int = 32,
  INT_PCIE_cfg_link_eq_req_int,
  INT_PCIE_usp_eq_redo_executed_int,
  INT_PCIE_edma_int_0,
  INT_PCIE_edma_int_1,
  INT_PCIE_edma_int_2,
  INT_PCIE_edma_int_3,
  INT_PCIE_edma_int_4,
  INT_PCIE_edma_int_5,
  INT_PCIE_edma_int_6,
  INT_PCIE_edma_int_7,
  INT_PCIE_edma_int_8,
  INT_PCIE_edma_int_9,
  INT_PCIE_edma_int_A,
  INT_PCIE_edma_int_B,
  INT_PCIE_edma_int_C,
  INT_PCIE_edma_int_D,
  INT_PCIE_edma_int_E,
  INT_PCIE_edma_int_F,
  INT_PCIE_assert_inta_grt,
  INT_PCIE_assert_intb_grt,
  INT_PCIE_assert_intc_grt,
  INT_PCIE_assert_intd_grt,
  INT_PCIE_deassert_inta_grt,
  INT_PCIE_deassert_intb_grt,
  INT_PCIE_deassert_intc_grt,
  INT_PCIE_deassert_intd_grt,
  INT_PCIE_cfg_safety_corr,
  INT_PCIE_cfg_safety_uncorr,

  // INT_SPI0_0,
  // INT_SPI0_1,  INT_SPI0_2,  INT_SPI0_3,  INT_SPI0_4,  INT_SPI0_5,
  // INT_SPI1_0,
  // INT_SPI1_1,  INT_SPI1_2,  INT_SPI1_3,  INT_SPI1_4,  INT_SPI1_5,
  // INT_SPI2_0,
  // INT_SPI2_1,  INT_SPI2_2,  INT_SPI2_3,  INT_SPI2_4,  INT_SPI2_5,
  // INT_SPI3_0,
  // INT_SPI3_1,  INT_SPI3_2,  INT_SPI3_3,  INT_SPI3_4,
  // INT_I2C0,
  // INT_I2C1,
  // INT_I2C2,
  // INT_I2C3,
  // INT_GPIO0_0,
  // INT_GPIO0_1,  INT_GPIO0_2,  INT_GPIO0_3,  INT_GPIO0_4,  INT_GPIO0_5,
  // INT_GPIO0_6,  INT_GPIO0_7,  INT_GPIO0_8,  INT_GPIO0_9,  INT_GPIO0_10,
  // INT_GPIO0_11,  INT_GPIO0_12,  INT_GPIO0_13,  INT_GPIO0_14,  INT_GPIO0_15,
  // INT_PWM0_0,
  // INT_PWM0_1, INT_PWM0_2, INT_PWM0_3,
  // INT_GDDR0_0,
  // INT_GDDR0_1,  INT_GDDR0_2,  INT_GDDR0_3,
  // INT_GDDR1_0,
  // INT_GDDR1_1,  INT_GDDR1_2,  INT_GDDR1_3,
  // INT_GDDR2_0,
  // INT_GDDR2_1,  INT_GDDR2_2,  INT_GDDR2_3,
  // INT_GDDR3_0,
  // INT_GDDR3_1,  INT_GDDR3_2,  INT_GDDR3_3,
  // INT_PCIE_0,
  // INT_PCIE_1, INT_PCIE_2,
} interrupt_id_t;
// Interrupts <---------------------------------------------------------

// DMA Peripheral channels -------------------------------------------->
typedef enum {
  UART0_TX = 0,
  UART0_RX,
  UART1_TX, UART1_RX,
  SPI0_TX, SPI0_RX,
  SPI1_TX, SPI1_RX,
  SPI2_TX, SPI2_RX,
  SPI3_TX, SPI3_RX,
  I2C0_TX, I2C0_RX,
  I2C1_TX, I2C1_RX,
  I2C2_TX, I2C2_RX,
  I2C3_TX, I2C3_RX,
} dma_peri_ch_t;
// DMA Peripheral channels <--------------------------------------------

#endif /* VERIFICATION_TOP_H */
