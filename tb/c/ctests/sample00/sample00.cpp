#include "verification_top.h"

static volatile int interrupt_callcount=0;

extern "C" void semifive_templatewrapper_0_1_0_irq_handler(unsigned int id) {
	SLOG_DEBUG( "Interrupt occurred\n" );
	writel(0xFFFFFFFF, (void*)(INTG0_BASE_ADDR+4));
	interrupt_callcount++;
}

TEST_GROUP(SAMPLE_TEST1);

TEST_SETUP(SAMPLE_TEST1)
{
    // executed before each test
}

TEST_TEAR_DOWN(SAMPLE_TEST1)
{
    // executed after each test
}

TEST(SAMPLE_TEST1, ReadWriteSampleGICReg)
{
	writel( 0xFFFFFFFF, (void*)(GIC_BASE_ADDR) );
	TEST_ASSERT_EQUAL(0xFFFFFFFF, readl((void*)GIC_BASE_ADDR));
	writel( 0xDEADC0DE, (void*)(GIC_BASE_ADDR + 0x10) );
	TEST_ASSERT_EQUAL(~0xDEADC0DE, readl((void*)GIC_BASE_ADDR));
}

class RandomNumberGenerator
{
public:
	RandomNumberGenerator( uint32_t seed=123456789 ) : randomSeed(seed){}
	void     set( uint32_t seed ){ randomSeed = seed; }
	uint32_t get( void ) { return randomSeed; }
	uint32_t inc( void ) {
		randomSeed = MULTIPLIER * (randomSeed % Q) - R * (randomSeed / Q);
		return randomSeed;
	}
private:
	enum {
		 MODULUS    = 2147483647 // DON'T CHANGE THIS VALUE
		,MULTIPLIER = 48271      // DON'T CHANGE THIS VALUE
		,Q          = (MODULUS / MULTIPLIER)
		,R          = (MODULUS % MULTIPLIER)
	};
	uint32_t randomSeed;
};

TEST(SAMPLE_TEST1, ReadWriteSampleGDDR6Access)
{
	uint32_t checksum_golden = 0xDEADC0DE;
	uint32_t checksum_dump   = 0xDEADC0DE;
	RandomNumberGenerator r;
	writel(r.get(), (void*)(GDDR_BASE_ADDR) );
	TEST_ASSERT_EQUAL(r.get(), readl((void*)GDDR_BASE_ADDR));
	for( int i=0; i<1024; i++ ) {
		writel(r.get(), (void*)(GDDR_BASE_ADDR + i*4) );
		checksum_golden += r.get();
		r.inc();
	}
	for( int i=0; i<1024; i++ ) {
		checksum_dump += readl( (void*)(GDDR_BASE_ADDR + i*4) );
	}
	TEST_ASSERT_EQUAL(checksum_golden, checksum_dump);
    SLOG_DEBUG( "SHM checksum : %08x %08x \n", checksum_golden, checksum_dump );
}

TEST(SAMPLE_TEST1, ReadWriteSampleSHMAccess)
{
	uint32_t checksum_golden = 0xDEADC0DE;
	uint32_t checksum_dump   = 0xDEADC0DE;
	RandomNumberGenerator r;
	writel(r.get(), (void*)(SHM_BASE_ADDR) );
	TEST_ASSERT_EQUAL(r.get(), readl((void*)SHM_BASE_ADDR));
	for( int i=0; i<1024; i++ ) {
		writel(r.get(), (void*)(SHM_BASE_ADDR + i*4) );
		checksum_golden += r.get();
		r.inc();
	}
	for( int i=0; i<1024; i++ ) {
		checksum_dump += readl( (void*)(SHM_BASE_ADDR + i*4) );
	}
	TEST_ASSERT_EQUAL(checksum_golden, checksum_dump);
    SLOG_DEBUG( "SHM checksum : %08x %08x \n", checksum_golden, checksum_dump );
}

TEST(SAMPLE_TEST1, BasicInterrupt)
{
	interrupt_callcount=0;
    metal_interrupt_register_handler(METAL_INTC,INT_INTG0,semifive_templatewrapper_0_1_0_irq_handler);
	metal_interrupt_enable(METAL_INTC,INT_INTG0);
    metal_cpu_enable_interrupts();
	writel(1, (void*)(INTG0_BASE_ADDR));
	while(0==interrupt_callcount)	{
		simulation_tick(5);
		SLOG_DEBUG( "Wait for interrupt\n" );
	}
    metal_cpu_disable_interrupts();
	metal_interrupt_disable(METAL_INTC,INT_INTG0);
	TEST_ASSERT_EQUAL(0, readl((void*)(INTG0_BASE_ADDR)));
}

TEST_GROUP_RUNNER(SAMPLE_TEST1)
{
	RUN_TEST_CASE(SAMPLE_TEST1, ReadWriteSampleGICReg);
	RUN_TEST_CASE(SAMPLE_TEST1, ReadWriteSampleGDDR6Access);
	RUN_TEST_CASE(SAMPLE_TEST1, ReadWriteSampleSHMAccess);
	RUN_TEST_CASE(SAMPLE_TEST1, BasicInterrupt);
}

TEST_GROUP_CONSTRUCTOR(SAMPLE_TEST1) { REGISTER_TEST_GROUP(SAMPLE_TEST1); }
