// See LICENSE.SEMIFIVE for license details.

module semifive_apb_dummy
(
	 input                   CLK__APB
	,input                   RSTN__APB

    ,input                   APB__PENABLE
    ,input                   APB__PSEL
    ,output                  APB__PREADY
    ,output                  APB__PSLVERR
    ,input                   APB__PWRITE
    ,input  [ 2:0]           APB__PPROT
    ,input  [11:0]           APB__PADDR
    ,input  [ 3:0]           APB__PSTRB
    ,input  [31:0]           APB__PWDATA
    ,output [31:0]           APB__PRDATA
);
    wire       APB_RE  = APB__PSEL && APB__PENABLE && (0==APB__PWRITE);
    wire       APB_WE  = APB__PSEL && APB__PENABLE && (1==APB__PWRITE);
    reg [31:0] rPRDATA  ;

    assign APB__PREADY  = 1;
    assign APB__PSLVERR = 0;
    assign APB__PRDATA = rPRDATA;

	//--------------------------------------------------------------------
	reg[31:0] MESSAGE_0;
	reg[31:0] MESSAGE_1;
	reg[31:0] MESSAGE_2;
	reg[31:0] MESSAGE_3;
	reg[31:0] MESSAGE_4;
	reg[31:0] MESSAGE_5;
	reg[31:0] MESSAGE_6;
	reg[31:0] MESSAGE_7;
	reg[31:0] MESSAGE_8;
	reg[31:0] MESSAGE_9;
	reg[31:0] MESSAGE_A;
	reg[31:0] MESSAGE_B;
	reg[31:0] MESSAGE_C;
	reg[31:0] MESSAGE_D;
	reg[31:0] MESSAGE_E;
	reg[31:0] MESSAGE_F;

`define REG_IMP(name, address) \
	always @(posedge CLK__APB or negedge RSTN__APB) \
		if (~RSTN__APB)                                                       name[7+b*8:b*8] <= 0; \
		else if(APB_WE && ({APB__PADDR[7:2],2'b0}==address) && APB__PSTRB[b]) name[7+b*8:b*8] <= APB__PWDATA[7+b*8:b*8] ;

	genvar b;
	generate
    for( b=0; b<4; b=b+1 )
    begin : PSTRB
		`REG_IMP( MESSAGE_0, 'h00 )
		`REG_IMP( MESSAGE_1, 'h04 )
		`REG_IMP( MESSAGE_2, 'h08 )
		`REG_IMP( MESSAGE_3, 'h0C )
		`REG_IMP( MESSAGE_4, 'h10 )
		`REG_IMP( MESSAGE_5, 'h14 )
		`REG_IMP( MESSAGE_6, 'h18 )
		`REG_IMP( MESSAGE_7, 'h1C )
		`REG_IMP( MESSAGE_8, 'h20 )
		`REG_IMP( MESSAGE_9, 'h24 )
		`REG_IMP( MESSAGE_A, 'h28 )
		`REG_IMP( MESSAGE_B, 'h2C )
		`REG_IMP( MESSAGE_C, 'h30 )
		`REG_IMP( MESSAGE_D, 'h34 )
		`REG_IMP( MESSAGE_E, 'h38 )
		`REG_IMP( MESSAGE_F, 'h3C )
    end
    endgenerate

	//--------------------------------------------------------------------
	always @( * )
		case (APB__PADDR)
		'h00:    rPRDATA = MESSAGE_0 ;
		'h04:    rPRDATA = MESSAGE_1 ;
		'h08:    rPRDATA = MESSAGE_2 ;
		'h0C:    rPRDATA = MESSAGE_3 ;
		'h10:    rPRDATA = MESSAGE_4 ;
		'h14:    rPRDATA = MESSAGE_5 ;
		'h18:    rPRDATA = MESSAGE_6 ;
		'h1C:    rPRDATA = MESSAGE_7 ;
		'h20:    rPRDATA = MESSAGE_8 ;
		'h24:    rPRDATA = MESSAGE_9 ;
		'h28:    rPRDATA = MESSAGE_A ;
		'h2C:    rPRDATA = MESSAGE_B ;
		'h30:    rPRDATA = MESSAGE_C ;
		'h34:    rPRDATA = MESSAGE_D ;
		'h38:    rPRDATA = MESSAGE_E ;
		'h3C:    rPRDATA = MESSAGE_F ;
		default: rPRDATA = 'hDEAD_C0DE;
		endcase
endmodule

