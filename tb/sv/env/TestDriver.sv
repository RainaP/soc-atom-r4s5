// See LICENSE.SEMIFIVE for license details.

timeunit 1ns/1ns;

module TestDriver ();
  localparam HZ = 100000000;

  bit clock = 1'b0;
  bit reset = 1'b1;

  always  #(1000000000.0/(2.0*HZ)) clock = !clock;
  initial begin
      reset=1;
      repeat(50) begin @(posedge clock); end
      reset=0;
  end

  initial begin
      #500000000;
      $display("\033[31m*E:%s(%d): !!! TIME OUT !!! \033[0m", `__FILE__,`__LINE__);
      $finish;
  end
  initial begin
      $fsdbDumpfile("novas.fsdb");
      $fsdbDumpvars(0,TestDriver);
  end

	ATOM_top
	ATOM_top (
		 .TEST_MODE        (1'h0)
		,.CLK__TEMP        (clock)
		,.RSTN__TEMP       (~reset)
		,.TIMER_0__IO__PWM1()
		,.TIMER_0__IO__PWM2()
		,.TIMER_0__IO__PWM3()
		,.TIMER_0__IO__PWM4()
		,.GPIO_0__IO__A_A  (16'h0)
		,.GPIO_0__IO__B_A  (16'h0)
		,.GPIO_0__IO__C_A  (16'h0)
		,.GPIO_0__IO__D_A  (16'h0)
		,.GPIO_0__IO__A_Y  ()
		,.GPIO_0__IO__B_Y  ()
		,.GPIO_0__IO__C_Y  ()
		,.GPIO_0__IO__D_Y  ()
		,.GPIO_0__IO__A_EN ()
		,.GPIO_0__IO__B_EN ()
		,.GPIO_0__IO__C_EN ()
		,.GPIO_0__IO__D_EN ()
		,.UART_0__IO__RX   (1'h0)
		,.UART_0__IO__TX   ()
		,.UART_1__IO__RX   (1'h0)
		,.UART_1__IO__TX   ()
		,.I2C_0__IO__SCL   (1'h0)
		,.I2C_0__IO__SDA   (1'h0)
		,.I2C_1__IO__SCL   (1'h0)
		,.I2C_1__IO__SDA   (1'h0)
		,.I2C_2__IO__SCL   (1'h0)
		,.I2C_2__IO__SDA   (1'h0)
		,.I2C_3__IO__SCL   (1'h0)
		,.I2C_3__IO__SDA   (1'h0)
		,.SPI_0__IO__CLK   ()
		,.SPI_0__IO__CS    ()
		,.SPI_0__IO__DAT_A (8'h0)
		,.SPI_0__IO__DAT_Y ()
		,.SPI_0__IO__DAT_EN()
		,.SPI_1__IO__CLK   ()
		,.SPI_1__IO__CS    ()
		,.SPI_1__IO__DAT_A (8'h0)
		,.SPI_1__IO__DAT_Y ()
		,.SPI_1__IO__DAT_EN()
		,.SPI_2__IO__CLK   ()
		,.SPI_2__IO__CS    ()
		,.SPI_2__IO__DAT_A (8'h0)
		,.SPI_2__IO__DAT_Y ()
		,.SPI_2__IO__DAT_EN()
		,.SPI_3__IO__CLK   ()
		,.SPI_3__IO__CS    ()
		,.SPI_3__IO__DAT_A (8'h0)
		,.SPI_3__IO__DAT_Y ()
		,.SPI_3__IO__DAT_EN()
	);

	//	sanity test

    task testRead( input longint address, output logic [31:0] data );
		@(posedge clock); #1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARID    = 0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARADDR  = address;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARLEN   = 0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARSIZE  = 2;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARBURST = 1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARLOCK  = 0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARCACHE = 0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARPROT  = 0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARUSER  = 0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARVALID = 1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__RREADY  = 1;
		@(posedge clock);
		while( 0==TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARREADY ) begin
			@(posedge clock);
		end
		#1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__ARVALID = 0;
		@(posedge clock);
		while( 0==TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__RVALID ) begin
			@(posedge clock);
		end
		case( address[3:2] )
		2'b00: data = TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__RDATA[31+0*32:0*32];
		2'b01: data = TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__RDATA[31+1*32:1*32];
		2'b10: data = TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__RDATA[31+2*32:2*32];
		2'b11: data = TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__RDATA[31+3*32:3*32];
		endcase
		if( 0!=TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__RRESP ) begin
			$display( "*E: testRead RRESP(%d) : address(%x) data(%d)", TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__RRESP,address,data );
		end
    endtask

    task testWrite( input longint address, input logic [31:0]  data );
		@(posedge clock); #1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWID    =0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWADDR  =address;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWLEN   =0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWSIZE  =2;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWBURST =1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWLOCK  =0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWCACHE =0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWPROT  =0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWUSER  =0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWVALID =1;
		@(posedge clock);
		while( 0==TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWREADY ) begin
			@(posedge clock);
		end
		#1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__AWVALID =0;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WVALID  =1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WLAST   =1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WDATA   ={4{data}};
		case( address[3:2] )
		2'b00: force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WSTRB = 'h000F;
		2'b01: force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WSTRB = 'h00F0;
		2'b10: force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WSTRB = 'h0F00;
		2'b11: force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WSTRB = 'hF000;
		endcase
		@(posedge clock);
		while( 0==TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WREADY ) begin
			@(posedge clock);
		end
		#1;
		force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__WVALID  =0;
		@(posedge clock);
		while( 0==TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__BVALID ) begin
			@(posedge clock);
		end
		if( 0!=TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__BRESP ) begin
			$display( "*E: testWrite BRESP(%d) : address(%x) data(%d)", TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.CPU_0.AXI4__BUS__BRESP,address,data );
		end
    endtask


	localparam TESTADDRESS_DRAM0_DATA = 'h00_0000_0000;
	localparam TESTADDRESS_DRAM1_DATA = 'h00_0000_0400;
	localparam TESTADDRESS_DRAM2_DATA = 'h00_0000_0800;
	localparam TESTADDRESS_DRAM3_DATA = 'h00_0000_0C00;
	localparam TESTADDRESS_SHM0_DATA  = 'h1F_E400_0000;
	localparam TESTADDRESS_SHM1_DATA  = 'h1F_E400_0080;
	localparam TESTADDRESS_SHM2_DATA  = 'h1F_E400_0100;
	localparam TESTADDRESS_SHM3_DATA  = 'h1F_E400_0180;

	localparam TESTADDRESS_CP_SCU_0     = 'h1F_F400_0000;
	localparam TESTADDRESS_CP_INTC_0    = 'h1F_F400_1000;

	localparam TESTADDRESS_DRAM0_SCU_0  = 'h1F_F430_0000;
	//localparam TESTADDRESS_DRAM0_CON_0  = !!!
	//localparam TESTADDRESS_DRAM0_PHY_0  = !!!
	localparam TESTADDRESS_DRAM1_SCU_0  = 'h1F_F430_1000;
	//localparam TESTADDRESS_DRAM1_CON_0  = !!!
	//localparam TESTADDRESS_DRAM1_PHY_0  = !!!
	localparam TESTADDRESS_DRAM2_SCU_0  = 'h1F_F430_2000;
	//localparam TESTADDRESS_DRAM2_CON_0  = !!!
	//localparam TESTADDRESS_DRAM2_PHY_0  = !!!
	localparam TESTADDRESS_DRAM3_SCU_0  = 'h1F_F430_3000;
	//localparam TESTADDRESS_DRAM3_CON_0  = !!!
	//localparam TESTADDRESS_DRAM3_PHY_0  = !!!
	localparam TESTADDRESS_PCIE_SCU_0   = 'h1F_F440_0000;
	localparam TESTADDRESS_PCIE_CON_0   = 'h1F_F440_1000;
	localparam TESTADDRESS_PCIE_EPCSR_0 = 'h1F_F440_2000;
	localparam TESTADDRESS_PCIE_UPCS_0  = 'h1F_F440_3000;
	localparam TESTADDRESS_PERI_SCU_0   = 'h1F_F450_0000;
	localparam TESTADDRESS_PERI_MBOX_0  = 'h1F_F450_1000;
	localparam TESTADDRESS_PERI_WDT_0   = 'h1F_F450_2000;
	localparam TESTADDRESS_PERI_OTP_0   = 'h1F_F450_3000;
	localparam TESTADDRESS_PERI_PVT_0   = 'h1F_F450_4000;
	localparam TESTADDRESS_PERI_PWM_0   = 'h1F_F450_5000;
	localparam TESTADDRESS_PERI_GPIO_0  = 'h1F_F450_6000;
	localparam TESTADDRESS_PERI_UART_0  = 'h1F_F450_7000;
	localparam TESTADDRESS_PERI_UART_1  = 'h1F_F450_8000;
	localparam TESTADDRESS_PERI_I2C_0   = 'h1F_F450_9000;
	localparam TESTADDRESS_PERI_I2C_1   = 'h1F_F450_A000;
	localparam TESTADDRESS_PERI_I2C_2   = 'h1F_F450_B000;
	localparam TESTADDRESS_PERI_I2C_3   = 'h1F_F450_C000;
	localparam TESTADDRESS_PERI_SPI_0   = 'h1F_F450_D000;
	localparam TESTADDRESS_PERI_SPI_1   = 'h1F_F450_E000;
	localparam TESTADDRESS_PERI_SPI_2   = 'h1F_F450_F000;
	localparam TESTADDRESS_PERI_SPI_3   = 'h1F_F451_0000;

	reg[31:0] testReadData;
	integer   testErrorCount;

	initial begin
		testErrorCount = 0;
		//force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.INTC_0.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.SCU_0.APB__PREADY       = 1; //  "DRAM0_SCU_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.DRAMCON_0.APB__PREADY   = 1; //  "DRAM0_CON_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.DRAMPHY_0.APB__PREADY   = 1; //  "DRAM0_PHY_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.SCU_0.APB__PREADY       = 1; //  "DRAM1_SCU_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.DRAMCON_0.APB__PREADY   = 1; //  "DRAM1_CON_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.DRAMPHY_0.APB__PREADY   = 1; //  "DRAM1_PHY_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.SCU_0.APB__PREADY       = 1; //  "DRAM2_SCU_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.DRAMCON_0.APB__PREADY   = 1; //  "DRAM2_CON_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.DRAMPHY_0.APB__PREADY   = 1; //  "DRAM2_PHY_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.SCU_0.APB__PREADY       = 1; //  "DRAM3_SCU_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.DRAMCON_0.APB__PREADY   = 1; //  "DRAM3_CON_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.DRAMPHY_0.APB__PREADY   = 1; //  "DRAM3_PHY_0 "
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SCU_0.APB__PREADY        = 1; //  "PCIE_SCU_0  "
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.PCIECON_0.APB__PREADY    = 1; //  "PCIE_CON_0  "
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.PCIEEPCSR_0.APB__PREADY  = 1; //  "PCIE_EPCSR_0"
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.PCIEUPCS_0.APB__PREADY   = 1; //  "PCIE_UPCS_0 "
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SCU_0.APB__PRDATA   = 1; //  "PERI_SCU_0  "
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.MBOX_0.APB__PREADY  = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.WDT_0.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.OTP_0.PREADY        = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.PVT_0.PREADY        = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.TIMER_0.APB__PREADY = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.APB__PREADY  = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.UART_0.APB__PREADY  = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.UART_1.APB__PREADY  = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_0.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_1.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_2.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_3.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_0.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_1.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_2.APB__PREADY   = 1;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_3.APB__PREADY   = 1;

		//force TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.INTC_0.APB__PRDATA   =
		force TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.SCU_0.APB__PRDATA       = 'hC0DE_A00A; //  "DRAM0_SCU_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.DRAMCON_0.APB__PRDATA   = 'hC0DE_A00B; //  "DRAM0_CON_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.DRAMPHY_0.APB__PRDATA   = 'hC0DE_A00C; //  "DRAM0_PHY_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.SCU_0.APB__PRDATA       = 'hC0DE_A00D; //  "DRAM1_SCU_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.DRAMCON_0.APB__PRDATA   = 'hC0DE_A00E; //  "DRAM1_CON_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.DRAMPHY_0.APB__PRDATA   = 'hC0DE_A00F; //  "DRAM1_PHY_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.SCU_0.APB__PRDATA       = 'hC0DE_A010; //  "DRAM2_SCU_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.DRAMCON_0.APB__PRDATA   = 'hC0DE_A011; //  "DRAM2_CON_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.DRAMPHY_0.APB__PRDATA   = 'hC0DE_A012; //  "DRAM2_PHY_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.SCU_0.APB__PRDATA       = 'hC0DE_A013; //  "DRAM3_SCU_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.DRAMCON_0.APB__PRDATA   = 'hC0DE_A014; //  "DRAM3_CON_0 "
		force TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.DRAMPHY_0.APB__PRDATA   = 'hC0DE_A015; //  "DRAM3_PHY_0 "
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SCU_0.APB__PRDATA        = 'hC0DE_A016; //  "PCIE_SCU_0  "
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.PCIECON_0.APB__PRDATA    = 'hC0DE_A017; //  "PCIE_CON_0  "
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.PCIEEPCSR_0.APB__PRDATA  = 'hC0DE_A018; //  "PCIE_EPCSR_0"
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.PCIEUPCS_0.APB__PRDATA   = 'hC0DE_A019; //  "PCIE_UPCS_0 "
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SCU_0.APB__PRDATA   = 'hC0DE_A01A; //  "PERI_SCU_0  "
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.MBOX_0.APB__PRDATA  = 'hC0DE_A01B;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.WDT_0.APB__PRDATA   = 'hC0DE_A01C;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.OTP_0.PRDATA        = 'hC0DE_A01D;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.PVT_0.PRDATA        = 'hC0DE_A01E;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.TIMER_0.APB__PRDATA = 'hC0DE_A01F;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.APB__PRDATA  = 'hC0DE_A020;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.UART_0.APB__PRDATA  = 'hC0DE_A021;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.UART_1.APB__PRDATA  = 'hC0DE_A022;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_0.APB__PRDATA   = 'hC0DE_A023;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_1.APB__PRDATA   = 'hC0DE_A024;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_2.APB__PRDATA   = 'hC0DE_A025;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_3.APB__PRDATA   = 'hC0DE_A026;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_0.APB__PRDATA   = 'hC0DE_A027;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_1.APB__PRDATA   = 'hC0DE_A028;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_2.APB__PRDATA   = 'hC0DE_A029;
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_3.APB__PRDATA   = 'hC0DE_A02A;

		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.MBOX_0.IRQ = 0; // 0
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.WDT_0.IRQ = 0;  // 1
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.UART_0.IRQ = 0; // 2
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.UART_1.IRQ = 0; // 3
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.TIMER_0.IRQ = 0; // 4~7
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_0.IRQ = 0;   // 8
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_1.IRQ = 0;   // 9
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_2.IRQ = 0;   //10
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_3.IRQ = 0;   //11
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_0.IRQ = 0;   //12
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_1.IRQ = 0;   //13
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_2.IRQ = 0;   //14
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_3.IRQ = 0;   //15
		force TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ = 0;  //16~31
		force TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.IRQ__controller_int = 0;  //64
		force TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.IRQ__phy_pi_int     = 0;  //65
		force TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.IRQ__controller_int = 0;  //66
		force TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.IRQ__phy_pi_int     = 0;  //67
		force TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.IRQ__controller_int = 0;  //68
		force TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.IRQ__phy_pi_int     = 0;  //69
		force TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.IRQ__controller_int = 0;  //70
		force TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.IRQ__phy_pi_int     = 0;  //71
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__cfg_vpd_int              = 0; // 72
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__cfg_link_eq_req_int      = 0; // 73
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__usp_eq_redo_executed_int = 0; // 74
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int                 = 0; // 75-90
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__assert_inta_grt          = 0; // 91
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__assert_intb_grt          = 0; // 92
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__assert_intc_grt          = 0; // 93
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__assert_intd_grt          = 0; // 94
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__deassert_inta_grt        = 0; // 95
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__deassert_intb_grt        = 0; // 96
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__deassert_intc_grt        = 0; // 97
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__deassert_intd_grt        = 0; // 98
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__cfg_safety_corr          = 0; // 99
		force TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__cfg_safety_uncorr        = 0; //100

`define READTEST( address, name, golden )	\
		testRead( address, testReadData );  \
		if( golden === testReadData ) begin \
			$display( "msg: PASSED : %s : %x", name, testReadData ); \
		end else begin \
			$display( "\033[31mmsg: FAILED : %s : expected(%x) observed(%x)\033[0m", name, golden, testReadData ); \
			testErrorCount = testErrorCount+1; \
		end

`define IRQTEST( name, number, path )	\
		force path = 1; @(posedge clock); \
		if( TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.INTC_0.IRQ__SRC === (1<<number) ) begin \
			$display( "msg: PASSED : %s : %d", name, number ); \
		end else begin \
			$display( "\033[31mmsg: FAILED : %s : %d  expected(%x) observed(%x)\033[0m", name, number, 1<<number, TestDriver.ATOM_top.ATOM_design.u_cpu_subsys.INTC_0.IRQ__SRC ); \
			testErrorCount = testErrorCount+1; \
		end \
		force path = 0; @(posedge clock);

		wait( ~reset );
		repeat(10) @(posedge clock);
		$display( "ATOM sanity test" );

		// $display( "ATOM data bus read test" );
		//`READTEST( TESTADDRESS_DRAM0_DATA   , "DRAM0_DATA  ", 'hC0DE_A000 );
		//`READTEST( TESTADDRESS_DRAM1_DATA   , "DRAM1_DATA  ", 'hC0DE_A001 );
		//`READTEST( TESTADDRESS_DRAM2_DATA   , "DRAM2_DATA  ", 'hC0DE_A002 );
		//`READTEST( TESTADDRESS_DRAM3_DATA   , "DRAM3_DATA  ", 'hC0DE_A003 );
		//`READTEST( TESTADDRESS_SHM0_DATA    , "SHM0_DATA   ", 'hC0DE_A004 );
		//`READTEST( TESTADDRESS_SHM1_DATA    , "SHM1_DATA   ", 'hC0DE_A005 );
		//`READTEST( TESTADDRESS_SHM2_DATA    , "SHM2_DATA   ", 'hC0DE_A006 );
		//`READTEST( TESTADDRESS_SHM3_DATA    , "SHM3_DATA   ", 'hC0DE_A007 );

		$display( "ATOM control bus read test" );
		//`READTEST( TESTADDRESS_CP_SCU_0     , "CP_SCU_0    ", 'hC0DE_A008 );
		//`READTEST( TESTADDRESS_CP_INTC_0    , "CP_INTC_0   ", 'hC0DE_A009 );
		`READTEST( TESTADDRESS_DRAM0_SCU_0  , "DRAM0_SCU_0 ", 'hC0DE_A00A );
		//`READTEST( TESTADDRESS_DRAM0_CON_0  , "DRAM0_CON_0 ", 'hC0DE_A00B );
		//`READTEST( TESTADDRESS_DRAM0_PHY_0  , "DRAM0_PHY_0 ", 'hC0DE_A00C );
		`READTEST( TESTADDRESS_DRAM1_SCU_0  , "DRAM1_SCU_0 ", 'hC0DE_A00D );
		//`READTEST( TESTADDRESS_DRAM1_CON_0  , "DRAM1_CON_0 ", 'hC0DE_A00E );
		//`READTEST( TESTADDRESS_DRAM1_PHY_0  , "DRAM1_PHY_0 ", 'hC0DE_A00F );
		`READTEST( TESTADDRESS_DRAM2_SCU_0  , "DRAM2_SCU_0 ", 'hC0DE_A010 );
		//`READTEST( TESTADDRESS_DRAM2_CON_0  , "DRAM2_CON_0 ", 'hC0DE_A011 );
		//`READTEST( TESTADDRESS_DRAM2_PHY_0  , "DRAM2_PHY_0 ", 'hC0DE_A012 );
		`READTEST( TESTADDRESS_DRAM3_SCU_0  , "DRAM3_SCU_0 ", 'hC0DE_A013 );
		//`READTEST( TESTADDRESS_DRAM3_CON_0  , "DRAM3_CON_0 ", 'hC0DE_A014 );
		//`READTEST( TESTADDRESS_DRAM3_PHY_0  , "DRAM3_PHY_0 ", 'hC0DE_A015 );
		`READTEST( TESTADDRESS_PCIE_SCU_0   , "PCIE_SCU_0  ", 'hC0DE_A016 );
		`READTEST( TESTADDRESS_PCIE_CON_0   , "PCIE_CON_0  ", 'hC0DE_A017 );
		`READTEST( TESTADDRESS_PCIE_EPCSR_0 , "PCIE_EPCSR_0", 'hC0DE_A018 );
		`READTEST( TESTADDRESS_PCIE_UPCS_0  , "PCIE_UPCS_0 ", 'hC0DE_A019 );
		`READTEST( TESTADDRESS_PERI_SCU_0   , "PERI_SCU_0  ", 'hC0DE_A01A );
		`READTEST( TESTADDRESS_PERI_MBOX_0  , "PERI_MBOX_0 ", 'hC0DE_A01B );
		`READTEST( TESTADDRESS_PERI_WDT_0   , "PERI_WDT_0  ", 'hC0DE_A01C );
		`READTEST( TESTADDRESS_PERI_OTP_0   , "PERI_OTP_0  ", 'hC0DE_A01D );
		`READTEST( TESTADDRESS_PERI_PVT_0   , "PERI_PVT_0  ", 'hC0DE_A01E );
		`READTEST( TESTADDRESS_PERI_PWM_0   , "PERI_PWM_0  ", 'hC0DE_A01F );
		`READTEST( TESTADDRESS_PERI_GPIO_0  , "PERI_GPIO_0 ", 'hC0DE_A020 );
		`READTEST( TESTADDRESS_PERI_UART_0  , "PERI_UART_0 ", 'hC0DE_A021 );
		`READTEST( TESTADDRESS_PERI_UART_1  , "PERI_UART_1 ", 'hC0DE_A022 );
		`READTEST( TESTADDRESS_PERI_I2C_0   , "PERI_I2C_0  ", 'hC0DE_A023 );
		`READTEST( TESTADDRESS_PERI_I2C_1   , "PERI_I2C_1  ", 'hC0DE_A024 );
		`READTEST( TESTADDRESS_PERI_I2C_2   , "PERI_I2C_2  ", 'hC0DE_A025 );
		`READTEST( TESTADDRESS_PERI_I2C_3   , "PERI_I2C_3  ", 'hC0DE_A026 );
		`READTEST( TESTADDRESS_PERI_SPI_0   , "PERI_SPI_0  ", 'hC0DE_A027 );
		`READTEST( TESTADDRESS_PERI_SPI_1   , "PERI_SPI_1  ", 'hC0DE_A028 );
		`READTEST( TESTADDRESS_PERI_SPI_2   , "PERI_SPI_2  ", 'hC0DE_A029 );
		//`READTEST( TESTADDRESS_PERI_SPI_3   , "PERI_SPI_3  ", 'hC0DE_A02A );

		@(posedge clock);

		$display( "ATOM interrupt connection test" );
		`IRQTEST( "INT_MBOX0"     ,  0, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.MBOX_0.IRQ  );
		`IRQTEST( "INT_WDT0 "     ,  1, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.WDT_0.IRQ   );
		`IRQTEST( "INT_UART0"     ,  2, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.UART_0.IRQ  );
		`IRQTEST( "INT_UART1"     ,  3, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.UART_1.IRQ  );
		`IRQTEST( "INT_TIMER0_0"  ,  4, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.TIMER_0.IRQ[0] );
		`IRQTEST( "INT_TIMER0_1"  ,  5, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.TIMER_0.IRQ[1] );
		`IRQTEST( "INT_TIMER0_2"  ,  6, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.TIMER_0.IRQ[2] );
		`IRQTEST( "INT_TIMER0_3"  ,  7, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.TIMER_0.IRQ[3] );
		`IRQTEST( "INT_I2C0"      ,  8, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_0.IRQ );
		`IRQTEST( "INT_I2C1"      ,  9, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_1.IRQ );
		`IRQTEST( "INT_I2C2"      , 10, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_2.IRQ );
		`IRQTEST( "INT_I2C3"      , 11, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.I2C_3.IRQ );
		`IRQTEST( "INT_SPI0"      , 12, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_0.IRQ );
		`IRQTEST( "INT_SPI1"      , 13, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_1.IRQ );
		`IRQTEST( "INT_SPI2"      , 14, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_2.IRQ );
		`IRQTEST( "INT_SPI3"      , 15, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.SPI_3.IRQ );
		`IRQTEST( "INT_GPIO0_00"  , 16, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 0] );
		`IRQTEST( "INT_GPIO0_01"  , 17, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 1] );
		`IRQTEST( "INT_GPIO0_02"  , 18, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 2] );
		`IRQTEST( "INT_GPIO0_03"  , 19, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 3] );
		`IRQTEST( "INT_GPIO0_04"  , 20, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 4] );
		`IRQTEST( "INT_GPIO0_05"  , 21, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 5] );
		`IRQTEST( "INT_GPIO0_06"  , 22, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 6] );
		`IRQTEST( "INT_GPIO0_07"  , 23, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 7] );
		`IRQTEST( "INT_GPIO0_08"  , 24, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 8] );
		`IRQTEST( "INT_GPIO0_09"  , 25, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[ 9] );
		`IRQTEST( "INT_GPIO0_10"  , 26, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[10] );
		`IRQTEST( "INT_GPIO0_11"  , 27, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[11] );
		`IRQTEST( "INT_GPIO0_12"  , 28, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[12] );
		`IRQTEST( "INT_GPIO0_13"  , 29, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[13] );
		`IRQTEST( "INT_GPIO0_14"  , 30, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[14] );
		`IRQTEST( "INT_GPIO0_15"  , 31, TestDriver.ATOM_top.ATOM_design.u_peri_subsys.GPIO_0.IRQ[15] );
		`IRQTEST( "INT_DRAM0_CON" , 64, TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.IRQ__controller_int );
		`IRQTEST( "INT_DRAM0_PHY" , 65, TestDriver.ATOM_top.ATOM_design.u_dram0_subsys.IRQ__phy_pi_int     );
		`IRQTEST( "INT_DRAM1_CON" , 66, TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.IRQ__controller_int );
		`IRQTEST( "INT_DRAM1_PHY" , 67, TestDriver.ATOM_top.ATOM_design.u_dram1_subsys.IRQ__phy_pi_int     );
		`IRQTEST( "INT_DRAM2_CON" , 68, TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.IRQ__controller_int );
		`IRQTEST( "INT_DRAM2_PHY" , 69, TestDriver.ATOM_top.ATOM_design.u_dram2_subsys.IRQ__phy_pi_int     );
		`IRQTEST( "INT_DRAM3_CON" , 70, TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.IRQ__controller_int );
		`IRQTEST( "INT_DRAM3_PHY" , 71, TestDriver.ATOM_top.ATOM_design.u_dram3_subsys.IRQ__phy_pi_int     );
		`IRQTEST( "INT_PCIE0_cfg_vpd_int              " , 72, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__cfg_vpd_int              );
		`IRQTEST( "INT_PCIE0_cfg_link_eq_req_int      " , 73, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__cfg_link_eq_req_int      );
		`IRQTEST( "INT_PCIE0_usp_eq_redo_executed_int " , 74, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__usp_eq_redo_executed_int );
		`IRQTEST( "INT_PCIE0_edma_int_0               " , 75, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 0]             );
		`IRQTEST( "INT_PCIE0_edma_int_1               " , 76, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 1]             );
		`IRQTEST( "INT_PCIE0_edma_int_2               " , 77, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 2]             );
		`IRQTEST( "INT_PCIE0_edma_int_3               " , 78, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 3]             );
		`IRQTEST( "INT_PCIE0_edma_int_4               " , 79, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 4]             );
		`IRQTEST( "INT_PCIE0_edma_int_5               " , 80, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 5]             );
		`IRQTEST( "INT_PCIE0_edma_int_6               " , 81, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 6]             );
		`IRQTEST( "INT_PCIE0_edma_int_7               " , 82, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 7]             );
		`IRQTEST( "INT_PCIE0_edma_int_8               " , 83, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 8]             );
		`IRQTEST( "INT_PCIE0_edma_int_9               " , 84, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[ 9]             );
		`IRQTEST( "INT_PCIE0_edma_int_A               " , 85, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[10]             );
		`IRQTEST( "INT_PCIE0_edma_int_B               " , 86, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[11]             );
		`IRQTEST( "INT_PCIE0_edma_int_C               " , 87, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[12]             );
		`IRQTEST( "INT_PCIE0_edma_int_D               " , 88, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[13]             );
		`IRQTEST( "INT_PCIE0_edma_int_E               " , 89, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[14]             );
		`IRQTEST( "INT_PCIE0_edma_int_F               " , 90, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__edma_int[15]             );
		`IRQTEST( "INT_PCIE0_assert_inta_grt          " , 91, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__assert_inta_grt          );
		`IRQTEST( "INT_PCIE0_assert_intb_grt          " , 92, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__assert_intb_grt          );
		`IRQTEST( "INT_PCIE0_assert_intc_grt          " , 93, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__assert_intc_grt          );
		`IRQTEST( "INT_PCIE0_assert_intd_grt          " , 94, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__assert_intd_grt          );
		`IRQTEST( "INT_PCIE0_deassert_inta_grt        " , 95, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__deassert_inta_grt        );
		`IRQTEST( "INT_PCIE0_deassert_intb_grt        " , 96, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__deassert_intb_grt        );
		`IRQTEST( "INT_PCIE0_deassert_intc_grt        " , 97, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__deassert_intc_grt        );
		`IRQTEST( "INT_PCIE0_deassert_intd_grt        " , 98, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__deassert_intd_grt        );
		`IRQTEST( "INT_PCIE0_cfg_safety_corr          " , 99, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__cfg_safety_corr          );
		`IRQTEST( "INT_PCIE0_cfg_safety_uncorr        " ,100, TestDriver.ATOM_top.ATOM_design.u_pcie_subsys.SouthBUS__IRQ__cfg_safety_uncorr        );

		repeat(10) @(posedge clock);

		if( 0===testErrorCount ) begin
	      $display("\033[32mmsg: PASSED \033[0m");
		end else begin
	      $display("\033[31mmsg: FAILED with status (%d errors)\033[0m", testErrorCount);
		end

		$finish;
	end


endmodule : TestDriver
