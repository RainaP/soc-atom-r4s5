// See LICENSE.SEMIFIVE for license details.

module semifive_cpu_dummy #(
    parameter hartid=0,
              ID=  8 ,
              A = 40 ,
              D = 128,
              L = 8,
              AUSER = 32,
              RUSER = 4,
              MAX_BURST = 64,
              MAX_OUTSTANDING = 64
)(
    input               CLK__AXI,
    input               RSTN__AXI,

    output              IRQ__CORE ,
    input               IRQ__GIC  ,

    output              APB__BUS__PSEL,
    output              APB__BUS__PENABLE,
    output              APB__BUS__PWRITE,
    output [38-1:0]     APB__BUS__PADDR,
    output [31:0]       APB__BUS__PWDATA,
    input               APB__BUS__PREADY,
    input               APB__BUS__PSLVERR,
    input  [31:0]       APB__BUS__PRDATA,

    input   [ID-1:0]    AXI4__CORE__AWID,
    input   [A-1:0]     AXI4__CORE__AWADDR,
    input   [L-1:0]     AXI4__CORE__AWLEN,
    input   [2:0]       AXI4__CORE__AWSIZE,
    input   [1:0]       AXI4__CORE__AWBURST,
    input               AXI4__CORE__AWLOCK,
    input   [3:0]       AXI4__CORE__AWCACHE,
    input   [2:0]       AXI4__CORE__AWPROT,
    input   [AUSER-1:0] AXI4__CORE__AWUSER,
    input               AXI4__CORE__AWVALID,
    output              AXI4__CORE__AWREADY,
    input   [D-1:0]     AXI4__CORE__WDATA,
    input   [(D/8)-1:0] AXI4__CORE__WSTRB,
    input               AXI4__CORE__WLAST,
    input               AXI4__CORE__WVALID,
    output              AXI4__CORE__WREADY,
    output  [ID-1:0]    AXI4__CORE__BID,
    output  [RUSER-1:0] AXI4__CORE__BUSER,
    output              AXI4__CORE__BVALID,
    output  [1:0]       AXI4__CORE__BRESP,
    input               AXI4__CORE__BREADY,
    input   [ID-1:0]    AXI4__CORE__ARID,
    input   [A-1:0]     AXI4__CORE__ARADDR,
    input   [L-1:0]     AXI4__CORE__ARLEN,
    input   [2:0]       AXI4__CORE__ARSIZE,
    input   [1:0]       AXI4__CORE__ARBURST,
    input               AXI4__CORE__ARLOCK,
    input   [3:0]       AXI4__CORE__ARCACHE,
    input   [2:0]       AXI4__CORE__ARPROT,
    input   [AUSER-1:0] AXI4__CORE__ARUSER,
    input               AXI4__CORE__ARVALID,
    output              AXI4__CORE__ARREADY,
    output  [ID-1:0]    AXI4__CORE__RID,
    output  [D-1:0]     AXI4__CORE__RDATA,
    output  [1:0]       AXI4__CORE__RRESP,
    output              AXI4__CORE__RLAST,
    output  [RUSER-1:0] AXI4__CORE__RUSER,
    output              AXI4__CORE__RVALID,
    input               AXI4__CORE__RREADY,

    output  [ID-1:0]    AXI4__BUS__AWID,
    output  [A-1:0]     AXI4__BUS__AWADDR,
    output  [L-1:0]     AXI4__BUS__AWLEN,
    output  [2:0]       AXI4__BUS__AWSIZE,
    output  [1:0]       AXI4__BUS__AWBURST,
    output              AXI4__BUS__AWLOCK,
    output  [3:0]       AXI4__BUS__AWCACHE,
    output  [2:0]       AXI4__BUS__AWPROT,
    output  [AUSER-1:0] AXI4__BUS__AWUSER,
    output              AXI4__BUS__AWVALID,
    input               AXI4__BUS__AWREADY,
    output  [D-1:0]     AXI4__BUS__WDATA,
    output  [(D/8)-1:0] AXI4__BUS__WSTRB,
    output              AXI4__BUS__WLAST,
    output              AXI4__BUS__WVALID,
    input               AXI4__BUS__WREADY,
    input   [ID-1:0]    AXI4__BUS__BID,
    input   [RUSER-1:0] AXI4__BUS__BUSER,
    input               AXI4__BUS__BVALID,
    input   [1:0]       AXI4__BUS__BRESP,
    output              AXI4__BUS__BREADY,
    output  [ID-1:0]    AXI4__BUS__ARID,
    output  [A-1:0]     AXI4__BUS__ARADDR,
    output  [L-1:0]     AXI4__BUS__ARLEN,
    output  [2:0]       AXI4__BUS__ARSIZE,
    output  [1:0]       AXI4__BUS__ARBURST,
    output              AXI4__BUS__ARLOCK,
    output  [3:0]       AXI4__BUS__ARCACHE,
    output  [2:0]       AXI4__BUS__ARPROT,
    output  [AUSER-1:0] AXI4__BUS__ARUSER,
    output              AXI4__BUS__ARVALID,
    input               AXI4__BUS__ARREADY,
    input   [ID-1:0]    AXI4__BUS__RID,
    input   [D-1:0]     AXI4__BUS__RDATA,
    input   [1:0]       AXI4__BUS__RRESP,
    input               AXI4__BUS__RLAST,
    input   [RUSER-1:0] AXI4__BUS__RUSER,
    input               AXI4__BUS__RVALID,
    output              AXI4__BUS__RREADY
);
    assign IRQ__CORE = IRQ__GIC;

    assign APB__BUS__PSEL    = 0;
    assign APB__BUS__PENABLE = 0;
    assign APB__BUS__PWRITE  = 0;
    assign APB__BUS__PADDR   = 0;
    assign APB__BUS__PWDATA  = 0;

    assign AXI4__BUS__AWID    = AXI4__CORE__AWID    ;
    assign AXI4__BUS__AWADDR  = AXI4__CORE__AWADDR  ;
    assign AXI4__BUS__AWLEN   = AXI4__CORE__AWLEN   ;
    assign AXI4__BUS__AWSIZE  = AXI4__CORE__AWSIZE  ;
    assign AXI4__BUS__AWBURST = AXI4__CORE__AWBURST ;
    assign AXI4__BUS__AWLOCK  = AXI4__CORE__AWLOCK  ;
    assign AXI4__BUS__AWCACHE = AXI4__CORE__AWCACHE ;
    assign AXI4__BUS__AWPROT  = AXI4__CORE__AWPROT  ;
    assign AXI4__BUS__AWUSER  = AXI4__CORE__AWUSER  ;
    assign AXI4__BUS__AWVALID = AXI4__CORE__AWVALID ;
    assign AXI4__BUS__WDATA   = AXI4__CORE__WDATA   ;
    assign AXI4__BUS__WSTRB   = AXI4__CORE__WSTRB   ;
    assign AXI4__BUS__WLAST   = AXI4__CORE__WLAST   ;
    assign AXI4__BUS__WVALID  = AXI4__CORE__WVALID  ;
    assign AXI4__BUS__BREADY  = AXI4__CORE__BREADY  ;
    assign AXI4__BUS__ARID    = AXI4__CORE__ARID    ;
    assign AXI4__BUS__ARADDR  = AXI4__CORE__ARADDR  ;
    assign AXI4__BUS__ARLEN   = AXI4__CORE__ARLEN   ;
    assign AXI4__BUS__ARSIZE  = AXI4__CORE__ARSIZE  ;
    assign AXI4__BUS__ARBURST = AXI4__CORE__ARBURST ;
    assign AXI4__BUS__ARLOCK  = AXI4__CORE__ARLOCK  ;
    assign AXI4__BUS__ARCACHE = AXI4__CORE__ARCACHE ;
    assign AXI4__BUS__ARPROT  = AXI4__CORE__ARPROT  ;
    assign AXI4__BUS__ARUSER  = AXI4__CORE__ARUSER  ;
    assign AXI4__BUS__ARVALID = AXI4__CORE__ARVALID ;
    assign AXI4__BUS__RREADY  = AXI4__CORE__RREADY  ;

    assign  AXI4__CORE__AWREADY = AXI4__BUS__AWREADY ;
    assign  AXI4__CORE__WREADY  = AXI4__BUS__WREADY  ;
    assign  AXI4__CORE__BID     = AXI4__BUS__BID     ;
    assign  AXI4__CORE__BUSER   = AXI4__BUS__BUSER   ;
    assign  AXI4__CORE__BVALID  = AXI4__BUS__BVALID  ;
    assign  AXI4__CORE__BRESP   = AXI4__BUS__BRESP   ;
    assign  AXI4__CORE__ARREADY = AXI4__BUS__ARREADY ;
    assign  AXI4__CORE__RID     = AXI4__BUS__RID     ;
    assign  AXI4__CORE__RDATA   = AXI4__BUS__RDATA   ;
    assign  AXI4__CORE__RRESP   = AXI4__BUS__RRESP   ;
    assign  AXI4__CORE__RLAST   = AXI4__BUS__RLAST   ;
    assign  AXI4__CORE__RUSER   = AXI4__BUS__RUSER   ;
    assign  AXI4__CORE__RVALID  = AXI4__BUS__RVALID  ;
endmodule
