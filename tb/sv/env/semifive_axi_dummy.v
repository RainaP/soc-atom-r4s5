//----------------------------------------------------------------------------------------------
//
//    © 2021 SEMIFIVE Inc. - Proprietary and Confidential. All rights reserved.
//
//    NOTICE: All information contained herein is, and remains the property of SEMIFIVE Inc.
//    The intellectual and technical concepts contained herein are proprietary to SEMIFIVE Inc.
//    and may be covered by applicable patents, patents in process, and are protected by trade
//    secret and/or copyright law of applicable jurisdiction.
//
//    This work may not be copied, modified, re-published, uploaded, executed, or distributed
//    in any way, in any medium, whether in whole or in part, without prior written permission
//    from SEMIFIVE Inc.
//
//    The copyright notice above does not evidence any actual or intended publication or
//    disclosure of this work or source code, which includes information that is confidential
//    and/or proprietary, and is a trade secret, of SEMIFIVE Inc.
//
//----------------------------------------------------------------------------------------------
module semifive_axi_dummy (
	 input          clk
	,input          rst_n
	,input  [  6:0] awid
	,input  [ 36:0] awaddr
	,input  [  7:0] awlen
	,input  [  2:0] awsize
	,input  [  1:0] awburst
	,input          awlock
	,input  [  3:0] awcache
	,input  [  2:0] awprot
	,input  [  3:0] awqos
	,input  [  3:0] awregion
	,input  [  3:0] awuser
	,input          awvalid
	,output         awready
	,input  [511:0] wdata
	,input  [ 63:0] wstrb
	,input          wlast
	,input          wvalid
	,output         wready
	,output [  6:0] bid
	,output [  1:0] bresp
	,output [  3:0] buser
	,output         bvalid
	,input          bready
	,input  [  6:0] arid
	,input  [ 36:0] araddr
	,input  [  7:0] arlen
	,input  [  2:0] arsize
	,input  [  1:0] arburst
	,input          arlock
	,input  [  3:0] arcache
	,input  [  2:0] arprot
	,input  [  3:0] arqos
	,input  [  3:0] arregion
	,input  [  3:0] aruser
	,input          arvalid
	,output         arready
	,output [  6:0] rid
	,output [511:0] rdata
	,output [  1:0] rresp
	,output         rlast
	,output [  3:0] ruser
	,output         rvalid
	,input          rready
);
	assign awready = 1;
	assign wready  = 1;
	assign bid     = 0;
	assign bresp   = 0;
	assign buser   = 0;
	assign bvalid  = 0;
	assign arready = 1;
	assign rid     = 0;
	assign rdata   = 0;
	assign rresp   = 0;
	assign rlast   = 0;
	assign ruser   = 0;
	assign rvalid  = 0;

endmodule
