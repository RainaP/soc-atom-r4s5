# soc-atom-s5

ATOM SOC Project to collaborate between R4 and S5


# How to use

1. Create workspace
   ```
   %> git clone git@gitlab.semifive.com:R4/soc-atom-r4s5.git < your workspace neme >
   %> cd < your workspace nema >
   ```

2. Run build and simulation
   Sanity test
   ```
   make -C scripts
   ```
   Build RTL
   ```
   make -C scripts build
   ```
